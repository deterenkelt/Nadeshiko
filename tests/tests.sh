#! /usr/bin/env bash

#  tests.sh
#  A suite of a couple dozen tests (some are comprising a bunch), that should
#  assist in verifying, that Nadeshiko’s codebase is intact and all primary
#  and secondary features are working and the code doesn’t throw an error.
#  Updated for Nadeshiko v2.28.
#  Author: deterenkelt, 2018–2024

#  WTFPL

set -feEuT
shopt -s  extglob  inherit_errexit

BAHELITE_LOAD_MODULES=(
	menu
	variable_checks
	misc  # testing file name and metadata tags, see test no. 20.
)

# Verbosity for metadata collection on the encoded fragment
#   2 – regular output (few lines)
#   3 – ordinary output (Gathering metadata as an output block,
#       as you see in Nadeshiko.)
#   4 – verbose output, for debugging.
VERBOSITY=Gather_info=2

mypath=$(dirname "$(realpath --logical "$0")")
. "$mypath/../lib/bahelite/bahelite.sh"

#  To get “read_last_log()” into the namespace, without enabling logging for
#  the test suite. To do this trick, we need to temporarily unset the arguments
#  from the command line, as the Bahelite modules can accept arguments altering
#  the default for their initial setup. In other words, the logging module must
#  not see any arguments (the “source” command always conveys arguments from
#  the main script).
#
set --
. "$mypath/../lib/bahelite/modules/logging.sh"
set -- "${ARGS[@]}"

LIBDIR="$mypath/../lib"  audio=t  subs=t
. "$mypath/../lib/gather_source_info.sh"
. "$mypath/../lib/time_functions.sh"

#  A dummy declaration solely to make it possible to load all metaconf files
#  without error. We need to load settings from metaconf/, but that implies
#  that Bahelite’s “rcfile” module was loaded beforehand and declared some
#  variables. Typically, the main script (e.g. nadeshiko.sh) does that, but
#  here it’s preferable to avoid dealing with the config file infrastructure:
#    - One reason is that this test suite doesn’t have a config, while the
#      “rcfile” module reasonably expects, that if you’ve loaded the facility,
#      you need to work with config. If there’s nothing to work with, it is
#      most probably an error;
#    - The other reason is that for safety reasons this test suite was remade
#      into a safe simulation, test configs for Nadeshiko aren’t written to
#      ~/.config/nadeshiko/ and there’s no more need to back up the configs,
#      that the user had. It is safe and clean, while loading the “rcfile”
#      module would require a layer of isolation to separate files made by
#      this test suite and the real config files. As we’re trying to simu-
#      late the natural environment for nadeshiko, inclulding its .rc files,
#      it’s better to just have the two things separate.
#
declare -A  RCFILE_CHECKVALUE_VARS  RCFILE_REPLACEVALUE_VARS
noglob_off
for conf in "$mypath/../metaconf/nadeshiko."*.sh; do
	. "$conf"
done
noglob_rewind

nadeshiko_dir="$MYDIR/.."
defconf_dir="$MYDIR/../defconf"
config_dir="$HOME/.config/nadeshiko"

nadeshiko="$nadeshiko_dir/nadeshiko.sh"
nadempv="$nadeshiko_dir/nadeshiko-mpv.sh"
rcfile="$config_dir/nadeshiko-TEST.rc.sh"
nadempv_rcfile="$config_dir/nadeshiko-mpv-TEST.rc.sh"
example_rcfile="$defconf_dir/nadeshiko.10_main.rc.sh"
example_nadempv_rcfile="$defconf_dir/nadeshiko-mpv.10_main.rc.sh"

ffmpeg='ffmpeg'
ffprobe='ffprobe'

tests_dir="$MYDIR/TESTS/$(env "$nadeshiko" --version | sed -rn '1 s/Nadeshiko //p')"
source_sets="$MYDIR/tests_source_sets.sh"
mkdir -p "$tests_dir"

[ -r "$source_sets" ] || {
	cat <<-EOF >&2
	$source_sets wasn’t found!
	The tests in this file need these sets to be defined. The file should
	  contain bash arrays – name of the array becomes the name of a set –
	  with three items defined:
	  [0] – absolute path to a video file;
	  [1] – start time of the slice;
	  [2] – stop time of the slice.
	Example:
	bales_surprised_face=(
	    '/home/video/Talian.mkv'
	    '1:11:43.291'
	    '1:11:47.291'
	)
	EOF
	err "Create source sets first."
}
. "$source_sets"

for _source in ${!source_set_*}; do
	declare -n source_set="${_source}"
	[ -e "${source_set[0]}" ]  || {
		redmsg "Source ${_source} cannot be found:
		        ${source_set[0]}"
		sources_missing=t
	}
done
[ -v sources_missing ]  \
	&& err "Please restore paths to all sources in “tests_source_sets.sh”."

 # Remove the timestamp file and rc files used for testing purposes.
#
on_exit() {
	rm -f 't' "$rcfile" "$nadempv_rcfile"
	return 0
}


 # Changes a parameter in $rcfile.
#  $1 – parameter name
#  $2 – parameter value
#
change_in_rc() {
	local  param_name="$1"
	local  param_val="$2"

	# if grep -qE "^\s*#?\s*$param_name=.*" "$rcfile"; then
	# 	sed -ri "s/^\s*#?\s*$param_name=.*/$param_name=$param_val/" "$rcfile"
	# 	[ $? -ne 0 ] \
	# 		&& err "$FUNCNAME: Couldn’t change “$param_name” to “$param_val” – sed error."
	# else
		echo  >>"$rcfile"
		echo "$param_name=$param_val" >>"$rcfile"
	# fi

	return 0
}


prepare_test() {
	#  By default, take $test_NN_desc as description. Tests with internal
	#  subtests (13.a, 13.b, 13.c…) may call prepare_test before each such
	#  subtest and pass a custom description.
	#  That description is passed via $1.
	local test_description="${1:-}" desc
	unset fname_prefix

	if [ "$test_description" ]; then
		fname_prefix="test_${test_description%% *}"
	else
		declare -n desc="${FUNCNAME[1]}_desc"
		test_description="${FUNCNAME[1]#test_}.  $desc"
		fname_prefix=${FUNCNAME[1]}
	fi
	echo

	info "${__bri}$test_description${__s}"
	declare -g where_to_place_new_file="$tests_dir"

	set +f
	#  Removing old files. If the test wouldn’t generate new file(s),
	#  there should be no files – old ones shouldn’t be confused with the new.
	rm -f "$tests_dir/$fname_prefix"* || true
	set -f

	cp "$example_rcfile" "$rcfile"
	cp "$example_nadempv_rcfile" "$nadempv_rcfile"

	[ -r "${source_set[0]}" ] \
		|| err "Source video not a readable file: “${source_set[0]}”."

	#  Encoded file shouldn’t be older than this file.
	#  This is to be sure, that the file mentioned in the last log
	#  is the real file, and not some old encoded file.
	touch "$TMPDIR/t"

	return 0
}


 # Call nadeshiko.sh
#  Passes common parameters: source file and times, directory, where to place
#  encoded file, file name prefix “test_NN” and custom parameters.
#  [$1..n] – custom parameters.
#
encode() { __encode "$@"; }
encode-notime() { __encode "$@"; }
__encode() {
	local fpath="${source_set[0]}"
	local time1
	local time2


	[ "${FUNCNAME[1]}" = encode ] && {  \
		time1="${source_set[1]}"
		time2="${source_set[2]}"
	}

	env "$nadeshiko" "$rcfile"  \
	                 "$fpath"  \
	                 ${time1:+$time1}  \
	                 ${time2:+$time2}  \
	                 "fname_pfx=$fname_prefix"  \
	                 "$where_to_place_new_file"  \
	                 "$@"
}


get_the_last_encoded_file() {
	local  lastlog_path
	local  last_file

	lastlog_path=$(
		export LOGDIR=$HOME/.cache/nadeshiko/logs/
		get_lastlog_path 'nadeshiko'
	)

	last_file=$(
		log_lines=$(sed -rn '/Encoded successfully/ { N; p }' "$lastlog_path")
		log_lines=$(strip_colours "$log_lines")
		sed -rn 'N; s/^(\s*)\* Encoded successfully\.\s*\n\1  //p'  <<<"$log_lines"
	)

	echo "$last_file"

	return 0
}


post_test() {
	unset  new_file  file_created

	declare -g new_file="$(get_the_last_encoded_file)"

	if [ -r "$new_file"  -a  -f "$new_file" ]; then
		[ "$new_file" -nt "$TMPDIR/t" ] && declare -g file_created=t || {
			warn "No file created."
			return 8
		}
	else
		warn "No such file: “$new_file”."
		return 8
	fi

	declare -gA enc${FUNCNAME[1]#test_}
	#
	#  Alas, bash doesn’t allow to declare an array variable with a const-
	#  ructed name and set it in a single command, so…
	#
	local -n _arr="enc${FUNCNAME[1]#test_}"
	_arr[path]="$new_file"
	unset -n _arr

	gather_source_info 'test-suite' "enc${FUNCNAME[1]#test_}"

	return 0
}


play_encoded_files() {
	local f

	if (set +f;  ls "$tests_dir/${FUNCNAME[1]}"* &>/dev/null); then
		info "Encoding seems to be complete.
		      Press any key to play files > "
		read -n1

		set +f
		for f in "$tests_dir/${FUNCNAME[1]}"* ; do
			mpv --loop-file "$f"
		done
		set -f
	else
		info "The test didn’t produce any video files to play."
	fi


	return 0
}


 # Ask, if the test was successfull or not.
#  The result is a variable test_NN_result, which contains either “OK”
#  or “Fail” – it will be shown in the main menu.
#  $1 – a text describing conditions. It better have newlines.
#
ask_if_ok() {
	local  conditions="$1"
	local  test_no="${FUNCNAME[1]}"
	local  lines
	local  xdialog_max_width_chars=48
	local  xdialog_window_height

	conditions="${conditions//+([[:space:]])/ }"
	lines=$(( ${#conditions} / xdialog_max_width_chars ))
	xdialog_window_height=$(( 116 + 20*( (lines>1 ? lines : 1)-1 ) ))
	conditions=$(
		set -f
		shopt -s extglob
		nroff <<-EOF 2>/dev/null
		.pl 1
		.ll $xdialog_max_width_chars
		$conditions
		EOF
	)
	errexit_off
	if Xdialog --title "Nadeshiko test suite"  \
	           --ok-label "OK"  \
	           --cancel-label="Cancel"  \
	           --buttons-style default  \
	           --yesno "$conditions"  \
	           450x$xdialog_window_height
	then
		declare -g ${test_no}_result=OK
	else
		declare -g ${test_no}_result=Fail
	fi
	errexit_rewind

	return 0
}


#
#  Below are several dozens of test cases are grouped into several sets.
#  Certain tests require specific input data, such as having subtitles
#  and displaying them (there should be lines on the screen) on the time
#  Nadeshiko should cut.
#
#

 # Tests 01–04 are encoding samples, that go to wiki.
#  Readme.md points to that page.
#
test_01_desc='Animation, 4 seconds, 1080p'
test_01() {
	declare -gn source_set='source_set_1_yuru_camp'
	prepare_test
	encode 'tiny'
	post_test
	play_encoded_files
	conditions="Playback, 1080p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_02_desc='Animation, 4 minutes, 1080p'
test_02() {
	declare -gn source_set='source_set_2_seiken'
	prepare_test
	encode 'small'
	post_test
	play_encoded_files
	conditions="Playback, 1080p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_03_desc='Film, 4 seconds, 1080p (orig. 818p), 6ch → 2ch'
test_03() {
	declare -gn source_set='source_set_4_the_flowers'
	prepare_test
	encode 'tiny'
	post_test
	play_encoded_files
	conditions="Playback, 1080p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_04_desc='Film, 4 minutes, 1080p (orig. 818p), 6ch → 2ch'
test_04() {
	declare -gn source_set='source_set_5_the_flowers'
	prepare_test
	encode size=2M
	post_test
	play_encoded_files
	conditions="Playback, 1080p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_05_desc='Animation, 2 seconds, 1080p'
test_05() {
	declare -gn source_set='source_set_3_seiken'
	prepare_test
	encode size=2M
	post_test
	play_encoded_files
	conditions="Playback, 1080p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_06_desc='Animation, 2 seconds, 1080p → forced scale to 480p, vb12M, ab88k'
test_06() {
	declare -gn source_set='source_set_3_seiken'
	prepare_test
	encode  size=2M  480p
	post_test
	play_encoded_files
	echo
	# VP9 and Opus in a webm container don’t provide bitrate metadata.
	[ "${enc06_v[bitrate]:-}" = "12000" ] \
		&& info "Vbitrate is 12M, as expected." \
		|| warn "Vbitrate is ${enc06_v[bitrate]:-<?>} k, should be 12000 k."
	[ "${enc06_a[bitrate]:-}" = "88" ] \
		&& info "Abitrate is 88k, as expected." \
		|| warn "Abitrate is ${enc06_a[bitrate]:-<?>} k, shoud be 88 k."
	echo
	conditions="Playback, 480p, audio, v+a bitrates"
	ask_if_ok "$conditions"
	return 0
}

test_07_desc='Animation, 2 seconds, 1080p → default scale (in RC) to 720p'
test_07() {
	declare -gn source_set='source_set_3_seiken'
	prepare_test
	change_in_rc 'scale' '720p'
	encode  size=2M
	post_test
	play_encoded_files
	conditions="Playback, 720p, audio"
	ask_if_ok "$conditions"
	return 0
}

test_08_desc='Nadeshiko-mpv UI, postpone encode, hardcoding external VTT subs'
test_08() {
	declare -gn source_set='source_set_6_luna'
	prepare_test
	cat <<-EOF >>"$rcfile"

	new_filename_prefix=$FUNCNAME
	EOF
	infon "This test will launch an mpv instance and operate it automatically
	       to some extent. Please don’t make any input that is not requested
	       from your side. Now an mpv window will spawn. The video will be
	       paused. Place the window so that you could see both it and the
	       terminal. Press any key to contine ${__g}>${__s} "
	read -n1
	echo
	local temp_sock="$(mktemp -u)"
	cat <<-EOF >"$nadempv_rcfile"
	# nadeshiko-mpv.rc.sh v1.2
	mpv_sockets=(  [Usual]='$temp_sock'  )
	EOF
	env mpv --x11-name=mpv-nadeshiko-test-suite-preview  \
	        --no-terminal  \
	        --input-ipc-server="$temp_sock"  \
	        --loop-file=inf  \
	        --sub-file="$mypath/${source_set[3]##*/}"  \
	        --start=43.994 --pause  \
	        --msg-level=all=v  \
	        "${source_set[0]}" &
	        # --log-file=/tmp/ooooo  \
	sleep 1
	cat <<-EOF | socat - "$temp_sock" || true
	{ "command": ["set_property", "fullscreen", false] }
	EOF

	menu-bivar 'Did mpv spawn?' 'Yes, continue the test' 'Cancel the test'
	[ "$CHOSEN" = 'Cancel the test' ]  \
		&& abort "Test ${FUNCNAME[0]#test_} cancelled."

	infon "Setting Time1.
	       Press any key to continue ${__g}>${__s} "
	read -n1
	echo
	env "$nadeshiko_dir/nadeshiko-mpv.sh" "$nadempv_rcfile"

	infon "Enabling playback for a couple of seconds, then stopping to set Time2.
	       Press any key to continue ${__g}>${__s} "
	read -n1
	echo
	cat <<-EOF | socat - "$temp_sock" || err "Mpv-ipc: Connection refused."
	{ "command": ["set_property", "pause", false] }
	EOF

	sleep 2

	cat <<-EOF | socat - "$temp_sock" || err "Mpv-ipc: Connection refused."
	{ "command": ["set_property", "pause", true] }
	EOF

	env "$nadeshiko_dir/nadeshiko-mpv.sh" "$nadempv_rcfile" postpone

	info "Mpv may be closed now."
	sleep 1

	infon "Let’s look if the directory for postponed commands has stored jobs…
	       Press any key to continue ${__g}>${__s} "
	read -n1
	echo
	env "$nadeshiko_dir/nadeshiko-do-postponed.sh"
	echo

	local  new_file_name
	new_file_name=${source_set[0]##*/}
	new_file_name=${new_file_name%\.*}
	new_file_name="$( set +f;  ls "$PWD/$new_file_name"* )"
	mv "$new_file_name" "$tests_dir/$FUNCNAME ${new_file_name##*/}"
	play_encoded_files
	conditions="Playback, audio, subtitles"
	rm "$temp_sock"
	ask_if_ok "$conditions"
	pkill -f "mpv.*nadeshiko-test-suite-preview" &>/dev/null
	return 0
}

test_09_desc='Time2 as seconds >59 (0–72 s), duration check, nosub, H264+AAC.'
test_09() {
	declare -gn source_set='source_set_7_yuru_72s'
	prepare_test
	# Codecs
	change_in_rc 'ffmpeg_vcodec' 'libx264'
	change_in_rc 'ffmpeg_acodec' 'libfdk_aac'
	# We test duration, not quality here.
	change_in_rc 'libx264_preset' 'ultrafast'
	encode  size=10M  nosub || exit $?
	post_test
	echo -e '\n'
	(( ${enc09_c[duration_total_s_ms]%\.*} == 72 )) \
		&& info "Duration is 72 s." \
		|| warn "Duration is ${enc09_c[duration_total_s_ms]%\.*} s, should be 72 s."
	[ "${enc09_v[format]}" = AVC ] \
		&& info "Video codec is AVC." \
		|| warn "Video codec is ${enc09_v[format]}, should be AVC."
	[ "${enc09_a[format]}" = AAC ] \
		&& info "Audio codec is AAC." \
		|| warn "Audio codec is ${enc09_a[format]}, should be AAC."
	echo -e '\n'
	play_encoded_files
	conditions="duration = 1:12, nosub, H264+AAC"
	ask_if_ok "$conditions"
	return 0
}

test_10_desc='Fit to size: 10M with k=1000. 72 seconds, noaudio, VP9.'
test_10() {
	declare -gn source_set='source_set_7_yuru_72s'
	prepare_test
	#  We test fitting to size, not quality here.
	#  In short, the idea is to make Nadeshiko downscale a video (supposedly
	#  a 1080p one to 720p) in order to fit the duration into 10 MiB.
	# encode size=10M 'k=1000' 'noaudio'
	encode size=10M 'k=1000' 'noaudio'

	post_test || err 'No file was encoded.'
	echo
	(( ${enc10[size_B]:-0} <= 10*1000*1000 ))  \
		&& info "Size fits in 10M with k=1000."  \
		|| warn "File size DOES NOT fit 10M with k=1000."
	[ "${enc10_v[format]:-}" = VP9 ]  \
		&& info "Video codec is VP9."  \
		|| warn "Video codec is ${enc10_v[format]:-}, should be VP9."
	info "Audio codec is “${enc10_a[format]:-}” <-- should be empty."
	echo
	play_encoded_files
	conditions="duration = 1:12, VP9, noaudio, 10M with k=1000"
	ask_if_ok "$conditions"
	return 0
}

test_11_desc='Quit on critical overshoot. Encoding settings are deliberately worsened.'
test_11() {
	declare -gn source_set='source_set_7_yuru_72s'

	#  15% is the Nadeshiko default for libvpx-vp9, we define it here
	#  just to have correct values in both the config, in the (future) log
	#  file and in the message here. The percent may chance in the future,
	#  but this amount would still be suitable for testing the mechanics.
	#
	local  pct=15

	prepare_test
	#  We test fitting to size, not quality here.
	#  Though if time would allow, it’d be better to test with a slower setup…
	#  -deadline for pass1 is set to “good”, because “realtime” somehow
	#  doesn’t produce the log needed for pass2. free_shrugs.tiff
	change_in_rc 'libvpx_vp9_pass1_deadline' 'good'
	change_in_rc 'libvpx_vp9_pass1_cpu_used' '4'
	change_in_rc 'libvpx_vp9_pass2_deadline' 'realtime'
	change_in_rc 'libvpx_vp9_pass2_cpu_used' '4'
	change_in_rc 'bitres_profile_360p[libvpx-vp9_max_q]' '60'
	change_in_rc 'bitres_profile_480p[libvpx-vp9_max_q]' '60'
	change_in_rc 'bitres_profile_576p[libvpx-vp9_max_q]' '60'
	change_in_rc 'bitres_profile_720p[libvpx-vp9_max_q]' '60'
	change_in_rc 'bitres_profile_1080p[libvpx-vp9_max_q]' '60'
	change_in_rc 'libvpx_vp9_critical_overhead_pct' "$pct"
	encode size=10M 'k=1000' 'noaudio'  \
		|| info "It seems that no file was encoded!"
	echo
	conditions="Nadeshiko overshot size on more than $pct% and quit."
	ask_if_ok "$conditions"
	return 0
}

test_12_desc='Redo encode on a <15% size overshoot.'
test_12() {
	declare -gn source_set='source_set_7_yuru_72s'
	prepare_test
	#  We test fitting to size, not quality here.
	#  Though if time would allow, it’d be better to test with a slower setup…
	#  -deadline for pass1 is set to “good”, because “realtime” somehow
	#  doesn’t produce the log needed for pass2. free_shrugs.tiff

	change_in_rc 'libvpx_vp9_pass1_deadline' 'good'
	change_in_rc 'libvpx_vp9_pass1_cpu_used' '4'
	change_in_rc 'libvpx_vp9_pass2_deadline' 'realtime'
	change_in_rc 'libvpx_vp9_pass2_cpu_used' '4'

	change_in_rc 'bitres_profile_360p[libvpx-vp9_min_q]' '7'
	change_in_rc 'bitres_profile_360p[libvpx-vp9_max_q]' '37'

	change_in_rc 'bitres_profile_480p[libvpx-vp9_min_q]' '7'
	change_in_rc 'bitres_profile_480p[libvpx-vp9_max_q]' '37'

	change_in_rc 'bitres_profile_576p[libvpx-vp9_min_q]' '7'
	change_in_rc 'bitres_profile_576p[libvpx-vp9_max_q]' '37'

	change_in_rc 'bitres_profile_720p[libvpx-vp9_min_q]' '7'
	change_in_rc 'bitres_profile_720p[libvpx-vp9_max_q]' '37'

	change_in_rc 'bitres_profile_1080p[libvpx-vp9_min_q]' '7'
	change_in_rc 'bitres_profile_1080p[libvpx-vp9_max_q]' '37'

	encode size=2M 'k=1000' 'noaudio'
	echo
	conditions="Nadeshiko should have overshot size and redo the encode several
	times. Usually the amount or retries in this test is between 5 and 10,
	depending on the source and with the deliberately worsened -cpu-used
	parameter to speed the test up. (In reality nadeshiko would do an encode
	slower, but with less retries.)"
	ask_if_ok "$conditions"
	return 0
}

test_13_desc='Invalid input in various combinations. Nadeshiko shouldn’t encode.'
test_13() {
	declare -gn source_set='source_set_1_yuru_camp'
	milinc

	prepare_test '13.a  Wrong Time2: 0.00:00'
	#     This is OK --vv   v------ This is not.
	if  encode-notime  10  0.00:00
	then
		warn "Fail – Time2 was wrong.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test '13.b  Options that cannot be used together: noaudio and ab100k'
	#          vvvvvvv   vvvvvv
	if encode 'noaudio' 'ab100k'; then
		warn "Fail – “noaudio” and “ab100k” cannot be used together.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test '13.c  Options that cannot be used together: NNNp and crop'
	#          vvvv   vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	if encode "576p" "crop=$((1920/2)):120:200:$((1080/2))"; then
		warn "Fail – “NNNp” and “crop” cannot be used together.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test '13.d  Wrong vcodec and acodec: hurr and durr'
	change_in_rc 'ffmpeg_vcodec' 'hurr'  # codec doesn’t exist
	change_in_rc 'ffmpeg_acodec' 'durr'  # codec doesn’t exist
	if encode; then
		warn "Fail – “hurr” and “durr” aren’t real codecs.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test "13.e  Incompatible combination of real codecs and container:
	                    mp4 + libx264 + libvorbis"
	change_in_rc 'container'     'mp4'        # intermingling one of the two
	change_in_rc 'ffmpeg_vcodec' 'libx264'    # main sets
	change_in_rc 'ffmpeg_acodec' 'libvorbis'  # (mp4+libx264+aac)
	if encode; then
		warn "Fail – mp4, libx264 and libvorbis cannot be used together.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test "13.f  Incompatible combination of codecs and container:
	                    webm + libvpx-vp9 + aac"
	change_in_rc 'container'     'webm'        # intermingling one of the two
	change_in_rc 'ffmpeg_vcodec' 'libvpx-vp9'  # main sets
	change_in_rc 'ffmpeg_acodec' 'aac'         # (webm+libvpx-vp9+libvorbis)
	if encode; then
		warn "Fail – webm, libvpx-vp9 and aac cannot be used together.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	prepare_test "13.g  Incompatible combination of codecs and container:
	                    webm + libtheora + libvorbis"
	change_in_rc 'container'     'webm'
	change_in_rc 'ffmpeg_vcodec' 'libtheora'  # exists, but not in the list
	change_in_rc 'ffmpeg_acodec' 'libvorbis'
	if encode; then
		warn "Fail – mp4, libvpx-vp9 and libvorbis cannot be used together.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	local ext_sub=$(mktemp -u)
	prepare_test "13.h  Pass external subtitles, which file doesn’t exist
	                    subs=$ext_sub"
	if encode "subs=$ext_sub" ; then
		warn "Fail – external subtitle file doesn’t exist.
		      Nadeshiko shouldn’t have encoded anything."
	else
		info "No encode – as it should be!"
	fi

	mildec
	echo
	info "All runs are complete.
	      Please verify, that Nadeshiko refused to encode
	      and press any key to continue > "
	read -n1
	conditions="No encodes. No warnings"
	ask_if_ok "$conditions"
	return 0
}

test_14_desc='Cropping'
test_14() {
	declare -gn source_set='source_set_8_slow_crop_to_subs'
	prepare_test
	#                 The south-west part with subtitles
	encode  size=10M "crop=$((1920/2)):$((1080/2)):200:$((1080/2))"
	post_test || exit $?
	play_encoded_files
	conditions="Playback, audio, subs, cropped"
	ask_if_ok "$conditions"
	return 0
}

 # This set requires you to put at the bottom of test_source_sets.sh
#  a line like uploader="$HOME/bin/uploader.sh" that would be sourced
#  from there. uploader.sh must be a script that uploads your files.
#
test_15_desc='Use both main codec combinations and test playback in the browser.'
test_15() {
	[ -x "$uploader" ] \
		|| err "Uploader “$uploader” is not an executable file."
	declare -gn source_set='source_set_3_seiken'
	prepare_test '15.a  Making a clip with WebM + VP9 + Opus and uploading it'
	encode 'tiny'
	post_test
	play_encoded_files
	info "Press any key to upload > "
	read -n1
	info 'Uploading…'
	"$uploader" "$new_file"

	prepare_test '15.b  Making a clip with MP4 + H264 + AAC and uploading it'
	change_in_rc 'ffmpeg_vcodec' 'libx264'
	change_in_rc 'ffmpeg_acodec' 'libfdk_aac'
	encode 'tiny'
	post_test
	play_encoded_files
	info "Press any key to upload > "
	read -n1
	info 'Uploading…'
	"$uploader" "$new_file"

	info "Both files should be uploaded by now. Please watch them
	      and then press any key to continue > "
	read -n1
	conditions="Both files, webm and mp4, are playing in the browser"
	ask_if_ok "$conditions"
	return 0
}

test_16_desc='With and without row_mt. Time stats'
test_16() {
	declare -gn source_set='source_set_9_yuru_20s'
	prepare_test '16.a  ROW-MT: off'
	change_in_rc 'time_stat' 't'
	change_in_rc 'libvpx_row_mt' '0'
	encode size=10M fname_pfx="$fname_pfx without row-mt"
	post_test

	prepare_test '16.b  ROW-MT: on'
	change_in_rc 'time_stat' 't'
	change_in_rc 'libvpx_row_mt' '1'
	encode size=10M fname_pfx="$fname_pfx with row-mt"

	echo
	info "Encode complete. Press any key to continue > "
	read -n1
	conditions="Videos created, decide which is better and encoded faster."
	ask_if_ok "$conditions"
	return 0
}

test_17_desc='Looping the last frame for 5 sec with VP9. Low-quality Opus to Opus'
test_17() {
	declare -gn source_set='source_set_10_konosuba_loop_last_fr'
	prepare_test
	encode  loop_last_frame=5  nosubs
	post_test
	play_encoded_files
	#  This shows the tester, what he should be looking for.
	#  Would be handy to show it before the test, too…
	conditions="The last frame looped for 5 seconds, no extra frames at the end."
	ask_if_ok "$conditions"
	return 0
}

test_18_desc='Looping the last frame (extedned, 15 s; H.264); colour space conversion; seekbefore.'
test_18() {
	declare -gn source_set='source_set_11_tenki_seekbefore_cspconv_llf_redo'
	prepare_test
	change_in_rc 'ffmpeg_vcodec' 'libx264'
	change_in_rc 'ffmpeg_acodec' 'libfdk_aac'
	#  This source encounters an error, when the primary method is used.
	#  “[libx264 @ 0x55b2328c29c0] slice=P but 2pass stats say B”.
	#  As this concerns only libx264, it’s considered a minor bug, and the
	#  alternative method should fire up, when primary fails. Depedning on
	#  the needs, the test may be changed to start llf mode with the primary
	#  or with the secondary method. As it is now, the primary method is tried
	#  and then (at run time) Nadeshiko switches to the alternative method.
	#  The line below isn’t needed, it’s left as a reminder.
	# change_in_rc 'loop_last_frame_use_alter_method' 'no'
	encode  loop_last_frame=15  subs
	post_test
	play_encoded_files
	#  This shows the tester, what he should be looking for.
	#  Would be handy to show it before the test, too…
	conditions="The encode using advanced seeking. The BT.2020 ncl colour space
converted to BT.709 without drastic colour loss. The last frame looped for
5 seconds. Re-encoded one time, since the 1 reserved frame in the looped part
was not enough (2 is enough). The looped fragment should look identical to the
frames before it thanks to the reuse of the filter chain 1 in chain 2."
	ask_if_ok "$conditions"
	return 0
}

test_19_desc='Looping the last frame and cropping overlay subtitles (DVD); seekbefore.'
test_19() {
	declare -gn source_set='source_set_12_lodoss_dvd'
	prepare_test
	change_in_rc 'ffmpeg_vcodec' 'libx264'
	change_in_rc 'ffmpeg_acodec' 'libfdk_aac'

	encode  audio:1  \
	        subs:0   \
	        deinterlace=on  \
	        size=20M        \
	        crop=694:394:6:40  \
	        loop_last_frame=3  \

	post_test
	play_encoded_files
	#  This shows the tester, what he should be looking for.
	#  Would be handy to show it before the test, too…
	conditions="Nadeshiko should have applied overlay filter to render DVD subtitles,
	crop the original frame and make the last frame loop for 3 seconds. Looping
	the last frame on a DVD source should force exponential incrementation of
	the number of reserved frames. Tailoring of the video bitrate should have taken
	place according to the default settings (\"by_orig_res\", because the frame has
	4:3 ratio, but not \"by_crop_res\" since the default is to let the cropped frame
	demand full sized frame’s bitrate."
	ask_if_ok "$conditions"
	return 0
}


test_20_desc='Strings for the output file name and metadata tags.'
test_20() {

	validate_results() {
		local  fname="$1"
		local  title="$3"
		local  descr="$5"
		local  fname_size
		local  title_size
		local  descr_size
		local  fname_test_pattern="$2"
		local  title_test_pattern="$4"
		local  descr_test_pattern="$6"
		local  test_failed
		local  test_varname
		local  test_varval
		local  test_pattern

		cat <<-EOF
		Produced strings:
		fname = “$fname”
		title = “$title”
		descr = “$descr”
		EOF

		fname_size=$( echo -n "$fname" | wc --bytes )
		title_size=$( echo -n "$title" | wc --bytes )
		descr_size=$( echo -n "$descr" | wc --bytes )

		(( fname_size > 255 ))  \
			&& redmsg "new_file_name exceeds 255 B."  \
			&& test_failed=t
		(( title_size > 255 ))  \
			&& redmsg "outp_mdata_title exceeds 255 B."  \
			&& test_failed=t
		(( descr_size > 255 ))  \
			&& redmsg "outp_mdata_descr exceeds 255 B."  \
			&& test_failed=t

		for test_varname in fname title descr; do
			local -n test_varval=$test_varname
			local -n test_pattern=${test_varname}_test_pattern
			[[ "$test_varval" =~ ^$test_pattern$ ]]  || {
				redmsg "$test_varname: mismatch with the expected result."
				test_failed=t
			}
		done

		if [ -v test_failed ]; then
			redmsg "$test_no.: test ${__r}failed${__s}."
		else
			info "$test_no.: test ${__g}passed${__s}."
		fi
		return 0
	}
	export -f  validate_results


	 # Considering that this test suite runs on the same version of Bahelite
	#  as Nadeshiko, there’s no need to define info(), warn(), err() and other
	#  functions as dummies. And replace_windows_unfriendly_chars(), which is
	#  part of Bahelite, too (module “misc”).
	#
	(
		test_no='20a'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: normal length"
		declare -gA src=( [path]="$TMPDIR/A.mkv" )
		touch "$TMPDIR/A.mkv"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -gA src_v=( [frame_rate]='23.976' )
		declare -gA src_c=( [title]='Some title, yo' )
		declare -g new_filename_user_prefix
		declare -g scale
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='webm'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "A\ 0:01.23–7:08:09\.32\.webm"  \
		                 "${outp_mdata_title:-}"  \
		                    "\(A\ fragment\ of\)\ Some\ title,\ yo"  \
		                 "${outp_mdata_description:-}"  \
		                    "A"
	)
	(
		test_no='20b'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: normal length, target file and file-1 exist"
		declare -gA src=( [path]="$TMPDIR/B.webm" )
		touch "$TMPDIR/B.webm"
		touch "$TMPDIR/B-1.webm"
		declare -gA start=( [ts]='' )
		declare -gA stop=(  [ts]='' )
		declare -g encoding_entire_file=t
		declare -gA src_v=( [frame_rate]='23.976' )
		declare -gA src_c=( [title]='Some title, yo' )
		declare -g new_filename_user_prefix
		declare -g scale
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='webm'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "B-2.webm"  \
		                 "${outp_mdata_title:-}"  \
		                    "Some\ title,\ yo"  \
		                 "${outp_mdata_description:-}"  \
		                    "B"
	)
	(
		test_no='20c'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: normal length, 60 fps require 3-digit milliseconds, simulated autoscale to 720p"
		declare -gA src=( [path]="$TMPDIR/C.webm" )
		touch "$TMPDIR/C.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='59.940' )
		declare -gA src_c=( [title]='' )
		declare -g new_filename_user_prefix
		declare -g scale='720'
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='mp4'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "C\ 0:01\.235–7:08:09\.321\ \[720p]\[AS]\.mp4"  \
		                 "${outp_mdata_title:-}"  \
		                    "\(A\ fragment\ of\)\ C"  \
		                 "${outp_mdata_description:-}"  \
		                    "C"
	)
	(
		test_no='20d'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: normal length, filename prefix (within 200 B), forced scale to 576p"
		declare -gA src=( [path]="$TMPDIR/D.webm" )
		touch "$TMPDIR/D.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='23.976' )
		declare -gA src_c=( [title]='Behold, it’s the D' )
		declare -g new_filename_user_prefix='A long, long string, whose length is at the 200 B limit. 笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑'
		declare -g scale='576'
		declare -g forced_scale=t
		declare -g version='X.Y.Z'
		declare -g container='mp4'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "A long,\ long\ string,\ whose\ length\ is\ at\ the\ 200 B\ limit\.\ 笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑笑\ D\ 0:01\.23–7:08:09\.32\ \[576p]\[FS]\.mp4"  \
		                 "${outp_mdata_title:-}"  \
		                    "\(A\ fragment\ of\)\ Behold,\ it’s\ the\ D"  \
		                 "${outp_mdata_description:-}"  \
		                    "D"
	)
	(
		test_no='20e'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: file name and title of exceeding length, no filename prefix"
		declare -gA src=( [path]="$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm" )
		#  Name without extension: 250 bytes. Plus 5 bytes for “.webm” equals 258 bytes.
		touch "$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='23.976' )
		#  ↓ 255 B
		declare -gA src_c=( [title]='YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' )
		declare -g new_filename_user_prefix
		declare -g scale
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='webm'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "E[e]{222}…eeeeeeh\ 0:1\.23-7:8:9\.32\.webm"  \
		                 "${outp_mdata_title:-}"  \
		                    "\(A\ fragment\ of\)\ Y[O]{218}ABCDEFGHIJ…4567890"  \
		                 "${outp_mdata_description:-}"  \
		                    "E[e]{248}h"
	)
	(
		test_no='20f'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: file name and title of exceeding length, filename prefix is set AND used in the title"
		declare -gA src=( [path]="$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm" )
		#  Name without extension: 250 bytes. Plus 5 bytes for “.webm” equals 258 bytes.
		touch "$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='23.976' )
		#  ↓ 255 B
		declare -gA src_c=( [title]='YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' )
		declare -g new_filename_user_prefix='失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ　⧸　退屈させないよに対立させなくちゃね　⧸　三つ巴の正義もぐちゃぐちゃになってく'
		declare -g include_fname_pfx_to_mdata_title=t
		declare -g scale
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='webm'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ　⧸　退屈させないよに対立させなくちゃね　⧸　三つ巴の正義もぐちゃぐちゃになってく\.webm"  \
		                 "${outp_mdata_title:-}"  \
		                    "失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ　⧸　退屈させないよに対立させなくちゃね　⧸　三つ巴の正義もぐちゃぐちゃになってく YOOOO…4567890"  \
		                 "${outp_mdata_description:-}"  \
		                    "E[e]{248}h"
	)
	(
		test_no='20g'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: file name and title of exceeding length; filename prefix is set AND used in the title, this time within 100 B"
		declare -gA src=( [path]="$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm" )
		#  Name without extension: 250 bytes. Plus 5 bytes for “.webm” equals 258 bytes.
		touch "$TMPDIR/Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='23.976' )
		#  ↓ 255 B
		declare -gA src_c=( [title]='YOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' )
		declare -g new_filename_user_prefix='失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ'
		declare -g include_fname_pfx_to_mdata_title=t
		declare -g scale
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='webm'
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ\ E[e]{105}…eeeeeeh\ 0:1\.23-7:8:9\.32\.webm"  \
		                 "${outp_mdata_title:-}"  \
		                    "失敗作だったよ　あらかじめの不平等̅　⧸　真上から見下ろして掻き回してたんだよ\ Y[O]{127}…4567890"  \
		                 "${outp_mdata_description:-}"  \
		                    "E[e]{248}h"
	)
	(
		test_no='20h'
		echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
		info "Test $test_no: file name and title of small length; filename prefix is set AND used in the title; LFL=15; everything should fit nicely into 255 B"
		declare -gA src=( [path]="$TMPDIR/…and a side of fri-i-ies.webm" )
		#  Name without extension: 250 bytes. Plus 5 bytes for “.webm” equals 258 bytes.
		touch "$TMPDIR/…and a side of fri-i-ies.webm"
		declare -gA start=( [ts]='00:00:01.235' )
		declare -gA stop=(  [ts]='07:08:09.321' )
		declare -g encoding_entire_file
		declare -gA src_v=( [frame_rate]='23.976' )
		#  ↓ 255 B
		declare -gA src_c=( [title]='Hambur— Hambur—…' )
		declare -g new_filename_user_prefix='Frogs!!'
		declare -g include_fname_pfx_to_mdata_title=t
		declare -g scale='1080'
		declare -g forced_scale
		declare -g version='X.Y.Z'
		declare -g container='mp4'
		declare -g loop_last_frame=t
		declare -g loop_last_frame_s=15
		declare -g where_to_place_new_file="$TMPDIR"
		. "$mypath/../modules/nadeshiko/07c_set_file_name_and_metadata_tags.sh"

		set_file_name_and_metadata_tags

		validate_results "${new_file_name##*/}"  \
		                    "Frogs\!\!\ …and\ a\ side\ of\ fri-i-ies\ 0:01\.23–7:08:09\.32\ \[1080p]\[AS]\[LFL-15]\.mp4"  \
		                 "${outp_mdata_title:-}"  \
		                    "Frogs\!\!\ Hambur—\ Hambur—…"  \
		                 "${outp_mdata_description:-}"  \
		                    "…and\ a\ side\ of\ fri-i-ies"
	)

	echo '=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+'
	info "Press any key to continue ${__g}>${__s} "
	read -n1
	return 0
}


 # Test skeleton
#  (The test № 99 is never shown in the list and cannot be executed)
#
test_99_desc='What this test is made for'  # Will be shown in the main menu.
test_99() {
	declare -gn source_set='name_of_a_source_set (see above)'
	#  Copies exmaple.nadeshiko.rc.sh in place of RC file
	#  Checks, that source video is present.
	#  Shows a message “* Running test 99”
	prepare_test

	#  Alternate where the $new_file should be placed, if needed.
	#  By default it will be placed in $tests_dir, which is set to
	#  “$MYDIR/TESTS/<current_nadeshiko_version>/”
	where_to_place_new_file="$HOME"

	#  Change default settings in the RC file here, if needed.
	#  The parameter will be uncommented, if needed.
	change_in_rc 'parameter name' 'new value'

	#  This line calls Nadeshiko. Parameters, that tell which source video
	#  to use, Time1, Time2 and where to place the file are already there.
	encode 'tiny'

	#  In case you’d want to peruse a source video another time, but with ano-
	#  ther Time1 and Time2, use “encode-notime”. This is the same command,
	#  except it won’t put on the line with the command those Time1 and Time2,
	#  which come from “$source_set” – you can specify yours as additional
	#  parameters.
	#encode-notime 0 1

	#  Get the name of the last encoded file and check if it exists.
	#  It will return >0, if there’d be no file! Use “||:” to avoid quitting,
	#  if bad exit is intended.
	post_test

	#  Play the file, if things cannot be done automatically.
	play_encoded_files

	#  Do autiomated checks. The format is $fileNN_parameter_name,
	#  see lib/gather_file_info for the list of available parameters.
	[ "$file99_vcodec" != VP9 ]  \
		&& warn "Video codec is $file99_vcodec, should be VP9!"

	#  Do other stuff with the $new_file, analyze, read logs etc.
	cp "$where_to_place_new_file/$new_file" "$tests_dir"

	#  This shows the tester, what he should be looking for.
	#  Would be handy to show it before the test, too…
	conditions="Playback, 720p, audio"
	ask_if_ok "$conditions"
	return 0
}



pick_a_test() {
	local  all_tests
	local  desc
	local  result

	echo -e "\n${__bri}Nadeshiko test suite${__s} (Updated for v2.28)\n"

	cat <<-EOF
	   You can pass the number of a test to launch a specific one,
	   without the notice and the menu showing up:
	      $ ./${0##*/} 5
	EOF

	run_func="${1:-}"
	[[ "$run_func" =~ ^[1-9]$ ]] && run_func="0$run_func"
	if	[[ "$run_func" =~ ^0?$INT$ ]]  \
		&& (( 10#$run_func > 0 && 10#$run_func < 99 )) \
		&& [ "$(type -t "test_$run_func")" = 'function' ]
	then
		test_$run_func || err 'Error during the test execution.'
		return 0
	fi


	Xdialog --title "Nadeshiko test suite" \
	        --ok-label "OK" \
	        --buttons-style default \
	        --msgbox "Please wear headphones." \
	        324x116  \
		|| true
	until [ "${CHOSEN:-}" = Quit ]; do
		all_tests=(  $(compgen -A function | grep -E '^test_[0-9]+$')  )

		#  Unset the test-skeleton №99
		unset  all_tests[-1]

		for ((i=0; i<${#all_tests[@]}; i++)); do
			unset -n desc result
			declare -n desc="${all_tests[i]}_desc"
			declare -p ${all_tests[i]}_result &>/dev/null && {
				declare -n result=${all_tests[i]}_result
				case "$result" in
					OK)  result=" ${__bri}– OK –${__s}";;
					Fail)  result=" ${__bri}${__r}– Fail –${__s}";;
				esac
			}
			all_tests[$i]="${all_tests[i]#test_}${result:-}  ${desc:-}"
		done

		menu-list "Pick a test" "${all_tests[@]}" "Quit"
		if [ "$CHOSEN" = Quit ]; then
			break

		elif [[ "$CHOSEN" =~ ^([0-9]+)\ .*$ ]]; then
			test_${BASH_REMATCH[1]} || err 'Error during the test execution.'

		else
			err "An unknown error happened."
		fi
	done

	return 0
}


pick_a_test "${1:-}"

exit 0
