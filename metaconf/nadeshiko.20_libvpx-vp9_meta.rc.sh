#  nadeshiko.20_libvpx-vp9_meta.rc.sh



 # Formats, that this codec is known as, e.g. “AVC” for libx264.
#  Knowing the format of the source is essential to estimate what quality
#    does the source video bit rate provide and whether certain bit rate
#    bumps would make sense, when Nadeshiko will re-encode the video.
#  Type: array.
#  Item example: <the value of the “Format” field for video stream,
#                 as reported by mediainfo>
#
declare -ga  \
vcodec_name_as_formats_libvpx_vp9=(
	#  mediainfo                            #  ffprobe
	'VP9'                                   'vp9'
)


 # Working combinations of containers and audio/video codecs.
#  One string represents a working combination. When $container is set
#    to “auto” in the user’s config or in nadeshiko.10_main.rc.sh, the first
#    item, that has $ffmpeg_acodec,  would be picked.
#  Type: regular array.
#  Item example: 'container  audio_codec'  (as named in ffmpeg,
#                                           the order is free)
#
declare -ga  \
libvpx_vp9_muxing_sets=(
	'webm libopus'
	'webm libvorbis'
	#  No mkv, because browsers download it instead of playing.
)

declare -ga  \
libvpx_vp9_pix_fmts=(
	 yuv420p        yuv422p        yuv440p        yuv444p        gbrp
	yuva420p        yuv422p10le    yuv440p10le    yuv444p10le    gbrp10le
	 yuv420p10le    yuv422p12le    yuv440p12le    yuv444p12le    gbrp12le
	 yuv420p12le
)

 # Validates the option value, if the variable is set.
#
RCFILE_CHECKVALUE_VARS+=(
	[libvpx_vp9_minimal_bitrate_pct]='int_in_range_with_unit_or_without_it  0  100  %'
	[libvpx_vp9_pix_fmt]="^($(IFS='|'; echo "${libvpx_vp9_pix_fmts[*]}"))$"

	# 2⁶ = 64 columns × 128 px per column = 8192 frame width.
	[libvpx_vp9_tile_columns]='int_in_range 0 6'

	[libvpx_vp9_threads]="int_in_range  0 999"
	[libvpx_vp9_adaptive_tile_columns]='bool'
	[libvpx_vp9_frame_parallel]='^(0|1)$'

	[libvpx_vp9_pass1_deadline]='^(best|good|realtime)$'
	[libvpx_vp9_pass2_deadline]='^(best|good|realtime)$'
	#  -cpu-used aka -speed technically has limits from −9 to 9, but all exam-
	#  ples in official docs and on FFmpeg wiki have it within 0–4.
	[libvpx_vp9_pass1_cpu_used]='int_in_range 0 4'
	[libvpx_vp9_pass2_cpu_used]='int_in_range 0 4'

	[libvpx_vp9_auto_alt_ref]='int_in_range  0  6'
	[libvpx_vp9_allow_autoaltref6_only_for_videos_longer_than_sec]='int_in_range 0 99999'
	[libvpx_vp9_lag_in_frames]='int_in_range  0  25'
	[libvpx_vp9_arnr_maxframes]='int_in_range  0  15'
	[libvpx_vp9_arnr_strength]='int_in_range  0  6'
	[libvpx_vp9_arnr_type]='^(backward|forward|centered)$'

	#  As golden frames in VP9 are not necessarily key frames, the video /may/
	#  (but not necessarily should) have no keyframes at all. Or have them at
	#  really large distances. The default is 240, see defconf.
	[libvpx_vp9_kf_max_dist]='int_in_range  0 99999'
	[libvpx_vp9_minsection_pct]='int_in_range_with_unit_or_without_it  0  1000  %'
	[libvpx_vp9_maxsection_pct]='int_in_range_with_unit_or_without_it  0  1000  %'
	[libvpx_vp9_overshoot_pct]='int_in_range_with_unit_or_without_it  0  1000  %'
	[libvpx_vp9_undershoot_pct]='int_in_range_with_unit_or_without_it  0  1000  %'
	[libvpx_vp9_bias_pct]='int_in_range_with_unit_or_without_it  0  100  %'
	[libvpx_vp9_aq_mode]='int_in_range  0  4'
	[libvpx_vp9_tune_content]='int_in_range 0 2'
	[libvpx_vp9_level]='^(3(\.(0|1)|)|4(\.(0|1)|)|5(\.(0|1|2)|)|6(\.(0|1|2)|))$'
)

 # Strips the units, leaving only the value.
#  Units may accompany numeric values in the config files – for clarity.
#
RCFILE_REPLACEVALUE_VARS+=(
	[libvpx_vp9_minimal_bitrate_pct]='\1'
	[libvpx_vp9_minsection_pct]='\1'
	[libvpx_vp9_maxsection_pct]='\1'
	[libvpx_vp9_overshoot_pct]='\1'
	[libvpx_vp9_undershoot_pct]='\1'
	[libvpx_vp9_bias_pct]='\1'
)