#  nadeshiko.10_subtitles_meta.rc.sh



 # The list of subtitle formats, that can be hardsubbed.
#  Used in the stream check at initial stage.
#
declare -ga  \
known_sub_codecs=(
	ass
	  ssa
	srt
	  subrip
	  webvtt
	  vtt
	mov_text
	#
	#  When dvd_subtitle is packed as SPU (a subpicture unit), it may be
	#  reported as RLE in mediainfo. It’s still the same VobSub, but encoded
	#  differently and packed into the video stream as an MPEG program. That
	#  is, its packets are mixed in to the video packets, so it’s basically
	#  not a stream per se, but specific packets in the video stream. ffmpeg
	#  often needs to probe the file deeper to discover all the subtitles.
	#
	dvd_subtitle
	hdmv_pgs_subtitle
)


 # Some video, such as old DVDs, may pack a 16:9 frame into a 4:3 pixel grid
#  by using a non-square *pixel ratio*. (In this case, it would be 1.185.)
#  That means that when the video will be played, the media player would
#  “stretch” the area of the pixel on the screen, making the picture itself
#  wider – to restore the look that was intended.
#
#  To re-encode the picture itself is no problem, but the filter overlaying
#  the subtitles may or may not consider the “packed” pixels, whose area
#  would be stretched on the screen. The ASS/SSA filter considers it, the
#  generic “subtitle” filter (responsible e.g. for .srt, .webvtt) does not.
#  As for the “overlay” filter, this problem typically doesn’t exist (as
#  its subtitles have been already overlaid by the creators, just put as a
#  separate stream in the container).
#
#  So if the video has a pixel aspect ratio ≠ 1.0  AND  the subtitle codec
#  to be used is from the list below,  THEN  Nadeshiko will apply a rule to
#  re-stretch the video during encoding, so as to it would have square pixels,
#  i.e. use square pixels, without compressing anything. THEN, when the “sub-
#  title” filter would be applied, so that the subtitles wouldn’t look con-
#  densed.
#
declare -ga  \
subtitle_types_to_restretch=(
	subrip
	srt
	webvtt
	vtt
	mov_text
)