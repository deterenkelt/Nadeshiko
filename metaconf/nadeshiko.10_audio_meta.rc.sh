#  nadeshiko.10_audio_meta.rc.sh



 # Helps to determine, when it makes sense to use extra bitrate for audio.
#
#  * DTS-HD MA is reported in ffprobe as such, but in “mediainfo -f
#    --output=XML” (which performs the check on lossless format) it is
#    reported as “DTS” format with “MA” profile.
#
#  Formats are compared in the order listed here, so the more common
#  formats better be first.
#
declare -ga  \
known_audio_lossless_formats=(

	#  Mediainfo                  #  ffprobe
	FLAC                          flac
	PCM                           pcm_bluray
	TrueHD                        truehd
	DTS:MA
	'MLP FBA'
)


 # The character which, if found in the format string, would indicate
#  that a format with a specific profile is required (the latter should
#  follow after the delimiter).
#
declare -g  \
known_audio_lossless_formats_profile_delimiter=':'