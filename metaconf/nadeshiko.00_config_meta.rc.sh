#  nadeshiko.00_config_meta.rc.sh
#

 # This initial piece of configuration preemptively declares the types for
#  regular and associative arrays. This allows the config files to be simpler
#  for the end users, who don’t need to type “declare”, and thus will not
#  confuse ‘declare -a’ with ‘declare -A’.
#
declare -gA  ffmpeg_missing
declare -ga  preserve_colour_spaces
declare -gA  bitres_profile_360p
declare -gA  bitres_profile_480p
declare -gA  bitres_profile_576p
declare -gA  bitres_profile_720p
declare -gA  bitres_profile_1080p
declare -gA  bitres_profile_1440p
declare -gA  bitres_profile_2160p
declare -gA  ffmpeg_subtitle_fallback_style


 # Meta configuration files also set predefines for the libraries and modules.
#  What has to be set for Bahelite is set in the main meta configuration
#  file, i.e. in this file. Other specific things, especially module-specific
#  ones, are placed each in their own meta configuration file.
#

 # Validates the option value, if the variable is set.
#
#  One of the handy additions of Bahelite’s rcfile module are the pseudo-
#  boolean variables, that may contain virtually any string, that could be
#  unambiguously interpreted as positive or negative: yes/no, on/off, true/
#  false, 1/0 etc. Such variable undergoes a test and then either remains
#  set (if it had a “positive” value) or gets unset (when was “negative”).
#  With this, the very existence of the variable becomes a boolean flag,
#  which is easy to check later with [ -v varname ].
#
RCFILE_CHECKVALUE_VARS+=(
	[desktop_notifications]='bool'
	[max_size_normal]='int_in_range_with_unit 1 99999 [kMG]'
	[max_size_small]='int_in_range_with_unit 1 99999 [kMG]'
	[max_size_tiny]='int_in_range_with_unit 1 99999 [kMG]'
	[max_size_unlimited]='int_in_range_with_unit 1 99999 [kMG]$'
	[max_size_default]='^(tiny|small|normal|unlimited)$'
	[kilo]='^(1000|1024)$'
	[extra_fonts_dir]='dirpath'
	[pedantic]='bool'
	[time_stat]='bool'
	[create_windows_friendly_filenames]='bool'
	[subs]='bool'
	[audio]='bool'
	[ffmpeg_progressbar]='bool'
	[scale]='^((2160|1440|1080|720|576|480|360)p|no)$'
	[video_sps_threshold]="^$FLOAT$"
	[frame_rate]="^($INT|$INT/$INT|$FLOAT)$"
	[frame_rate_fixed]='bool'
	[motion_interp_allowed]='bool'
	[crop_uses_profile_vbitrate]='bool'
	[adapt_srt_style_to_resolution]='bool'
	[do_colour_space_conversion]='bool'
	[force_colour_space_conversion]='bool'
	[tag_output_colour_space]='bool'
	[forced_colour_space_conversion_also_forces_tagging]='bool'
	[cached_attachments_lifespan_in_months]='int_in_range 1 99999'
	[webm_muxing_ovh_coef]="^$FLOAT$"
	[mp4_muxing_ovh_coef]="^$FLOAT$"
	[deinterlace]="^($PBOOL_TRUE|$PBOOL_FALSE|auto)$"
	[seekbefore]='int_in_range 1 99999'
	[seekbefore_for_HDR_NCL_s]='int_in_range 1 99999'
	[seekbefore_for_cmdline_s]='int_in_range 1 99999'
	[seekbefore_in_search_for_a_keyframe_s]='int_in_range 1 99999'
	[loop_last_frame_use_alter_method]='bool'
	[loop_last_frame_max_attempts]='int_in_range 1 125'
	[loop_last_frame_incr_reserved_frames]='^(linear|exponential)$'
	[loop_last_frame_compensatory_second]='bool'
	[include_fname_pfx_to_mdata_title]='bool'
)