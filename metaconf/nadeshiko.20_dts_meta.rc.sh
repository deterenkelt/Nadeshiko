#  nadeshiko.20_dts_meta.rc.sh



 # Formats, that this codec is known as, e.g. “Opus” for libopus, “AAC:LC”
#    for AAC (to distinguish from AAC:HE). Format profile, if specified,
#    serves to determine the variation of the encoded audio stream.
#  Knowing the format of the source is essential to estimate what quality
#    does the source video bit rate provide and whether certain bit rate
#    bumps would make sense, when Nadeshiko will re-encode the video.
#  Type: array.
#  Item example: "<Format>[<delimiter><Format profile>]".
#    The values for Format and Format profile are those reported by mediainfo.
#    The delimiter, if used, must be specified via the variable
#    acodec_delimiter_for_name_as_formats_AUDIOCODECNAME.
#
declare -ga  \
acodec_name_as_formats_dts=(
	#  mediainfo                      # ffprobe
	'DTS'                             'dts'  'dts:DTS'
	'DTS:96/24'                       'dts:DTS 96/24'
	'DTS:MA'                          'dts:DTS-HD MA'
	'DTS:DTS-HD Master Audio'
)

declare -g  \
acodec_delimiter_for_name_as_formats_dts=':'


 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The commonly
#  recommended bitrate for general-purpose use.
#
#  The DTS “good at” value is kind of special, since any unknown codec is
#  measured up to its quality. For this reason the value “503” (kbps) may be
#  found as a constant in the code. Most often DTS would be found in its loss-
#  less variant DTS:MA, so the “good at” value is specified only for the rare
#  cases, when DTS is lossy or when format profile (the “MA” part) would be
#  lost due to improper tagging. For DTS is meant to exist as “one standard
#  bitrate per X channels”, there’s no specific “nadeshiko.90_dts.rc.sh” file,
#  and the “good at” value (which is still required for the program’s algo-
#  rithm to work correctly) is kept here, in the .meta.rc.sh file. One last
#  reminder, that Nadeshiko uses this varible only when it enoucnters a lossy
#  DTS track (lossless formats are considered top quality by definition and
#  thus there’s no need in bitrate comparison, in which this variable is used).
#
dts_is_fairly_good_at='503'