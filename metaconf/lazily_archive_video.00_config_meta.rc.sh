#  lazily_archive_video.00_config_meta.rc.sh
#

RCFILE_CHECKVALUE_VARS+=(
	[default_speed]='^(1.[0-9][05]?|2.00?)$'
	[default_scale]='^(360|480|576|720|1080)$'
	[default_quality]='int_in_range 15 35'
	[default_audio_bitrate]='^((48|64|96|112|128|160|192|224|256)k|copy|audio\ disabled)$'
	[default_pix_fmt]='^yuv(420p|444p|420p10le|444p10le)$'
)