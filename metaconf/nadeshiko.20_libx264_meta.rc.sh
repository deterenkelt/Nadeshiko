#  nadeshiko.20_libx264_meta.rc.sh



 # Formats, that this codec is known as, e.g. “AVC” for libx264.
#  Knowing the format of the source is essential to estimate what quality
#    does the source video bit rate provide and whether certain bit rate
#    bumps would make sense, when Nadeshiko will re-encode the video.
#  Type: array.
#  Item example: <the value of the “Format” field for video stream,
#                 as reported by mediainfo>
#
declare -ga  \
vcodec_name_as_formats_libx264=(
	#  mediainfo                            #  ffprobe
	'AVC'                                   'h264'
	'MPEG-4'

	#  Potentially possible:
	'H.264' 'H264'
)


 # Working combinations of containers and audio/video codecs.
#  One string represents a working combination. When $container is set
#    to “auto” in the user’s configuration or in nadeshiko.10_main.rc.sh,
#    the first item, that has $ffmpeg_acodec,  would be picked.
#  Type: regular array.
#  Item example: 'container  audio_codec'  (as named in ffmpeg,
#                                           the order is free)
#
declare -ga  \
libx264_muxing_sets=(
	'mp4 libfdk_aac'
	'mp4 aac'
	'mp4 libmp3lame'
	'mp4 eac3'
	'mp4 ac3'
	#  'mp4 libopus'  # libopus in mp4 is still experimental
	#                 # and browsers can’t play opus in mp4 anyway
	#  No mkv, because browsers download it instead of playing.
)

declare -ga  \
libx264_pix_fmts=(
	 yuv420p        yuv422p        yuv444p        nv12      gray
	yuvj420p       yuvj422p       yuvj444p        nv16      gray10le
	 yuv420p10le    yuv422p10le    yuv444p10le    nv20le
)

 # Validates the option value, if the variable is set.
#
RCFILE_CHECKVALUE_VARS+=(
	[libx264_pix_fmt]="^($(IFS='|'; echo "${libx264_pix_fmts[*]}"))$"
	[libx264_minimal_bitrate_pct]='int_in_range_with_unit_or_without_it  0  100  %'
	[libx264_preset]='^(placebo|veryslow|slower|slow|medium|ultrafast)$'
	[libx264_tune]='^(animation|film|fastdecode|zerolatency)$'
	[libx264_profile]='^(high444p|high10|high|main|baseline)$'
	[libx264_level]='^(3(\.(0|1|2)|)|4(\.(0|1|2)|)|5(\.(0|1)|))$'
	[libx264_keyint]="^(auto|$INT)$"
	[libx264_keyint_min]="^(auto|$INT)$"
	[libx264_keyint_min_dyn]='int_in_range 1 100'
	[libx264_keyint_min_sta]='int_in_range 1 100'
	[libx264_opengop]='bool'

)

 # Strips the units, leaving only the value.
#  Units may accompany numeric values in the config files – for clarity.
#
RCFILE_REPLACEVALUE_VARS+=(
	[libx264_minimal_bitrate_pct]='\1'
)