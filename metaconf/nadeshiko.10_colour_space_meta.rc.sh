#  nadeshiko.10_colour_space_meta.rc.sh



declare -gA target_colour_space_bt709
declare -gA target_colour_space_bt2020_cl

 # To understand things described here, it’s recommended to read the “Intro-
#  duction to colour spaces” on the wiki and then notes in the main configu-
#  ration file: defconf/nadeshiko.10_main.rc.sh.
#

 # The “known_cspace_*” sets are those known to ffmpeg, i.e. the detectable
#  ones. Those that are considered known to the filters, that perform colour
#  space conversion, are listed below as “known_cspace_*_for_filter_FILTERNAME”.
#

 # For the sake of unity, throughout the Nadeshiko configuration the default
#  names for matrix coefficients, primaries and transfer characteristics used
#  are those names native to FFmpeg (its -colorspace, -color_primaries and
#  -color_trc options, as these sets are most complete. The values are taken
#  from “man ffmpeg-codecs”).
#
declare -ga  \
known_cspace_matrix_coefs=(
	rgb                #  RGB. Not sure if FFmpeg devs mean the full RGB
	                   #    colour range (as the one you get from e.g.
	                   #    rendering a scene in Blender), or the limited
	                   #    sRGB space.
	bt709              #  BT.709, the first commonly used standard for
	                   #    distributing video content for computers.
	fcc                #  FCC
	bt470bg            #  BT.470 B/G (originally a standard for PAL and SECAM
	                   #    television, later adopted for computers as BT.601-x
	                   #    625, which has evolved since then).
	smpte170m          #  SMPTE 170 M (originally a standard for for NTSC tele-
	                   #    vision, later adopted for computers as BT.601-x
	                   #    525, which again, has evolved since then).
	smpte240m          #  SMPTE 240 M (dead format, that was replaced
	                   #    with BT.709).
	ycocg              #  YCOCG
	bt2020nc           #  BT.2020 non-constant luminance (advised for broad-
	                   #    casting, more lossy).
	bt2020_ncl         #  ————”————
	bt2020c            #  BT.2020 constant luminance (advised for compression
	                   #    efficiency, loses less luma information).
	bt2020_cl          #  ————”————
	smpte2085          #  SMPTE ST 2085, a 2015 addition to the SMPTE ST 2084,
	                   #    aims at HDR signals in non-broadcast content.
	chroma-derived-nc  #  Chroma-derived NCL
	chroma-derived-c   #  Chroma-derived CL
	ictcp              #  ICtCp, a BT.2100 specification for “color representa-
	                   #    tion designed for high dynamic range and wide color
	                   #    gamut imagery and is intended as a replacement for
	                   #    Non-Constant Luminance Y’C’bC’r with HDR and WCG
	                   #    signals”.
)


declare -ga  \
known_cspace_tone_curves=(
	bt709           #  BT.709
	gamma22         #  BT.470 M
	gamma28         #  BT.470 B/G
	smpte170m       #  SMPTE 170 M
	smpte240m       #  SMPTE 240 M
	linear          #  Linear
	log             #  Log
	log100          #  ————”––––
	log_sqrt        #  Log square root
	log316          #  ————”————
	iec61966_2_4    #  IEC 61 966-2-4, part of Sony’s xvYCC colour space.
	iec61966-2-4    #  ————”————
	bt1361          #  BT.1361
	bt1361e         #  ————”————
	iec61966_2_1    #  IEC 61 966-2-1, aka more scrupulously specified sRGB.
	iec61966-2-1    #  ————”————
	bt2020_10       #  BT.2020 – 10 bit
	bt2020_10bit    #  ————”————
	bt2020_12       #  BT.2020 – 12 bit
	bt2020_12bit    #  ————”————
	smpte2084       #  SMPTE ST 2084, aka Perceptive quantisation (PQ) curve
	                #    used with BT.2020 matrix to provide HDR.
	smpte428        #  SMPTE ST 428-1
	smpte428_1      #  ————”————
	arib-std-b67    #  ARIB STD-B67, aka Hybrid log-gamma curve, is a curve
	                #    proposed by BBC and NHK with an intention to match
	                #    SDR curve where possible (at the bottom, where is
	                #    black), while providing higher dynamic range in high-
	                #    lights. Its range is narrower than PQ’s.
)


declare -ga  \
known_cspace_primaries=(
	bt709         #  BT.709
	bt470m        #  BT.470 M
	bt470bg       #  BT.470 BG
	smpte170m     #  SMPTE 170 M
	smpte240m     #  SMPTE 240 M
	film          #  Film
	bt2020        #  BT.2020
	smpte428      #  SMPTE ST 428-1, Digital Cinema standard
	smpte428_1    #  ————”————
	smpte431      #  SMPTE ST 431-2, part of DCI-P3 colour space
	smpte432      #  SMPTE ST 432-1 (the D65 one), part of DCI-P3 colour space.
	jedec-p22     #  JEDEC P22 phosphors aka EBU Tech. 3213-E
)


declare -ga  \
known_cspace_colour_ranges=(
	tv  #  Limited
	pc  #  Full
)


#  Nothing else is supported as of now.
declare -gr  \
colour_space_filter='zscale'


 # Three arrays below specify matrix coefficients, primaries and transfer
#  characteristics, with which zscale filter can work. Values are given as
#  ffmpeg names of the corresponding properties translated to their names
#  in the zscale filter. The values below are valid for ffmpeg 4.4 and zimg
#  3.0.2.
#
#  For the list of properties refer to the output of
#     $ ffmpeg -h filter=zscale
#
declare -gA  \
known_cspace_matrix_coefs_for_filter_zscale=(
	[input]='input'          #  =“unspecified”
	[bt709]='bt709'
	[bt470bg]='bt470bg'      #  Zimg colorspace.h for v3.0.3 has only a single
	[smpte170m]='smpte170m'  #    REC_601 constant, so not sure, how this
	                         #    should work.
	[smpte240m]='7'          #  The “smpte2400m” specified in the output of
	                         #      $ ffmpeg -h filter=zscale
	                         #    seems unreliable, so an integer here.
	[bt2020_ncl]='2020_ncl'
	[bt2020nc]='2020_ncl'
	[bt2020_cl]='2020_cl'
	[bt2020c]='2020_cl'
	[fcc]='fcc'
	[ycocg]='ycgco'          #  Sic!
	[chroma-derived-nc]='chroma-derived-nc'
	[chroma-derived-c]='chroma-derived-c'
	[ictcp]='ictcp'
	[rgb]='gbr'              #  Zimg colorspace.h mentions RGB among known
	                         #  matrix coefficients, and ITU sometimes refers
	                         #  to RGB as GBR, which makes it almost certain,
	                         #  that Zimg gbr means (s?)RGB here. In the case
	                         #  if “gbr” would mean something else, Zimg de-
	                         #  faults to RGB, when matrix coefficients are
	                         #  unspecified
)


declare -gA  \
known_cspace_primaries_for_filter_zscale=(
	[input]='input'          #  =“unspecified”
	[bt709]='bt709'
	[smpte170m]='smpte170m'
	[smpte240m]='smpte240m'
	[bt2020]='2020'
	[bt470m]='bt470m'
	[bt470bg]='bt470bg'
	[film]='film'
	[smpte428]='smpte428'
	[smpte428_1]='smpte428'
	[smpte431]='smpte431'
	[smpte432]='smpte432'
	[jedec-p22]='jedec-p22'
)


declare -gA  \
known_cspace_tone_curves_for_filter_zscale=(
	[input]='input'            #  =“unspecified”
	[bt709]='bt709'
	[linear]='linear'
	[bt2020_10]='bt2020-10'
	[bt2020_10bit]='bt2020-12'
	[gamma22]='bt470m'
	[gamma28]='bt470bg'
	[smpte170m]='smpte170m'    #  Zscale alias “601” points here, too.
	[log]='log100'
	[log100]='log100'
	[log_sqrt]='log316'
	[log316]='log316'
	[smpte2084]='smpte2084'
	[iec61966_2_4]='iec61966-2-4'
	[iec61966-2-4]='iec61966-2-4'
	[iec61966_2_1]='iec61966-2-1'
	[iec61966-2-1]='iec61966-2-1'
	[arib-std-b67]='arib-std-b67'
)


declare -ga  \
known_cspace_colour_ranges_for_filter_zscale=(
	tv     #  Limited
	pc     #  Full
	input  #  =“unspecified”. Rely on whichever of the two ranges zscale
	       #    determines for the input.
)


 # A subset of the “known_cspace_tone_curves” array above, that contains
#  only the HDR ones (the rest are considered SDR, with a range of up to
#  100 cd/m²).
#
#  Each item of this array represents a *group* of standard curves, whose
#  dynamic range is identical or similar to the point of interchangeability.
#  The format for an item is
#      [up_to_CANDELAS_cdpsqm]='CURVE-A CURVE-B … CURVE_N'
#  where CANDELAS is the maximum number of candelas per square metre, which
#  defines the dynamic range of the curve, and each CURVE_* is an item of
#  “known_cspace_tone_curves”.
#
#  The difference between the dynamic range is necessary to determine, whether
#  tone mapping is required or zscale is enough.
#
declare -gA  \
known_cspace_hdr_tone_curves=(
	# <2000 cd/m²
	[up_to_2000_cdpsqm]='arib-std-b67'

	# <10000 cd/m² (or should peak luminance of 1000 cd/m² be considered?)
	[up_to_4000_cdpsqm]='smpte2084'
)


 # The list of matrix coefficients, which indicate that the video has non-
#  constant luminance (only found in HDR content), which would require “seek-
#  before” flag to be on in order to catch all contrast changes. (“Non-con-
#  stant” means that the contrast setting flags would be spread within the
#  video stream, and you can’t just seek to a PTS and get the correct contrast/
#  luminance, you need to read /enough/ frames before to catch that one set-
#  ting, which affects the video at the point that you direct seeking to.
#  To be sure, we read from PTS 0:00.000.)
#
declare -g  \
HDR_NCL_matrix_coefs=(
	bt2020nc
	bt2020_ncl
	chroma-derived-nc
	smpte2085             #  Maybe these two formats use constant luminance,
	ictcp                 #  in fact. We just don’t know^W^W are too lazy
	                      #  to check.
)


RCFILE_CHECKVALUE_VARS+=(
	[apply_deflicker_on_HDR_NCL]='bool'
	[ffmpeg_filter_zscale_npl]='int_in_range 0 10000'
	[ffmpeg_filter_tonemap_alg]='^(none|linear|gamma|reinhard|hable|mobius)$'
	[ffmpeg_filter_tonemap_param_value]="^$FLOAT$"
	#  NB “-ize” – it’s from the filter name. As for its companion,
	#  “…normalize_smoothing”, because it may need conversion, it’s
	#  checked separately.
	[ffmpeg_filter_use_normalize]='bool'

)