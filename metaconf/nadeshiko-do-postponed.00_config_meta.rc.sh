#  nadeshiko-do-postponed.00_config_meta.rc.sh
#
#  This initial piece of configuration predefines types for regular and
#  associative arrays. This allows to keep the actual definitions minimal
#  for the end users – who might forget to copy the definitions to their
#  config files or type -a instead of -A.



 # Pseudo-boolean variables, that the main script will check for yes/no
#  on/off, true/false, 1/0 and either leaves or unsets – making their
#  existence into a boolean value.
#
RCFILE_CHECKVALUE_VARS=(
	[processors]='int_in_range 1 9999'
	[niceness_level]='^(-?([0-9]|[1-9][0-9])|)$'
	[nadeshiko_desktop_notifications]='^(none|error|all)$'
	[icon_set]='^(nade(1|2|3|4|5)|random)$'
	[show_control_keys_prompt]='bool'
)