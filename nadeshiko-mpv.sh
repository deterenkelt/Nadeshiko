#! /usr/bin/env bash

#  nadeshiko-mpv.sh
#  Wrapper for Nadeshiko to provide IPC with mpv
#  and a GUI for selecting encoding profile (codec and size).
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko.sh
#

#  mpv_crop_script.lua by TheAMM, 2018
#  Licence: GPL v3, see the full text in “lib/mpv_crop_script/LICENSE”
#


set -feEuT
shopt -s  inherit_errexit  extglob
MY_SUITE_NAME='nadeshiko'
BAHELITE_LOAD_MODULES=(
	libdir
	modulesdir
	resdir
	logging:use_cachedir
	rcfile:use_metaconf,use_defconf
	messages_to_desktop
	misc
)
mypath=$(dirname "$(realpath --logical "$0")")
case "$mypath" in
	'/usr/bin'|'/usr/local/bin')
		source "${mypath%/bin}/lib/nadeshiko/bahelite/bahelite.sh";;
	*)
		source "$mypath/lib/bahelite/bahelite.sh";;
esac

. "$LIBDIR/mpv_ipc.sh"
. "$LIBDIR/gather_source_info.sh"
. "$LIBDIR/time_functions.sh"
. "$LIBDIR/xml_and_python_functions.sh"

noglob_off
for module in "$MODULESDIR"/nadeshiko-mpv/*.sh ; do
	. "$module" || err "Couldn’t source module $module."
done
noglob_rewind

place_examplerc 'nadeshiko-mpv.10_main.rc.sh'

declare -r version="2.9.0"

declare -r datadir="$CACHEDIR/nadeshiko-mpv_data"
declare -r postponed_commands_dir="$CACHEDIR/postponed_commands_dir"

single_process_check



on_error() {
	local func
	local pyfile="$TMPDIR/dialogues_gtk.py"
	local gladefile="$TMPDIR/dialogues_gtk.glade"

	#  Wipe the data directory, so that after a stop caused by an error
	#  we wouldn’t read the old data, but tried to create new ones.
	#  The data per se probably won’t break the script, but those data
	#  could be just stale.
	touch "$datadir/wipe_me"

	for func in ${FUNCNAME[@]}; do
		#  If we couldn’t prepare option list, because we hit an error
		#  with Nadeshiko in dryrun mode…
		[ "$func" = show_dialogue_choose_preset ] && {
			[ -r "$pyfile" ]  \
				&& cp "$pyfile" "$LOGDIR/$LOGNAME.dialogues_gtk.py"
			[ -r "$gladefile" ]  \
				&& cp "$gladefile" "$LOGDIR/$LOGNAME.dialogues_gtk.glade"
		}
	done

	#  Cropping module’s own on_error().
	[ "$(type -t on_croptool_error)" = 'function' ] && on_croptool_error

	return 0
}


on_exit() {
	#  If mpv still runs, clear any OSD message, that might be left hanging.
	[ -v WIPE_MPV_SCREEN_ON_EXIT  -a  -e "/proc/${mpv_pid:-doesn’t exist}" ]  \
		&& send_command  show-text  ' '  '1'
	return 0
}



print_program_version $version

[ -d "$datadir" ] || mkdir "$datadir"
cd "$datadir"
if [ -e wipe_me ]; then
	noglob_off
	rm -rf ./*
	noglob_rewind
else
	# Delete files older than one hour.
	find -type f -mmin +60  -delete
fi

post_read_rcfile
REQUIRED_UTILS+=(
	python3      #  Dialogue windows.
	xmlstarlet   #  To alter XML in the GUI file.
	find         #  To find and delete possible leftover data files.
	lsof         #  To check, that there is an mpv process listening to socket.
	jq           #  To parse JSON from mpv.
	socat        #  To talk to mpv via UNIX socket.
)
check_required_utils
declare -r xml='xmlstarlet'  # for lib/xml_and_python_functions.sh
parse_args


 # Test, that all the entries from our properties array
#  can be retrieved.
#
# retrieve_properties


 # Here is a hen and egg problem.
#
#  We would like to ask user to choose the socket only once, i.e. only when
#  he sets Time1. On setting Time2 Nadeshiko-mpv should read $mpv_socket
#  variable from the $data_file. However, in order to find the corresponding
#  $data_file, we must know, which file is opened in mpv, where Nadeshiko-mpv
#  is called from. So we must first query mpv to read $filename, and then
#  by the $filename find the $data_file, which would have that $filename inside.
#    Thus trying to read $data_file before querying mpv is futile and will
#  break the process of setting Time1 and Time2.
#    To avoid querying mpv socket twice, Nadeshiko-mpv should process each
#  video fragment in one run, not in two runs, like it is now. Nadeshiko-mpv
#  should have two windows: one before predictor runs, and one after it runs.
#  The first window would have options to connect to sockets, set times (how-
#  ever many, 2, 4, 20…), turn on and off sound and subtitles, set crop area,
#  and run preview. The second window would be as it is now, unchanged.
#
check_socket
get_props  mpv-version  filename
mpv_version=${mpv_version#mpv }
data_file=$(grep -rlF "${filename@A}" |& head -n1)
if [ -e "$data_file" ]; then
	info 'Run 2'
	milinc
	(( VERBOSITY_CHANNELS[Datafile] > 2 ))  \
		&& info "Found data file “${data_file#**/}”."
	info 'Reading properties.'
	. "$data_file"
else
	info 'Run 1'
	milinc
	#
	#  Check connection and get us filename to serve as an ID for the playing
	#  file, as for getting path we’d need working-directory. Not taking
	#  path for ID to not do the job twice.
	#
	data_file=$(mktemp --tmpdir='.'  mpvfile.XXXX)
	(( VERBOSITY_CHANNELS[Datafile] > 2 ))  \
		&& info "Creating data file “${data_file#**/}”."
	info 'Setting mpv file name and socket for Run 2.'
	write_var_to_datafile  filename "$filename"
	write_var_to_datafile  mpv_socket "$mpv_socket"
fi

mildec

 # Must be here, because mpv_pid is used in functions, that send messages
#  to mpv window, when it may be closed. To avoid that, we must know
#  its PID and check if it’s still running, so if there would be
#  no window, we wouldn’t send anything.
#
info 'Retrieving mpv PID.'
milinc
export mpv_pid=$(lsof -t -c mpv -a -f -- "$mpv_socket")
[[ "$mpv_pid" =~ ^[0-9]+$ ]] || err "Couldn’t determine mpv PID."
mildec


put_time_and_collect_props
if [ -v time2 ]; then
	#  Check, that Time1 and Time2 aren’t the same and arrange them in order.
	arrange_times
	choose_crop_settings
	play_preview
	if [ -v quick_run ]; then
		[ "$quick_run_preset" ] && nadeshiko_preset=$quick_run_preset
	else
		choose_preset
	fi
	#  Prepares the encoding
	#  - if “postpone” is enabled, saves the job and calls exit
	#  - if “postpone” is disabled, runs encoder, then returns
	#    to play encoded file.
	encode

else
	info "Run again to set Time2!"
fi


exit 0