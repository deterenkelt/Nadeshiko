#  Should be sourced.

#  logging.sh
#  Selects logging directory and writes logs. Hosts output
#    channel “log”. Read the note above the function “start_logging”,
#    which explains how to return interactiveness to “read” and “less”,
#    if they won’t work.
#  Author: deterenkelt, 2018–2024
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_LOGGING_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_LOGGING_VER='1.9.6'
bahelite_load_module 'check_directory' || exit $?
BAHELITE_INTERNALLY_REQUIRED_UTILS+=(
#	date   #  (coreutils) to add date to $LOGPATH file name and to the log itself.
#	ls     #  (coreutils)
#	tee    #  (coreutils)
	xargs  #  (findutils)
	pkill  #  (procps) to find and kill the logging tee nicely, so it wouldn’t
	       #           hang.
)
# ^ Commented binaries are dependencies pulled from already requried packages.


 # The output verbosity channel
#
vchan setup  name=log  \
             level=2    \
             meta=M      \
             hint=output  \
             min_level=0  \
             max_level=2  \
             level_is_locked='L'

show_help_on_verbosity_channel_log() {
	cat <<-EOF
	log (output channel)
	    Controls output from stdout and stderr to log files.

	Levels:
	    0 – disabled (no logs created).
	    1 – only stderr is logged.
	    2 – stderr and stdout are logged (the default).

	Environment variables:
	    LOGDIR – when set, defines the absolute path to a directory, which
	       should be used to contain log files.
	    LOGPATH – directly specifies the file, where the output should be
	       going to. May be an existing file, and its original contents will
	       be added to. May not exist, in which case it will be created.
	When the log level is set to 0, the variables LOGDIR and LOGPATH are unset,
	and no logs are written.
	EOF
	return 0
}


 # Self-report channel.
#
vchan setup  name=Logging   \
             level=2         \
             meta=M           \
             hint=self-report  \
             min_level=2       \
             max_level=4


show_help_on_verbosity_channel_Logging() {
	cat <<-EOF
	Logging (self-report channel)

	    Describes the process of choosing the logging directory, writing a log
	    file, duplicating stdout and stderr (to let console have a copy).

	Levels:
	    2 – no output (the default);
	    3 – basic information on LOGDIR and LOGFILE;
	    4 – on this level the output will have additional information on the
	        duplication of stdout and stderr and handling “tee” processes,
	        which are used for this.

	EOF
	return 0
}


__show_usage_logging_module() {
	cat <<-EOF
	Bahelite module “logging” arguments:

	use_cachedir
	    Load the module to handle XDG directory for cache files and maintain
	    logs there, in the subdirectory ~/.cache/$MYNAME_NOEXT/logs.

	max_runs=${__bri}1…99999${__s}
	    How many log files to keep in the log directory. Files are preserved
	    per-run, thus the option’s name – “max_runs”. By default, logs from
	    the last 5 runs are kept.

	All option names are also accepted with hyphens in place of underscores.
	EOF
	return 0
}


declare -gx BAHELITE_LOGGING_MAX_RUNS=5

for opt in "$@"; do
	if  [[ "$opt" =~ ^max[_-]runs\= ]]; then
		[[ "$opt" =~ ^max[_-]runs\=([1-9]|[1-9][0-9]{1,4})$ ]]  \
			&& BAHELITE_LOGGING_MAX_RUNS="${BASH_REMATCH[1]}"  \
			|| err "Logging: module option “max-runs” accepts a number in range 1…99999."

	 # On the usage of cachedir (~/.cache/<main script name>/logs) for logs
	#
	#  This module will use ~/.cache/… directory as long as no environment
	#  variable overrides LOGDIR or LOGPATH and the cachedir is prepared.
	#  Now for the cases, when it’s prepared (actually, almost always):
	#    - when you don’t specify BAHELITE_LOAD_MODULES (i.e. when bahelite.sh
	#      loads every module it can find – the full bundle includes a module
	#      for cachedir);
	#    - when you load a specific bunch of modules, and mention “cachedir”
	#      among others in BAHELITE_LOAD_MODULES array;
	#    - when you use BAHELITE_LOAD_MODULES and omit “cachedir” in the
	#      items, but use a parameter for the logging module instead, like
	#          BAHELITE_LOAD_MODULES=(
	#              "logging:use_cachedir"
	#          )
	#
	elif [[ "$opt" =~ ^use[_-]cachedir$ ]]; then
		bahelite_load_module 'cachedir' || return $?

	elif [ "$opt" = help ]; then
		__show_usage_logging_module
		exit 0

	else
		__show_usage_logging_module
		exit 4
	fi
done
unset opt



 # Provides logging for the main script
#
#  Determines LOGDIR and creates file LOGNAME in LOGPATH, then writes initial
#  records to it. LOGPATH and LOGDIR might be specified through environment,
#  see below. Logging obeys verbosity leves, see more about them
#  in bahelite_messages.sh.
#
#  Logging will replace common file descriptors 1 and 2 with pipes, which
#  will duplicate the output for console and log file as necessary. This in-
#  troduces a slight inconvenience, namely that some interactive programs
#  like “read” or “less” may not see the output as an interactive terminal
#  and would thus refuse to work as expected. There are two solutions to this:
#
#    1. One is to use “/dev/tty” for the input/output, that is
#          read -n1 </dev/tty
#       or
#          cat <<-EOF | less >/dev/tty
#       /dev/tty is a permanently available link to the process’s real termi-
#       nal, a link provided by the kernel.
#
#    2. The other way is meant for the cases when “/dev/tty” doesn’t help,
#       and for the “read” builtin it’s handled by Bahelite’s own wrapper.
#       So, you shouldn’t have problems with “read”, but if you encounter
#       a problem like one described below, you may copy the solution, until
#       the causes for this bug could be dealt with.
#
#       In those cases it seems that previsouly used subshell with input
#       redirection, i.e. “< <(…)”, makes read fail within the same function
#       scope, the entire process gets suspended like with Ctrl+Z, and a mes-
#       sage like “[1] Stopped” is printed to console before it returns to the
#       prompt.
#
#       It was noticed, that in these cases a call to “info” (actually, any
#       function that ends up calling __msg(), not just “info”) placed before
#       “read” makes “read” work properly (and /dev/tty isn’t needed). Looking
#       into what exactly makes “read” work, it was found, that the line, which
#       fixes the behaviour of the “read” builtin uses a herestring in a sub-
#       shell. You may thus use the following snippet before “read”, until
#       the root of the problem would be found and hacks like these won’t be
#       needed any more:
#          (<<<'')
#          read …
#
#  Enabling extglob temporarily or the “source” command, which sources this
#  file, will catch an error while trying to load the function code.
#
extglob_on
start_logging() {
	#
	#  In this order!
	#  Do not do “declare -gx BAHELITE_LOGGING_USES_TEE” if start_logging
	#  should quit! Exporting it will make the variable visible for [ -v … ]
	#  and this will cause a segmentation fault in stop_logging(), because
	#  “kill” command will attempt to kill something, that it shouldn’t.
	#
	declare -gx LOGPATH
	declare -gx LOGDIR

	local v_level=${VERBOSITY_CHANNELS[Logging]}

	(( v_level >= 3 )) && {
		info 'Starting log file.'
		milinc
	}

	(( ${VERBOSITY_CHANNELS[log]} == 0 )) && {
		unset LOGDIR LOGPATH
		(( v_level >= 3 ))  \
			&& warn 'Log disabled. LOGDIR and LOGPATH are unset!'
		return 0
	}

	 # In this order!
	#  (The variables below must be defined and set only AFTER the condition
	#   above, i.e. only after the code going to “return 0” route. They must
	#   not be present in any way, if the logging is disabled.)
	#
	declare -gx LOGNAME
	declare -gx BAHELITE_LOGGING_STARTED
	#  These are used only in the sameshell context.
	declare -g  BAHELITE_LOGGING_USES_TEE
	declare -g  BAHELITE_LOGGING_TEE_CMD

	local  arg
	local  overwrite_logpath=t

	#  Priority of directories to be used as LOGDIR:
	#
	if [ -v LOGPATH ]; then
		#
		#  1. Path indicated by LOGPATH, if it’s set in the the environemnt.
		#     Essentially LOGDIR=${LOGPATH%/*}
		#
		touch "$LOGPATH" && [ -w "$LOGPATH" ] || {
			redmsg "Cannot write to LOGPATH specified in the environment:
			        $LOGPATH"
			err "LOGPATH is not writeable."
		}
		LOGDIR=${LOGPATH%/*}
		LOGNAME=${LOGPATH##*/}
		__check_directory "$LOGDIR"  'Logging'
		unset overwrite_logpath

	elif [ -v LOGDIR ]; then
		#
		#  2. LOGDIR set in the environment
		#
		#  Checks permissions on an existing directory and tries to create
		#  it, if it doesn’t exist. Throws an error, if the result is not
		#  a writeable directory.
		#
		__check_directory "$LOGDIR"  'Logging'

	elif [ -v CACHEDIR ]; then
		#
		#  3. CACHEDIR/logs
		#     Requires module option “use_cachedir” to be added.
		#
		LOGDIR="$CACHEDIR/logs"
		__check_directory "$LOGDIR"  'Logging'

	elif [ -w "$MYDIR" ]; then
		#
		#  4. MYDIR/logs
		#     A variant for portable scripts, that should be contained within
		#     their own folder. Basically what LOGDIR ends up being by default,
		#     when “use_cachedir” module option isn’t used and no custom set-
		#     tings control logging via the environment.
		#
		LOGDIR="$MYDIR/logs"
		__check_directory "$LOGDIR"  'Logging'

	elif [ -w "$TMPDIR" ]; then
		#
		#  5. TMPDIR/logs
		#     Last resort solution. The log is written, but then is wiped
		#     along with TMPDIR, – except when an error would occur. To keep
		#     TMPDIR (and logs) after clear runs, “tmpdir” verbosity channel
		#     would have to be raised by 1.
		#     P.S. Can’t you really allow writing to CACHEDIR or MYDIR?
		#
		LOGDIR="$TMPDIR/logs"
		__check_directory "$LOGDIR"  'Logging'

	elif [ -w '/tmp' ]; then
		#
		#  6. /tmp
		#     The very last resort is to write stuff directly to /tmp. It
		#     should be safe, as Bahelite wipes logs only by name and launch
		#     time… but this is isn’t right, you hear me?
		#
		LOGDIR='/tmp'

	else
		err 'No writeable directory to store logs to.'
	fi

	[ -v LOGPATH ] || {
		LOGNAME="$MYNAME_NOEXT.$MY_LAUNCH_TIME"
		LOGPATH="$LOGDIR/$LOGNAME.log"
	}
	#  Must be set at one place, because it will be used in another function,
	#  that stops logging. (Keys to tee are separated for compatibility.)
	BAHELITE_LOGGING_TEE_CMD=(tee -i -a "$LOGPATH")

	(( v_level >= 3 ))  \
		&& info "LOGDIR = $LOGDIR
		         LOGPATH = $LOGPATH"

	clear_logdir

	[ -v overwrite_logpath ]  && {
		(( v_level >= 3 ))  \
			&& info 'LOGPATH file is going to be trimmed to 0 length.'
		echo -n  >"$LOGPATH"
	}
	echo "${__mi}Log started at $(LC_TIME=C date)."  >>"$LOGPATH"
	echo "${__mi}Command line: $CMDLINE" >>"$LOGPATH"
	for ((i=0; i<${#ORIG_ARGS[@]}; i++)) do
		echo "${__mi}ORIG_ARGS[$i] = ${ORIG_ARGS[i]}" >>"$LOGPATH"
	done

	#  Toggling ondebug trap, as (((under certain circumstances))) it happens
	#  to block ^C sent from the terminal.
	functrace_off

	#  When we will be exiting (even successfully), we will need to send
	#  SIGPIPE to that tee, so it would quit nicely, without terminating
	#  and triggering an error. It will, however, quit with a code >0,
	#  so we catch it here with “|| true”.

	(( ${VERBOSITY_CHANNELS[log]} == 1 )) && {
		(( v_level >= 3 ))  \
			&& info 'Only stderr will be written.'

		#  If console verbosity (that is primary and not aware of whether
		#  logging would be enabled afterwards) has directed the stderr
		#  to /dev/null, it must be restored, and then the FD 2 must be
		#  redirected to the file in $LOGPATH.
		[ -v STDERR_ORIG_FD ] && {
			#  Restore the original FD 2 for the exec
			exec 2>&${STDERR_ORIG_FD}
		}
		if [ -v STDERR_ORIG_FD ]; then
			#  Console has directed FD 2 to /dev/null, so we’re free
			#    to take it and redirect it to the log, which needs
			#    that output.
			#  Moreover, we cannot make it so that FD 2 would point
			#    to /dev/null (for console) and still use the same FD 2
			#    to pipe output from commands to the log or to a subpro-
			#    cess with tee.
			exec 2>>"$LOGPATH"
		else
			#  || true is needed only for non-nice, emergency and direct
			#  killing of the logging tee (Ctrl+F “Way II”).
			exec 2> >( "${BAHELITE_LOGGING_TEE_CMD[@]}" >&2 || true)
			BAHELITE_LOGGING_USES_TEE=t
			(( v_level >= 4 ))  \
				&& info 'Have to duplicate stderr for console. Using tee.'
		fi
	}

	(( ${VERBOSITY_CHANNELS[log]} >= 2 )) && {
		(( v_level >= 3 ))  \
			&& info 'Writing both stdout and stderr.'

		#  The messages differ between levels 2 and 3:
		#    - on level 2 messages of info/abort/plainmsg level
		#      are forbidden in the messages module. The logging verbosity
		#      uses console settings here, this cannot be changed.
		#    - on level 3+ all messages are allowed.
		#
		#  Same story as above, expect that here we handle both
		#  stdout and stderr.
		#
		[ -v BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL ] && {
			#  Restore the original FD 1 for the exec
			exec 1>&${STDOUT_ORIG_FD}
		}
		if [ -v BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL ]; then
			#  Console doesn’t need this FD, but the log does.
			exec >>"$LOGPATH"
		else
			#  Console needs this FD, so we have to use tee.
			#  || true is needed for two reasons:
			#      1. To handle non-nice, emergency and direct killing
			#         of the logging tee (Ctrl+F: “Way II”).
			#      2. Without “|| true” subshell will return a negative
			#         result which will end the program before it’s due.
			exec > >( ${BAHELITE_LOGGING_TEE_CMD[@]} || true)
			BAHELITE_LOGGING_USES_TEE=t
			(( v_level >= 4 ))  \
				&& info 'Have to duplicate stdout for console. Using tee.'
		fi

		[ -v BAHELITE_CONSOLE_VERBOSITY_SENT_STDERR_TO_DEVNULL ] && {
			#  Restore the original FD 2 for the exec
			exec 2>&${STDERR_ORIG_FD}
		}
		if [ -v BAHELITE_CONSOLE_VERBOSITY_SENT_STDERR_TO_DEVNULL ]; then
			#  Console doesn’t need this FD, but the log does.
			exec 2>>"$LOGPATH"
		else
			#  Console needs this FD, so we have to use tee.
			#  || true is needed for two reasons:
			#      1. To handle non-nice, emergency and direct killing
			#         of the logging tee (Ctrl+F: “Way II”).
			#      2. Without “|| true” subshell will return a negative
			#         result which will end the program before it’s due.
			exec 2> >( "${BAHELITE_LOGGING_TEE_CMD[@]}" >&2 || true)
			BAHELITE_LOGGING_USES_TEE=t
			(( v_level >= 4 ))  \
				&& info 'Have to duplicate stderr for console. Using tee.'
		fi
	}

	#  Restoring the trap on DEBUG
	functrace_rewind

	BAHELITE_LOGGING_STARTED=t
	VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[log]='L'

	(( v_level >= 3 )) && mildec
	return 0
}
extglob_rewind
#  No export: init stage function.


 # Removing old log files
#
#  Only logs matching $MYNAME_NOEXT<separator><any valid timestamp>… are con-
#  sidered relevant to the main script and may be considered for removal.
#  Separator is either a dot “.” or an underscore “_” character (the latter
#  was used in older versions). A valid timestamp is a string like
#  “2022-12-01_13:00:05”.
#
#  The ending part can be any (but no spaces), thus each run may have multiple
#  log files that differ by the ending part.
#
#  Logs from the last 5 runs (determined by the timestamp) are preserved by
#  default. This number can be altered with the “max_runs” module option.
#
clear_logdir() {
	declare -g  LOGDIR
	declare -g  MYNAME_NOEXT
	declare -g  BAHELITE_LOGGING_MAX_RUNS

	local -a  relevant_logs=()  # must be initialised as an empty array
	local -a  log_patterns_to_keep

	local  relevant_logs_count
	local  pattern
	local  this_run
	local  i
	local  j

	pushd "$LOGDIR" >/dev/null

	#  First, to gather all relevant files in the LOGDIR, that match the cur-
	#  rently running main script.
	#
	while read -r -d ''; do
		relevant_logs+=( "${REPLY##*/}" )
	done < <(
		find -L "$LOGDIR"  \
		     -regextype egrep  \
		     -regex ".+/$MYNAME_NOEXT[_\.][0-9_:-]{19}\..+"  \
		     -print0
	)
	#
	#  Now to find out, which timestamps are the most recent five
	#  (or however many user specified with BAHELITE_LOGGING_MAX_RUNS)
	#
	this_run=$(( VERBOSITY_CHANNELS[log] > 0  ? 1  : 0 ))
	readarray -d $'\n' -t  log_patterns_to_keep < <(
		{ IFS=$'\n'; echo "${relevant_logs[*]}"; }   \
			| sed -rn "s/^($MYNAME_NOEXT[_\.][0-9_:-]{19})\..*/\1/p"  \
			| sort -u  \
			| tail -n $(( BAHELITE_LOGGING_MAX_RUNS - this_run ))
	)
	#                                               ^ an optional −1, because
	#  this run will create some log files, too (provided that the “log” out-
	#  put channel wasn’t set to 0). And the requested count plus this run
	#  would make LOGDIR have files for one extra run. That’s a discrepancy,
	#  which is solved with decrementing the requested count by one, when nece-
	#  ssary. The check for the module option “max-runs” has verified, that
	#  BAHELITE_LOGGING_MAX_RUNS won’t be set to anything lower than 1, so we
	#  don’t have to worry about the negative numbers.
	#
	#  Remove logs, that are to be kept, from the list of relevant logs,
	#  so that that list would be comprised of those logs, which are to be
	#  removed.
	#
	relevant_logs_count=${#relevant_logs[*]}
	for ((i=0; i<relevant_logs_count; i++)); do
		for ((j=0; j<${#log_patterns_to_keep[*]}; j++)); do
			pattern="${log_patterns_to_keep[j]}"
			[[ "${relevant_logs[i]}" =~ ^$pattern ]] && {
				unset relevant_logs[$i]
				continue 2
			}
		done
	done

	{ IFS=$'\n'; echo "${relevant_logs[*]}"; }  \
		| xargs rm -f &>/dev/null || true

	popd >/dev/null
	return 0
}


print_logpath() {
	info "Log is written to
	      $LOGPATH"
	return 0
}
export -f  print_logpath


 # Returns absolute path to the last modified log in $LOGDIR.
#  [$1] – log name prefix, if not set, equal to $MYNAME without “.sh”
#       at the end (caller script’s own log).
#  [$2] – “log”, “log-nc” or other file extension, among which the last
#       file will be searched.
#
get_lastlog_path() {
	local  logname="${1:-$MYNAME_NOEXT}"
	local  ext="${2:-log}"
	local  lastlog_name

	pushd "$LOGDIR" >/dev/null
	lastlog_name=$(
		set +f
		ls -tr "${logname}"{_,\.}????-??-??_??:??:??.$ext |& tail -n1
	)
	[ -f "$lastlog_name" ] || {
		popd >/dev/null
		return 1
	}
	popd >/dev/null

	echo "$LOGDIR/$lastlog_name"
	return 0
}
export -f  get_lastlog_path


 # Reads the contents of the last log file to stdout
#  [$1] – log file name prefix to search for (script name without date and
#         extension). If not set, equals to $MYNAME without .sh at the end
#         (i.e. refers to the caller script’s default log name).
#
#  You are supposed to read the output into a variable, i.e.
#
#      lastlog_text=$(read_lastlog)
#
#  or, if you run one main script from another…
#
#      lastlog_text=$(
#          LOGDIR="$custom_path"  read_lastlog 'the_other_main_script_name'
#      )
#
read_lastlog() {
	# declare -g LASTLOG_ERROR
	local  err_msg_marker
	local  err_msg_text
	local  lastlog_path

	lastlog_path=$(get_lastlog_path "$@")

	strip_colours_from_file "$lastlog_path"

	 # Setting LASTLOG_ERROR is disabled, for it’s easier to just
	#  heave-ho the entire log into another log, than to parse its errors.
	#
	#  However, the following two methods can be consiedered:
	#  1. Finding “--- Call stack”
	#     Conditions:
	#       - the error must be caught by bahelite (some are still not).
	#       - catching one line before “--- Call stack” sometimes is very
	#         useful in understanding the real error.
	#     How to grab:
	#         log_call_stack=$(
	#             grep -B 1 -A 99999 '\-\-\- Call stack ' "$LASTLOG_PATH"
	#         )
	#  2. Finding the first entrance of the red colour, or whatever sequence
	#     is put into BAHELITE_ERR_MESSAGE_COLOUR.
	#     Conditions:
	#       - the error must be caught.
	#       - catching it is equally necessary as the error with call stack,
	#         because error messages with red colour are expected and there-
	#         fore, do not print the call stack. And vice versa unexpected
	#         errors do not use an error message, they just print the trace.
	#         So there are at least two types, and both of them are important.
	#     How to grab:
	#       Replace shell’s own alias for the escape sequence (\e)
	#       with its real hex code (\x1b), so that it could be used
	#       in the pattern for sed.
	#         err_msg_marker="${BAHELITE_ERR_MESSAGE_COLOUR//\\e/\\x1b}"
	#         err_msg_marker="${err_msg_marker//\[/\\\[}"
	#         sed -rn "/$err_msg_marker/,$ p" "$LASTLOG_PATH"
	#       or simply
	#         log_err_message=$(
	#             sed -rn "/\x1b\[31m/,$ p" "$LASTLOG_PATH"
	#         )
	#  3. Finding uncaught errors. There is no way to tell for sure,
	#       if a line in the log would, so, unless all of the error could be
	#       caught, it is more reasonable to perform all these checks from
	#       a script above the one, whose LOGPATH is parsed, i.e. the script call-
	#       ing the mother script. The script above should receive a non-zero
	#       exit code from the inner script, and this provides the reason
	#       to do every possible check for an error, including this one.
	#     Indeed, this check should be the last one among the three.
	#     How to grab:
	#         log_last_line=$(
	#             tac "$LASTLOG_PATH" | grep -vE '^\s*$' | head -n1  \
	#                 | sed -r "s/.*/$__mi&/" >&2
	#         )
	#
	return 0
}
export -f read_lastlog


stop_logging() {
	local  tee_pids=()
	local  tee_parents_pids=()
	local  use_pause
	local  v_level=${VERBOSITY_CHANNELS[Logging]}

	__pause_if_needed() {
		[ -v use_pause ] || return 0
		echo -en "\n${__bri:-}Press any key to continue${__s:-} ${__g:-}>${__s:-} "
		read -n1
		echo -e '\n'
		return 0
	}


	[ -v BAHELITE_LOGGING_STARTED  -a  -v BAHELITE_LOGGING_USES_TEE ] && {
		#  Without this the script would seemingly quit, but the bash
		#    process will keep hanging to support the logging tee.
		#  Searching with a log name is important to not accidentally kill
		#    the mother script’s tee, in case one script calls another,
		#    and both of them use Bahelite.
		#
		#  Since the “tee” is made to ignore signals (tee -ia), there is no
		#    need to kill it, as the main process catches all the signals,
		#    and tee now receives SIGPIPE (or SIGHUP) in a natural way.
		#  Is this still needed?

		(( v_level >= 4 )) && {
			echo
			info-4 "Bahelite will now proceed to stop the logging processes."
			milinc
			[ -v BAHELITE_LOGGING_TEE_DEBUG_PAUSE ]  \
				&& use_pause=t  \
				|| info-4 "Need a pause?
				           There is a helper variable BAHELITE_LOGGING_TEE_DEBUG_PAUSE.
				           Set it to any value before calling the main script, e.g.
				               BAHELITE_LOGGING_TEE_DEBUG_PAUSE=t  ./my_script.sh
				           and it will wait for a keypress between the stages of this
				           process."
			echo
			declare -p ORIG_BASHPID  BASHPID  ORIG_PPID  PPID
			echo
			ps -o session,pgid,ppid,pid,s,args --forest -s $ORIG_PPID  || {
				warn "Our own PPID didn’t start the process session we’re in.
				      Are we running from another script?"
				ps -o session,pgid,ppid,pid,s,args  \
				   --forest  \
				   -s /proc/$ORIG_PPID/sessionid
			}
			echo
		}

		 # There are several ways to stop logging. The nicest one is preferred.
		#
		#  Way I. Finding the parent shells of the logging tee processes
		#         and closing them.
		#  − Somewhat slower than doing it not nicely (but not noticeably slow
		#    for the user).
		#  + No ugly messages to console.
		#  + Clean exit. All shells and processes quit with zero code, so
		#    there can be no false errors. If an error happens, this means,
		#    that it is a real error and something went wrong.
		#

		 # Finding PIDs of the logging tee processes themselves
		#  “--session 0” is pgrep syntax for process’s own session id.
		#    It works fine for both standalone main scripts and for main
		#    scripts that run from within another main script.
		#  $ORIG_PPID may be used instead of it ONLY if the main script
		#    is standalone (not running from within another main script, that
		#    itself uses bahelite – then session id will belong to the higher
		#    main script’s PPID, and ALL underlying processes including the
		#    internal main script and its subprocesses, in which tee is run-
		#    ning, they all will have that as session id)
		#
		tee_pids=(
			$(pgrep --session 0  -f "${BAHELITE_LOGGING_TEE_CMD[*]}" || true)
		)
		(( v_level >= 4 ))  \
			&& info-4 "TEE PIDS: ${tee_pids[*]} (${#tee_pids[*]} in total)."

		#  Something has killed logging tees before us. If they somehow hanged,
		#  the user might have pkill’ed them by hand or something. Anyway,
		#  this function should close logging, and there is already nothing
		#  to close, so we’re quitting.
		(( ${#tee_pids[*]} == 0 )) && return 0

		(( v_level >= 4 ))  \
			&& __pause_if_needed

		tee_parents_pids=( $(ps h -o ppid "${tee_pids[@]}" || true) )
		(( v_level >= 4 ))  \
			&& info-4 "TEE PARENTS PIDS: ${tee_parents_pids[*]} (${#tee_parents_pids[*]} in total)."

		(( ${#tee_parents_pids[*]} == ${#tee_pids[*]} )) && {

			(( v_level >= 4 ))  && {
				__pause_if_needed
				info-4 'Killing tees by sending HUP to their shells:'
			}

			 # When the subprocesses, that hold logging tee processes, are
			#  killed, bash prints their PIDs to console. This cannot be
			#  avoided with a simple stream redirection like
			#      exec {TEMP_STDERR_FD}>&2
			#      kill -TERM "${tee_parents_pids[@]}"
			#      exec 2>&${TEMP_STDERR_FD}
			#  as once the remembered FD for stderr is restored, it prints
			#  the accumulated contents. And no, neither read, nor readarray,
			#  nor even read inside while with the descriptor passed via the
			#  -u switch can flush the contents. The only way is to shut down
			#  stderr and stdout completely.
			#
			#  Thankfully, stop_logging is called at the very end of the trap
			#  on EXIT, i.e. bahelite_on_exit and is literaly the last executed
			#  command. There is nothing past this function, traps on signals
			#  are already unset at this point. So it’s comparatively safe
			#  to close stdout and stderr.
			#
			#  As a measure to ease the debugging, stdout and stderr are closed
			#  only when the verbosity stays on the default, user-friendly
			#  level (i.e. not higher than 3 for console or the log file).
			#  Once the verbosity level is raised, stdout and stderr will remain
			#  intact and let out the PIDs show in the console, along with
			#  the messages that may come after this. (Though in regular use
			#  no messages should be shown).
			#
			(( VERBOSITY_CHANNELS[Error_handling] < 4 && v_level < 4 )) && {
				exec 2>/dev/null
				exec >/dev/null
			}

			 # “kill -HUP” caused a segmentation fault before. Read the
			#  comment above the initial declarations in start_logging().
			#
			#  There is an evidence retrieved with consolve verbosity level 5,
			#  that -TERM does not kill the tee parents – tee processes are
			#  still there after 10 seconds, and this function has to go
			#  Way II to deal with them.
			#
			#  However, it works fine like it is, and it is undesirable to
			#  touch a working system. Killing parents with -INT won’t kill
			#  tees. Maybe sending -PIPE to them is not quite clean (tee com-
			#  mands themselves must be shielded with “|| true”), but at least
			#  there was no spam to console – even without the redirection to
			#  /dev/null above. So, touching it is undesirable.
			#
			kill -TERM "${tee_parents_pids[@]}"

			(( v_level >= 4 ))  && {
				__pause_if_needed
				info-4 'Testing if the processes still hang there:'
			}

			#  No processes = BAD from pgrep, GOOD for us – they are closed,
			#  as they should be. So “… || return 0”. All done.
			#
			pgrep --session 0  -f "${BAHELITE_LOGGING_TEE_CMD[*]}"  \
				|| return 0
		}


		(( v_level >= 4 ))  && {
			echo
			info-4 "Normally, logging processes are already stopped at this point
			        and $FUNCNAME() returns – the code past this point never runs.

			        However, when the debugging level is raised, $FUNCNAME doesn’t
			        close stdout and stderr file descriptors. Because of this, the
			        code above wasn’t able to close the logging tee processes grace-
			        fully (as it normally does). $FUNCNAME will have to resort to
			        a less graceful solution, that’s placed between this code and
			        the end of the function. Remember, that if the verbosity levels
			        were set at their default values, this function would already
			        quit. See also the comments in the body of this function."

			        #  Keeping stdout and stderr open at a raised verbosity level
			        #  is done to show as much debugging output as possible. The
			        #  debugging of this very function would not be feasible with-
			        #  out holding stdout and stderr open until the last moment,
			        #  that allows to have a clean exit from the program.

			__pause_if_needed
			mildec
		}


		 # Way II. Sending pkill -PIPE to tee processes
		#  + Fast.
		#  + Doesn’t leave any ugly messages to console.
		#  − It doesn’t make a clean exit.
		#    As this essentially abruptly kills the tee processes, they
		#    will quit with a negative result, what will trigger additional
		#    errors. To avoid that, “|| true” must be added inside the sub-
		#    shell call >( … ) with exec in start_logging() above.
		#
		pkill -PIPE  --session 0  -f "${BAHELITE_LOGGING_TEE_CMD[*]}" || true


		 # Way III. Sending pkill -KILL to tee processes.
		#  + Fast.
		#  − Leaves ugly messages to console.
		#  − It doesn’t make a clean exit (see way II).
		#
		# pkill -KILL  --session 0  -f "tee -ia $LOGPATH" || true
		#
	}

	#  In case logging has started and a log file was created, write a version
	#  with stripped colours and place it besides. LOGPATH may be not set, if
	#  the main program would quit early: for example, when “--verbosity-help-
	#  Logging” is used, the corresponding help function is already knwon –
	#  as the “logging” module itself was loaded (sourced), but at that time
	#  start_logging() hasn’t been called yet, so, when bahelite_on_exit()
	#  runs the tasks on quit (and the logging module has defined the corres-
	#  ponding functions, as it was sourced) LOGPATH isn’t set at that time
	#  yet.
	#
	[ -v LOGPATH ] && {
		if [ -v HAVE_ANSIFILTER ]; then
			ansifilter "$LOGPATH" > "$LOGPATH-nc"
		else
			warn "Plain text version of the log could be created,
			      if “ansifilter” was installed on this system."
		fi
	}
	return 0
}
#  No export: it’s for bahelite_on_exit() function, that executes only
#  in top level context.


postload_job='start_logging'
[ -v BAHELITE_MODULE_CACHEDIR_VER ]  \
	&& postload_job+=':after=prepare_cachedir'

BAHELITE_POSTLOAD_JOBS+=( "$postload_job" )
unset postload_job


return 0