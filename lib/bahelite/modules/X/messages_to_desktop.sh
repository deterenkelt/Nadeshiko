#  Should be sourced.

#  messages_to_desktop.sh
#  Implements duplicating of certain classes of console
#    messages as desktop notifications. Works with notify-send
#    or an analogue.
#  Author: deterenkelt, 2019–2020
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	return 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MESSAGES_TO_DESKTOP_VER ] && return 0
bahelite_load_module 'messages' || exit $?
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MESSAGES_TO_DESKTOP_VER='1.1.8'

 # Which messages are shown depending on the verbosity level:
#  0 – no messages
#  1 – only error-level messages
#  2 – only error- and warning-level messages
#  3 – all messages (default)
#
vchan setup  name=desktop  level=3  meta=M  hint=output  min_level=0  max_level=3

show_help_on_verbosity_channel_desktop() {
	cat <<-EOF
	desktop (output channel)
	    Controls output to desktop with notify-send.
	    Levels:
	    0 – no desktop messages are sent.
	    1 – only error messages are shown.
	    2 – only error and warning messages are shown.
	    3 – all messages are shown (the default).
	EOF
	return 0
}

#  No self-report verbosity channel, the module is simple.


BAHELITE_INTERNALLY_REQUIRED_UTILS+=(
	notify-send
)


BAHELITE_INTERNALLY_REQUIRED_UTILS_HINTS+=(
	[notify-send]='notify-send belongs to the libnotify package
	https://developer.gnome.org/libnotify/'
)


show_usage_messages_to_desktop_module() {
	cat <<-EOF
	Bahelite module “messages_to_desktop” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_messages_to_desktop_module
		exit 0

	else
		redmsg "Bahelite: messages_to_desktop: unrecognised option “$opt”."
		__show_usage_messages_to_desktop_module
		exit 4
	fi
done
unset opt



                      #  Desktop notifications  #

 # To send a notification to both console and desktop, main script should
#  call info-ns(), warn-ns() or err() functions, which are defined
#  in messages.sh.
#

 # Define this variable to make notifications with icons.
#
# declare -gx MSG_NOTIFYSEND_USE_ICON=t


 # Shows a desktop notification
#  $1 – message.
#  $2 – icon type: empty, “information”, “warning” or “error”.
#
bahelite_notify_send() {
	local  msg="$1"
	local  type="${2:-info}"
	local  duration
	local  urgency
	local  icon

	case "${VERBOSITY_CHANNELS[desktop]}" in
		0)	return 0
			;;

		1)	[[ "$type" =~ ^(dialog-|)(info(rmation|)|warn(ing|))$ ]]  \
				&& return 0
			;;

		2)	[[ "$type" =~ ^(dialog-|)info(rmation|)$ ]]  \
				&& return 0
			;;
	esac

	msg=${msg##+([[:space:]])}
	msg=${msg%%+([[:space:]])}
	#
	#  Support all the possible variations, that may come to mind to the
	#  author of the main script: bahelite function names and notify-send
	#  icons (along with the inconsistency in the icon names, that is
	#  with or without “dialog-” prefix).
	#
	case "$type" in
		err|error|dialog-error)
			icon='dialog-error'
			urgency=critical
			duration=10000
			;;
		warn|warning|dialog-warning)
			icon='dialog-warning'
			urgency='normal'
			duration=10000
			;;
		info|information|dialog-information)
			icon='info'
			urgency='normal'
			duration=3000
			;;
	esac

	[ -v MSG_NOTIFYSEND_USE_ICON ] || unset icon

	(
		 # Dbus session address may be stale in the environment, – and to force
		#    applications to evaluate it at runtime, this variable must be
		#    unset. Displaying a desktop message is of critical importance for
		#    the scripts not running from the terminal. The lack of a message
		#    may lead to confusion.
		#  The value in the environment variable gets stale, if the shell was
		#    spawned during one Dbus session, but it was closed, and the shell
		#    now runs in another. This is common for applications like tmux
		#    and screen, that usually persist between X sessions.
		#  As the variable is a global one, unsetting it in the global scope
		#    may lead to unforseen consequences, hence the subshell.
		#
		unset DBUS_SESSION_BUS_ADDRESS
		#  The hint is for the message to not pile in the stack –
		#  it is limited.
		notify-send --hint int:transient:1  \
		            --urgency "$urgency"  \
		            -t $duration  \
		            "$MY_DISPLAY_NAME"  "$msg"  \
		            ${icon:+--icon=$icon}
	)
	return 0
}
export -f  bahelite_notify_send


 # info() that also duplicates a message to desktop
#  (by default only err() and abort() messages are duplicated.)
#
info-ns() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* "
		[desktop_message]='yes'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  info-ns


 # Like warn(), but also duplicates the message to desktop
#  (by default only err() and abort() duplicate their messages to desktop).
#
warn-ns() {
	declare -A __msg_properties=(
		[message_array]='WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='yes'
		[desktop_message_type]='warn'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warn-ns



return 0