#  Should be sourced.

#  menu.sh
#  Implements interactive menus of various kinds.
#  Author: deterenkelt, 2018–2024
#

 # The benefits of using these functions over “read” and “select”:
#  - with read and select you type a certain string as teh answer…
#    …and the user often mistypes it
#    …and you must place strict checking on what is typed
#    …which blows your code, so you need a helper function anyway.
#    But with Bahelite this function is ready for use and the moves
#    are reduced to keyboard arrows – one can’t answer wrong, when
#    arrows highlight one string or the other (“Yes” or “No” for example)
#  - the menu changes appearance depending on the number of elements
#    to choose from:
#    1)  Pick server: "Server 1" < "Server 2"    (server 1 is highlighted)
#        <right arrow pressed>
#        Pick server: "Server 1" > "Server 2"    (now server 2 is highlighted)
#    2) Choose letter: <| S |>                   ()
#
#    …

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MENU_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MENU_VER='3.0.0'
bahelite_load_module 'variable_checks' || exit $?


 # Output verbosity channel
#  To be implemented.
#
# vchan setup  name=menu  level=3  meta=M  hint=output

# show_help_on_verbosity_channel_menu() {
# 	cat <<-EOF
# 	menu (output channel)
# 	    Changes how verbose should menu options be
# 	    Levels:
# 	    0 – terse/regular output (the default);
# 	    1 – enable hints/descriptions.
# 	EOF
# 	return 0
# }



 # Self-report verbosity channel
#  It’s not clear how to make the debugging messages not turn the screen
#  into a mess, as drawing menus is heavily tied to the output per-character.
#
# vchan setup  name=Menu  level=2  hint=self-report  group_items=__menu

# show_help_on_verbosity_channel_Menu() {
# 	cat <<-EOF
# 	Menu (self-report channel)
# 	    Reports on how menus are built.
# 	    Levels:
# 	    2 – no additional output (the default);
# 	    3 –
# 	    4 –
# 	EOF
# 	return 0
# }


__show_usage_menu_module() {
	cat <<-EOF
	Bahelite module “menu” options:

	style=<${__so}cool${__s}|${__so}poor${__s}>
	    Determines, which character set to use for drawing menu elements.
	    “cool” style uses unicode symbols, the intended and default look.
	    “poor” style uses simplest ASCII alternatives to cool symbols.

	clear_on_redraw=<${__so}yes${__s}|${__so}no${__s}>
	    By default, when an input forces the menu to redraw itself (in order
	    to reflect a change), the fresh menu is printed in the terminal simply
	    as a new set of lines – and the old lines are still seen above, though
	    behind a gap of several empty lines, intentionally left between them.

	    If you wish the menu to be always drawn over a clear screen, set this
	    option to “yes”.
	EOF
	return 0
}


 # Characters to use for pseudographic
#  Unicode capable terminals (all terminals running in X and jfbterm on TTY)
#    are able to draw characters from the “cool” set, while the others (bare
#    TTY) better use the “poor” set.
#
declare -gax __MENU_COOL_GRAPHIC_STYLE=(
	'“'  '”'  '…'    '–'  '│'  '─'  '∨'  '∧'  '◆' '◀' '▶'
)
declare -gax __MENU_POOR_GRAPHIC_STYLE=(
	'"'  '"'  '...'  '-'  '|'  '-'  'v'  '^'  '+' '<|' '|>'
)
#
declare -gnx MENU_GRAPHIC_STYLE='__MENU_COOL_GRAPHIC_STYLE'


 # To clear screen each time menu() redraws its output.
#  Clearing helps to focus, while leaving old screens
#  allows to check the console output before menu() was called.
#
# declare -gx MENU_CLEAR_SCREEN=t


for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_menu_module
		exit 0

	elif [[ "$opt" =~ ^style=(cool|poor)$ ]]; then
		declare -n MENU_GRAPHIC_STYLE="__MENU_${BASH_REMATCH[1]^^}_GRAPHIC_STYLE"

	elif [[ "$opt" =~ ^clear(_on_|-on-)redraw=(yes|no)$ ]]; then
		[ "${BASH_REMATCH[2]}" = yes ]  && MENU_CLEAR_SCREEN=t

	else
		redmsg "Bahelite: menu: unrecognised option “$opt”."
		__show_usage_menu_module
		exit 4
	fi
done
unset opt


 # Shows a menu, where a selection is made with only arrows on keyboard.
#  $1    – a prompt text.
#  $2..n – options to choose from.
#          The first option in the list will become selected as default.
#            If the default option is not the first one, it should be
#            given _with underscores_.
#          If the user must set values to options, vertical type of menu
#            allows to show values aside of the option names.
#            To pass a value for an option, add it after the option name
#            and separate from it with “---”.
#          If the option name has underscores marking it as default,
#            they surround only the option name, as usual.
#
menu-bivar()    { __menu "$@"; }
menu-carousel() { __menu "$@"; }
menu-list()     { __menu "$@"; }
__menu() {
	declare -gx CHOSEN

	local  arg
	local  mode
	local  pairs
	local  opts
	local  chosen_idx=0  #   ← both are zero-based
	local  start_idx     #  ←
	local  choice_is_confirmed
	local  prompt
	local  options=()
	local  optvals=()
	local  option
	local  help_text


	                    #  Reading from keyboard  #

	local  rest
	local  arrow_up=$'\e[A'
	local  arrow_right=$'\e[C'
	local  arrow_down=$'\e[B'
	local  arrow_left=$'\e[D'
	local  home_key=$'\e[7~'        #  The escape sequence depends on the stan-
	local  alter_home_key=$'\e[1~'  #  dard the terminal emulator is following.
	local  end_key=$'\e[8~'         #
	local  alter_end_key=$'\e[4~'   #
	local  clear_line=$'\r\e[K'
	local  left=t
	local  right=t
	local  i


	                      #  Drawing the menu  #

	local  ___g=${__green:-$__standout}

	local -n graphic='MENU_GRAPHIC_STYLE'
	local oq=${graphic[0]}    #  opening quote
	local cq=${graphic[1]}    #  closing quote
	local el=${graphic[2]}    #  ellipsis
	local da=${graphic[3]}    #  en dash
	local vb=${graphic[4]}    #  vertical bar
	local hb=${graphic[5]}    #  horizontal bar
	local ad=${graphic[6]}    #  arrow down
	local au=${graphic[7]}    #  arrow up
	local di=${graphic[8]}    #  diamond
	local trl=${graphic[9]}   #  triangle pointing left
	local trr=${graphic[10]}  #  triangle pointing right

	if [[ "${FUNCNAME[1]}" =~ ^menu-(bivar|carousel|list)$ ]]; then
		mode="${BASH_REMATCH[1]}"
		[ "$mode" = bivar ] && mode='bivariant'
	else
		err "__menu() must be called either as “menu-bivar”, or “menu-carousel” or “menu-list”."
	fi

	for arg in "$@"; do
		if [ "$arg" = 'pairs' ]; then
			pairs=t
			shift

		elif [[ "$arg" =~ ^start_idx=($INT)$ ]]; then
			#  Overrides the element chosen by default, even if it’s
			#  selected by _undescores_.
			start_idx="${BASH_REMATCH[1]}"
			shift

		elif [[ "$arg" =~ ^foreword=(.+)$ ]]; then
			#  The text that is shown after the title, but before
			#  any list items.
			foreword="${BASH_REMATCH[1]}"
			shift

		elif [[ "$arg" =~ ^help_text=(.+)$ ]]; then
			#  The text that is shown after all list items,
			#  but before the prompt.
			help_text="${BASH_REMATCH[1]}"
			shift

		else
			break
		fi
	done

	(( $# < 3 ))  \
		&& err-stack "Prompt and at least two arguments must be specified to show a menu."

	prompt="$1"
	shift

	[ -v pairs  -a  "$mode" != 'list' ]  \
		&& err-stack 'Pairs of keys and values work only with --mode list!'
	# [ "${OVERRIDE_DEFAULT:-}" ] && chosen_idx="$OVERRIDE_DEFAULT"

	opts=("$@")
	if [ -v pairs ]; then
		for ((i=1; i<${#opts[*]}+1; i++)); do
			((  (i % 2) == 0  ))  \
				&& optvals+=("${opts[i-1]}")  \
				|| options+=("${opts[i-1]}")
		done
	else
		for ((i=0; i<${#opts[*]}; i++)); do
			#
			#  ^ This mode allows some options to have value and some be just
			#    ordianry per se options. Hence “$optval[i]” may be set for
			#    one option and unset for another.
			#
			if [[ "${opts[i]}" =~ ^(.+)---(.+)$ ]]; then
				options[i]="${BASH_REMATCH[1]}"
				optvals[i]="${BASH_REMATCH[2]}"
			else
				options[i]="${opts[i]}"
			fi
		done
	fi

	for ((i=0; i<${#options[*]}; i++)); do
		[[ "${options[i]}" =~ ^_(.+)_$ ]] && {
			#  Option specified _like this_ is to be selected by default.
			[ -v start_idx ] || chosen_idx=$i
			#  Erasing underscores.
			options[i]="${BASH_REMATCH[1]}"
		}
	done

	[ -v start_idx ] && {
		(( start_idx <= (${#options[*]}-1) ))  \
			&& chosen_idx=$start_idx  \
			|| err-stack "Start index “$start_idx” shouldn’t be greater than the maximum index “${#options[*]}”."
	}

	[ "$mode" = 'bivariant' ] && {
		(( chosen_idx == 0 ))  \
			&& right=''        \
			|| left=''
	}

	(( ${#options[*]} < 2 ))  \
		&& err-stack "${FUNCNAME[1]}: needs two or more items to choose from."

	until [ -v choice_is_confirmed ]; do
		[ -v MENU_CLEAR_SCREEN ] && clear
		case "$mode" in
			bivariant)
				echo -en "${__mi}$prompt ${left:+$___g}${options[0]}${left:+$__s $trl} ${right:+$trr $___g}${options[1]}${right:+$__s} "
				;;

			carousel)
				(( chosen_idx == 0 )) && left=''
				(( chosen_idx == (${#options[*]} -1) )) && right=''
				echo -en "$prompt ${left:+$___g}$trl${__s} ${__bri}${options[chosen_idx]}$__s ${right:+$___g}$trr$__s "
				;;

			list)
				echo -en "\n\n/${hb}${hb}${hb}"
				echo -n  "  ${__bri}${prompt}${__s}"
				echo     "  ${hb}${hb}${hb}${hb}${hb}${hb}${hb}"

				[ -v foreword ] && {
					cat <<-EOF

					$foreword

					EOF
				}

				for ((i=0; i<${#options[*]}; i++)); do

					if (( i == chosen_idx )); then
						pre="$___g${di}$__s"
					else
						if (( i == 0 )); then
							pre="$___g${au}$__s"
						else
							(( i == (${#options[@]} -1) ))  \
								&& pre="$___g${ad}$__s"  \
								|| pre="${vb}"
						fi
					fi

					echo -en "$pre ${options[i]}"
					[ -v optvals[$i] ]  \
						&& echo  "  ${__bri}"\["${__s}${optvals[i]}${__bri}"\]"${__s}"  \
						|| echo

				done

				[ -v help_text ] && {
					cat <<-EOF

					$help_text

					EOF
				}

				echo -en "${___g}Up${__s}/${___g}Down${__s}, "
				echo -en "${___g}Home${__s}/${___g}End${__s}: "
				echo -en "select parameter, ${___g}Enter${__s}: confirm. "
				;;
		esac
		#  Sometimes there is something wrong with stdin, and read returns 1.
		#    Apparently, it receives EOF. SO says it’s common when we are
		#    in a loop reading something else, e.g. a file, but this happens
		#    also in some inexplainable situations.
		#  So to make it work properlyin every case, we read directly
		#    from /dev/tty.
		read -s -n1 </dev/tty

		[ "$REPLY" = $'\e' ]  \
			&& read -s -n2 rest </dev/tty  \
			&& REPLY+="$rest"

		[[ "$REPLY" =~ ^$'\e['[7814]$ ]]  \
			&& read -s -n1 rest </dev/tty  \
			&& REPLY+="$rest"

		if [ "$REPLY" ]; then
			case "$REPLY" in
				"$arrow_left"|"$arrow_down"|',')
					case "$mode" in
						bivariant)
							left=t  right=''  chosen_idx=0
							;;
						carousel)
							if (( chosen_idx == 0 )); then
								left=''
							else
								((chosen_idx--, 1))
								right=t
							fi
							;;
						*)
							(( chosen_idx != (${#options[*]} -1) ))  \
								&& ((chosen_idx++, 1))
							;;
					esac
					;;

				"$arrow_right"|"$arrow_up"|'.')
					case "$mode" in
						bivariant)
							left=''  right=t  chosen_idx=1
							;;
						carousel)
							if (( chosen_idx == (${#options[*]} -1) )); then
								right=''
							else
								((chosen_idx++, 1))
								left=t
							fi
							;;
						*)
							(( chosen_idx != 0 )) && ((chosen_idx--, 1))
							;;
					esac
					;;

				"$home_key"|"$alter_home_key")
					case "$mode" in
						carousel)
							left=''  right=t  chosen_idx=0
							;;
						list)
							chosen_idx=0
							;;
					esac
					;;

				"$end_key"|"$alter_end_key")
					case "$mode" in
						carousel)
							left=t  right=''  chosen_idx=$(( ${#options[*]} -1 ))
							;;
						list)
							chosen_idx=$(( ${#options[*]} -1 ))
							;;
					esac
					;;
			esac
			[[ "$mode" =~ ^(bivariant|carousel)$ ]]  \
				&& echo -en "$clear_line"
		else
			echo
			choice_is_confirmed=t
		fi
	done
	CHOSEN=${options[chosen_idx]}
	return 0
}
export -f  __menu  \
               menu-bivar  \
               menu-carousel  \
               menu-list


 # A wrapper over shell’s “read”.
#  Provides a prompt unified with Bahelite output – it respects current
#  indentation level and by default is coloured green, as it asks user
#  to take an action.
#
#  Commented, because it causes at least one serious problem: when read is used
#  for reading a stream or file, call to the function makes an extra line
#  to appear. This can be avoided by using “builtin read” within the main
#  script, but that’d be bad.
#
# read() {
# 	local i _args=( "$@" )
# 	for ((i=0; i<${#_args[@]}; i++)); do
# 		[ "${_args[i]}" = -p ] && {
# 			[ -v _args[i+1] ] \
# 				|| err "Prompt key is used, but no string provided."
# 			_args[i+1]="$(echo -en "${__mi}${___g}${_args[i+1]}${__s} ${__bri}>${__s} ")"
# 		}
# 	done
# 	builtin read "${_args[@]}"
# 	return 0
# }



return 0