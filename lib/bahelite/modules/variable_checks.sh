#  Should be sourced.

#  variable_checks.sh
#  Functions to check variable type and its contents:
#    - types: integer, float and (pseudo-)boolean;
#    - checks user-defined ranges for int and float types;
#    - for pseudo-boolean variables, unset the value if it’s false,
#      to turn its existence into the boolean flag itself.
#  The module “rcfile” may automatically check types, ranges etc. for rc vari-
#    ables specified in the variable RCFILE_CHECKVALUE_VARS, that should typi-
#    cally defined in the “metaconf” directory, that the main program sources.
#  Formerly a part of module “misc”.
#  Author: deterenkelt, 2018–2023
#

                            #  TO DO  #

#  This module should contain *universal* functions verifying the values.
#  That means functions at the core mustn’t call err() and must return
#  either 1 or 0 instead. These must be internal functions (__of_this_type)
#  and the functions to be called from the code must be specialised wrappers:
#    - one for verifying variables anywhere in the code. This wrapper should
#      assume that that a variable belongs to the code body and is nothing
#      else than itself;
#    - another wrapper – to check variables that are program arguments or
#      configuration file parameters.
#  Wrappers thus must call the __internal function and if it returns false,
#  call err() and refer to the variable as to:
#    - either a (generic) “variable”;
#    - or as to an “option”, meaning an option from the RC file;
#    - or as to an “argument”, meaning that the variable is a command line
#      argument. If that’s the case, the variable name should be presented
#      in the option form, i.e. with double dashes in front and with under-
#      scores replaced by hyphens.
#  It’s desirable to avoid checking the caller in the body of the __internal
#  function, and instead make it take arguments refining the behaviour. So
#  that the caller would just pass another 1–2 arguments to process an error
#  an option-way or a cmd argument-way.
#




#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_VARIABLE_CHECKS_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_VARIABLE_CHECKS_VER='1.0.2'

#  No self-report verbosity channel: here’s just many simple functions.


BAHELITE_INTERNALLY_REQUIRED_UTILS+=(
	pgrep   # (procps) Single process check.
#	wc      # (coreutils) Single process check.
#	shuf    # (coreutils) For random(), it works better than $RANDOM.
	bc      # (bc) Verifying numbers.
)
BAHELITE_INTERNALLY_REQUIRED_UTILS_HINTS+=(
	[pgrep]='pgrep is a part of procps-ng.
	http://procps-ng.sourceforge.net/
	https://gitlab.com/procps-ng/procps'
)


__show_usage_variable_checks_module() {
	cat <<-EOF
	Bahelite module “variable_checks” doesn’t take options!
	EOF
	return 0
}


for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_variable_checks_module
		exit 0

	else
		redmsg "Bahelite: variable_checks: unrecognised option “$opt”."
		__show_usage_variable_checks_module
		exit 4
	fi
done
unset opt



declare -gr PBOOL_TRUE='(y|Y|[Yy]es|1|t|T|[Tt]rue|[Oo]n|[Ee]nable[d])'
declare -gr PBOOL_FALSE='(n|N|[Nn]o|0|f|F|[Ff]alse|[Oo]ff|[Dd]isable[d])'
declare -gr PBOOL='(y|Y|[Yy]es|1|t|T|[Tt]rue|[Oo]n|[Ee]nable[d]|n|N|[Nn]o|0|f|F|[Ff]alse|[Oo]ff|[Dd]isable[d])'

 # Patterns for the common data types to be used in expressions
#  like [[ $var =~ ^$DATATYPE,abcdef:$OTHERDATATYPE$ ]]
#
#  Any integer number above zero. No front zeroes allowed.
#  The maximum integer for bash is supposed to be 2⁶³-1, which is
#  9223372036854775807. This number has 19 digits, so in this simple check we
#  will assume that our maximal number can have 18 decimal digits.
declare -gr INT='([0-9]|[1-9][0-9]{1,17})'
#
#  Any floating point number, with or without the point, any number
#  of digits after the dot. No front zeroes allowed.
declare -gr FLOAT='([0-9]|[1-9][0-9]{1,17})(\.[0-9]+|)'
#
#  Any space character: space, tab, newline
declare -gr SPACE='[[:space:]]'


__varname_exists() {
	local  varname="${1:-}"
	local  called_from_readrcfile
	local  i

	[ -v "$varname" ] || {
		for ((i=1; i<${FUNCNAME[*]}; i++)); do
			[ "${FUNCNAME[i]}" = read_rcfile ] && called_from_readrcfile=t
		done
		if [ -v called_from_readrcfile ]; then
			err "Config option “$varname” is requried, but it’s missing."
		else
			err "Cannot check variable “$varname” – it doesn’t exist."
		fi
	}

	return 0
}


 # Returns 0, if variable passed by name contains a value, that can be treated
#  as “positive” and returns 1, if the value can be treated as “negative”.
#  Triggers an error, if the value cannot be treated as either.
#
#  $1 – variable name.
#
#  is_bool() works like is_true(), but alters the variable:
#    - if its value equals to “true”, it becomes “t”;
#    - if the value equals to “false”, the variable gets unset.
#  Thus the existence of a variable becomes a boolean flag, which can be
#  checked with a test on whether variable with such a name was declared:
#  e.g.
#    [ -v my_var ] && …
#
#  is_true() is a “proper boolean function” – it returns true or false depen-
#  ding on the argument passed to it for a test. It’s thus convenient in use
#  within test clauses such as
#    is_true "my_var" && …
#  and
#    if is_true "my_var"; then … ; fi
#  is_bool(), on the other hand, is not for testing
#  ways returns
#
is_true() {
	local varname="${1:-}"
	__varname_exists "$varname"
	declare -n varval="$varname"
	if [[ "$varval" =~ ^(y|Y|[Yy]es|1|t|T|[Tt]rue|[Oo]n|[Ee]nable[d])$ ]]; then
		[ ${FUNCNAME[1]} = is_bool ]  \
			&& varval=t
		return 0
	elif [[ "$varval" =~ ^(n|N|[Nn]o|0|f|F|[Ff]alse|[Oo]ff|[Dd]isable[d])$ ]]; then
		if [ ${FUNCNAME[1]} = is_bool ]; then
			unset $varname
			return 0
		else
			return 1
		fi
	else
		err "Variable “$varname” must have a boolean value (0/1, on/off, yes/no…),
		     but it contains “$varval”."
	fi
	return 0
}
export -f  is_true
#
is_bool() { is_true "$@"; }
export -f  is_bool


#  $1 – name of the variable to test.
#
#
is_float()            { __is_number   'float' "$1"; }
is_integer()          { __is_number 'integer' "$1"; }
#  is_signed_float    { __is_number 'signed_float'   "$1"; }
#  is_signed_integer  { __is_number 'signed_integer' "$1"; }
#
#
#  $1 – number type, one of: “integer”, “float”,
#       “signed_integer”, “signed_float”.
#  $2 – name of the variable to test.
#
__is_number() {
	local number_type="$1"  varname="$2"  varval  number_pattern
	__varname_exists "$varname"
	declare -n varval="$varname"
	case $number_type in
		integer)
			number_pattern='^([0-9]|[1-9][0-9]+)$'
			;;
		float)
			number_pattern='^([0-9]|[1-9][0-9]+)(\.[0-9]+|)$'
			;;
	esac

	[[ "$varval" =~ $number_pattern ]]  \
		|| err "Variable “$varname” must be an integer number, but it’s set to “$varval”."

	return 0
}
export -f  __is_number     \
               is_integer  \
               is_float



#  $1 – name of the variable to test.
#  $2 – minimum for the value.
#  $3 – maximum for the value.
#
is_float_in_range()   { __is_number_in_range   'float' "$@"; }
is_integer_in_range() { __is_number_in_range 'integer' "$@"; }
#
#  $1 – number type: either “integer” or “float”.
#  $2 – the name of the variable to test.
#  $3 – minimum for the value.
#  $4 – maximum for the value.
#
__is_number_in_range() {
	local  number_type="$1"
	local  varname="$2"
	local  minval="$3"
	local  maxval="$4"
	local  var_or_opt='variable'
	local  f

	__is_number "$number_type" "$varname"
	declare -n varval="$varname"

	 # If the check is called from the “rcfile” module, where it verifies
	#  the types of configuration file variables, refer to them as to “options”,
	#  otherwise assume a check in the code, where the variables should remain
	#  just “variables”.
	#
	for f in "${FUNCNAME[@]}"; do
		[ "$f" = __postprocess_rc_variables ]  && {
			var_or_opt='option'
			break
		}
	done

	case $number_type in
		integer)
			number_pattern='^([0-9]|[1-9][0-9]+)$'
			;;
		float)
			number_pattern='^([0-9]|[1-9][0-9]+)(\.[0-9]+|)$'
			;;
	esac

	[[ "$minval" =~ $number_pattern  &&  "$maxval" =~ $number_pattern ]]  \
		|| err "The $var_or_opt “$varname” must be in range $minval…$maxval."

	if [ "$number_type" = integer ]; then
		# fast arithmetic
		if (( varval >= minval  &&  varval <= maxval )); then
			return 0
		else
			err "The $var_or_opt “$varname” must be in range $minval…$maxval."
		fi
	else
		# slow bc arithmetic
		case "$(echo "$varval >= $minval && $varval <= $maxval" | bc)" in

			0)  # bc “false”
				err "The $var_or_opt “$varname” must be in range $minval…$maxval."
				;;

			1)  # bc “true”
				return 0
				;;

			*)  # unknown error
				err "Couldn’t verify that the $var_or_opt “$varname”
				     conforms to the required range."
				;;
		esac
	fi
	return 0
}
export -f  __is_number_in_range     \
               is_integer_in_range  \
               is_float_in_range


#  $1 – name of the variable to test.
#  $2 – PCRE-style pattern to match the unit.
#
is_float_with_unit()                 { __is_number_with_unit     'with_unit'   'float' "$@"; }
is_integer_with_unit()               { __is_number_with_unit     'with_unit' 'integer' "$@"; }
is_float_with_unit_or_without_it()   { __is_number_with_unit 'or_without_it'   'float' "$@"; }
is_integer_with_unit_or_without_it() { __is_number_with_unit 'or_without_it' 'integer' "$@"; }
#
#  $1 – name of the variable to test.
#  $2 – minimum for the value.
#  $3 – maximum for the value.
#  $4 – PCRE-style pattern to match the unit.
#
is_float_in_range_with_unit()                 { __is_number_with_unit     'with_unit'   'float' "$@"; }
is_integer_in_range_with_unit()               { __is_number_with_unit     'with_unit' 'integer' "$@"; }
is_float_in_range_with_unit_or_without_it()   { __is_number_with_unit 'or_without_it'   'float' "$@"; }
is_integer_in_range_with_unit_or_without_it() { __is_number_with_unit 'or_without_it' 'integer' "$@"; }
#
#  $1  – possible values: “with_unit”,  “or_without_unit”
#  $2  – number type: either “integer” or “float”
#  $3  – name of the varaible to test
# [$4] – minimal value
# [$5] – maximal value
#  $6  – PCRE-style unit pattern
#
__is_number_with_unit() {
	local  may_be_without_unit
	local  varname
	local  unit_pattern
	local  minval
	local  maxval
	local  varval
	local  proto_number
	local  use_range
	local  errors

	[ "$1" = 'or_without_it' ]  \
		&& may_be_without_unit=t
	number_type="$2"

	if (( $# == 4 )); then
		varname="$3"  unit_pattern="$4"

	elif (( $# == 6 )); then
		varname="$3" minval="$4" maxval="$5" unit_pattern="$6"
		use_range=t

	else
		err "Invalid arguments: “$@”"
	fi

	__varname_exists "$varname"
	declare -n varval=$varname

	[[ "$varval" =~ ^([0-9]+(\.[0-9]+|))\ *($unit_pattern${may_be_without_unit:+|})$ ]]  \
		|| errors=t

	[ -v BASH_REMATCH[1] ]  && {
		proto_number="${BASH_REMATCH[1]}"

		if [ -v use_range ]; then
			(is_${number_type}_in_range "proto_number" "$minval" "$maxval")  \
				|| errors=t
		else
			(is_${number_type} "proto_number")  \
				|| errors=t
		fi
	}

	[ -v errors ]  \
		&& err "Variable $varname must be an integer with${may_be_without_unit:+ or without} unit
		        (unit pattern is “$unit_pattern”), but it was set to “$varval”."
	return 0
}
export -f  __is_number_with_unit                    \
               is_float_with_unit                    \
               is_integer_with_unit                   \
               is_float_with_unit_or_without_it        \
               is_integer_with_unit_or_without_it       \
               is_float_in_range_with_unit               \
               is_integer_in_range_with_unit              \
               is_float_in_range_with_unit_or_without_it   \
               is_integer_in_range_with_unit_or_without_it


is_a_readable_file() { __is_a_readable_file_or_dir  file       "$1";  }
is_a_readable_dir()  { __is_a_readable_file_or_dir  directory  "$1";  }
#
#  Validates a path in RC variable
#  $1  – either “file” or “directory”.
#  $2  – variable name, whose value is to be checked.
#
__is_a_readable_file_or_dir() {
	local  file_or_dir="$1"
	local  varname="$2"

	[[ "$file_or_dir" =~ ^(file|directory)$ ]]  \
		|| err "First argument should be either “file” or “directory”."

	[ -v "$varname" ]  \
		|| err "Second argument must be a variable name, but a variable “$varname” doesn’t exist."
	declare -g $varname

	local f_or_d_key=${file_or_dir:0:1}
	local -n path=$varname

	[ -$f_or_d_key "$path"  -a  -r "$path" ] || {
		redmsg "Variable “$varname” must hold a valid path to a $file_or_dir,
		        but it holds “$path”.
		        This path either doesn’t exist or is not readable."
		err "Wrong path in “$varname”."
	}
	return 0
}
export -f  __is_a_readable_file_or_dir  \
               is_a_readable_file       \
               is_a_readable_dir


is_extfile() {
	[ "$(type -t "$1")" = 'file' ]
}
export -f  is_extfile


return 0