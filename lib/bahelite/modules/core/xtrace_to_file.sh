#  Should be sourced.

#  xtrace_to_file.sh
#  Provides a verbosity channel to redirect
#    xtrace output to a separate file.
#  Author: deterenkelt, 2019–2023
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_XTRACE_TO_FILE_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_XTRACE_TO_FILE_VER='1.0.1'

 # Facility’s verbosity channel
#  0 – the default operational mode
#  1 – verbose mode: do not remove the directory for temporary files on exit.
#
vchan setup  name=xtrace_to_file  \
             level=0      \
             meta=M       \
             hint=output  \
             min_level=0  \
             max_level=1  \
             level_is_locked='L'



show_help_on_verbosity_channel_xtrace() {
	cat <<-EOF

	xtrace_to_file (output channel)
	    Controls the redirection of debugging output (what \`set -x\` enables)
	    from stderr to a log file.

	Levels:
	    0 – the default level, no changes;

	    1 – xtrace output is redirected to its own log file. If the “logging”
	        module is used, then the log file is stored together with the logs.
	        Otherwise, the temporary directory is used, and it’s protected
	        from auto-removal, when program quits.

	EOF
	return 0
}
#  No export: init stage function.


bahelite_place_xtrace_redirect() {
	local  fname="$MYNAME_NOEXT.$MY_LAUNCH_TIME.xtrace"
	local  fpath

	if [ -d "${LOGDIR:-}" -a -w "${LOGDIR:-}" ]; then
		fpath="$LOGDIR/$fname"

	elif [ -d "${TMPDIR:-}" -a -w "${TMPDIR:-}" ]; then
		fpath="$TMPDIR/$fname"
		BAHELITE_DONT_CLEAR_TMPDIR=t

	else
		redmsg "xtrace_to_file: couldn’t create log file: neither LOGDIR nor
		TMPDIR are available for writing to."
	fi

	info "Writing xtrace log to $fpath"
	exec {BASH_XTRACEFD}<>"$fpath"
	VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[xtrace_to_file]='L'

	return 0
}


(( ${VERBOSITY_CHANNELS[xtrace_to_file]} != 0 )) && {
	BAHELITE_POSTLOAD_JOBS+=(
		#  Must be done as a postload job, in order to run after loading
		#  “logging”, which is an optional module.
		bahelite_place_xtrace_redirect
	)
}


return 0