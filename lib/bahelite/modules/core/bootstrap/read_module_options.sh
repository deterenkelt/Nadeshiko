#  Should be sourced.

#  read_module_options.sh
#  Read options specified by the main script, validate them
#    and split for further reading by the modules.
#  Author: deterenkelt, 2020
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_READ_MODULE_OPTIONS_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_READ_MODULE_OPTIONS_VER='1.0'

#  No verbosity channels: the “verbosity_base” module isn’t loaded yet.


bahelite_validate_load_modules_list() {
	local job
	local name='[A-Za-z0-9_]+'    # module name
	local opt='[A-Za-z0-9_-]+'    # option for the module
	local val='[/A-Za-z0-9_.-]+'   # option value

	for job in "${BAHELITE_LOAD_MODULES[@]}"; do
		[[ "$job" =~ ^$name(:$opt(=$val)?(,$opt(=$val)?)*)?$ ]]  || {
			cat <<-EOF  >&2
			Bahelite error: invalid item in BAHELITE_LOAD_MODULES:
			  “$job”.
			The format allows:
			  - “module_name”
			  - “module_name:option”
			  - “module_name:option1=value1,option2=value2,option3”
			Module name: [A-Za-z0-9_]+
			Option: [A-Za-z0-9_-]+
			Option value: [/A-Za-z0-9_.-]+
			EOF
			exit 4
		}
	done
	return 0
}


 # Converts the rules specified by user into a format more usable
#  for processing in the library.
#
#  BAHELITE_LOAD_MODULES is specified in the main script, where it’s given
#  in a user-friendly format – an array of strings. Here we split these
#  strings into module name and options. Options then are split further into
#  an array of their own. BAHELITE_LOAD_MODULES then is converted to an asso-
#  ciative array, where the module name is the key and the value is *the array
#  name, which holds the options for this module*.
#
#  This split is necessary to provide the bootstrap modules with the options
#  (e.g. read “nocolour” for the “bahelite” module).
#
bahelite_convert_load_modules_list() {
	declare -ga BAHELITE_LOAD_MODULES
	local -a old_load_modules=( "${BAHELITE_LOAD_MODULES[@]}" )
	unset BAHELITE_LOAD_MODULES
	declare -gA BAHELITE_LOAD_MODULES

	local  load_spec
	local  module_name
	local  module_opts=()

	for load_spec in "${old_load_modules[@]}"; do
		unset  module_name  module_opts
		[[ "$load_spec" =~ ^([^:]+)(:(.+)|)$ ]] && {
			module_name="${BASH_REMATCH[1]}"
			[ "${BASH_REMATCH[3]}" ]  \
				&& module_opts=( ${BASH_REMATCH[3]//,/ } )

			opt_array_name="BAHELITE_LOAD_MODULES__OPTS_FOR_${module_name^^}"
			declare -ga $opt_array_name
			local -n opt_array=$opt_array_name
			opt_array=( "${module_opts[@]}" )
			BAHELITE_LOAD_MODULES+=( [$module_name]="$opt_array_name" )
		}
	done

	return 0
}


 # Updates TIMEFORMAT variable to a format, that would look native
#  in bahelite modules’ output.
#
update_timeformat() {
	declare -gx TIMEFORMAT="${__mi}  real: %3R  (user %3U,  sys %3S)"$'\n'
	return 0
}
export -f update_timeformat



                     #  Reading module options  #

bahelite_validate_load_modules_list
bahelite_convert_load_modules_list


return 0