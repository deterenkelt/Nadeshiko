#  Should be sourced.

#  checking_core_dependencies.sh
#  Functions to verify, that all external binaries required
#    by the Bahelite core and its modules are present in the
#    system.
#  Author: deterenkelt, 2020–2021
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_CHECKING_CORE_DEPENDENCIES_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_CHECKING_CORE_DEPENDENCIES_VER='1.1.1'

#  No self-report verbosity channel: the module is plain, it isn’t needed.


__show_usage_checking_core_dependencies_module() {
	cat <<-EOF
	Bahelite module “checking_core_dependencies” doesn’t take options!
	EOF
	return 0
}

for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_CHECKING_CORE_DEPENDENCIES[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_checking_core_dependencies_module
		exit 0

	else
		redmsg "Bahelite: checking_core_dependencies: unrecognised option “$opt”."
		__show_usage_checking_core_dependencies_module
		exit 4
	fi
done
unset opt



 # Verifies, that GNU sed, GNU grep, coreutils and util-linux are available
#  for Bahelite and its modules.
#
bahelite_check_core_dependencies() {
	local  do_exit
	local  sed_version
	local  grep_version
	local  getopt_version
	local  yes_version

	if [ "$(type -t sed)" != 'file' ]; then
		redmsg 'Bahelite: sed is not installed.'
		do_exit=t

	elif [ "$(type -t grep)" != 'file' ]; then
		redmsg 'Bahelite: grep is not installed.'
		do_exit=t

	elif [ "$(type -t getopt)" != 'file' ]; then
		redmsg 'Bahelite: util-linux is not installed.'
		do_exit=t

	elif [ "$(type -t yes)" != 'file' ]; then
		redmsg 'Bahelite: coreutils is not installed.'
		do_exit=t
	fi
	#  This is to accumulate messages, so that if more than one utility would be
	#  missing, the messages would appear at once.
	[ -v do_exit ]  \
		&& exit 4  \
		|| unset do_exit

	sed_version=$(sed --version | head -n1)
	grep -q 'GNU sed' <<<"$sed_version" || {
		redmsg 'Bahelite: sed must be GNU sed.'
		do_exit=t
	}
	grep_version=$(grep --version | head -n1)
	grep -q 'GNU grep' <<<"$grep_version" || {
		redmsg 'Bahelite: grep must be GNU grep.'
		do_exit=t
	}
	[ -v do_exit ]  \
		&& exit 4  \
		|| unset do_exit

	#  ex: sed (GNU sed) 4.5
	if [[ "$sed_version" =~ ^sed.*\ ([0-9]+)(\.([0-9]+)|)(\.([0-9]+)|)$ ]]; then

		if	((    ${BASH_REMATCH[1]} <= 3
			   ||
			      (      ${BASH_REMATCH[1]:-0} == 4
			         &&  ${BASH_REMATCH[3]:-0} <= 2
			         &&  ${BASH_REMATCH[5]:-0} <  1
			      )
			))
		then
			redmsg "Bahelite: GNU sed v4.2.1 or higher required."
			exit 4
		fi
	else
		redmsg 'Bahelite: cannot determine sed version.'
		exit 4
	fi

	#  ex: grep (GNU grep) 3.1
	if [[ "$grep_version" =~ ^grep.*\ ([0-9]+)(\.([0-9]+)|)(\.([0-9]+)|)$ ]]; then
		if	((    ${BASH_REMATCH[1]} <= 1
			   || (      ${BASH_REMATCH[1]:-0} == 2
			         &&  ${BASH_REMATCH[3]:-0} <  9
			      )
			))
		then
			redmsg "Bahelite: GNU grep v2.9 or higher required."
			exit 4
		fi
	else
		redmsg 'Bahelite: cannot determine grep version.'
		exit 4
	fi

	#  ex: getopt from util-linux 2.32
	getopt_version="$(getopt --version | sed -n '1p')"
	if [[ "$getopt_version" =~ ^getopt.*util-linux.*\ ([0-9]+)(\.([0-9]+)|)(\.([0-9]+)|)$ ]]; then
		if	((    ${BASH_REMATCH[1]} <= 1
			   || (      ${BASH_REMATCH[1]:-0} == 2
			         &&  ${BASH_REMATCH[3]:-0} <  20
			      )
			))
		then
			redmsg "Bahelite: util-linux v2.20 or higher required."
			exit 4
		fi
	else
		redmsg 'Bahelite: cannot determine util-linux version.'
		exit 4
	fi

	#  ex: yes (GNU coreutils) 8.29
	yes_version="$(yes --version | sed -n '1p')"
	if [[ "$yes_version" =~ ^yes.*coreutils.*\ ([0-9]+)(\.([0-9]+)|)(\.([0-9]+)|)$ ]]; then
		if	((  ${BASH_REMATCH[1]} < 8  ))
		then
			redmsg "Bahelite: coreutils v8.0 or higher required."
			exit 4
		fi
	else
		redmsg 'Bahelite: cannot determine coreutils version.'
		exit 4
	fi

	#  ex: column from util-linux 2.32
	column_version="$(column --version | sed -n '1p')"
	[[ "$column_version" =~ ^column.*util-linux.*$ ]] || {
		#  If column is BSD column, which may happen on Debian, Ubuntu and
		#  Linux Mint, replace column with an alias function, that replaces
		#  the output with a warning, if the arguments would have options
		#  that BSD column cannot handle.
		column() {
			OPTERR=0
			local  opt
			local  column_args=()
			local  unknown_to_bsd

			while  getopts 'c:s:tx' opt; do
				case "$opt" in
					c)  column_args+=( -c "$OPTARG" );;
					s)  column_args+=( -s "$OPTARG" );;
					t)  column_args+=( -t );;
					x)  column_args+=( -x );;
					*)  unknown_to_bsd=t
					    break
				esac
			done

			[ -v unknown_to_bsd ] && {
				milinc
				cat <<-EOF  >&2

				${__mi}${__y}*${__s} There should be a message nicely formatted with “column”,
				${__mi}  but it may be displayed only partly, because the “column”
				${__mi}  binary installed on your system is of unknown origin and
				${__mi}  doesn’t support the necessary options. Installing package
				${__mi}  util-linux may solve the problem.

				EOF
				mildec
			}

			while IFS=$'\n' read; do
				echo "$REPLY"
			done  | command column "${column_args[@]}"

			return $?
		}
	}

	return 0
}



bahelite_check_core_dependencies


return 0