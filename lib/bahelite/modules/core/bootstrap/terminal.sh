#  Should be sourced.

#  terminal.sh
#  Module for everything related to how terminal functions. Defines aliases
#    for common terminal codes (colours, line wipe etc.), and Bash global
#    variables related to the terminal.
#  Author: deterenkelt, 2022
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_TERMINAL_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_TERMINAL_VER='1.4.2'

#  No verbosity channels: the “verbosity_base” module isn’t loaded yet.


__show_usage_terminal_module() {
	cat <<-EOF
	Bahelite module “terminal” options:

	no_colours
	    Forbids the use of colours in messages. The parameter name is accepted
	    with or without ‘u’; with or without plural ‘s’.

	Option names are also accepted with hyphens in place of underscores.
	EOF
	return 0
}

for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_TERMINAL[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_terminal_module
		exit 0

	elif [[ "$opt" =~ ^no[_-]colou?rs?$ ]]; then
		declare -gx NO_COLOURS=t

	else
		echo "Bahelite: terminal: unrecognised option “$opt”." >&2
		__show_usage_terminal_module
		exit 4
	fi
done
unset opt



 # Customised pager for printing the long help output, like a man page.
#  The intended pager is “less”, and the options below are set so that
#    - when the help output would fit onto the screen, the pager would
#      not be used (i.e. the output printed to console like with “cat”).
#    - leave the program’s help output on the terminal when “less” quits.
#  Apparently, not all Linux distributions set PAGER variable, so using it
#  is pointless.
#
#  Example of use within your script:
#     show_help() {
#         $SHOW_HELP_PAGER  >/dev/tty  <<-EOF
#         …
#         …
#         …
#         EOF
#         return 0
#     }
#
declare -gx SHOW_HELP_PAGER

if which less &>/dev/null; then
	SHOW_HELP_PAGER='less -r -M --shift 5 -x4 -F -X'
	unset LESS LESSKEY

elif which more &>/dev/null; then
	SHOW_HELP_PAGER='more'

else
	SHOW_HELP_PAGER='cat'
fi


 # Why tput and not escape sequences?
#  1. Because escape sequences work with “echo -e” and “printf”,
#     but not in heredocs and herestrings.
#  2. tput codes make the copypaste from the logs or console easy:
#     they aren’t copied to buffer, so the logs are copied and pasted
#     as plain text, without escape codes in them.
#

 # When the script is running not in a real terminal emulator, but instead is
#   - double-clicked in a file manager;
#   - or launched with a hotkey by the window manager;
#   - or is running as a scheduled (cron) job;
#  the TERM variable is set to the default kernel’s terminal type “linux”.
#  This terminal is able to handle colours, but if the output is then
#  closed/redirected to a log file, there will appear “^O” nearly every-
#  where, where terminal capabilities are used (not only colours, but also
#  standout codes and sgr0, which makes the issue related to both colour-
#  enabled and colour-disabled scripts). Such logs are hard to read and
#  problematic to copypaste to services like pastebin.
#
#  To avoid this issue, Bahelite changes TERM to another type, “linux-basic”.
#  Both terminals are part of ncurses terminfo database, and typically are
#  always installed. But some Linux distributions (Debian) split ncurses into
#  several packages, and “linux-basic” gets into a separate package from
#  “linux”  (e.g. into “ncurses-term” package, while “linux” remains
#  in “ncurses-base”).
#
[ "$TERM" = linux ] && {
	#  toe is part of ncurses. It lists available terminal types.
	[ "$(type -t toe)" = 'file' ] && {
		while read; do
			[[ "$REPLY" =~ ^linux-basic$'\t' ]] && {
				declare -gx TERM='linux-basic'
				break
			}
		done < <(toe -a)  #  Dependency checks are yet to run,
		                  #  so cannot rely on grep.
	}
}


 # If set, this variable indicates, that the main script is running in a shell
#  that is running in a terminal window (thus it may receive keyboard input).
#  If it isn’t set, the script is probably running in a bare shell (typical
#  for script launched from the environment).
#
declare -gx CONNECTED_TO_TERMINAL


 # This hook must be used before Bahelite duplicates outputs and sends them
#  to a pipe (to do logging). Pipe’ified file descriptors will not be as dis-
#  cernible as regular ones, so this is better checked while the things are
#  still simple.
#
[ -t 0  -a  -t 1 ]  \
	&& CONNECTED_TO_TERMINAL=t  # meaning the terminal is interactive.

which tput &>/dev/null || {
	cat <<-EOF >&2
	Bahelite error: “tput” is required by the module “terminal”.
	“tput” is a part of ncurses: https://www.gnu.org/software/ncurses/
	EOF
	exit 4
}

 # Foreground colours
#
if [ -v NO_COLOURS ]; then
	declare -grx __k=''
	declare -grx __bla=${__k}
	declare -grx __blk=${__k}
	declare -grx __black=${__k}

	declare -grx __r=''
	declare -grx __red=${__r}

	declare -grx __g=''
	declare -grx __gre=${__g}
	declare -grx __grn=${__g}
	declare -grx __green=${__g}

	declare -grx __y=''
	declare -grx __yel=${__y}
	declare -grx __ylw=${__y}
	declare -grx __yellow=${__y}

	declare -grx __b=''
	declare -grx __blu=${__b}
	declare -grx __blue=${__b}

	declare -grx __m=''
	declare -grx __mag=${__m}
	declare -grx __mgn=${__m}
	declare -grx __mgt=${__m}
	declare -grx __magenta=${__m}

	declare -grx __c=''
	declare -grx __cya=${__c}
	declare -grx __cyn=${__c}
	declare -grx __cyan=${__c}

	declare -grx __w=''
	declare -grx __whi=${__w}
	declare -grx __wht=${__w}
	declare -grx __white=${__w}

else
	#  When colours are allowed
	declare -grx __k=$(tput setaf 0)
	declare -grx __bla=${__k}
	declare -grx __blk=${__k}
	declare -grx __black=${__k}

	declare -grx __r=$(tput setaf 1)
	declare -grx __red=${__r}

	declare -grx __g=$(tput setaf 2)
	declare -grx __gre=${__g}
	declare -grx __grn=${__g}
	declare -grx __green=${__g}

	declare -grx __y=$(tput setaf 3)
	declare -grx __yel=${__y}
	declare -grx __ylw=${__y}
	declare -grx __yellow=${__y}

	declare -grx __b=$(tput setaf 4)
	declare -grx __blu=${__b}
	declare -grx __blue=${__b}

	declare -grx __m=$(tput setaf 5)
	declare -grx __mag=${__m}
	declare -grx __mgn=${__m}
	declare -grx __mgt=${__m}
	declare -grx __magenta=${__m}

	declare -grx __c=$(tput setaf 6)
	declare -grx __cya=${__c}
	declare -grx __cyn=${__c}
	declare -grx __cyan=${__c}

	declare -grx __w=$(tput setaf 7)
	declare -grx __whi=${__w}
	declare -grx __wht=${__w}
	declare -grx __white=${__w}

	# declare -grx TERM_COLORS=$(tput colors)
fi


                     #  Other control sequences  #

 # Some capabilities vary from terminal to terminal. If appearance is not
#  guaranteed, there is a note in the description below.
#

 # Stop, \e[0m
#  Turns off all style alternations.
#
declare -grx __s=$(tput sgr0)
declare -grx __sto=${__s}
declare -grx __stp=${__s}
declare -grx __stop=${__s}

declare -grx __o=${__s}
declare -grx __off=${__s}

declare -grx __rst=${__s}
declare -grx __reset=${__s}

declare -grx __sgr0=${__s}


 # Bold/bright mode, \e[1m
#  In terminals it makes text use either a brighter foreground colour or
#  a bold font face. What will it looks like depends on the default and user
#  settings for a particular terminal. Can be reset only with sgr0 code.
#
declare -grx __bo=$(tput bold)
#
declare -grx __bol=${__bo}
declare -grx __bld=${__bo}
declare -grx __bold=${__bo}

declare -grx __bri=${__bo}
declare -grx __brt=${__bo}
declare -grx __bright=${__bo}


 # Standout mode
#  Can be used to emphasize a particular part in the text, but the actual
#  look is undefined: it may be italic face or inverted fg/bg colours.
#  To the merits of this mode, there’s a code to turn it off.
#
declare -grx __so=$(tput smso)
declare -grx __standout=${__so}
#
#  Turning off standout mode works /only/ for standout mode – it will not
#  remove bright/blink/invis/underline.
#
declare -grx __so_off=$(tput rmso)
declare -grx __so_rst=${__so_off}
declare -grx __standout_off=${__so_off}
declare -grx __standout_reset=${__so_off}


 # Italic mode
#  The italic look is not guaranteed: may look like inverted fg/bg in some
#  terminals.
#
# declare -grx __it=$(tput sitm)
# declare -grx __italic=${__it}
#
#  Italic off
# declare -grx __it_off=$(tput ritm)
# declare -grx __it_rst=${__it_off}
# declare -grx __italic_off=${__it_off}
# declare -grx __italic_reset=${__it_off}


 # Dimmed colour, \e[2m
#  If bright/bold mode chooses the bright version of the current colour,
#  this mode vice versa chooses the hushed tone.
#
declare -grx __di=$(tput dim)
declare -grx __dim=${__di}


 # Blink, \e[3m
#  Unless you know that it will work, do not use it. It’s commonly forbidden
#  in terminal settings. Can be reset only with sgr0.
#
# declare -grx __bl=$(tput blink)
# declare -grx __bli=${__bl}
# declare -grx __bln=${__bl}
# declare -grx __blink=${__bl}


 # Underline, \e[4m
#
declare -grx __ul=$(tput smul)
declare -grx __und=${__ul}
declare -grx __underline=${__ul}
#
#  Underline off
declare -grx __ul_off=$(tput rmul)
declare -grx __ud_rst=${__ul_off}
declare -grx __und_off=${__ul_off}
declare -grx __und_rst=${__ul_off}
declare -grx __underline_off=${__ul_off}
declare -grx __underline_reset=${__ul_off}


 # Inverted fg/bg, \e[7m
#  Can be reset only with sgr0
#
declare -grx __re=$(tput rev)
declare -grx __rev=${__re}
declare -grx __reverse_fg_bg=${__re}


 # Hidden text, \e[8m
#  Aka secure mode. Should make text appear as invisible (empty space) on the
#  screen. Appearance is not guaranteed. There are similar options “prot”
#  (protected) and “sshm” (shadowed), but neither of them guarantee, that the
#  text will be hidden. You have a better chance with setting both foreground
#  and background colours to one colour, for example:
#      $ echo "$(tput setaf 0; tput setab 0)completely hidden text"
#
# declare -grx __hi=$(tput invis)
# declare -grx __hid=${__hi}
# declare -grx __hidden=${__hi}
# declare -grx __inv=${__hi}
# declare -grx __invisible=${__hi}


#  Sequences that reset style and colour
#declare -grx __bri_rst='\e[21m'  __bright_reset='\e[21m'  # reset bold/bright,
#declare -grx __brt_rst='\e[21m'
#declare -grx __bol_rst='\e[21m'  __bold_reset='\e[21m'
#declare -grx __bld_rst='\e[21m'
#declare -grx __fg_rst='\e[39m'  __fg_reset='\e[39m'  # reset fg to its default



                #  Settings for progressbar-like things  #

#  Set cursor invisible
# declare -grx __cursor_invisible=$(tput civis)
#
#  Set cursor very visible
# declare -grx __cursor_visible=$(tput cvvis)
#
#  Reset cursor to its normal state.
#  Recommended to undo civis.
# declare -grx __cursor_normal=$(tput cnorm)
# declare -grx __cursor_reset=${__cursor_normal}

#  Wipe the current line and move cursor to the beginning, \r\e[K
declare -grx __cl=$(tput el; tput cr)
declare -grx __clearline=${__cl}
declare -grx __wi=${__cl}
declare -grx __wipe=${__cl}


which ansifilter &>/dev/null  \
	&& declare -grx HAVE_ANSIFILTER=t


 # Strip colours from the string or a file
#
#  Useful for when a message should go somewhere where terminal control
#  sequences wouldn’t be recognised, or to parse
#
strip_colors()  { __strip_colours "$@"; }
strip_colours() { __strip_colours "$@"; }
strip_colors_from_file()  { __strip_colours "$@"; }
strip_colours_from_file() { __strip_colours "$@"; }
__strip_colours() {
	local  str_or_file="${1:-}"
	local  operate_on_a_file

	 # If the locale is an UTF-*, then ansifilter must be told to ignore
	#  unicode control characters: otherwise it will strip any non-ANSI
	#  characters and some ANSI will be caught in the wiping.
	#
	local  ignore_csi='--ignore-csi'
	[[ $LANG =~ \.utf- ]] || unset ignore_csi

	[[ "${FUNCNAME[1]}" =~ _from_file$ ]]  \
		&& operate_on_a_file=t

	if [ -v HAVE_ANSIFILTER ]; then
		if [ -v operate_on_a_file ]; then
			ansifilter ${ignore_csi:-} "$str_or_file"
		else
			ansifilter ${ignore_csi:-} <<<${str_or_file@E}
		fi
	else
		if [ -v operate_on_a_file ]; then
			sed -r 's/\x1B(\[[0-9;]*[GJKmsu]|\(B)//g' "$str_or_file"
		else
			#  Thanks be to https://stackoverflow.com/a/52781213
			sed -r 's/\x1B(\[[0-9;]*[GJKmsu]|\(B)//g' <<<${str_or_file@E}
		fi
	fi

	return 0
}
export -f __strip_colours    \
          strip_colours         \
          strip_colors            \
          strip_colours_from_file  \
          strip_colors_from_file


 # Dealing with COLUMNS and LINES
#  1. The original names are confusing, so another pair of names,
#     with a “TERM_” prefix is used.
#  2. COLUMNS and LINES are not set in non-interactive terminals, so
#     they are not reliable.
#
if [ -v TERM_COLUMNS  -a  -v TERM_LINES ]; then
	#  When forced by user or set by mother script that also runs Bahelite
	declare -x TERM_COLUMNS
	declare -x TERM_LINES

elif [ -v COLUMNS  -a  -v LINES ]; then
	#  When set for the first time
	declare -nx TERM_COLUMNS=COLUMNS
	declare -nx TERM_LINES=LINES
	declare -x  COLUMNS
	declare -x  LINES

else
	#  If it wasn’t possible to determine, because of a weird terminal
	#  (e.g. custom framebuffer, broken ssh(d) settings or when it’s
	#  a weird physical terminal), use the default.
	#
	declare -x TERM_COLUMNS=80
	declare -x TERM_LINES=25
fi


return 0