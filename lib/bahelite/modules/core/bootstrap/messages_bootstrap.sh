#  Should be sourced.

#  messages_bootstrap.sh
#  Crude implementation of the “messages” module for the bootstrap stage.
#    It’s here to provide uniform output during the module loading stage,
#    and so that modules might have the same simple code and not worry about
#    implementation, whether something is already accessible or not yet.
#  Author: deterenkelt, 2020
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MESSAGES_BOOTSTRAP_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MESSAGES_BOOTSTRAP_VER='1.0'

#  No verbosity channels: the “verbosity_base” module isn’t loaded yet.
#  Option processing is done in the “messages” module.



info() {
	[ -v NO_COLOURS ]  \
		&& echo -e "${__mi:-}$*" \
		|| echo -e "${__mi:-}${__gre}*${__s} $*"
	return 0
}

warn() {
	[ -v NO_COLOURS ]  \
		&& echo -e "${__mi:-}Bahelite warning: $*"  >&2  \
		|| echo -e "${__mi:-}${__yel}*${__s} $*"  >&2
	return 0
}


redmsg() {
	[ -v NO_COLOURS ]  \
		&& echo -e "${__mi:-}Bahelite error: $*"  >&2  \
		|| echo -e "${__mi:-}${__red}*${__s} $*"  >&2
	return 0
}
err() {
	redmsg "$*"
	exit 5
}
err-stack() {
	print_call_stack
	redmsg "$*"
	exit 5
}


plainmsg() { msg "$*"; }
msg(){
	echo -e "${__mi:-}  $*"
	return 0
}


print_call_stack() {
	declare -p FUNCNAME  BASH_SOURCE  BASH_LINENO >/proc/$$/fd/2
	return 0
}


return 0