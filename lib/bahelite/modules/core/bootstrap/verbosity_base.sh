#  Should be sourced.

#  verbosity_base.sh
#  Provides basic functionality for the bootstrap stage.
#    To be extended by the “verbosity” module.
#  Author: deterenkelt, 2021–2024
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_VERBOSITY_BASE_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_VERBOSITY_BASE_VER='2.2.4'

#  Verbosity channels are defined below function definitions.


__show_usage_verbosity_module() {
	cat <<-EOF
	Bahelite module “verbosity” doesn’t take options!
	To output more information about the processing that is done in the module
	“verbosity”, you may raise the level of its verbosity channel, “Verbosity”,
	as shown below.

	    $ VERBOSITY=Confdir=3 ./$MYNAME

	Alternatively, use the command line option, for example as

	    $ ./$MYNAME --verbosity=Rcfile=3

	To get more information about the usage of verbosity channels and setting
	levels, run the script with --verbosity-help.
	EOF
	return 0
}

for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_VERBOSITY[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_verbosity_module
		exit 0

	else
		redmsg "Bahelite: verbosity: unrecognised option “$opt”."
		__show_usage_verbosity_module
		exit 4
	fi
done
unset opt


                        #  Verbosity channels  #

 # The variables below are the foundation of the verbosity system. At the
#  bootstrap stage only a few are used, other come useful once the full-
#  fledged version of this module is loaded, i.e. “core/verbosity.sh”.
#
#  Verbosity setup and further management is performed via the aggregate
#  function “vchan()”, whose bootstrap version provides only a basic func-
#  tionaly, but the full version in verbosity.sh provides all the necessary
#  help information.
#
#  See also the notes in commentaries at the bottom of verbosity.sh
#


 # The list of verbosity channels and their levels.
#  Level is a number between 0 and 9. See also VERBOSITY_CHANNELS_LEVEL_IS_LOCKED
#    and VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS below.
#  Format: [channel_name]=level
#
declare -gAx VERBOSITY_CHANNELS=()


 # The “meta” attribute defines whether the channel name is an abstract name
#  (like “Rcfile”), or it should match a certain function’s name in the code.
#
#  Abstract names are convenient for naming groups handling entire subsystems
#  within programs: this way a verbosity channel may have a comprehensive
#  name. Moreover, it may be used as a group name, but in an “iside out” way,
#  when all functions within a subsystem use this channel name hardcoded in
#  them for checks on verbosity. This is a suitable way, when using groups
#  is not an option. Bahelite modules use this method, because of dependen-
#  cies between modules (so a function from facility A may call another func-
#  tion from facility B, and though the output should be controlled by the
#  verbosity channel of A, B, being deeper in the call stack, would over-
#  ride that). For separated subsystems within the main script, nothing
#  should prevent usage of ordinary groups (that quite often would have
#  an abstract name.) Groups “inside out” is a way to speed up the loading
#  process, as using function calls may be costly, when these calls are
#  made often.
#
#  Names matching the function name are handy for taking a part of code and
#  separating its output’s verbosity from the common stream. It’s conveni-
#  ent for smaller subsystems (think of one function and several helpers,
#  used internally). All the calls within a function with a defined verbo-
#  sity channel named after it would retain belonging to that verbosity
#  channel (except when another function deeper in the call stack would
#  have a channel of its own).
#
#  Format: [channel_name]='M' (or any string), if the channel is
#  a meta-channel, or unset, if it isn’t.
#
declare -gAx VERBOSITY_CHANNELS_IS_META=()


 # Contains list of slave channels for every channel having the “group”
#  attribute. Slave channels will use their parent channel’s verbosity level.
#
#  Format: [channel_name]='slave_ch1,slave_ch2…,slave_chN'
#
declare -gAx VERBOSITY_CHANNELS_GROUP_ITEMS=()


 # The attribute defines, whether it’s possible to change the level
#  of a verbosity channel.
#
#  For some channels setting a level is possible only once – i.e. when
#  disabling console output, this action is irreversible.
#
#  Format: [channel_level]='L' (or any string) if the channel’s level is
#    locked, or unset, if the level can be freely changed.
#
declare -gAx VERBOSITY_CHANNELS_LEVEL_IS_LOCKED=()


 # Hints exist primarily to separate channels into convenient groups
#  for the “vchan list” command.
#
#  All Bahelite’s channels have hints, except the channel “main”, which is
#  left aside to be together with the main script channels (i.e. with the
#  channels created in the program, that included Bahelite in its source),
#  as “main” is the primary channel for controlling the verbosity “in general”.
#
#  There are several formats:
#
#    - [channel_name]='output' – for those gating an output. Currently that’s
#      console, log, desktop, xtrace_to_file, bahelite.
#
#    - [channel_name]='self-report' – for self-report channels of the Bahelite
#      modules;
#
#    - [channel_name]='?' for unknown channels. As you should know, verbosity
#      levels can be controlled via command line, and Bahelite processes them
#      at the bootstrap stage. But at this time only a few channels exist,
#      and some of the specified channels may refer to channels which belong
#      to the main script, and will appear only after Bahelite finishes its
#      startup jobs. Now, in the case when the user have mistyped the channel
#      name, how Bahelite should tell them that the name is unknown? That’s
#      where the “suspicious” mark comes handy. Every channel, that is created
#      at bootstrap stage to store the setting, is given the ‘?’ hint to mark
#      it as suspicious. Then as “vchan setup” would one by one set up verbo-
#      sity channels, it would take off the ‘?’ hint, by either replacing it
#      with “BO”, “BSR” or by unsetting it. (Remains valid for the channels
#      created by the main script after Bahelite finishes its work.) Suspi-
#      cious channels – if found – will be shown in the output of “vchan list”
#      at the bottom, helping the user to notice, that they’re trying to con-
#      trol a non-existing channel;
#
#    - a non-existing entry means there’s no hint for that channel, obviously.
#
declare -gAx VERBOSITY_CHANNELS_HINTS=()


 # Which level should newly created verbosity channels use, if the level
#  isn’t specified at the creation time. Defaults to 3, as level 3 provides
#  the optimal message verbosity for the channel “main”, and the levels
#  for other channels are aligned according to the meanings, which those
#  levels have for the channel “main”.
#
declare -gx VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS=3


 # There are three channel types:
#  - “stand-alone”;
#  - “group”;
#  - and “channel-in-a-group”.
#  Channel type is set – and, if necessary, changed – automatically,
#  when “vchan()” performs operations.
#
declare -gAx VERBOSITY_CHANNELS_TYPES=()


 # This is the sibling to VERBOSITY_CHANNELS_GROUP_ITEMS, but this array
#  is for the group members. This way search can go easily from top to bottom
#  (from group leader to its members) and from bottom to top (querying a group
#  member, we can retrieve the name of the channel that is the group leader).
#
declare -gAx VERBOSITY_CHANNELS_GROUP_LEADERS=()


 # Boundaries for minimum and maximum levels used by a channel.
#  They are supposed to be specified at the channel creation time, to help
#  the vchan routines correct the set level to the maximum (or the minimum)
#  value, shall it exceed the bounds.
#
declare -gAx VERBOSITY_CHANNELS_MIN_LEVELS=()
declare -gAx VERBOSITY_CHANNELS_MAX_LEVELS=()


 # When set, prints the information about known verbosity channels. Two common
#  ways to set it are raising the “Verbosity” channel level to and passing
#  --verbosity-list-channels to the main script.
#
declare -g BAHELITE_VERBOSITY_LIST_CHANNELS


 # As the bootstrap version of vchan() operates only with pairs like
#  “channel name” – “level”, it stores the (full) requested definition
#  in the temporary function, which will be executed by the core verbosity
#  module, when it loads.
#
#  The thing is, modules that are loaded between “verbosity_base” and
#  “verbosity” may request channels, that have to be operated, while
#  the facility controlling verbosity is itself only in bootstrap stage.
#  Thankfully, nothing but levels is needed at this stage.
#
declare -ga VERBOSITY_POSTPONED_SETUP_FUNCS=()


 # The set of channels and levels, which is applied when the --quiet parameter
#  is set. It’s made into a variable, so that the main script could describe
#  --quiet in its own help with a direct reference to Bahelite setting, inde-
#  pendently of how this set may change in the future.
#
declare -gr VERBOSITY_QUIET_SET='log=0,console=0,desktop=0,bahelite=0,main=0'



show_help_on_verbosity() {
	#
	#  As this would probably be the first help message a user sees (in regards
	#  to verbosity), let’s not delve into the differences between Bahelite’s
	#  and main script’s own channels.
	#
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF

	                             VERBOSITY CHANNELS

	    By tuning the levels of verbosity channels, one can make this program
	    more quiet or verbose.

	    ${__blu}Output${__s} channels control not messages, but the means by which they are
	    displayed: stdout and stderr, log file, desktop notifications, xtrace
	    output etc. The low-level stuff.

	    ${__blu}Self-report${__s} channels describe, what a particular subsystem does at an
	    init stage. They’re useful for debugging launch problems, like finding
	    directory paths and such.

	    ${MY_DISPLAY_NAME:-$MYNAME}’s ${__blu}own channels${__s} are, before all, “main”, which is the generic
	    channel, that encompasses all output messages, that don’t belong to any
	    specific channel, and those very specific channels, introduced for
	    ${MY_DISPLAY_NAME:-$MYNAME}’s subsystems. Which information there is to report might
	    be known by querying a particular subsystem.


	                                  Levels

	    Each channel has an assigned ${__blu}level${__s}. The higher is the level, the more
	    messages it sends. At level 0 every channel is quiet. The default level
	    is 3, which corresponds to having errors (enabled on lv. 1), warnings
	    (added by lv. 2) and informational messages, your regular console out-
	    put (which makes it 3). Level 4 and above add more messages, than you
	    normally see. In general, all channels align to this rule (except the
	    low-level ones).


	                                 Examples

	    List verbosity channels

	        \$ ./$MYNAME --verbosity-list-channels

	    Show help for a particular channel, e.g. Rcfile

	        \$ ./$MYNAME --verbosity-help-Rcfile

	    To raise verbosity level for a channel, e.g. “Confdir”, use

	        \$ ./$MYNAME --verbosity Confdir=3

	    Multiple channels may be specified via comma:

	        \$ ./$MYNAME --verbosity main=4,Rcfile=3

	    Follow the capitalisation shown in the channel list to avoid typos.
	    The list will show mistyped channels (if there would be any) as “Unknown”
	    at the bottom.

	    More on verbosity channels in the output of --verbosity-morehelp
	EOF

	return 0
}


show_more_help_on_verbosity() {
	#
	#  Here we can be more elaborate on the types, on the library vs the main
	#  script and add some hints for the developers.
	#
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF

	                        VERBOSITY CHANNELS. DETAILS

	                        What a verbosity channel is

	    A string with a number – the name and the level. Regular channels are
	    matched against function names, the search algorithm walks the call
	    stack and if a function name would happen to match with an existing
	    verbosity channel name, all err(), warn(), info(), info-4() and so on
	    would display their messages if the verbosity level allows them to.
	    One channels may override another depending on their position in the
	    call stack. The farthermost from the beginning (closest to the execution
	    point) take priority over those closer to “main” (and the “main” chan-
	    nel itself). The function “main” is created by Bash and holds the top-
	    most position for every program that runs on Bash. Hence the channel
	    “main” is created to span over the entire program and control verbosity
	    where no specialised channels exist.


	                                  Groups

	    Groups are created, when one channel is subjugated to another, thus copy-
	    ing its level (or “mirroring” it). While the automated name matching
	    helps to separate entire branches of calls and works for depth, the groups
	    are what stretches the power of a verbosity channel flat, picking several
	    functions on the same level at once, each making its own branch of calls,
	    that will be now controlled by the verbosity level of the group’s master
	    channel. Group items are not controllable entities, one must control the
	    master channel to set the level.


	                                   Meta

	    So far it was about regular channels, that match function names. For a
	    channel that corresponds to a module, a name similar to the name of the
	    module is more convenient, as this helps to find the channel of a par-
	    ticular subsystem faster. Channels, whose name isn’t intended to match
	    any function’s name are called “meta-channels” and have a corresponding
	    attribute set.


	                                   Tips

	    The option --verbosity takes an argument, which may be specified sepa-
	    rately (without the equals sign), so these commands are equivalent:

	        \$ ./$MYNAME --verbosity Rcfile=3,main=4
	        \$ ./$MYNAME --verbosity=Verbosity=3,main=4
	        \$ ./$MYNAME --verbosity "Rcfile=3,main=4"
	        \$ ./$MYNAME --verbosity 'Rcfile=3,main4'

	    Verbosity channels may be specified as with the command line option
	    --verbosity, as with the environment variable VERBOSITY, so the fol-
	    lowing two commands are also equivalent:

	        \$ ./$MYNAME --verbosity Confdir=3
	        \$ VERBOSITY="Confdir=3" ./$MYNAME


	                            For the developers

	    1. See the output of --verbosity-devhelp
	    2. The creation of verbosity channels and altering them is performed
	       with calls to the “vchan” function. To output the call syntax, use
	       “vchan help” command inside the program.

	EOF

	return 0
}


show_developer_help_on_verbosity() {
	$SHOW_HELP_PAGER  >/dev/tty  <<-outer_EOF

	                VERBOSITY CHANNELS. INFORMATION FOR DEVELOPERS

	    ***
	    “vchan help” placed in the code will show syntax for “vchan” commands.
	    “vchan help-COMMAND” displays help for a particular COMMAND.


	    ***
	    If using the command line argument --verbosity leads to an error,
	    which you’d want to debug, the environment variable VERBOSITY may
	    help you circumvent the error. (For errors related to setting verbo-
	    sity levels via command line or the environment.)


	    ***
	    — The internal verbosity channels have their descriptions, but mine
	    do not! How do I make descriptions for my channels?
	    — To create a description, one must do two things:

	      1. Define a function, that would show the description on console
	      2. Update the argument parser (in *your* program, i.e. in the main
	         script) to respond to --verbosity-help-CHANNELNAME parameter.

	      Below is an example of a description function. The function name
	      *must* start as shown below.

	          show_help_on_verbosity_channel_CHANNELNAME() {
	              cat <<-EOF
	              CHANNELNAME verbosity channel

	              The levels:
	                  0 – no output;
	                  1 – ………………;
	                  2 – …………… ……… ………;
	                  3 – ………… ………… ………… ……;
	                  4 – …… ……… ………;
	                  5 – ………… ………… ……… …….
	              EOF
	          }

	      Note, that in order for the “cat <<-EOF” command to work properly,
	      the indentation must be in tabs. At least up to the level of the
	      “cat” command.

	      Now to add a clause in the option parser of the main script. For simpli-
	      city, this example assumes, that the arguments are parsed in a “for”
	      loop, and that each command line parameter is tested in an “if” (or
	      “elif”) clause. Let’s assume also, that the name of the newly intro-
	      duced channel name is in “ch_name” variable, and the name of the cor-
	      responding function, which prints the description of that channel, is
	      put in the “help_func” variable. (These two variables are introduced
	      for convenience of reading the example, and you probably won’t need
	      those in the real code.)

	          local  ch_name
	          local  help_func

	          …

	          for arg in "\$@"; do
	              if … then
	                  ……………………

	              elif …
	                  ……………………

	              elif [[ "\$arg" =~ ^--verbosity-help-([A-Za-z_][A-Za-z0-9_-]*)$ ]]; then
	                  ch_name=\${BASH_REMATCH[1]}
	                  help_func="show_help_on_verbosity_channel_\$ch_name"
	                  if     [ -v VERBOSITY_CHANNELS[\$ch_name]        ]  \\
	                      && [ "\$(type -t "\$help_func")" = 'function' ]
	                  then
	                      \$help_func
	                      exit 0
	                  else
	                      err "No such verbosity channel: “\$ch_name”."
	                  fi

	              else
	                  err "Parameter unrecognised: “\$arg”."

	              fi
	          done


	    ***
	    On the hierarchy of channels and the structure of Verbosity system

	    The verbosity system is meant to be simple, so that its maintenance
	    wouldn’t become a burden. This lead to it having somewhat crude parts.

	    Channels are meant to be created once with “vchan setup” and then alte-
	    red only in the few necessary circumstances. “vchan setup” isn’t made
	    to “redefine” or “alternate” a channel once it’s created. If alernation
	    is possible, there’s a separate vchan command for that.

	    The intended alternations are:

	        - the only exception – the update from the bootstrap stage. During
	          this stage, modules operate with a simple version of “vchan setup”,
	          which creates a channel with a name and sets a level to it. The
	          bootstrap vchan passes the full definition of each channel (that it
	          could only partially implement) in a “reminder” function, which is
	          processed by the full-fledged vchan;

	        - one way of adding channels to a group is to specify them at the
	          time of the channel creation. It’s the preferred way. Another way
	          is using “vchan addtogroup”, which modifies an already existing
	          channel, adding to it group items (of its master channel) and
	          changing the channel type as necessary;

	        - changing verbosity level with “vchan setlevel”.

	    As for the group items, while they are nominally present as channels of
	    a certain type, they are actually more like attributes to a real channel.
	    This is why they aren’t listed as individual channels: they’re comple-
	    tely dependent on the master channel. Then why do they occupy the same
	    arrays as the independent channels, and do they do it the same way
	    (sans having less attributes)? – the answer is: it’s easier to loop up
	    channel names this way. (Looking up is performed when a function like
	    “err” or “info-4” queries the currrent verbosity level to determine,
	    whether sending output is allowed.)

	    An attempt to control group’s master channel via one of its group items
	    will trigger an error.

	    To catch user input errors related to setting levels via --verbosity,
	    all channels specified this way (i.e. specified at startup) are given
	    the ‘?’ hint, which marks them as suspicious (as at the startup time
	    they are technically just their names yet – the main script’s modules,
	    where these channels are defined properly, weren’t loaded yet). Once
	    a “vchan setup” command would claim a channel name and set up the chan-
	    nel, it will also take off the ‘?’ hint. Thus when it will be the time
	    to show the list of channels (requested with --verbosity-list-channels),
	    all the channels should be set up, and only the unclaimed will still
	    have the ‘?’ hint. Presence of such channels will trigger a special
	    section “Unknown channels” to appear in the list of channels.

	    There’s no way of changing channel name or the “meta” attribute, or set-
	    ting a new list of group items to replace the old one. To “re-setup”
	    a channel one must delete one first with “vchan delete” and then create
	    it anew with “vchan setup”. Though the situation when this might be
	    needed, is hard to imagine.


	    ***
	    The most handy way vs the fastest way
	    of introducing verbosity levels to a program

	    The “handy way” is when you let your channels work like “main”, i.e.
	    having their names match function names. Then normal err(), warn(),
	    info(), info-4(), info-5()… will obey the custom channels and look up
	    to their levels at runtime.

	    So, all that’s needed to employ verbosity channels, is to set a couple
	    of them and then just use the common message functions like those that
	    were mentioned above. Yes, it’s that easy. (This whole thing was meant
	    to be simple, remember? And this usage was, in fact, one of the the end
	    goals.)

	    As for the “fastest way”… Things are more difficult here. The “fastness”
	    here refers not to the speed with which you can set up channels, but the
	    speed, with which the active verbosity level can be retrieved at runtime.
	    Thus, we’re speaking about mitigating latencies here. The code in func-
	    tions like “info”, “info-4” etc. ends up making calls to “vchan iflevel”,
	    “vchan minlevel”, “vchan getname”, “vchan getlevel”. Within the code
	    of the messaging and verbosity subsystems they are used quite often.
	    For the main script code, the fastest way would be to query the verbosity
	    level directly once:

	        local  v_level=\${VERBOSITY_CHANNELS[ <channel_name> ]}

	    and then each time use an arithmetic comparison, employing the regular
	    message functions info(), warn(), err():

	        (( v_level >= 3 ))  \\
	            && info "… … … … … "

	    Mind, however, that info() etc. will still internally look up the current
	    verbosity channel. Meaning that for the “info”-class message function
	    the level of verbosity, which permits output, starts at 3 and can’t be
	    lower, no wrapping can dictate to function’s internals how to behave 
	    (in case it might seem to you, that it will).

	    After the last rework (Bahelite 5.2.0) the reliance of messaging func-
	    tions on vchan has been lifted up, so the speed penalty shouldn’t be
	    as big as it was before (in Bahelite 5.0 and below).


	    ***
	    To let xtrace delve into Bahelite code, raise the level of “bahelite”
	    verbosity channel to 4.

	    vchan setlevel bahelite 4
	    functrace_off
	    set -x
	    …
	    <your code here>
	    …
	    set +x
	    functrace_rewind


	    ***
	    To leave \$TMPDIR intact on exist (prevent it from being wiped), use
	    --verbosity tmpdir=1


	    ***
	    A note about recursion errors, which should concern those seeking
	    to replace, redefine or upgrade some of Bahelite facilities

	    The modules “verbosity”, “messages” and “message_indentation” must
	    be dependent on each other as less as possible. These three may trig-
	    ger recursion errors, especially on raised verbosity levels. Currently
	    this is solved by using more “low-level” approaches, when it comes
	    to verbosity (which also granted more speed for Bahelite as a whole),
	    and untied modules from dependencies at the places which were detected
	    as capable of triggering a recursion error. The hooks and dummy decla-
	    rations employed are described in the code as such and shouldn’t be
	    removed.


	    ***
	    On the “main” function

	    Bash creates function “main”, which holds the entire code, that the
	    interpreter then runs as a program. And the verbosity system relies
	    on this to certain extent in order to work properly. A user-created
	    function main will limit the scope for the Verbosity system and may
	    portrait it as incapable.

	    There’s no reason to create another function “main” when there’s
	    already one, Bash isn’t C, which required a programmer to create
	    this function.

	outer_EOF

	return 0
}


 # Validates verbosity string specified via VERBOSITY environment variable
#  or via the command line option --verbosity.
#
#  $1 – a string specifying channels and the levels they should use.
#       The format is “channel1=levelA[,channel2=levelB,…]”.
#       “channel” may be a group name (but only the name, without the list
#       of function names that comprise that group).
#
__validate_verbosity() {
	local verbosity="$1"
	local chan_name='[A-Za-z_][A-Za-z0-9_-]*'
	local level='([0-9]|[1-9][0-9]{0,4})'
	local unit="$chan_name=$level"

	[[ "$verbosity" =~ ^($unit)(,$unit)*,?$ ]]  || {
		redmsg "Invalid specification of verbosity: “$verbosity”."
		redmsg "Run with --verbosity-help to show the format."
		err 'Cannot apply verbosity setting.'
	}

	return 0
}


 # Reads channel=level specifiers from $VERBOSITY and assigns them
#  to VERBOSITY_CHANNELS.
#
#  $1 – a string with verbosity specifiers, see the format
#       in __validate_verbosity().
#
__set_verbosity_to_channels() {
	declare -gAx VERBOSITY_CHANNELS
	declare -gAx VERBOSITY_CHANNELS_HINTS

	local verbosity="$1"
	local verbosity_as_array=(  ${verbosity//,/ }  )
	local item
	local ch_name
	local ch_level

	for item in "${verbosity_as_array[@]}"; do
		ch_name=${item%=*}
		ch_level=${item#*=}
		VERBOSITY_CHANNELS[$ch_name]=$ch_level
		VERBOSITY_CHANNELS_TYPES[$ch_name]='stand-alone'
		VERBOSITY_CHANNELS_HINTS[$ch_name]="?"
	done

	return 0
}


 # Sets VERBOSITY according to --verbosity or --quiet. Verbosity levels
#  specified with these options take priority over the specification in the
#  environment variable VERBOSITY.
#
#  $1..n – positional arguments to the main script, usually
#          passed as "${ARGS[@]}".
#
#  Changes $ARGS.
#
__extract_verbosity_from_args() {
	declare -gx VERBOSITY
	declare -gx ARGS
	declare -gx VERBOSITY_CHANGED_ARGS

	local  temp_args=( "$@" )
	local  i
	local  arg
	local  nextarg
	local  number_of_deleted_args=0
	local  args_to_unset=()
	local  verb_spec

	for ((i=0; i<${#temp_args[*]}; i++)); do
		unset -n  arg || true  # Sic!
		declare -n  arg="temp_args[$i]"

		(( i < (${#temp_args[*]}-1) ))  && {
			declare -n nextarg="temp_args[$i+1]"
		}

		[ "$arg" = '--quiet' ] && {
			VERBOSITY="$VERBOSITY_QUIET_SET"
			args_to_unset+=(  $(( i - number_of_deleted_args++ ))  )
			continue
		}

		[[ "$arg" =~ ^--verbosity$ ]] && {
			if  (( i < (${#temp_args[*]}-1) ));  then
				__validate_verbosity "$nextarg"
				VERBOSITY="$nextarg"
				args_to_unset+=(  $((   i - number_of_deleted_args   ))
				                  $(( i+1 - number_of_deleted_args++ ))  )
				let '++i,  1'
				continue

			else
				err '--verbosity needs an argument. Run --verbosity-help for details.'
			fi
		}

		[[ "$arg" =~ ^--verbosity=(\'(.+)\'|\"(.+)\"|(.+))$ ]]  && {
			verb_spec="${BASH_REMATCH[2]:-${BASH_REMATCH[3]:-${BASH_REMATCH[4]}}}"
			__validate_verbosity "$verb_spec"
			VERBOSITY="$verb_spec"
			args_to_unset+=(  $(( i - number_of_deleted_args++ ))  )
			continue
		}
	done

	for i in ${args_to_unset[*]}; do
		unset temp_args[$i]
	done

	(( ${#temp_args[*]} != ${#ARGS[*]} ))  \
		&& ARGS=( "${temp_args[@]}" )  \
		&& VERBOSITY_CHANGED_ARGS=t

	return 0
}
#  No export: init stage function.


verbosity_init_report() {
	declare -g  VERBOSITY_CHANNELS

	local v_level=${VERBOSITY_CHANNELS[Verbosity]}

	(( v_level >= 3 )) && {
		info "Setting Verbosity channel level to “$v_level”."
		info "Verbosity was set from command line, setting new ARGS:"
		milinc
		for ((i=0; i<${#ARGS[@]}; i++)) do
			msg "ARGS[$i] = ${ARGS[i]}"
		done
		mildec
	}

	return 0
}


 # A short version of vchan for the bootstrap stage
#  $1 – vchan command.
#  $2…n – command parameters.
#
#  Option syntax conforms with that of the full-fledged vchan and described
#  in verbosity.sh. (Alternatively, run “vchan help-minlevel” and “vchan-help-
#  setup”, when the complete module is loaded.)
#
#  At the bootstrap stage it is crucial to avoid running subshells, as the
#  “error_handling” and “messages” modules, that deal with them properly,
#  are yet to be loaded. Thus implementing bootstrap checks for verbosity like
#      (( $(vchan getlevel <channel_name>) >= 5 )) && {
#          …
#      }
#  would be prone to spawning issues. Implementing the “minlevel” subcommand
#  instead allows to avoid using a subshell.
#
vchan() {
	local  command="${1:-}"; shift
	local  known_commands=( minlevel setup )
	local  name
	local  level
	local  min_level
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'
	local  level_pat='([0-9]|[1-9][0-9]?)'

	[[ "$command" =~ ^($(IFS='|'; echo "${known_commands[*]}"))$ ]]  \
		|| err "vchan (bootstrap): Unknown command: “$command”."

	case "$command" in
		minlevel)
			name="${1:-}"
			[[ "$name" =~ ^$name_pat$ ]]  \
				|| err-stack "Invalid name for a verbosity channel: “$name”.
				              Refer to “vchan help”."

			[ -v VERBOSITY_CHANNELS[$name] ]  \
				|| err-stack "Verbosity channel “$name” doesn’t exist."

			min_level="${2:-}"
			[[ "$min_level" =~ ^$level_pat$ ]]  \
				|| err-stack "Invalid minimal level for a verbosity channel: “$min_level”.
				              It must be a number in range 0…99."

			level=${VERBOSITY_CHANNELS[$name]}
			(( level >= min_level ))  \
				&& return 0  \
				|| return 1
			;;

		setup)
			for arg in "$@"; do
				if [[ $arg =~ ^name=($name_pat)$ ]]; then
					name="${BASH_REMATCH[1]}"

				elif [[ $arg =~ ^level=($level_pat)$ ]]; then
					level="${BASH_REMATCH[1]}"
				fi
			done

			[ -v name -a -v level ]  \
				|| err-stack "Invalid specification for a verbosity channel setup:
				             “$*”."

			#  The variables are sanitised, so it’s safe.
			#  eval is faster than  source <( cat <<-EOF 
			#
			eval "__bahelite_set_up_verbosity_channel_$name() { vchan setup $@; }"

			VERBOSITY_POSTPONED_SETUP_FUNCS+=(
				"__bahelite_set_up_verbosity_channel_$name"
			)

			[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" = '?' ]  \
				&&  unset VERBOSITY_CHANNELS_HINTS[$name]

			VERBOSITY_CHANNELS_TYPES[$name]='stand-alone'

			[ -v VERBOSITY_CHANNELS[$name] ]  && {
				#  Channel exists. Probably created via environment variable
				#  or command line parameter – it’s a user setting (and the
				#  level – too).
				return 0
			}
			VERBOSITY_CHANNELS[$name]=$level
			;;
	esac
	return 0
}
#  No export: init stage function.



show_help_on_verbosity_channel_console() {
	cat <<-EOF
	console (output channel)
	    Controls output to console.

    Levels:
	    0 – stderr and stdout are disabled;
	    1 – only stderr is enabled;
	    2 – both stderr and stdout are enabled (the default).

	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_bahelite() {
	cat <<-EOF
	bahelite (output channel)
	    Controls the information coming from the core of this library.

	Levels:
	    2 – no additional output (the default).
	        This level allows warnings and errors. However, the errors are
	        displayed only when the program cannot continue, and as of warn-
	        ings, none should appear, except for rare non-standard situations.

	    3 – a few informational messages are added to the output;

	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_main() {
	cat <<-EOF
	main (generic channel for all messages)
	    Controls the verbosity of the main script’s, i.e. $MYNAME’s messages.

	Levels:
	    0 – all messages disabled.
	    1 – only error-level messages.
	    2 – only errors and warnings.
	    3 – all messages – errors, warnings and informational – are allowed
	        (the default).
	    4..99 – turn on verbose messages. (Levels from 4 onwards are effective
	        only when the program has channels using levels above 3.)

	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_Verbosity() {
	cat <<-EOF
	Verbosity (self-report channel for the verbosity subsystem)

	Levels:
	    1 – only errors;
	    2 – only warnings and errors (the default);
	    3 – report on channel creation/alternation;
	    4 – turns on the reporting in more details in certain places.

	EOF
	return 0
}
#  No export: init stage function.



                        #  Setting verbosity  #

 # First, from the environment variable
#
[ -v VERBOSITY ]  && {
	__validate_verbosity "$VERBOSITY"
	__set_verbosity_to_channels "$VERBOSITY"
}
#
#  Then, from the command line options
#
[[ "${ARGS[*]}"  =~ ^(|.+[[:blank:]])(--verbosity(=.+|[[:blank:]].+|)|--quiet)([[:blank:]]*|[[:blank:]]+.+|)$ ]]  && {
	__extract_verbosity_from_args "${ARGS[@]}"
	__validate_verbosity "$VERBOSITY"
	__set_verbosity_to_channels "$VERBOSITY"
}


 # Setting up basic channels
#  vchan command respects settings installed via the command line option
#  or via the environment variable.
#

 # Common verbosity channel for the main script (the one that’s in MYNAME).
#  Automatically hooks to the function “main”, thus spanning over the entire
#  program. For this reason it’s created as a regular (non-meta) channel,
#  (so that it /would/ match the function’s name). This channel may be over-
#  ridden by any other, who’d be deeper in the FUNCNAME call stack and match
#  an existing function name. The maximum possible level is left unconstrained
#  intentionally to allow main script messages use anything up to 99.
#
vchan setup  name=main   \
             level=3     \
             min_level=0


 # Controls stdout and stderr
#  Further details are in verbosity.sh
#
vchan setup	 name=console  \
             level=2       \
             meta=M        \
             hint=output   \
             min_level=0   \
             max_level=2
             #  level_is_locked='L'  #  Locked later in the “verbosity” module.


 # The core module’s verbosity channel
#  Initially it was supposed to mark the start and the end of the library’s
#  loading process; to be used for things that aren’t related to any parti-
#  cular module and concern many at once. Currently it does little, see the
#  help output.
#
vchan setup  name=bahelite  \
             level=2        \
             meta=M         \
             hint=output    \
             min_level=2    \
             max_level=3


 # Verbosity module’s own channel for self-reporting
#  Each module with a developed (and sometimes tangled) structure has a self-
#  report channel to describe the processing in runtime. This channel isn’t
#  made into a group intentionally to provide better execution speed, as Ver-
#  bosity penetrates through at least a half of the library. Generally, the
#  Verbosity subsystem is costly, so while for the main script it may be handy
#  and fine to use vchan often, Bahelite often queries variables directly,
#  seldom sets, giving preference to vchan.
#
vchan setup  name=Verbosity    \
             level=2           \
             meta=M            \
             hint=self-report  \
             min_level=1       \
             max_level=4


#  Finally, report changes.
verbosity_init_report


 # Postload job to parse --verbosity-help-CHANNELNAME. Obviously, for the
#  function to be able to show anything, all core modules must load first.
#
bahelite_parse_--verbosity-help() {
	declare -gx BAHELITE_VERBOSITY_LIST_CHANNELS

	local  i
	local  arg
	local  ch_name
	local  help_func

	for ((i=0; i<${#ARGS[*]}; i++)); do
		arg="${ARGS[i]}"

		[ "$arg" = '--verbosity-help' ] && {
			show_help_on_verbosity
			exit 0
		}

		[[ "$arg" =~ ^--verbosity-more-?help$ ]] && {
			show_more_help_on_verbosity
			exit 0
		}

		[[ "$arg" =~ ^--verbosity-dev-?help$ ]] && {
			show_developer_help_on_verbosity
			exit 0
		}

		[[ "$arg" =~ ^--verbosity-help-([A-Za-z_][A-Za-z0-9_-]*)$ ]] && {
			ch_name=${BASH_REMATCH[1]}
			help_func="show_help_on_verbosity_channel_$ch_name"
			if	   [ -v VERBOSITY_CHANNELS[$ch_name]        ]  \
				&& [ "$(type -t "$help_func")" = 'function' ]
			then
				$help_func
				exit 0
			fi

			(( ${VERBOSITY_CHANNELS[Verbosity]} >= 3 ))  \
				&& warn "Bahelite: --verbosity-help: unknown channel “$ch_name”.
				         Leaving it to be parsed by the main script."
		}

		[[ "$arg" =~ ^--verbosity-list(-channels|)$ ]]  && {
			BAHELITE_VERBOSITY_LIST_CHANNELS=t
			unset ARGS[$i]
		}

	done

	return 0
}



BAHELITE_POSTLOAD_JOBS+=(
	bahelite_parse_--verbosity-help
)



return 0
