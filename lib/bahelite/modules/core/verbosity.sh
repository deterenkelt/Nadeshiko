#  Should be sourced.

#  verbosity.sh
#  This facility allows to control major outputs, such as stdout, stderr
#    or a logfile. It also allows to control the amount of messages shown
#    (messages are those that are send with the “messages” Bahelite module).
#  This module doesn’t control /any/ echo or output sent to stdout.
#    The control over outputs is limited and consists of either leaving
#    them as is or shutting them down. So the term “verbosity” refers
#    primarily to the messaging system.
#  Author: deterenkelt, 2019–2024
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_VERBOSITY_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_VERBOSITY_VER='2.2.6'

#  The processing of module options is delegated to “verbosity_base” module.
#  Channels are set up there below function definitions.



                    #  Internal functions for vchan()  #

 # For anything related to verbosity channels, any code outside of this file
#  and messages.sh must use the “vchan” function. Only this way proper vali-
#  dation is guaranteed.
#

 # Checks if a string is a proper verbosity channel name.
#  $1  – string to check.
#
__validate_verbosity_channel_name() {
	local  name="$1"
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'

	[[ "$name" =~ ^$name_pat$ ]]  || {
		redmsg "“$name” isn’t a valid verbosity channel name.
		        Run the script with --help-verbosity for description."
		err-stack 'Incorrect name for verbosity channel.'
	}
	[ -v VERBOSITY_CHANNELS[$name] ]  \
		|| err-stack "Verbosity channel “$name” doesn’t exist."

	return 0
}
export -f __validate_verbosity_channel_name


 # Checks if a string is a proper number for a verbosity level.
#  $1 – string to check.
#
__validate_verbosity_channel_level() {
	local  ch_level="$1"
	local  level_pat='([0-9]|[1-9][0-9]?)'

	[[ "$ch_level" =~ ^$level_pat$ ]]  || {
		#  Could simply say that valid values are 0…99, but it’s better
		#  to direct unknowledgeable people to the tip at least once.
		redmsg "“$ch_level” isn’t a valid level for a verbosity channel.
		        Refer to “vchan help” to show the format."
		err-stack 'Incorrect value for verbosity level.'
	}

	return 0
}
export -f __validate_verbosity_channel_level


 # Retrieves the name of the verbosity channel that is currently in effect
#  (that is, at the place in the code from where “vchan”, calling this func-
#  tion, was called.)
#
#  The __nomsg alias is intended for the use by the messages.sh module. Called
#  by this alias, this function will not call __msg() in any form, and will
#  rely only on the return codes to indicate an error. This is a measure to
#  prevent recursive calls. Between verbosity.sh and messages.sh when either
#  an error happens or when the level of the Verbosity channel is raised and
#  it needs __msg() to report what’s going on.
#
__nomsg_get_current_verbosity_channel_name() {
	__get_current_verbosity_channel_name "$@"
}
__get_current_verbosity_channel_name() {
	local  i
	local  func_name
	local  found_ch_name
	local  nomsg
	local  funcs_to_skip=2

	[ "${FUNCNAME[1]}" = __nomsg_get_current_verbosity_channel_name ] && {
		nomsg=t
		funcs_to_skip=3
	}

	 # Without an alias
	#
	#  FUNCNAME[0] == this function’s name
	#          [1] == vchan
	#          [2] == <the name of the actual function, that made the request>
	#
	#  With an alias
	#
	#  FUNCNAME[0] == this function
	#          [1] == __msg
	#          [2] == a wrapper, that used __msg (like “info”, “warn”…)
	#          [3] == <the name of the actual function, that made the request>
	#
	for ((i=funcs_to_skip; i<${#FUNCNAME[@]}; i++)); do
		func_name=${FUNCNAME[i]}
		if	   [   -v VERBOSITY_CHANNELS[$func_name]      ]  \
			&& [ ! -v VERBOSITY_CHANNELS_IS_META[$func_name] ]
		then
			found_ch_name="$func_name"
			break
		fi
	done

	if [ -v nomsg ]; then
		[ -v found_ch_name ]  \
			&& echo "$found_ch_name"  \
			|| return 1
	else
		if [ -v found_ch_name ]; then
			(( v_level >= 4 ))  && {
				(
				set +T
				echo "${__mi}verbosity: cur. channel is “$found_ch_name”"
				echo -n "${__mi}  "
				sed -r "s/ /\n${__mi}  ← /g"  <<<"${FUNCNAME[*]}"
				) >&${STDERR_ORIG_FD_PATH:-/proc/$$/fd/2}
			}
		else
			err-stack "Couldn’t determine current verbosity channel. No “main”?"
		fi
	fi
	return 0
}
export -f __get_current_verbosity_channel_name  \
          __nomsg_get_current_verbosity_channel_name


 # Sets verbosity level for the specified channel.
#  $1 – channel name. Can be an ordinary, stand-alone channel or a group.
#       Cannot be a group member.
#  $2 – level, 0…99. If the channel specified as $1 is a group,
#       then the level is applied also to all members of this group.
#
__set_verbosity_level() {
	local  name="$1"
	local  level="$2"
	local  group_items
	local  gr_item

	[ -v VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[$name] ]  && {
		level=${VERBOSITY_CHANNELS[$name]}
		redmsg "The level of the verbosity channel “$name” is locked
		        and cannot be changed at this point in code.

		        The reason is that some – or even all – tasks, that the verbosity of
		        this channel should affect, have been completed by now. Thus, to set
		        the level now is futile.

		        Any channel accepts settings, if they are made at the pre-init stage,
		        that is, when they’re specified via the --verbosity command line argu-
		        ment, or conveyed with the VERBOSITY environment variable, e.g.

		            $ ./$MYNAME --verbosity  tmpdir=1  <other arguments>
		        or
		            $ VERBOSITY=tmpdir=1 ./$MYNAME  <arguments>

		        DO NOT set the level of a locked channel directly, by altering the
		        VERBOSITY_CHANNELS[<Channel_name>] variable: if you do that and the
		        channel is a group channel, its children won’t be affected!

		        "
		echo
        err "Cannot set verbosity level on channel “$name” – the level is locked."
	}

	case "${VERBOSITY_CHANNELS_TYPES[$name]}" in
		group)
			group_items=${VERBOSITY_CHANNELS_GROUP_ITEMS[$name]}
			for gr_item in ${group_items//,/ }; do
				VERBOSITY_CHANNELS[$gr_item]=$level
			done
			;&
		stand-alone)
			VERBOSITY_CHANNELS[$name]=$level
			;;

		channel-in-a-group)
			redmsg "Changing verbosity level on a channel, that belongs to a group,
			        is forbidden. Instead, set the level for its master channel."
			err "Cannot set verbosity level on channel “$name” –
			     the channel belongs to group “${VERBOSITY_CHANNELS_GROUP_LEADERS[$name]}”."
			;;
	esac

	return 0
}
export -f __set_verbosity_level


 # Creates a verbosity channel by given parameters.
#  $@ – parameters according to specification. See format in “vchan setup”.
#
__set_up_verbosity_channel() {
	local  arg

	local  name
	local  level=$VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS
	local  type='stand-alone'
	local  meta
	local  group_item
	local  group_items
	local  level_is_locked
	local  hint=''
	local  min_level
	local  max_level

	local  gr_item
	local  setting_group_items_one_by_one
	local  setting_group_items_as_a_bunch
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'
	local  level_pat='([0-9]|[1-9][0-9]?)'

	(( v_level >= 3 )) && {
		info "Setting up a verbosity channel."
		milinc
	}

	#  I. Validating the input
	#
	for arg in "$@"; do
		case "$arg" in
			group_item=*)
				setting_group_items_one_by_one=t
				;;
			group_items=*)
				setting_group_items_as_a_bunch=t
				;;
		esac
	done
	[ -v setting_group_items_one_by_one -a -v setting_group_items_as_a_bunch ]  \
		&& err-stack "vchan setup: cannot use “group_item” and “group_items” simultaneously."

	for arg in "$@"; do
		case "$arg" in
			name=*)
				name="${arg#name=}"
				[[ "$name" =~ ^$name_pat$ ]]  \
					|| err-stack "Wrong name for a channel. Refer to “vchan help-setup”."
				;;

			level=*)
				level=${arg#level=}
				[[ "$level" =~ ^$level_pat$ ]]  \
					|| err-stack "Wrong level for a channel. Refer to “vchan help-setup”."
				;;

			meta=*)        #  Anything goes. The parameter could be just “meta”,
				meta='M'   #  but everyone would instinctively try to assign smth.
				;;

			group_item=*)
				group_item=${arg#group_item=}
				[[ "$group_item" =~ ^$name_pat$ ]]  \
					|| err-stack "Wrong name for a group item. Refer to “vchan help-setup”."
				group_items="${group_items:+$group_items,}$group_item"
				;;

			group_items=*)
				group_items=${arg#group_items=}
				[[ "$group_items" =~ ^$name_pat(,$name_pat)*$ ]]  \
					|| err-stack "Wrong set of group items for a channel.
					              Refer to “vchan help-setup”."
				;;

			level_is_locked=*)         #  Anything goes, just like with “meta”.
				level_is_locked='L'
				;;

			hint=*)
				hint=${arg#hint=}  # Anything goes.
				;;

			min_level=*)
				min_level=${arg#min_level=}
				[[ "$min_level" =~ ^$level_pat$ ]]  \
					|| err-stack "Wrong minimum level for a channel. Refer to “vchan help-setup”."
			 	;;

			max_level=*)
				max_level=${arg#max_level=}
				[[ "$max_level" =~ ^$level_pat$ ]]  \
					|| err-stack "Wrong maximum level for a channel. Refer to “vchan help-setup”."
			 	;;

			*)
				err-stack "Unknown argument for vchan setup: “$arg”."
				;;
		esac
	done

	 # II. Checking for conflicts between input data
	#      and existing data
	#
	[ -v name ]  \
		|| err-stack "vchan setup: at least the channel name must be specified."

	[ -v VERBOSITY_CHANNELS[$name] ] && {
		#
		#  Checking, whether the level is in bounds
		#
		[ -v min_level -a -v max_level ] && (( min_level > max_level )) && {
			err-stack "Minimum verbosity level must be lower or equal to the maximum level."
		}
		level=${VERBOSITY_CHANNELS[$name]:-$level}
		[ -v min_level ] && (( level < min_level )) && {
			(( v_level >= 2 ))  \
				&& warn "Verbosity: correcting channel “$name” level from $level to $min_level."
			level=$min_level
		}
		[ -v max_level ] && (( level > max_level )) && {
			(( v_level >= 2 ))  \
				&& warn "Verbosity: correcting channel “$name” level from $level to $max_level."
			level=$max_level
		}
		#
		#  Checking whether any of the other attributes exist (they shouldn’t)
		#
		if 	   [ -v VERBOSITY_CHANNELS_IS_META[$name] ]          \
			|| [ -v VERBOSITY_CHANNELS_GROUP_ITEMS[$name] ]      \
			|| [ -v VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[$name] ]  \
			|| [ -v VERBOSITY_CHANNELS_MIN_LEVELS[$name] ]       \
			|| [ -v VERBOSITY_CHANNELS_MAX_LEVELS[$name] ]       \
			|| (    [ -v VERBOSITY_CHANNELS_HINTS[$name] ]       \
			     && [ "${VERBOSITY_CHANNELS_HINTS[$name]}" != '?' ]  )
		then
			err-stack "Cannot run setup for verbosity channel “$name”: channel attributes
			           already exist! Use specific vchan commands to modify a channel,
			           alternativiley delete it and create anew."
		fi
	}

	[ -v group_items ]  && {
		type=group
		[ "${VERBOSITY_CHANNELS_TYPES[$name]:-}" = 'channel-in-a-group' ]  \
			&& err-stack "A verbosity channel that is a part of a group cannot be made
			              into a separate channel of its own."

		#  Check, that the group items, that we’re going to set, are not
		#  existing channels, groups or group items.
		#
		for gr_item in ${group_items//,/ }; do
			[ -v VERBOSITY_CHANNELS[$gr_item] ] && {
				(( v_level >= 2 ))  && {
					warn "Verbosity channel “$gr_item” is supposed to be added to group “$name”,
					      but the channel already exists with type “${VERBOSITY_CHANNELS_TYPES[$gr_item]:-}”."
				}
				milinc
				if [ "${VERBOSITY_CHANNELS_HINTS[$gr_item]:-}" = '?' ]; then
					#
					#  If the level was created due to user attempting to
					#  control entire group via it (probably not knowing
					#  it’s a mistake), this can be corrected by simply
					#  ignoring the user input and deleting the channel.
					#
					#  Anyone would agree, that if program crashes because
					#  of a faulty custom setting, instead of ignoring it
					#  run on the basic devices, it’s a bad program.
					#
					(( v_level >= 2 ))  && {
						warn "It seems the channel was specified via command line.
						      To change its verbosity level, the level of the master
						      channel should be changed instead, that is, “$name”’s."
						warn "The problem channel is set for ${__bri}${__yel}deletion${__s} as its existence
						      most probably is a result of mistake."
					}
					__delete_verbosity_channel "$gr_item"
				else
					#
					#  If it isn’t just user input error, but a misconfi-
					#  guration, that is, main script let a mistake in the
					#  code, like setting a group member with “vchan setup”
					#  instead of “vchan addtogroup”, or simply mistaken
					#  one channel name for another, it’s more reasonable
					#  to stop execution and let the programmer notice it
					#  immediately.
					#
					err-stack "Cannot create verbosity channel “$name” as a group
					           with item “$gr_item”: the latter already exists."
				fi
				mildec
			}
		done
	}

	 # III. Creating/updating the channel
	#
	#  The “setup” procedure is supposed to be run only once, and the further
	#  changes are to be made with separate commands like “setlevel”. Hence
	#  the “setup” command /is not meant to update the channel data/, however:
	#    - due to early creation of a channel (when the level was set
	#      by user via command line), the level specified to “vchan setup”
	#      yields to one assigned this way;
	#    - for the same reason the hint '?' is taken down and replaced;
	#    - the rest of the attributes are supposed to be created and must
	#      not exist, otherwise the call to “vchan setup” will be treated
	#      as an attempt to modify an existing channel and will trigger
	#      an error.
	#
	VERBOSITY_CHANNELS[$name]=$level        #  Either setting it or updating.
	VERBOSITY_CHANNELS_TYPES[$name]=$type   #  Either setting it or updating.

	[ -v meta ] && VERBOSITY_CHANNELS_IS_META[$name]='M'
	[ -v level_is_locked ] && VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[$name]='L'
	VERBOSITY_CHANNELS_HINTS[$name]="$hint"
	[ -v min_level ] && VERBOSITY_CHANNELS_MIN_LEVELS[$name]=$min_level
	[ -v max_level ] && VERBOSITY_CHANNELS_MAX_LEVELS[$name]=$max_level


	(( v_level >= 3 ))  \
		&& info "Verbosity channel “${__bri}$name${__s}” created with type “${__bri}$type${__s}”."

	[ -v group_items ] && {
		VERBOSITY_CHANNELS_GROUP_ITEMS[$name]="$group_items"
		(( v_level >= 3 )) && milinc
		for gr_item in ${group_items//,/ }; do
			VERBOSITY_CHANNELS[$gr_item]=$level
			VERBOSITY_CHANNELS_TYPES[$gr_item]='channel-in-a-group'
			VERBOSITY_CHANNELS_GROUP_LEADERS[$gr_item]="$name"
			unset VERBOSITY_CHANNELS_HINTS[$gr_item]  # remove possible '?'
			(( v_level >= 3 ))  \
				&& info "Channel “${__bri}$gr_item${__s}” created as a group member."
		done
		(( v_level >= 3 )) && mildec
	}

	(( v_level >= 3 )) && mildec
	return 0
}
export -f __set_up_verbosity_channel


 # Adds a verbosity channel to a group.
#  $1 – name=<channel_name>. Name of the channel to be added.
#  $2 – group=<group_name>. The channel name of the group, that the channel
#       specified in $1 is to be added to. Must exist beforehand. May be a
#       regular channel, in which case its type will be changed to a group.
#
#  If the speed is a concern: it’s better to avoid using this function and
#  instead specify all items at once at the time you run “vchan setup”.
#
__add_verbosity_channel_to_group() {
	local  arg
	local  ch_name
	local  gr_name
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'
	local  group_items

	for arg in "$@"; do
		case "$arg" in
			name=*)
				ch_name="${arg#name=}"
				[[ "$ch_name" =~ ^$name_pat$ ]]  \
					|| err-stack "Wrong name for a channel. Refer to “vchan help-addtogroup”."
				;;

			group=*)
				gr_name="${arg#group=}"
				[[ "$gr_name" =~ ^$name_pat$ ]]  \
					|| err-stack "Wrong name for a group. Refer to “vchan help-addtogroup”."
				;;
		esac
	done

	[ -v VERBOSITY_CHANNELS[$gr_name] ]  \
		|| err-stack "Cannot add channel “$ch_name” to “$gr_name”: the latter doesn’t exist."

	[ "${VERBOSITY_CHANNELS_TYPES[$gr_name]}" = 'channel-in-a-group' ]  \
		&& err-stack "Cannot add channel “$ch_name” to “$gr_name”: the latter is a group member itself
		              (belongs to “${VERBOSITY_CHANNELS_GROUP_LEADERS[$gr_name]})”."

	#  The channel may exist, if the level was set via command line.
	#  But it may not be itself a group or a group member already.
	#  It may only be an underdeveloped group member.
	#
	#  Solving this the same way as in __set_up_verbosity_level()
	#  (see also the commentaries there).
	#
	[ -v VERBOSITY_CHANNELS[$ch_name] ] && {
		(( v_level >= 2 ))  && {
			warn "Verbosity channel “$ch_name” is supposed to be added to group “$gr_name”,
			      but the channel already exists with type “${VERBOSITY_CHANNELS_TYPES[$ch_name]:-}”."
		}
		milinc
		if	[ "${VERBOSITY_CHANNELS_HINTS[$ch_name]:-}" = '?' ]
		then
			(( v_level >= 2 ))  && {
				warn "It seems the channel was specified via command line.
				      To change its verbosity level, the level of the master
				      channel should be changed instead, that is, “$gr_name”’s."
				warn "The problem channel is set for ${__bri}${__yel}deletion${__s} as its existence
				      most probably is a result of mistake."
			}
			__delete_verbosity_channel "$ch_name"

		else
			err-stack "Cannot add verbosity channel “$ch_name” as a member of group
			           “$gr_name”: channel already exists."
		fi
		mildec
	}

	(( v_level >= 2 )) && {
		[ "$(type -t "$ch_name")" != 'function' ]  \
			&& warn "Channel “$ch_name” is going to be added to group “$gr_name”,
			         but it seems to be pointless – there’s no function for this channel."
	}

	 # Updating the group list in the parent channel
	#
	group_items=${VERBOSITY_CHANNELS_GROUP_ITEMS[$gr_name]}
	group_items="${group_items:+$group_items,}$ch_name"
	VERBOSITY_CHANNELS_GROUP_ITEMS[$gr_name]="$group_items"
	#  Making sure the type is set correctly
	VERBOSITY_CHANNELS_TYPES[$gr_name]='group'

	 # Setting up the channel
	#
	VERBOSITY_CHANNELS[$ch_name]=${VERBOSITY_CHANNELS[$gr_name]}  # level
	VERBOSITY_CHANNELS_TYPES[$ch_name]='channel-in-a-group'
	VERBOSITY_CHANNELS_GROUP_LEADERS[$ch_name]="$gr_name"
	unset VERBOSITY_CHANNELS_HINTS[$ch_name]  # remove possible '?'

	return 0
}
export -f __add_verbosity_channel_to_group


 # Removes all VERBOSITY_CHANNELS_*[channel_name] in arrays and removes men-
#  tions of the specified channel in VERBOSITY_CHANNELS_GROUP_ITEMS[*]
#  $1 – channel name to delete
#
__delete_verbosity_channel() {
	local  name=${1:-}
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'

	(( v_level >= 3 )) && {
		info "Deleting verbosity channel “$name”…"
		milinc
	}

	[[ "$name" =~ ^$name_pat$ ]]  \
		|| err-stack "Invalid name for a verbosity channel: “$name”.
		              Refer to “vchan help”."

	 # Checking for existence before unsetting, because if there will be
	#  no variable found, “unset” will delete a function, and this may be harm-
	#  ful, if the function would exist – and it may, because verbosity chan-
	#  nels are named after functions.
	#
	[ -v VERBOSITY_CHANNELS[$name] ]  \
		&& unset VERBOSITY_CHANNELS[$name]

	[ -v VERBOSITY_CHANNELS_IS_META[$name] ]  \
		&& unset VERBOSITY_CHANNELS_IS_META[$name]

	[ -v VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[$name] ]  \
		&& unset VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[$name]

	[ -v VERBOSITY_CHANNELS_HINTS[$name] ]  \
		&& unset VERBOSITY_CHANNELS_HINTS[$name]

	[ -v VERBOSITY_CHANNELS_TYPES[$name] ]  \
		&& unset VERBOSITY_CHANNELS_TYPES[$name]

	[ -v VERBOSITY_CHANNELS_GROUP_LEADERS[$name] ]  \
		&& unset VERBOSITY_CHANNELS_GROUP_LEADERS[$name]

	[ -v VERBOSITY_CHANNELS_MIN_LEVELS[$name] ]  \
		&& unset VERBOSITY_CHANNELS_MIN_LEVELS[$name]

	[ -v VERBOSITY_CHANNELS_MAX_LEVELS[$name] ]  \
		&& unset VERBOSITY_CHANNELS_MAX_LEVELS[$name]

	for key in ${!VERBOSITY_CHANNELS_GROUP_ITEMS[*]}; do
		[[ "${VERBOSITY_CHANNELS_GROUP_ITEMS[$key]}" =~ ^(|.+,)$name(,.+|)$ ]] && {
			VERBOSITY_CHANNELS_GROUP_ITEMS[$key]="${BASH_REMATCH[1]}${BASH_REMATCH[2]}"
			VERBOSITY_CHANNELS_GROUP_ITEMS[$key]=${VERBOSITY_CHANNELS_GROUP_ITEMS[$key]//,,/,}
		}
	done

	(( v_level >= 3 ))  && {
		info "Deleted."
		mildec
	}
	return 0
}


 # A helper for debugging, lists all mentions of a channel.
#  $1 – channel name
#
__describe_verbosity_channel() {
	local  name="${1:-}"
	local  name_pat='[A-Za-z_][A-Za-z0-9_]*'
	local  report=''
	local  key
	local  varname
	local  varval

	info "Describing verbosity channel “${__bri}$name${__s}”:"
	milinc

	[[ "$name" =~ ^$name_pat$ ]]  || {
		warn "“$name” isn’t a valid channel name."
		#  Hence no entries could be created, unless they were created
		#  by circumventing vchan.
		return 0
	}

	for varname in VERBOSITY_CHANNELS                  \
	               VERBOSITY_CHANNELS_TYPES            \
	               VERBOSITY_CHANNELS_IS_META          \
	               VERBOSITY_CHANNELS_LEVEL_IS_LOCKED  \
	               VERBOSITY_CHANNELS_HINTS            \
	               VERBOSITY_CHANNELS_MIN_LEVELS       \
	               VERBOSITY_CHANNELS_MAX_LEVELS       \
	               VERBOSITY_CHANNELS_GROUP_ITEMS      \
	               VERBOSITY_CHANNELS_GROUP_LEADERS
	do
		if [ -v $varname[$name] ]; then
			declare -n varval=$varname[$name]
			report+="$varname[]\t \t${__bri}$varval${__s}\n"
		else
			report+="${__dim}$varname[]\t \tnot set${__s}\n"
		fi
	done

	msg "$(echo -e "$report" | column -t -s $'\t')\n"

	report=''

	for key in ${!VERBOSITY_CHANNELS_GROUP_ITEMS[*]}; do
		[[ "${VERBOSITY_CHANNELS_GROUP_ITEMS[$key]}" =~ ^(|.+,)$name(,.+|)$ ]]  \
			&& report+="${report:+,  }"
	done

	if [ "$report" ]; then
		info "It is a group member of these channels: $report."
	else
		info "The data of group channels have no mention of this channel."
	fi

	mildec
	return 0
}


 # Displays all known verbosity channels.
#
__list_verbosity_channels() {
	local  name
	local  level
	local  type
	local  meta
	local  hint
	local  group_items
	local  min_level
	local  max_level

	local  table
	local  unrecogn_ch_found

	local -A selected_ch_names=()


	(( v_level >= 4 )) && {
		for name in "${!VERBOSITY_CHANNELS[@]}"; do
			__describe_verbosity_channel "$name"
			echo
		done
		return 0
	}

	echo
	info 'List of known verbosity channels:'

	 # Selecting only those channels, that are standalone or group
	#  other channels. Slave channels won’t get in this list (these
	#  will be printed beside their group channels).
	#
	for name in ${!VERBOSITY_CHANNELS[*]}; do
		[ "${VERBOSITY_CHANNELS_TYPES[$name]}" = 'channel-in-a-group' ]  \
			&& continue
		selected_ch_names+=( [$name]='' )
	done

	 # Grouping channels by VERBOSITY_CHANNELS_HINT
	#
	#  The hint “field” exists for the sole purpose of making this list
	#  pretty when printed on the screen, so the hint field is not described
	#  in the help.
	#
	table="\n${__bri}Output channels${__s}\n\n"

	table+=$(
		while read name; do
 			[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" != 'output' ]  \
 				&& continue
			level="${VERBOSITY_CHANNELS[$name]:-}"
			min_level="${VERBOSITY_CHANNELS_MIN_LEVELS[$name]:-0}"
			max_level="${VERBOSITY_CHANNELS_MAX_LEVELS[$name]:-99}"
			type="${VERBOSITY_CHANNELS_TYPES[$name]:-}"
			meta="${VERBOSITY_CHANNELS_IS_META[$name]:-}"
			group_items="${VERBOSITY_CHANNELS_GROUP_ITEMS[$name]:-}"
			group_items="${group_items//,/\\n \\t \\t \\t \\t \\t}"  # nbsp!

			echo -e "$level\t$min_level–$max_level\t$type\t$meta\t$name\t$group_items"

			unset selected_ch_names[$name]

		done < <(IFS=$'\n'; echo "${!selected_ch_names[*]}" | sort)  \
		     | column -t -s $'\t' -N "Level,L. range,Type,Meta,Name,Group items"
	)

	table+="\n\n${__bri}Bahelite self-report channels${__s}\n\n"

	table+=$(
		while read name; do
 			[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" != 'self-report' ]  \
 				&& continue
			level="${VERBOSITY_CHANNELS[$name]:-}"
			min_level="${VERBOSITY_CHANNELS_MIN_LEVELS[$name]:-0}"
			max_level="${VERBOSITY_CHANNELS_MAX_LEVELS[$name]:-99}"
			type="${VERBOSITY_CHANNELS_TYPES[$name]:-}"
			meta="${VERBOSITY_CHANNELS_IS_META[$name]:-}"
			group_items="${VERBOSITY_CHANNELS_GROUP_ITEMS[$name]:-}"
			group_items="${group_items//,/\\n \\t \\t \\t \\t \\t}"  # nbsp!

			echo -e "$level\t$min_level–$max_level\t$type\t$meta\t$name\t$group_items"

			unset selected_ch_names[$name]

		done < <(IFS=$'\n'; echo "${!selected_ch_names[*]}" | sort)  \
		     | column -t -s $'\t' -N "Level,L. range,Type,Meta,Name,Group items"
	)

	table+="\n\n${__bri}$MY_DISPLAY_NAME’s own channels${__s}\n\n"

	table+=$(
		while read name; do
 			[[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" =~ ^(output|self-report|\?)$ ]]  \
	 			&& continue

			level="${VERBOSITY_CHANNELS[$name]:-}"
			min_level="${VERBOSITY_CHANNELS_MIN_LEVELS[$name]:-0}"
			max_level="${VERBOSITY_CHANNELS_MAX_LEVELS[$name]:-99}"
			type="${VERBOSITY_CHANNELS_TYPES[$name]:-}"
			meta="${VERBOSITY_CHANNELS_IS_META[$name]:-}"
			group_items="${VERBOSITY_CHANNELS_GROUP_ITEMS[$name]:-}"
			group_items="${group_items//,/\\n \\t \\t \\t \\t \\t}"  # nbsp!

			echo -e "$level\t$min_level–$max_level\t$type\t$meta\t$name\t$group_items"

			unset selected_ch_names[$name]

		done < <(echo 'main'; IFS=$'\n'; echo "${!selected_ch_names[*]}" | sort | grep -vE '^main$')  \
		     | column -t -s $'\t' -N "Level,L. range,Type,Meta,Name,Group items"
	)

	for name in ${!selected_ch_names[*]}; do
		[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" = '?' ] && {
			unknown_ch_found=t
			break
		}
	done

	[ -v unknown_ch_found ] && {
		table+="\n\n${__bri}Unknown channels\n\n${__s}"

		table+=$(
			while read name; do
	 			[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" != '?' ]  \
		 			&& continue

				level="${VERBOSITY_CHANNELS[$name]:-}"
				echo -e "$level\t${__red}$name${__s}"

				unset selected_ch_names[$name]

			done < <(IFS=$'\n'; echo "${!selected_ch_names[*]}")  \
			     | column -t -s $'\t' -N "Level,Name"
		)
	}

	table+=$'\n\n'"To get information about a channel, run with ${__bri}--verbosity-help-${__g}CHANNELNAME${__s}"

	msg "$table"
	return 0
}
export -f __list_verbosity_channels


__detect_unknown_verbosity_channels() {
	local  name
	local  unknown_channels

	unknown_channels=()

	for name in ${!VERBOSITY_CHANNELS[*]}; do
		[ "${VERBOSITY_CHANNELS_HINTS[$name]:-}" = '?' ]  \
			&& unknown_channels+=( "$name" )
	done

	(( ${#unknown_channels[*]} > 0 )) && {
		warn "Unknown verbosity channel name$(plur_sing ${#unknown_channels[*]}): $(sed -r 's/ /, /g' <<<"${unknown_channels[*]}").
		      Make sure, that the channel name is spelled correctly – the case matters.
		      See --verbosity-list to see the list of channels."
	}
	return 0
}
export -f __detect_unknown_verbosity_channels


 # Universal command to work with verbosity channels, levels and groups.
#
#  Any code outside of this file should operate on VERBOSITY_CHANNELS arrays
#  through the means of vchan.¹ It has a simple and stable syntax, and it also
#  performs checks, that prevent
#    - creation of duplicate channels;
#    - assignments to non-existing channels;
#    - setting a verbosity channel level to a value outside of the bounds
#      defined at that channel’s creation.
#
#  ¹ There are exception in the faces of messages and message_indentation
#  modules, the details are described in --verbosity-devhelp.
#
vchan() {
	local  command="${1:-}"; shift
	local  name
	local  level
	local  min_level
	local  cur_level
	local  eq_level
	local  v_level=${VERBOSITY_CHANNELS[Verbosity]}

	(( v_level >= 3 ))  && {
		info "Executing “vchan $command”, args: $*"
		milinc
	}

	case "$command" in
		getname)
			__get_current_verbosity_channel_name
			;;

		getlevel)
			if [ "${1:-}" ]; then
				__validate_verbosity_channel_name "$1"
				name="$1"
			else
				#  This handles meta / non-meta too
				name=$(__get_current_verbosity_channel_name)
			fi
			echo "${VERBOSITY_CHANNELS[$name]}"
			;;

		setlevel)
			if [ "${1:-}" -a "${2:-}" ]; then
				__validate_verbosity_channel_name "$1"
				name="$1"
				__validate_verbosity_channel_level "$2"
				level="$2"

			elif [ "${1:-}"  -a  -z "${2:-}" ]; then
				name=$(__get_current_verbosity_channel_name)
				__validate_verbosity_channel_level "$1"
				level="$1"

			else
				err-stack "Cannot set level on verbosity channel: invalid operands.
				           Refer to “vchan help-setlevel”."
			fi
			__set_verbosity_level "$name" "$level"
			;;

		iflevel)
			if [ "${1:-}" -a "${2:-}" ]; then
				__validate_verbosity_channel_name "$1"
				name="$1"
				__validate_verbosity_channel_level "$2"
				eq_level="$2"

			elif [ "${1:-}"  -a  -z "${2:-}" ]; then
				name=$(__get_current_verbosity_channel_name)
				__validate_verbosity_channel_level "$1"
				eq_level="$1"

			else
				err-stack "Cannot test level on verbosity channel: invalid operands.
				           Refer to “vchan help-iflevel”."
			fi

			cur_level="${VERBOSITY_CHANNELS[$name]}"

			(( cur_level == eq_level ))  \
				&& return 0  \
				|| return 1
			;;

		minlevel)
			if [ "${1:-}" -a "${2:-}" ]; then
				__validate_verbosity_channel_name "$1"
				name="$1"
				__validate_verbosity_channel_level "$2"
				min_level="$2"

			elif [ "${1:-}"  -a  -z "${2:-}" ]; then
				__validate_verbosity_channel_level "$1"
				min_level="$1"
				name=$(__get_current_verbosity_channel_name)

			else
				err-stack "Cannot test level on verbosity channel: invalid operands.
				           Refer to “vchan help-minlevel”."
			fi

			cur_level="${VERBOSITY_CHANNELS[$name]}"

			(( cur_level >= min_level )) \
				&& return 0  \
				|| return 1
			;;

		gettype)
			if [ "${1:-}" ]; then
				__validate_verbosity_channel_name "$1"
				name="$1"
			else
				name=$(__get_current_verbosity_channel_name)
			fi
			echo "${VERBOSITY_CHANNELS_TYPES[$name]}"
			;;

		setup)
			#  This case is more complex, so all validation is done inside.
			__set_up_verbosity_channel "$@"
			;;

		addtogroup)
			#  This case is more complex, so all validation is done inside.
			__add_verbosity_channel_to_group "${1:-}" "${2:-}"  # As is!
			;;

		list)
			__list_verbosity_channels
			;;

		describe)
			__describe_verbosity_channel "${1:-}"
			;;

		detect_unknown_channels)
			__detect_unknown_verbosity_channels
			;;

		help)
			cat <<-EOF

			                         VCHAN

			A set of commands to control verbosity channels.

			    vchan getname

			    vchan getlevel [channel_name]

			    vchan setlevel [channel_name] <level>

			    vchan iflevel [channel_name] <eq_level>

			    vchan minlevel [channel_name] <min_level>

			    vchan setup name=<channel_name>          \\
			                level=<0…99>                 \\
			                meta=<M>                     \\
			                group_items=<chA,chB,…,chN>  \\
			                level_locked=<L>             \\
			                hint=<any string*>

			    vchan addtogroup name=<channel_name>  \\
			                     group=<group_name>

			    vchan delete <channel_name>

			    vchan list

			    vchan describe <channel_name>

			    vchan detect_unknown_verbosity_channels

			Call “vchan help-COMMAND” to see the description of a particular COMMAND.
			EOF
			;;

		help-getname)
			cat <<-EOF

			                        VCHAN GETNAME

			Description
			    Retrieves the name of the verbosity channel, that would be
			    applied at this point in code.

			Syntax
			    vchan getname

			    There is no options to this command.

			EOF
			;;

		help-getlevel)
			cat <<-EOF

			                          VCHAN GETLEVEL

			Description
			    Retrieves the level of the specified channel or, if no name is
			    specified, returns the level of the channel currently in effect.

			Syntax
			    vchan getlevel [channel_name]

			Options
			    “channel_name” – an existing channel’s name.
			EOF
			;;

		help-setlevel)
			cat <<-EOF

			                          VCHAN SETLEVEL

			Description
			    Sets the level of the specified channel name or, if no name
			    is specified, change the verbosity level of the channel that is
			    currently in effect.

			Syntax
			    vchan setlevel [channel_name] <level>

			Options
			    “channel_name” – an existing channel’s name.
			    “level” – a number in range 0…99.
			EOF
			;;

		help-iflevel)
			cat <<-EOF

			                          VCHAN IFLEVEL

			Description
			    Returns true, if the verbosity level of the specified channel
			    matches the specified number exactly – and false, if they don’t
			    match. If no channel name is specified, the channel currently
			    in effect is assumed.

			Syntax
			    vchan iflevel [channel_name] <level>

			Options
			    “channel_name” – an existing channel’s name.
			    “level” – a number in range 0…99.
			EOF
			;;

		help-minlevel)
			cat <<-EOF

			                          VCHAN MINLEVEL

			Description
			    Returns true, if the verbosity level of the speficied channel
			    is high enough to match the specified number or exceeds it.
			    If the channel name is not specified, the channel that is cur-
			    rently in effect is assumed.

			Syntax
			    vchan minlevel [channel_name] <level>

			Options
			    “channel_name” – an existing channel’s name.
			    “level” – a number in range 0…99.
			EOF
			;;

		help-setup)
			cat <<-EOF

			                             VCHAN SETUP

			Description
			    Sets up a verbosity channel, creating either an abstract entity
			    (that has to be mentioned by name in the code) or an entity bound
			    to a specific function (in this case their names must match). Cre-
			    ated channel will affect that function and all functions called
			    from within it.

			Syntax
			    vchan setup name=<channel_name>           \\
			               [level=<0…99>]                 \\
			               [meta="M"]                     \\
			               [group_items=<chA,chB,…,chN>]  \\
			               [level_locked="L"]             \\
			               [hint=<any string*>]

			    The order of options is free.

			Options
			    name – a string, that may consist of Latin letters, numbers and
			        underscore character. Letters may be of any case. Name cannot
			        start with a number.
			    level – sets the initial level for the channel. Must be a number
			        in range 0…99. If not specified, defaults to 3. Levels lower
			        than 3 are considered suppressed, and those higher are verbose.
			    meta – if set, creates an abstract entity, not bound to any
			        function name. If not set, the channel name will be matched
			        against function names in the \$FUNCNAME call stack.
			    group_items – the list of comma-separated channel (name)s, that will
			        become group members of the channel. Group members copy the ver-
			        bosity level of the master channel, always match function names.
			        Usually group members are used with the “meta” channels to
			        unite the verbosity output of several functions in a module under
			        a single channel.
			    level_locked – if set, locks the level and prevents its changing.
			        If not set, the level can be changed at any time. This is usually
			        set for output channels, that either close output irreversibly
			        or turn a greater verbosity on, that cannot be silenced afterwards.
			    hint – the option exists for Bahelite’s own channels and is used
			        to group channels nicely by their purpose in the output of
			        “vchan list”.

			        *The field may be used freely by the main script channels, as long
			        as they do not assume Bahelite hints (“output” and “self-report”)
			        for themselves (this will mess up the output of “vchan list”, that
			        uses hints to separate Behelite channels from the main script ones).
			EOF
			;;

		help-addtogroup)
			cat <<-EOF

			                          VCHAN ADDTOGROUP

			Description
			    Adds channel to a group. The channel that is added cannot be an
			    existing channel itself. Neither a stand-alone one, nor a group,
			    nor a member of another group. It must be created by this command
			    as a member of a group. The group it is added to must exist. Any
			    existing channel is suitable to be expanded to a group with this
			    function (if it wasn’t created with group members via “vchan setup”).

			Syntax
			    vchan addtogroup name=<channel_name>  \\
			                     group=<group_name>

			Options
			    name – a string, that may consist of Latin letters, numbers and
			        underscore character. Letters may be of any case. Name cannot
			        start with a number.
			    group – name of an existing channel.
			EOF
			;;

		help-list)
			cat <<-EOF

			                           VCHAN LIST

			Description
			    List all known verbosity channels with their attributes.

			Syntax
			    vchan list

			    There are no options.
			EOF
			;;

		help-describe)
			cat <<-EOF

			                        VCHAN DESCRIBE

			Description
			    This command is a debuging helper. It lists all mentions of the
			    specified verbosity channel in the internal arrays.

			Syntax
			    vchan describe <channel_name>

			Options
			    “channel_name” – an existing channel’s name.
			EOF
			;;

		help-detect[_-]unknown[_-]channels)
			cat <<-EOF

			                  VCHAN DETECT UNKNOWN CHANNELS

			Description
			    Shows channel names, whose hint is still ‘?’ at the moment when
			    this request is made. At the initialisation stage, all channels
			    specified from the environment variable VERBOSITY or via the
			    command line option --verbosity are set up “in advance”, that is,
			    before they are properly initialised for the first time. And all
			    such channels receive the ‘?’ hint, designated them as “poten-
			    tially unknown”. The hint is then taken off (or replaced), when
			    the channels is set up in due time. If no setup procedure would
			    thus claim the channel, it remains in its incomplete setup state,
			    and its hint remains ‘?’. When the “vchan list” command runs,
			    it checks this hint and shows unclaimed channels, suggesting to
			    check, if the name was spelled properly (as the case matters).
			    “vchan detect_unknown_channels” does exactly that, and is also
			    intended to run before program quits.

			Syntax
			    vchan detect_unknown_channels

			EOF
			;;

		*)
			redmsg "Unknown command to vchan: “$command”.\n"
			vchan help
			echo
			err "vchan: no such command: “$command”."
			;;
	esac

	(( v_level >= 3 ))  && {
		info "Exiting “vchan $command”."
		echo
		mildec
	}
	return 0
}
export -f  vchan



__bahelite_set_up_postponed_channels() {
	local func
	for func in "${VERBOSITY_POSTPONED_SETUP_FUNCS[@]}"; { $func; }
	return 0
}

__bahelite_set_up_postponed_channels


                         #  Stream control  #

 # On the purpose of STD*_ORIG_FD and STD*_ORIG_FD_PATH
#
#  First of all, they have different purposes. And though it may seem like
#  an *_FD_PATH variable simply provides the same path as the descriptor,
#  that the corresponding *_ORIG_FD is pointing to, it’s not like that. They
#  may contain different paths, and that’s why there are two variables for
#  each of stdin, stdout and stderr.
#
#  The purpose is best illustrated with ./tests/STD\*_FD\*_test.sh
#
#  These variables keep pointers to the original console stdin, stdout and
#  stderr descriptors before and after they were duplicated or replaced with
#  a pipe. The “logging” module, for example, replaces the path of file de-
#  scriptors stdout and stderr to pipes, which send one copy of the output
#  to console, and the other to the log file. Subshells are connected – via
#  pipe again – to their parent shell. Only stdout gets replaced in that case,
#  though. But that means, that a message from err() would go directly to the
#  parent shell’s stderr, but a message from info() would be caught on the way.
#  And because info messages are not data, nobody wants them captured. They
#  should go to console and logs. For these kinds of situations STDOUT_ORIG_FD
#  and STDOUT_ORIG_FD_PATH are convenient to have. Technically, there’s rarely,
#  if ever, appears a need to peruse STDERR_* or STDIN_* variables, but they
#  have their descriptor numbers and paths stored because it’s better to have
#  them, if they will be needed, as this costs basically nothing.
#
#       STDIN_ORIG_FD        These always point to the copy of an original de-
#       STDOUT_ORIG_FD         scriptor, a copy that was made at startup (it’s
#       STDERR_ORIG_FD         right below, actually). And those copies always
#                              point to the terminal’s stdin, stdout and stderr,
#                              before any sort of replacement could occur.
#                            They store /a number/ which represents a filename
#                              in /proc/$$/fd/.
#
#       STDIN_ORIG_FD_PATH       These variables point to the /descriptors’
#       STDOUT_ORIG_FD_PATH        original paths/. At first this may seem
#       STDERR_ORIG_FD_PATH        that /exactly these variables contain the
#                                  very original paths (to the terminal, that
#                                  is), and the *_ORIG_FD are those whose
#                                  values are more “volatile”.
#                                It isn’t like that.
#                                The *_ORIG_FD_PATH ones point to /that path,
#                                  which the original descriptor (the numbered
#                                  file in /proc/$$/fd/) pointed to.
# STD*_ORIG_FD contains
#

 # Remembering the original FD paths. They are needed to send info, warn etc.
#  messages from subshells properly.
#
#  When you’re using STD**_ORIG_FD_PATH, the similarity between
#
#      >&$STDOUT(ERR)_ORIG_FD_PATH
#  and
#      >$STDOUT(ERR)_ORIG_FD_PATH
#
#  may be confusing. YOU SHOULDN’T BE USING THE LATTER FORM. EVER. This will
#  lead to overwriting the log file. You should use
#
#      >>$STDOUT(ERR)_ORIG_FD_PATH
#
#  To make things clear: >&2 doesn’t /write/ anything, it only tosses around
#  where a link in /proc/$$/fd/ points to.
#
if (( BASH_SUBSHELL == 0 )); then
	declare -gx STDIN_ORIG_FD_PATH="/proc/$$/fd/0"
	declare -gx STDOUT_ORIG_FD_PATH="/proc/$$/fd/1"
	declare -gx STDERR_ORIG_FD_PATH="/proc/$$/fd/2"
else
	[ -v STDIN_ORIG_FD_PATH ]   \
		|| declare -gx STDIN_ORIG_FD_PATH="/proc/$$/fd/0"
	[ -v STDOUT_ORIG_FD_PATH ]  \
		|| declare -gx STDOUT_ORIG_FD_PATH="/proc/$$/fd/1"
	[ -v STDERR_ORIG_FD_PATH ]  \
		|| declare -gx STDERR_ORIG_FD_PATH="/proc/$$/fd/2"
fi


 # Saving the original destinations of console stdout and stderr,
#  so that they may be used to bypass writing some temporary information
#  like a progress bar to the log file, and also for the logging module
#  to have these in case console verbosity would redirect stdout and stderr
#  to /dev/null.
#
exec {STDIN_ORIG_FD}>&0
exec {STDOUT_ORIG_FD}>&1
exec {STDERR_ORIG_FD}>&2

case "${VERBOSITY_CHANNELS[console]}" in
	0)	exec 1>/dev/null
		exec 2>/dev/null

		 # If the logging module would need to log stdout, it would need
		#  to know, that it must grab the handle not from FD 1, but from
		#  FD $STDOUT_ORIG_FD. Same for stderr.
		#
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL=t
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDERR_TO_DEVNULL=t
		;;

	1)	exec 1>/dev/null
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL=t
		;;
esac
VERBOSITY_CHANNELS_LEVEL_IS_LOCKED[console]='L'


return 0
