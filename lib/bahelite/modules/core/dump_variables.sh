#  Should be sourced.

#  dump_variables.sh
#  This module provides an output channel to store the list
#    of created variables at the time of exit. Values are
#    included in the list.
#  Author: deterenkelt, 2018–2023
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_DUMP_VARIABLES_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_DUMP_VARIABLES_VER='1.0.1'

 # Facility’s verbosity channel
#  0 – no dump (the default);
#  1 – write the “variables” file on exit.
#  2 – write the “variables” file and also “variables_full”, the latter
#      will contain a bigger list, that includes not only the main prog-
#      ram variables, but also variables of the Bahelite modules.
#
vchan setup  name=dump_vars  \
             level=0         \
             meta=M          \
             hint=output     \
             min_level=0     \
             max_level=2



 # This global variable is actually created at startup time in the Bahelite
#  core file, i.e. “bahelite.sh”, and immediately set to read-only. This list
#  isn’t used anywhere, but it’s handy to have it for looking into the start-
#  ing environment, which the script receives. Especially, when one script
#  runs another with “env”, and the environment gets sanitised.
#
declare -g BAHELITE_VARLIST_BEFORE_STARTUP


 # The initial list of variables, which will be compared later to the list
#  composed on exit. The difference between the two lists would help to
#  filter off Bahelite’s own varaibles and provide a list of variables
#  created by the main program. See the details below.
#
declare -g BAHELITE_VARLIST_AFTER_STARTUP


 # This flag enforces the creation of a variable dump, as if the “dump_vars”
#  level was increased to 1. Two situations enable variable dump:
#    ⋅ when the program has encountered an error;
#    ⋅ when the level of “dump_vars” verbosity channel is set to 1 or 2.
#  To provide a unified way, this glabacl variable is used.
#
declare -g BAHELITE_DUMP_VARIABLES
#
#  When this flag is additionally set to the previous one, creates a second
#  dump file, which contains not only main script’s but Bahelite variables
#  too.
#
declare -g BAHELITE_DUMP_VARIABLES_FULL


 # Before the main script starts, gather variables.
#  This list would be compared to the other, created before exiting, and
#  the diff will be placed in "$LOGDIR/variables".
#
#  It’s important, that gathering the …AFTER_STARTUP list happens /before/
#  running postload jobs. Because postload jobs may load configuration files,
#  journals and such, which would bring their own variables. And in order
#  for these variables to appear in the dump, they must be present /both at
#  the time of populating …AFTER_STARTUP list and when the program quits/.
#
#  The thing is that the variable dump, as it is implemented now, is meant
#  to provide the dump for the variables of teh main script, that is, sans
#  the Bahelite’s own stuff. And the reason for gathering the …AFTER_STARTUP
#  list first is to run “uniq” on that list and that which will be gathered
#  on exit, so that the resulting list could be comprised of only the vari-
#  ables, that were created by the main script itself.
#
#  The “run time” stuff, such as things loaded in postload jobs are consi-
#  dered “user space” in this regard, as they might probably contain user-
#  altered data, which may be important for debugging.
#
#  Technically, this command might be executed
#    - from here as is, but then, due to this very module running within
#      the “load_modules” framework, some local variables will get into
#      the …AFTER_STARTUP list, which would have to be erased from it
#      (as they wouldn’t be found by “declare -p” and will trigger an error);
#    - from postload jobs, but then this job must guaranteely run as the
#      very first job (which cannot be guaranteed). And also this would
#      create a similar problem to the above, because of running within
#      a framework again.
#
bahelite_gather_the_after_startup_varlist() {
	declare -gr BAHELITE_VARLIST_AFTER_STARTUP="$(compgen -A variable)"
}


show_help_on_verbosity_channel_dump_vars() {
	cat <<-EOF
	dump_vars (output channel)

	At the time when the program quits, gathers all created variables and
	dumps their names and values to a file called “${MYNAME_NOEXT}_variables…”.
	The file would be created
	  - in the log directory, if the “logging” module is used;
	  - in the temporary directory, if logging isn’t enabled (the
	    temporary directory is normally wiped on exit, but in this
	    case it’ll stay);
	  - if neither directory is present and writeable, file is not created.

	Levels:
	  0 – no dump (the default).
	  1 – write the “variables” file on exit.
	  2 – write the “variables” file and also “variables_full”, the latter
	      will contain a bigger list, that includes not only the main prog-
	      ram variables, but also variables of the Bahelite modules.
	EOF
}


 # This function is called from “bahelite_on_exit()” in the “error_handling”
#  module.
#
bahelite_dump_variables_on_exit() {
	declare -g BAHELITE_VARLIST_AFTER_STARTUP
	declare -g BAHELITE_DONT_CLEAR_TMPDIR
	declare -g BAHELITE_DUMP_VARIABLES
	declare -g BAHELITE_DUMP_VARIABLES_FULL

	local  current_varlist
	local  variables_not_to_print
	local  new_variables
	local  fname="$MYNAME_NOEXT.$MY_LAUNCH_TIME.variables"
	local  fname_full="$MYNAME_NOEXT.$MY_LAUNCH_TIME.variables_full"
	local  dump_fpath
	local  dump_fpath_full

	[ -v BAHELITE_DUMP_VARIABLES ]  || return 0

	 # This list shouldn’t have variables, that were read
	#  from (user’s) RC file.
	#
	current_varlist=$(compgen -A variable)

	 # Pattern list for the variables that shouldn’t be logged
	#
	variables_not_to_print=(
		'BAHELITE_(EXIT|ERROR)_PROCESSED'

		#  Only BAHELITE_VARLIST_AFTER_STARTUP is removed, because the
		#  output is going to get uniq’ed, and the …AFTER_STARTUP variable
		#  would be present only once, while …BEFORE_STARTUP will be there
		#  twice and thus removed automatically by “uniq -u”.
		#
		'BAHELITE_VARLIST_AFTER_STARTUP'

		#  Any module that was loaded after Bahelite finished startup
		#  (this is common for optional modules), will declare its pre-
		#  sence after BAHELITE_VARLIST_BEFORE_STARTUP is set, thus some
		#  presence declaration variables will not be auto-nullified.
		#
		'BAHELITE_MODULE_.*_VER'

		#  Obviously we have it set if variables are going to be printed.
		#
		'BAHELITE_DUMP_VARIABLES'
		'BAHELITE_DUMP_VARIABLES_FULL'

		#  Avoid adding variables from this function as well.
		#
		'current_varlist'
		'variables_not_to_print'
	)

	new_variables=$(
		echo "${BAHELITE_VARLIST_AFTER_STARTUP:-}"$'\n'"$current_varlist"  \
		    | grep -vE "($(IFS='|'; echo "${variables_not_to_print[*]}"))"  \
		    | sort | uniq -u | sort
	)

	if [ -d "${LOGDIR:-}" -a -w "${LOGDIR:-}" ]; then
		dump_fpath="$LOGDIR/$fname"
		dump_fpath_full="$LOGDIR/$fname_full"

	elif [ -d "${TMPDIR:-}" -a -w "${TMPDIR:-}" ]; then
		dump_fpath="$TMPDIR/$fname"
		dump_fpath_full="$TMPDIR/$fname_full"
		#
		#  It’s a bad idea to force TMPDIR to stay, as not all errors (that
		#  trigger this function) are to be looked into – plenty of runs would
		#  just spam temporary folders in TMPDIR. So it’s more reasonable to
		#  rely on the specific request by the user – by setting “tmpdir” ver-
		#  bosity channel to 1.
		#
		# BAHELITE_DONT_CLEAR_TMPDIR=t

	else
		redmsg "dump_vars: couldn’t create dump file: neither LOGDIR nor
		TMPDIR are available for writing to."
	fi

	#  God bless 2 GiB command lines
	#
	[ "${dump_fpath:-}" ]  && {
		declare -p ${new_variables//$'\n'/ }  >"$dump_fpath"
		[ -v BAHELITE_DUMP_VARIABLES_FULL ]  \
			&& declare -p ${current_varlist//$'\n'/ }  >"$dump_fpath_full"
	}

	return 0
}


(( VERBOSITY_CHANNELS[dump_vars] != 0 ))  \
	&& BAHELITE_DUMP_VARIABLES=t

(( VERBOSITY_CHANNELS[dump_vars] > 1 ))  \
	&& BAHELITE_DUMP_VARIABLES_FULL=t


return 0