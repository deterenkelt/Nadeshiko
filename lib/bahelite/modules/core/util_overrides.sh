#  Should be sourced.

#  util_overrides.sh
#  Overrides for the set built-in – for internal use
#    within Bahelite and helpers for the main script.
#  Author: deterenkelt, 2019–2023
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_UTIL_OVERRIDES_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_UTIL_OVERRIDES_VER='1.3.0'

 # No self-report verbosity channel:
#  1. The code here demands as few moves as possible to be unobtrusive.
#     Handling xtrace output will suffer in particular.
#  2. More verbosity clutter would significantly worsen understanding
#     (and especially the understanding of an actual xtrace output from
#     Bahelite). Also due to the requirements of being terse, the code
#     must remain minimalistic.
#


__show_usage_util_overrides_module() {
	cat <<-EOF
	Bahelite module “util_overrides” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_util_overrides_module
		exit 0

	else
		redmsg "Bahelite: util_overrides: unrecognised option “$opt”."
		__show_usage_util_overrides_module
		exit 4
	fi
done
unset opt



                  #  Functions altering shell flags  #

 # The helper functions below make it possible to switch a shell option tem-
#  poraily on or off, without having to remember the global state of the
#  shell flag (or the current state). For example, it’s possible to have
#
#      set -feEuT
#
#  in the beginning of the main module, and in another module:
#
#      noglob_off
#      while file in Jan9*; do
#          …
#      done
#      noglob_rewind
#
#  The -f switch forbids the shell expanding the asterisk (*) into file names
#  withing the current directory. As you would put shell variables in double
#  quotes, when taking their values, an unexpected asterisk might pose a prob-
#  lem. Thus it’s better to have this kind of expansion turned off by default.
#  But what if you need this asterisk expansion just for one time? When you
#  know exactly what files might be there? Then you would like to have the
#  expansion back and after using it, return the shell flag to the global
#  state (i.e. disabled).
#    The functions in this module remember the original state and allow one
#  to rewind the state after the specific part of code. These pairs, which
#  temporarily enable of disable the flag, can be nested: there is a stack
#  of states for each controlled option.
#

 # On errexit, functrace and xtrace shell flags
#  (This information is important only if you’re going to look into Bahelite
#  internals. If you just write programs with Bahelite, you can skip this.)
#
#  Errexit and functrace flags are bound to signal traps. Bahelite traps ERR
#  and DEBUG for better error handling and thus turning the flag on and off
#  requires to also place or remove the trap accordingly to the state. For
#  this reason, the function which remember and restore option state also
#  enable and disable traps (if defined), together with the flag.
#
#  If you’re debugging with “xtrace”, REMEMBER TO DISABLE THE TRAP ON DEBUG.
#  You can do that with functrace_off and bring it back with functrace_rewind.
#

 # For some functions the declarations could be unified and automated, but
#  it was decided to write them out, because the defferences are plenty and…
#  different by nature:
#    - some shell options related to debugging need to affect the corres-
#      ponding trap functions (trapped signals introduced in Bahelite);
#    - most shell options are enabled and disabled with the “set” builtin,
#      but some can only be controlled with “shopt”;
#    - some options, such as “xtrace” affect the internal mechanics of Ba-
#      helite modules (which expand on them), thus switching such an option
#      should also take into account the rest, which depends on it.
#

 # On “set” and “shopt” builtins
#
#  The “set” builtin is older, it was the generic means to control the shell
#  options for a long time. The primary set of options, which it controls,
#  is exposed through different handles, such as the “-o” parameter for the
#  /bin/bash executable, and the [ -o shell_option ] expression, which allows
#  to test, whether a primary shell option is enabled or not.
#
#  “shopt” is a newer builtin, which retains control of all the older options
#  while adding new ones. The new options ARE NOT available for “/bin/bash -o”
#  and “[ -o … ]”. And though “shopt” CAN set and unset primary options avail-
#  able for “set”, it won’t do so, unless you specify the “-o” parameter for
#  “shopt”, so “shopt pipefail” will report about “no such option”, while
#  “shopt -o pipefail” would receive information like “[ -o pipefail ]” does.
#

 # *_on, *_off, *_rewind
#
#  Use *_on or *_off to switch the shell option to desired state, then some
#  lines later, when you don’t need it anymore, call the *_rewind function.
#

 # To turn extglob on or off temporarily.
#  (Controls ?(…) *(…) +(…) @{…} and !(…) in regular expressions.)
#
declare -gx shopt_extglob_stack=()
#
extglob_on() { __bahelite_extglob; }
extglob_off() { __bahelite_extglob; }
extglob_rewind() { __bahelite_extglob; }
__bahelite_extglob() {
	declare -g  shopt_extglob_stack
	local mode=${FUNCNAME[1]##*_}

	case "$mode" in
		on)
			if shopt -q extglob; then
				shopt_extglob_stack+=( noop )
			else
				shopt_extglob_stack+=( off )
				builtin shopt -s extglob
			fi
			;;
		off)
			if shopt -q extglob; then
				shopt_extglob_stack+=( on )
				builtin shopt -u extglob
			else
				shopt_extglob_stack+=( noop )
			fi
			;;
		rewind)
			(( ${#shopt_extglob_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_extglob_stack[-1]}" in
				on)
					builtin shopt -s extglob ;;
				off)
					builtin shopt -u extglob ;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_extglob_stack[-1]
			;;
	esac

	return 0
}
export -f  extglob_on    \
           extglob_off    \
           extglob_rewind  \
           __bahelite_extglob


 # To turn globstar on or off temporarily.
#  (Controls the double asterisk “**” as in /path/** )
#
declare -gx shopt_globstar_stack=()
#
globstar_on() { __bahelite_globstar; }
globstar_off() { __bahelite_globstar; }
globstar_rewind() { __bahelite_globstar; }
__bahelite_globstar() {
	declare -g  shopt_globstar_stack
	local mode=${FUNCNAME[1]##*_}

	case "$mode" in
		on)
			if shopt -q globstar; then
				shopt_globstar_stack+=( noop )
			else
				shopt_globstar_stack+=( off )
				builtin shopt -s globstar
			fi
			;;
		off)
			if shopt -q globstar; then
				shopt_globstar_stack+=( on )
				builtin shopt -u globstar
			else
				shopt_globstar_stack+=( noop )
			fi
			;;
		rewind)
			(( ${#shopt_globstar_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_globstar_stack[-1]}" in
				on)
					builtin shopt -s globstar ;;
				off)
					builtin shopt -u globstar ;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_globstar_stack[-1]
			;;
	esac

	return 0
}
export -f  globstar_on    \
           globstar_off    \
           globstar_rewind  \
           __bahelite_globstar


 # To turn noglob (set -f) on or off temporarily.
#  This comes handy when shell needs to use globbing like for “ls *.sh”,
#  but it is disabled by default for safety.
#
declare -gx shopt_noglob_stack=()
#
noglob_on() { __bahelite_noglob; }
noglob_off() { __bahelite_noglob; }
noglob_rewind() { __bahelite_noglob; }
__bahelite_noglob() {
	declare -g  shopt_noglob_stack
	local mode=${FUNCNAME[1]##*_}

	case "$mode" in
		on)
			if [ -o noglob ]; then
				shopt_noglob_stack+=( noop )
			else
				shopt_noglob_stack+=( off )
				builtin shopt -so noglob
			fi
			;;
		off)
			if [ -o noglob ]; then
				shopt_noglob_stack+=( on )
				builtin shopt -uo noglob
			else
				shopt_noglob_stack+=( noop )
			fi
			;;
		rewind)
			(( ${#shopt_noglob_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_noglob_stack[-1]}" in
				on)
					builtin shopt -so noglob ;;
				off)
					builtin shopt -uo noglob ;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_noglob_stack[-1]
			;;
	esac

	return 0
}
export -f  noglob_on    \
           noglob_off    \
           noglob_rewind  \
           __bahelite_noglob


 # To turn nocasematch on or off temporarily.
#  This comes handy when shell needs to use globbing like for “ls *.sh”,
#  but it is disabled by default for safety.
#
declare -gx shopt_nocasematch_stack=()
#
nocasematch_on() { __bahelite_nocasematch; }
nocasematch_off() { __bahelite_nocasematch; }
nocasematch_rewind() { __bahelite_nocasematch; }
__bahelite_nocasematch() {
	declare -g  shopt_nocasematch_stack
	local mode=${FUNCNAME[1]##*_}

	case "$mode" in
		on)
			if shopt -q nocasematch; then
				shopt_nocasematch_stack+=( noop )
			else
				shopt_nocasematch_stack+=( off )
				builtin shopt -s nocasematch
			fi
			;;
		off)
			if shopt -q nocasematch; then
				shopt_nocasematch_stack+=( on )
				builtin shopt -u nocasematch
			else
				shopt_nocasematch_stack+=( noop )
			fi
			;;
		rewind)
			(( ${#shopt_nocasematch_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_nocasematch_stack[-1]}" in
				on)
					builtin shopt -s nocasematch ;;
				off)
					builtin shopt -u nocasematch ;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_nocasematch_stack[-1]
			;;
	esac

	return 0
}
export -f  nocasematch_on    \
           nocasematch_off    \
           nocasematch_rewind  \
           __bahelite_nocasematch


 # To turn errexit (set -e) on or off temporarily and switch the trap on ERR
#  signal accordingly to the shell option state (on – on, off – off).
#
#  Bahelite traps the ERR signal to handle the call stack, when an error
#  occurs. The function “bahelite_toggle_onerror_trap” is defined in the
#  “error_handling” module.
#
declare -gx shopt_errexit_stack=()
#
errexit_on() { __bahelite_errexit; }
errexit_off() { __bahelite_errexit; }
errexit_rewind() { __bahelite_errexit; }
__bahelite_errexit() {
	declare -g  shopt_errexit_stack
	local  mode=${FUNCNAME[1]##*_}
	local  toggle_trap_func

	[ "$(type -t bahelite_toggle_onerror_trap)" = 'function' ]  \
		&& toggle_trap_func=t

	case "$mode" in
		on)
			if [ -o errexit ]; then
				shopt_errexit_stack+=( noop )
			else
				shopt_errexit_stack+=( off )
				builtin shopt -so errexit
				[ -v toggle_trap_func ]  \
					&& bahelite_toggle_onerror_trap  set
			fi
			;;

		off)
			if [ -o errexit ]; then
				shopt_errexit_stack+=( on )
				builtin shopt -uo errexit
				[ -v toggle_trap_func ]  \
					&& bahelite_toggle_onerror_trap  unset
			else
				shopt_errexit_stack+=( noop )
			fi
			;;

		rewind)
			(( ${#shopt_errexit_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_errexit_stack[-1]}" in
				on)
					builtin shopt -so errexit
					[ -v toggle_trap_func ]  \
						&& bahelite_toggle_onerror_trap  set
					;;
				off)
					builtin shopt -uo errexit
					[ -v toggle_trap_func ]  \
						&& bahelite_toggle_onerror_trap  unset
					;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_errexit_stack[-1]
			;;
	esac

	return 0
}
export -f  errexit_on    \
           errexit_off    \
           errexit_rewind  \
           __bahelite_errexit


 # To turn functrace (set -T) on or off temporarily and switch the trap on
#  DEBUG signal accordingly to the shell option state (on – on, off – off).
#
#  Switching this option might come helpful when suspecting an issue with
#  subshells or pipes.
#
#  Bahelite traps the DEBUG signal in the “error_handling” module to catch
#  errors better. The “bahelite_toggle_ondebug_trap” function is defined
#  in that module. See also the commentary above.
#
declare -gx shopt_functrace_stack=()
#
functrace_on() { __bahelite_functrace; }
functrace_off() { __bahelite_functrace; }
functrace_rewind() { __bahelite_functrace; }
__bahelite_functrace() {
	declare -g  shopt_functrace_stack
	local  mode=${FUNCNAME[1]##*_}
	local  toggle_trap_func

	[ "$(type -t bahelite_toggle_ondebug_trap)" = 'function' ]  \
		&& toggle_trap_func=t

	case "$mode" in
		on)
			if [ -o functrace ]; then
				shopt_functrace_stack+=( noop )
			else
				shopt_functrace_stack+=( off )
				builtin shopt -so functrace
				[ -v toggle_trap_func ]  \
					&& bahelite_toggle_ondebug_trap  set
			fi
			;;

		off)
			if [ -o functrace ]; then
				shopt_functrace_stack+=( on )
				builtin shopt -uo functrace
				[ -v toggle_trap_func ]  \
					&& bahelite_toggle_ondebug_trap  unset
			else
				shopt_functrace_stack+=( noop )
			fi
			;;

		rewind)
			(( ${#shopt_functrace_stack[*]} == 0 )) && {
				err "${FUNCNAME[0]}: ERROR: shell option value stack is empty,
				     when it shouldn’t! Cannot rewind."
			}
			case "${shopt_functrace_stack[-1]}" in
				on)
					builtin shopt -so functrace
					[ -v toggle_trap_func ]  \
						&& bahelite_toggle_ondebug_trap  set
					;;
				off)
					builtin shopt -uo functrace
					[ -v toggle_trap_func ]  \
						&& bahelite_toggle_ondebug_trap  unset
					;;
				noop)
					: "no operation, nothing to do." ;;
			esac
			unset shopt_functrace_stack[-1]
			;;
	esac

	return 0
}
export -f  functrace_on    \
           functrace_off    \
           functrace_rewind  \
           __bahelite_functrace


 # Overrides env to allow running a child process in an environment, stripped
#  of Bahelite additions. (It’d be wrong to use “env -i”, as we’d get a COM-
#  PLETELY clean environment, which would lack important bash globals related
#  to both the shell and the terminal capabilities. There wouldn’t be even
#  $HOME defined.)
#
#  All the variables, that appeared since Bahelite was loaded are passed to
#  env with “-u” flag to unset them. Moreover, variables preset for Bahelite
#  are removed too (it’s those variables, that can be set *before* sourcing
#  bahelite.sh to alternate its behaviour).
#
env() {
	local  current_varlist
	local  new_vars
	local  retval

	current_varlist=$(compgen -A variable)
	new_vars=(
		$(
			echo "$BAHELITE_VARLIST_BEFORE_STARTUP"$'\n'"$current_varlist" \
				| sort | uniq -u | sort
		)
		${!BAHELITE_*}  ${!MSG_*}  LOGPATH  LOGDIR  TMPDIR
	)

	#  Other variables for removal, that could be set before Bahelite
	#  startup procedure, hence may not appear in the VARLIST_BEFORE_STARTUP
	#  variable, that actually collects variables at the time of startup.
	#
	functrace_off

	command env $(sed -r 's/\S+/-u &/g' <<<"${new_vars[*]}") \
	            TERM_COLUMNS=${TERM_COLUMNS:-80}              \
	            TERM_LINES=${TERM_LINES:-25}                  \
	            STDIN_ORIG_FD_PATH="$STDIN_ORIG_FD_PATH"     \
	            STDOUT_ORIG_FD_PATH="$STDOUT_ORIG_FD_PATH"   \
	            STDERR_ORIG_FD_PATH="$STDERR_ORIG_FD_PATH"    \
	            MSG_INDENTATION_LEVEL="$MSG_INDENTATION_LEVEL" \
	            "$@"

	retval=$?
	functrace_rewind

	return $retval
}
export -f  env


 # Overridden “read” builtin is to circumvent a bug. When the “logging” module
#  is enabled, the “read” builtin somehow can’t use proper stdout any more
#  (yes, stdout). The snippet with a herestring fixes the bug, which is at run-
#  time caused by a “< <(…)” expression in the main script code. See the details
#  in the comment to “start_logging()” in the “logging” module.
#
read() {
	(<<<'')
	builtin read "$@"
}
export -f read


return 0