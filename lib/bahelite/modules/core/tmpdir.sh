#  Should be sourced.

#  tmpdir.sh
#  Set up a temporary directory for the main script and set TMPDIR
#    variable. The path would be /tmp/$MYNAME_NOEXT.XXXXXXXXXX .
#  Author: deterenkelt, 2019–2023
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_TMPDIR_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_TMPDIR_VER='1.1.3'


 # Facility’s verbosity channel
#  0 – the default operational mode
#  1 – verbose mode: do not remove the directory for temporary files on exit.
#
#      This is implemented as a verbosity level (and not as a module option),
#      because the need to leave TMPDIR would happen only occasionally. The
#      end users would have less troubles by accessing this feature via non-
#      intrusive means like changing verbosity level, instead of having to
#      edit the code of the program. (That action may be dangerous for non-
#      professionals, and the necessity to revert the changes thereafter
#      makes it double dangerous for them. In case when updates are performed
#      via “git pull” it would be critical to have a clean, non-edited state
#      for the files.)
#
vchan setup  name=tmpdir  \
             level=0      \
             meta=M       \
             hint=output  \
             min_level=0  \
             max_level=1  \
             level_is_locked='L'


show_help_on_verbosity_channel_tmpdir() {
	cat <<-EOF
	tmpdir
	    Controls the persistence of TMPDIR ($MYNAME’s directory
	    for temporary files).
	    Levels:
	    0 – delete TMPDIR on exit (the default).
	    1 – keep TMPDIR on exit.

	The path is usually /tmp/$MYNAME_NOEXT.XXXXXXXXXX

	In case when “dump_vars” output channel is set to 1, the temporary
	directory is forced to stay.
	EOF
}


 # Module’s self-report verbosity channel
#
vchan setup  name=Tmpdir     \
             level=2          \
             meta=M            \
             hint=self-report  \
             min_level=2       \
             max_level=4


show_help_on_verbosity_channel_Tmpdir() {
	cat <<-EOF
	Tmpdir (self-report channel)
	    Levels:
	    2 – no additional messages (the default)
	    3 – no additional messages on this level either.
	    4 – show details on how the directory is created and deleted.

	EOF
}


__show_usage_tmpdir_module() {
	cat <<-EOF
	Bahelite module “tmpdir” doesn’t take options!
	Tips:
	  - to set a custom path, export it in TMPDIR variable (the temporary
	    directory will be created as a folder /inside/ the specified path);
	  - to avoid deletion of TMPDIR on exit, export VERBOSITY=tmpdir=1.
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_tmpdir_module
		exit 0

	else
		redmsg "Bahelite: tmpdir: unrecognised option “$opt”."
		__show_usage_tmpdir_module
		exit 4
	fi
done
unset opt


 # Flag to prevent the deletion of TMPDIR on exit.
#
#  It’s set when “tmpdir” verbosity level is raised to 1. Useful for
#  debugging. Essential, if logs had no other place to be written to,
#  as to TMPDIR/logs.
#
declare -g BAHELITE_DONT_CLEAR_TMPDIR



 # Create a unique subdirectory for temporary files
#
#  It’s used by Bahelite and the main script. bahelite_on_exit() will call
#    this function’s counterpart, bahelite_delete_tmpdir() to remove the
#    directory.
#  Use TMPDIR for small files. For larger ones (1+ GiB) you should load
#    module “runtimedir”, that will create a directory under XDG_RUNTIME_DIR
#    and place the big files there.
#  You may set TMPDIR before sourcing Bahelite, this way the set directory
#    will be used instead.
#
create_tmpdir() {
	declare -gx TMPDIR

	local  v_level=${VERBOSITY_CHANNELS[Tmpdir]}

	(( v_level >= 4 )) && {
		info-4 "Setting TMPDIR…"
		milinc
		info-4 'Choosing base directory for temporary files'
		milinc
	}

	[ -v TMPDIR ] && {
		(( v_level >= 4 )) && {
			info-4 'TMPDIR is set in the environment, trying to use it.'
			milinc
		}
		if [ -d "${TMPDIR:-}" ]; then
			(( v_level >= 4 ))  \
				&& msg-4 "directory is OK, using “$TMPDIR” as a base."
		else
			warn "tmpdir: no such directory: “$TMPDIR”."
			unset TMPDIR
		fi
		(( v_level >= 4 )) && mildec
	}
	(( v_level >= 4 )) && {
		mildec-4
		info-4 "Creating a temporary directory within base directory.
		        Base directory = ${TMPDIR:-/tmp}"
	}
	TMPDIR=$(mktemp --tmpdir=${TMPDIR:-/tmp/}  -d $MYNAME_NOEXT.XXXXXXXXXX  )
	(( v_level >= 4 ))  \
		&& info-4 "Setting TMPDIR to $TMPDIR"

	declare -r TMPDIR
	(( v_level >= 4 )) && mildec
	return 0
}
#  No export: init stage function.


 # For bahelite_on_exit() which is called in error_handling.sh.
#
delete_tmpdir() {
	local  v_level=${VERBOSITY_CHANNELS[Tmpdir]}

	#  mountpoint is a apart of coreutils, so it doesn’t have to be
	#  specified in BAHELITE_INTERNALLY_REQUIRED_UTILS
	(( v_level >= 4 ))  && {
		info-4 'Deleting TMPDIR…'
		milinc
	}

	if	[ -d "$TMPDIR" ]  \
		&& [ ! -v BAHELITE_DONT_CLEAR_TMPDIR ]  \
		&& ! mountpoint --quiet "$TMPDIR"
	then
		#  Remove TMPDIR only after logging is done.
		rm -rf "$TMPDIR"
		(( v_level >= 4 ))  \
			&& info-4 'Deleted.'
	else
		(( v_level >= 4 ))  \
			&& info-4 'Kept in place.'
	fi

	(( v_level >= 4 )) && mildec
	return 0
}
#  No export: to be executed only once and only in the top level context.



(( VERBOSITY_CHANNELS[tmpdir] >= 1 ))  \
	&& BAHELITE_DONT_CLEAR_TMPDIR=t

#  Temp directory is an immediate need.
create_tmpdir


return 0