#  Should be sourced.

#  messages.sh
#  Provides messages for console. If the “messages_to_desktop” module
#    is loaded too, then messages may be duplicated to both console
#    and desktop, as necessary.
#  Author: deterenkelt, 2018–2024
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MESSAGES_VER ] && return 0
bahelite_load_module 'error_codes' || exit $?
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MESSAGES_VER='2.12.0'

#  No self-report verbosity channel: that would be more harmful (to clarity)
#  than actually useful.


__show_usage_messages_module() {
	cat <<-EOF
	Bahelite module “messages” options:

	autofold=${__bri}0…1${__s}
	    Whether to automatically fold messages longer than the current
	    terminal width. The order of preference for the tool is the following:
	      1) busybox fold (best available);
	      2) anything found in PATH that is not GNU fold (e.g. BSD fold);
	      3) GNU fold. The last, because it sees any multibyte (i.e. non-ASCII)
	         character as two, and this results in GNU fold using only a half
	         of terminal width.
	EOF

	# kw-dir=<path>
	#     The directory which holds localisation files. Path may be an absolute
	#     path or relative to MYDIR (the directory in which the main script
	#     resides).

	#     See the wiki for how to implement localisation.
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_messages_module
		exit 0

	elif [ "$opt" = autofold ]; then
		declare -gx MSG_AUTOFOLD_MESSAGES=t
		#
		#  The order of preference:
		#  1. busybox fold;
		#  2. anything except coreutils fold (e.g. BSD fold);
		#  3. coreutils fold – the last, because it shortens the line twice,
		#     for strings that consist of multibyte characters (i.e. if your
		#     language is not something ASCII, “fold” from coreutils will
		#     think, that your terminal window is twice as short).
		#
		if busybox fold --help &>/dev/null; then
			declare -gx __fold="busybox fold"

		elif [ "$(type -t fold)" = file ]; then
			declare -gx __fold="fold"
			[[ "$(fold --version | head -n1)" =~ ^fold\ \(GNU\ coreutils\) ]]  && {
				warn 'GNU fold doesn’t support multibyte characters, hence all strings
				      made of non-ASCII characters will be folded twice as short.
				      To avoid this issue, install busybox with “fold” applet. Alterna-
				      tively, install BSD fold and make sure that its path in the PATH
				      variable precedes the path, where coreutils fold resides.'
			}
			#  BSD fold, for example would have here
			#  “usage: fold [-bs] [-w width] [file ...]”
		fi

	else
		redmsg "Bahelite: messages: unrecognised option “$opt”."
		__show_usage_messages_module
		exit 4
	fi
done
unset opt



                         #  Message lists  #

 # keyworded message lists
#
declare -gAx BAHELITE_INFO_MESSAGES=()
declare -gAx BAHELITE_WARNING_MESSAGES=()

 # Error messages
#  Keys are used as parameters to err() and values are printed via msg().
#  The keys can contain spaces – e.g. ‘my key’. Passing them to err() doesn’t
#    require quoting and the number of spaces is not important.
#  You can localise messages by redefining this array in some file
#    and sourcing it.
#
declare -gAx BAHELITE_ERROR_MESSAGES=(
	[no such msg]='No such message keyword: “$1”.'
)

 # User message lists
#  By default, functions like info(), warn(), err() will accept a text string
#  and display it. However, it’s possible to replace strings with keywords
#  and hold them separately. This comes handy, when
#  - the messages are too big and ruin the length of lines in the code;
#  - especially when you’d like to use the text of the message as a template,
#    and pass parameters to err(), so that it would substitute them – making
#    a big string with big variable names inside may be really ugly.
#  - when you want to localise your script and keep language-agnostic keywords
#    in the code while pulling the actual messages from a file with localisa-
#    tion.
#  In order to enable keyword-based messages, define MSG_USE_KEYWORDS with
#  any value in the main script. This will switch off the messaging system
#  to arrays.
#
# declare -x MSG_USE_KEYWORDS=t
#
declare -gAx INFO_MESSAGES=()
declare -gAx WARNING_MESSAGES=()
declare -gAx ERROR_MESSAGES=()


 # Colours for the console and log messages
#  Regular functions (info, warn, err) apply it only to asterisk.
#
#  Somebody may have an idea to use these variables to colour their own
#  output, but if NO_COLOURS would be set, such usage may end with a bash
#  error, so there should be at least an empty value.
#
declare -gx INFO_MESSAGE_COLOUR=${__green:-}
declare -gx WARN_MESSAGE_COLOUR=${__yellow:-}
declare -gx ERR_MESSAGE_COLOUR=${__red:-}
declare -gx PLAIN_MESSAGE_COLOUR=${__stop:-}
declare -gx HEADER_MESSAGE_COLOUR=${__yellow:-}${__bright:-}
declare -gx DEBUG_MESSAGE_COLOUR=${__magenta:-}


 # Define this variable to start each message not with just an asterisk
#    ex:  * Stage 01 completed.
#  but with a keyword that would define the type of the message. Especially
#  handy if you load “bahelite” module with “no-colours” option.
#    ex:  * INFO: Stage 01 completed.
#
declare -gx MSG_ASTERISK_WITH_MSGTYPE

[ -v NO_COLOURS ] && MSG_ASTERISK_WITH_MSGTYPE=t


 # Define this variable to make verbose messages (of 4+ level) have also
#  the verbosity channel name before the text of the message.
#
#declare -gx MSG_VERBOSE_MSG_WITH_CHNAME=t



               #  Messages to console, log and desktop  #

 # Message properties array
#
#  message_array
#      When message functions are passed keywords instead of actual message
#      text, this property holds the name of the associative array, where
#      the pairs of keyword – message text are stored.
#
#  colour
#      Defines the “colour code” for the message. This property holds the
#      name of the global variable that defines the colour for a family of
#      functions, e.g. INFO_MESSAGE_COLOUR. The colour will be used only for
#      the asterisk, that begins the message. If MSG_DISABLE_COLOURS is set,
#      then it has no effect.
#
#  whole_message_in_colour
#      Whether to colour the whole message in colour instead of just the
#      asterisk. Values are “yes” or “no”.
#
#  asterisk
#      The string to prepend the actual message with, typically an asterisk
#      with a space. If MSG_DISABLE_COLOURS is set, then the asterisk colour
#      cannot convey the message type and thus it is added explicitly, e.g.
#      “ERROR:” or “WARNING:”.
#
#  desktop_message
#      Whether the message should be duplicated as a desktop message (i.e.
#      sent with notify-send). Values are “yes” or “no”.
#
#  desktop_message_type
#      The type of message to pass for “notify-send”. If “desktop_message”
#      is set to “yes”, then this property should be set to either “info”,
#      or “dialog-warning” or “dialog-error”. If “desktop_message” is set
#      to “no”, then this property may hold an empty string.
#
#  stay_on_line
#      If set to “yes”, then newline isn’t added at the end of the message,
#      allowing the next command (typically an “echo”), to continue the line.
#      It is basically the “-n” switch to echo. For most messages should be
#      set to “no”. Messages duplicated to desktop ignore this option – 
#      for them the ending newline is always cut off.
#
#  output
#      Destination output for console messages: “stdout” or “stderr”.
#      Must be always set.
#
#  keyworded
#      Whether the message is keyworded, i.e. the message function is passed
#      a keyword rather than an actual message text. Values are “yes” or “no”.
#      Allows the function to use keyworded functionality without setting the
#      global variable MSG_USE_KEYWORDS. Internal functions such as err-ikw()
#      use this.
#
#  exit_code
#      Whether the message function should initiate an exit from the program.
#      Only err(), abort(), err-stack(), errw() and err-ikw() use exit codes.
#      For them this property holds a number that they should pass to the
#      “return” builtin – and eventually trigger a call to “exit” with the
#      same code. All the other message functions must have an empty string
#      here.
#
#      On exit codes
#
#      1 – is not used. It is a generic code, with which the main script may
#          exit, if the programmer forgets to place his exit’s and return’s
#          properly. Let these mistakes be exposed. Moreover, using an unset
#          variable also uses this code.
#      2 – is not used. Bash exits with this code, when it catches an inter-
#          preter or a syntax error. Such errors may happen in the main script.
#      3 – Bahelite exits with this code, if the system runs an incompatible
#          version of the Bash interpreter, or the interpreter is not Bash.
#      4 – Bahelite uses this code for all internal errors, i.e. related to
#          the inner mechanics of this library. Most commonly an exit code 4
#          means a failed self-check at the initialisation stage: a lack in
#          minimal dependencies, failing to load a module, or an unsolicited
#          attempt to source the main script (instead of executing it).
#      5 – any error happening in the main script after Bahelite is loaded.
#          This is the exit code, when you call err() to quit the program and
#          send a message.
#      6 – an exit code issued by the abort() function. This function is to be
#          used instead of err() in cases, when the end user of the main script
#          intentionally interrupts the execution at some stage (e.g. presses
#          “Cancel”). The abort() messages have appearance of informational
#          ones, but since the program didn’t complete whatever it was made
#          for, the exit code “0” doesn’t suit here. The exit code “5” doesn’t
#          suit either, because the exit happens not because of a user mistake
#          or due to an uncontrolled situation that has lead to a stop.
#      7…125 – free for the main script code. Can be used with ERROR_CODES.
#      126…165 – not used by Bahelite and must not be used in the main script:
#          this range belongs to the interpreter.
#      166…254 – free for the main script. Can be used with ERROR_CODES.
#      255 – is not used. This is an ambiguous code, that Bash triggers for
#          more than one reason.
#
#      You are strongly advised to rely on err() and abort() to exit from the
#      program:
#        - they make the exit builtin work from subshells and from within func-
#          tions, that are used as a condition in logical expressions (normal-
#          ly bash ignores non-zero exit status there and errexit doesn’t work);
#        - like all message functions, they print their messages directly
#          to console/log, preventing them to be caught by subshell, i.e. $();
#        - err-stack() prints a nice call stack;
#        - err() and abort() by default duplicate their messages to desktop
#          (if the module is loaded);
#        - err() has a companion redmsg(), which you may use to print long-
#          winded and descriptive message to console before quitting with a
#          shorter, concise message.
#
#      There’s no need to employ ERROR_CODES and MSG_USE_KEYWORDS, unless you
#      absolutely have to have distinctive error codes.
#
#      If you have to use custom exit codes (be it for ERROR_CODES or for
#      a custom err()-like wrapper), use only ranges 7…125 and 166…254.
#
#  verbosity_minlevel
#      Determines, at which minimum verbosity level this function is allowed
#      to display the message passed to it. Must be a number in range 0…99999.
#
#      Channels that control messages are silent at level 0, allow only error-
#      level messages at level 1, errors and warnings at level 2 and on level 3
#      they display also informational messages. Level 3 thus is the default
#      level for the most channels, and the verbose/debugging-level messages
#      are hidden unless the level is switched to 4 or above.
#
#      Channels that control *outputs* – and there’s only a few of them:
#      “console”, “log”, “desktop” and “xtrace” – restrict messages on a
#      higher level than this property “verbosity_minlevel”, so don’t look
#      at the levels of those channels: their levels is their own business,
#      specific to their particular output. And despite that the “desktop”
#      channel looks like a message channel, it is only made to look so
#      (the difference is vague now, though).
#


#  For arguments, see __msg() below.


 # Shows an informational message
#
info() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* "
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  info


 # Same as info(), but omits the ending newline, like “echo -n” does.
#
infon() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* "
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='yes'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  infon


 # Like “info()”, except the whole message string gets coloured.
#
infow() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* "
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  infow


 # Shows an info message and then waits for the specified command to finish,
#  depending on the exit code, continues the message with either [ OK ]
#  or [ Fail ]. In case the exit code was non-zero, prints the output of the
#  command.
#
#  $1 – a message. Something like “Starting up servicename… ”
#  $2 – a shell command.
#  $3 – any string to force the output even if the exit code was 0. This is
#       handy for poorly written programs that return 0 even on error.
#
info-wait() {
	declare -g INFOWAIT_CMD_RESULT

	local  message=$1
	local  command=$2
	local  force_output="$3"
	local  outp
	local  result

	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+RUNNING: }"
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='yes'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$message"

	errexit_off
	outp=$( bash -c "$command" 2>&1 )
	result=$?
	errexit_rewind

	(( result == 0 ))  \
		&& echo -e "${__bri:-} [ ${__g:-}OK${__s:-}${__bri:-} ] ${__s:-}"  \
		|| echo -e "${__bri:-} [ ${__r:-}Fail${__s:-}${__bri:-} ]${__s:-}"

	if [ "$outp" ]; then
		[ $result -ne 0  -o  "$force_output" ] && {
			msg "Here is the output of “$command”:"
			milinc
			divider-msg 'command output'
			plainmsg "$outp"
			divider-msg
			mildec
		}
	else
		plainmsg "(No output from “$command”.)"
	fi

	INFOWAIT_CMD_RESULT=$result
	return 0
}
export -f  info-wait


 # Prints a warning message to console.
#
warn() {
	declare -A __msg_properties=(
		[message_array]='WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warn


 # Like “warn()”, but the entire string is coloured. YOU SHOULD THINK TWICE
#  AND THRICE BEFORE ACTUALLY USING THIS MUCH COLOUR.
#
warnw() {
	declare -A __msg_properties=(
		[message_array]='WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warnw


 # Shows an error message and calls the “exit” builtin
#
#  err() was specifically created to differentiate between handled and non-
#  handled, spontaneous errors.
#
#          Handled errors                          Non-handled errors
#  These are  “known bad  ends”: the       This sort of errors happens, when an
#  errors, whose reason  is clear to       unexpected stop occurs because  of a
#  the developer and what went wrong       wrong command, wrong argument, a syn-
#  can be described to the user with       tax error or another such thing,
#  a message.                              which cannot be predicted beforehand.
#    - a descriptive message                 - no message is shown (as the
#      is required;                            reason is not predictable);
#    - call stack is not shown.              - call stack is displayed.
#
#  tl;dr: “exit 0” is fine, but “exit 10” (11… 12…) will make your program
#  quit like if it crashed – with a call stack. So if you want a “clean” (but
#  still “negative”) exit, and you have a clear message to the user, then use
#  err(). For an intended exit due to user choosing to cancel running the prog-
#  ram – i.e. when it is not quite an OK exit, but not an error either – use
#  abort().
#
#  As all messages delivered by err() are mandatory duplicated as desktop mes-
#  sages, the text should be as short as possible. However, at the same time
#  some error messages should vice versa be elaborative – when they’re shown
#  on the console. To kill two hares with one stone, ONLY THE FIRST LINE of
#  the error message is duplicated to dekstop (while the console receives the
#  full text). What’s said above is true for all err()-class functions: errw(),
#  err-ikw(), but not abort(), as it’s a message of its own class.
#
#  In a case when the error message would better be split, and the warn()
#  doesn’t seem like it has an appropriate level of importance, there is also
#  redmsg() – a companion function to err(), which looks like the latter, but
#  is basically a “red warn()”. redmsg() never spawns desktop notifications,
#  and neither it forces the program to quit. Its sole purpose is to give some
#  messages before the call to err().
#
#  The exit code is 5, unless you employ MSG_USE_KEYWORDS and the ERROR_CODES
#  array.
#
err() {
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]='5'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#^ Exits.
}
export -f  err


 # Displays a message on console with an appearance of err(), but doesn’t do
#  anything beyond that.
#
redmsg() {
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='no'
		[desktop_message_type]='warn'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	return 0
}
export -f  redmsg


 # Like “redmsg()”, but the whole string is coloured. YOU SHOULD THINK TWICE
#  AND THRICE BEFORE ACTUALLY USING THIS MUCH COLOUR.
#
redmsgw() {
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='no'
		[desktop_message_type]='warn'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	return 0
}
export -f  redmsg


 # An info message marking a dead end
#  It suites to a situation, when the main script attempts to go some auxili-
#  ary route, but that attempt wasn’t successful. The execution then returns
#  to a point before and continues from there. Message indentation should
#  probably return several levels back at the same time too. This is similar
#  to a situation when in real life you’re driving, then encounter a roadblock
#  and go around.
#
denied() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="× ${MSG_ASTERISK_WITH_MSGTYPE:+DENIED: }"
		[desktop_message]='no'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  denied


 # Shows a hint for the previous message
#
#  Shell scripts often run external programs, and those sometimes have weird,
#  ambiguous or confusing messages. In case there is no way to suppress such
#  a message, it’s reasonable to provide a hint how to understand that previ-
#  ous line, for the user to be able to tell whether it’s worrisome or may be
#  ignored. Basically this:
#
#      . . . . . . . .                      #  Some console messages.
#      . . . . . . . .                      #
#      Pl0x donate to my Surveillatrion     #  A weird message from some program.
#      ^ please ignore this message //_-    #  The hint message that you pass to
#                                           #  this function.
#
hint-msg() { sub-msg "$@"; }
sub-msg() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='PLAIN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="^ "
		[desktop_message]='no'
		[desktop_message_type]='plain'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  sub-msg  \
           hint-msg


 # Same as err(), but prints the whole line in red.
#  More of an example for a whole-coloured message, than something that
#  should really be used. Has no advantages over err(). Except for being
#  annoyingly red.
#
errw() {
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]='5'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#^ Exits.
}
export -f errw


 # Like err(), but has the appearance of an info message. For the case when
#  the end user chooses to halt the execution and finish the program halfway. 
#
#  The message should be concise, as it’s duplicated to desktop. Usually
#  something like “Cancelled.” is enough.
#
#  abort() is to be used when the main script *offers the user an option* –
#  a menu item or a button – to end the program preventively. Ctrl-C or kill-
#  ing the process are forceful interruptions not related here.
#
abort() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ABORT: }"
		[desktop_message]='yes'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]='6'
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	#^ Exits.
}
export -f  abort


 # Bahelite keyworded info() for internal use
#
#info-ikw () {
#
#}


 # Bahelite keyworded warn() for internal use
#
warn-ikw() {
	declare -A __msg_properties=(
		[message_array]='BAHELITE_WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='no'
		[desktop_message_type]='warn'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='yes'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warn-ikw


 # Bahelite keyworded err() for internal use
#
err-ikw() {
	declare -A __msg_properties=(
		[message_array]='BAHELITE_ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${U:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='yes'
		[exit_code]='4'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#^ Exits.
}
export -f  err-ikw


 # Err-stack is an internal feature of Bahelite. It’s an alias to err() for
#  those tangled places in messaging and verbosity modules, where simply
#  showing a descriptive error is not enough yet, and the chain of execution
#  must be before one’s eyes to get an idea where exactly an error happened.
#  It may be used in the main script, too. For similar occasions, indeed.
#  Good code should mostly rely on err().
#
err-stack() {
	#
	#  For bahelite_on_exit(). In that type of errors, which are described
	#  in the test no. 24, checking FUNCNAME has no meaning, as the trap on
	#  DEBUG catches the $? before bash executes the next command *after*
	#  err-stack(). Meaning that err-stack is not in the FUNCNAME stack any
	#  more and the neat check like
	#      [[ "${FUNCNAME[*]}" =~ \ err-stack\  ]] && …
	#  wouldn’t work. (It would work for other, simpler cases.)
	#
	declare -g BAHELITE_ERR_STACK_CALL=t
	err "$@"
	#^ Exits.
}


 # The “plain”, asterisk-less version of info(). The most simple
#  message function.
#
#  > There’s already info, why add a “plain” version of it?
#  Though all message functions handle multiline strings perfectly, there are
#  times when you’d want to send an “accompanying” message under the same in-
#  dentation level. (In other words, to split what you want to report, across
#  multiple messages.) And such a group of messages would better have one aste-
#  risk in the first message and the rest be plain.
#
#  > Er… Can’t I just use “echo”?
#  msg() offers a couple advantages over the “echo” builtin:
#    - it follows message indentation level (which you increase or decrease
#      with “milinc” and “mildec”, reset with “mildrop”);
#    - it pads its message text with spaces, so that it would be aligned to
#      the message text of other, non-plain, messages, which possess an aste-
#      risk.
#  Consider an example:
#      info "Parameters to be set:"
#      milinc
#      for key in ${!parameters[*]}; do
#          msg "$key: ${parameters[$key]}"
#      done
#      mildec
#
msg() { plainmsg "$@"; }
plainmsg() {
	declare -A __msg_properties=(
		[message_array]='BAHELITE_INFO_MESSAGES'
		[colour]='PLAIN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]='  '
		[desktop_message]='no'
		[desktop_message_type]='plain'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  msg  plainmsg


 # Same as msg(), but omits the ending newline, like “echo -n” does.
#
msgn() { plainmsgn "$@"; }
plainmsgn() {
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='PLAIN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]='  '
		[desktop_message]='no'
		[desktop_message_type]='plain'
		[stay_on_line]='yes'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  msgn plainmsgn


 # Debug-level messaging functions: info-N() and msg-N(). Where N is a number
#  from 4 to 8. Thus 5 additional verbosity levels.
#
#  > Why do they start from level 4?
#  Because the default level of info() is 3. Anything above is simply a “ver-
#  bose info”.
#
#  > Why no warn-N(), err-N()?
#  Because debugging messages are all basically info(). All errors and warn-
#  ings are supposed to be *important* messages, and this means *a priority
#  higher than info() and all of the debugging messages*. If a regular info()
#  is shown on the verbosity levels 3 and above, warnings belong to level 2,
#  and errors are shown from level 1. Debugging messages lie on the other side
#  of info()’s, and start with level 4. In other words, it has no sense to hide
#  errors and warnings, they should vice versa be shown right away.
#
#  > Why not call them dbg-N() or debug-N() then?
#  Do you really want to remember another function name? They are basically
#  verbose info()’s, anyway…
#
#  > Okay, okay, now why there are info-N()’s *AND* msg-N(), again?
#  msg-N()’s are the “plain”, asterisk-less versions of info-N()’s. Just like
#  with regular info() and msg(). (See the comment to msg() above).
#
#  > Can I have MORE verbosity levels?
#  Sure. Copy the cycle below to your code and replace the range {4..8} with
#  the numbers of your preference, e.g. {9..12} would add four more levels to
#  the already available ones. The maximum level is 99. Be sure that the
#  copied code is executed in the global context.
#
source <(
	#  Could be just {4..8}, but this somehow breaks syntax highlight.
	#  It’s a one-time execution, and eval does a simple expansion of numbers,
	#  so nothing to worry about here.
	for i in `eval echo "{4..8}"`; do
		cat <<-EOF
		info-$i() {
			__verbose_msg "\$@"
		}
		msg-$i() {
			__verbose_msg "\$@"
		}
		export -f  info-$i  msg-$i
		EOF
	done
)
__verbose_msg() {
	local msg_level
	local asterisk
	local cur_ch_name=$(__nomsg_get_current_verbosity_channel_name)
	local cur_verb_level=${VERBOSITY_CHANNELS[$cur_ch_name]}
	#  ^ Not using vchan for the same reason as described in __msg().

	msg_level=${FUNCNAME[1]}
	msg_level=${msg_level#info-}
	msg_level=${msg_level#msg-}
	[[ "$msg_level" =~ ^([4-9]|[1-9][0-9])$ ]]  \
		|| err "Aliases to ${FUNCNAME[0]}() must use verbosity level 4 or above."

	 # Saving 15 ms on each info-N call by avoiding call to __msg()
	#  when it will be unnecessary. Thanks to this, Bahelite loads
	#  in 560 ms instead of 1035, or 45% faster.
	#
	(( cur_verb_level >= msg_level ))  || return 0

	[[ "${FUNCNAME[1]}" =~ ^info- ]]  \
		&& asterisk="* "  \
		|| asterisk="  "

	[ -v VERBOSITY_MSG_WITH_CHNAME ] && {
		[ "$cur_ch_name" != main ]  \
			&& asterisk+="$cur_ch_name, "
	}
	asterisk+="V$msg_level: "

	declare -A __msg_properties=(
		[message_array]='BAHELITE_DEBUG_MESSAGES'
		[colour]='DEBUG_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="$asterisk"
		[desktop_message]='no'
		[desktop_message_type]='verbose'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]="passes"
		#  ^ This allows __msg() to skip its verbosity check.
	)
	__msg "$@"
	return 0
}
export -f __verbose_msg


 # The all-in-one handler for displaying informational messages, warnings
#  and errors. The parent function must define __msg_properties array, which
#  will basically define, what this function will do.
#
#  Applies verbosity and output (log/console/desktop) restrictions, colour
#  codes and message indentation to messages. Certain settings allow to call
#  “exit” and stop the program. Aids in error catching through the global
#  variable BAHELITE_STIPULATED_ERROR (see below).
#
#  $1 — a text message or, if MSG_USE_KEYWORDS is set, a keyword.
#
#  Retrun status is 0, except the parent function was err(), err-stack(),
#  abort() or the like (which performs an intentional exit). In case of an
#  error, that triggers a recursion (i.e. messages shows while already pro-
#  cessing an error, and then another error is encountered), the exit status
#  is 4, and the recursion is broken from.
#
__msg(){
	#
	#  This is a marker that a stipulated exit procedure was initiated. A sti-
	#  pulated exit is one that is explicitly requested with e.g. err(), err-
	#  stack() or abort(). Exiting with the help of these functions is good
	#  not only because of the fancy messages and that it’s “echo and exit in
	#  one bottle” – it also helps to catch errors better.
	#
	#      ┃  > Why only with these functions, and not with “exit” itself?
	#      ┃  The “exit” builtin would have to be overridden, and placing over-
	#      ┃  rides for builtins is troublesome. So, it’s less bugs this way.
	#      ┃  Aren’t you glad, that the “exit” remained the “exit” you know?
	#
	#  Leaving this marker is important for two reasons:
	#     1. It tells the traps on SIGEXIT and SIGERR, that the non-zero re-
	#  turn code comes from an intentional, handled exit procedure (and thus
	#  differentiate between exit from spontaneous, true errors in the program
	#  logic, from those exits with non-zero status, that the programmer knows
	#  about, displays a message sensible for the end user, and quits).
	#     2. It helps to write programs, that guaranteedly quit when it’s
	#  needed. The thing is that a call to “exit” may be “gulped” by shell
	#  under certain circumstances, basically, the call to “exit” is ignored
	#  and the execution pipeline continues. Test no. 24 in “./tests/error_
	#  handling/error_catching.sh” demonstrates exactly this type. In order
	#  to catch a request to quit from even such tricky places (about which
	#  many people, including the author of this library, often forget), bahe_
	#  lite_on_each_command(), the trap on DEBUG, has a check on the return
	#  value of the last command. If the return value is greater than zero AND
	#  BAHELITE_STIPULATED_ERROR is set, this means, that the execution went
	#  *past* the place where the “errexit” shell option would normally catch
	#  the non-zero return code, and the program should quit. So, the trap on
	#  DEBUG essentially performs a check on an uncaught error and forces the
	#  program to quit (calls “exit” itself). Because the trap on DEBUG appa-
	#  rently executes in a context outside of the logic expression, its call
	#  to “exit” *does* work.
	#
	#  In addition to reason no. 1: remember, that this variable works only
	#  within the context it’s created in. It doesn’t cross the context boun-
	#  dary, and it doesn’t need to – because it must live until the trap on
	#  SIGEXIT processes this variable. The closest time SIGEXIT is triggered:
	#  if it’s in a subshell, then it’s in a subshell, if it’s in the main con-
	#  text, then it’s in the main context. In case when a subshell quits or
	#  calls “exit” and one SIGEXIT is triggered, and then errexit catches it
	#  to send its own SIGEXIT and triggers the trap for the second time –
	#  Bahelite handles all that and lets the trap called from the inner and
	#  the outer context do its job in accordance with what has to be done at
	#  each level of depth. Unnecessary duplicate runs are prevented. Things
	#  that cross the context boundary are conveyed with the marker files.
	#  This variable, BAHELITE_STIPULATED_ERROR, is created with an intention
	#  to live within the context it was set in. There’s no need to change any-
	#  thing, everything was tested and should work well.
	#
	declare -gx  BAHELITE_STIPULATED_ERROR

	#  Bash specificity: must read!
	#
	#  The status of all local variables within this function should be defi-
	#  ned: either variables receives a value – receives right here, within
	#  the body, before it is used for the first time, – or it must be expli-
	#  citly unset!
	#
	#  This is because __msg() can be called within a call to __msg(), and
	#  sometimes this lead to troubles. Consider the following example: a call
	#  to err() triggers the “exit” command, then, in the bahelite_on_exit()
	#  a call to info-4() must show a debug-level message. But as we techni-
	#  cally remain within err(), we’re in __msg(), *that retains local vari-
	#  ables from its parent __msg() function, that is, err()*. Which means
	#  that info-4() inherits among other variables, the “exit_code”, which
	#  should never be set for an info message! And this causes an abrupt
	#  exit *now within bahelite_on_exit() or bahelite_on_error().* For this
	#  reason all local variables must explicitly be either assigned a value
	#  or unset.
	#
	#  P.S. Technically, this could be avoided, if we’d simply put the call
	#  to __msg() within higher-level functions in a logical statement, and
	#  return from there, like in:
	#      err() {
	#          declare -A __msg_properties=(
	#              …
	#          )
	#          __msg || return $?
	#      }
	#  However, for those functions, that do not have a right to exit the
	#  program, such as info()’s, warn()’s and plainmsg()’s, “|| return $?”
	#  has no sense – __msg() called from those functions should always
	#  return 0. On the other hand, there’s a theoretical possibility, that
	#  if something would break within __msg() itself, it’s better to have
	#  “|| return $?” there. And back again: putting the call to __msg() in-
	#  side a scope, where exit cannot be done from, is bad, because __msg()
	#  must have an ability to detect dangerous recursion (that is, going
	#  for one too many loops, than a simple debugging info-4() called from
	#  within err() creates) and break from that recursion. So the only way
	#  remains is to unset.
	#
	#  (The logical statement solution is based on the shell rule, according
	#  to which errexit ignores parts of the “…&&… ” and “…||…” expressions
	#  except for the last one. The call to __msg() thus would be closed,
	#  meaning that __msg would work once and return once, all local variables
	#  would not be inherited ever. The upper functions would typically have
	#  only the __msg_properties, which is always set anew and creates no
	#  problems even if it would be inherited in an occasional recursion.)
	#
	#  Modern Bash has an option “localvar_unset”, but the way it is to be
	#  used, is unclear. The description says it’s much like “calling ‘unset’
	#  in the current scope”, so why even…
	#
	local  message_array;                    unset  message_array
	local  colour;                           unset  colour
	local  whole_message_in_colour;          unset  whole_message_in_colour
	local  asterisk;                         unset  asterisk
	local  desktop_message;                  unset  desktop_message
	local  desktop_message_type;             unset  desktop_message_type
	local  stay_on_line;                     unset  stay_on_line
	local  output;                           unset  output
	local  keyworded;                        unset  keyworded
	local  cur_verb_chname;                  unset  cur_verb_chname
	local  cur_verb_level;                   unset  cur_verb_level
	local  verbosity_minlevel;               unset  verbosity_minlevel
	local  exit_code;                        unset  exit_code
	local  f;                                unset  f
	local  f_count=0
	local  message=''
	local  message_key;                      unset  message_key
	local  message_key_exists;               unset  message_key_exists
	local  _message=''
	local  i;                                unset  i
	local  already_printing_call_stack  #  The only variable, which is expec-
	                                    #  ted to be set in a recursion.
	local -x ast_padding=''


	 # As a precaution against internal bugs, we check how many times __msg()
	#    appears in the call stack. If the number will be more than 3, this
	#    would hint at a recursive error.
	#  It has to be 3, because an error may be triggered within this very
	#    function, and if an error then happens while displaying the error
	#    message, there will be two __msg()  in the call stack. However
	#    this isn’t an erroneous recursion yet, but happen this function
	#    appear one more time (making it three), it will definitely be that.
	#
	for f in "${FUNCNAME[@]}"; do
		[ "$f" = "${FUNCNAME[0]}" ] && let '++f_count,  1'
	done
	(( f_count >= 3 )) && {
		echo "Bahelite error: call to $FUNCNAME has went into recursion."  \
			>&${STDERR_ORIG_FD_PATH:-/proc/$$/fd/2}
		echo "Message to be delivered: “${1:-}”"  \
			>&${STDERR_ORIG_FD_PATH:-/proc/$$/fd/2}
		#  ^ Sending to regular stderr, if STDERR_ORIG_FD_PATH wasn’t
		#  defined yet.
		[ "$(type -t print_call_stack)" = 'function' ]  && {
			#  Print call stack, unless already in the middle of doing it
			for f in "${FUNCNAME[@]}"; do
				[ "$f" = print_call_stack ]  \
					&& already_printing_call_stack=t
			done
			[ ! -v already_printing_call_stack ]  \
				&& print_call_stack
		}
		#  Unsetting the traps, or the recursion may happen again.
		trap '' EXIT TERM INT HUP PIPE ERR DEBUG RETURN
		#  Now that the script will exit is guaranteed.
		exit 4
	}

	verbosity_minlevel="${__msg_properties[verbosity_minlevel]}"
	[[ "${__msg_properties[exit_code]}" =~ ^[0-9]{1,3}$ ]]  \
		&& exit_code=${__msg_properties[exit_code]}

	 # Hook for the current verbosity channel
	#  It allows
	#    - to suppress messages by default without disabling stdout or stderr.
	#    - to have some parts of code more silent – or vice versa, more
	#      verbose – than the main body of code.
	#
	#  Note: using the __nomsg alias to avoid recursion, when the Verbosity
	#  module would have raised verbosity level and will use info/warn/err…
	#  to report what is going on.
	#
	if	   [ -v BAHELITE_MODULE_VERBOSITY_VER  ]  \
		&& [ "$verbosity_minlevel" != 'passes' ]
	then
		cur_verb_chname="$(__nomsg_get_current_verbosity_channel_name)"
		cur_verb_level=${VERBOSITY_CHANNELS[$cur_verb_chname]}
		if	   (( cur_verb_level < verbosity_minlevel ))  \
			&& [ ! -v exit_code ]  #  Makes sure, that err() can always stop
			                       #  the program.
		then
			#  This is a message-level verbosity, it takes priority over the
			#  verbosity level of an output that it was supposed to go to.
			return 0
		fi
	fi

	declare -n message_array=${__msg_properties[message_array]}

	[ -v NO_COLOURS ]  \
		|| declare -n colour=${__msg_properties[colour]}

	[ "${__msg_properties[whole_message_in_colour]}" = 'yes' ] \
		&& whole_message_in_colour=${__msg_properties[whole_message_in_colour]}  \
		|| unset whole_message_in_colour

	asterisk=${__msg_properties[asterisk]}

	[ "${__msg_properties[desktop_message]}" = 'yes' ]  \
		&& desktop_message=${__msg_properties[desktop_message]}  \
		|| unset desktop_message

	desktop_message_type=${__msg_properties[desktop_message_type]}

	[ "${__msg_properties[stay_on_line]}" = 'yes' ]  \
		&& stay_on_line=${__msg_properties[stay_on_line]}  \
		|| unset stay_on_line

	output=${__msg_properties[output]}

	[ "${__msg_properties[keyworded]}" = 'yes' ]  \
		&& keyworded=${__msg_properties[keyworded]}  \
		|| unset keyworded


	 # Setting the message text.
	#  Checking here if the text is in $1, or it has to be found by a keyword.
	#
	if [ -v MSG_USE_KEYWORDS  -o  -v keyworded ]; then
		#  What was passed to us is not a message per se,
		#  but a key in the messages array.
		message_key="${1:-}"
		for key in "${!message_array[@]}"; do
			[ "$key" = "$message_key" ] && message_key_exists=t
		done
		if [ -v message_key_exists ]; then
			#  Positional parameters "$2..n" now can be substituted
			#  into the message strings. To make these substitutions go
			#  from the number 1, drop the $1, holding the message key.
			shift
			eval message=\"${message_array[$message_key]}\"
		else
			err-ikw 'no such msg' "$message_key"
		fi
	else
		# message="${1:-No message?}"
		message="${1:-}"
	fi


	 # Removing blank space before message lines.
	#  This allows strings to be split across lines and at the same time
	#  be well-indented with tabs and/or spaces – indentation will be cut
	#  from the output.
	#
	message=$(sed -r 's/^\s*//; s/\n\t/\n/g' <<<"$message")


	 # Making “$desktop_message” hold a copy of $message, so that the two
	#  routes (to console and to desktop) might be handled separately.
	#
	#  Text, that is duplicated to desktop is cut to the first line of the
	#  full message. (Console and log will receive the full text as usual.)
	#
	[ -v desktop_message ] && desktop_message="${message/$'\n'*}"


	 # The logging module hook –
	#  to see, if the output should be suppressed or not. The “console” chan-
	#  nel may have an output disabled, but it may be enabled in the “log”
	#  module – in which case the message should be sent to its original dest-
	#  ination (i.e. stdout or stderr), and the redirections made in the “log”
	#  module will suppress the output to console as necessary.
	#
	#  Not using vchan to avoid recursion.
	#
	case "$output" in
		stdout)
			if (( ${VERBOSITY_CHANNELS[console]} < 2 )); then
				[ -v BAHELITE_LOGGING_STARTED ]  \
					&& (( ${VERBOSITY_CHANNELS[log]} >= 2 ))  \
					|| output='devnull'
			fi
			;;

		stderr)
			if (( ${VERBOSITY_CHANNELS[console]} < 1 )); then
				[ -v BAHELITE_LOGGING_STARTED ]  \
					&& (( ${VERBOSITY_CHANNELS[log]} >= 1 ))  \
					|| output='devnull'
			fi
			;;
	esac


	 # Desktop message hook –
	#  to see, if this function should trigger the desktop message or not.
	#
	#  Unlike with “logging” module, where start_logging() has to set
	#  a flag for us to be sure, that the facility is operational, here
	#  we can simply check if the module has defined its presence.
	#
	[ -v desktop_message ] && {
		if	   [ -v BAHELITE_MODULE_MESSAGES_TO_DESKTOP_VER ]      \
			&& [ "$(type -t bahelite_notify_send)" = 'function' ]  \

		then
			case "${VERBOSITY_CHANNELS[desktop]}" in
				0)	unset desktop_message
					;;

				1)	[ "$desktop_message_type" = 'err' ]  \
						|| unset desktop_message
					;;

				2)	[[ "$desktop_message_type" =~ ^(err|warn)$ ]]  \
						|| unset desktop_message
					;;
			esac
		else
			#  If the module is not loaded (or not loaded yet) –
			#  then disable message to desktop.
			unset desktop_message
		fi
	}


	[ -v message ] && {
		#  Reset any colour alternating codes, that may be in effect.
		_message+="${__stop:-}"

		#  Add colour code (info/warn/err/debug) or an asterisk with
		#  message type when colours are disabled.
		_message+="${colour:-}$asterisk"

		#  If the whole message should be coloured, don’t reset the colour.
		[ -v whole_message_in_colour ]  || _message+="${__stop:-}"

		#  Add message text and colour reset code.
		_message+="$message${__stop:-}"

		#  If colours are disabled, strip them (including any colour codes,
		#  that may be withing the message text itself).
		[ -v NO_COLOURS ]  \
			&& _message=$(strip_colours "$_message")

		#  Adding message indentation and folding lines.
		#  See the description to MSG_AUTOFOLD_MESSAGES.
		for ((i=0; i<${#asterisk}; i++)); do
			ast_padding+=' '
		done
		if [ -v MSG_AUTOFOLD_MESSAGES ]; then
			export asterisk
			message=$(echo -e ${stay_on_line:+-n} "$_message" \
			              | $__fold -w $((TERM_COLUMNS - ${#__mi} - ${#asterisk})) -s \
			              | sed -r "1s/^/${__mi}/; 1!s/^/$__mi$ast_padding/g" )
			export -n asterisk
		else
			message=$(echo -e ${stay_on_line:+-n} "$_message" \
			              | sed -r "1s/^/${__mi}/; 1!s/^/$__mi$ast_padding/g" )
		fi

		#  Printing the list of channels may be accompanied by a (naturally
		#  happening) error message, e.g. because the program expected some
		#  arguments, which weren’t provided. While the “--verbosity-list”
		#  option seems like an ordinary option, which completes /instead/ of
		#  an actual task, akin to --help or --version, it works differently.
		#  As the modules may be loaded in runtime (based on some condition in
		#  the main program), it’s only at the end of the program, that the
		#  list of verbosity channels (from all modules) would be complete.
		#    Error messages (especially when shown on desktop) would be unex-
		#  pected by most users and confusing. For this reason. To suppress the
		#  error messages completely would be wrong, because, as the program
		#  continutes to run till the end “as usual”, it would probably break
		#  without those arguments which it needs. So, err() and other func-
		#  tions which are allowed to stop the program, are allowed and their
		#  messages are shown. A hint, however, is added to the console mes-
		#  sage and sending desktop notifications is disabled, if the list of
		#  verbosity channels was requested.
		#
		[ -v exit_code  -a  -v BAHELITE_VERBOSITY_LIST_CHANNELS ] && {
			message+=$'\n'"${__mi}^ The error may be safely ignored. To print the list of all"
			message+=$'\n'"${__mi}  verbosity channels – some of which may be loaded at runtime –"
			message+=$'\n'"${__mi}  the program has to do a complete run."
		}

		case "$output" in
			stdout)
				if	(( BASH_SUBSHELL == 0 ))  ||  [ ! -v STDOUT_ORIG_FD_PATH ]
				then
					echo ${stay_on_line:+-n} "$message"
				else
					#
					#  If this is a subshell, use the parent shell’s file
					#  descriptors to send messages, because they shouldn’t
					#  be grabbed along with the output. The parent shell’s
					#  FD may be closed, so a check is needed to confirm,
					#  that it’s still writeable.
					#
					[ -w "$STDOUT_ORIG_FD_PATH" ]  \
						&& echo ${stay_on_line:+-n} "$message"   \
						        >>$STDOUT_ORIG_FD_PATH

					#           ^^ append, because overwrite will be confusing
					#  for the logging pipe, and, under certain circumstances,
					#  the message would rewrite the log file. The circumstan-
					#  ces are:
					#    1) console output is disabled (verbosity channel
					#       “console” is set to 0);
					#    2) message comes from the trap on error;
					#    3) said error occurs in a logical or conditional
					#       expression (subshell doesn’t seem to affect that).
				fi
				;;

			stderr)
				if	(( BASH_SUBSHELL == 0 ))  ||  [ ! -v STDERR_ORIG_FD_PATH ]
				then
					echo ${stay_on_line:+-n} "$message" >&2

				else
					#
					#  If this is a subshell, use the parent shell’s file
					#  descriptors to send messages, because they shouldn’t
					#  be grabbed along with the output. The parent shell’s
					#  FD may be closed, so a check is needed to confirm,
					#  that it’s still writeable.
					#
					[ -w "$STDERR_ORIG_FD_PATH" ]  \
						&& echo ${stay_on_line:+-n} "$message"   \
						        >>$STDERR_ORIG_FD_PATH

					#           ^^ append, because overwrite will be confusing
					#  for the logging pipe. (See the similar comment above.)
				fi
				;;

			devnull)
				:  #  Not sending anything
				;;
		esac
	}

	[ -v desktop_message ] && {
		#  See the comment to the similar condition a screen above.
		[ -v exit_code  -a  -v BAHELITE_VERBOSITY_LIST_CHANNELS ]  || {
			bahelite_notify_send "$(strip_colours "$desktop_message")"  \
			                     "$desktop_message_type"
		}
	}

	 # Triggering return or exit for error-level messages
	#
	#  In order to get a consistent picture in the call stack, “exit” should
	#  be used only in the subshell contexts, and “return” – everywhere else.
	#  The use of “exit” breaks the consistency in cases when one sourced
	#  script calls a function from another sourced script, and an error is
	#  triggered there. Call stack would become abrupt with just the “source”
	#  in FUNCNAME, rendering it almost useless. (Verify.)
	#
	[ -v exit_code ] && {
		if [ -v keyworded ]; then
			#  Must be set right before return in order for the trap
			#  on DEBUG to work as it should!
			BAHELITE_STIPULATED_ERROR=t
			return $exit_code

		elif [ -v MSG_USE_KEYWORDS ]; then
			bahelite_validate_error_code "${ERROR_CODES[$*]}"
			#  Must be set right before return in order for the trap
			#  on DEBUG to work as it should!
			BAHELITE_STIPULATED_ERROR=t
			return ${ERROR_CODES[$*]}

		else
			#  Must be set right before return in order for the trap
			#  on DEBUG to work as it should!
			BAHELITE_STIPULATED_ERROR=t
			return $exit_code
		fi
	}
	return 0
}
export -f  __msg


 # A divider is a message, that is printed highlighted and takes the entire
#  line. It is intended to improve readability for the cases, when the output
#  of the main script is temporarily suspended, and another program prints
#  to console: for this reason the divider message also increases and decreases
#  the message indentation level accordingly.
#
header-msg() {
	milinc
	#  Bad idea: to add a spacing “echo”.
	#  It would confuse the one reading the inner output (probably a log)
	#    with a question “Does this space belong to the log? Is this what
	#    causes an error?”
	divider-msg "$@"
	return 0
}
export -f header-msg


footer-msg() {
	divider-msg "$@"
	#  No spacing echo for the same reason as above.
	mildec
	return 0
}
export -f footer-msg


 # Print a message, that will span over entire line
# [$1] – text message. It is advised to use the message at least for the
#          opening divider, to put something like “command output:” there.
#          You can as well use the bracing messages like “beginning of …”,
#          “end of …”.
# [$2] – character to use for the divider line. If unspecified, set to “+”.
# [$3] – style to use for the line. If unspecified, uses whatever is set
#        in the $HEADER_MESSAGE_COLOUR variable.
#
divider-msg() {
	local  message="${1:-}"
	local  divider_line_character="${2:-+}"
	local  style="${3:-$HEADER_MESSAGE_COLOUR}"
	local  line_to_print=''
	local  line_to_print_length
	local  i

	(( MSG_INDENTATION_LEVEL > 0 ))  \
		&& line_to_print+="${__mi}"

	[ -v NO_COLOURS ]  \
		|| line_to_print+="$style"

	for	i in {1..3}; do
		line_to_print+="$divider_line_character"
	done

	line_to_print+="${message:+  $message  }"

	if [ -v NO_COLOURS ]; then
		line_to_print_length="$(strip_colours "$line_to_print")"
		line_to_print_length=${#line_to_print_length}
	else
		line_to_print_length=${#line_to_print}
	fi

	for	(( i=0;  i < TERM_COLUMNS - line_to_print_length;  i++ )); do
		line_to_print+="$divider_line_character"
	done

	[ -v NO_COLOURS ] \
		|| line_to_print+="${__s:-}"

	if (( BASH_SUBSHELL == 0 )); then
		[ "${FUNCNAME[1]}" = header-msg ] && echo
		echo -e "$line_to_print"
		[ "${FUNCNAME[1]}" = footer-msg ] && echo

	else
		#  If this is the subshell, use the parent shell’s
		#    file descriptors to send messages, because they
		#    shouldn’t be grabbed along with the output.
		#  The parent shell’s FD may be closed, so a check
		#    is needed to confirm, that it’s still writeable.
		[ -w "$STDOUT_ORIG_FD_PATH" ] && {
			[ "${FUNCNAME[1]}" = header-msg ]  \
				&& echo  >>$STDOUT_ORIG_FD_PATH
				#        ^^ note the append
			echo -e "$line_to_print"  >>$STDOUT_ORIG_FD_PATH
			#                         ^^ note the append
			[ "${FUNCNAME[1]}" = footer-msg ]  \
				&& echo  >>$STDOUT_ORIG_FD_PATH
			#            ^^ note the append
		}
	fi
	return 0
}
export -f divider-msg


return 0