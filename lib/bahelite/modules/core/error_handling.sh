#  Should be sourced.

#  error_handling.sh
#  Places traps on signals ERR, EXIT group and DEBUG. Catches even those
#    errors, that do not trigger sigerr (errexit is imperfect and doesn’t
#    catch nounset and arithmetic errors).
#  On error prints call trace, the failed command and its return code,
#    all highlighted distinctively.
#  Author: deterenkelt, 2018–2024
#

# Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_ERROR_HANDLING_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_ERROR_HANDLING_VER='1.10.10'


__show_usage_error_handling_module() {
	cat <<-EOF
	Bahelite module “error_handling” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_error_handling_module
		exit 0

	else
		redmsg "Bahelite: error_handling: unrecognised option “$opt”."
		__show_usage_error_handling_module
		exit 4
	fi
done
unset opt



 # Self-report verbosity channel for the “error_handling” module
#
#  The default level should be no less than 3 to allow --verbosity-list-chan-
#  nels show that list. Though the 3rd level is basically devoid of messages,
#  some optionally may appear – like in this case. (For Bahelite 6.0 it doesn’t
#  seem to be an issue, but still better to be careful here.)
#
vchan setup  name=Error_handling  \
             level=3  \
             meta=M   \
             hint=self-report  \
             group_item=bahelite_on_error  \
             group_item=bahelite_on_exit   \
             group_item=print_call_stack   \
             min_level=3  \
             max_level=6  \
             level_is_locked='L'


show_help_on_verbosity_channel_Error_handling() {
	cat <<-EOF
	Error_handling (self-report channel)

	Controls the verbosity of processing errors, handling the signals such
	as EXIT, ERR, TERM etc., and the hooks that should run before the program
	quits. A debugging feature.

	Levels:
	    3 – No additional messages (the default).
	    4 – A message is printed every time that a trap on EXIT or ERR is
	        entered;
	    5 – In addition to the above, report about the execution of user-
	        defined hooks “on_exit()” and “on_error()” and whether such hooks
	        are set; report about the logging, when it’s enabled;
	    6 – This level is to debug the call stack itself: an additional, simple
	        call stack will be printed along the ususal one. The simple call
	        stack doesn’t hide bahelite internals and adds some internal vari-
	        ables, however, it doesn’t warrant, that the chain printed will
	        actually be full: sometimes a call or two will still be missing,
	        but that’s rare and probably caused by the nature of subshell calls
	        in Bash.
	EOF
}
#  No export: active only in the main context.


vchan setup  name=call_stack  \
             level=0          \
             meta=M           \
             hint=output      \
             min_level=0      \
             max_level=1


show_help_on_verbosity_channel_call_stack() {
	cat <<-EOF
	call_stack (output channel)

	Enforces the call stack to be printed on exit. A debugging feature.

	Levels:
	    0 – The default level. Call stack is printed only when Bash encounters
	        an error;
	    1 – Call stack is printed on exit, independently of whether the exit
	        was “clean” or not. Mind, that printing the call stack right
	        before exit may be too late to get any interesting information,
	        as by that time the execution might have left the context, where
	        the call to EXIT (or ERR) did happen. Placing “err-stack” in the
	        code (a call to function “err-stack”) would report about what’s
	        going on more precisely, provided, that you have an idea, where
	        to place it.
	EOF
	return 0
}
#  No export: active only in the main context.


 # Stores values, that environment variable $LINENO takes, in an array.
#
declare -gax BAHELITE_STORED_LNOS=()


 # Stores values, that environment variable $BASH_COMMAND takes, in an array.
#
declare -gax BAHELITE_STORED_BASH_CMDS=()

 # Helps to catch nasty errors and to process them better.
#
#  Requires trap on debug to be set and functions, subshells and command sub-
#  stitutions to inherit it, i.e.  bahelite_toggle_ondebug_trap  must be
#  called below to set DEBUG trap and the mother script should have “set -T”.
#
#  A trap on DEBUG is a great means, but it must be used with utter caution:
#  there’s no user’s on_debug() for a reason. See the documentation for details.
#
bahelite_on_each_command() {
	local  retval=$?
	local  i
	local  line_number="$1"
	#  We don’t need more than two stored LINENO’s:
	#  - one for the failed command we want to catch,
	#  - and one that inevitably caused by calling this trap.
	local  lnos_limit=3

	local  bash_cmd="$2"
	local  bash_cmds_limit=2


	#  One of the two things, that this function does is to aid with printing
	#  a meaningful call stack. Normally, just passing $LINENO to the traps
	#  on EXIT and ERR is enough, but when bash encounters not a simple, but
	#  a syntactic error, $LINENO returns bullshit. And to retrieve the pro-
	#  per line number of the last executed command, we have to keep on hand
	#  a small stack of $LINENO values, and take the line of the last command
	#  from there. Though in most cases (i.e. with simple errors) there would
	#  be no difference between this approach and simply passing $LINENO. See
	#  the comment on the function, which toggles the trap on DEBUG on for
	#  further information.
	#
	for ((i=${#BAHELITE_STORED_LNOS[*]}; i>0; i--)); do
		BAHELITE_STORED_LNOS[i]=${BAHELITE_STORED_LNOS[i-1]}
	done

	(( ${#BAHELITE_STORED_LNOS[*]} == (lnos_limit+1) ))  \
		&& unset BAHELITE_STORED_LNOS[$lnos_limit]
	BAHELITE_STORED_LNOS[0]=$line_number

	#  This maintains an array of values that $BASH_COMMAND takes, it’s also
	#  to print sane lines in the call stack, but this time the code aids the
	#  second purpose of this function, about which you may read in the next
	#  commentary block.
	#
	for ((i=${#BAHELITE_STORED_BASH_CMDS[*]}; i>0; i--)); do
		BAHELITE_STORED_BASH_CMDS[i]=${BAHELITE_STORED_BASH_CMDS[i-1]}
	done

	(( ${#BAHELITE_STORED_BASH_CMDS[*]} == (bash_cmds_limit+1) ))  \
		&& unset BAHELITE_STORED_BASH_CMDS[$bash_cmds_limit]
	BAHELITE_STORED_BASH_CMDS[0]=$bash_cmd

	#  Happily, with this we can aid the err() function to exit the program
	#  even in the contexts, where the call to “exit” wouldn’t work due to
	#  shell specifics. (See test no. 24 in ./tests/error handling/error_
	#  catching.sh for details.)
	#
	(( $retval > 0 )) && [ -v BAHELITE_STIPULATED_ERROR ] && {
		#
		#  Making sure, that the aiding code works only /after/
		#  bahelite_on_error() has run once. The “…error_processed” marker
		#  file is an important indicator, that bash has entered the procedure
		#  of quitting the subshell, and nothing can meddle with it. (Not be-
		#  cause entering a trap per se makes some things frozen – at least,
		#  it’s not the case here – but because once the trap is entered, it
		#  unsets the DEBUG and RETURN traps.) And the thing that can meddle
		#  is… this function itself: in the very last moment within __msg(),
		#  where the “return” is called with a non-zero number, this trap can
		#  do what resembles an interception of what err() is supposed to do.
		#  The err() function must make us exit from the subshell neatly: the
		#  error is processed once, message is shown once, as we go up and up
		#  from a subshell to a subshell, returning to the main context, our
		#  traps aren’t bothering processing, until it’s the very time to exit,
		#  and bahelite_on_exit() executes the “last moment before exit” type
		#  of jobs. That’s when err() and traps work together as intended. Now
		#  without the check below, as __msg() executes “return N”, where N>0,
		#    1) by the rule of errexit, the trap on ERR should be triggered;
		#    2) thus the next command should be either the line specified to
		#       the “trap” command, or the first line in bahelite_on_error();
		#    3) but as the trap on DEBUG (this very function) is also active,
		#       it sees the retval > 0 and BAHELITE_STIPULATED_ERROR set by
		#       __msg(), and triggers an “exit” of its own(!)
		#    4) …which is then seems to be caught by errexit, and this time
		#       the execution leaves the subshell, where err() was originally
		#       called, and the outer scope (or context) receives a non-zero
		#       return code(!!)
		#    5) …which is then processed by bahelite_on_error() as a regular
		#       shell error, and a superfluous and unspecific “Bash error.
		#       See console” error-class message is shown on desktop – because
		#       /it’s the first time, that bahelite_on_error was actually call-
		#       ed!/
		#
		#       bahelite_on_error() should’ve been called one level deeper,
		#       but because the trap on DEBUG “stole her turn”, it couldn’t.
		#       This is why after checking the first two conditions (which
		#       are easy and not adducing a huge penalty speed-wise), another
		#       condition is required.
		#
		#  Now the last thing to describe is that why a check on BASH_SUBSHELL
		#  is there. It’s needed, because this trap on debug catches here the
		#  non-zero return codes coming from /logical expressions/ and /condi-
		#  tions/. Those commands to the left of “||” or between “if–then”.
		#  If the check on the marker file would affect main context execution
		#  pipeline, then this check simply wouldn’t work. So, one way to deal
		#  with subshells, other way to deal with logical expressions. Now to
		#  solve cases when these two are nested… <to be done>
		#
		#  Remember to prioritise tests nos. 17, 18 and 24 in the test suite,
		#  if you’re going to implement those esoteric cases with subshells
		#  and logical expressions nested into each other.
		#
		#  The situation, which is described in the first half of this comment,
		#  is reproduced in the test no. 18.
		#
		(( BASH_SUBSHELL > 0 )) && {
			[ ! -e "$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.error_processed" ]  \
				&& return 0
		}
		declare -g BAHELITE_ERROR_CAUGHT_BY_DEBUG_TRAP=t
		exit $retval
	}
	return 0
}
export -f  bahelite_on_each_command


 # This trap handles various signals that cause bash to quit.
#
#  On handling signals
#
#  SIGEXIT is handled, because not all errors trigger SIGERR.
#  SIGQUIT is always ignored by bash. On catching SIGQUIT bash only prints
#    “Quit”, “Segmentation fault” and no trap is processed.
#  SIGKILL forcibly wipes the process from the memory, this doesn’t leave any
#    possibility to handle this case. Just as planned by the OS, actually.
#  SIGINT, SIGTERM, SIGHUP and SIGPIPE are all common causes of induced termi-
#    nation, and these four signals are successfully handled with bash traps.
#
#  On exit codes
#
#  1 – is not used. It is a generic code, with which the main script may exit,
#      if the programmer forgets to place his exit’s and return’s properly.
#      Let these mistakes be exposed.
#  2 – is not used. Bash exits with this code, when it catches an interpreter
#      or a syntax error. Such errors may happen in the main script.
#  3 – Bahelite exits with this code, if the system runs an incompatible ver-
#      sion of the Bash interpreter, or the interpreter is not bash.
#  4 – Bahelite uses this code for all internal errors, i.e. related to the
#      inner mechanics of this library. Most commonly an exit code 4 means
#      a failed self-check at the initialisation stage: a lack in minimal
#      dependencies, failing to load a module, on an unsolicited attempt
#      to source the main script (instead of executing it).
#  5 – any error happening in the main script after Bahelite is loaded.
#      To use custom error codes, use MSG_USE_KEYWORDS and the ERROR_CODES
#      array (see the “messages.sh” module).
#  6 – an exit code issued by the abort() function. This function is to be
#      used instead of err() in cases, when the end user of the main script
#      intentionally interrupts the execution at some stage (e.g. presses
#      “Cancel”). The abort() messages have appearance of informational ones,
#      but since the program didn’t complete whatever it was made for, the
#      exit code “0” doesn’t suit here. The exit code “5” doesn’t suit either,
#      because the exit happens not because of a user mistake or due to an
#      uncontrolled situation that has lead to a fault.
#  7…125 – free for the main script. Can be used with ERROR_CODES.
#  126…165 – not used by Bahelite and must not be used in the main script:
#      this range belongs to the interpreter.
#  166…254 – free for the main script. Can be used with ERROR_CODES.
#  255 – is not used. This is an ambiguous code, that Bash triggers
#      for more than one reason.
#
#  You are strongly advised to rely on err() and abort() to exit from the
#  program:
#    - it’s styled “echo” and “exit” in one bottle!
#    - like all message functions, they output their messages directly to
#      to console/log, thus you may send info’s, warnings and error messages
#      within command substitutions $(…), without fear, that the message will
#      be grabbed together with the rest of stdout;
#    - err() prints a nice call stack (if it doesn’t, you can force it to
#      by raising the level of the “call_stack” verbosity channel to 1; or
#      use err-stack() to always print an error message with a stack at that
#      point);
#    - err() and abort() by default duplicate their messages to desktop
#      (if the module is loaded);
#    - err() has a companion redmsg(), which is basically “a red info()”.
#      While err() whose message may be duplicated as a desktop message,
#      can be left concise, detailed information associated with that error
#      might be printed with redmsg before calling err().
#
#  There’s no need to employ ERROR_CODES and MSG_USE_KEYWORDS, unless you
#  absolutely have to have distinctive error codes AND maintain a subsystem
#  holding the descriptions for these codes.
#
#  It’s no problem to use just “exit”, only remember to stay in ranges 7…125
#  and 166…254.
#
bahelite_on_exit() {
	#
	#  First to do:
	#  - to disable xtrace; for even if the programmer has put set +x where
	#    needed, the program might catch an error before that;
	#  - detach the traps from signals; the DEBUG and RETURN traps have made
	#    their service and aren’t needed any more; traps on interrupting
	#    signals may cause a recursiion, which is to be prevented (esp. in
	#    the “messages” module);
	#  - to drop the message indentation level; an end is an end.
	#
	(( ${VERBOSITY_CHANNELS[bahelite_on_exit]} < 4 )) && shopt -qo xtrace && {
		#
		#  In case the xtrace feature was turned on by the time this message is
		#  shown, it’s more sensible to stop xtrace immediately and avoid debug
		#  output take a screenful or two, when running info().
		#
		builtin set +x  #  Sic!
		info "Turning off xtrace! Raise the verbosity level for the “Error_handling”
		      channel to let xtrace stay enabled when it enters ${FUNCNAME[0]}!"
	}
	trap ''  DEBUG  EXIT  TERM  INT  HUP  PIPE  ERR  RETURN

	local  command="$1"
	local  command_before_that="$2"
	local  retval="$3"
	local  real_line_number="$4"
	local  real_cwd="$5"
	local  signal="$6"

	#  The use of own channel (instead of the group leader) is made explicit
	#  to avoid a discrepancy: we use info-4 and info-5, which compare their
	#  minimal level to the group item’s (bound to this particular function
	#  name), verbosity channel. Setting $v_level to “Error_handling” would
	#  make this function use two different channels, and if Error_handling
	#  would be specified directly somewhere (which is a mistake), that could
	#  lead to some out shown and some not, which would be a silent failure
	#  in reporting.
	#
	local  v_level=${VERBOSITY_CHANNELS[bahelite_on_exit]}
	local  error_marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.error_processed"
	local  dump_vars_marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.do_dump_vars"
	local  dont_clear_tmpdir_marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.dont_clear_tmpdir"

	info-4 'Hello from bahelite_on_exit()!'

	(( v_level >= 5 ))  \
		&& declare -p BASH_SUBSHELL SHLVL ORIG_SHLVL command retval

	 # All errors, that is, cases when the program logic doesn’t work, end up
	#  with SIGEXIT. Forceful interruptions or terminal issues are a special
	#  kind of errors, because now we don’t know for certain, if some code has
	#  triggered an error (was it a pipe issue, perhaps?) or it was influence
	#  from the outside (a human pressing Ctrl-C). Thus the return code has
	#  to be disregarded and the related routines won’t run. But some other
	#  still must run: such as the main program’s  on_exit()  or other func-
	#  tions tidying up the hard drive: clearing caches, deleting tmpdir and
	#  such.
	#
	[ "$signal" != 'EXIT' ]  \
		&& redmsg "Caught SIG$signal. Processing the last return code will be skipped."

	#  It’s futile to have a marker file for the trap on EXIT, for if it runs
	#  completely once, it wipes the temporary directory, where the marker
	#  file would be placed. But such file isn’t needed, as for the conveying
	#  control to “bahelite_on_error” in the first half we use its marker file,
	#  and subshell runs are cut off with a check on BASH_SUBSHELL. What’s left
	#  is the runs in the main execution context, but that also means our glo-
	#  bal variables will stay with us, if we set them. So…
	#
	[ -v BAHELITE_EXIT_PROCESSED ] && return $retval


	 # Consider the following
	#
	#  ⋅ Some internal bash errors, like the “unbound variable” (the one you
	#    catch with “set -u”), do not actually trigger SIGERR, so they must be
	#    caught in the trap on SIGEXIT by the return code.
	#
	#  ⋅ When you use err() to quit after a handled error, SIGERR isn’t trig-
	#    gered either – err() and the like simply throw a fancy message and
	#    call “exit” with a code higher or equal than 5, depending on the com-
	#    mand in question.
	#
	#  ⋅ Stipulated errors (catching a SIGERR within a subshell or calling
	#    err() in a subshell context) are tricky, because there are several
	#    combinations in which SIGERR and SIGEXIT may be triggered. (As you
	#    remember, shell errors do not necessarily trigger SIGERR.) Thus we
	#    may catch a SIGERR in subshell first, then in the main context it
	#    will be either another SIGERR (normally) or SIGEXIT (if there is
	#    some logic chain, i.e. … && … || …, involved). And vice versa,
	#    a SIGEXIT in a subshell context may stay SIGEXIT in the main context
	#    or become a SIGERR (and then SIGEXIT). Irrelevant of the order in
	#    which traps are called, an error must be processed as an error, even
	#    if it comes with a SIGEXIT. Thus there is a condition to call the
	#    trap on SIGERR from the trap on SIGEXIT. Furthermore, irrelevant of
	#    the amount of times signals would be received, we must catch and
	#    process the first, the original signal, which stops the program (or
	#    should stop by the logic of the program), and disregard signals, that
	#    appear later, as this would most certainly lead to a confusion. Each
	#    of the signals – SIGERR and SIGEXIT must be processed only once in the
	#    normal case. Exclusively for the debugging purposes it is allowed –
	#    with special settings to the “call_stack” verbosity channel – to show
	#    the stack more than once.
	#
	#  ⋅ Remember, that expressions like
	#
	#        … && exit $?
	#        … || exit $?
	#        … && return $?
	#        … || return $?
	#
	#    are evil, and if there’s a subshell involved
	#
	#        a=$( … ) || exit $?
	#
	#    they’re twice as evil.
	#

	 # Dealing with error codes, which halt the program, but do not trigger
	#  SIGERR. So we relay the control to bahelite_on_error for a moment.
	#
	[ "$signal" = 'EXIT' ] && (( retval > 0 ))  && {
		#
		#  Only if bahelite_on_error wasn’t already called.
		#
		[ ! -e "$error_marker_file" ] && {
			#
			#  Only if we’re not dealing with a handled error, a stipulated,
			#  intended exit as with err(), abort() and such functions, that
			#  themselves call “exit” (handled errors do not need the call
			#  stack to be printed, no need to dump variables etc.).
			#
			[[ ! -v BAHELITE_STIPULATED_ERROR ]] && {
				#
				#  As an exception from the rule, permit the call to “exit”
				#  from the main script as a kind of handled error. Who knows,
				#  maybe the programmer didn’t like or didn’t know about the
				#  greatness of err() and other functions?
				#
				! [[ "$command" =~ ^exit[[:space:]]([1-9]|[0-9]+) ]]  && {
					bahelite_on_error "$command"              \
					                  "$command_before_that"  \
					                  "$retval"               \
					                  "$real_line_number"     \
					                  "$real_cwd"             \
					                  "from_on_exit"
				}
			}

			#  Also, while we’re still in the (possibly) deepest context,
			#  at the level when SIGEXIT or SIGERR were triggered first,
			#  check if err-stack has requested the call stack to be
			#  printed.
			#
			[ -v BAHELITE_ERR_STACK_CALL ] && print_call_stack
		}
	}

	unset BAHELITE_STIPULATED_ERROR

	#  If we’re in a subshell, quit early, do not(!) place the marker file.
	#  There’s no need in a separate marker file for the first half of this
	#  function, as there’s essentially only the call to bahelite_on_error
	#  and we already check for SIGERR trap marker file there.
	#
	(( BASH_SUBSHELL > 0 )) && {
		info-4 "Quitting early: we’re in a subshell."
		return $retval
	}
	#
	#  Further go common jobs that are to be performed before exit,
	#  independently of the exit code. These jobs are to be done in the
	#  main execution context. It must be the very last EXIT.
	#

	#  Dropping message indentation level. In bahelite_on_exit() it should
	#  be done even at the time actual exit jobs are going to start.
	#
	__mi=''

	#  Run user’s on_exit(). Keep in mind, that at this point
	#  $? is already frozen.
	info-5 "Running user’s on_exit()…"
	milinc-5
	if [ "$(type -t on_exit)" = 'function' ]; then
		on_exit
		msg-5 "Finished."
	else
		msg-5 "User hook is not set."
	fi
	mildec-5

	(( ${VERBOSITY_CHANNELS[call_stack]} != 0 ))  \
		&& print_call_stack

	#  Reading a marker file is made to set a global variable which in its
	#  turn works as a flag permitting the execution, because a marker file
	#  is not the only way to enable certain functionality. Some are enforced
	#  with raised verbosity and have nothing to do with errors, subshells
	#  and can use a more direct way than a marker file – a global flag.
	#

	#  Must be called before attempting to delete TMPDIR.
	bahelite_dump_variables_on_exit

	#  List verbosity channels, if requested, or detect unknown (possibly mis-
	#  spelled) verbosity channels specified to --verbosity.
	#
	if [ -v BAHELITE_VERBOSITY_LIST_CHANNELS ]; then
		vchan list
	else
		vchan detect_unknown_channels
	fi

	[ "$(type -t delete_tmpdir)" = 'function' ] && delete_tmpdir

	#  This must be the latemost action, if possible, as when logging stops,
	#  the console output ceases: to close the logging pipes “the natural
	#  way”, i.e. without triggering an error, file descriptions from stdin
	#  and stdout are redirected to /dev/null. (It’s an OS-dependent issue,
	#  though. On the developer’s machine logging stops without that redirec-
	#  tion.)
	#
	[ "$(type -t stop_logging)" = 'function' ] && stop_logging

	BAHELITE_EXIT_PROCESSED=t

	#  ULTRAKILL. Don’t do that.
	#
	# kill -- -$ORIG_BASHPID
	return 0
} \
   >&2

export -f bahelite_on_exit


 # Prints the stack of the function calls
#
#  No “bahelite_” prefix, as it also works as a general-purpose means
#  to get the call stack at an arbitrary point in the code.
#
#  Every output from this function must go to (the real) stderr,
#  because *err*() may as well be called from a subshell
#
print_call_stack() {
	#  Must be the very first line in this function!
	local  real_lineno_for_a_flying_call="${BAHELITE_STORED_LNOS[-1]}"
	#
	#  Why the group item’s channel is used, see in the similar place
	#  in bahelite_on_exit().
	#
	local  v_level=${VERBOSITY_CHANNELS[print_call_stack]}

	#  Message indentation
	declare -g __mi

	#  Indicator or an explicit request to print a call stack at an arbitrary
	#  point. Allows for the call stack to be printed more than once.
	#
	local  flying_call=t

	local  marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.call_stack_printed"
	local  old_mi_level
	local  f

	for ((f=0; f<${#FUNCNAME[*]}; f++)); do
		[[ "${FUNCNAME[f]}" =~ ^bahelite_on_(exit|error)$ ]] && {
			unset flying_call
			break
		}
	done

	#  In the short period during module loading, an error may happen
	#  between loading this module and “tmpdir”, in which case TMPDIR
	#  wouldn’t be set yet.
	#
	[ ! -v flying_call  -a  -e "$marker_file" ] && {
		info-5 "Not printing call stack: already printed."
		return 0
	}

	local  levels_to_skip=0
	local  prev_file
	local  cur_file
	local  line_number_to_print

	#  Determining how many levels in the call stack (i.e. calls) to skip
	for ((f=0; f<${#FUNCNAME[*]}; f++)); do
		#
		#  There’s no err() and abort() in this list, because
		#    - these functions trigger a “handled exit”, and a handled exit
		#      doesn’t normally request printing the call stack;
		#    - when printing the call stack is explicitly requested, it would
		#      be reasonable to include the point of the stop, i.e. the line
		#      in the code, where the exit was performed. (This might be a
		#      function on the same level in the call stack as err() or abort(),
		#      or even one of them.)
		#
		case "${FUNCNAME[f]}" in
			__msg)
				;&
			bahelite_on_exit)
				;&
			bahelite_on_error)
				;&
			bahelite_on_each_command)
				;&
			${FUNCNAME[0]})
				let ++levels_to_skip
				;;
			*)
				break
		esac
	done

	#  Dropping message indentation level. In bahelite_on_exit() it should
	#  be done even at the time, when the jobs “to be launched right before
	#  the complete exit” are going to start.
	#
	#  Remove padding spaces from __mi, if we’re at the top-level indentation,
	#  or leave it as “mildrop” has reset it – if we’re running a main-script-
	#  in-a-main-script, and our starting indentation level is not right at
	#  the left border (i.e. column 0).
	#
	[ -v flying_call ] && old_mi_level="$__mi"
	__mi=''


	#  Simple, barebones call stack. Gives a more complete view of the inter-
	#  preter internals, with which the usual, fancy call stack is printed.
	#  So uncomment this, if you’d wish to debug the code, whose job is to
	#  print the fancy stack.
	#
	(( v_level >= 6 )) && {
		echo
		echo --- Simple stack ---
		for ((f=0; f<${#FUNCNAME[*]}; f++)); do
			echo -e "${FUNCNAME[f]:-?}\tline ${BASH_LINENO[f]:-?}\tin file ${BASH_SOURCE[f+1]:-?}."
		done | column -t -s $'\t'
		echo "LINENO = $LINENO"
		declare -p BAHELITE_STORED_LNOS
		declare -p real_line_number levels_to_skip
		echo --- End of simple stack ---
	}

	 # Fancy call stack
	#
	#  With flying calls printing directories doesn’t have much sense,
	#  and $real_cwd would be set only when this function is called from
	#  a trap on EXIT or ERR.
	#
	[ ! -v flying_call ] && {
		echo  # in case call stack printed before a line ends.
		echo "Directory, that is hosting the executable file:"
		echo "    ${__bri:-}$MYDIR${__s}"
		echo "Current directory from the interpreter:"
		echo "    ${__bri:-}$real_cwd${__s}"
		echo
	}

	#  Printing the call stack
	#  Omitting the tail containing “main” function with line number “0”,
	#  and from the front we cut the functions, that belong to the trapped
	#  code. And the very last line, which is the one, that triggered an
	#  error, is also outside of this cycle, as its data have to be assem-
	#  bled in a special way.
	#
	for ((f=${#FUNCNAME[*]}-1; f>levels_to_skip; f--)); do
		#  Hide on_exit and on_error, as the error only bypasses through
		#  there. We don’t show THIS function in the call stack, right?
		[ "${FUNCNAME[f]}" = bahelite_on_error ] && continue
		[ "${FUNCNAME[f]}" = bahelite_on_exit ] && continue

		line_number_to_print="${BASH_LINENO[f-1]}"

		#  It’s important to prepend the file path with “in file:” to avoid
		#  a situation when it maybe mistaken for a function name.
		#
		#  By default, the path in front of the main executable is taken for
		#  the default path in the call stack, and all the sourced files will
		#  be shown with relative paths. But if the file(s) residing in the
		#  default path wouldn’t have an extension (e.g. “.sh”), they may be
		#  confused for function names in the call stack.
		#
		#  Indicating path by placing “./” in front of it was disregarded,
		#  because the CWD above could be confused for the path to the main
		#  executable.
		#
		cur_file="${BASH_SOURCE[f]#$MYDIR/}"
		if [ ! -v prev_file ] || [ "$prev_file" != "$cur_file" ]; then
			echo -e "in file: $cur_file"
		fi
		echo -e "    on line $line_number_to_print: ${__mi}${__bri:-}${FUNCNAME[f-1]}${__s:-}"
		prev_file="$cur_file"
	done

	cur_file="${BASH_SOURCE[f]#$MYDIR/}"
	if [ ! -v prev_file ] || [ "$prev_file" != "$cur_file" ]; then
		echo -e "in file: $cur_file"
	fi

	if [ -v BAHELITE_ERROR_CAUGHT_BY_DEBUG_TRAP ]; then
		for ((i=0; i<TERM_COLUMNS-18; i++)); { echo -n '⋅'; }
		echo ' reconstructed ⋅⋅⋅'
		echo -e "    on line <?>: ${__bri:-}err, errw, err-stack, err-ikw or abort${__s:-} (probably)."
		echo -e "in file: ${BAHELITE_DIR#$MYDIR}/modules/core/messages/messages.sh"
		for ((i=0; i<TERM_COLUMNS-18; i++)); { echo -n '⋅'; }
		echo ' reconstructed ⋅⋅⋅'
		echo -e "    on line $real_line_number: ${__bri:-}$command_before_that${__s:-}"
	else
		[ -v flying_call ]  \
			&& echo -e "    on line $real_lineno_for_a_flying_call: ${__bri:-}${FUNCNAME[0]}${__s:-}"  \
			|| echo -e "    on line $real_line_number: ${__bri:-}$command${__s:-}"
	fi

	[ -v flying_call ]  \
		&& echo -e "    ${__w:-}${__bri:-}(flying call)${__s:-}."  \
		|| echo -e "    ${__r:-}${__bri:-}(exit code: $retval)${__s:-}."

	echo

	if [ -v flying_call ]; then
		__mi="$old_mi_level"
	else
		touch "$marker_file"
	fi

	return 0
} \
   >&2

export -f print_call_stack


 # SIGERR is always triggered /before/ SIGEXIT. Not all errors trigger SIGERR,
#  some can be caught only with checking $? in the trap on SIGEXIT. SIGERR may
#  be triggered inside a subshell, and it’s GRAVELY IMPORTANT to process every
#  error at the place where it happened. Thus if ERR is triggered in a subshell,
#  it’s to be processed right there, without delaying until execution returns
#  to the main context. Another important thing is that this trap, unlike the
#  trap on EXIT, should be called only once. Marker files are used to remember
#  once processed signals.
#
bahelite_on_error() {
	(( ${VERBOSITY_CHANNELS[bahelite_on_error]} < 4 )) && shopt -qo xtrace && {
		#
		#  In case the xtrace feature was turned on by the time this message is
		#  shown, it’s more sensible to stop xtrace immediately and avoid debug
		#  output take a screenful or two, when running info().
		#
		builtin set +x  #  Sic!
		info "Turning off xtrace! Raise the verbosity level for the “Error_handling”
		      channel to let xtrace stay enabled when it enters ${FUNCNAME[0]}!"
	}
	trap '' DEBUG RETURN

	declare -gx BAHELITE_DUMP_VARIABLES
	declare -gx BAHELITE_ERROR_PROCESSED

	local  command="$1"
	local  command_before_that="$2"
	local  retval="$3"
	local  real_line_number="$4"
	local  real_cwd="$5"

	#  Meaning, that we deal with a syntactic error,
	#  which didn’t trigger SIGERR.
	#
	local  from_on_exit="${6:-}"
	local  log_path_copied_to_clipboard

	#  Why the group item’s channel is used, see in the similar
	#  place in bahelite_on_exit().
	#
	local  v_level=${VERBOSITY_CHANNELS[bahelite_on_error]}
	local  marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.error_processed"
	local  dump_vars_marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.do_dump_vars"
	local  dont_clear_tmpdir_marker_file="$TMPDIR/$MYNAME_NOEXT.$MY_LAUNCH_TIME.dont_clear_tmpdir"


	info-4  'Hello from bahelite_on_error()!'

	(( v_level >= 5 ))  \
		&& declare -p BASH_SUBSHELL SHLVL ORIG_SHLVL command retval FUNCNAME

	[ -e "$marker_file" ] && return 0

	[ -v BAHELITE_STIPULATED_ERROR  -a  ! -v BAHELITE_ERR_STACK_CALL ] && {
		touch "$marker_file"
		return 0
	}

	#  Dropping message indentation level either to the level upon entrance,
	#  or completely, if the output is on top level.
	#
	__mi=''

	#  This is for the last of the possible calls of bahelite_on_exit().
	touch "$dump_vars_marker_file"

	#  Since an error occurred, let all output go to stderr by default.
	#  Bad idea: to put “exec 2>&1” here
	#  Run user’s on_error().
	#
	info-5 "Running user’s on_error()…"
	milinc-5

	if [ "$(type -t on_error)" = 'function' ]; then
		on_error
		msg-5 "Finished."
	else
		msg-5 "User hook is not set."
	fi
	mildec-5

	print_call_stack

	#  err-stack is a debugging feature, there’s no need to help user to find
	#  the log file. We better spare the user a few messages in the console.
	#
	[ -v BAHELITE_ERR_STACK_CALL ] && {
		touch "$marker_file"
		return 0
	}

	if [ -v BAHELITE_LOGGING_STARTED ]; then
		[ "$(type -t bahelite_notify_send)" = 'function' ]  \
			&& bahelite_notify_send "Bash error. See the log.${log_path_copied_to_clipboard:-}"   \
			                         error
		print_logpath

	else
		if	   [ "$(type -t bahelite_notify_send)" = 'function' ]  \
			&& (( ${VERBOSITY_CHANNELS[desktop]:- -1} >= 1 ))
		then
			bahelite_notify_send "Bash error. See console." error
		fi
		info-5 "Logging wasn’t enabled in $MYNAME.
		        To enable logs, add the module “logging” to BAHELITE_LOAD_MODULES."
	fi

	touch "$marker_file"
	return 0
} \
   >&2

export -f bahelite_on_error


 # Trap on DEBUG is a part of the system, that helps to build a meaningful
#  call trace in case when an error happens.
#
#  Trap on DEBUG is temporarily disabled for the time xtrace shell option
#  is enabled in the mother script. This is handled in the set builtin
#  wrapper in bahelite.sh.
#
bahelite_toggle_ondebug_trap() {
	case "$1" in
		set)
			#  Note the single quotes – to prevent early expansion
			trap 'bahelite_on_each_command "$LINENO" "$BASH_COMMAND"' DEBUG
			;;
		unset)
			#  trap '' DEBUG will ignore the signal.
			#  trap - DEBUG will reset command to 'bahelite_on_each_command… '.
			trap '' DEBUG
			;;
	esac
	return 0
}
#  No export: futile in subshell context.


 # When it is needed to disable the errexit shell option (you usually do this
#  with “set +e”), the SIGERR is still triggered – it only doesn’t make the
#  program quit any more. And the trap associated with SIGERR keeps being
#  called. An average programmer probably won’t remember about this and
#  this creates a problem.
#
#  The trap on SIGERR is useful, when the “errexit” shell option stops execu-
#  tion: the trap would then print the call stack and do other helpful stuff.
#  When “errexit” is unset, this trap becomes more of an inconvenience: as the
#  program won’t stop, SIGERR may be triggered several times, each calling the
#  associated trap function.
#
#  Once the trap is detached from the signal, the “errtrace” shell option
#  (set ±E) has no effect any more, and may be left in either state, set or
#  unset.
#
bahelite_toggle_onerror_trap() {
	case "$1" in
		set)
			#
			#  Note the single quotes – to prevent early expansion
			#  On why BAHELITE_STORED_LNOS is used instead of LINENO,
			#  see the commentary down below.
			#
			trap 'bahelite_on_error "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD"' ERR  # $LINENO
			;;
		unset)
			#  trap '' ERR will ignore the signal.
			#  trap - ERR will reset command to 'bahelite_show_error… '.
			trap '' ERR
			;;
	esac
	return 0
}
#  No export: futile in subshell context.


 # Traps for SIGEXIT and other signals, except ERR, which is controlled above
#
#  Note the single quotes to prevent early expansion.
#  On why BAHELITE_STORED_LNOS is used instead of LINENO,
#  see the commentary below.
#
trap 'bahelite_on_exit "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD" EXIT' EXIT  # $LINENO
trap 'bahelite_on_exit "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD" TERM' TERM
trap 'bahelite_on_exit "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD"  INT'  INT
trap 'bahelite_on_exit "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD"  HUP'  HUP
trap 'bahelite_on_exit "$BASH_COMMAND" "${BAHELITE_STORED_BASH_CMDS[-1]:-\?}" "$?" "${BAHELITE_STORED_LNOS[-2]}" "$PWD" PIPE' PIPE
#
#  Traps on ERR and DEBUG signals may need to be toggled,
#  hence activated through their own wrappers.
#
bahelite_toggle_onerror_trap  set
#
#  The trap on DEBUG has a meaning to be set only when functrace shell option
#  is activated in the main script (usually with “set -T”), so on top of
#  the wrapper there is another check.
#
#  Trap on DEBUG is necessary for the better error handling. Primarily it is
#  needed to avoid cases when $LINENO is bugged, as in “value of an unset
#  variable was used” type of syntactic errors. You can see this for yourself:
#  try changing the ${BAHELITE_STORED_LNOS[-1]} to $LINENO in the traps above,
#  comment the two lines below and run the following test:
#      $ ./tests/error_handling/error_catching.sh 2
#
[ -o functrace ]  \
 	&& bahelite_toggle_ondebug_trap  set


return 0