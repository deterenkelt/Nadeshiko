#  Should be sourced.

#  misc.sh
#  Miscellaneous helper functions.
#  Author: deterenkelt, 2018–2025
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MISC_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MISC_VER='2.1.0'

#  No self-report verbosity channel: here is just many simple functions.


BAHELITE_INTERNALLY_REQUIRED_UTILS+=(
	pgrep   # (procps) Single process check.
#	wc      # (coreutils) Single process check.
#	shuf    # (coreutils) For random(), it works better than $RANDOM.
	bc      # (bc) Verifying numbers.
)
BAHELITE_INTERNALLY_REQUIRED_UTILS_HINTS+=(
	[pgrep]='pgrep is a part of procps-ng.
	http://procps-ng.sourceforge.net/
	https://gitlab.com/procps-ng/procps'
)


__show_usage_misc_module() {
	cat <<-EOF
	Bahelite module “misc” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_misc_module
		exit 0

	else
		redmsg "Bahelite: misc: unrecognised option “$opt”."
		__show_usage_misc_module
		exit 4
	fi
done
unset opt



 # Sets MYRANDOM global variable to a random number either fast or secure way
#  Secure way may take seconds to complete.
#  $1 – an integer number, which will define the range, [0..$1].
#
random-fast()   { __random fast   "$@"; }
random-secure() { __random secure "$@"; }
#
 # Generic function
#  $1 – mode, either “fast” or “secure”
#  $2 – an integer number, which will define the range, [0..$1].
#
__random() {
	declare -gx MYRANDOM
	local mode="${1:-}" max_number="${2:-}"

	case "$mode" in
		fast)    random_source='/dev/urandom';;
		secure)  random_source='/dev/random';;
		*)  err 'Random source must be set to either “fast” or “secure”.'
	esac
	[ -r "$random_source" ] \
		|| err "Random source file $random_source is not a readable file."

	[[ "$max_number" =~ ^[0-9]+$ ]] \
		|| err "The max. number is not specified, got “$max_number”."

	 # $RANDOM is too bad to use even when security is not a concern,
	#  because its seed works bad in containers, and 9/10 times returns
	#  the same value, if you call $RANDOM with equal time spans of one hour.
	#
	#  MYRANDOM will be set to a number between 0 and $max_number inclusively.
	#
	MYRANDOM=$(shuf --random-source=$random_source -r -n 1 -i 0-$max_number)
	return 0
}
export -f  __random    \
           random-fast  \
           random-secure


 # Replaces characters, that are forbidden in Windows™ filenames.
#  $1 – a string, in which the characters have to be replaced.
#  Returns a new string to stdout.
#
replace_windows_unfriendly_chars() {
	local str="${1:-}"
	str=${str//\</\(}
	str=${str//\>/\)}
	str=${str//\:/\.}
	str=${str//\"/\'}
	str=${str//\\/}
	str=${str//\|/}
	str=${str//\?/}
	str=${str//\*/}
	echo -n "$str"
	return 0
}
export -f  replace_windows_unfriendly_chars


 # Allows only one instance of the main script to run.
#
single_process_check() {
	local our_processes        total_processes \
	      our_processes_count  total_processes_count  our_command
	[ ${#ORIG_ARGS[*]} -eq 0 ]  \
		&& our_command="bash $MYNAME_AS_IN_DOLLARZERO"  \
		|| our_command="bash $MYNAME_AS_IN_DOLLARZERO ${ORIG_ARGS[@]}"
	our_processes=$(
		pgrep -u $USER -afx "$our_command" --session 0 --pgroup 0
	)
	total_processes=$(
		pgrep -u $USER -af  "bash $MYNAME_AS_IN_DOLLARZERO"  # sic!
	)
	our_processes_count=$(echo "$our_processes" | wc -l)
	total_processes_count=$(echo "$total_processes" | wc -l)
	(( our_processes_count < total_processes_count )) && {
		redmsg "Processes: our: $our_processes_count, total: $total_processes_count.
		        Our processes are:
		        $our_processes
		        Our and foreign processes are:
		        $total_processes"
		err 'Still running.'
	}
	return 0
}
#  No export: init stage function.


 # Check a number and echo either a plural string or a singular string.
#   $1  – the number to test.
#  [$2] – plural string. If unset, equals to “s”. 
#  [$3] – singular string. By default has no value (and no value is needed).
#
#  Examples
#  1. line – lines
#     echo "The file has $line_number line$(plur_sing  $line_number)."
#        line_number == 1  -->  “The file has 1 line.”
#        line_number == 2  -->  “The file has 2 lines.”
#
#  2. dummy – dummies, mouse – mice
#  echo "We’ve found $mice_count $(plur_sing  $mice_count  mice  mouse)."
#     mice_count == 1   -->  “We’ve found 1 mouse.”
#     mice_count == 2   -->  “We’ve found 2 mice.”
#
#  3. await – awaits
#  echo "$task_count task$(plur_sing  $task_count) await$(plur_sing  $task_count  '' s) your attention."
#     task_count == 1  -->  “1 task awaits your attention.”
#     task_count == 2  -->  “2 tasks await your attention.”
#
#  The name of the function is the mnemonic for the argument order. That they
#    go first plural, then singular may look anti-intuitive, but if the func-
#    tion was called sing_plur, it would add yet another problem,
#    because “plur_sing” sounds more natural.
#
#  As specifying the default plural ending “s” for the function may often seem
#    logical, though not obligatory, the form of the call with the 2nd argument
#    set and the 3rd omitted is also allowed.
#
plur_sing_v() {	plur_sing "$@"; }
plur_sing() {
	local type=noun
	[ "${FUNCNAME[1]}" = plur_sing_v ]  && type=verb

	local  sg_noun_ending=''   # e.g. paper[ ]
	local  pl_noun_ending='s'  # e.g. paper[s]
	local  sg_verb_ending='s'  # e.g. it clean[s]
	local  pl_verb_ending=''   # e.g. they wash[ ]

	local -n sg_ending=sg_${type}_ending
	local -n pl_ending=pl_${type}_ending

	local  num="${1:-}"
	[[ "$num" =~ ^[0-9]+$ ]] || {
		print_call_stack
		warn "${FUNCNAME[0]}: “$num” is not a number!"
		return 0
	}

	(( $# >= 2 )) && sg_ending="$2"
	(( $# >= 3 )) && pl_ending="$3"

	#  Avoiding shell arithmetic
	#  Even in case of error in the main script, this way there’s
	#  a 50/50 chance, that the right string would be printed.
	extglob_on
	[ "${num##+(0)}" = '1' ]  \
		&& echo -n "$sg_ending"  \
		|| echo -n "$pl_ending"

	extglob_rewind
	return 0
}
export -f  plur_sing  \
           plur_sing_v


nth() {
	local number="$1"
	[[ "$number" =~ ^([0-9]|[1-9][0-9]+)$ ]]  \
		|| err "The argument must be a number, but “$number” was given."
	echo -n "$number"
	case $number in
		1) echo -n 'st';;
		2) echo -n 'nd';;
		3) echo -n 'rd';;
		*) echo -n 'th';;
	esac
	return 0
}
export -f nth


 # Determine bash variable type
#  Returns: “string”, “regular array”, “assoc. array”
#  $1 – variable name.
#
vartype() {
	local varname="${1:-}" varval vartype_letter
	[ -v "$varname" ] || {
		print_call_stack
		err "misc: $FUNCNAME: “$1” must be a variable name!"
	}
	declare -n varval=$varname
	vartype_letter=${varval@a}
	case "${vartype_letter:0:1}" in
		a)	echo 'regular array';;
		A)  echo 'assoc. array';;
		*)  echo 'string';;
	esac
	return 0
}


 # Rounds a digit up or down.
#  $1 – a digit or a valid expression for bc.
# [$2] – precision, number of digits after the dot within range from 0 to 10.
#       Equals to 0 by default.
#
round() {
	local  expr="$1"
	local  prec="${2:-0}"

	echo "scale = $prec+1;
	      result = $expr;

	      if ( result > 0 )  \
	         result += 5/10^($prec+1)  \
	         \
	      else if ( result < 0 )  \
	         result -= 5/10^($prec+1);

	      if ( result > -1  &&  result < 0 )  \
	      	 {  \
	      	    print \"-0\";
	      	    scale=$prec;
	      	    -result/1  \
	         }  \
	      else  if ( result < 1  &&  result > 0 )  \
	     	 {  \
	     	    print 0;
	     	    scale=$prec;
	     	    result/1  \
	     	 }  \
	      else   \
	     	 {  \
	     	    scale=$prec;
	     	    result/1  \
	     	 }  \
	     	    \
	      /*  All this crap only because bloody bc cannot print the leading  \
	       *  zero before the dot. God dammit…  \
	       */"  \
	| bc -q

	return 0
}
export -f round
#  Thanks be to https://stackoverflow.com/a/26864946
#  and https://stackoverflow.com/a/42786713



return 0
