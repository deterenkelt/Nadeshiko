#  Should be sourced.

#  check_directory.sh
#  Makes sure, that a directory has R/W permissions. Creates th edirectory,
#    if it doesn’t exist yet. A common dependency of modules, which use
#    or rely on default paths.
#  Author: deterenkelt, 2019–2020
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	return 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_CHECK_DIRECTORY_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_CHECK_DIRECTORY_VER='1.0.1'

#  No self-report verbosity channel: the module is simple.


__show_usage_check_directory_module() {
	cat <<-EOF
	Bahelite module “check_directory” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_check_directory_module
		exit 0

	else
		redmsg "Bahelite: check_directory: unrecognised option “$opt”."
		__show_usage_check_directory_module
		exit 4
	fi
done
unset opt



 # Makes sure, that a directory exists and has R/W permissions.
#  $1 – path to the directory.
#  $2 – the purpose like “config” or “logging”. It is used only in the
#       error message.
#
__check_directory() {
	local  dir="${1:-}"
	local  purpose="${2:-}"

	[ -v purpose ] && purpose="${purpose,,}"
	if [ -d "$dir" ]; then
		[ -r "$dir" ]  \
			|| err "${purpose^} directory “$dir” isn’t readable."
		[ -w "$dir" ]  \
			|| err "${purpose^} directory “$dir” isn’t writeable."
	else
		mkdir -p -m 750 "$dir" || err "Couldn’t create $purpose directory “$dir”."
	fi
	return 0
}
#  No export: init stage function.



return 0