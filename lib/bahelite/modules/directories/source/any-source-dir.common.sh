#  Should be sourced.

#  any-source-dir.common.sh
#  Finds source directories in the installation and sets
#    paths. See source_directories.sh module for the details.
#  Author: deterenkelt, 2019–2023
#

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Get the name of the module in whose stead to act
module_name="${BASH_SOURCE##*/}"
module_name="${module_name%.sh}"

 # When Bahelite modules are loaded not selectively, this module
#  might be loaded too – as is, – but that must be avoided.
#
[ "$module_name" = any-source-dir.common ]  && return 0


#  Avoid sourcing this module twice
[ -v BAHELITE_MODULE_${module_name^^}_VER ] && return 0
#  Declaring presence of this module
declare -grx BAHELITE_MODULE_${module_name^^}_VER='1.0.3'
bahelite_load_module 'source_directories' || exit $?


 # Describes what arguments this module takes (if if takes any)
#
__show_usage_any-source-dir_module() {
	cat <<-EOF  >&2
	Bahelite module “$module_name” arguments:

	subdir=${__so}custom_directory_name${__s}
	        The name of the subdirectory to be searched under system directories
	        such as /usr/(local/)lib, /usr/(local/)share etc. By default the
	        name would be equal to $MYNAME_NOEXT. (Specify it when instead of
	        one main script you have a bundle, and every script in it uses the
	        same source subdirectory.)
	EOF
	return 0
}
#  No export: init stage function.
#  Throwaway function: it is called only when the execution is going to stop,
#  in case of an error or because module was called with “help” as a parameter.
#  It is okay for this function to be redefined each time the module loads.


own_subdir_name="${MY_SUITE_NAME:-$MYNAME_NOEXT}"

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_any-source-dir_module
		exit 0

	elif [ "$opt" = '' ]; then
		:

	elif [[ "$opt" =~ ^subdir=.+ ]]; then
		own_subdir_name="${opt#subdir=}"

	else
		redmsg "Bahelite: $module_name: unrecognised option “$opt”."
		__show_usage_any-source-dir_module
		exit 4
	fi
done
unset opt


 # Creating a channel with a name corresponding to module name
#  (So that the name in BAHELITE_LOAD_MODULES would match the channel.)
#
vchan setup  name=${module_name^}  \
             level=2              \
             meta=M              \
             hint=self-report   \
             min_level=2       \
             max_level=3


 # The varaibles are sanitised, so it’s safe
#  eval is faster than  source <( cat <<-EOF
#
#  Note, that the function uses cat <<-EOF, so the eval’ed code is
#  indented with tabs inside the string.
#
eval "  show_help_on_verbosity_channel_${module_name^}() {
			cat <<-EOF
			${module_name^} (self-report channel)
			    Prints information about setting up source directory $module_name.
			    Levels:
			    2 – no additional output (the default);
			    3 – display the name of the global variable and the value
			        (the path) assigned to it.
			EOF
			return 0
		}
	 "



                         #  Postload job  #

 # Now to have a postload job we essentially need to construct a function
#  with a unique name in runtime. (Unique name is necessary to have, so that
#  other modules, that depends on particular directories, might depend on
#  their respective prepare_*dir() functions.)
#

source <(
	cat <<-EOF
	 # Creates the requested subdirectory and its XDG parent.
	#
	prepare_$module_name() {

		__set_source_dir "${module_name%dir}" "$own_subdir_name"

		return 0
	}
	#  No export: init stage function.
	EOF
)

BAHELITE_POSTLOAD_JOBS+=( "prepare_$module_name" )

unset  module_name  \
       own_subdir_name

return 0
