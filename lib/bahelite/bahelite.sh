#  Should be sourced.

#  bahelite.sh
#  Bash helper library for Linux to create more robust shell scripts.
#  Author: deterenkelt, 2018–2025
#  https://codeberg.org/deterenkelt/Bahelite
#

 # Bahelite is free software; you can redistribute it and/or modify it
#    under the terms of the GNU General Public Licence as published
#    by the Free Software Foundation; either version 3 of the Licence,
#    or (at your option) any later version.
#  Bahelite is distributed in the hope that it will be useful,
#    but without any warranty; without even the implied warranty
#    of merchantability or fitness for a particular purpose.
#  See the GNU General Public Licence for more details.
#

 # It is a requirement, that the main script, which would source bahelite.sh,
#  would use the following shell settings before sourcing:
#      set -eEuT
#      shopt -s inherit_errexit
#  Or use the same set in the shebang line:
#      #! /usr/bin/env -S  bash -eEuT -O inherit_errexit
#
#  Bahelite itself doesn’t enable or disable permanently any shell options,
#  leaving the optimal set wholly on the author of the main script (the one
#  that sources bahelite.sh). Bahelite may switch some options, but only
#  temporarily.
#


                          #  Initial checks  #

 # Necessities from different versions of Bash, that mandate
#  the version requirement:
#  bash v4.3:
#     ⋅ declare -n.
#  bash v4.4:
#     ⋅ fixed “typeset -p” behaviour;
#     ⋅ ${param@x} operators;
#     ⋅ SIGINT respecting built-ins and interceptable by traps;
#     ⋅ BASH_SUBSHELL that is updated for process substitution;
#     ⋅ “inherit_errexit” shell option, that makes errexit actually
#       work in subshells (“set -e” will enable it only in global space
#       and “set -E” will make subshells inherit /the trap/ on ERR, but
#       the “trigger an error on enoucntering non-zero return code”
#       behaviour).
#
if	((
		${BASH_VERSINFO[0]:-0} <= 3  || (     ${BASH_VERSINFO[0]:-0} == 4
		                                   && ${BASH_VERSINFO[1]:-0} <  4
		                                )
	))
then
	echo "Bahelite error: bash v4.4 or higher required." >&2
	#  so that it would work for both sourced and executed scripts
	return 3  2>/dev/null || exit 3
fi

 # Sourced scripts are not allowed. (This is almost always a bad idea.)
#  In case you absolutely need to run Bahelite with a sourced script,
#  *and you are ready, that things will break,* you may export the
#  variable BAHELITE_LET_MAIN_SCRIPT_BE_SOURCED with any value.
#
if	   [ ! -v BAHELITE_LET_MAIN_SCRIPT_BE_SOURCED ]  \
	&& [ "${BASH_SOURCE[-1]}" != "$0" ]
then
	echo "Bahelite error: ${BASH_SOURCE[-1]} shouldn’t be sourced." >&2
	return 4
fi


                #  Cleaning the environment before start  #

 # Wipe user functions from the environment
#
#  This is done by default, because of the custom things, that often
#  exist in ~/.bashrc or exported from some higher, earlier shell. Being
#  supposed to only simplify the work in terminal, such functions may –
#  and often will – complicate things for a script.
#
#  To keep the functions exported to us in this scope, that is, the scope
#  where this very script currently executes, define BAHELITE_KEEP_ENV_FUNCS
#  variable before sourcing bahelite.sh. Keep in mind, that outer functions
#  may lead to an unexpected behaviour.
#
#  Aliases are not inherited through the environment, thankfully.
#
if [ ! -v BAHELITE_KEEP_ENV_FUNCS ]; then
	#  This wipes all functions, which names don’t start with an underscore
	#  (those that start with “_” or “__” are internal ones, mostly
	#  related to completion, and are harmless.)
	while read funcdef; do
		funcdef=${funcdef#declare }   #  strip “declare ”
		funcdef=${funcdef#* }         #  strip keys to declare
		[[ "$funcdef" =~ ^_ ]]  \
			|| unset -f "$funcdef"
	done < <(declare -F)
	unset funcdef
fi

 # The env command in shebang will not recognise -i, so an internal respawn
#  is needed in order to run the script in a totally clean environment.
#
#  But be aware, that “env -i” literally WIPES the environment – you won’t
#  find there neither $HOME nor $USER any more. If you wish simply to get
#  rid of globals from the mother script, simply use “env” (Bahelite over-
#  rides it in the “util_overrides” module for convenience.)
#
if	   [ -v BAHELITE_TOTAL_ENV_CLEAN ]  \
	&& [ ! -v BAHELITE_ENV_CLEANED   ]
then
	#  Re-execute the script in a new, clean environment
	exec /usr/bin/env -i BAHELITE_ENV_CLEANED=t  bash "$0" "$@"
	#  …and stop execution of the currently running script,
	#  as it was only a launchpad.
	exit $?
fi

shopt -qo errexit     \
&& shopt -qo errtrace  \
&& shopt -qo nounset    \
&& shopt -qo functrace   \
&& shopt -q  inherit_errexit || {
	cat <<-EOF >&2
	Please enable the following shell options in your
	program before sourcing Bahelite:

	    set -eEuT
	    shopt -s inherit_errexit

	EOF
	exit 4
}

which realpath &>/dev/null || {
	cat <<-EOF >&2
	“realpath”, which is a required utility, was not found on this system.
	Typically it is installed with “coreutils” package.
	EOF
	exit 4
}

 # Part of the “dump_variables” module.
#
declare -r BAHELITE_VARLIST_BEFORE_STARTUP="$(compgen -A variable)"

 # The lists of defined functions, gathered twice – before and after startup
#  procedures, were supposed to be a part of the subsystem, which would hide
#  Bahelite code from the xtrace output, when debugging the main script. Maybe
#  it will serve another purpose some day.
#
#declare -a BAHELITE_FUNCLIST_BEFORE_STARTUP
#declare -a BAHELITE_FUNCLIST_AFTER_STARTUP
#readarray -d $'\n' -t  BAHELITE_FUNCLIST_BEFORE_STARTUP < <(compgen -A function)



                       #  I. Bootstrap stage  #

                       #   Global variables   #

declare -grx BAHELITE_VERSION="6.6.0"
declare -grx BAHELITE_DIR=${BASH_SOURCE[0]%/*}


 # Specifies which modules to load and their options
#  (set it in your script, not here)
#  (set it /before/ you call bahelite.sh)
#
#  Syntax:
#      BAHELITE_LOAD_MODULES=(
#          module_name1
#          module_name2:optionA
#          module_name3:optionB,optionC=valueC,optionD
#      )
#      source /path/to/bahelite.sh
#
#  ⋅ Module name corresponds to its file name without the “.sh” extension.
#  ⋅ Core modules are loaded automatically (including bootstrap), the rest
#    must be mentioned explicitly.
#  ⋅ With an exception of bootstrap modules, every module takes an option
#    “help”, that prints to console, what arguments does that module take.
#    When the “help” option is used (for any module), the execution of the
#    program stops after displaying it.
#  ⋅ Every line must be a solid string, no spaces within it are allowed.
#
declare -ga BAHELITE_LOAD_MODULES


 # Modules may define startup jobs as elements in this array
#
#  When modules are loaded, they mostly just define functions, and if they
#  execute any code, it’s small and defines the necessary abstracts, things,
#  that don’t depend on anything. The functions, that organise facilities,
#  *may* depend on other functions-facility-organisers. These dependencies
#  are solved after loading modules. At the end of this script the functions
#  defined in this array, are called as “startup jobs” and their dependencies
#  are resolved then.
#
#  This array is to be used and expanded ONLY by Bahelite modules.
#  Item format: “func_name” or “func_name:after=another_funcname”.
#
declare -ga BAHELITE_POSTLOAD_JOBS=()


                      #  Main script variables  #

#  $0 == -bash if the script is sourced.
[ -f "$0" ] && {
	declare -g MYNAME="$(realpath --logical "$0")"
	declare -grx MYNAME="${MYNAME##*/}"
	declare -grx MYNAME_NOEXT=${MYNAME%.*}

	 # So that code in sourced modules could have the value of $0,
	#  as if it was running in the file called for execution.
	#  Used, e.g. in  single_process_check()  in misc.sh.
	#
	declare -grx MYNAME_AS_IN_DOLLARZERO="$0"
	declare -grx MYPATH=$(realpath --logical "$0")
	declare -grx MYDIR=${MYPATH%/*}

	 # Used for desktop notifications in bahelite_messages_to_desktop.sh
	#  and in the title for dialog windows in bahelite_dialog.sh
	#
	[ -v MY_DISPLAY_NAME ] || {
		#  Not forcing lowercase, as there may be intended
		#  caps, like in abbreviations.
		declare -gx  MY_DISPLAY_NAME="${MYNAME_NOEXT^}"
		declare -r   MY_DISPLAY_NAME="${MY_DISPLAY_NAME//_/ }"
	}

	 # Useful for logs, dumps and marker files. In case several Bahelite-enab-
	#  led scripts would use the same $TMPDIR, files belonging to each script
	#  will be distinguished by the launch time.
	#
	declare -grx MY_LAUNCH_TIME=$(date +%Y-%m-%d_%H:%M:%S)


	 # Common name that lets a set of scripts use a single config directory,
	#  cache directory etc., while the scripts themselves may have different
	#  names.
	#
	#  Having a globally defined variable removes the need to specify that
	#  common name as loading options to modules, that find/create common
	#  directories. See /modules/directories/source/any-source-dir.common.sh.
	#  As each script in a suite is supposed to have their own config file,
	#  the presence of MY_SUITE_NAME automatically forces recognition by
	#  name in the rcfile module (a default “.rc.sh” or ”config.rc.sh” now
	#  won’t be loaded). MY_SUITE_NAME doesn’t affect modules like “tmpdir”
	#  and “logging” etc., except when a module uses basic directories
	#  (e.g. logging, that typically depends on CACHEDIR).
	#
	[ -v MY_SUITE_NAME ] && declare -grx MY_SUITE_NAME

	declare -grx ORIG_BASHPID=$BASHPID
	declare -grx ORIG_PPID=$PPID
	declare -grx ORIG_SHLVL=$SHLVL
}



                      #  Command line arguments  #

declare -grx CMDLINE="$0 $*"

 # ARGS array is for the common use, and it may undergo changes, as the
#  main script would find necessary. A common change would happen when
#  the main script calls read_rcfile() from the rcfile module. It will read
#  a config file name (argument that ends on “.rc.sh”) and set it to the
#  RCFILE variable, then delete this item from the argument list.
#
declare -gx ARGS=("$@")

 # ORIG_ARGS is set once and for all, it will always have the list
#  of arguments as they were passed to the program. It should be relied
#  upon instead of ARGS, when the arguments need to be shown as the user
#  provided them, without any (pre)processing.
#
declare -grx ORIG_ARGS=("$@")



                      #  II. Loading modules  #

 # Loading bootstrap modules
#
#  During the bootstrap stage Bahelite is limited in the means to perform
#  self-reporting. To help that, it first loads modules that provide basic
#  or abridged functionality. See the README file in /modules/bootstrap
#  for details.
#
for module in 'read_module_options.sh'    \
              'terminal.sh'                \
              'messages_bootstrap.sh'       \
              'message_indentation.sh'       \
              'verbosity_base.sh'             \
              'checking_core_dependencies.sh'  \
              'load_modules.sh'
do
	source "$BAHELITE_DIR/modules/core/bootstrap/$module" || exit $?
done
unset module

 # Loading core modules
#  and user-specified modules
#
bahelite_load_modules

 # Checking modules’ dependencies
#
check_required_utils



                #  III. Running modules’ postload jobs  #

 # Part of the “dump_variables” module. Should be executed after all
#  the necessary modules are loaded, but before launching postload jobs.
#
bahelite_gather_the_after_startup_varlist
#
#  Was supposed to be a part of the “util_overrides” module, in hiding Bahe-
#  lite code from xtrace by default. If it were to be used, this had also to
#  be run before postload jobs.
#readarray -d $'\n' -t BAHELITE_FUNCLIST_AFTER_STARTUP < <(compgen -A function)
#declare -r BAHELITE_FUNCLIST_AFTER_STARTUP

bahelite_validate_postload_jobs "${BAHELITE_POSTLOAD_JOBS[@]}"

bahelite_run_postload_jobs "${BAHELITE_POSTLOAD_JOBS[@]}"

(( ${VERBOSITY_CHANNELS[bahelite]} >= 3 ))  && {
	info "Bahelite v$BAHELITE_VERSION is loaded."
	milinc
	info "Installed to “$BAHELITE_DIR”."
	info "To see which modules were loaded, raise the verbosity level on the
	      “Load_modules” channel."
	mildec
}

update_timeformat


return 0
