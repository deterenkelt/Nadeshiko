#  Should be sourced.

#  gather_source_info_cprops.sh
#  A submodule of gather_source_info.sh that holds functions to retrieve
#  container properties.
#  Author: deterenkelt, 2018–2021
#
#  For licence see nadeshiko.sh
#


                          #  Container info  #

gather_cprop_ffprobe() {
	source_c[ffprobe]=$(
		$ffprobe -hide_banner -v error  \
		         -show_format  \
		         -of default=noprint_wrappers=1  \
		         "${source[path]}"  \
		    | grep -vE '=N/A\*$'
	)
	[ "${source_c[ffprobe]}" ]  \
		|| warn "Couldn’t retrieve ffprobe for the container metadata."
	return 0
}


 # Some containers will have duration specified in the container metadata,
#  so it will work as a backup for the length of the video stream.
#
gather_cprop_duration_total_s_ms() {
	#  The format in this field differs for mediainfo, so we assume, that it
	#  may be either “N”, “N.NNN” or “N.NNNNNNNNN”. We need to guarantee, that
	#  it conforms to either the first or the second form, and strip extra ms
	#  precision: 6400.360xxxxxx → 6400.360
	[[ "${source_c[duration_total_s_ms]}" =~ ^([0-9]+)((\.[0-9]{1,3})[0-9]*|)$ ]]  \
		&& source_c[duration_total_s_ms]="${BASH_REMATCH[1]}${BASH_REMATCH[3]}"
	return 0
}


gather_cprop_attachments_count() {
	local  slashes

	if [ -v source_c[attachments] ]; then
		#
		#  The string holds attachments separated with “ / ”: a space, then
		#  a slash and one more space.
		#  Example:  "font1.otf / font2.ttf / FONT3.TTF"
		#  So the number of slashes is the attachment count minus one.
		#
		slashes="${source_c[attachments]//[^\/]/}"
		source_c[attachments_count]=$(( ${#slashes} + 1 ))
	else
		#  Empty string guarantees that mediainfo reported no attachments.
		source_c[attachments_count]=0
	fi
	return 0
}


gather_cprop_bitrate_by_extraction() {
	if	[ ! -v source_v[bitrate_by_extraction] ]                \
			|| (                                                 \
			          (( source_c[audio_streams_count] > 0 ))  \
			       && [ ! -v source_a[bitrate_by_extraction] ]     \
			   )
	then
		warn "Couldn’t determine overall bitrate by extraction."
		return 0
	fi

	source_c[bitrate_by_extraction]=$((
	      ${source_v[bitrate_by_extraction]}  \
	    + ${source_a[bitrate_by_extraction]:-0}
	))
	return 0
}


gather_cprop_muxing_overhead_B() {
	if [ ! -v source_v[stream_size_by_extraction_B] ]              \
			|| (                                                    \
			          (( source_c[audio_streams_count] > 0 ))        \
			       && [ ! -v source_a[stream_size_by_extraction_B] ]  \
			   )
	then
		warn "Couldn’t calculate muxing overhead."
		return 0
	fi

	source_c[muxing_overhead_B]=$((      source[size_B]
	                                 -   source_v[stream_size_by_extraction_B]
	                                 - ${source_a[stream_size_by_extraction_B]:-0}
	))
	[[ "${source_c[muxing_overhead_B]}" =~ ^$INT$ ]]  \
		|| unset source_c[muxing_overhead_B]
	return 0
}


gather_cprop_ovh_ratio_to_fsize_pct() {
	[ -v source_c[muxing_overhead_B] ] || return 0
	source_c[ovh_ratio_to_fsize_pct]=$(
		echo "scale=4;    ${source_c[muxing_overhead_B]}  \
		                * 100  \
		                / ${source[size_B]}"  | bc
	)
	[[ "${source_c[ovh_ratio_to_fsize_pct]}" =~ ^$FLOAT$ ]]  || {
		warn "Couldn’t determine overhead ratio to file size as a percentage."
		unset source_c[ovh_ratio_to_fsize_pct]
	}
	return 0
}


gather_cprop_ovh_ratio_to_1sec_of_playback() {
	local one_second_of_playback

	[ -v source_c[muxing_overhead_B] ] || return 0
	[ -v source_v[bitrate_by_extraction] ] || return 0
	if	   (( source_c[audio_streams_count] > 0 ))  \
		&& [ -v source_a[bitrate_by_extraction] ]
	then
		warn "Couldn’t determine overhead ratio to one second of playback."
		return 0
	fi

	one_second_of_playback=$((    (   source_v[bitrate_by_extraction]     / 8)
	                            + ( ${source_a[bitrate_by_extraction]:-0} / 8)
	                        ))
	source_c[ovh_ratio_to_1sec_of_playback]=$(
		echo "scale=4;   ${source_c[muxing_overhead_B]}   \
		               / $one_second_of_playback"  | bc
	)
	[[ "${source_c[ovh_ratio_to_1sec_of_playback]}" =~ ^[0-9]*(\.[0-9]+|)$ ]]  || {
		warn "Couldn’t determine overhead ratio to one second of playback."
		unset source_c[ovh_ratio_to_1sec_of_playback]
	}

	return 0
}


return 0