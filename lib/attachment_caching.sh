#  Should be sourced.

#  attachment_caching.sh
#  This library provides an ability to store extracted fonts and attachments
#  for future use, saving time on further runs with the same file.
#  Author: deterenkelt, 2021–2024
#
#  For licence see nadeshiko.sh
#

 # Reasoning
#
#  1. Extracting attachments may take a long time on files reaching several
#       gigabytes in size, and places a considerable penatly on the encoding
#       time for sources taking 100 gigabytes and more. As Nadeshiko processes
#       each cut with a separate run, without caching the extraction would nor-
#       mally also be repeated for each fragment to be cut from the source file.
#  2. The hardusbbing filters in ffmpeg require fonts to be placed to a speci-
#       fied folder on the disk, and only then they can be seen (if they are
#       not system fonts, of course). This created a need to extract the attach-
#       ments from every video file, that uses custom fonts.
#  3. Attachments like fonts and images may be injected as text in the SSA
#       subtitles, and ffmpeg’s hardsubbing filters of course cannot use them
#       just like that, first the subtitles have to be extracted, then their
#       attachments.
#

 # How caching works
#
#  Before running ffmpeg, Nadeshiko extracts all attachments from the conatiner
#  to $TMPDIR/extracted. Then it analyses the text files (SSA subs) for built-
#  in attachments and extracts them to the same directory. Now the folder is
#  ready to be backed up to ~/.cache/nadeshiko/cached_attachments/.
#
#  To find the cached attachments quickly, they are stored into folders named
#  after the source file inode and device numbers, something like “1088356-
#  2080”. Thus, first the source file’s inode and device numbers are retrieved,
#  and a subdirectory with a corresponding name is created in the directory
#  that holds the cache. Later, if another cut would be made from that source
#  file, its inode and device numbers would be queried as a folder name in
#  the cache directory, and if none will be found, the extraction will be
#  performed.
#
#  To double check, that a cache belongs to a particular source video, an empty
#  file is created within the cache, and it’s created with the same name as the
#  source file. When both pairs match: the cache folder name and source’s inode
#  and device numbers, the source file name and the empty file’s name, then the
#  cache is considered corresponding to the source file.
#


declare -g  \
CACHED_ATTS_DIR="$CACHEDIR/cached_attachments"

declare -g  \
CACHED_ATTS_LIFESPAN_MONTHS="3"

 # Nadeshiko stores cache version to $CACHED_ATTS_DIR/cache_version (which
#  holds the version of nadeshiko.sh, that created it). If the cache version
#  in the file would be lower than this version, or the file wouldn’t exist,
#  the cache is considered invalid and is automatically wiped.
#
declare -gr  \
CACHED_ATTS_CACHE_MINVER='2.30.0'

 # This variable is set, if attachment extraction (which is mostly fonts)
#  has occurred during this run.
#
declare -g ATTS_CACHE_POPULATED



copy_attachments_from_cache() {
	local  backup_dir="$CACHED_ATTS_DIR/${src[atts_cache_id]}"

	[ -d "$TMPDIR/external" ]  \
		|| err "$TMPDIR/external doesn’t exist. Nowhere to place the files."

	while IFS= read -r -d ''; do
		ln -s "$REPLY"  "$TMPDIR/external/"
	done < <(
		find -L "$backup_dir"  \
		        \( -type f -o -xtype l \)  \
		        -size +0  \
		        -printf "%p\0"
	)
	return 0
}


backup_attachments_to_cache() {
	local  backup_dir="$CACHED_ATTS_DIR/${src[atts_cache_id]}"

	mkdir -p "$backup_dir"  \
		|| err "Couldn’t create directory to back up attachments."

	while IFS= read -r -d ''; do
		cp -ra "$REPLY" "$backup_dir/"
	done < <(
		find -L "$TMPDIR/external/"   \
		        \( -type f -o -xtype l \)  \
		        -size +0  \
		        -printf "%p\0"
	)

	touch "$backup_dir/${src[path]##*/}"

	return 0
}


find_cached_attachments() {
	local  backup_dir="$CACHED_ATTS_DIR/${src[atts_cache_id]}"

	if [ -d "$backup_dir" ]  \
		&& [ -e "$backup_dir/${src[path]##*/}" ]
	then
		return 0
	else
		return 1
	fi

	return 0
}


run_maintenance_for_the_attachments_cache() {
	[ -d "$CACHED_ATTS_DIR" ] || return 0

	info 'Running maintenance on the attachments cache.'
	milinc

	info "Removing files older than $CACHED_ATTS_LIFESPAN_MONTHS months…"
	find "$CACHED_ATTS_DIR"  \
	     -mindepth 1  \
	     -maxdepth 1  \
	     -type d  \
	     -mtime $((30*CACHED_ATTS_LIFESPAN_MONTHS))  \
	     -exec rm -rf {} \;  \
		|| warn "An error happened during removal of old attachments"

	#  Deduplicate existing files
	#
	#  Fonts are extracted from every file, but when multiple files would have
	#  the same (or a very similar) font set of say, 40 fonts (and there are
	#  files with over 60 fonts in a container), then an ordinary show having
	#  24 episodes would produce 15–20 copies. That’s quite a lot.
	#
	[ -v ATTS_CACHE_POPULATED ] && {
		info "The attachments cache was populated during this run, now going
		      to deduplicate files in it."
		if which rdfind &>/dev/null; then
			msg "Running rdfind…"
			rdfind  -makeresultsfile false  \
			        -ignoreempty true  \
			        -makehardlinks true  \
			        "$CACHED_ATTS_DIR"  \
			        > /dev/null
		else
			warn "If “rdfind” was installed, Nadeshiko could possibly save
			      hundreds of megabytes."
		fi
	}

	mildec
	return 0

}


create_attachments_cache_directory() {
	if [ -d "$CACHED_ATTS_DIR" ]; then
		[ -r "$CACHED_ATTS_DIR"  -a  -w "$CACHED_ATTS_DIR" ]  \
			|| err "Directory for cached attachments is inaccessible.
			        Cannot read or write to “$CACHED_ATTS_DIR”."
	else
		mkdir -p "$CACHED_ATTS_DIR" &>/dev/null  \
			|| err "Couldn’t create directory to backup attachments to.
			        mkdir -p “$CACHED_ATTS_DIR” failed."
	fi
	return 0
}


validate_attachments_cache() {
	local  cache_version=0

	[ -e "$CACHED_ATTS_DIR/cache_validated_by" ] && {
		cache_version=$(< "$CACHED_ATTS_DIR/cache_validated_by")
		[[ "$cache_version" =~ ^($INT)(\.$INT|\.$INT\.$INT)$ ]]  || {
			warn "Cache version “$cache_version” is an invalid version number.
			      Assuming version 0."
			cache_version=0
		}
	}

	compare_versions "$cache_version" '<' "$CACHED_ATTS_CACHE_MINVER" && {
		warn "Cache version “$cache_version” is considered deprecated.
		      Wiping cache."

		find "$CACHED_ATTS_DIR"  \
		     -mindepth 1  \
		     -maxdepth 1  \
		     -type d  \
		     -exec rm -rf {} \;  \
		     || true  # ignore empty cache
	}

	echo "$version" >"$CACHED_ATTS_DIR/cache_validated_by"

	return 0
}


check_attachments_cache() {
	create_attachments_cache_directory
	validate_attachments_cache

	return 0
}



return 0
