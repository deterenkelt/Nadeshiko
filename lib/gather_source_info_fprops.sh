#  Should be sourced.

#  gather_source_info_fprops.sh
#  A submodule of gather_source_info.sh that holds functions to retrieve
#  file per se properties.
#  Author: deterenkelt, 2018–2021
#
#  For licence see nadeshiko.sh
#


                      #  Info about file per se  #

gather_fprop_mediainfo() {
	source[mediainfo]=$(mediainfo --Full --Output=XML "${source[path]}")
	[ "${source[mediainfo]}" ]  \
		|| err "Couldn’t retrieve mediainfo output for the source file."
	return 0
}


gather_fprop_size_B() {
	source[size_B]=$(stat --format %s "${source[path]}")
	[[ "${source[size_B]}" =~ ^$INT$ ]]  \
		|| unset source[size_B]
	return 0
}


gather_fprop_size_kB() {
	source[size_kB]=$(( source[size_B]/1024 ))
	return 0
}


gather_fprop_size_MiB() {
	source[size_MiB]=$(( source[size_B]/1024/1024 ))
	return 0
}


gather_fprop_atts_cache_id() {
	#
	#  Compose cache id from inode and device numbers, uniquely identifying
	#  the file on the disk(s). Lastmod *can* match between two episode files.
	#
	source[atts_cache_id]=$(stat --format=%i-%d "${source[path]}")
	[[ "${source[atts_cache_id]}" =~ ^$INT-$INT$ ]]  \
		|| err "Couldn’t get source video last modification time."
	return 0
}


return 0
