#  Should be sourced.

#  gather_source_info_vprops.sh
#  A submodule of gather_source_info.sh that holds functions to retrieve
#  video stream properties.
#  Author: deterenkelt, 2018–2023
#
#  For licence see nadeshiko.sh
#


                        #  Video track info  #

gather_vprop_ffprobe() {
	source_v[ffprobe]=$(
		$ffprobe -hide_banner -v error  \
		         -select_streams V  \
		         -show_entries stream  \
		         -of default=noprint_wrappers=1  \
		         "${source[path]}"  \
		    | grep -vE '=N/A\s*$'
	)
	[ "${source_v[ffprobe]}" ]  \
		|| warn "Couldn’t retrieve ffprobe for the video stream."
	return 0
}


 # Determining the typeorder string for the video stream and setting as a
#  contaienr property. Unlike for (built-in) subtitles and audio tracks,
#  [track_id] for the video stream is unnecessary, since ffmpeg can be spe-
#  cified the type as ‘V’ (and not ‘v’), to use the *actual* video track (and
#  not some jpeg cover). These JPEG streams also have resolution, bitrate(!)
#  and framerate fields, which makes gathering data problematic.
#
#  This function thus probes video streams, checks their frame rate and
#  remembers the [@typeorder] attribute of the track with the highest bitrate.
#  source_v[track_typeorder] will then hold this attribute and will be used
#  to query all of the video related parameters.
#
gather_vprop_track_id_and_typeorder() {
	local  bitrates=()
	local  highest_bitrate=0
	local  i

	#  Mediainfo will always grab the Video streams count, unless the
	#  file is broken and the content is not readable.
	[[ "${source_c[video_streams_count]}" =~ ^$INT$ ]]  \
		|| err "Cannot get video streams count in the source."

	(( source_c[video_streams_count] == 0 ))  \
		&& err "The source file has no detectable video streams."

	(( source_c[video_streams_count] == 1 )) && {
		#  Technically, not needed, for ffmpeg has ‘V’ stream type.
		source_v[track_id]=0
		# source_v[track_typeorder] remains unset
		return 0
	}

	#  When there’s two or more streams
	#  Setting the default to the first stream. (Again, technically not
	#  necessary, but maybe it’ll be used in the future.)
	source_v[track_id]=0
	for ((i=0; i<source_c[video_streams_count]; i++)); do
		bitrates[i]=$(
			${xml[@]} "//x:track[@type=\"General\"][@typeorder=\"$((i+1))\"]/x:BitRate"  \
				<<<"${source[mediainfo]}" 2>/dev/null
		) || true
		#  In case the cover stream didn’t have bitrate set.
		[[ "${bitrates[i]}" =~ ^[0-9]+$ ]]  || bitrates[i]=0

		#  Remembering the highest value
		(( ${bitrates[i]} > highest_bitrate )) && {
			highest_bitrate=${bitrates[i]}
			source_v[track_id]=$i
			source_v[track_typeorder]=$(( i+1 ))
		}
	done

	return 0
}


gather_vprop_duration_total_s_ms() {
	#  The format in this field differs, so assuming, that it may be
	#  “N”, “N.NNN” or “N.NNNNNNNNN”. We need to guarantee, that it conforms
	#  to either first or the second format, and strip extra ms precision:
	#  6400.360xxxxxx → 6400.360
	#
	if [[ "${source_v[duration_total_s_ms]}" =~ ^([0-9]+)((\.[0-9]{1,3})[0-9]*|)$ ]]; then
		source_v[duration_total_s_ms]="${BASH_REMATCH[1]}${BASH_REMATCH[3]}"

	elif [[ "${source_c[duration_total_s_ms]}" =~ ^([0-9]+)((\.[0-9]{1,3})[0-9]*|)$ ]]; then
		#  If duration was in the metadata for container itself, use it.
		source_v[duration_total_s_ms]="${BASH_REMATCH[1]}${BASH_REMATCH[3]}"

	else
		warn 'Couldn’t determine video duration. Assuming 999999.000 s.'
		# unset source_v[duration_total_s_ms]
		#  This is a workaround for .m2ts files
		source_v[duration_total_s_ms]='999999.000'
	fi

	return 0
}


gather_vprop_bitrate() {
	[[ "${source_v[bitrate]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t determine video bitrate."
		unset source_v[bitrate]
	}
	return 0
}


gather_vprop_width() {
	[[ "${source_v[width]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t retrieve video stream width."
		unset source_v[width]
	}
	return 0
}


gather_vprop_height() {
	[[ "${source_v[height]}" =~ ^$INT$ ]] || {
		warn "Couldn’t retrieve video stream height."
		unset source_v[height]
	}
	return 0
}


 # Resolution is most commonly used to check, if the width and height
#  are set (that is, to check the existence of one variable instead of two).
#  Its actual value is probably never used.
#
gather_vprop_resolution() {
	[ -z "${source_v[height]}"  -o  -z "${source_v[width]}" ] && {
		warn "Couldn’t determine video stream resolution."
		return 0
	}

	source_v[resolution]="${source_v[width]}×${source_v[height]}"
	return 0
}


gather_vprop_resolution_total_px() {
	[ -z "${source_v[height]}"  -o  -z "${source_v[width]}" ] && {
		warn "Couldn’t determine video stream resolution (total numebr of pixels)."
		return 0
	}

	source_v[resolution_total_px]=$(( source_v[width] * source_v[height] ))
	return 0
}


gather_vprop_pixel_aspect_ratio() {
	#  Convert FFprobe ratio, e.g. 1:1 or 16:9 to a number in decimal notation
	[[ "${source_v[pixel_aspect_ratio]}" =~ ^$INT:$INT$ ]]  && {
		source_v[pixel_aspect_ratio]=$(
			echo "scale=6; ${source_v[pixel_aspect_ratio]/\:/\/}" | bc
		)
	}
	[[ "${source_v[pixel_aspect_ratio]}" =~ ^$FLOAT$ ]]  || {
		warn "Couldn’t determine pixel aspect ratio."
		unset source_v[pixel_aspect_ratio]
	}
	return 0
}


gather_vprop_display_aspect_ratio() {
	local  width
	local  height
	local  pixel_ar
	#  Convert FFprobe ratio, e.g. 1:1 or 16:9 to a number in decimal notation
	[[ "${source_v[display_aspect_ratio]}" =~ ^$INT:$INT$ ]]  && {
		source_v[display_aspect_ratio]=$(
			echo "scale=5; ${source_v[display_aspect_ratio]/\:/\/}" | bc
		)
	}
	[[ "${source_v[display_aspect_ratio]}" =~ ^$FLOAT$ ]]  || {
		if [ -v source_v[pixel_aspect_ratio] ]  \
			&& [ -v source_v[width] ]  \
			&& [ -v source_v[height] ]
		then
			width=${source_v[width]}
			height=${source_v[height]}
			pixel_ar=${source_v[pixel_aspect_ratio]}
			source_v[display_aspect_ratio]=$(
				echo "scale=5; $width / $height * $pixel_ar" | bc
			)
		fi
	}

	[[ "${source_v[display_aspect_ratio]}" =~ ^$FLOAT$ ]]  || {
		warn "Couldn’t determine display aspect ratio."
		unset source_v[display_aspect_ratio]
	}
	return 0
}


gather_vprop_is_16to9() {
	if [ -v source_v[display_aspect_ratio] ]; then
		#
		#  When you check DAR in the file by hand, a mistake may be costly.
		#  If you mistype an unusual video resolution, e.g. 848×480 instead of
		#  848×360 (cinemascope 2.35:1), this may lead to a false ratio of 1.76,
		#  that looks close to 1.77, which leads to a wrong assumption, that
		#  the video should have a 16:9 aspect ratio (that it doesn’t really
		#  have). Double check the video resolution of your test samples. The
		#  16:9 ratio remains 1.77 for all resolutions from 360p up to 2160p.
		#
		#  Comparing with 1.77, because mediainfo reports the number with
		#  three digits after the dot, and the third is rounded.
		#
		[[ "${source_v[display_aspect_ratio]}" =~ ^1\.77 ]]  \
			&& source_v[is_16to9]='yes'  \
			|| source_v[is_16to9]='no'
	fi
	return 0
}


gather_vprop_frame_rate() {
	#
	#  Converting ffprobe fraction (“2997/100”, “500/21”, “24/1”
	#  and the like) to a decimal number
	#
	[[ "${source_v[frame_rate]}" =~ ^($INT)/($INT)$ ]] && {
		source_v[frame_rate]=$(
			echo "scale=3; ${source_v[frame_rate]}" | bc
		)
	}
	[[ "${source_v[frame_rate]}" =~ ^$FLOAT$ ]]  || {
		warn 'Couldn’t retrieve the source video frame rate.'
		unset source_v[frame_rate]
	}
	return 0
}


gather_vprop_frame_rate_mode() {
	[[ "${source_v[frame_rate_mode]}" =~ ^(CFR|VFR)$ ]]  \
		|| unset source_v[frame_rate_mode]

	return 0
}


gather_vprop_frame_rate_max() {
	if [[ "${source_v[frame_rate_max]}" =~ ^([0-9]|[1-9][0-9]+)(\.([0-9]{1,3})[0-9]*|)$ ]]; then
		#
		#  Making sure that the value has maximum 3 digits after the dot.
		#
		[ "${BASH_REMATCH[3]}" ]  \
			&& source_v[frame_rate_max]=${BASH_REMATCH[1]}.${BASH_REMATCH[3]}

	elif [[ "${source_v[frame_rate_max]}" =~ ^($INT)/($INT)$ ]]; then
		#
		#  Converting ffprobe fraction (“2997/100”, “500/21”, “24/1”
		#  and the like) to a decimal number
		#
		source_v[frame_rate_max]=$(
			echo "scale=3; ${source_v[frame_rate_max]}" | bc
		)

	else
		unset source_v[frame_rate_max]
	fi

	return 0
}

 # A metric used at one time in the past. It needs to be tuned for the
#  resolution at hand, hence requires more testing.
#
gather_vprop_frame-heaviness() {
	[ -v source_v[frame_count]  ] || return 0
	[ -v source_v[resolution_total_px]  ] || return 0

	source_v[frame-heaviness]=$((    source_v[frame_count]
	                               * source_v[resolution_total_px]  ))
	# source_v[frame-heaviness]=$((    source_v[frame_count]
	#                                * (
	#                                      source_v[resolution_total_px]
	#                                    * source_v[muxovhead_coeff]
	#                                  )
	#                                * (
	#                                      source_v[bitrate]
	#                                    * source_v[muxovhead_coeff]
	#                                  )
	#                            ))

	return 0
}


gather_vprop_stream_size_by_extraction_B() {
	local raw_stream="$TMPDIR/vid.raw"
	rm -f "$raw_stream"
	#  Assuming, that webms that we test all have video track GOING FIRST,
	#  i.e. video stream id is 0 (Mkvextract operates on the whole bunch
	#  of streams without subdivision by kind), audio track has id 1.
	mkvextract "${source[path]}" tracks --fullraw "0:$raw_stream"  &>/dev/null
	source_v[stream_size_by_extraction_B]=$( stat -c %s "$raw_stream" )
	[[ "${source_v[stream_size_by_extraction_B]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t calculate video stream size by extraction."
		unset source_v[stream_size_by_extraction_B]
	}
	return 0
}


gather_vprop_bitrate_by_extraction() {
	[ -v source_v[stream_size_by_extraction_B] ] || return 0
	[ -v source_c[duration_total_s_ms] ] || return 0

	source_v[bitrate_by_extraction]=$(
		echo "scale=2;  br =     ${source_v[stream_size_by_extraction_B]}  \
		                       * 8  \
		                       / ${source_c[duration_total_s_ms]};  \
		      scale=0;  br/1" | bc
	)
	[[ "${source_v[bitrate_by_extraction]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t calculate (average) video bitrate by extraction."
		unset source_v[bitrate_by_extraction]
	}
	return 0
}


gather_vprop_chroma_subsampling() {
	[[ "${source_v[chroma_subsampling]}" =~ ^[43]:[421]:[4210]$ ]]  || {
		warn "Couldn’t determine chroma subsampling."
		unset source_v[chroma_subsampling]
	}

	return 0
}


gather_vprop_cspace_matrix_coefs() {
	if ! [[ "${source_v[cspace_matrix_coefs]}"  \
		    =~ ^($(IFS='|'; echo "${known_cspace_matrix_coefs[*]}"))$ ]]
	then
		warn "Couldn’t determine the matrix coefficients."
		unset source_v[cspace_matrix_coefs]
	fi
	return 0
}


gather_vprop_cspace_primaries() {
	if ! [[ "${source_v[cspace_primaries]}"  \
		    =~ ^($(IFS='|'; echo "${known_cspace_primaries[*]}"))$ ]]
	then
		warn "Couldn’t determine the primaries."
		unset source_v[cspace_primaries]
	fi
	return 0
}


gather_vprop_cspace_tone_curve() {
	if ! [[ "${source_v[cspace_tone_curve]}"  \
		    =~ ^($(IFS='|'; echo "${known_cspace_tone_curves[*]}"))$ ]]
	then
		warn "Couldn’t determine the tone curve."
		unset source_v[cspace_tone_curve]
	fi
	return 0
}


gather_vprop_cspace_colour_range() {
	if ! [[ "${source_v[cspace_colour_range]}"  \
		    =~ ^($(IFS='|'; echo "${known_cspace_colour_ranges[*]}"))$ ]]
	then
		warn "Couldn’t determine the colour range (full or limited).
		      Will rely on the input."
		source_v[cspace_colour_range]='input'
	fi
	return 0
}


gather_vprop_cspace_valid() {
	#  The colour range (full or limited) is not checked, because
	#    - there is many software misusing it;
	#    - most of the distributed content is in limited range,
	#      even BDs, so the players and the encoding software
	#      probably assume it’s as the default and can’t miss.
	#
	if [ -v source_v[cspace_matrix_coefs] ]  \
		&& [ -v source_v[cspace_primaries] ]  \
		&& [ -v source_v[cspace_tone_curve] ]
	then
		source_v[cspace_valid]=t
	fi

	return 0
}


gather_vprop_scan_type() {
	[[ "${source_v[scan_type]}" =~ ^(Interlaced|Progressive)$ ]]  \
		|| unset source_v[scan_type]

	return 0
}


gather_vprop_scan_order() {
	[[ "${source_v[scan_order]}" =~ ^(TFF|BFF)$ ]]  \
		|| unset source_v[scan_order]

	return 0
}


return 0
