#  Should be sourced.

#  gather_source_info.sh
#  A set of functions to get information with mediainfo and ffprobe.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


vchan setup  name=Gather_info  \
             level=3  \
             meta=M  \
             group_item=gather_source_info  \
             min_level=2  \
             max_level=4

show_help_on_verbosity_channel_Gather_info() {
	cat <<-EOF
	Gather_info (self-report channel)
	    Controls messages related to the retrieval of metadata from the source
	    file(s) with the help of mediainfo, ffprobe and other tools.
	Levels:
	    2 – warnings and errors;
	    3 – informational, warnings and errors (the default).
	        Marks entering the module and quitting, when the work is done.
	        Basically two lines in the output sans possible warnings.
	    4 – verbose messages. All of the above plus detailed information
	        on the retrieval of each property.
	EOF
	return 0
}


              #  Retrieving information from the source file  #

 # Notes about mediainfo and ffprobe backends
#
#  Both have to be used, but most of the time mediainfo does the job, and
#  ffprobe is used as a backup. To be precise, the container, video and audio
#  metadata are considered to return values in the same or a similar format,
#  so in the code there are minimal conversion, if there are any. For sub-
#  titles the two programs differ in how they name and separate one type from
#  the other, mediainfo additionally mixes up some types, so ffprobe is used
#  there instead. Finally, whichever program would be used for retrieval, it
#  will have its own nuances, so you should expect, that their usage – in the
#  code below – is unified only as much as possible. Keep that in mind.
#
#  Mediainfo output is grabbed *once* for all source metadata, that is, all
#  video, audio, subtitle etc. metadata are grabbed in one go, and then the
#  properties are retrieved for each used stream from *that* output. The excep-
#  tion is only for external audio tracks and external subtitles: for each
#  physical file there has to be a separate mediainfo output, of course.
#
#  FFprobe is meant to be used differently. It’s simple to call it for a par-
#  ticular property in, say a particular subtitle stream, – but that’s as long
#  as you call ffprobe each time you need to retrieve a property. There are op-
#  tions to grab all the metadata for a particular file, but in those metadata
#  there are no keys, by which one could address a particular property in
#  a particular stream later. If you specify stream as “s:2” for ffmpeg, that
#  means “the second subtitle stream among subtitles”, however, the metadata
#  do not have such an internal index, only the global one, which counts
#  streams in the container. So with ffprobe the output has to be grabbed
#  separately for the container metadata and for each stream of interest.
#  Properties then are retrieved from the specific ffprobe output, that was
#  grabbed for the particular stream (or for the container).
#

 # Grabs information about a video file from mediainfo and other means.
#  $1 – mode (defined preset of properties to be retrieved),
#       one of: “before-encode”, “container-ratios”
#  $2 – name of the variable, which the data will be assigned to. The variable
#       must already exist, be an associative array and have element [path]
#       set. Some other properies may be set beforehand, such as those, which
#       hold the subtitle/audio tracks selected by user.
#
#       The variable name will be used as a generic name to create an accom-
#       panying set of  NAME_v, NAME_a, NAME_s, NAME_c  arrays to hold video,
#       audio, subtitle and container properties separately. The original
#       array will be preserved and further expanded with properties of a file
#       as a disk file.
#
#
#  Identifying selected tracks
#
#  Nadeshiko uses two groups of variables, that carry the selected track
#  number (among its kind), depending on the purpose for which it is used:
#
#    - The “track_id” property holds the track number, which will be used
#      by ffmpeg. The track number from the command line options “subs:N”
#      and “audio:M” translates to this property, when used. When no track
#      number is excplicitly requested, “track_id” is set to whichever track
#      is marked as the default in the container. “track_id” uses FFmpeg
#      numbering, hence is 0-based.
#
#    - The “track_typeorder” property also holds the track number, which is
#      determined after the “track_id” value, but this property serves only
#      to aid the retrieval with mediainfo and doesn’t figure anywhere out-
#      side and doesn’t affect anything else. There are several reasons, why
#      there has to be a separate variable, in short it comes down to
#        1. “track_id” is zero-based, and “track_typeorder” is 1-based.
#        2. “track_typeorder” may be unset, when “track_id” is present.
#             This depends on the number of tracks in the file and the
#             Nadeshiko’s variable simply mirrors the way it is done in
#             mediainfo’s output xml.
#        3. As there’s a conditionality involved, we can’t simply take an
#             incremented “track_id” in mediainfo-related code, – to deal
#             with said conditionality would require require running
#             an equivalent of the gather_aprop_track_id_and_typeorder()
#             every time a property must be read.
#
#  As it was said before, mediainfo in its output counts tracks from 1. How-
#  ever, it uses numbering only in cases, when there is more than one track of
#  this type. Without knowing the number of the track by order (the “typeorder”
#  attribute), it is not possible to point xmlstarlet at the proper entity in
#  the XML and extract the metadata. Basically, we have ffmpeg track id in
#  “track_id”, but on the stage of metadata retrieval, where we have to use
#  mediainfo, we have to use an adapted version of “track_id”. It is conveni-
#  ent to determine it once and store it, and this is what “track_typeorder”
#  basically is for.
#
#  Rules of generic property retrieval are explained in the description
#  to “gather_source_prop()”.
#
gather_source_info() {
	local mode="$1"       #  Sic! Before the declaration of globals!
	local varname="$2"    #

	declare -gA  $varname         #  File per se properties
	declare -gA  ${varname}_c     #  Container properties
	declare -gA  ${varname}_v     #  Video track properties
	declare -gA  ${varname}_a     #  Audio. . . . . .
	declare -gA  ${varname}_s     #  Subtitle. . . . . .

	#  Creating direct links to the destination arrays (the global ones),
	#  that should hold the properties.
	#
	local -n source="$varname"
	local -n source_c="${varname}_c"
	local -n source_v="${varname}_v"
	local -n source_a="${varname}_a"
	local -n source_s="${varname}_s"

	#  Usually the empty declaration isn’t needed, but later we’re going to
	#  count the elements in each array. So, in order to not get an error
	#  about the unset “$source_s[*]”, we need either to either
	#    - bloat with conditions our math
	#    - make sure the arrays are defined /and set/, at least as empty ones,
	#      beforehand (and they actually are – in Nadeshiko, but there are also
	#      test scripts)
	#    - or check their existence ourselves.
	#  The first array, “$source[*]”, was globally set beforehand, it cannot be
	#  empty, or we wouldn’t be here. “$source_a[*]” and “$source_s[*]” may be
	#  set, because of the “track_id” specified on the command line.
	#
	[ "${!source_c[*]}" ] || source_c=()
	[ "${!source_v[*]}" ] || source_v=()
	[ "${!source_a[*]}" ] || source_a=()
	[ "${!source_s[*]}" ] || source_s=()

	#  “$gsi_collected_for_$varname” holds the list of properties that were
	#  collected once. Keeping the list per-variable allows to use “gather_
	#  source_info()” multiple times within the same run, to e.g. populate
	#  the global space with data collected for the source file and for the
	#  encoded file. The nameref variable “gsi_collected” is used as a short
	#  name for the long one.
	#
	#  The multiple runs part concerns more the testing suite, than Nadeshiko
	#  scripts, and using per-name arrays should fix the problem of consecutive
	#  runs, but to be 120% sure, the variable is better to be unset. That’s
	#  in case somebody would attempt to reuse the array name itself, which
	#  is passed to “gather_source_info()”.
	#
	unset  gsi_collected_for_$varname
	#
	declare -gA  gsi_collected_for_$varname
	declare -gn  gsi_collected="gsi_collected_for_$varname"

	local -a  props_to_gather

	local  prop
	local  props_found

	 # Using the new mediainfo format “XML” seems to be the lesser evil, for
	#  it reports the data in the needed format, but it requires expressions
	#  to use namespace. (Can be parsed out with sed, but eh.)
	#
	local xml=(xmlstarlet  sel -N x="https://mediaarea.net/mediainfo" -t -v)

	info "Gathering metadata"
	milinc
	#
	#  Modes are named sets of properties. Programs of Nadeshiko suite request
	#  the set by name as the first argument to this function. Thus a conveni-
	#  ent slice of information is retrieved. Properties may be specified in
	#  any order, but the lists below attempt to load those, that depend on
	#  others, last.
	#
	case "$mode" in
		before-encode)
			props_to_gather=(
				#  File properties, i.e. properties of a file on the disk.
				#  Note, that this type of properties goes right into the
				#  $varname array (the main array), there’s no ${varname}_f.
				#
				#'path'  #  Must be set beforehand.
				'f:mediainfo'
				'f:atts_cache_id'

				#  Container metadata
				'c:ffprobe'
				'c:video_streams_count'
				'c:format'
				'c:duration_total_s_ms'
				'c:title'
				'c:attachments'
				'c:attachments_count'

				#  Video
				'v:track_id_and_typeorder'
				'v:ffprobe'
				'v:format'
				'v:duration_total_s_ms'
				'v:bitrate'
				'v:width'
				'v:height'
				'v:resolution'
				'v:resolution_total_px'
				'v:pixel_aspect_ratio'
				'v:display_aspect_ratio'
				'v:is_16to9'
				'v:frame_rate'
				'v:frame_rate_mode'
				'v:frame_rate_max'
				'v:frame_count'
				'v:chroma_subsampling'
				#  Colour space metadata are always gathered, because
				#  safe tagging of the output is on by default.
				'v:cspace_matrix_coefs'
				'v:cspace_primaries'
				'v:cspace_tone_curve'
				'v:cspace_colour_range'
				'v:cspace_valid'
				'v:scan_type'
				'v:scan_order'
				'v:encoded_library_name'
				'v:encoded_library_version'
				'v:encoded_library_settings'
			)
			[ -v audio ] && {
				props_to_gather+=(
					# 'external_file'   #  May be set by the “audio=”
					                    #  command line option.
					'c:audio_streams_count'
					'a:external_file_mediainfo'
					'a:track_id_and_typeorder'
					'a:ffprobe'
					'a:format'
					'a:format_profile'
					'a:is_lossless'
					'a:bitrate'
					'a:channels'
				)
			}
			[ -v subs ] && {
				props_to_gather+=(
					# 'external_file'  #  May be set by the “subs=”
					                   #  command line option.
					# 's:external_file_mediainfo'  #  mediainfo is not used
					                               #  for subtitles. See below.
					'c:subtitle_streams_count'
					's:track_id_and_typeorder'
					's:ffprobe'
					's:codec'
					's:width'
					's:height'
				)
			}
			;;

		test-suite)
			props_to_gather=(
				'f:mediainfo'
				'f:size_B'

				#  Container metadata
				'c:ffprobe'
				'c:video_streams_count'
				'c:format'
				'c:duration_total_s_ms'

				#  Video
				'v:track_id_and_typeorder'
				'v:ffprobe'
				'v:format'
				'v:duration_total_s_ms'
				'v:bitrate'
				'v:width'
				'v:height'
				'v:resolution'
				'v:resolution_total_px'
				'v:pixel_aspect_ratio'
				'v:display_aspect_ratio'
				'v:is_16to9'
				'v:frame_rate'
				'v:frame_rate_mode'
				'v:frame_rate_max'
				'v:frame_count'
				'v:chroma_subsampling'
				'v:cspace_matrix_coefs'
				'v:cspace_primaries'
				'v:cspace_tone_curve'
				'v:cspace_colour_range'
				'v:cspace_valid'
				'v:encoded_library_name'
				'v:encoded_library_version'
				'v:encoded_library_settings'

				'c:audio_streams_count'
				'a:track_id_and_typeorder'
				'a:ffprobe'
				'a:format'
				'a:format_profile'
				'a:is_lossless'
				'a:bitrate'
				'a:channels'

				'c:subtitle_streams_count'
				's:track_id_and_typeorder'
				's:ffprobe'
				's:codec'
			)
			;;

		container-ratios)
			#
			#  This preset was created as a part of the test suite. Basically,
			#  to get statistics from the files encoded locally with Nadeshiko
			#  (primary concern being the amount of container overhead relative
			#  to the resulting file size). Expect, that many of the processing
			#  functions, which are used here and _not_ used in the “before-
			#  encode” set, assume that the source has only 1 video track and
			#  1 audio track – as Nadeshiko encodes files.
			#
			props_to_gather=(
				# 'path'  #  Must be set beforehand.
				'f:mediainfo'
				'f:size_B'
				'f:size_kB'
				'f:size_MiB'

				#  Container metadata
				'c:format'
				'c:duration_total_s_ms'
				'c:muxing_overhead_B'
				'c:ovh_ratio_to_fsize_pct'
				'c:ovh_ratio_to_1sec_of_playback'
				'c:bitrate_by_extraction'

				#  Video
				'v:format'
				'v:duration_total_s_ms'
				'v:stream_size_by_extraction_B'
				'v:bitrate_by_extraction'
				'v:width'
				'v:height'
				'v:resolution'
				'v:resolution_total_px'
				'v:display_aspect_ratio'
				'v:is_16to9'
				'v:frame_count'
				'v:frame-heaviness'
				'v:pix_fmt'

				#  Audio
				'a:format'
				'a:stream_size_by_extraction_B'
				'a:bitrate_by_extraction'
			)
			;;
	esac

	for prop in "${props_to_gather[@]}"; do
		gather_source_prop "$prop"
	done

	#  Generic mediainfo, ffprobe for V/A/S (including external ones),
	#  1 extra mediainfo for external audio, if present.
	props_found=$((   ${#source[*]}
	                + ${#source_c[*]}
	                + ${#source_v[*]}
	                + ${#source_a[*]}
	                + ${#source_s[*]}
	                - 4
	                - 0${source_a[external_file]:+1}
	                ))
	info "$props_found of the ${#props_to_gather[*]} properties were found."
	mildec
	return 0
}


 # A table that contains information on how to retrieve properties.
#  The format for the array item is as follow:
#      [category:property_name]="key1=value1,key2=value2,…keyN=valueN"
#
#  Category determines, where does the property belong to:
#    - ‘f’ – file property, for properties of the file as a disk file.
#            Will be put into “source” array.
#    - ‘c’ – container property, i.e. that which is grabbed from the
#            container metadata. Goes to “source_c”.
#    - ‘v’ – video stream property, the destination is “source_v”.
#    - ‘a’ – audio stream property, will be put in “source_a”
#    - ‘s’ – subtitle track property, which goes to “source_s”
#
#  Propety name is the Nadeshiko internal name for the property. It’s the name
#  by which it’s queried in gather_source_info() and by which it is put into
#  the source* arrays.
#
#  Now for the “rules”. It’s a string that may contain three types of keywords,
#  namely “mediainfo”, “ffprobe” and “depends”. Each keyword must be assigned
#  a value, so they come in pairs keyword=value. There may be multiple pairs
#  speicified, in which case they are separated with a comma (,) sign. Spaces,
#  tabs and newlines are allowed to make proper indentation for the lines, that
#  compose the string. These spacing characters are stripped from the string,
#  when it’s parsed.
#
#  The “mediainfo” keyword specfies, that the property value should be retrie-
#  ved with mediainfo. The value has format “section_name/property_name”. The
#  section name is one of: General, Video, Audio, Text. In the output of “me-
#  diainfo --Full --Output=XML” section refers to
#     <track type="Section"> … </track>
#  and the property name is the clause of tags within <track>, that holds the
#  value, e.g.
#     <track type="Section">
#         …
#         <PropertyName>VALUE</PropertyName>
#         …
#     </track>
#  Rarely, some properties are put into subsections, so e.g. for attachments
#  it is like
#     <track type="General">
#         …
#         <extra>
#             <Attachments>LIST / OF / ATTACHMENTS</Attachments>
#             …
#         </extra>
#         …
#     </track>
#  So there must be specified a section (here it’s “General”) and a subsection
#  (“extra”). The description line for such properties will have them both
#  via slash:
#      mediainfo=General/extra/Attachments
#
#  The “ffprobe” keyword specifies, that the property value should be retrie-
#  ved with ffprobe. The value has a format “stream_type/property_name”. The
#  stream type is one of ‘c’, ‘v’, ‘a’, ‘s’. Note, that the stream type speci-
#  fied here is *not* the exact thing passed to ffprobe as the stream type.
#  The ‘c’ type refers to the “FORMAT” section in the ffprobe output. The ‘v’
#  type is what ‘V’ presented for the video stream. In the table of dependen-
#  cies the categories are referred to in the common Nadeshiko notation. (Thus,
#  everything is simple and as usual on this level, but expect, that in the
#  internals there are some specifics.) Finally, the key names are those print-
#  ed by ffprobe’s default output format, e.g.
#      time_base=1/1000
#      bit_rate=N/A
#      DISPOSITION:default=0
#      TAG:language=eng
#  A sample of that may be retrieved with
#      $ ffprobe -show_format
#  (for the container metadata) or with
#      $ ffprobe -show_streams -select_entries stream
#  for a stream.
#
#  Both “mediainfo” and “ffprobe” keywords may be used within one dependency
#  rule and may be called each several times with different values. “gather
#  _source_prop()” will try all retrievers in the order they are specified in,
#  until one of them sets the property to a non-empty value, this is consider-
#  ed as a successful retrieval.
#
#  Finally, the “depends” keyword points at what properties should be retrie-
#  ved first, before this one. The format is the same as for the array index:
#  “category:property_name” (i.e. in the Nadeshiko notation). Note, that a
#  dependency isn’t a hard requirement here: resolving a dependency only at-
#  tempts to make certain variables available (if they are retrievable at all).
#  Not all dependencies may actually be required. For those that are, the cus-
#  tom processing function (a function that is defined per property and runs
#  after the retrieval, if present) does a check for what’s present at hand
#  and whether it does suffice or not.
#
#  Do not confuse the syntax! Here’s a small table for comparison
#
#      Nadeshiko                Retrieving it with
#      property name            mediainfo             ffprobe
#      ----------------------------------------------------------------------
#      v:format                 Video/Format          V:format (or v:format)
#      c:video_streams_count    General/VideoCount    — ¹
#      a:codec                  Audio/CodecID         a:codec_name
#      s:codec                  <not used>²           s:codec_name
#
#  ¹ ffprobe indeed can report the container metadata,
#      but it isn’t used for this purpose.
#  ² mediainfo is not reliable in this case, see details
#      in gather_…_sprops.sh.
#
#  For this array, order of items is not important.
#
#  In general, mediainfo is preferred over ffprobe. The reason is that most
#  of the needed properties can be retrieved with mediainfo from get go, while
#  ffprobe might need to go into probing first. For example, in case with RLE
#  subtitles, that are built-in in the frame data (i.e. into the frames of the
#  video stream, rather then existing as a stream of their own withing the con-
#  tainer), mediainfo would report presence after the ordinary check, while
#  ffprobe may need to employ a non-trivial, longer probe to detect subtitles
#  in the video stream. For this reason it’s faster to retrieve the number of
#  tracks with mediainfo and then run ffprobe to determine, how deep it must
#  probe the file to detect all tracks. Medianfo isn’t all good, however, as
#  to retrieve the subtitle codec type, ffprobe is better.
#
declare -A  \
gsi_propdata=(
	[f:mediainfo]=''

	[f:size_B]=''

	[f:size_kB]='depends=size_B'

	[f:size_MiB]='depends=size_B'

	[f:atts_cache_id]=''


	[c:ffprobe]=''

	[c:video_streams_count]='mediainfo=General/VideoCount'
	                         #  ^ Must only be mediainfo!

	[c:audio_streams_count]='mediainfo=General/AudioCount'
	                         #  ^ Must only be mediainfo!

	[c:subtitle_streams_count]='mediainfo=General/TextCount'
	                            #  ^ Must only be mediainfo!

	[c:format]='mediainfo=General/Format'

	[c:duration_total_s_ms]='mediainfo=General/Duration,
	                         ffprobe=c/duration'

	[c:title]='mediainfo=General/Title,
	           ffprobe=c/TAG:title'

	[c:attachments]='mediainfo=General/extra/Attachments'

	[c:attachments_count]='depends=c:attachments'


	#  Virtually everything below depends on either [track_id]
	#  or [track_typeorder], so these two properties are retrieved
	#  first for each stream type.
	#
	#  So far the items are listed in the order of accumulating dependencies
	#  (i.e. dependencies first).

	[v:track_id_and_typeorder]='depends=c:video_streams_count'

	[v:ffprobe]=''

	[v:format]='mediainfo=Video/Format,
	            ffprobe=v/codec_long_name'

	[v:duration_total_s_ms]='mediainfo=Video/Duration,
	                         depends=c:duration_total_s_ms'

	[v:bitrate]='mediainfo=Video/BitRate,
	             mediainfo=Video/NominalBitRate,
	             ffprobe=v/bit_rate'

	[v:width]='mediainfo=Video/Width,
	           ffprobe=v/width'

	[v:height]='mediainfo=Video/Height,
	            ffprobe=v/height'

	[v:resolution]='depends=v:width,
	                depends=v:height'

	[v:resolution_total_px]='depends=v:width,
	                         depends=v:height'

	[v:pixel_aspect_ratio]='mediainfo=Video/PixelAspectRatio,
	                        ffprobe=v/sample_aspect_ratio'

	[v:display_aspect_ratio]='mediainfo=Video/DisplayAspectRatio,
	                          ffprobe=v/display_aspect_ratio,
	                          depends=v:width,
	                          depends=v:height,
	                          depends=v:pixel_aspect_ratio'

	[v:is_16to9]='depends=v:display_aspect_ratio'

	[v:frame_rate]='mediainfo=Video/FrameRate,
	                ffprobe=v/r_frame_rate'

	[v:frame_rate_mode]='mediainfo=Video/FrameRate_Mode'

	[v:frame_rate_max]='mediainfo=Video/FrameRate_Maximum'

	[v:frame_count]='mediainfo=Video/FrameCount,
	                 mediainfo=Video/FromStats_FrameCount'

	[v:frame-heaviness]='depends=v:frame_count,
	                     depends=v:resolution_total_px'

	[v:pix_fmt]='ffprobe=v/pix_fmt'

	[v:chroma_subsampling]='mediainfo=Video/ChromaSubsampling'

	[v:cspace_matrix_coefs]='ffprobe=v/color_space'

	[v:cspace_primaries]='ffprobe=v/color_primaries'

	[v:cspace_tone_curve]='ffprobe=v/color_transfer'

	[v:cspace_colour_range]='ffprobe=v/color_range'

	[v:cspace_valid]=''

	[v:scan_type]='mediainfo=Video/ScanType'

	[v:scan_order]='mediainfo=Video/ScanOrder,
	                depends=v:scan_type'

	[v:encoded_library_name]='mediainfo=Video/Encoded_Library_Name'

	[v:encoded_library_version]='mediainfo=Video/Encoded_Library_Version'

	[v:encoded_library_settings]='mediainfo=Video/Encoded_Library_Settings'

	[v:stream_size_by_extraction_B]='depends=v:track_id_and_typeorder'

	[v:bitrate_by_extraction]='depends=v:track_id_and_typeorder,
	                           depends=v:stream_size_by_extraction'


	[a:track_id_and_typeorder]='depends=c:audio_streams_count'

	[a:ffprobe]='depends=c:audio_streams_count,
	             depends=v:duration_total_s_ms'

	[a:format]='mediainfo=Audio/Format,
	            ffprobe=a/codec_name'

	[a:format_profile]='mediainfo=Audio/Format_Profile,
	                    mediainfo=Audio/Format_Commercial_IfAny,
	                    ffprobe=a/profile,
	                    mediainfo=Audio/Format_AdditionalFeatures'

	                   #  ^ Format and Profile are better be gathered by one
	                   #  and the same retriever in order to compose a valid
	                   #  set (pair of Format and Profile). Some pairs, like
	                   #  AAC:LC are common among both mediainfo and ffmpeg,
	                   #  though. Here “ffprobe=a/profile” is put above the
	                   #  last line with mediainfo intentionally, as those
	                   #  “AdditionalFeatures” may contain who knows what.
	                   #  Sometimes they contain only “LC”.

	[a:is_lossless]='mediainfo=Audio/Compression_Mode,
	                 depends=a:format,
	                 depends=a:format_profile'

	[a:bitrate]='mediainfo=Audio/BitRate,
	             ffprobe=a/bit_rate'

	[a:channels]='mediainfo=Audio/Channels,
	              ffprobe=a/channels'

	[a:stream_size_by_extraction_B]='depends=a:track_id_and_typeorder'

	[a:bitrate_by_extraction]='depends=a:track_id_and_typeorder,
	                           depends=a:stream_size_by_extraction'


	[s:track_id_and_typeorder]='depends=c:subtitle_streams_count'

	[s:ffprobe]='depends=c:subtitle_streams_count,
	             depends=v:duration_total_s_ms'

	[s:codec]='ffprobe=s/codec_name,
	           mediainfo=Text/Format'

	[s:width]='ffprobe=s/width'

	[s:height]='ffprobe=s/height'

	#  Container options
	#
	[c:muxing_overhead_B]='depends=v:stream_size_by_extraction_B,
	                       depends=a:stream_size_by_extraction_B,
	                       depends=f:size_B'

	[c:bitrate_by_extraction]='depends=v:bitrate_by_extraction,
	                           depends=a:bitrate_by_extraction'

	[c:ovh_ratio_to_fsize_pct]='depends=c:muxing_overhead_B'

	[c:ovh_ratio_to_1sec_of_playback]='depends=c:muxing_overhead_B,
	                                   depends=v:bitrate_by_extraction,
	                                   depends=a:bitrate_by_extraction'
)


 # Retrieves and assigns the requested property to a particular metadata array
#
#  If a property wasn’t found, or the read value was considered invalid,
#  the property will not be set (e.g. if subsampling data wasn’t present,
#  then there will be no source_v[chroma_subsampling] item. Note, however,
#  that “test -v” doesn’t work on array items, only on arrays themselves,
#  so existence of properties should be checked with a test on a non-empty
#  string).
#
#  Retrieval is done by methods and in order, which was specified in “gsi_prop-
#  data” data. In general, Nadeshiko takes the reading from mediainfo. If it
#  won’t be present or come out successful, ffprobe is used. So there are two
#  generic retrieval methods. There is the third, which is used for some op-
#  tions, that are essential for the retrieval itself: they have no retrieval
#  rules specified in the “gsi_propdata” table and instead are retrieved with
#  a specialised function named like “gather_PROPTYPEprop_PROPNAME” (see the
#  argument list below). Simple properties are retrieved in an automated way,
#  but those that require some additional checking for the value, may have
#  custom functions, too.
#
#  Remember, that there are external files, which have their own specifics.
#  Comments in the depths of this file cover that when needed.
#
#  $1 – property specification in the format TYPE:NAME. The TYPE may be one of:
#       f, c, v, a, s, which stand for file, container, video, audio and sub-
#       title properties repsectively. PROPERTY_NAME is the Nadeshiko name for
#       a particular property (property names in the source file vary depending
#       on the container, its codecs and the way of muxing, so this has to be
#       unified). The PROPERTY_NAME is taken from the preset in “gather_source_
#       props()” (see above).
#
gather_source_prop() {
	#  Simply to remind you, that the varaible is indeed global and is being
	#  set here. Redeclaring a global nameref variable as global again won’t
	#  throw off its nameref tag, so it’s okay. We could, of course, go and
	#  redeclare it as a nameref (and this wouldn’t be an error either), but
	#  without a comment on it, this would seem no less fishy, so… keeping it
	#  short.
	#
	declare -g  gsi_collected

	local  prop
	prop="$1"

	info-4 "Requesting property ${__bri}$prop${__bri}"
	milinc-4
	[ -v gsi_collected[$prop] ]  && {
		info-4 "Already collected."
		mildec-4
		return 0
	}

	local -a  primary_prop_patterns=()

	local  propname=${prop#*:}
	local  proptype=${prop%:*}
	local  prop_retrievers
	local  prop_deps
	local  track_id
	local  dep
	local  retr
	local  section
	local  field

	prop_retrievers=()
	prop_deps=()

	#  Creating one link, that always points to the array, whose property
	#  is being filled currently. The one link is convenient to have here,
	#  within the body of this function, when we do a standard retrieval
	#  and assignment. The direct links are needed for the custom processor
	#  functions, which may need to read/write to two arrays.
	#
	case "$proptype" in
		f)
			local -n ptr_to_source="$varname"
			;;

		c|v|a|s)
			local -n ptr_to_source="${varname}_$proptype"
			;;
	esac


	#  Track specifier, to find an appropriate (or selected) track number
	#  among other tracks of the same kind. Track specifiers are regarded
	#  as container properties and must be retrieved before queries to
	#  video, audio and subtitle tracks.
	#
	case "$proptype" in
		v)
			track_id="${source_v[track_id]:-}"
			track_typeorder="${source_v[track_typeorder]:-}"
			;;
		a)
			track_id="${source_a[track_id]:-}"
			track_typeorder="${source_a[track_typeorder]:-}"
			;;
		s)
			track_id="${source_s[track_id]:-}"
			track_typeorder="${source_s[track_typeorder]:-}"
			;;
		*)
			#  Normally, these variables wouldn’t be set, but if this function
			#  runs recursively – to pull a dependency – these variables may
			#  get to the environment here from the caller function. And these
			#  variables would request stupid things, like “a property of the
			#  container second by order”.
			#
			unset track_id   track_typeorder
			;;
	esac

	#  In case when there’s no track, skip gathering property.
	#  Skip the check for “master” (or “primary”) properties, which
	#  must be collected first and aren’t dependent on track id.
	#
	primary_prop_patterns=(
		'f:.*'
		'c:.*'
		'.:track_id_and_typeorder'
		'.:ffprobe'
		'.:external_file_mediainfo'
	)
	[[ "$prop" =~ ^($(IFS='|'; echo "${primary_prop_patterns[*]}"))$ ]]  || {
		[[ "$track_id" =~ ^$INT$ ]]  || {
			info-4 "Skipping retrieval of “$prop”: no track_id."
			mildec-4
			return 0
		}
	}
	#  Additionally, require “audio” and “subs” flag variables to be set,
	#  in order to continue with a:… and s… properties. This is needed to
	#  catch situations when *…track_id_and_typeorder() has unset a flag
	#  variable, but another primary property, that depends on it (e.g.
	#  “ffprobe”), would still attempt to run, while there’s no “track_id”.
	#
	[ "$proptype" = a ] && {
		[ -v audio ] || {
			info-4 "Skipping retrieval of “$prop”: no audio stream
			        or no audio requested."
			mildec-4
			return 0
		}
	}
	[ "$proptype" = s ] && {
		[ -v subs ] || {
			info-4 "Skipping retrieval of “$prop”: no subtitle stream
			        or no subtitles requested."
			mildec-4
			return 0
		}
	}

	while read -d ','; do
		case "$REPLY" in
			mediainfo=*)
				;&
			ffprobe=*)
				prop_retrievers+=( "$REPLY" )
				;;

			depends=*)
				prop_deps+=( "${REPLY#depends=}" )
				;;
		esac
	done < <(sed -zr 's/\s+//g; s/\n//g' <<<"${gsi_propdata[$prop]:-},")

	info-4 "Dependencies: ${prop_deps[*]}"
	info-4 "Retrievers: ${prop_retrievers[*]}"

	for dep in "${prop_deps[@]}"; do
		milinc-4
		gather_source_prop "$dep"
		mildec-4
	done

	for retr in "${prop_retrievers[@]}"; do
		retriever_type="${retr%=*}"
		retr=${retr#*=}
		section="${retr%/*}"
		field="${retr##*/}"
		#
		#  In general, retrievers are allowed to fail in gathering properties:
		#  sometimes those are simply not present. A property would then be
		#  unset and the next retriever is tried. Properties may be empty, but
		#  an empty property would again call for the next retriever.
		#
		#  For about a half of all properties, which can be gathered in an
		#  ideal case, an empty value is okay and doesn’t prevent Nadeshiko
		#  to continue.
		#
		[ -v cannot_rely_on_ffprobe_for_audio ] && {
			[ "$retriever_type" = 'ffprobe'  -a  "$section" = 'a' ] && {
				info-4 "Skipping retrieval with $retriever_type for $section/$field: retriever is unreliable."
				continue
			}
		}
		[ -v cannot_rely_on_ffprobe_for_subtitles ] && {
			[ "$retriever_type" = 'ffprobe'  -a  "$section" = 's' ] && {
				info-4 "Skipping retrieval with $retriever_type for $section/$field: retriever is unreliable."
				continue
			}
		}
		info-4 "Running retriever $retriever_type: $section/$field"
		case "$retriever_type" in
			mediainfo)
				ptr_to_source[$propname]=$(
					retrieve_prop_with_mediainfo "$section"  \
					                             "$field"    \
					                             "${track_typeorder:-}"
				)  \
					|| unset ptr_to_source[$propname]
				;;

			ffprobe)
				ptr_to_source[$propname]=$(
					retrieve_prop_with_ffprobe "$section"  \
					                           "$field"
				)  \
					|| unset ptr_to_source[$propname]
				;;

			*)
				: 'When nothing specified, a specialised function
				   must be handling the retrieval.';;
		esac

		if [ -v ptr_to_source[$propname] ]; then
			if [ "${ptr_to_source[$propname]}" ]; then
				info-4 'Retriever has set the property and it contains something.'
				break
			else
				info-4 'Retriever has set the property, but it’s empty.'
			fi

		else
			info-4 'Retriever didn’t set property.'
		fi

	done

	#  Lastly, run custom processors for properties (if present), i.e. for
	#  *track_specifier ones and those, whose value has to be checked or
	#  converted to a unified format used by Nadeshiko.
	#
	if [ "$(type -t gather_${proptype}prop_$propname)" = 'function' ]; then
		info-4 "Function gather_${proptype}prop_$propname() was found. Executing it…"
		milinc
		gather_${proptype}prop_$propname
		mildec
	else
		info-4 "No custom function for this property."
	fi

	#  Consider empty value a nonsuccessful attempt to retrieve the property.
	#  Hence unsetting the property item in the destination array.
	#
	#  Strictly speaking, if a retriever failed and it knows that, it unsets
	#  the variable. Checking for an empty value is for the cases, when it
	#  doesn’t have a check.
	#
	if [[ "$propname" =~ track_id_and_typeorder$ ]]; then
		if [ -v ptr_to_source[track_id] ]; then
			info-4 "“track_id” was set to “${ptr_to_source[track_id]}”."
		else
			info-4 "“$proptype:track_id” wasn’t set."
		fi

		if [ -v ptr_to_source[track_typeorder] ]; then
			info-4 "“$proptype:track_typeorder” was set to “${ptr_to_source[track_typeorder]}”."
		else
			info-4 "“$proptype:track_typeorder” wasn’t set."
		fi

		#  No “failed” case check for “track_id”, because if this property
		#  won’t be gathered, execution stops. “track_typeorder” should exist
		#  only when there’s more than one of a kind (video, audio, subtitles).

	elif [ "${ptr_to_source[$propname]:-}" ]; then
		info-4 "“$prop” was set to “${ptr_to_source[$propname]}”."

	else
		info-4 "Property is empty and is getting unset."
		unset ptr_to_source[$propname]  #  Sic!
	fi

	gsi_collected+=( [$prop]='t' )
	info-4 "Retrieval finished."  # “finished”, because its completeness
	                              # can be only relative.
	mildec-4
	return 0
}


#  $1  – section in the mediainfo output
#  $2  – field name in the section
#  $3  – track number for the typeorder attribute
#        (provided only when necessary)
#
#  See also the description to gather_source_info() for the explanation
#  of the typeorder attribute.
#
retrieve_prop_with_mediainfo() {
	local  section="$1"
	local  subsection
	local  field="$2"
	local  typeorder="${3:-}"
	local  mediainfo_source

	[ "$typeorder" ]  \
		&& typeorder="[@typeorder=\"$typeorder\"]"

	[[ "$section" =~ ^(.+)/(.+)$ ]]  && {
		section="${BASH_REMATCH[1]}"
		subsection="/x:${BASH_REMATCH[2]}"
	}

	case "$section" in
		Audio)
			if [ -v source_a[external_file] ]; then
				local -n mediainfo_source="source_a[external_file_mediainfo]"
			else
				local -n mediainfo_source="source[mediainfo]"
			fi
			;;

		 # Mediainfo isn’t used for subtitles.
		#
		# Subtitles)
		# 	if [ -v source_s[external_file] ]; then
		# 		local -n mediainfo_source="source_s[external_file_mediainfo]"
		# 	else
		# 		local -n mediainfo_source="source[mediainfo]"
		# 	fi
		# 	;;

		*)
			local -n mediainfo_source="source[mediainfo]"
			;;
	esac

	"${xml[@]}" "//x:track[@type=\"$section\"]${typeorder:-}${subsection:-}/x:$field"  \
		<<<"$mediainfo_source"  2>/dev/null  \
		|| true

	return 0
}


#  $1 – stream type, one of: ‘c’, ‘v’, ‘a’, ‘s’ for container, video, audio
#       and subtitles metadata, repsectively.
#  $2 – property name to retrieve from ffprobe output.
#
#  Due to how ffprobe output is gathered (see the big note above), there’s no
#  need in the typeorder attribute or checking for external file here.
#
retrieve_prop_with_ffprobe() {
	local  stream_type="$1"
	local  field="$2"

	case "$stream_type" in
		c)	sed -rn "s/^$field=(.*)$/\1/p" <<<"${source_c[ffprobe]:-}" || true
			#  Errors are not critical.
			;;

		v)	sed -rn "s/^$field=(.*)$/\1/p" <<<"${source_v[ffprobe]:-}" || true
			#  Errors are not critical.
			;;

		a)	sed -rn "s/^$field=(.*)$/\1/p" <<<"${source_a[ffprobe]:-}" || true
			#  Errors are not critical.
			;;

		s)	sed -rn "s/^$field=(.*)$/\1/p" <<<"${source_s[ffprobe]:-}" || true
			#  Other errors are not critical.
			;;
	esac

	return 0
}


 # Before stream properties can be requested, the streams themselves must be
#  detected. While formats like MKV usually contain the necessary information
#  in the container metadata, other, such as VOB, do not. Which means, that
#  ffmpeg would have to go deeper into the stream data and see for itself what
#  is there. Most troubles come with MPEG program streams, but presumably that
#  remains true for transport streams as well.
#
#  The default ffmpeg duration length for -probesize and -analyseduration
#  options is 5M (meaning 5 000 000 microseconds or 5 seconds), and this is
#  the value of the first test.
#
#  Getting the smallest probesize needed is tricky and may go two ways: early
#  or lately. Nadeshiko attempts to save time, so it to not bruteforce the
#  entire file in seeking of data streams, when this is avoidable. The two ways
#  differ on whether the number of subtitle streams is reported by mediainfo
#  (see request for $source_c[subtitle_stream_count]). If it is reported, then
#  things are much simpler, and Nadeshiko runs /late/ probing, trying probe
#  sizes from smallest to biggest until the number of the detected streams
#  would match the one reported by mediainfo. Another way is when mediainfo
#  didn’t report on subtitle stream count, and the job of detecting streams
#  is wholly relied on Nadeshiko’s shoulders. This is when the whole length
#  of the file is read. Happily, the only time is was seen in reality was
#  a .VOB file from an official DVD dating 1999.
#
#  Same mechanics were implemented for audio in fact “just in case” and may
#  not be really be needed. The container metadata usually don’t neglect
#  informmation about the audio tracks, while for subtitles it /may/ be miss-
#  ing. So, while staying available, this code will probably never execute.
#
# [$1] – when set to “full_length_probe”, runs just one test on the full
#       length of the file (skipping all tests on the smaller durations),
#       sets $source_c[subtitle_streams_count]. (The “early” way of calling
#       this function.)
#
#  Sets: source_a[probe_duration] or source_s[probe_duration] (depending on
#  the name this function was called as). The biggest value is determined
#  later in “run_checks()” and set to the global variable $probe_size, which,
#  being  an output option, will be passed to ffmpeg’s options -probesize and
#  -analyzeduration as an argument (same to both).
#
__gather_aprop__set_probesize() { __gather_xprop__set_probesize "$@"; }
__gather_sprop__set_probesize() { __gather_xprop__set_probesize "$@"; }
__gather_xprop__set_probesize() {
	declare -g  source_s    #  These two are actually “gather_source_info()”’s,
	declare -g  source_a    #  local pointers to the actually global src_* vari-
	                        #  ables. But in relation to this function, source_*
	                        #  are in the outside, hence global, scope. Their
	                        #  declaration isn’t necessary, it’s here just to
	                        #  show that this was taken account of.
	local  probes
	local  probe_start_idx
	local  detected_streams_count
	local  all_streams_detected
	local  full_length_probe
	local  i

	[ "${1:-}" = full_length_probe ]  \
		&& full_length_probe=t

	case "${FUNCNAME[1]}" in
		*sprop__set_probesize)
			what='subtitle'
			what_spelled_out='subtitles'
			ff_stream_type='s'
			;;
		*aprop__set_probesize)
			what='audio'
			what_spelled_out='audio tracks'
			ff_stream_type='a'
			;;
	esac
	#   ^ The difference between “$what” and “$what_spelled_out” is
	#     - “$what” is used to reference variable names, “$what_spelled_out”
	#        is purely a message thing;
	#     - “$what” is used in messages when it’s important to point out the
	#        *stream* nature of the subject, as in streams in the container,
	#        that are present in a particular number, that are having their
	#        indices; “$what_spelled_out” is a more humane version for the
	#        times when we mean just “subtitles” or “audio tracks” in princi-
	#        ple, however many there is or available. Moreover, when every
	 #       message blurts out “stream stream stream” this is annoying.

	 # Automatically set probe size to the default 5M (=5 seconds), which
	#  ffmpeg applies, if it’s left unspecified. This indeed supposes that the
	#  external file is audio or subtitles per se, not another video container.
	#
	[ -v source_${ff_stream_type}[external_file] ]  && {
		declare -g source_${ff_stream_type}[probe_duration]='5M'
		new_time_array  probe_for_${what}_required_time 5
		info-4 "Optimal probe length for $what_spelled_out was found to be 0:0:5."
		return 0
	}

	#  The first probe is quick. By default, ffmpeg tools check into the first
	#  5 seconds. -probesize and -analyzeduration accept value in microseconds,
	#  thus 5 seconds equals 5’000’000.
	#
	probes=( 5M )                                          #  0:05

	(( ${source_v[duration_total_s_ms]%\.*} > 5 ))  \
		&& probes+=( $((10*60))M )                         #  10:00

	(( ${source_v[duration_total_s_ms]%\.*} > 10*60 ))  \
		&& probes+=( $((30*60))M )                         #  30:00

	(( ${source_v[duration_total_s_ms]%\.*} > 30*60 )) && {
		#  half the duration
		probes+=( $(( ${source_v[duration_total_s_ms]%\.*} /2))M  )
		#  entire duration
		probes+=( ${source_v[duration_total_s_ms]%\.*}M )
	}

	[ -v full_length_probe ]  \
		&& probe_start_idx=$(( ${#probes[*]} - 1 ))  \
		|| probe_start_idx=0

	info "Probing the source video to detect $what_spelled_out."
	milinc

	[ -v full_length_probe ]  \
		&& info "The container metadata offered no information on the number of
		         $what streams. In order to ensure, that ffmpeg would be able
		         to detect the requested one, Nadeshiko will have to probe the
		         entire length of the source video to detect them. This will take
		         some time. Blame whoever ignored putting this information in the
		         metadata. All shorter tests will be skipped."

	for ((i=$probe_start_idx; i<${#probes[*]}; i++)); do
		case $i in
			0)	info "Seeking up to 0:05. This is the ordinary ffmpeg probing
				      length and it’s fast."
				;;

			1)	info "Seeking up to 10:00. This is still fast."
				;;

			2)	info "Seeking up to 30:00. Usually completes within
				      several seconds."
				;;

			3)	info "Seeking up to half the duration of the source file."
				;;

			4)	info "Seeking for the full file length."
				;;
		esac
		milinc

		detected_streams_count=$(
			$ffprobe -hide_banner -v error  \
			         -probesize "${probes[i]}"  \
			         -analyzeduration "${probes[i]}"  \
			         -show_streams -select_streams $ff_stream_type  \
			         "$path"  \
			| grep -cE '^\[STREAM]$' || true
		)
		msg "Complete."

		[ "${source_c[${what}_streams_count]:-}" ] && {
			if (( detected_streams_count == ${source_c[${what}_streams_count]} )); then
				all_streams_detected=t
				mildec
				break

			elif (( detected_streams_count > ${source_c[${what}_streams_count]} )); then
				all_streams_detected=t
				warn "ffprobe could detect more $what streams, than mediainfo."
				mildec
				break
			fi
		}

		mildec

	done
	let "i == ${#probes[*]} && i-- ,1"

	if [ ! -v full_length_probe  -a  ! -v  all_streams_detected ]; then
		redmsg "Despite that ffmpeg tried to probe the file to the maximum depth,
		        not all $what_spelled_out were detected. There is no guarantee, that
		        the properties retrieved would belong to the requested stream,
		        as well as that ffmpeg could be pointed to the proper stream."
		unset probes

	else
		new_time_array  probe_for_${what}_required_time ${probes[i]%M}
		local -n optimal_probe_length="probe_for_${what}_required_time"
		info-4 "Optimal probe length for $what_spelled_out was found to be ${optimal_probe_length[ts_short_no_ms]}."
	fi

	[ -v full_length_probe ]  \
		&& source_c[${what}_streams_count]=$detected_streams_count

	declare -g source_${ff_stream_type}[probe_duration]="${probes[i]}"

	mildec
	return 0
}


noglob_off

gsi_libs_to_load=(
	"$LIBDIR"/gather_source_info_fprops.sh
	"$LIBDIR"/gather_source_info_cprops.sh
	"$LIBDIR"/gather_source_info_vprops.sh
)
[ -v audio ] && gsi_libs_to_load+=( "$LIBDIR/gather_source_info_aprops.sh" )
[ -v subs ] && gsi_libs_to_load+=( "$LIBDIR/gather_source_info_sprops.sh" )

for gsi_lib in "${gsi_libs_to_load[@]}"; do
	source "$gsi_lib"
done

unset  gsi_libs_to_load  \
       gsi_lib

noglob_rewind


return 0



                     #  Notes of lower importance  #

 # About the mediainfo backend
#
#  - Do not use OLDXML output – it returns property values in several formats,
#    but leaves them in the same tags, dammit. Since neither xmlstarlet, nor
#    xmllint nor any other tool supports fancy XPath functions like “matches()”,
#    it is easier to use plaintext output and parse it with sed, rather than
#    trying to make sense of this bullshit.
#  - XML output provides values, that are surprisingly sensible in comparison
#    to the “default” and “OLDXML” outputs. These values must only be checked
#    that they exist, only a few require conversion to some other format, con-
#    venient for Nadeshiko. XML output has a nuance with namespaces, see below.
#

 # About the ffprobe backend
#
#  - It allows to select the format (representation) of the data with command
#    line parameters.
#  - On the other hand, the stream bitrate must be retrieved in some cases
#    from the stream data field, and in other – from a subsection called “tags”.
#    Unpredictable behaviour makes this tool inconvenient.
#  - JSON format should be used to request data
#
#        $ ffprobe -hide_banner  \
#                  -v error  \
#                  -print_format json=string_validation=replace:string_validation_replacement=ffprobe_could_not_retrieve_the_data  \
#                  -show_streams -select_streams v:0  \
#                                -select_streams a:1  \
#                  -show_entries format=bit_rate,filename,start_time:stream=duration,width,height,display_aspect_ratio,r_frame_rate,bit_rate \
#                  -sexagesimal
#                   ^ to print times in HH:MM:SS.sssssssss format
#
#  - Disregard the “flat” format, it is simple, but will create too many
#    variables, that are not needed. However, why not try it out:
#        -print_format=flat=sep_char=_:hierarchical=1
#                                                   ^ 1 or 0
#
#  - XML format may be considered to lower the count of dependencies
#    (ditch jq and use xmlstarlet for both the mediainfo and ffprobe output)
#    However, jq is necessary for Nadeshiko-mpv to read data via the mpv IPC
#    protocol, so it’s a dependency anyway.
#
#  - ffprobe options to consider:
#    “-unit” – shows the unit of the displayed values.
#    “-prefix” – orders to use SI prefixes for the displayed values. Unless
#                the “-byte_binary_prefix” option is used all the prefixes
#                are decimal.
#    “-byte_binary_prefix” – forces the use of binary prefixes for byte values.
#
#  - To retrieve data with ffprobe, there are several ways:
#    -show_format – displays container metadata
#    -show_streams – displays metadata of all streams
#    -select_streams – allows to narrow down the list of streams to show,
#         however, it’s unclear how to pass more than one stream specifier to
#         this option.
#    -show_entries stream – is needed to show all fields in the stream, or
#         with “format=…” syntax may be used to compose the list of fields
#         to show in the output, further narrowing the search.
#    -print_format (also -of) is used to get rid of the wrappers or field
#         names in the output.
#         There are various output types looking enticing, e.g. json and xml,
#         but XML is a pain to parse, because one half of the data is in the
#         attributes, and the other half are keys. JSON is harder to parse
#         than Default format.
#

 # Formerly used single-property retrieving code based on ffprobe
#  May be helpful to one-time check what’s in the file during debugging.
#
# set -o pipefail
# #  For BDMV ffprobe will return duplicated strings, because
# #  there are secondary stream entries in the [PROGRAM] section.
# #  We leave only the [STREAM] part by reading the last line only.
# ffprobe -hide_banner -v error -select_streams $stype  \
#         -show_entries stream=$key  \
#         -of default=noprint_wrappers=1:nokey=1  \
#         "$video"  \
# 	| sed -n '$p'  \
# 	|| err "Cannot retrieve “$stype_full” property “$key”: ffmpeg error."
# set +o pipefail
