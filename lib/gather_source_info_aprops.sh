#  Should be sourced.

#  gather_source_info_aprops.sh
#  A submodule of gather_source_info.sh that holds functions to retrieve
#  audio stream properties.
#  Author: deterenkelt, 2018–2023
#
#  For licence see nadeshiko.sh
#


               #  Audio track / audio external file info  #

gather_aprop_external_file_mediainfo() {
	[ -v source_a[external_file] ] && {
		source_a[external_file_mediainfo]=$(
			mediainfo --Full --Output=XML "${source_a[external_file]}"
		)
		[ "${source_a[external_file_mediainfo]}" ]  \
			|| warn "Couldn’t retrieve mediainfo for the external audio track."
	}
	return 0
}


 # Getting track id, checking it, if it’s already set, and determining it,
#  if neither user nor the container specify to us any preference. Addition-
#  ally, store to a property the track typeorder, which is necessary to query
#  other properties via mediainfo.
#
gather_aprop_track_id_and_typeorder() {
	local  is_default
	local  err_msg
	local  path
	local  i

	path="${source_a[external_file]:-${source[path]}}"


	                        #  External file  #

	 # If the external file is set, it must have passed the command line
	#  argument check and is a readable file. There, however, must be a check
	#  for its contents being actually decodable, but that comes after we
	#  read the rest of the properties, including the codec.
	#
	[ -v source_a[external_file] ] && {
		#  Note, that track_id ≠ built-in track id.
		source_a[track_id]=0
		# source_a[track_typeorder] remains not set
		return 0
	}

	                   #  Built-in, but none found  #

	#  Preventive forceful detection of the audio streams count,
	#  if mediainfo retrieved us nothing.
	#
	[ "${source_c[audio_streams_count]:-}" ]  \
		|| __gather_aprop__set_probesize  'full_length_probe'

	(( source_c[audio_streams_count] == 0 ))  && {
		[ -v source_a[track_id] ]  && {
			err_msg="Cannot use audio track ${source_a[track_id]} "
			err_msg+="($(nth "$(( source_a[track_id] + 1 ))") by order) – "
			err_msg+="there’s none in the source."
			err "$err_msg"
		}
		info "No audio track to add. Disabling sound."
		unset audio
		unset source_a[track_id]  #  (sic!)
		# source_a[track_typeorder] remains not set
		return 0
	}

	                     #  Built-in, 1 or more  #

	 # If the track is selected by user, there’s nothing to retrieve. If the
	#  track isn’t set and it’s known that there is one or more, set the id
	#  to 0 by default.
	#
	if [ -v source_a[track_id] ]; then
		(( source_a[track_id] > ${source_c[audio_streams_count]} ))  && {
			err_msg="Cannot use audio track ${source_a[track_id]} "
			err_msg+="($(nth "$(( source_a[track_id] + 1 ))") by order) – "
			err_msg+="the video has only ${source_c[audio_streams_count]}."
			err "$err_msg"
		}
	else
		#  If we’re here, that means that audio is enabled, there are built-in
		#  audio tracks, and that we must find the default track and set its
		#  number to $source_a[track_id].
		#
		#  The check would be futile for three reasons:
		#    - none of the audio tracks may be marked as default
		#       in the container;
		#    - in order to run this check, we would have to grab the ffprobe
		#       output first. And this means also to run the depth probes
		#       first – to verify, that ffmpeg is able to detect all the
		#       streams. In order to avoid wasting time on probing later,
		#       when we’ll be going to retrieve stream properites, everything
		#       would be better grabbed in one pass, basically there will be
		#       a huge tangled ball, where retrieval of several properties
		#       done at once.
		#    - finally, people rely more on their media player, than to the
		#       container properties. And their media player would choose
		#       the default based on track language. If we’re going to find,
		#       what users want by default, it’d be better to introduce rele-
		#       vant properties, such as “select subtitles for this language
		#       by default”.
		#  So, we just use use track 0.
		#
		source_a[track_id]=0
	fi

	#  Quit early, if the only built-in track becomes the default:
	#  everything, that’s needed, is already set.
	#
	(( source_c[audio_streams_count] == 1 ))  && {
		# source_a[track_typeorder] remains not set
		return 0
	}

	                  #  Definitely multiple tracks  #

	source_a[track_typeorder]=$(( source_a[track_id] +1 ))

	return 0
}


gather_aprop_ffprobe() {
	declare -g  cannot_rely_on_ffprobe_for_audio

	local  path

	path="${source_a[external_file]:-${source[path]}}"

	[ ! -v source_a[probe_duration] ]  \
		&& __gather_aprop__set_probesize

	source_a[ffprobe]=$(
		$ffprobe -hide_banner -v error  \
			     -probesize "${source_a[probe_duration]}"  \
			     -analyzeduration "${source_a[probe_duration]}"  \
		         -select_streams "a:${source_a[track_id]}"  \
		         -show_entries stream  \
		         -of default=noprint_wrappers=1  \
		         "$path"  \
		    | grep -vE '=N/A\s*$'
	)
	[ "${source_a[ffprobe]}" ]  || {
		warn "Couldn’t retrieve ffprobe for the audio stream."
		cannot_rely_on_ffprobe_for_audio=t
	}
	return 0
}


gather_aprop_is_lossless() {
	local  fmt
	local  delim=$known_audio_lossless_formats_profile_delimiter
	local  fmt_at_hand="${source_a[format]:-}"
	fmt_at_hand+="${source_a[format_profile]:+$delim${source_a[format_profile]}}"

	if [ "${source_a[is_lossless]}" = Lossless ]; then
		source_a[is_lossless]=t

	else
		unset source_a[is_lossless]
		shopt -s nocasematch
		for fmt in "${known_audio_lossless_formats[@]}"; do
			[[ "$fmt" = "$fmt_at_hand" ]] && {
				source_a[is_lossless]=t
				break
			}
		done
		shopt -u nocasematch
	fi

	return 0
}


gather_aprop_bitrate() {
	#
	#  Keep in mind, that some modern codecs like DTS-HD MA may contain
	#  lossy data as part of the complete lossless stream. In this case
	#  the Bitrate field will return something like “Unknown / 1509000”.
	#  If this becomes common in the future, activate the check
	#  on Bitrate Mode field, and see if it would contain “VBR / CBR”.
	#
	[[ "${source_a[bitrate]:-}" =~ ^$INT$ ]]  || {
		warn "Couldn’t determine audio stream bitrate."
		unset source_a[bitrate]
	}
	return 0
}


gather_aprop_channels() {
	[[ "${source_a[channels]:-}" =~ ^$INT$ ]]  || {
		warn "Couldn’t determine the number of channels in the audio stream."
		unset source_a[channels]
	}
	return 0
}


gather_aprop_stream_size_by_extraction_B() {
	local raw_stream="$TMPDIR/aud.raw"
	rm -f "$raw_stream"
	#  Assuming, that webms that we test all have audio track AFTER the video,
	#  i.e. that the audio stream has id 1 (video stream id is 0). Mkvextract
	#  operates on the whole bunch of streams without subdivision by kind.
	mkvextract "${source[path]}" tracks --fullraw "1:$raw_stream"  &>/dev/null
	source_a[stream_size_by_extraction_B]=$( stat -c %s "$raw_stream" )
	[[ "${source_a[stream_size_by_extraction_B]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t calculate audio stream size by extraction."
		unset source_a[stream_size_by_extraction_B]
	}
	return 0
}


gather_aprop_bitrate_by_extraction() {
	[ -v source_a[stream_size_by_extraction_B] ] || return 0
	[ -v source_c[duration_total_s_ms] ] || return 0
	source_a[bitrate_by_extraction]=$((
		  source_a[stream_size_by_extraction_B]
		* 8
		/ ${source_c[duration_total_s_ms]%\.*}
	))
	[[ "${source_a[bitrate_by_extraction]}" =~ ^$INT$ ]]  || {
		warn "Couldn’t calculate video stream size by extraction."
		unset source_a[bitrate_by_extraction]
	}
	return 0
}

 # May be needed in the future.
#
# gather_info_audio_codec_id() {
# 	if [  -v source_a[external_file]  ]; then
# 		__gather_audio_info_check_for_external_file
# 		source_a[codec_id]=$(
# 			${xml[@]} '//x:track[@type="Audio"]/x:CodecID'  \
# 				<<<"${source_a[external_file_mediainfo]}"
# 		)
# 	elif (( source_c[audio_streams_count] > 0 )); then
# 		__gather_audio_info_set_track_specifier
# 		source_a[codec_id]=$(
# 			${xml[@]} "//x:track[@type=\"Audio\"]${source_a[track_specifier]:-}/x:CodecID"  \
# 				<<<"${source[mediainfo]}"
# 		)
# 	else
# 		:  #  No audio tracks – nothing to set.
# 	fi
# 	return 0
# }
#
#
# gather_info_audio_bitrate_mode() {
# 	if [  -v source_a[external_file]  ]; then
# 		__gather_audio_info_check_for_external_file
# 		source_a[bitrate_mode]=$(
# 			${xml[@]} '//x:track[@type="Audio"]/x:BitRate_Mode'  \
# 				<<<"${source_a[external_file_mediainfo]}"
# 		)
# 	elif (( source_c[audio_streams_count] > 0 )); then
# 		__gather_audio_info_set_track_specifier
# 		source_a[bitrate_mode]=$(
# 			${xml[@]} "//x:track[@type=\"Audio\"]${source_a[track_specifier]:-}/x:BitRate_Mode"  \
# 				<<<"${source[mediainfo]}"
# 		)
# 	else
# 		:  #  No audio tracks – nothing to set.
# 	fi
# 	return 0
# }


return 0