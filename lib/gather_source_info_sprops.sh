#  Should be sourced.

#  gather_source_info_sprops.sh
#  A submodule of gather_source_info.sh that holds functions to retrieve
#  subtitle stream properties.
#  Author: deterenkelt, 2018–2023
#
#  For licence see nadeshiko.sh
#


              #  Subtitle stream / subtitle external file info  #

 # Mediainfo is less reliable here for it mixes up types, so the subtitle
#  metadata rely only on ffprobe.
#
# gather_sprop_external_file_mediainfo() {
# 	[ -v source_s[external_file] ] && {
# 		source_s[external_file_mediainfo]=$(
# 			mediainfo --Full --Output=XML "${source_s[external_file]}"
# 		)
# 		[ "${source_s[external_file_mediainfo]}" ]  \
# 			|| warn "Couldn’t retrieve mediainfo for the external subtitle track."
# 	}
# 	return 0
# }


 # Getting track id, checking it, if it’s already set, and determining it,
#  if neither user nor the container specify to us any preference. Addition-
#  ally, store to a property the track typeorder, which is necessary to query
#  other properties via mediainfo.
#
gather_sprop_track_id_and_typeorder() {
	local  is_default
	local  err_msg
	local  path
	local  i

	path="${source_s[external_file]:-${source[path]}}"


	                        #  External file  #

	 # If the external file is set, it must have passed the command line
	#  argument check and is a readable file. There, however, must be a check
	#  for its contents being actually decodable, but that comes after we
	#  read the rest of the properties, including the codec.
	#
	[ -v source_s[external_file] ] && {
		#  Note, that track_id ≠ built-in track id.
		source_s[track_id]=0
		# source_s[track_typeorder] remains not set
		return 0
	}

	                   #  Built-in, but none found  #

	#  Preventive forceful detection of the subtitle streams count,
	#  if mediainfo retrieved us nothing.
	#
	[ "${source_c[subtitle_streams_count]:-}" ]  \
		|| __gather_sprop__set_probesize  'full_length_probe'

	(( source_c[subtitle_streams_count] == 0 ))  && {
		[ -v source_s[track_id] ]  && {
			err_msg="Cannot use subtitle track ${source_s[track_id]} "
			err_msg+="($(nth "$(( source_s[track_id] + 1 ))") by order) – "
			err_msg+="there’s none in the source."
			err "$err_msg"
		}
		info "No subtitle track to add. Disabling hardsub."
		unset subs
		unset source_s[track_id]  #  (sic!)
		# source_s[track_typeorder] remains not set
		return 0
	}

	                     #  Built-in, 1 or more  #

	 # If the track is selected by user, there’s nothing to retrieve. If the
	#  track isn’t set and it’s known that there is one or more, set the id
	#  to 0 by default.
	#
	if [ -v source_s[track_id] ]; then
		(( source_s[track_id] >= source_c[subtitle_streams_count] ))  && {
			err_msg="Cannot use subtitle track ${source_s[track_id]} "
			err_msg+="($(nth "$(( source_s[track_id] + 1 ))") by order) – "
			err_msg+="the video has only ${source_c[subtitle_streams_count]}."
			err "$err_msg"
		}
	else
		#  If we’re here, that means that audio is enabled, there are built-in
		#  audio tracks, and that we must find the default track and set its
		#  number to $source_a[track_id].
		#
		#  (See considerations in the audio counterpart to this function.)
		#
		source_s[track_id]=0
	fi

	#  Quit early, if the only track becomes the default: everything,
	#  that’s needed, is already set.
	#
	(( source_c[subtitle_streams_count] == 1 ))  && {
		# source_s[track_typeorder] remains not set
		return 0
	}

	                  #  Definitely multiple tracks  #

	#  Only needed for mediainfo. Basically all subtitle properties are ret-
	#  rieved with ffprobe, so “track_typeorder” currently is never used.
	#  Still, it’s a basic thing, that should be defined – would be a problem
	#  to count on something basic when it’s not present.
	#
	source_s[track_typeorder]="$(( source_s[track_id] +1 ))"

	return 0
}


gather_sprop_ffprobe() {
	declare -g  cannot_rely_on_ffprobe_for_subtitles

	local  path

	path="${source_s[external_file]:-${source[path]}}"

	[ ! -v source_s[probe_duration] ]  \
		&& __gather_sprop__set_probesize

	source_s[ffprobe]=$(
		$ffprobe -hide_banner -v error  \
			     -probesize "${source_s[probe_duration]}"  \
			     -analyzeduration "${source_s[probe_duration]}"  \
		         -select_streams "s:${source_s[track_id]}"  \
		         -show_entries stream  \
		         -of default=noprint_wrappers=1  \
		         "$path"  \
		    | grep -vE '=N/A\s*$' || true
	)
	[ "${source_s[ffprobe]}" ]  || {
		warn "Couldn’t retrieve ffprobe output for the subtitle stream."
		cannot_rely_on_ffprobe_for_subtitles=t
	}

	return 0
}


 # There’s a problem with subtitles, namely that mediainfo relies on the type
#  looking at the file extension. It also seems to not differentiate between
#  subrip and vtt, reporting them both as just “UTF-8”. So, for the external
#  files it’s best to use mimetype, which looks inside the file.
#
#  It’s better to have (Web)VTT separated from SubRip, because who knows what
#  the former will become, maybe it will replace ASS one day. In the videos
#  downloaded from YouTube vtt subs already have a style section, even though
#  virtually nothing can read those styles yet. Try this ID: xlCfdggC1fI.
#
#  The value of “source_s[codec]” corresponds to how ffprobe reports subtitle
#  codecs. Thus, mimetype of external files is converted to “ass”, “webvtt”
#  and “subrip”.
#
gather_sprop_codec() {
	local  subs_mimetype

	[ -v source_s[external_file] ] && {
		subs_mimetype=$(mimetype -L -b -d "${source_s[external_file]}")
		case "$subs_mimetype" in
			'SSA subtitles')
				;&
			'ASS subtitles')
				source_s[codec]='ass'
				;;
			'WebVTT subtitles')
				source_s[codec]='webvtt'
				;;
			'SubRip subtitles')
				source_s[codec]='subrip'
				;;
			#'<bitmap subtitles in general>')
				#  Bitmap subtitles technically can be rendered, but:
				#  - it is unclear how to identify them properly;
				#  - VobSub subtitles extract as two files – an .idx file
				#    and a .sub file. There’s no example how to use -map
				#    on them;
				#  Thus, unless there’s a precedent, VobSub as external
				#  files won’t be supported, a workaround may be building
				#  them into a mkv (a simple stream copy would do, and
				#  it would be fast), then Nadeshiko can overlay them,
				#  when they are included as a stream.
				#;;
			*)
				source_s[codec]='undefined'
				#  Set [mimetype], so that when [codec] is not set, the main
				#  program could output something about the file.
				source_s[mimetype]=$subs_mimetype
				;;
		esac
	}

	return 0
}


return 0