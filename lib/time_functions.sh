#  Should be sourced

#  time_functions.sh
#  Helper functions to convert time formats, subtract times etc.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # Returns 0, if the argument is a valid timestamp.
#  Examples of valid timestamps:
#           5                           = 00:00:05.000
#           5.222                       = 00:00:05.222
#        2500                           = 00:41:40.000
#        2500.07        = 2500.070      = 00:41:40.070
#        1:3            = 01:03         = 00:01:03.000
#       11:38
#       11:38.642
#     1:22:33
#    01:22:33.444
#    99:22:33
#
is_valid_timestamp() {
	local ts=${1:-}  hours        minutes        seconds       mseconds
	#               /     \      /       \      /       \     /        \
	[[ "$ts" =~ ^(([0-9]{1,2}:|)([0-9]{1,2}:)|)([0-9]{1,6})(\.[0-9]{1,3}|)$ ]]  \
		|| return 1
	minutes="${BASH_REMATCH[3]%:}"
	[ "$minutes" ] && {
		#  If minutes are set, they must be <=59
		(( 10#$minutes > 59 ))  && return 5
		seconds="${BASH_REMATCH[4]}"
		#  Same for the seconds.
		#  Only when minutes are not present, seconds may be >59.
		(( 10#$seconds > 59 ))  && return 5
	}
	return 0
}
export -f  is_valid_timestamp


 # Creates a global associative array $1 with timestamp $2 represented
#  in several formats as the keys.
#  $1 – variable name, must not be declared beforehand
#  $2 – timestamp
#
new_time_array() {
	local varname="${1:-}"
	local ts="${2:-}"

	is_valid_timestamp "$ts"  \
		|| err "“$ts” is not a valid timestamp."
	[ "$varname" ]  \
		|| err "Variable name shouldn’t be empty."

	 # A note on the use of “new_time_array”
	#
	#  This function receives a variable name and makes it into a global asso-
	#  ciative array. For the variable to be created in the global context is
	#  important, as all time-related variables are used across multiple func-
	#  tions. So be it the main context or subshell context – the variable must
	#  be declared with “declare -g”. While the local scope may be enough for
	#  certain variables, it’s more robust to make them global – there’s only
	#  four of them in total, three are declared in the main context, and one
	#  in a coproc.
	#
	#  The actual problem is the declaration, which may accidentally precede
	#  the call to “new_time_array”. We can check that the declared variable
	#  is an associative array or not (and then throw an error, because our
	#  assignments to it won’t be successful in that case), but we can’t check,
	#  whether the variable is global or local: the “declare” builtin doesn’t
	#  report that. Thus there is only way to assuredly create a global asso-
	#  ciative array and assign items to it – and it’s to create it here. And
	#  it has to be confirmed, that the variable wasn’t declared beforehand.
	#
	#  On one hand, the declaration as a global associative array could be
	#  relayed to the caller of this function. “There’s only four of them,
	#  after all. So what’s the problem?” The real problem is that… Nadeshiko-
	#  mpv fails if the variables are declared beforehand. It’s a somewhat
	#  tangled process – that of settimg time1 and time2 – as those variables
	#  must persist across runs, values are saved and read back, and this
	#  complicates the declaration. Currently, there’s no spare time to get
	#  head on into Nadeshiko-mpv code, it took several months to roll out
	#  the last 2.23 version.
	#
	#  Moreover, the necessity to have those varaibles declared at the right
	#  time and place is hard to remember. And to not defined them as local
	#  by accident. This actually happened once.
	#
	#  tl;dr if you’re not planning to rethink a large hunk of code in more
	#        than one program,
	#          1) don’t change the way the variables are declared here;
	#          2) don’t pre-declare them.
	#

	 # Don’t ask me why it works like that, but if there would be no equals
	#  sign, that is, if $varname would be declared, but not assigned any-
	#  thing simultaneously, it will vanish when this function completes.
	#  Yes, despite being declared as a global variable, it will vanish. Poof.
	#
	#  The declaration, that involves an assignment, ends up with a properly
	#  declared array, which will persist, but it will not be created exactly
	#  empty – there will be an element created with an index [0], WHICH IS
	#  GRAVELY IMPORTANT TO THE PERSISTENCE OF THIS ARRAY IN THE GLOBAL SCOPE.
	#  TRY TO UNSET IT AND THE ARRAY WILL VANISH.
	#
	declare -gA $varname=  #  DO NOT TOUCH THIS! IT WORKS ONLY THIS WAY.
	local -n newarr="$varname"

	local  h  m  s  ms
	local  ts_short
	local  ts_short_no_ms
	local  total_s_ms
	local  total_s
	local  total_ms

	#  BASH_REMATCH was set in “is_valid_timestamp()”.
	h="${BASH_REMATCH[2]%:}"
	m="${BASH_REMATCH[3]%:}"
	s="${BASH_REMATCH[4]}"
	#  Leading zeroes switch arithmetic in bash to octal numbers,
	#  so where we can’t know the leading zeroes are removed, we have
	#  to use “10#” prefix to indicate, that a number is decimal.
	(( 10#$s > 59 )) && {
		 h=$((  10#$s / 3600 ))
		 m=$(( (10#$s - 10#$h*3600) /60 ))
		 s=$(( (10#$s - 10#$h*3600 - 10#$m*60 ) ))
	}
	ms="${BASH_REMATCH[5]#\.}"

	#  Guarantee, that HH MM SS are two digit numbers
	#  (hours may have 2 or more digits).
	#
	local time var
	for var in h m s; do
		declare -n time=$var
		until [[ "$time" =~ ^.{2,}$ ]]; do time="0$time"; done
	done

	#  Guarantee, that milliseconds have proper zeroes and are a three
	#  digit number: “.1” should later count as 100 ms, not 1 ms.
	#
	until [[ "$ms" =~ ^...$ ]]; do ms+='0'; done
	ts="$h:$m:$s.$ms"
	ts_short="${h#0}:${m#0}:${s#0}.${ms}"
	ts_short_no_ms="${h#0}:${m#0}:${s#0}"
	#
	#  Here h m s ms variables must be
	#    - not empty – or we’ll be summing emptiness $((  +  +  +  )).
	#    - do not have leading zeroes – so that bash wouldn’t treat them
	#      as octal, remember?
	#
	total_s=$((   ${h#0}*3600
	            + ${m#0}*60
	            + ${s#0}       ))
	total_s_ms="$total_s.$ms"
	total_ms=$((   ${h#0}*3600000
	             + ${m#0}*60000
	             + ${s#0}*1000
	             + ${ms##@(0|00)}  ))

	#  Timestamp in the full format, 00:00:00.000.
	#  Hours, minutes and seconds less than 10 have leading zeroes.
	#  Milliseconds are always a three-digit number.
	newarr[ts]="$ts"
	#  Same as “ts”, but colons replaced with dots.
	newarr[ts_windows_friendly]=${ts//\:/\.}
	#  Same as “ts”, but milliseconds dropped.
	newarr[ts_no_ms]=${ts%.*}
	#  Same as “ts”, but the leading zeroes are removed.
	#  This is handy, when showing duration among text, and aligning
	#  digits in columns is not needed.
	newarr[ts_short]=$ts_short
	#  “ts_short”, but without milliseconds.
	newarr[ts_short_no_ms]=$ts_short_no_ms

	#  Hours, leading zeroes removed.
	newarr[h]=${h#0}
	#  Minutes, leading zeroes removed.
	newarr[m]=${m#0}
	#  Seconds, leading zeroes removed.
	newarr[s]=${s#0}
	#
	#  With milliseconds there’s a non-trivial problem.
	#  On one hand, if we strip the leading zeroes, this will create ambiguity
	#    (like does that “8” mean “8 ms” or “800 ms” – was it ….008 or ….800?).
	#    If one would encounter a bug, his suspicion would focus on that value
	#    of millisecons with stripped zeroes. However, – he would think – when
	#    “$h”, “$m”, “$s” have their leading zeroes removed, it is expected for
	#    “$ms” to not have them, too. It even seems more likely, because the
	#    opposite (trailing zeroes stripped) seems too irrational, even though
	#    possible. If we go by suppositions alone, it seems more rational to
	#    use “$ms” with leading zeroes removed.
	#  But what does practice say? Practice shows, that things are even more
	#    complex, than how they seem they would at the first glance. On this
	#    other hand we see, that the “$ms” value is used *only* in its comp-
	#    lete, 3-digit form. What’s more suprising, *even* in calculations!
	#  All in all, “$ms” is used in three ways:
	#    - to display the amount of milliseconds (after H:M:S) – here only
	#      the complete form is not ambiguous;
	#    - in calculations with “bc” (the calculator), when it is used after
	#      the total amount of seconds. And here again, if leading zeroes
	#      won’t be there, the calculation would be wrong, as 1.080 is not the
	#      same as 1.80;
	#    - in bash calculations, that use timestamps as an integer number of
	#      milliseconds (basically, values of “$h” and “$m” converted to se-
	#      conds, “$s” added as is, the value being multiplied by 1000 and
	#      “$ms” added as is, albeit with the 10# prefix added before “$ms”
	#      to make bash see it as a decimal number and not octal). Here the
	#      variant with leading zeroes removed might come useful, but it occurs
	#      only once, and requires only “10#” added.
	#  Thus, it makes more sense to keep “$ms” in the complete form, and when
	#    (seldom) the form without leading zeroes would be needed, “10#” may
	#    be used.
	#
	#  Milliseconds, as is.
	newarr[ms]=$ms

	#  Hours, minutes and seconds as total seconds with remaining milliseconds
	#  after a dot. This is a precise value.
	newarr[total_s_ms]="$total_s.$ms"

	#  Hours, minutes and seconds as total seconds. Milliseconds are dropped,
	#  so this is a value THAT’S NOT PRECISE.
	newarr[total_s]="$total_s"

	#  A variant of the about, where any existing milliseconds are rounded up.
	#  Even 1 ms counts as one extra seconds. This value is for when it’s pre-
	#  ferrable to use a non-zero value or the “possible maximum”, when being
	#  precise to milliseconds is not a concern.
	#
	#  The duration may be less than one second. And in this case “$ms” may
	#  start with a zero, e.g. “079”. For this reason the decimal system needs
	#  to be specified explicitly.
	#
	(( 10#$ms > 0 ))  \
		&& newarr[total_s_ceil]="$((total_s+1))"  \
		|| newarr[total_s_ceil]="$total_s"

	#  Hours, minutes, seconds and milliseconds combined as milliseconds.
	#  This is a precise value.
	newarr[total_ms]=$total_ms

	return 0
}
export -f  new_time_array


 # Converts milliseconds to seconds.milliseconds – an acceptable format
#    for new_time_array.
#  $1 – number of milliseconds. As this value is supposed to be some big,
#      “total” number, no zero in front is supposed to be there.
#
total_ms_to_total_s_ms() {
	local ms="${1:-}"

	[[ "$ms" =~ ^$INT$ ]]  \
		|| err "Wrong number of milliseconds: “$ms”."

	_ms_padded=$(( ms - ms/1000*1000 ))
	until [[ "${_ms_padded}" =~ ^...$ ]]; do
		_ms_padded="0${_ms_padded}"
	done

	echo "$(( ms / 1000 )).${_ms_padded}"

	return 0
}
export -f  total_ms_to_total_s_ms


 # Converts the duration as reported by mediainfo to total number of seconds,
#  that is, “3 h 44 m 55 s 687 ms” to “13496” (milliseconds are rounded up).
#  $1 – string with hours, minutes and seconds, as reported by mediainfo.
#
mediainfo_hms_to_total_s() {
	local  mihms="$1"  d  h  m  s  ms  total_s

	[[ "$mihms" =~ ^(([0-9]{1,})d|)(([0-9]{1,2})h|)(([0-9]{1,2})mi?n?|)(([0-9]{1,2})s|)(([0-9]{1,3})ms|)$ ]]  \
		|| err "“$mihms” is not a valid mediainfo timestamp."

	d=${BASH_REMATCH[2]:-0}
	h=${BASH_REMATCH[4]:-0}
	m=${BASH_REMATCH[6]:-0}
	s=${BASH_REMATCH[8]:-0}
	ms=${BASH_REMATCH[10]:-0}
	(( 10#$ms != 0 ))  && let '++s,  1'
	total_s=$((    10#$d * 60 * 60 * 24
	             + 10#$h * 60 * 60
	             + 10#$m * 60
	             + 10#$s                 ))
	(( total_s == 0 ))  && err 'Duration is zero.'

	echo "$total_s"
	return 0
}
export -f  mediainfo_hms_to_total_s


 # Converts a timestamp (in any valid form, see above) to seconds, in total,
#  rounded down by default (milliseconds are disregarded). This function is
#  for the cases, when a volatile conversion is needed, when there’s no need
#  to keep the variable for a long time. For variables intended for continous
#  use, there’s “new_time_array()”.
#
#  $1 – string with a timestamp
# [$2] – “round_up” or just “up” to make the total number of seconds be round-
#       ed up instead. Even 1 millisecond will cause a rouning up by 1 s.
#
#  Returns the total number of seconds in the timestamp as an integer to the
#    stdout.
#
ts_to_seconds() { __ts_to_seconds "${1:-}"; }
ts_to_seconds_rounded_up() { __ts_to_seconds "${1:-}" round_up; }
__ts_to_seconds() {
	local  ts="${1:-}"
	local  round_up="${2:-}"

	is_valid_timestamp "$ts"  \
		|| err "“$ts” is not a valid timestamp."

	[ "${round_up:-}" = 'round_up' -o  "${round_up:-}" = 'up' ]  \
		|| unset round_up

	local  h  m  s  ms
	local  total_s

	#  BASH_REMATCH was set in “is_valid_timestamp()”.
	h="${BASH_REMATCH[2]%:}"
	m="${BASH_REMATCH[3]%:}"
	s="${BASH_REMATCH[4]}"

	#  Leading zeroes switch arithmetic in bash to octal numbers,
	#  so where we can’t know the leading zeroes are removed, we have
	#  to use “10#” prefix to indicate, that a number is decimal.
	(( 10#$s > 59 )) && {
		 h=$((  10#$s / 3600 ))
		 m=$(( (10#$s - 10#$h*3600) /60 ))
		 s=$(( (10#$s - 10#$h*3600 - 10#$m*60 ) ))
	}
	ms="${BASH_REMATCH[5]#\.}"

	 # Guarantee, that HH MM SS are two digit numbers (hours may have 2
	#  or more digits). This is needed to make sure, that when we’ll be
	#  stripping an extra zero, there’s at least one left.
	#
	local time var
	for var in h m s; do
		declare -n time=$var
		until [[ "$time" =~ ^.{2,}$ ]]; do time="0$time"; done
	done

	 # Here h m s variables must be
	#    - not empty – or we’ll be summing emptiness $((  +  +  +  )).
	#    - do not have leading zeroes – so that bash wouldn’t treat them
	#      as octal, remember?
	#
	total_s=$((   ${h#0}*3600
	            + ${m#0}*60
	            + ${s#0}       ))

	[ -v round_up ] && (( 10#$ms > 0 )) && let 'total_s++, 1'

	echo "$total_s"
	return 0
}
export -f  ts_to_seconds


return 0