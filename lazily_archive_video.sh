#! /usr/bin/env bash

#  lazily_archive_video.sh
#
#  This script converts a video, allowing some quality loss. The intention of
#  “lazily archiving” is to save videos like DIY instructions made by ordinary
#  people or lectures. Such kind of videos, that you would often like to save,
#  but cut out some parts, speed it up or cut down the (huge) size by throwing
#  out the excess frame rate and (or) by downscaling it. That is to say, it’s
#  not a tool for the archival purposes in the strict sense of the word (which
#  would imply lossless). Though this program allows to use high quality set-
#  tings which includes low CRF, stream copying the audio, and doing the video
#  conversion with 10 bits.
#
#  It’s not particularly efficient either in terms of retained quality in re-
#  lation to used space (you’d need a fine-tuned 2-pass for that). The conver-
#  sion employing CRF mode with x265, however, does allow to shrink the file
#  size considerably (roughly 1.4…1.7 times, and that without resolution
#  change). For the aforementioned ends, the combination of x265 and libopus
#  does the job, so these are the staple of conversion, no other codecs are
#  intended. The few options which allow further control also limit the pos-
#  sibilities somewhat, but what’s allowed should be more than enough for
#  lazily archiving.
#
#  Author: deterenkelt, 2024
#

 # lazily_archive_video.sh is free software; you can redistribute it
#    and/or modify it under the terms of the GNU General Public Licence
#    as published by the Free Software Foundation; either version 3 of
#    the Licence, or (at your option) any later version.
#  lazily_archive_video.sh is distributed in the hope that it will be
#    useful, but without any warranty; without even the implied warranty
#    of merchantability or fitness for a particular purpose.
#  See the GNU General Public Licence for more details.
#

set -feEuT
shopt -s  extglob  inherit_errexit

declare -r version='1.0.3'
declare -r release_hint=''

BAHELITE_LOAD_MODULES=(
	messages_to_desktop
	logging:use_cachedir
	rcfile:use_metaconf,use_defconf,named_configfile
	menu
	misc
)
mypath=$(dirname "$(realpath --logical "$0")")
case "$mypath" in
	'/usr/bin'|'/usr/local/bin')
		source "${mypath%/bin}/lib/nadeshiko/bahelite/bahelite.sh"
		source "${mypath%/bin}/lib/nadeshiko/time_functions.sh"
		source "${mypath%/bin}/lib/nadeshiko/nadeshiko-do-postponed/02_job_control.sh"
		;;
	*)
		source "$mypath/lib/bahelite/bahelite.sh"
		source "$mypath/lib/time_functions.sh"
		source "$mypath/modules/nadeshiko-do-postponed/02_job_control.sh"
		;;
esac

place_examplerc 'lazily_archive_video.10_main.rc.sh'

#  These three variables have names as expected by the “02_job_control.sh”
#  Nade-do-post module.
declare -r  postponed_commands_dir="$CACHEDIR/jobs"
declare -r  completed_jobs_dir="$postponed_commands_dir/completed"
declare -r  failed_jobs_dir="$postponed_commands_dir/failed"

processors=$(( cpu_count-1 ))
processors=$(( processors == 0 ? 1 : processors ))

#  Setting these solely to make the “02_job_control” run. As here we’re
#  using bare ffmpeg, these variables don’t really affect anything.
#
nadeshiko_desktop_notifications='3'  # i.e. post-conversion “all”
nadeshiko_verbosity='log=2,console=0,desktop=3'

JOB_CONTROL_MODE='Lazily archive video'

#  Making sure that the default speed has two digit precision.
#
default_speed=$( printf "%.2f" "$default_speed" )



on_exit() {
	#  Make sure, that we move the failed job files to “$failed_jobs_dir” if
	#  the program is interrupted. Normally, the coprocess running postponed
	#  job handles this (in case of handled interruptions too), so this here
	#  is a sort of precaution measure against termination caused from the
	#  outside (^C for example).
	#
	[ -v job_logdir  -a  -d "${job_logdir:-}" ]  \
		&& move_job_to_failed "$jobfile" "$job_logdir"

	rmdir "$failed_jobs_dir" 2>/dev/null || true  # Remove directory, if empty.

	return 0
}


show_usage() {
	local b="${__bright}"
	local g="${__green}"
	local w="${__white}"
	local bg="${__bright}${__green}"
	local s="${__stop}"

	 # Use this line instead to check that the output fits into 78 columns.
	#
	#  When restoring the $SHOW_HELP_PAGER line, there should be an empty
	#  line left between it and the header of the manual page.
	#
	# cat <<-EOF |& ansifilter |& grep -nE '^.{79,}$' || info 'All fits!'
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF
	   
	                  ${__bri}${__blue}Lazily archive video – Help page${s}

	Usage

	   Simply pass the video, then set options in the UI and encode:
	      ${b}./${0##*/}${s} <${b}video1${s}> [ <${b}video2${s}> … <${b}videoN${s}> ]

	   To run jobs that were created, but postponed for later:
	      ${b}./${0##*/} -r${s}


	Menus

	   To navigate the menus, you can use arrow keys (${g}↑${s} ${g}↓${s} ${g}←${s} ${g}→${s}), ${g}Home${s} and ${g}End${s}.


	Informing options

	  ${b}-h${s}, ${b}--help${s}
	      Shows this help.

	  ${b}-v${s}, ${b}--version${s}
	      Print program version and licence.


	Files

	  Config file: ${b}${__b}~/.config/lazily_archive_video/lazily_archive_video.rc.sh${s}
	  Postponed jobs: ${b}${__b}~/.cache/lazily_archive_video/postponed_commands_dir/${s}
	  Logs: ${b}${__b}~/.cache/lazily_archive_video/logs/${s}


	About

	  This script converts a video, allowing some quality loss. The intention of
	  “lazily archiving” is to save videos like DIY instructions made by ordinary
	  people or lectures. Such kind of videos, that you would often like to save,
	  but cut out some parts, speed it up or cut down the (huge) size by throwing
	  out the excess frame rate and (or) by downscaling it. That is to say, it’s
	  not a tool for the archival purposes in the strict sense of the word (which
	  would imply lossless). Though this program allows to use high quality set-
	  tings which includes low CRF, stream copying the audio, and doing the video
	  conversion with 10 bits.

	  It’s not particularly efficient either in terms of retained quality in re-
	  lation to used space (you’d need a fine-tuned 2-pass for that). The conver-
	  sion employing CRF mode with x265, however, does allow to shrink the file
	  size considerably (roughly 1.4…1.7 times, and that without resolution
	  change). For the aforementioned ends, the combination of x265 and libopus
	  does the job, so these are the staple of conversion, no other codecs are
	  intended. The few options which allow further control also limit the pos-
	  sibilities somewhat, but what’s allowed should be more than enough for
	  lazily archiving.


	Links

	  Wiki: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/wiki/Lazily-archive-video${s}
	  Report bugs here: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/issues${s}

	                                        Updated for Lazily archive video 1.0.1
	                                                             22 September 2024

	                                    ◈ ◈ ◈

	EOF
	#  Maximum 78 characters ————————————————————————————————————————————————————>|<——
	return 0
}


show_version() {
	print_program_version "$version" "$release_hint"
	cat <<-EOF
	Author: deterenkelt, 2024.
	Licence: GNU GPL version 3  <http://gnu.org/licenses/gpl.html>
	This is free software: you are free to change and redistribute it.
	There is no warranty, to the extent permitted by law.
	EOF
	return 0
}


check_for_deprecated_folders() {
	#  We can rely on XDG_*_HOME existence (both variables
	#  and the directories) thanks to Bahelite modules.
	[ -d "$XDG_CONFIG_HOME/lae" ]  \
		&& warn "“$XDG_CONFIG_HOME/lae/” directory was found, while the proper
		         path should be “$XDG_CONFIG_HOME/lazily_archive_video/”. Please
		         carefully examine existing folders and move your configuration,
		         if necessary."

	[ -d "$XDG_CACHE_HOME/lae" ]  \
		&& warn "“$XDG_CACHE_HOME/lae/” directory was found, while the proper
		         path should be “$XDG_CACHE_HOME/lazily_archive_video/”. Please
		         carefully examine existing folders and move any postponed jobs
		         (and maybe logs), if necessary."

	return 0
}


#  Code is taken from a Nadeshiko function
#
set_frame_rate() {
	local  frame_rate

	frame_rate=$( $ffprobe -hide_banner -v error  \
	                       -select_streams V  \
	                       -show_entries stream=r_frame_rate  \
	                       -of default=noprint_wrappers=1:nokey=1  \
	                       "${vid_link[path]}" )

	#  Converting rate from defconf to decimal notation, if it’s specified
	#  as a fraction.
	#
	[[ "$frame_rate" =~ ^($INT)/($INT)$ ]]  \
		&& frame_rate=$( echo "scale=3; $frame_rate" | bc )

	[[ "$frame_rate" =~ ^([1-9]|[1-9][0-9]*)(\.[0-9]+|)$ ]]  && {
		if ((    ${frame_rate%.*} >= 30  \
		      || ${frame_rate%.*} <  23  ))
		then
			vid_link[frame_rate]='24000/1001'
			vid_link[frame_rate_altered]=t
			# #  Using default frame rate from defconf + motion interp.
			# #  filter
			# [ -v motion_interp_allowed ]  \
			# 	&& motion_interp_filter_needed=t
		else
			vid_link[frame_rate]="$frame_rate"
			#  Using the input frame rate (better quality, frame-exact
			#  conversion). The global $frame_rate, the output option,
			#  takes its value from either $src_v[frame_rate] or
			#  $src_v[frame_rate_max]. Both are already converted
			#  to decimal notation.
			#
			# [ "${src_v[frame_rate_mode]:-}" = VFR  -a  -v src_v[frame_rate_max] ]  \
			# 	&& frame_rate=${src_v[frame_rate_max]}  \
			# 	|| frame_rate=${src_v[frame_rate]}
		fi
	}
	: ${vid_link[frame_rate]:='24000/1001'}

	return 0
}


validate_args() {
	# declare -gA vid_NNN  #  arrays from 001 to 999
	declare -g  only_run_jobs

	local  arg_no=1
	local  arg
	local  test_path
	local  no

	case ${#ARGS[*]} in
		0)	err "Neither files nor options were specified. Try running with “-h” / “--help”?"
			;;

		1)  case "${ARGS[*]}" in
				'-h'|'--help')
					show_usage
					exit 0
					;;

				'-v'|'--version')
					show_version
					exit 0
					;;

				'-r'|'--run-jobs')
					only_run_jobs=t
					return 0
					;;
			esac
			;;
	esac

	for arg in "${ARGS[@]}"; do
		[ -f "$arg" ]  \
			|| err "Path doesn’t exist:
			        $arg"

		[[ "$(mimetype -L -b "$arg")" =~ ^video/ ]]  \
			|| err "Path is not a video file:
			        $arg"

		#  There are two reasons to check for MPEG TS
		#  1. To force a keyframe at 0:00.000 during encoding and to
		#     put -ss and -to as *output* options. This is to avoid
		#     garbage-looking artefacts at the beginning.
		#  2. .ts (old MPEG transport stream) files cannot be recognised
		#     properly with mimetype (“mimetype -MLb” also doesn’t work,
		#     reports files as application/x-font-tex-tfm), so to recognise
		#     these input files as video, “file” is necessary to use.
		#     For that purpose, “file” is faster than mediainfo or ffprobe.
		#
		shopt -s nocasematch
		[[ "$(file -L -b  "$arg")" =~ .*(program|transport).* ]] && {
			seekbefore=t
			seekbefore_due_to_tr_or_pr_stream=t
		}
		shopt -u nocasematch

		#  In case when the path to the video file is relative, i.e. con-
		#  tains no directories, it’s necessary to expand it, because
		#  later we’ll be switching directories (e.g. when extracting
		#  fonts), and a relative path may not work.
		#
		no=$(printf '%03u' "$arg_no")
		declare -gA vid_$no
		local -n vid_link=vid_$no
		vid_link[path]=$(realpath --logical "$arg")
		vid_link[speed]="$default_speed"
		vid_link[crf]="$default_quality"
		vid_link[pix_fmt]="$default_pix_fmt"

		#  A simple detection of video-only files. To disable selecting
		#  of audio options saves us some trouble with user input further.
		#  Indeed, a proper check needs to be more sophisticated.
		#
		streams_count=$( $ffprobe -hide_banner -v error  \
		                          -show_entries format=nb_streams  \
		                          -of 'default=noprint_wrappers=1:nokey=1'  \
		                          "${vid_link[path]}" )

		if	[[ "$streams_count" =~ ^$INT$ ]]  \
			&& (( streams_count == 1 ))
		then
			vid_link[abitrate]='audio disabled'
			vid_link[abitrate_hard_disabled]=t
		else
			vid_link[abitrate]="$default_audio_bitrate"
		fi

		set_frame_rate

		#  This is merely a precaution against file paths containing weird
		#  symbols. None were met so far, but should there be one, a later
		#  error caused by wrong expansion may be troublesome to find.
		#
		[ -r "${vid_link[path]}" ] || err "Failed to set source path."
		let 'arg_no++'
	done

	return 0
}


check_for_multiple_args() {
	[ -v only_run_jobs ] && return 0

	declare -g individual_options

	if (( ${#ARGS[*]} == 1 )); then
		individual_options=t
	else
		info "There are multiple files to be encoded, so should they all use the same
		      settings, or be encoded each with individual set of options?"
		menu-bivar "Choose:  " "All use the same" "_Individual option sets_"
		[ "$CHOSEN" = 'Individual option sets' ]  \
			&& individual_options=t
	fi

	return 0
}


set_encoding_options() {
	local  amount
	local  title
	local  menu_list_options
	local  menu_start_idx
	local  i j

	[ -v individual_options ]  \
		&& amount=${#ARGS[*]}  \
		|| amount=1

	for ((i=0; i<amount; i++)); do
		[ -v individual_options ]  \
			&& title="Setting options for video $((i+1))"  \
			|| title="Setting options for ${__bri}ALL${__s} encodes"

		local -n vid_link=vid_$(printf '%03u' "$((i+1))")

		menu_start_idx=2

		while true; do
			menu_list_options=(
				foreword="Video source: ${vid_link[path]##*/}"
				"$title"
				"Video encoder---libx265 (hard set)"
			)

			[ -v vid_link[abitrate_hard_disabled] ]  \
				&& menu_list_options+=( "Audio encoder---(disabled: no audio in the source)" )  \
				|| menu_list_options+=( "Audio encoder---libopus (hard set)" )

			menu_list_options+=(
				"Start and end time of the fragment---${vid_link[start]:-not set, equals full length}${vid_link[end]:+–${vid_link[end]}}"
				"Scale---${vid_link[scale]:-not set}"
				"Crop---${vid_link[crop]:-not set}"
				"Speed---${vid_link[speed]:-not set}"
				"CRF---${vid_link[crf]:-not set}"
				"Pixel format---${vid_link[pix_fmt]:-not_set}"
				"Key frame maximum distance---${vid_link[kf_max_dist]:-250 (default)}"
			)
			[ ! -v vid_link[abitrate_hard_disabled] ] && {
				if	[ -v vid_link[speed] ]  \
					&& [ "${vid_link[speed]}" != '1.00' ]  \
					&& [ "${vid_link[abitrate]:-}" = 'copy' ]
				then
					vid_link[abitrate]="$default_audio_bitrate"
					menu_list_options+=( "Audio bit rate---${__y}${vid_link[abitrate]} (reverted)${__s}" )
				else
					menu_list_options+=( "Audio bit rate---${vid_link[abitrate]:-not set}" )
				fi
			}
			menu_list_options+=(
				"${__g}Confirm${__s}"
				"${__r}Cancel${__s}"
			)

			menu-list  start_idx="$menu_start_idx"  \
			           "${menu_list_options[@]}"

			case "$CHOSEN" in
				'Start and end time of the fragment')
					while true; do
						echo
						read -p "${__bri}Enter start time:${__s} ${__g}>${__s} "
						is_valid_timestamp "$REPLY"  || {
							redmsg "“$REPLY” is not a valid timestamp."
							continue
						}
						new_time_array time1 "$REPLY"
						vid_link[start]="${time1[ts]}"

						read -p "${__bri}Enter end time:${__s} ${__g}>${__s} "
						is_valid_timestamp "$REPLY"  || {
							redmsg "“$REPLY” is not a valid timestamp."
							continue
						}
						new_time_array time2 "$REPLY"
						vid_link[end]="${time2[ts]}"
						#
						#  ^ Converting format to the standard one to have the
						#    look that is easily understood as a timestamp by
						#    humans. These values will later go in the file
						#    name and the metadata.
						#
						break

					done
					menu_start_idx=2
					;;

				'Scale')
					local  ds_options=(320 480 576 720 1080)
					orig_height=$( $ffprobe -hide_banner -v error  \
					                        -select_streams V  \
					                        -show_entries stream=height  \
					                        -of default=noprint_wrappers=1:nokey=1  \
					                        "${vid_link[path]}" )

					[[ "$orig_height" =~ ^$INT$ ]] && {
						for ((j=${#ds_options[*]}-1; j>-1; j--)); do
							(( orig_height <= ${ds_options[j]} ))  \
								&& unset ds_options[$j]
						done
					}

					#  find the currently set option and mark it as default
					if [ -v vid_link[scale] ]; then
						for ((j=0; j<${#ds_options[*]}; j++)); do
							[ "${ds_options[j]}" = "${vid_link[scale]}" ] && {
								ds_options[j]="_${ds_options[j]}_"
								break
							}
						done
					else
						ds_options[-1]="_${ds_options[-1]}_"
					fi

					menu-list "Downscaling"  \
					          "${ds_options[@]}"  \
					          Cancel
					case "$CHOSEN" in
						'Cancel')
							unset vid_link[scale]
							;;
						*)
							vid_link[scale]="$CHOSEN"
							;;
					esac
					menu_start_idx=3
					;;

				'Crop')
					while true; do
						echo
						local  read_options=( -p "${__bri}Enter crop parameters (WxH+X+Y) or “-” to unset:${__s} ${__g}>${__s} " )
						[ "${vid_link[crop]:-}" ]  \
							&& read_options+=( -i "${vid_link[crop]}" )

						read "${read_options[@]}"

						[ "$REPLY" = '-' ] && {
							unset vid_link[crop]
							break
						}
						[[ "$REPLY" =~ ^($INT)[\ x\:]($INT)[\ :\+]($INT)[\ :\+]($INT)$ ]]  || {
							redmsg "“$REPLY” is not a valid crop specification."
							continue
						}
						vid_link[crop]="${BASH_REMATCH[1]}"
						vid_link[crop]+=":${BASH_REMATCH[3]}"
						vid_link[crop]+=":${BASH_REMATCH[5]}"
						vid_link[crop]+=":${BASH_REMATCH[7]}"

						break

					done
					menu_start_idx=4
					;;

				'Speed')
					echo
					info "Speeding up the video"
					msg "—————————————————————————"
					msg "Multiplying by more than ×1.5 results in choppy audio in some fragments.
					     To preserve the speech completely understandable, it is recommended to
					     stay within ×1.5 bounds in the common case. Do mind, that some videos
					     come already sped up by a factor of 1.15…1.25."
					echo

					local speed_options=(
						1.{0..9}{0,5} 2.0  #  1.0…2.0 | step: +0.05
					)

					#  find the currently set option and mark it as default
					if [ -v vid_link[speed] ]; then
						for ((j=0; j<${#speed_options[*]}; j++)); do
							[ "${speed_options[j]}" = "${vid_link[speed]}" ] && {
								speed_options[j]="_${speed_options[j]}_"
								break
							}
						done
					fi

					menu-carousel "Set speed:"  \
					              "${speed_options[@]}"

					[ "$CHOSEN" = '1.00' ]  \
						&& unset vid_link[speed]  \
						|| vid_link[speed]="$CHOSEN"

					menu_start_idx=5
					;;

				'CRF')
					echo
					info "Setting constant rate factor (CRF, quality-based VBR) for x265"
					msg "——————————————————————————————————————————————————————————————————"
					msg "CRF can be thought of as a bitrate strangling parameter – the more you
					     strangle (i.e. the higher is the CRF value), the poorer quality you get.
					     ${__bri}28 is where the middle ground is:${__s} it’s the encoder default, which in
					     most cases would produce a file, that is visually identical to the source
					     (no noticeable artefacts during playback), and having lesser file size
					     than the source (provided that the source had some quality to lose on
					     compression). The default is decent enough, no reason to not try it,
					     when in doubt. ${__bri}CRF lower than 26${__s} will look even better, but there may be
					     a risk to get a bigger file, than the source (if you don’t downscale,
					     or don’t speed the video up, or if the video is just rich in motions).
					     ${__g}CRF in range 19–23 is for when you save art${__s} and want to stay close to loss-
					     less. ${__bri}${__c}CRF 15–18 is the madman’s choice${__s} to get the maximum possible quality
					     disregarding the bloat. Consider the latter for the cases, when the source
					     has just really poor quality, so you want to avoid distortion at any cost,
					     or when the source format cannot be kept around (like raw video or an ISO
					     disk image). On the other side, ${__y}the range 29–35${__s} will save you even more
					     space at the cost of quality (the video may have some noticeable blur
					     or compression artefacts)."
					echo

					msg "Hint:  0–14  ${__bri}${__c}15–18${__s}  ${__g}19–23${__s}  ${__bri}24–28${__s}  ${__y}29–35${__s}  36–51"
					echo

					local crf_options=( {15..35} )

					#  find the currently set option and mark it as default
					if [ -v vid_link[crf] ]; then
						for ((j=0; j<${#crf_options[*]}; j++)); do
							[ "${crf_options[j]}" = "${vid_link[crf]}" ] && {
								crf_options[j]="_${crf_options[j]}_"
								break
							}
						done
					fi

					menu-carousel "Set CRF:"  \
					              "${crf_options[@]}"

					vid_link[crf]="$CHOSEN"
					menu_start_idx=6
					;;

				'Pixel format')
					echo
					info "Setting pixel format for x265 (chroma subsampling and bit depth)"
					msg "————————————————————————————————————————————————————————————————————"
					msg "${__bri}10-bit encodes${__s} provide better colour preservation than ${__bri}8-bit${__s} ones, and
					     this doesn’t affect file size in any significant way (nothing close to
					     bloat). ${__bri}4:2:0 chroma${__s} is the standard for most content around. Using
					     ${__bri}4:4:4${__s} makes sense only when you demand top notch quality and either
					     encode directly from a raw source, or you want to avoid compression
					     artefacts (like banding) at any cost (namely, at the cost of disk
					     space). The conversion from 4:2:0 to 4:4:4 usually produces bloat,
					     but it may be a winning strategy, if you don’t want to lower CRF
					     that much. ${__bri}If preservation of the picture is not a primary${__s}
					     ${__bri}concern,${__s} then choosing yuv420p format will provide faster
					     encoding speed."
					echo

					local pix_fmt_options=( yuv420p
					                        yuv444p
					                        yuv420p10le
					                        yuv444p10le  )

					#  find the currently set option and mark it as default
					if [ -v vid_link[pix_fmt] ]; then
						for ((j=0; j<${#pix_fmt_options[*]}; j++)); do
							[ "${pix_fmt_options[j]}" = "${vid_link[pix_fmt]}" ] && {
								pix_fmt_options[j]="_${pix_fmt_options[j]}_"
								break
							}
						done
					fi

					menu-carousel "Set pixel format:"  \
					              "${pix_fmt_options[@]}"

					vid_link[pix_fmt]="$CHOSEN"
					menu_start_idx=7
					;;

				'Key frame maximum distance')
					echo
					info "Setting key frame interval"
					msg "——————————————————————————————"
					msg "Usually there’s no need to alter the ${__bri}minimum${__s} distance between key frames,
					     as x265 won’t be stuffing them in ASAP. If there’s no real need to insert
					     them (when there’s little motion on the screen) x265 will prefer longest
					     possible distance. So, in terms of saving space on mostly static videos
					     (with little motion), you’d typically want to increase the ${__bri}maximum${__s} dis-
					     tance alone. ${__bri}In case you have a really long video${__s} (like an hour or long-
					     er) then setting the distance to “infinte” may amount to a considerable
					     saving of space (press ${__bri}[${__s}${__g}End${__s}${__bri}]${__s}).
					        Do mind, however, that with infinite distance between key frames you
					     won’t be able to easily rewind five minutes back or skip 20 minutes for-
					     ward: in the absence of key frames the player won’t have quick points of
					     reference to skip in-between frames, and ${__bri}will have to decode everything${__s}
					     from the last known key frame up to the requested point. This may take
					     a really long time, so only apply “infinite” for the stuff you’re not
					     going to rewind."

					echo

					#  Range is between 250 and ≈ the number of frames which
					#  10 minutes of 23.976 fps video would have. (There’s
					#  probably no reason to set it any higher, for infinite
					#  distance, there’s “infinite”.)
					#
					local  kf_max_dist_options=( 250 500 1000 2000 4000 8000 infinite )

					#  find the currently set option and mark it as default
					if [ -v vid_link[kf_max_dist] ]; then
						for ((j=0; j<${#kf_max_dist_options[*]}; j++)); do
							[ "${kf_max_dist_options[j]}" = "${vid_link[kf_max_dist]}" ] && {
								kf_max_dist_options[j]="_${kf_max_dist_options[j]}_"
								break
							}
						done
					fi

					menu-carousel "Set key frame maximum distance:"  \
					              "${kf_max_dist_options[@]}"

					vid_link[kf_max_dist]="$CHOSEN"
					menu_start_idx=8
					;;

				'Audio bit rate')
					echo
					local audio_stream_copy_allowed='copy'
					info "Setting audio bit rate for libopus"
					msg "——————————————————————————————————————"
					msg "There are different requirements for speech and music. ${__bri}Speech in general${__s}
					     requires relatively little bitrate in comparison to music. It may require
					     less than 10 kbps, but that’s where we approach the bounds of legibility.
					     ${__bri}The default here is set at 96k${__s} to retain most qualities of speech and
					     probable music fragments as they are. Bitrates below this bound don’t
					     grate your ear, but the loss of quality is fairly obvious. ${__bri}If your video${__s}
					     ${__bri}has a focus on music,${__s} then average recommended bitrate is 128k. Techni-
					     cally, the lowest border is at 48k, but again, there would be some obvious
					     quality loss. ${__bri}Choosing 192–256k${__s} makes sense if you convert a track with
					     music from a quality source. ${__bri}If the video is to be sped up,${__s} so has to be
					     the audio. And this means, that a sped up video shouldn’t use the lowest
					     audio bit rates, as the quality loss together with contraction may worsen
					     legibility of speech too much. Finally, there’s a ${__bri}“copy”${__s} option, which
					     allows to transfer the original audio stream (or a part of it) as is to
					     the converted video file. (Provided, that the speed remains original.)"
					echo
					[ -v vid_link[speed] ] && [ "${vid_link[speed]}" != '1.00' ] && {
						warn "Copying the original audio ${__y}is not available${__s} when the video
						      is going to be sped up."
						unset audio_stream_copy_allowed
						echo
					}

					local audio_bitrate_options=( 'audio disabled'
					                              {48,64,96,112,128,160,192,224,256}k
					                              ${audio_stream_copy_allowed:-}       )

					#  find the currently set option and mark it as default
					if [ -v vid_link[abitrate] ]; then
						for ((j=0; j<${#audio_bitrate_options[*]}; j++)); do
							[ "${audio_bitrate_options[j]}" = "${vid_link[abitrate]}" ] && {
								audio_bitrate_options[j]="_${audio_bitrate_options[j]}_"
								break
							}
						done
					fi

					menu-carousel "Set audio bit rate:"  \
					              "${audio_bitrate_options[@]}"

					vid_link[abitrate]="$CHOSEN"
					menu_start_idx=9
					;;

				*Confirm*)
					break
					;;

				*Cancel*)
					abort 'Cancelled.'
					;;
			esac
		done
	done

	return 0
}


compose_single_job () {
	declare -g  reserved_output_fpaths

	local  no="$1"
	local  key
	local  output_dir
	local  output_fpath
	local  output_fname
	local  output_fname_shortened
	local  timestamps
	local  ffmpeg_command
	local  x265_params
	local  filter_graph
	local  comment
	local  description
	local  job_fname
	local  job_fname_shortened
	local  job_path
	local  jobs_dir="$postponed_commands_dir"
	local  job_logdir
	local  i
	local  j

	local -n vid_link=vid_$(printf '%03u' "$((no+1))")

	[ ! -v individual_options  -a  $no -gt 0 ]  && {
		for key in ${!vid_001[*]}; do

			[ "$key" = 'path' ]  \
				&& continue

			[ -v vid_001[$key] ]  \
				&& vid_link[$key]="${vid_001[$key]}"
		done
	}

	output_dir="${vid_link[path]%/*}"
	output_fname="${vid_link[path]##*/}"
	output_fname="${output_fname%\.*}"

	# “… 00:00:00.000–00:00:00.000.LAE-01.mkv” = 42 B (“…” and “–” take 3 B)
	# “LAE” stands for “lazy archival encode”

	while (( $( echo -n "$output_fname" | wc --bytes ) > 255-42 )); do
		output_fname="${output_fname%?}"
		output_fname_shortened=t
	done

	[ -v output_fname_shortened ]  \
		&& output_fname+='…'

	if [ -v vid_link[start]  -a  -v vid_link[end] ]; then
		timestamps=" ${vid_link[start]}–${vid_link[end]}"
	else
		timestamps=''
	fi

	for i in {01..99}; do
		output_fpath="$output_dir/$output_fname${timestamps:-}.LAE-$i.mkv"
		for ((j=0; j<${#reserved_output_fpaths[*]}; j++)); do
			[ "$output_fpath" = "${reserved_output_fpaths[j]}" ]  \
				&& continue 2
		done
		[ ! -e "$output_fpath" ] && {
			output_fname="${output_fpath##*/}"
			reserved_output_fpaths+=("$output_fpath")
			break
		}
	done

	ffmpeg_command=(
		$ffmpeg -y  -hide_banner  -v error  -nostdin  -nostats
	)

	[ -v vid_link[start]  -a  -v vid_link[end] ]  \
		&& ffmpeg_command+=( -ss "${vid_link[start]}"
		                     -to "${vid_link[end]}"
		                   )

	ffmpeg_command+=( -i "${vid_link[path]}"
	                  -pix_fmt "${vid_link[pix_fmt]}"
	                  -r "${vid_link[frame_rate]}"
	                  -c:v libx265
	                  -b:v 0
	                  -crf "${vid_link[crf]}"
	                  -preset:v veryslow
	)

	x265_params="log-level=none"   #  This is to prevent x265 from writing to
	                               #  stdout (“ffmpeg -v error” alone cannot
	                               #  silence it).

	if [ "${vid_link[kf_max_dist]:-}" = 'infinite' ]; then
		x265_params+=':keyint=-1'

	elif [[ "${vid_link[kf_max_dist]:-}" =~ ^$INT$ ]]; then
		x265_params+=":keyint=${vid_link[kf_max_dist]}"
	else
		: 'Nothing to do, the default of 250 will be applied.'
	fi

	ffmpeg_command+=( -x265-params "$x265_params" )

	if [ "${vid_link[abitrate]}" = 'copy' ]; then
		ffmpeg_command+=( -c:a copy )

	elif [ "${vid_link[abitrate]}" = 'audio disabled' ]; then
		ffmpeg_command+=( -an )

	else
		ffmpeg_command+=(
			-c:a libopus
			-b:a "${vid_link[abitrate]}"
			-vbr on
			-ac 2
			-cutoff 20000
		)
	fi

	if	(	[ -v vid_link[speed] ]  \
			|| [ -v vid_link[scale] ]  \
			|| [ -v vid_link[crop] ]
		)
	then
		#  Must be set after the filter chain is combined, as non-empty
		#  “$filter_graph” indicates, that there’s a filter applied.
		#
		#filter_graph="[0:V] "

		[ -v vid_link[crop] ]  \
			&& filter_graph+="crop=${vid_link[crop]}"

		[ -v vid_link[scale] ] && {
			filter_graph="${filter_graph:+$filter_graph,}"
			filter_graph+="scale=-2:${vid_link[scale]}"
		}

		[ -v vid_link[speed] ]  && {
			filter_graph="${filter_graph:+$filter_graph,}"
			filter_graph+="setpts=1/${vid_link[speed]}*PTS"

			[ "${vid_link[abitrate]:-}" != 'audio disabled' ]  \
				&& filter_graph+="; [0:a] atempo=${vid_link[speed]}"  # <- new chain!
		}

		ffmpeg_command+=( -filter_complex "[0:V] $filter_graph" )
	fi

	ffmpeg_command+=( -sn
	                  -map_metadata -1
	                  -metadata "title=${output_fname%${timestamps:-}\.LAE-??\.mkv}"
	)

	comment="Converted with Lazily archive video v$version / "

	[ -v vid_link[start]  -a  -v vid_link[end] ]  \
		&& comment+="${vid_link[start]}–${vid_link[end]} / "

	comment+="video: libx265, "

	[ -v vid_link[scale] ]  \
		&& comment+="scale: ${vid_link[scale]}p, "

	[ -v vid_link[crop] ]  \
		&& comment+="crop: ${vid_link[crop]}, "

	comment+="crf: ${vid_link[crf]}, "

	comment+="pix_fmt: ${vid_link[pix_fmt]}, "

	[ -v vid_link[frame_rate_altered] ]  \
		&& comment+="frame_rate: ${vid_link[frame_rate]}, "

	if [ "${vid_link[abitrate]}" = 'copy' ]; then
		comment="${comment%, }; "
		comment+="audio: stream copied, "
	else
		comment="${comment%, }; "
		comment+="audio: libopus, "
		comment+="audio bitrate: ${vid_link[abitrate]}, "
	fi

	[ -v vid_link[speed] ] && {
		comment="${comment%\, }; "
		comment+="speed: ×${vid_link[speed]}, "
	}

	ffmpeg_command+=( -metadata "comment=${comment%\, }" )

	description="${vid_link[path]##*/}"
	description="${description%\.*}"
	ffmpeg_command+=( -metadata "description=$description" )

	ffmpeg_command+=( "$output_fpath" )

	info "Saving the command for video no. $((no+1)) as a job file."
	#
	#  The file name for the job file
	#
	#  There’s little reason to bother with “smart” shortening like
	#  for the real file to be encoded, as for the job it’s the
	#  “.XXXXXXXX” extension that uniquely defines the file. The original
	#  file name, if needed, is always present in the comment at the top.
	#
	#  It’s shortened to 215 bytes (may be less, but not more), as the
	#  standard length of the tail – a space, two timestamps, an exten-
	#  sion – takes 40 bytes. This length doesn’t change. “While” and not
	#  “…:offset:length”, because of multibyte characters.
	#
	job_fname="${vid_link[path]##*/}"
	while (( $( echo -n "$job_fname" | wc --bytes ) > 212 )); do
		job_fname="${job_fname%?}"
		job_fname_shortened=t
	done
	[ -v job_fname_shortened ]  \
		&& job_fname+='…'

	[ -d "$jobs_dir" ] || mkdir "$jobs_dir"
	job_path="$jobs_dir"

	job_path+="/$(
		mktemp -u "$job_fname${timestamps:-}.XXXXXXXX"
	).sh"
	job_logdir="$jobs_dir/${job_path: -11: 8}.log"
	cat <<-EOF >"$job_path"
	#! /usr/bin/env bash

	#  Lazily_archive_video.sh job file
	#  ${job_path##*/}

	FFREPORT="file=$job_logdir/${job_fname//:/\\:}.ffmpeg.log:level=32"  \\
	EOF
	chmod +x "$job_path"
	for ((i=0; i<${#ffmpeg_command[*]}; i++)); do
		case "$i" in
			0)
				printf '%q  \\\n\t\\\n' "${ffmpeg_command[i]}"  >>"$job_path"
				;;

			$(( ${#ffmpeg_command[*]} -1 ))  )
				printf '\t%q  \\\n' "${ffmpeg_command[i]}"  >>"$job_path"
				;;

			*)
				if [[ "${ffmpeg_command[i]}" =~ ^- ]]; then
					printf '\t%q' "${ffmpeg_command[i]}"  >>"$job_path"
					(( i < ${#ffmpeg_command[*]} - 1 ))  \
						&& [[ "${ffmpeg_command[i+1]}" =~ ^- ]]  \
						&& echo -en '  \\\n' >>"$job_path"
				else
					printf ' %q  \\\n' "${ffmpeg_command[i]}"  >>"$job_path"
				fi
		esac
	done

	milinc
	msg "${__blue}${job_path##*/}${__s}"
	mildec

	return 0
}


compose_jobs() {
	#
	#  As an input file may be specified twice on the command line (to make
	#  two different cuts, or to try encoding with different options and
	#  save the file which turns out best), we need to protect the “LAE-NN”
	#  identifier from being assigned again (so that ffmpeg wouldn’t end up
	#  overwriting the file or throw an error).
	#
	declare -ga  reserved_output_fpaths=()

	local i

	for ((i=0; i<${#ARGS[*]}; i++)); do
		compose_single_job "$i"
	done
	return 0
}


run_now_or_postpone() {
	info "Jobs prepared."
	menu-bivar "Launch them now?" "_Launch now_" "Postpone"
	[ "$CHOSEN" = 'Postpone' ] && {
		collect_jobs
		exit 0
	}
	return 0
}



check_for_deprecated_folders
validate_args
[ ! -v only_run_jobs ] && {
	check_for_multiple_args
	set_encoding_options
	compose_jobs
	run_now_or_postpone
}
collect_jobs
run_jobs


exit 0