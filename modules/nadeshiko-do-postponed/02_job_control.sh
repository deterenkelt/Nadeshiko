#  Should be sourced.

#  02_job_control.sh
#  Nadeshiko-do-postponed module that collects jobs postponed for later
#  by Nadeshiko-mpv and launches them.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko-do_postponed.sh
#


jobs_to_process=0
completed_jobs=0
failed_jobs=0
failed_jobs_at_init=0
launched_jobs=0

declare -gr cpu_count=$(grep -c processor /proc/cpuinfo)
[[ "$cpu_count" =~ ^([1-9]|[1-9][0-9]+)$ ]]  \
	|| err 'Cannot detect the number of processors.'


 # Target process name for CPU affinity adjustments
#
#  It would be better to read Nadeshiko’s rc file to retrieve what $ffmpeg
#  there expands to. But that’s another trouble, solving which is not a ne-
#  cessity, at least until a bug report appears.
#
declare -gx target_command='ffmpeg'


 # The self-report channel isn’t given any functions as items, because it’s
#  desirable to keep the flexibility of a “bare” channel:
#    - the debugging output has some customly shaped lines desribing PIDs
#        and processes, so it’s more convenient to use “echo” or heredoc
#        with info() and $v_level;
#    - the coprocess would have trouble to operate on the main shell’s
#        FUNCNAME thus checking $v_level is the only way there.
#
vchan setup  name=Job_control  \
             level=2  \
             meta=M  \
             min_level=2  \
             max_level=4

 # log_flooded_output is used to discern two forms of level 3 messages: the
#  simple form is used in regular UI, that user sees everyday; the complex
#  form adjusts the text and appearance of these messages, when the amount
#  of things shown in the UI rises. Without that, short regular messages may
#  give a confusing impression about what they’re referring to.
#
(( "${VERBOSITY_CHANNELS[Job_control]}" > 2 ))  \
	&& log_flooded_output=t


 # Counts jobs and sets counters.
#
collect_jobs() {
	if [ -d "$postponed_commands_dir" ]; then
		while IFS='' read -r -d ''; do
			let '++jobs_to_process'
		done < <( find "$postponed_commands_dir" -maxdepth 1  \
		               -type f -print0 )
	fi

	if [ -d "$failed_jobs_dir" ]; then
		while IFS='' read -r -d ''; do
			let '++failed_jobs_at_init'
		done < <( find "$failed_jobs_dir" -maxdepth 1 -iname "*.sh"  \
		               -type f -print0 )
	fi

	if (( failed_jobs_at_init == 0 )); then
		_failed_jobs='0'
	else
		_failed_jobs="${__bri}${__y}$failed_jobs_at_init${__s}"
	fi

	info "Jobs count
	      ─────────────────
	      to run: $jobs_to_process
	      in failed: $_failed_jobs"

	return 0
}


load_decrease() {
	declare -g  old_value=$processors

	processors=$(( processors > 0 ? --processors : 0 ))
	(( processors == old_value ))  \
		&& return 0  \
		|| set_load

	return 0
}


load_increase() {
	local old_value=$processors

	processors=$(( processors < cpu_count ? ++processors : cpu_count ))
	(( processors == old_value ))  \
		&& return 0  \
		|| set_load

	return 0
}


 # Creates a random list of processors so as to heat different parts
#  of the CPU on each run (or when the number of selected processors
#  for the work is changed).
#
#  $1 – the number of *processors* (cores or threads) to be enabled. The cpu
#       list for “taskset” will have exactly $1 items. Must be between 0 and
#       $cpu_count. (0 is a convenience synonym for 1, because technically
#       a running program can’t obviously be deprived of all CPUs.)
#
#  The numbers in the generated list will be in range 0…$cpu_count
#
get_a_random_cpulist_of() {
	local  max_cpus=${1:-}
	local  list=( $((RANDOM % cpu_count)) )  # item will be in range 0…count-1
	local  item
	local  i

	if	   [[ "$max_cpus" =~ ^$INT$ ]]  \
		&& (( max_cpus <= cpu_count ))
	then
		case "$processors" in
			0|1)
				:
				;;

			$cpu_count)
				eval list=( {0..$((cpu_count-1))} )
				;;

			*)
				until (( ${#list[*]} == max_cpus )); do
					item=$((RANDOM % cpu_count))
					for ((i=0; i<${#list[*]}; i++)); do
						(( ${list[i]} == item ))  \
							&& continue 2
					done
					list+=( $item )
				done
				;;
		esac

	else
		err "$FUNCNAME: argument must be a number in range 1…$cpu_count."
	fi

	echo "$(IFS=','; echo "${list[*]}")"

	return 0
}


set_load() {
	local  unfreeze=t
	local  v_level=${VERBOSITY_CHANNELS[Job_control]}
	local  taskset_redir='/dev/null'
	local  ffmpeg_pid
	local  cpu_affinity
	local  dbg_spacing_lines

	#  Tardy keypress may come when the job has already quit. Trying to send
	#  a signal with pkill then would be meaningless and will trigger an error.
	#
	[ -e /proc/$job_pid ] || return 0

	#  Depending on what route this function was called from,
	#  echo one or two newlines (to separate the debugging output
	#  from the regular output). One newline, when set_load is called
	#  directly after the job is launched (i.e. after an “info” message).
	#  Two newlines, when it’s called later from load_(in|de)crease, that is,
	#  when the user adjusts affinity.
	#
	[[ "${FUNCNAME[*]}" =~ \ load_(in|de)crease\  ]]  \
		&& dbg_spacing_lines='\n\n'  \
		|| dbg_spacing_lines='\n'

	#  Freeze the current job’s process group for the time we’ll be applying
	#  CPU affinity
	pkill -SIGSTOP -g "$job_pid"

	(( v_level >= 4 ))  \
		&& unset taskset_redir

	#  FFmpeg might or might not have been launched yet, depending on the
	#  point in time.
	#
	ffmpeg_pid=$(pgrep -g $job_pid "$target_command") || unset ffmpeg_pid

	 # As the backend spawns ffmpeg processes several times, it’s also
	#  required to set CPU affinity for the backend too. But unlike with
	#  ffmpeg, to determine the PID of nadeshiko.sh is problematic with pgrep,
	#  as there will be two processes (one created by “env”), and taskset
	#  can work only with one PID per command. So the backend PID is deter-
	#  mined as the parent of $ffmpeg_pid process.
	#
	#backend_pid=$(sed -rn 's/^PPid:\s*([0-9]+)\s*$/\1/p' /proc/$ffmpeg_pid/status)

	case $processors in
		0)
			enc_mode="stopped"
			unset unfreeze
			;;

		1)
			enc_mode="on 1/$cpu_count CPUs"
			cpu_affinity=$(( RANDOM % cpu_count ))
			;;

		$cpu_count)
			enc_mode="on all CPUs"
			cpu_affinity="0-$((cpu_count-1))"
			;;

		*)
			enc_mode="on $processors/$cpu_count CPUs"
			cpu_affinity=$(get_a_random_cpulist_of "$processors")
			;;
	esac

	[ -v unfreeze ] && {
		(( v_level >= 4 ))  && {
			echo -en "$dbg_spacing_lines"
			info "Setting CPU affinity for the backend:"
		}

		taskset -pac "$cpu_affinity" "$backend_pid" >${taskset_redir:-1}

		[ -v ffmpeg_pid ] && {
			(( v_level >= 4 ))  \
				&& info "Setting CPU affinity for the ffmpeg process"

			taskset -pac "$cpu_affinity"  "$ffmpeg_pid" >${taskset_redir:-1}
		}

		pkill -SIGCONT -g "$job_pid"
	}

	(( v_level >= 4 )) && {
		cat <<-EOF
		 
		PIDs:  Job      $job_pid
		       Backend  $backend_pid
		       ffmpeg   ${ffmpeg_pid:-not running atm}
		 
		EOF
		pstree -p $$
		echo
	}

	return 0
}


 # Process $postponed_commands_dir
#
process_dir_jobs() {
	declare -gx  job_file
	declare -gx  job_logdir

	declare -g  launched_jobs
	declare -g  failed_jobs
	declare -g  failed_jobs_at_init
	declare -g  completed_jobs
	declare -g  nadeshiko_verbosity
	declare -g  job_pid       #  Technically, these two may be local, they’re
	declare -g  backend_pid   #  declared as global just in case they’ll be
	                          #  needed in another function (e.g. on_exit).

	local  coproc_pid
	local  rest
	local  arrow_up=$'\e[A'
	local  arrow_right=$'\e[C'
	local  arrow_down=$'\e[B'
	local  arrow_left=$'\e[D'
	local  end_key=$'\e[8~'         #  The escape sequence depends on the stan-
	local  alter_end_key=$'\e[4~'   #  dard the terminal emulator is following.
	local  self_interrupted

	local -x  v_level=${VERBOSITY_CHANNELS[Job_control]}
	local -x  main_shell_input="/proc/$$/fd/0"
	local -x  handled_interruption_marker_file="$TMPDIR/handled_interruption"

	local  msg
	local  info_func
	local  info_message
	local  read_prompt
	local  _completed
	local  _jobs

	[ -v CONNECTED_TO_TERMINAL  -a  -v show_control_keys_prompt ] && {
		echo
		info "Control keys:
		      ${__bri}↑${__s}, ${__bri}→${__s} or ${__bri}>${__s} – more cores
		      ${__bri}↓${__s}, ${__bri}←${__s} or ${__bri}<${__s} – less cores
		       ${__bri}i${__s} or ${__bri}End${__s} – to interrupt. (The job file will be awaiting
		                  for the next launch, together with the jobs,
		                  which were further in the list.)"
	}

	(( processors == 0 ))  \
		&& enc_mode="stopped"  \
		|| enc_mode="on $processors/$cpu_count CPUs"


	info_job_desc() {
		local _msg

		_msg="Launching job "
		_msg+="${__bri}${__g}$((launched_jobs+1))${__s}"
		_msg+="/$jobs_to_process, "
		_msg+="id: ${__bri}$job_id${__s}"
		#  Need an echo to jump on the next line after ↑↓ key press.
		#  But coproc redirections will suppress an ordinary echo.
		msg ''
		info "$_msg"

		return 0
	}
	export -f info_job_desc


	info_job_complete() {
		msg ''
		if [ -v log_flooded_output ]; then
			info "Job id ${__bri}$job_id ${__g}complete.${__s}"
		else
			msg "  ${__g}Complete${__s}"
		fi

		return 0
	}
	export -f info_job_complete


	info_job_failed() {
		msg ''
		if [ -v log_flooded_output ]; then
			info "Job ID ${__bri}$job_id ${__r}failed.${__s}"
		else
			msg "  ${__r}Failed${__s}"
		fi

		return 0
	}
	export -f info_job_failed


	while IFS= read -r -d '' jobfile; do
		if [[ "${jobfile##*/}" =~ \.(.{8})\.sh$ ]]; then
			job_id="${BASH_REMATCH[1]}"
			job_logdir="$postponed_commands_dir/$job_id.log"
			[ -d "job_logdir" ] && rm -rf "$job_logdir"
			mkdir "$job_logdir"
		else
			err "Cannot determine job id.
			     Invalid job id in file “${jobfile##*/}”."
		fi

		info_job_desc

		 # Jobs are launched in a coprocess, because if they were simply to be
		#  spawned with &, their exit status would be gone. And we need it to
		#  move the logs and the job file either to “completed” or to “failed”
		#  subdirectory (or to delete the logs and leave the job as is, if the
		#  batch sequence is interrupted by ourselves).
		#
		coproc {
			#  Make the job process start its own process group
			#  (the future $job_pid).
			set -m

			declare -g  job_completed_successfully

			export coproc_pid=$BASHPID

			 # Moving the failed job to subdirectory in this trap also helps
			#  to guarantee, that the coprocess’s existence would last long
			#  enough to avoid a situation, when both the “coproc” here and
			#  the on_exit() in nadeshiko-do-postponed.sh would simultaneously
			#  attempt to move the log folder. (The call to abort() in the
			#  outer shell is postponed while this coproc’s PID exists.)
			#
			coproc_on_exit_trap() {
				if [ -v job_completed_successfully ]; then
					#
					#  To output $job_id is important to know which exactly
					#  job received the interruption signal. (In cases when
					#  the signal came when one job was quitting and the other
					#  just launched)
					#
					(( v_level >= 3 ))  \
						&& info "${FUNCNAME[0]}: Job $job_id completed successfully."

				else
					(( v_level >= 3 ))  \
						&& info "${FUNCNAME[0]}: Detected a failed or terminated job: $job_id."

					milinc

					(( v_level >= 3 ))  \
						&& msg "Waiting for ffmpeg to quit."

					#  Terminate ffmpeg, but wait until it its process will be
					#  gone from memory to safely move the logs.
					pkill -9 -g $$ "$target_command" || true
					until ! pgrep -g $$ "$target_command"; do
						sleep 1
					done

					mildec

					if [ -e "$handled_interruption_marker_file" ]; then
						#  As it’s not like the job failed because of the encoding
						#  backend, keep the job file together with the rest of
						#  postponed jobs and wipe the logs – they’re meaningless.
						#
						(( v_level >= 3 ))  \
							&& msg "Wiping logdir for the interrupted job."

						extract_and_apply_scene_complexity "$jobfile" "$job_logdir"
						rm -rf "$job_logdir"

					else
						(( v_level >= 3 ))  \
							&& msg "Moving the job file and its logdir to “failed”."

						info_job_failed
						move_job_to_failed "$jobfile" "$job_logdir"

						(( v_level >= 3 ))  \
							&& msg 'Complete.'
					fi

				fi

				#  Winding up the keyboard reading cycle, so that it
				#  wouldn’t hang without a timeout (see below). “Disown”
				#  is a precaution measure to make sure, that /this/
				#  process could be free of ties and could finish
				#  in the due time.
				#
				[ -v CONNECTED_TO_TERMINAL ] && {
					{
						#  Wait for the coproc to finish analysing the job file,
						#  saving its scene complexity result (if that stage was
						#  complete) and moving the job file.
						#
						while [ -d "/proc/${coproc_pid:-erm…}" ]; do
							sleep 1
						done
						#
						#  Here we could specifically wait for the “read” process
						#  to spawn in the main loop, but when the last job ends,
						#  it won’t be spawned, and waiting will only make proces-
						#  ses stale and prevent a new instance of nade-do-post
						#  to run.
						#
						#until pgrep -g $$ -P $$ wind_me_up >/dev/null; do
						#	sleep 1
						#done
						#
						#  So, just wait for the current job to end, wait two
						#  seconds more for bash to go to the next loop – if
						#  there will be one – and then send the signal
						#  UDP-style.
						#
						sleep 2
						pkill -TERM -g $$ -P $$ wind_me_up || true
					} &
					disown
				}

				return 0
			}
			trap 'coproc_on_exit_trap >&2' EXIT


			if	env  \
					LOGDIR="$job_logdir"  \
					VERBOSITY=$nadeshiko_verbosity  \
					MSG_INDENTATION_LEVEL="$MSG_INDENTATION_LEVEL"  \
					${nice_cmd:-}  \
					taskset -ac $(get_a_random_cpulist_of $processors)  \
					"$jobfile"
			then
				info_job_complete
				[ -d "$failed_jobs_dir/${job_logdir##*/}" ]  \
					&& rm -rf "$failed_jobs_dir/${job_logdir##*/}"
				move_job_to_completed "$jobfile" "$job_logdir"
				job_completed_successfully=t
				#  Can’t use counter for jobs in coproc

			else
				#  corpoc_on_exit_trap handles both “naturally happenning”
				#  and the quit after the outer shell sends SIGTERM
				:

				#  Can’t use counter for failed jobs, as there may be
				#  some jobs left from the previous run.
			fi
		}

		let '++launched_jobs'

		coproc_pid=$!
		if [ "$JOB_CONTROL_MODE" = 'Nadeshiko' ]; then
			until	job_pid=$(pgrep -P $coproc_pid -f 'bash.*/postponed_commands_dir/.*\.sh')  \
					&& backend_pid=$(pgrep -P $job_pid -f 'bash.*nadeshiko\.sh')  \
					|| [ ! -e /proc/$coproc_pid ]
			do
				sleep 1
			done

		elif [ "$JOB_CONTROL_MODE" = 'Lazily archive video' ]; then
			#
			#  In this mode Nadeshiko isn’t called, so “$backend_pid” refers
			#  directly to ffmpeg. “$ffmpeg_pid” is not necessary.
			#
			until	job_pid=$(pgrep -P $coproc_pid -f 'bash.*lazily_archive_video/jobs/.*\.sh')  \
					&& backend_pid=$(pgrep -P $job_pid -f 'ffmpeg')  \
					|| [ ! -e /proc/$coproc_pid ]
			do
				sleep 1
			done
		fi

		[ -e /proc/$coproc_pid ] || {
			warn "Coprocess has exited without spawning the postponed job."
			continue
		}


		 # Post-factum setting is needed to avoid the situation when a stopped
		#  job was terminated (OOM-killed or terminated by user), and a new
		#  job spawns and uses 1 or more processors, when it should be stopped.
		#
		set_load

		while [ -e /proc/$job_pid ]; do
			if [ ! -v CONNECTED_TO_TERMINAL ]; then
				sleep 1
			else
				read_prompt="${__clearline}  "
				[ -v log_flooded_output ] && {
					read_prompt+="$(nth $((launched_jobs+1))) job "
					read_prompt+="(id: ${__bri}$job_id${__s}). "
				}
				read_prompt+="Encoding $enc_mode. ${__bri}↑↓${__s} to control, "
				read_prompt+="${__bri}i${__s} or ${__bri}End${__s} to interrupt "
				read_prompt+="${__g}>${__s} "

				#  Behold, this is ancient magic!
				#
				#  There’s an intricacy to this “while” cycle and the call to
				#  “read” in it. Before we get down to the way things are done
				#  here, let’s look into the basic version of it. The batch
				#  starts, coproc spawned a job, and now it’s turn of this
				#  cycle. It must use “read” with “-t1” (a timeout of 1 second)
				#  so that “read” would check the input and if there’s any,
				#  then process it. Basically, the timeout for “read” deter-
				#  mines the iteration speed in the “while” cycle. It would be
				#  okay, if not for one circumstance: “read” has a prompt and
				#  with each iteration (every 1 second) writes to console –
				#  and to the log file! Considering that Nade-do-post may run
				#  for a couple of days, processing jobs, this will be a lot
				#  of writes. The filesystem cache in RAM somewhat alleviates
				#  the problem by delaying an actual write to the hardware
				#  until the cache will be full, but there still would be 3–7
				#  extra MiB per day of writes. That’s not tremendously bad,
				#  but this isn’t healthy either. So, there had to be found
				#  a way to avoid extra writes and, preferably, not worsen the
				#  user experience: the response from pressed keys should re-
				#  main precise and promptly.
				#
				#  The biggest problem was that if we remove the timeout from
				#  “read”, this cycle will be stuck on waiting for user input.
				#  Because the next iteration cannot start, until this “read”
				#  would finish. Without a timeout only a key pressed by the
				#  user would propel this cycle into the new iteration. And
				#  every next job would require the user to press a key just
				#  to let the “read” command complete.
				#
				#  As processes’ stdin and stdout aren’t pipes, writing to
				#  them is meaningless. Some success was achieved with
				#     { while :; do echo N; sleep 5; } | read …
				#  but it couldn’t read simultaneously from both the pipe and
				#  /dev/tty. In the end, it was solved with subprocesses again.
				#
				#  The “read” builtin must be called in a subshell – when it’s
				#  running in a separate process, it could be found and termi-
				#  nated. This can propel the cycle to the next iteration with-
				#  out the need for a (short) timeout to “read”. This mechan-
				#  ism resembles winding up a clock, so the subprocess got the
				#  name “wind_me_up”. By that name it is found later in the
				#  trap on EXIT signal in the coprocess. And thus this cycle
				#  gets “wound up”.
				#
				REPLY=$(
					echo -n "wind_me_up" >/proc/$BASHPID/comm
					read -s -n 1 -p "$read_prompt" </dev/tty || true
					echo -n "$REPLY"
				) || true

				[ "$REPLY" = $'\e' ]  \
					&& read -s -n2 rest </dev/tty  \
					&& REPLY+="$rest"

				[[ "$REPLY" =~ ^$'\e['[48]$ ]]  \
					&& read -s -n1 rest </dev/tty  \
					&& REPLY+="$rest"


				if [ "$REPLY" ]; then
					case "$REPLY" in
						"$arrow_left"|"$arrow_down"|',')
							load_decrease;;

						"$arrow_right"|"$arrow_up"|'.')
							load_increase;;

						"$end_key"|"$alter_end_key"|'i')
							#  Break from the line with the prompt.
							echo
							#
							#  Coprocess is able to tell between the “success-
							#  ful” and the “failed” status of the job, but it
							#  won’t be able to determine, whether the job
							#  exited because of an error in its logic, or
							#  it was termination from the outside.
							#
							#  This could indeed be done with signals. But
							#  all the clear and useful data is gathered out-
							#  side of the coprocess, and besides that – you
							#  don’t want to deal with this matrioshka of sub-
							#  shells and control it with signals.
							#
							touch "$handled_interruption_marker_file"
							#
							#  NB: sending -TERM /to the job itself(!)/ – to
							#  the shell executing the job file, not to the
							#  shell which is the coprocess.
							#
							pkill -SIGTERM -g "$job_pid"
							#
							#  Give coproc_on_exit_trap some time
							#  to move logs safely
							#
							sleep 2

							self_interrupted=t
							break 2
							;;
					esac
				fi
			fi
		done

		sleep 2  # give coproc_on_exit_trap some time to move logs safely

	done < <( find "$postponed_commands_dir"  -maxdepth 1  -type f  -print0 )


	if [ -d "$failed_jobs_dir" ]; then
		while IFS='' read -r -d ''; do
			let '++failed_jobs,  1'
		done < <( find "$failed_jobs_dir" -maxdepth 1 -iname "*.sh"  \
		               -type f -print0 )
	fi

	(( failed_jobs == 0 )) && {
		#  If there are no .sh files, then what’s left are the .log folders,
		#  leftovers from failed runs from before, basically, stale files.
		rm -rf "$failed_jobs_dir"
	}
	completed_jobs=$(( launched_jobs - (failed_jobs - failed_jobs_at_init)
	                                   ${self_interrupted:+ - 1}  ))

	(( launched_jobs > 0 )) && {
		(( launched_jobs == 1  &&  nadeshiko_desktop_notifications == 3 ))  \
			&& info_func='info'

		echo

		_jobs="job$(plur_sing $jobs_to_process)"

		info_message="$launched_jobs/$jobs_to_process $_jobs launched"
		(( completed_jobs == launched_jobs ))  \
			&& _completed=' – all'  \
			|| _completed=", $completed_jobs"
		info_message+="$_completed finished successfully"
		[ -v self_interrupted ]  \
			&& info_message+=", 1 was interrupted"
		info_message+='.'
		if (( failed_jobs != 0 )); then
			_jobs="job$(plur_sing $failed_jobs)"
			_reside="reside$(plur_sing_v $failed_jobs)"
			info_message+=$'\n'"  ${__yel}$failed_jobs${__s} $_jobs $_reside in the “${__yel}failed${__s}” directory,"
			info_message+=$'\n'"  of which ${__yel}$failed_jobs_at_init${__s} remained from previous runs."
		fi
		info "$info_message"

		if [ -v self_interrupted ]; then
			: # nothing to do, the abort() later will be showing a desktop message.

		elif (( failed_jobs == 0  &&  launched_jobs == completed_jobs)); then
			${info_func:-info-ns} 'All jobs done. All clean.'

		else
			${info_func:-info-ns} 'All jobs processed. There are failed jobs.'
		fi

	}

	[ -v self_interrupted ] && {
		while [ -e /proc/$job_pid ]; do
			sleep 1
		done
		abort 'Interrupted.
		       (ffmpeg may linger for some time to gather up what has been
		       processed and write it to file, please wait.)'
	}

	return 0
}


move_job_to_completed() {
	local  jobfile="$1"
	local  job_logdir="$2"

	if [ -d "$completed_jobs_dir" ]; then
		#
		#  Just a precaution.
		#
		[ -d "$completed_jobs_dir/${job_logdir##*/}" ]  \
			&& rm -rf "$completed_jobs_dir/${job_logdir##*/}"
	else
		mkdir "$completed_jobs_dir"
	fi

	mv "$jobfile"    "$completed_jobs_dir/"
	mv "$job_logdir" "$completed_jobs_dir/"

	return 0
}
export -f move_job_to_completed   #  For coproc


move_job_to_failed() {
	local  jobfile="$1"
	local  job_logdir="$2"

	milinc
	extract_and_apply_scene_complexity "$jobfile" "$job_logdir"

	if [ -d "$failed_jobs_dir" ]; then
		#
		#  Remove stale directory with logs (supposing that the user has moved
		#  the job file back to postponed jobs to run it with the next batch,
		#  but left the directory with old logs in “failed”.)
		#
		[ -d "$failed_jobs_dir/${job_logdir##*/}" ]  \
			&& rm -rf "$failed_jobs_dir/${job_logdir##*/}"
	else
		mkdir "$failed_jobs_dir"
	fi

	mv "$jobfile"    "$failed_jobs_dir/"
	mv "$job_logdir" "$failed_jobs_dir/"

	mildec
	return 0
}
export -f move_job_to_failed   #  For coproc


 # Try to save some time on the future encoding of the same job by reading
#  the scene complexity value from the log and injecting it as an additional
#  option to the job file.
#
#  The scene complexity check may take minutes, sometimes, tens of minutes
#  (especially with videos that require and trigger full decoding, that is,
#  from the PTS 0, which mostly happens on raw bluray disks).
#
#  Indeed, this optimisation will only work if the error, that made the
#  job fail, occurred *after* the check was done.
#
#  $1 – absolute path to the postponed job file.
#  $2 – the directory, which was used to store the logs of that job.
#
extract_and_apply_scene_complexity() {
	[ "$JOB_CONTROL_MODE" = 'Lazily archive video' ] && return 0
	local  jobfile="$1"
	local  job_logdir="$2"
	local  logfile
	local  scenecomp_value

	info "Checking, if we can save time on scene complexity check in the future…"
	milinc

	if grep -qE ".*force_scene_complexity=.*" "$jobfile"; then
		msg "The job already has it forcibly set, nothing to do."
	else
		msg "Checking the log file…"
		milinc
		logfile=$(
			export LOGDIR="$job_logdir"
			get_lastlog_path 'nadeshiko' || true
		)
		if [ -r "$logfile" ]; then
			scenecomp_value=$(
				sed -rn 's/.*Scene complexity: (static|dynamic).*/\1/p'  "$logfile"
			)
			if [ "$scenecomp_value" ]; then
				msg "Scene complexity was detected as “$scenecomp_value”.
				     Setting it to the job file…"
				sed -ri "$ s/(.*)/&\n\tforce_scene_complexity=$scenecomp_value  \\\\/"  \
				    "$jobfile"
				msg "Done!"
				mildec
			else
				denied "Error seems to have happened earlier than the scenecomp check ran."
				mildec
			fi
		else
			mildec
			denied "No log file?.."
		fi
	fi

	mildec
	return 0
}
export -f  extract_and_apply_scene_complexity  # for coproc


run_jobs() {
	local c

	[ -r /sys/devices/system/cpu/smt/active ] && {
		[ "$(</sys/devices/system/cpu/smt/active)" = 1 ]  \
			&& warn 'HyperThreading may slow down the encode.'
	}

	[ -d "$completed_jobs_dir" ] && {
		info "Wiping the logs of completed jobs from the previous batch…"
		rm -rf "$completed_jobs_dir"
	}

	noglob_off
	ls -d "$postponed_commands_dir/"*.log &>/dev/null && {
		info "Wiping leftover logs from an interrupted job…"
		rm -rf "$postponed_commands_dir/"*.log
	}
	noglob_on

	[ -d "$postponed_commands_dir" ] && process_dir_jobs

	return 0
}


return 0