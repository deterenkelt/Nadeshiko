#  Should be sourced.

#  00_show_help_and_legal_info.sh
#  Nadeshiko-do-postponed module to output help and legal use information.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


show_help() {
	local b="${__bright}"
	local g="${__green}"
	local w="${__white}"
	local bg="${__bright}${__green}"
	local s="${__stop}"

	 # Use this line instead to check that the output fits 78 columns.
	#
	#  When restoring the $SHOW_HELP_PAGER line, there should be an empty
	#  line left between it and the header of the manual page.
	#
	#cat <<-EOF |& ansifilter |& grep -nE '^.{79,}$' || info 'All fits!'
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF
	   
	               ${__bri}${__blue}Nadeshiko-do-postponed usage and list of options${s}

	Usage

	  ${b}./nadeshiko-do-postponed.sh${s}  [${b}options${s}]


	Workflow options

	  <${bg}config_file${s}>
	      Chooses an alternative configuration file. May be a file name alone
	      or a path.
	      There are several requirements:
	        - it must be an existing file in ~/.config/nadeshiko/;
	        - the file name must be in format
	          “$MYNAME_NOEXT${bg}suffix${s}.rc.sh”;
	      The ${bg}suffix${s} may consist only of these characters:
	          ${b}A-Z a-z 0-9 . , : ; _ -${s}
	      Presets are a useful feature, read about how to use them on the wiki
	      (see the link at the bottom).


	Verbosity options

	  ${b}--verbosity${s} <${bg}channelA${w}=${g}level1${w},${g}channelB${w}=${g}level2${w},${s}…${b},${g}channelN${w}=${g}levelM${s}>
	      Sets verbosity for the specified channels. For the names and the
	      possible levels refer to the list of channels (see below). The
	      “Troubleshooting guide” on the wiki has a section, that describes,
	      when this option comes helpful.

	      This option and the ${b}VERBOSITY${s} environment variable accept
	      the same value.

	  ${b}--verbosity-list-channels${s}, ${b}--verbosity-list${s}
	      Displays the list of verbosity channels.

	  ${b}--quiet${s}
	      A synonym for ${b}--verbosity $VERBOSITY_QUIET_SET${s}.


	Informing options

	  ${b}--verbosity-help${s}
	      Displays the introduction to verbosity channels, what groups there are,
	      and how the channels work.

	  ${b}--verbosity-help-${g}Channel_name${s}
	      Shows the description for a particular channel: its purpose and what
	      information becomes available, when its verbosity level is raised.

	      Mind the capitalisation (or the lack of it) in channel names.

	  ${b}-h${s}, ${b}--help${s}
	      Shows this help.

	  ${b}-v${s}, ${b}--version${s}
	      Print program version and licence.


	Keyboard controls (console UI)

	  When an input is needed to confirm an action, this would usually be the
	  ${bg}[${w}Enter${g}]${s} key. To decline and exit, press ${bg}[${w}Spacebar${g}]${s}.

	  When a job is launched, the batch is further controlled with these keys:

	    [←] [↓] [,]  decrease the number of CPU cores used for encoding. As the
	                 only CPU core cannot be taken away, SIGSTOP is sent to all
	                 processes in the job’s process group (including the job
	                 itself), which effectively pauses the encoding.

	    [→] [↑] [.]  increase the number of CPU cores available to the job proces-
	                 ses. If the process group was stopped earlier, SIGCONT is
	                 sent to “unpause” it.

	    [i] [End]    interrupts the currently running job. This is preferred to
	                 terminating a job with Ctrl+C or killing the job in any
	                 other way. See the details in the “About” section below.


	Files

	  Config file:
	      ${b}${__b}~/.config/nadeshiko/nadeshiko-do-postponed.rc.sh${s}

	  Postponed jobs:
	      ${b}${__b}~/.cache/nadeshiko/postponed_commands_dir/${s}

	  Logs:
	      ${b}${__b}~/.cache/nadeshiko/logs/${s}

	  Failed jobs and their logs are moved here:
	      ${b}${__b}~/.cache/nadeshiko/postponed_commands_dir/failed/${s}
	      (Files in this directory remain, until they are moved back or deleted
	      by user.)

	  Job files, that completed successfully, are moved here with their logs:
	      ${b}${__b}~/.cache/nadeshiko/postponed_commands_dir/completed/${s}
	      (Directory is cleaned before every new batch.)


	About

	  Nade-do-post provides a simple interface to launch the jobs, that were
	  saved by Nadeshiko-mpv for later. It has GUI and TUI (console interface).
	  The former is only capable of displaying the number of jobs and launching
	  them. The TUI is far more capable and allows to see when each job completes
	  to control the number of CPU cores used for encoding (in runtime!), and to
	  safely interrupt the currently running encoding process.

	  Failed jobs may occur, when the source video file was moved somewhere, for
	  example. To re-run the job, simply move the job file from the “failed” sub-
	  directory back up to “postponed_commands_dir” (see section “Files” above).
	  The log folder may be left in the “failed” directory – it will be wiped
	  anyway to avoid collisions.

	  When the encoding takes a long time, you may wish to close the process
	  and continue it some time later. Due to how ffmpeg works, there’s no way
	  to stash the process data somewhere, end (or kill) the process and then
	  launch it again from the point it was stopped on. This means, that if a
	  running job is interrupted, the encoded data is lost. The output file that
	  is being written, will have to be discarded and created anew, when Nade-
	  do-post is launched again.

	  A job terminated forcefully, that is, when the PC is shut down, or when the
	  process is terminated with Ctrl+C, the job is considered failed. (Meaning
	  that and has to be retrieved from the “failed” subdirectory back one level
	  up to run it again.) Happily for you, the console interface offers a way to
	  gracefully interrupt a job by pressing either “i” or “End” key. The graceful
	  interruption terminates the encoding process (it may take several seconds
	  for the ffmpeg process to unload from memory, though), then wipes the work-
	  ing directory and leaves the job file in its place – as if that job was not
	  approached yet. This allows to avoid the trouble of navigating to the job
	  files directory and getting the job file back from the “failed” subdirec-
	  tory.


	Links

	  Wiki: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/wiki/${s}
	  Report bugs here: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/issues${s}

	                                      Updated for Nadeshiko-do-postponed 2.8.4
	                                                                  18 July 2024

	                                    ◈ ◈ ◈

	EOF
	#  Maximum 78 characters ————————————————————————————————————————————————————>|<——
	return 0
}


show_licence() {
	cat <<-EOF
	Author: deterenkelt, 2018–2024.
	Licence: GNU GPL ver. 3  <http://gnu.org/licenses/gpl.html>
	This is free software: you are free to change and redistribute it.
	There is no warranty, to the extent permitted by law.
	EOF
	return 0
}


return 0