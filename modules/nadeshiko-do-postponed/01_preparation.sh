#  Should be sourced.

#  01_preparation.sh
#  Nadeshiko-do-postponed module that verifies configuration
#  and command line options.
#  Author: deterenkelt, 2018–2022
#
#  For licence see nadeshiko-do-postponed.sh
#


post_read_rcfile() {
	declare -g  taskset_cpulist
	declare -g  taskset_cmd
	declare -g  niceness_level
	declare -g  nice_cmd
	declare -g  nadeshiko_desktop_notifications

	if [ -v processors ]; then
		(( processors > 0  && processors <= cpu_count ))  \
			|| err "The config option “processors” must be a number in a range
			        between 1 and the maximum number of CPUs on this system."
	else
		processors=$(( cpu_count-1 ))
	fi
	processors=$(( processors == 0 ? 1 : processors ))

	[ "${niceness_level:-}" ] && {
		[[ "$niceness_level" =~ ^-?[0-9]{1,2}$ ]] \
			|| err 'Invalid level for nice.'
		REQUIRED_UTILS+=(nice)  # (coreutils)
		nice_cmd="nice -n $niceness_level"
	}
	if [[ "$nadeshiko_desktop_notifications" =~ ^(none|error|all)$ ]]; then
		case "$nadeshiko_desktop_notifications" in
			none)
				nadeshiko_desktop_notifications=0;;
			error)
				nadeshiko_desktop_notifications=1;;
			all)
				nadeshiko_desktop_notifications=3;;
		esac
	else
		err "Incorrect value for desktop notifications in the config.
		     The “nadeshiko_desktop_notifications” parameter should be set
		     to one of: none, error, all."
	fi

	[ -v nadeshiko_verbosity ]  || {
		nadeshiko_verbosity='log=2'
		nadeshiko_verbosity+=',console=0'
		nadeshiko_verbosity+=",desktop=$nadeshiko_desktop_notifications"
	}

	return 0
}


parse_args() {
	declare -g  icon_set

	local  arg
	local  icon_set_list

	for arg in "${ARGS[@]}"; do
		if [[ "$arg" =~ ^(-h|--help)$ ]]; then
			show_help
			exit 0

		elif [[ "$arg" =~ ^(-v|--version)$ ]]; then
			show_licence
			exit 0

		elif [[ "$arg" =~ ^--verbosity-help-([A-Za-z_][A-Za-z0-9_-]*)$ ]]; then
			#  There are currently no extra verbosity channels,
			#  but users may expect them by analogy with two other scripts.
			ch_name=${BASH_REMATCH[1]}
			err "No such verbosity channel: “$ch_name”."

		else
			err "“$arg”: parameter unrecognised.
			     For usage run the program with ${__so:-}-h${__s:-} or ${__so:-}--help${__s:-}."
		fi
	done

	[ "$icon_set" = random ] && {
		pushd "$RESDIR/icons" >/dev/null
		readarray -t icon_set_list < <(ls)
		icon_set=$(sed -rn "$((RANDOM % ${#icon_set_list[*]} + 1)) p" < <(ls))
		popd >/dev/null
	}
	ln -s "$RESDIR/icons/$icon_set" "$TMPDIR/icons"

	return 0
}


return 0