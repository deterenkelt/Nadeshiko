#  Should be sourced.

#  croptool_mpv-crop-script.sh
#  Implementation of a cropping module for Nadeshiko-mpv, that uses
#  mpv_crop_script.lua by TheAMM. Cropping with this Lua script is bugged
#  on mpv >=0.39.
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko.sh
#

 # Must be sourced before the call to “check_required_utils()”
#  in the main script.
#

CROPTOOL_NAME='mpv_crop_script.lua'
REQUIRED_UTILS+=( inotifywait )

orig_croptool_path="$LIBDIR/mpv_crop_script/mpv_crop_script.lua"

croptool_answer_dir="$TMPDIR/croptool_answer"
[ -d "$croptool_answer_dir" ]  \
	&& rm -rf "$croptool_answer_dir"

 # Being overly cautious here: mpv_pid must be defined in the environment
#  as global for all functions of this module.
#
declare -g  mpv_pid


is_crop_tool_available() {
	[ -r "$orig_croptool_path" ] || {
		redmsg "Cropping tool file “$orig_croptool_path” cannot be read."
		err "Cannot find cropping tool “$CROPTOOL_NAME”."
	}
}


 # Check settings sourced from Nadeshiko-mpv .rc files
#
croptool_validate_config() {
	declare -ga  croptool_osd
	declare -g   croptool_guides

	if [ ! -v croptool_osd ] || (( ${#croptool_osd[*]} == 0 )); then
		croptool_osd=(
			'Cropping: select an area'
			''
			'ENTER %dim%– accept%undim%   ESC %dim%– cancel%undim%'
			''
			'Z %dim%– switch frame colour%undim%'
			'X %dim%– switch guides%undim%'
			''
			'CTRL → %dim%– nudge border%undim%'
			'CTRL ↑ %dim%– nudge border%undim%'
			'CTRL ← %dim%– nudge border%undim%'
			'CTRL ↓ %dim%– nudge border%undim%'
			''
			'SHIFT → %dim%– move%undim%'
			'SHIFT ↑ %dim%– move%undim%'
			'SHIFT ← %dim%– move%undim%'
			'SHIFT ↓ %dim%– move%undim%'
		)
	fi

	for ((i=0; i<${#croptool_osd[*]}; i++)); do
		[[ "${croptool_osd[i]}" =~ [\"\$\`\(\<\>\/\\] ]]  \
			&& err 'Config error: croptool_osd items cannot have characters "$`(</>\ '
	done

	if [ ! -v croptool_guides ] || [ -z "$croptool_guides" ]; then
		croptool_guides='none'
	fi
	[[ "$croptool_guides" =~ ^(none|centre|center|trisection)$ ]]  \
		|| err 'croptool_guides may be one of: none, centre (or center), trisection.'
	case "$croptool_guides" in
		centre)
			croptool_guides='center'
			;;
		trisection)
			croptool_guides='grid'
			;;
	esac

	return 0
}


prepare_crop_tool() {
	declare -g  orig_croptool_path
	declare -g  active_croptool_path

	local  croptool_osd=( "${croptool_osd[@]}" )

	active_croptool_path="$TMPDIR/${orig_croptool_path##*/}"

	info 'Preparing the crop tool.'
	info "Copying “$orig_croptool_path”
	           to “$active_croptool_path”."
	cp "$orig_croptool_path" "$active_croptool_path"


	 # Snippets to be injected in the lua code. Nadeshiko-mpv would know
	#  about an error or cancelled cropping only when a specifically named
	#  file would appear in $croptool_answer_dir. (The contents of the files
	#  is unimportant and is added merely to provide a hint, what a particular
	#  file is used for.)
	#
	#  This code is placed into files so that sed could insert it as is,
	#  avoiding the need to escape.
	#
	cat <<-EOF  >"$TMPDIR/code_for_when_croptool_fails.lua"
	local file = io.open("$croptool_answer_dir/croptool_failed", "w")
	file:write("mpv_crop_script couldn’t create a file")
	file:close()
	EOF

	cat <<-EOF  >"$TMPDIR/code_for_when_croptool_was_cancelled.lua"
	local file = io.open("$croptool_answer_dir/croptool_cancelled", "w")
	file:write("mpv_crop_script cancelled cropping (ESC pressed)")
	file:close()
	EOF

	cat <<-EOF  >"$TMPDIR/code_for_when_mpv_was_closed_during_croptool_run.lua"
	local file = io.open("$croptool_answer_dir/croptool_mpv_closed", "w")
	file:write("mpv_crop_script cancelled cropping (mpv closed)")
	file:close()
	EOF


	 # Preparing the persistent OSD message for the crop mode
	#
	for ((i=0; i<${#croptool_osd[*]}; i++)); do
		croptool_osd[i]=${croptool_osd[i]//%dim%/\{\\\\c\&HBEBEBE\&\}}
		croptool_osd[i]=${croptool_osd[i]//%undim%/\{\\\\c\}}
		croptool_osd[i]="\"${croptool_osd[i]}\""  # each item is a line!
	done

	cat <<-EOF  >"$TMPDIR/code_for_croptool_persistent_osd.lua"
	      $(IFS=','; echo "${croptool_osd[*]}")
	EOF


	 # Alternating the script in runtime
	#
	#  To outline the purpose:
	#  - the user may have the original script installed in the mpv config
	#    directory, so we need to avoid clashes in namespace and keybindings;
	#  - depending on how cropping succeeds (or gets cancelled), drop a file
	#    with a specific name to TMPDIR to be a signal for Nadeshiko-mpv;
	#  - add self-cleaning to the original script in order to avoid phantom
	#    crop frames on consecutive runs;
	#  - remove some OSD messages, that would be redundant in the presence
	#    of Nadeshiko-mpv – it would have to report errors anyway;
	#  - correct some OSD messages for the altered code;
	#  - tune the persistent OSD message in the cropping mode to be a bit
	#    more useful.
	#
	sed -ri " # Change script name
	         #  mpv_crop_script → mpv_crop_script_for_nadeshiko
	         #
	         s/^(.*\sSCRIPT_NAME\s*=\s*\"mpv_crop_script)(\"\s*)$/\1_for_nadeshiko\2/


	          # Change the name of the command, that script runs to crop
	         #  crop-screenshot → crop-screenshot-for-nadeshiko
	         #
	         s/^(.*\sSCRIPT_HANDLER\s*=\s*\"crop-screenshot)(\"\s*)$/\1-for-nadeshiko\2/


	          # Specify output file name
	         #  <unpredictable path> → TMPDIR/croptool_W:H:X:Y.jpg
	         #
	         s~^(.*\"output_template\",\s*\")[^\"]+(\".*)$~\1$croptool_answer_dir/croptool_\${crop_w}:\${crop_h}:\${crop_x}:\${crop_y}.jpg\2~


	          # Specify output image format
	         #  As we only need the file name, choose faster method
	         #  png → mjpeg.
	         #  (Since writing the file is disabled later, perhaps, this
	         #  part of code is not necessary?)
	         #
	         s/^(.*\"output_format\",\s*\")[^\"]+(\".*)$/\1mjpeg\2/


	          # Disable the keybinding that toggles crop mode on “c” key.
	         #  Though as cropping mode is entered, the “c” key is used for
	         #  [switching the mode(?) of the] crosshair, and once cropping is
	         #  done we remove the binding from mpv, [it should be no problem],
	         #  but just in case let’s disable this.
	         #  false → true
	         #
	         s/^(.*\"disable_keybind\",\s*)[TRUEFALStruefals]+(.*)$/\1true\2/


	          # Remove the check on non-empty \${ext} in the template for
	         #  the file name – we hardcode the name, so this check would
	         #  only raise a false warning.
	         #
	         #  If you still see the warning, this is your own mpv-crop-script
	         #  installed locally to ~/.config/mpv/scripts/, not this one.
	         #
	         /-- Sanity-check output_template/ {N;N;N;d}


	          # The ENTER key route
	         #  Here the crop frame is accepted and a file goes written in
	         #  TMPDIR, to be then read by Nadeshiko-mpv.
	         #
	         /function ASSCropper:key_event\(name\)/ {
	             #
	             #  Go past the “if name == \"ENTER\" then…” clause
	             #
	             n;n;n;n;n;n;n;n;n;n;n
	             #
	             #  Remove the keybinding, or on the next run when
	             #  Nadeshiko-mpv uses
	             #      send_command 'script-binding' 'crop-screenshot-for-nadeshiko'
	             #  the binding that will be called, would exist in mpv twice.
	             #  And we’ll get two frames and a big lot of problems.
	             #  (self = nil just in case.)
	             #
	             s/.*/    \n/
	             s/.*/&    self = nil\n/
	             s/.*/&    mp.remove_key_binding(SCRIPT_HANDLER)\n/
	             s/.*/&    mp.unregister_event(croptool_on_shutdown)\n/
	         }


	          # The ESC key route
	         #  Where the user quits the cropping mode in the mpv window.
	         #
	         #  At some point it seemed better to make the execution linear and
	         #  leave only the ENTER key route. But with the ESC route, cancel-
	         #  ling an erroneously accepted crop frame goes much faster: you
	         #  just hit Escape and Nadeshiko-mpv does a loop and returns to
	         #  the “Do you wish to crop?” dialogue.
	         #    |  The other way would involve accepting whatever wrong crop
	         #    |  frame was on the screen and then closing/cancelling the
	         #    |  encoding – if Nadeshiko-mpv would receive data that were
	         #    |  accepted, it would leave the loop, that allows to redo
	         #    |  the cropping quickly instead of rewinding mpv, then
	         #    |  setting Time1, Time2 again…).
	         #  The ESC route is now stable.
	         #
	         /^  elseif name == \"ESC\" then/ {
	             n;n;n;n;n;n;n;n

	              # Remove the keybinding like for the ENTER route. (A common
	             #  removal after the entire “if” clause would be possible,
	             #  but there is a desire to preserve helper keys – for guides
	             #  and inversion – functional.
	             #
	             s/.*/    \n/
	             s/.*/&    self = nil\n/
	             s/.*/&    mp.remove_key_binding(SCRIPT_HANDLER)\n/
	             s/.*/&    mp.unregister_event(croptool_on_shutdown)\n/


	              # Inject code that would flag about cancelling
	             #  to Nadeshiko-mpv.
	             #
	             s/.*/&\n/
	             r $TMPDIR/code_for_when_croptool_was_cancelled.lua
	         }


	          # The TEST route
	         #  Ignoring the keybinding that enables testing. Users shouldn’t
	         #  accidentally go into testing mode.
	         #
	         #  NB: we don’t remove the “elseif” clause – just leaving it empty.
	         #
	         /^  elseif name == \"TEST\" then/ {
	             n;  s/.*/    do end/
	         }


	          # Avoid making mpv write a screenshot and then calling
	         #  another mpv to crop it
	         #
	         /^  if option_values\.skip_screenshot_for_images/,/^  end/  d


	          # Replace the call to “mpv --vf=crop …” with touch to write
	         #  an empty file, whose name will be the signal to Nadeshiko-mpv.
	         #
	         /^  local cmd = / {
	             n;n;N;N;N;N;N
	             s/.*/      \"touch\", output_path/
	         }


	          # As we didn’t take a temporary (intermediate) screenshot,
	         #  there’s nothing to remove.
	         #
	         /^  if not option_values\.keep_original/ {N;N;d}


	          # Catching an error in case “touch” wouldn’t be able
	         #  to write the file.
	         #
	         #  The current version of code is stable and no errors should
	         #  ever happen under normal circumstances. It’s just in case some-
	         #  thing may change in the future mpv update and the double frame
	         #  might appear again. (When it happened on the older version, one
	         #  of the cropping frames continued to use the options from a pre-
	         #  vious run, including the old variable “output_template” – hell
	         #  knows where it was stored, but the crop script attempted to
	         #  write the file to the old TMPDIR, from the previous run of
	         #  Nadeshiko-mpv). Another reason for an error with touch may be
	         #  tmpdir being out of inodes. There may be some CPU or disk lag
	         #  or a user error.
	         #
	         /^  if ret\.error or ret\.status ~= 0 then/ {
	             n; d
	             n; s/^([^\"]+\").+(\")[^\"]+$/\1$CROPTOOL_NAME: “touch” couldn’t write the file.\2/
	             n; s/.*/    msg.error(\"touch stdout:\")/


	              # Now injecting code that would flag about the error
	             #  to Nadeshiko-mpv, so it could be able to stop waiting
	             #  and quit.
	             #
	             n; s/.*/&\n/
	             r $TMPDIR/code_for_when_croptool_fails.lua
	         }


	          # Remove some more of original messages to OSD,
	         #  they would be redundant now.
	         #
	         /mp\.osd_message.*Took screenshot/d
	         /mp\.osd_message.*Crop canceled/d


	          # Make the message in the corner a bit more helpful: highlight
	         #  primary controls, hide what shouldn’t be used.
	         #
	         /^\s*fmt_key\(\"ENTER\",\s*\"Accept crop\"\)/ {
	             N;N; s/.*//
	             r $TMPDIR/code_for_croptool_persistent_osd.lua
	         }


	          # Set user’s default type of guides
	         #
	         s/^(  \{\"guide_type\", \")none(\",)\s*$/\1$croptool_guides\2/


	          # Register a shutdown event, that will flag to Nadeshiko-mpv
	         #  that cropping was cancelled – mpv may be closed while in
	         #  the cropping mode, and Nadeshiko-mpv would keep waiting.
	         #  (No, can’t check pid while reading inotifywait).
	         #
	         #  This injection is technically tricky, because
	         #  - the “r” command must be the last in the list between ‘{’
	         #    and ‘}’ – any s/ commands past “r” will end up written
	         #    before the contents of the file. So the injection has to be
	         #    split;
	         #  - and it’s impossible to use the $ address twice;
	         #  - and it’s impossible to use /^mp.add_key_binding/ twice too;
	         #  - so the split function has to be placed somewhere above
	         #    the actual last line in the original file. We hook it
	         #    to the closest unique line: “used_keybind = nil”;
	         #  - as the same /address/ cannot be used twice, the second
	         #    half of the function is hooked to /^mp.add_key_binding/
	         #    (indeed, the function definition bounds – that is, “function”
	         #    and “end” keywords – could be added to the function code
	         #    itself, to the portion of code to be injected; stupid
	         #    pedanticism required those portions to have only the code
	         #    per se);
	         #  - finally, the “mp.register_event” call can be hooked on $.
	         #
	         /^\s*used_keybind = nil/ {
	             n
	             s/.*/&\n\nfunction croptool_on_shutdown()/
	             r $TMPDIR/code_for_when_mpv_was_closed_during_croptool_run.lua
	         }

	         /^mp.add_key_binding/ {
	             s/.*/end\n\n&/
	         }

	         $ {
	             s/.*/&\nmp.register_event(\"shutdown\", croptool_on_shutdown)\n/
	         }

	         " \
		"$active_croptool_path"

	return 0
}


run_croptool_installer() {
	#  The original idea was to offer the user a set of cropping tools and let
	#  him choose the one he finds more convenient or available. The approp-
	#  riate Nadeshiko module would then be installed to the place of the
	#  “tool to crop” – and this is the installation, that gave the name to
	#  this function. It was long ago, when it wasn’t decided yet, whether
	#  there should be just one tool or many, will there be integration with
	#  desktop tools or it will be an mpv script. Things turned out so there’s
	#  now just one tool, but some code and the UI bear remains of the “choose
	#  and install” approach. Who knows, maybe they’ll be needed some day…
	#
	# chmod +x "$LIBDIR/mpv-crop-script_installer.sh"
	# "$LIBDIR/mpv-crop-script_installer.sh" || return $?
	# send_command 'load-script' "$mpv_confdir/scripts/mpv_crop_script.lua" \
	# 	|| return $?
	return 0
}


run_crop_tool() {
	unset  croptool_resp_cancelled
	unset  croptool_resp_failed
	unset  croptool_resp_width
	unset  croptool_resp_height
	unset  croptool_resp_x
	unset  croptool_resp_y

	declare -g  croptool_resp_cancelled
	declare -g  croptool_resp_failed
	declare -g  croptool_resp_width
	declare -g  croptool_resp_height
	declare -g  croptool_resp_x
	declare -g  croptool_resp_y
	declare -g  phantom_mpv_delay

	: ${phantom_mpv_delay:=0}

	local  s
	local  mpv_processes_number

	[ -e "/proc/${mpv_pid:-doesn’t exist}" ]  \
		|| abort 'Cancelled: mpv was closed.'

	set_prop 'fullscreen' 'yes'
	get_props 'cursor-autohide'  \
	          'cursor-autohide-fs-only'
	set_prop 'cursor-autohide' 'no'
	set_prop 'cursor-autohide-fs-only' 'no'

	 # Ideally there should be a check on what scripts are already loaded.
	#  Loading it multiple times is wrong and is an evident source of bugs.
	#
	send_command 'load-script' "$active_croptool_path"

	 # Forks to background immediately.
	#  The module should set the (variable with) dialog text.
	#
	#show_dialogue_cropping "Select an area, then press ENTER."

	#  Remove the flag files that might be left from previous runs.
	noglob_off
	rm -f "$TMPDIR/croptool_"*  &>/dev/null
	noglob_rewind
	info 'Launching the crop tool.'

	#  See the comment to this variable in mpv_ipc.sh.
	local MPV_IPC_CHECK_SOCKET_ASSUME_ONE_PROCESS=t
	send_command 'script-binding' 'crop-screenshot-for-nadeshiko'

	 # We’ll use “inotifywait” to wait for a file with a specific name to
	#  appear there. It’s easier to tell inotifywait to wait for the first
	#  event, that would create a file, and then quit (so we would know that
	#  the cropping is done, and we can read the result). But as with this
	#  method it’s just ANY new file, we can’t use $TMPDIR: bash uses $TMPDIR
	#  to create its own temporary files named “sh-thd.XXXXX”. Such files are
	#  created e.g. for heredocs (<<) and herestrings (<<<). And during the
	#  time when the croptool is being prepared, 17(!) sh-thd.XXXXX files
	#  are created. Though bash instantly wipes them out, some may linger
	#  for just a fraction of a second, and this will be enough for “inotify-
	#  wait” to notice the created file and quit. A one second delay would
	#  solve this problem most of the time, but it’s safer to read in a sepa-
	#  rate directory.
	#
	mkdir "$croptool_answer_dir"

	while  read -r ; do
		#  Nadeshiko-mpv waits for the croptool to place a file with a speci-
		#  fic name in $TMPDIR. There is no IPC or a pipe or anything else
		#  between them. The file names are predefined:
		#    - “croptool_123:345:567:789.jpg” (numbers may be any) – when
		#      user has accepted a crop frame. This will confim the choice
		#      and Nadeshiko-mpv will successfully leave the crop loop.
		#    - “croptool_cancelled” is created when user presses the Escape
		#      key without accepting crop frame. This will return Nadeshiko-
		#      mpv to the “Do you wish to crop?” dialogue, where user may
		#      either press Pick and go to the crop mode again or skip and
		#      continue without cropping.
		#    - “croptool_failed” is created when the lua script catches
		#      an error after accepting the crop frame. Effectively means
		#      that it couldn’t write the file name. Probably a path or a
		#      filesystem error.
		#    - “croptool_mpv_closed” appears when user enters crop mode in
		#      the mpv window, but quits it (perhaps, accidentally). Or mpv
		#      crashed (technically possible, but never observed). Nadeshiko-
		#      mpv quits in this case with the “aborted by user” exit code.
		#
		[[ "$REPLY" =~ ^croptool_(cancelled|failed|mpv_closed|([0-9]+):([0-9]+):([0-9]+):([0-9]+)\.jpg)$ ]] && {
			case "${BASH_REMATCH[1]}" in
				cancelled)
					croptool_resp_cancelled=t
					;;
				failed)
					croptool_resp_failed=t
					;;
				mpv_closed)
					abort 'Cancelled: mpv was closed.'
					;;
				*)
					croptool_resp_width="${BASH_REMATCH[2]}"
					croptool_resp_height="${BASH_REMATCH[3]}"
					croptool_resp_x="${BASH_REMATCH[4]}"
					croptool_resp_y="${BASH_REMATCH[5]}"
					;;
			esac
			break
		}
	done < <(inotifywait -q --format %f  -e create  "$croptool_answer_dir") || true

	set_prop 'fullscreen' 'no'
	set_prop 'cursor-autohide' "$cursor_autohide"
	set_prop 'cursor-autohide-fs-only' "$cursor_autohide_fs_only"

	return 0
}


 # If the script would break before restore_croptool_config would normally
#  run, call it from this hook, that is executed from Nadeshiko-mpv own
#  on_error().
on_croptool_error() {
	# [ -v croptool_config_backup ] && restore_croptool_config
	pkill -PIPE --session 0 --pgroup 0  -f inotifywait || true
	return 0
}


return 0
