#  Should be sourced.

#  04_encoding.sh
#  Nadeshiko-mpv module where the encoding backend is called (or the encoding
#  is postponed and stored as a job file for Nadeshiko-do-postponed).
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko-mpv.sh
#


encode() {
	local  audio
	local  subs
	local  deint
	local  nadeshiko_retval
	local  command
	local  postponed_job_file
	local  postponed_job_file_name
	local  str
	local  i
	local  first_line_in_postponed_command=t

	check_needed_vars

	echo
	info 'Ready to encode (or to postpone)'

	if [ ! -v mute_true  -a  -v ffmpeg_ext_audio ]; then
		audio="audio=$ffmpeg_ext_audio"

	elif [ ! -v mute_true  -a  -v ffmpeg_audio_tr_id ]; then
		audio="audio:$ffmpeg_audio_tr_id"

	else
		audio='noaudio'
	fi

	 # Never rely on “sub_visibility” property, as it is set to “on”
	#  by default, even when there’s no subtitles at all.
	#
	if [ -v ffmpeg_ext_subs ]; then
		subs="subs=$ffmpeg_ext_subs"

	elif [ -v ffmpeg_subs_tr_id ]; then
		subs="subs:$ffmpeg_subs_tr_id"

	else
		subs='nosubs'
	fi

	 # Unset, if mpv didn’t return a valid timestamp,
	#  or if it did, but the delay is zero.
	#
	if	[ "$subs" = 'nosubs' ]  \
		|| [[ "${sub_delay:-0.0}" =~ ^\-?0+\.0+$ ]]
	then
		unset sub_delay
	fi
	[ -v sub_delay ] && sub_delay=$( round "$sub_delay" 3 )

	 # Unset, if mpv didn’t return a valid timestamp,
	#  or if it did, but the delay is zero.
	#
	if	[ "$subs" = 'nosubs' ]  \
		|| [[ "${sub_scale:-1.0}" =~ ^(0|1)\.0+$ ]]
	then
		unset sub_scale
	fi
	[ -v sub_scale ] && sub_scale=$( round "$sub_scale" 2 )

	 # Unset, if mpv didn’t return a valid timestamp,
	#  or if it did, but the delay is zero.
	#
	if	[ -v mute_true ]  \
		|| [[ "${audio_delay:-0.0}" =~ ^\-?0+\.0+$ ]]
	then
		unset audio_delay
	fi
	[ -v audio_delay ] && audio_delay=$( round "$audio_delay" 3 )

	[ -v deinterlace_true ]  \
		&& deint='deinterlace=on'  \
		|| deint='deinterlace=off'

	if [ -e "/proc/${mpv_pid:-doesn’t exist}" ]; then
		send_command  show-text 'Encoding' '2000'
	else
		: warn-ns "Not sending anything to mpv: it was closed."
	fi


	#  The existence of the default preset is not obligatory.
	if  [ "${nadeshiko_preset:-}" = 'nadeshiko.rc.sh' ]  \
	    && [ ! -r "$CONFDIR/nadeshiko.rc.sh" ]
	then
		unset nadeshiko_preset
	fi

	 # “postpone” passed to nadeshiko-mpv.sh forks the process in a paused
	#  state in the background. When nadeshiko.sh (not …-mpv.sh!) is called
	#  with a single parameter “unpause”, it unfreezes the processes
	#  one by one and then quits.
	#
	#  This allows to free the watching from humming/heating and probably,
	#  even glitchy playback, if you attempt to continue watching after
	#  firing up the encode, especially, if it’s the 2nd, the 3rd or the 15th.
	#
	#  $max_size is allowed to be empty for quick_run mode (max_size then
	#  just sourced from the quick_run_preset or from the defconf settings).
	#
	nadeshiko_command=(
		"$MYDIR/nadeshiko.sh"  "${time1[ts]}"
		                       "${time2[ts]}"
		                       "$path"
		                       "$audio"
		                       ${audio_delay:+audio_delay=$audio_delay}
		                       "$subs"
		                       ${sub_delay:+sub_delay=$sub_delay}
		                       ${sub_scale:+sub_scale=$sub_scale}
		                       "$deint"
		                       ${max_size:+size=$max_size}
		                       ${crop:+crop=$crop}
		                       "${screenshot_directory:-$working_directory}"
		                       ${fname_pfx:+"fname_pfx=$fname_pfx"}
		                       ${loop_last_frame_s:+loop_last_frame=$loop_last_frame_s}
		                       ${scene_complexity:+force_scene_complexity=$scene_complexity}
		                       "${nadeshiko_preset[@]}"
		                       do_not_report_ffmpeg_progress_to_console
	)
	if [ -v postpone ]; then
		info "Saving the command as a postponed job file."
		#
		#  The file name for the job file
		#
		#  There’s little reason to bother with “smart” shortening like
		#  for the real file to be encoded, as for the job it’s the
		#  “.XXXXXXXX” extension that uniquely defines the file. The original
		#  file name, if needed, is always present in the comment at the top.
		#
		#  It’s shortened to 215 bytes (may be less, but not more), as the
		#  standard length of the tail – a space, two timestamps, an exten-
		#  sion – takes 40 bytes. This length doesn’t change. “Until” and not
		#  “…:offset:length”, because of multibyte characters.
		#
		postponed_job_file_name="${path##*/}"
		while (( $( echo -n "$postponed_job_file_name" | wc --bytes ) > 212 )); do
			postponed_job_file_name="${postponed_job_file_name%?}"
		done
		postponed_job_file_name+='…'
		[ -d "$postponed_commands_dir" ] || mkdir "$postponed_commands_dir"
		postponed_job_file="$postponed_commands_dir"
		postponed_job_file+="/$(
			mktemp -u "$postponed_job_file_name ${time1[ts]}–${time2[ts]}.XXXXXXXX"
		).sh"
		cat <<-EOF >"$postponed_job_file"
		#! /usr/bin/env bash

		#  Nadeshiko-mpv postponed job file
		#  ${postponed_job_file##*/}
		#  This file is to be run with nadeshiko-do-postponed.sh
		#  (But can be launched as a standalone script too.)
		#

		 # If you need to add options
		#
		#  - spaces in front may be tabs or spaces, in any quantity;
		#  - the trailing “\”, which serves as a line break, should be the
		#      last character on the line (not even whitespace characters are
		#      allowed to follow it);
		#  - initially, when you open this file, you may notice, that the last
		#      line also has a trailing “\”. It is placed only for your conve-
		#      nience, and you don’t have to end the last line with one after
		#      you finish editing the file;
		#  - strings with spaces may be put in quotes or escape special cha-
		#      racters with backslashes. The latter are used by default only
		#      because it’s somewhat easier to automate writing the file
		#      this way;
		#  - the options here are the same as specified in “nadeshiko --help”.
		#


		EOF
		chmod +x "$postponed_job_file"
		for str in "${nadeshiko_command[@]}"; do
			if [ -v first_line_in_postponed_command ]; then
				printf '%q  \\\n\t\\\n' "$str"  >>"$postponed_job_file"
				unset first_line_in_postponed_command
			else
				printf '\t%q  \\\n' "$str"  >>"$postponed_job_file"
			fi
		done
		rm "$data_file"
		if [ -e "/proc/${mpv_pid:-doesn’t exist}" ]; then
			send_command  show-text \
			              "Command to encode saved for later."  \
			              '2000'
		fi

	else
		info 'Running Nadeshiko\n'
		msg "${nadeshiko_command[0]}"
		milinc 2
		for ((i=1; i<${#nadeshiko_command[*]}; i++)) do
			msg "${nadeshiko_command[i]}"
		done
		mildec 2

		header-msg 'Nadeshiko log (see the full version in a separate file)'
		errexit_off

		env  \
			VERBOSITY=desktop=0  \
			"${nadeshiko_command[@]}"

		nadeshiko_retval=$?
		errexit_rewind

		footer-msg 'End of Nadeshiko log'
		rm "$data_file"

		if [ -e "/proc/${mpv_pid:-doesn’t exist}" ]; then
			if (( nadeshiko_retval == 0 )); then
				send_command  show-text 'Encoding done.' '2000'
				info-ns 'Encoding done.'
				play_encoded_file

			else
				send_command  show-text 'Failed to encode.' '3000'
				err 'Failed to encode.'
			fi

		else
			: warn-ns "Not sending anything to mpv: it was closed."
		fi

	fi

	return 0
}


return 0