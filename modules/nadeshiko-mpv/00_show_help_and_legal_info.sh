#  Should be sourced.

#  00_show_help_and_legal_info.sh
#  Nadeshiko-mpv module to output help and legal use information.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


show_help() {
	local b="${__bright}"
	local g="${__green}"
	local w="${__white}"
	local bg="${__bright}${__green}"
	local s="${__stop}"

	 # Use this line instead to check that the output fits 78 into columns.
	#
	#  When restoring the $SHOW_HELP_PAGER line, there should be an empty
	#  line left between it and the header of the manual page.
	#
	#cat <<-EOF |& ansifilter |& grep -nE '^.{79,}$' || info 'All fits!'
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF
	   
	                    ${__bri}${__blue}Nadeshiko-mpv usage and list of options${s}

	Usage

	  ${b}./nadeshiko-mpv.sh${s} [${b}options${s}]


	Workflow options

	  ${b}postpone${s}
	      Instead of launching the encoding right away, save the command as
	      a postponed job. Call ${b}${__b}nadeshiko-do-postponed.sh${s} to see the number
	      of postponed jobs and launch them.

	  ${b}quickrun${s}
	      Speed up making cuts: disable GUI, disable predictor and turn on
	      ${b}postpone${s}. Cropping dialogue will remain, as will preview and post-
	      view.

	  <${bg}config_file${s}>
	      Chooses an alternative configuration file. May be a file name alone
	      or a path.
	      There are several requirements:
	        - it must be an existing file in ~/.config/nadeshiko/;
	        - the file name must be in format “$MYNAME_NOEXT${bg}suffix${s}.rc.sh”;
	      The ${bg}suffix${s} may consist only of these characters:
	          ${b}A-Z a-z 0-9 . , : ; _ -${s}
	      Presets are a useful feature, read about how to use them on the wiki
	      (see the link at the bottom).


	Verbosity options

	  ${b}--verbosity${s} <${bg}channelA${w}=${g}level1${w},${g}channelB${w}=${g}level2${w},${s}…${b},${g}channelN${w}=${g}levelM${s}>
	      Sets verbosity for the specified channels. For the names and the
	      possible levels refer to the list of channels (see below). The
	      “Troubleshooting guide” on the wiki has a section, that describes,
	      when this option comes helpful.

	      This option and the ${b}VERBOSITY${s} environment variable accept
	      the same value.

	  ${b}--verbosity-list-channels${s}, ${b}--verbosity-list${s}
	      Displays the list of verbosity channels.

	  ${b}--quiet${s}
	      A synonym for ${b}--verbosity $VERBOSITY_QUIET_SET${s}.


	Informing options

	  ${b}--verbosity-help${s}
	      Displays the introduction to verbosity channels, what groups there are,
	      and how the channels work.

	  ${b}--verbosity-help-${g}Channel_name${s}
	      Shows the description for a particular channel: its purpose and what
	      information becomes available, when its verbosity level is raised.

	      Mind the capitalisation (or the lack of it) in channel names.

	  ${b}-h${s}, ${b}--help${s}
	      Shows this help.

	  ${b}-v${s}, ${b}--version${s}
	      Print program version and licence.


	Files

	  Config file: ${b}${__b}~/.config/nadeshiko/nadeshiko-mpv.rc.sh${s}
	  Postponed jobs: ${b}${__b}~/.cache/nadeshiko/postponed_commands_dir/${s}
	  Logs: ${b}${__b}~/.cache/nadeshiko/logs/${s}


	About

	  Nadeshiko-mpv is a frontend for Nadeshiko, that allows to make cuts
	  directly from the mpv media player, crop video in the mpv window
	  (thanks to the built-in script by TheAMM), and then either to run
	  Nadeshiko to encode the cut, or to save the encoding command for later.

	  The inter-process communication with mpv, requires the configuration
	  to be set up first. The configuration files should explain that well,
	  but for those, who may need a thorough step-by-step guide, there’s one
	  on the wiki.


	Socket error cheat sheet

	  This section exists only as a quick reminder. For a thorough explanation
	  and advices refer to the wiki (see “Links” below).

	  “Connection refused” or “N sockets occupied” (N>0) – several mpv windows
	  were clashing for one socket. Leave one – or “pkill -9 -f mpv” them, and
	  then launch the one you need.

	  “N sockets unused” (N>0) – mpv or Nadeshiko are misconfigured and are
	  set up to use mismatching socket paths; or mpv is not running.

	  “N bad sockets” (N>0) indicates a problem with the socket file itself:
	    - socket file doesn’t exist?
	       ⋅ your ~/.config/mpv/config is missing a line, that specifies
	         socket path? E.g. “input-ipc-server=/tmp/mpv.socket”.
	       ⋅ filesystem is out of space or inodes?
	       ⋅ mpv couldn’t create the socket by the specified path,
	         because the directory is not writeable?
	    - socket file exists, but isn’t accessible for reading/writing:
	       ⋅ file’s permissions?
	       ⋅ the permissions of a directory on a certain level above the file
	         are blocking access to it?

	  Get more details on how Nadeshiko detects and discards sockets with
	    $ nadeshiko-mpv --verbosity=Mpv_ipc=4


	Links

	    Wiki: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/wiki/${s}
	    Report bugs here: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/issues${s}

	                                               Updated for Nadeshiko-mpv 2.8.0
	                                                                  18 July 2024

	                                    ◈ ◈ ◈

	EOF
	#  Maximum 78 characters ————————————————————————————————————————————————————>|<——
	return 0
}


show_licence() {
	cat <<-EOF
	Author: deterenkelt, 2018–2024.
	Licence: GNU GPL ver. 3  <http://gnu.org/licenses/gpl.html>
	This is free software: you are free to change and redistribute it.
	There is no warranty, to the extent permitted by law.
	EOF
	return 0
}


return 0