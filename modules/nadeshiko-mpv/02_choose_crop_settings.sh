#  Should be sourced.

#  02_choose_crop_settings.sh
#  Nadeshiko-mpv module to run a dialogue window where user may pick crop
#  coordinates or pick them interactively.
#  Author: deterenkelt, 2018–2023
#
#  For licence see nadeshiko-mpv.sh
#


choose_crop_settings() {
	declare -g  predictor
	declare -g  crop;  unset crop

	local  pick
	local  has_croptool_installer
	local  resp_crop
	local  resp_predictor
	local  crop_width
	local  crop_height
	local  crop_x
	local  crop_y
	local  max_for_crop_width
	local  max_for_crop_height


	info "Running crop dialogue."
	milinc

	#  Module function.
	[ "$(type -t run_crop_tool)" = 'function' ] || return 0
	pause_and_leave_fullscreen
	#  Module function.
	[ "$(type -t run_croptool_installer)" = 'function' ]  \
		&& has_installer=yes  \
		|| has_installer=no
	[ -v predictor ]  \
		&& predictor=on  \
		|| predictor=off

	until [ -v cropsettings_accepted ]; do
		is_crop_tool_available  \
			&& pick='on'  \
			|| pick='off'

		show_dialogue_crop_and_predictor pick="$pick"  \
		                                 has_installer="$has_installer"  \
		                                 predictor="$predictor"  \
		                                 ${crop_width:-}  \
		                                 ${crop_height:-}  \
		                                 ${crop_x:-}  \
		                                 ${crop_y:-}

		IFS=$'\n' read -r -d ''  resp_crop       \
		                         resp_predictor  \
			< <(echo -e "$dialog_output\0")

		case "${resp_crop#crop=}" in
			nocrop)
				info 'Disabling crop.'
				unset crop
				cropsettings_accepted=t
				;;

			pick)
				unset crop_width  crop_height  crop_x  crop_y
				prepare_crop_tool  \
					|| err 'Cropping module failed at preparing crop tool.'
				run_crop_tool  \
					|| err 'Cropping module failed at running crop tool.'
				if [ -v croptool_resp_cancelled ]; then
					info-ns 'Cropping cancelled.'
				elif [ -v croptool_resp_failed ]; then
					warn-ns 'Crop tool failed.'
				else
					crop_width=$croptool_resp_width
					crop_height=$croptool_resp_height
					crop_x=$croptool_resp_x
					crop_y=$croptool_resp_y
				fi
				;;

			install_croptool)
				run_croptool_installer  \
					|| err 'Crop tool installer has exited with an error.'
				;;

			+([0-9]):+([0-9]):+([0-9]):+([0-9]))
				info 'Setting crop fragment parameters:'
				[[ "$resp_crop" =~ ^crop=([0-9]+):([0-9]+):([0-9]+):([0-9]+)$ ]]
				crop_width=${BASH_REMATCH[1]}
				crop_height=${BASH_REMATCH[2]}
				crop_x=${BASH_REMATCH[3]}
				crop_y=${BASH_REMATCH[4]}
				msg "Size of the full frame
				         width: $width px
				         height: $height px

				     Cropped fragment size:
				         width: $crop_width px
				         height: $crop_height px

				     Cropped fragment base coordinates:
				         X topleft: $crop_x
				         Y topleft: $crop_y"

				#  This is mostly to prevent mistypings (crop coordinates
				#  may be entered from keybord into the dialogue window).
				#
				(( crop_x >= width ))  \
					&& err "Crop Xtopleft >= frame width."

				(( crop_y >= height ))  \
					&& err "Crop Ytopleft >= frame height."


				#  With width and height of the cropped fragment we cannot
				#  dismiss wrong numbers right away.
				#
				#  There’s a bug with mpv_crop_script.lua, which allows the
				#  lower boundary of the movable on-screen rectangle to go
				#  below the frame height. It appears on some old DVDs with
				#  non-square pixel aspect ratio. (That aspect ratio, however,
				#  may or may not be the cause. After all, it’s the width,
				#  that it should affect, not the height).
				#
				#  This bug is dealt with silently by autocorrecting the ex-
				#  ceeding crop dimensions to what fits into the frame. As
				#  a precaution, the correction is applied to both width and
				#  height (though it’s only the height that caused this bug).
				#
				max_for_crop_width=$((width - crop_x))
				max_for_crop_height=$((height - crop_y))

				(( crop_width > max_for_crop_width )) && {
					warn-ns "Autocorrecting fragment width: $crop_width → $max_for_crop_width"
					crop_width=$max_for_crop_width
				}

				(( crop_height > max_for_crop_height )) && {
					warn-ns "Autocorrecting fragment height: $crop_height → $max_for_crop_height"
					crop_height=$max_for_crop_height
				}

				crop="$crop_width:$crop_height:$crop_x:$crop_y"
				cropsettings_accepted=t
				;;

			*)
				err "Dialog returned wrong value for crop: “$resp_crop”."
				;;
		esac

		case "${resp_predictor#predictor=}" in
			on)
				predictor=on
				;;
			off)
				predictor=off
				;;
			*)
				err "Dialog returned wrong value for predictor: “$resp_predictor”."
				;;
		esac

	done

	[ "$predictor" != on ] && unset predictor
	mildec
	echo

	return 0
}


return 0