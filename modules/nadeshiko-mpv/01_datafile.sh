#  Should be sourced.

#  01_datafile.sh
#  Nadeshiko-mpv module that queries the running mpv window and gathers the
#  current options from the player. Among the others the timestamp of the
#  position in the opened video.
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko-mpv.sh
#


vchan setup  name=Datafile  \
             level=1  \
             meta=M  \
             min_level=0  \
             max_level=3  \
             group_item=write_var_to_datafile  \
             group_item=del_var_from_datafile


show_help_on_verbosity_channel_datafile() {
	cat <<-EOF
	Datafile (self-report channel)
	    Filters messages related to the building and launching dialogue
	    windows.
	    Levels:
	    0 – no output;
	    1 – errors-only (the default);
	    2 – warnings and errors;
	    3 – informational messages, warnings and errors;
	EOF
	return 0
}


write_var_to_datafile() {
	local  varname="$1"
	local  varval="$2"
	local  var

	[ "$varval" = '--only-export' ] || {
		info "Setting $varname and appending it to ${data_file##*/}."
		declare -g $varname="$varval"
	}
	sed -ri "/^(declare -[a-zA-Z]+ |)$varname=/d" "$data_file"
	#  Human-readable times in the postponed jobs
	#  NB this code relies upon bash-4.4 “@A” operator. It appeared only
	#  recently, and the nuances described below may change in the future.
	#  1. @A operator resolves the nameref – the original variable name
	#     is printed.
	#  2. [@] is necessary to restore arrays, but the regular variables
	#     accept it too (even integers!), which allows to avoid making
	#     “[@]” placed conditionally.

	declare -n var=$varname
	echo "${var[@]@A}" >> "$data_file"
	return 0
}


del_var_from_datafile() {
	local varname="$1"
	unset $varname
	info "Deleting $varname from ${data_file##*/}."
	sed -ri "/^(declare -[a-zA-Z]+ |)$varname=/d" "$data_file"
	return 0
}


populate_data_file() {
	declare -g  ffmpeg_ext_subs
	declare -g  ffmpeg_subs_tr_id
	declare -g  ffmpeg_audio_tr_id

	local  tr_type
	local  tr_is_selected
	local  tr_id
	local  ff_index
	local  tr_is_external
	local  external_filename
	local  i

	#  NB path must come after working directory!
	#  path must be a valid file, but path *may be* relative
	#  to $working_directory, thus the latter becomes a part of the path’s
	#  “good value” test.
	#
	get_props  working-directory     \
	           screenshot-directory  \
	           path                  \
	           mute                  \
	           deinterlace           \
	           sub-visibility        \
	           track-list            \
	           track-list/count      \
	           sub-delay             \
	           sub-scale             \
	           audio-delay           \
	           volume                \
	           width                 \
	           height

	write_var_to_datafile working_directory "$working_directory"
	write_var_to_datafile screenshot_directory "$screenshot_directory"

	if [ -e "$path" ]; then
		write_var_to_datafile  path "$path"

	elif [ -e "$working_directory/$path" ]; then
		write_var_to_datafile  path "$working_directory/$path"
	fi

	write_var_to_datafile  mute $mute
	write_var_to_datafile  sub_visibility $sub_visibility

	info 'Traversing tracks.'
	milinc

	#  Subtitle and audio tracks are either an index of an internal stream
	#  or a path to an external file.
	for ((i=0; i<track_list_count; i++)); do
		IFS=$'\n' read -d '' tr_type            \
		                     tr_is_selected     \
		                     tr_id  ff_index    \
		                     tr_is_external     \
		                     external_filename  \
			< <( echo "$track_list"  \
			     | jq -r ".[$i].type,                                \
			              .[$i].selected,                            \
			              .[$i].id,  .[$i].\"ff-index\",             \
			              .[$i].external,  .[$i].\"external-filename\"";  \
			     echo -en '\0'
			   )
		info "Track $i"
		milinc
		msg "Type: $tr_type
		     Selected: $tr_is_selected
		     ID: $tr_id
		     FFmpeg index: $ff_index
		     External: $tr_is_external
		     External filename: $external_filename"
		mildec
		milinc
		[ "$tr_is_selected" = true ] && {
			#  “ffmpeg_subs_tr_id” and “ffmpeg_audio_tr_id” are to become
			#  “subs” and “audio” parameters for Nadeshiko, thus, they must be
			#  set only when subtitles are enabled and audio is on.
			if [ "$tr_type" = sub  -a  -v sub_visibility_true ]; then
				case "$tr_is_external" in
					true)
						if [ -f "$external_filename" ]; then
							ffmpeg_ext_subs="$external_filename"

						elif [ -f "$working_directory/$external_filename" ]; then
							ffmpeg_ext_subs="$working_directory/$external_filename"

						else
							ffmpeg_ext_subs="$FUNCNAME: mpv’s “$external_filename” is not a valid path."
						fi
						write_var_to_datafile  ffmpeg_ext_subs "$ffmpeg_ext_subs"
						;;
					false)
						#  man mpv says:
						#  “Note that ff-index can be potentially wrong if a
						#     demuxer other than libavformat (--demuxer=lavf)
						#     is used. For mkv files, the index will usually
						#     match even if the default (builtin) demuxer is
						#     used, but there is no hard guarantee.”
						#  (There’s nothing to worry about, if you build mpv
						#   on ffmpeg.)
						#
						 # Note the difference between the mpv options in
						#   $tr_id and $ff_index:
						#   - “ff_index” is the number of a stream by order.
						#     This includes all video, audio, subtitle streams,
						#     also fonts. ffmpeg -map would use syntax like
						#     0:<stream_id> to refer to the stream. (sic!)
						#   - “tr_id” is the number of this stream among the
						#     other streams of *this same type*. ffmpeg -map
						#     uses syntax like  0:s:<subtitle_stream_id> (sic!)
						#     for these – that’s exactly what we need.
						#     It’s necessary to decrement it by one, as mpv
						#     counts them from 1 and ffmpeg – from 0.
						ffmpeg_subs_tr_id=$((  tr_id - 1  ))
						write_var_to_datafile  ffmpeg_subs_tr_id "$ffmpeg_subs_tr_id"
						;;
				esac

			elif [ "$tr_type" = audio   -a  ! -v mute_true ]; then
				case "$tr_is_external" in
					true)
						if [ -f "$external_filename" ]; then
							ffmpeg_ext_audio="$external_filename"

						elif [ -f "$working_directory/$external_filename" ]; then
							ffmpeg_ext_audio="$working_directory/$external_filename"

						else
							ffmpeg_ext_audio="$FUNCNAME: mpv’s “$external_filename” is not a valid path."
						fi
						write_var_to_datafile  ffmpeg_ext_audio "$ffmpeg_ext_audio"
						;;
					false)
						#  See the note at the similar place above.
						ffmpeg_audio_tr_id=$((  tr_id - 1  ))
						write_var_to_datafile  ffmpeg_audio_tr_id "$ffmpeg_audio_tr_id"
						;;
				esac
			fi
		}
		mildec
	done

	mildec

	(( VERBOSITY_CHANNELS[Datafile] > 2 ))  && {
		info "Data file contents:
		 
		$(<"$data_file")
		 
		"
	}
	return 0
}


 # On the first run, it remembers the current mpv timestamp as time1, echoes
#  subtitle visibility and mute settings to mpv OSD, then quits. On the second
#  run, it remembers the current mpv timestamp as time2, echoes subvis and
#  mute props, gathers the remaining mpv properties.
#
put_time_and_collect_props() {
	local  mute_text
	local  subvis_text

	info 'Getting time from mpv.'
	get_prop  time-pos

	if [ ! -v time1 -o -v time2 ]; then
		new_time_array  time1  "${time_pos%???}"
		write_var_to_datafile  time1  --only-export
		info "Getting sub-visibility and mute mpv properties."
		get_props  sub-visibility  mute
		[ -v sub_visibility_true ]  \
			&& subvis_text='Subs: ON'  \
			|| subvis_text='Subs: OFF'
		[ -v mute_true ]  \
			&& mute_text='Sound: OFF'  \
			|| mute_text='Sound: ON'
		send_command  show-text "Time1 set\n$subvis_text\n$mute_text" "3000"
		unset time2
		del_var_from_datafile time2

	elif [ -v time1 -a ! -v time2 ]; then
		new_time_array  time2  "${time_pos%???}"
		write_var_to_datafile  time2  --only-export
		info "Getting sub-visibility, mute and the rest of mpv properties."
		populate_data_file
		[ -v sub_visibility_true ]  \
			&& subvis_text='Subs: ON'  \
			|| subvis_text='Subs: OFF'
		[ -v mute_true ] \
			&& mute_text='Sound: OFF'  \
			|| mute_text='Sound: ON'
		send_command show-text "Time2 set\n$subvis_text\n$mute_text" "3000"
	fi

	return 0
}


arrange_times() {
	info 'Arranging Time1 and Time2.'
	check_needed_vars
	[ "${time1[ts]}" = "${time2[ts]}" ] && err "Time1 and Time2 are the same."
	(( time1[total_ms] > time2[total_ms] )) && {
		new_time_array  time_buf "${time1[ts]}"
		new_time_array  time1 "${time2[ts]}"
		new_time_array  time2 "${time_buf[ts]}"
		write_var_to_datafile  time1 --only-export
		write_var_to_datafile  time2 --only-export
	}

	echo
	return 0
}


 # This function verifies, that all the necessary variables
#  are set at the current stage. For that it has a list of
#  variable names for each caller function.
#
check_needed_vars() {
	declare -A vars_needed=(
		[arrange_times]='time1  time2'
		[play_preview]='time1  time2  mute  sub_visibility'
		[choose_preset]=''
		[encode]='time1  time2  mute  sub_visibility  screenshot_directory  working_directory'
		[play_encoded_file]='screenshot_directory  working_directory'
	)
	[ -v quick_run ] || vars_needed[encode]+=' max_size'
	for var in ${vars_needed[${FUNCNAME[1]}]}; do
		[ -v $var ]  \
			|| err "Variable “$var” must be set for stage “${FUNCNAME[1]}”."
	done
	return 0
}


return 0