#  Should be sourced.

#  01_check_options_and_args.sh
#  Nadeshiko-mpv module which runs post-checks on rc file options and validates
#  the arguments passed via the command line.
#  Author: deterenkelt, 2018–2022
#
#  For licence see nadeshiko.sh
#


post_read_rcfile() {
	declare -g  icon_set
	declare -g  gui_default_preset

	local  preset_name
	local  gui_default_preset_exists
	local  icon_set_list

	if [ -v quick_run ]; then
		[ "${quick_run_preset:-}" ] && {
			if ! [     -r "$CONFDIR/$quick_run_preset" \
			       -a  -f "$CONFDIR/$quick_run_preset" ]
			then
				err "Quick run preset “$quick_run_preset” is not a readable file."
			fi
		}
		declare -gx postpone=t
		[ -v predictor ] && unset predictor
	else
		#  Pure default, Nadeshiko config may not exist, but this is expected.
		#  The presets in nadeshiko_presets must have file names only when
		#  there are 2+ of them.
		(( ${#nadeshiko_presets[*]} == 0 ))  \
			&& nadeshiko_presets=( [default]='nadeshiko.rc.sh' )

		if (( ${#nadeshiko_presets[*]} == 1 )); then
			gui_default_preset="${!nadeshiko_presets[@]}"
		else
			: ${gui_default_preset:=default}
			for preset_name in "${!nadeshiko_presets[@]}"; do
				[ "$gui_default_preset" = "$preset_name" ]  \
					&& gui_default_preset_exists=t
			done
			[ -v gui_default_preset_exists ]  \
				|| err "GUI default preset with name “$gui_default_preset” doesn’t exist."
		fi
	fi

	[ "$(type -t croptool_validate_config)" = 'function' ]  \
		&& croptool_validate_config

	[ "$icon_set" = random ] && {
		pushd "$RESDIR/icons" >/dev/null
		readarray -t icon_set_list < <(ls)
		icon_set=$(sed -rn "$((RANDOM % ${#icon_set_list[*]} + 1)) p" < <(ls))
		popd >/dev/null
	}
	ln -s "$RESDIR/icons/$icon_set" "$TMPDIR/icons"

	return 0
}


 # Processes arguments passed via command line
#
parse_args() {
	declare -g  postpone
	declare -g  quick_run

	local  arg
	local  ch_name
	local  help_func

	for arg in "${ARGS[@]}"; do
		if [[ "$arg" =~ ^(-h|--help)$ ]]; then
			show_help
			exit 0

		elif [[ "$arg" =~ ^(-v|--version)$ ]]; then
			show_licence
			exit 0

		elif [ "$arg" = 'postpone' ]; then
			postpone=t

		elif [[ "$arg" =~ ^quick[_-]?run$ ]]; then
			quick_run=t

		elif [[ "$arg" =~ ^--verbosity-help-([A-Za-z_][A-Za-z0-9_-]*)$ ]]; then
			ch_name=${BASH_REMATCH[1]}
			help_func="show_help_on_verbosity_channel_$ch_name"
			if	   [ -v VERBOSITY_CHANNELS[$ch_name]        ]  \
				&& [ "$(type -t "$help_func")" = 'function' ]
			then
				echo
				$help_func
				exit 0
			else
				err "No such verbosity channel: “$ch_name”."
			fi

		else
			err "“$arg”: parameter unrecognised.
			     For usage run the program with ${__so:-}-h${__s:-} or ${__so:-}--help${__s:-}."
		fi
	done

	return 0
}


return 0