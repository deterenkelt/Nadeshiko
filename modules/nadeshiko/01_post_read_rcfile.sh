#  Should be sourced.

#  01_post_read_rcfile.sh
#  Nadeshiko module to process the variables read from the meta, default
#  and user’s RC file(s).
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


compose_known_res_list() {
	declare -g  known_res_list

	local  bitres_profile
	local  swap
	local  i
	local  j

	for bitres_profile in ${!bitres_profile_*}; do
		[[ "$bitres_profile" =~ ^bitres_profile_([0-9]+)p$ ]]  \
			&& known_res_list+=( ${BASH_REMATCH[1]} )
	done
	for ((i=0; i<${#known_res_list[*]}-1; i++)); do
		for ((j=i+1; j<${#known_res_list[*]}; j++)); do
			(( ${known_res_list[j]} > ${known_res_list[i]} )) && {
				swap=${known_res_list[i]}
				known_res_list[i]=${known_res_list[j]}
				known_res_list[j]=$swap
			}
		done
	done
	return 0
}


 # Runs in 03_run_checks.sh during validation of the subitle settings.
#  This check is performed only when specific subtitle codecs are used.
#
validate_option_ffmpeg_subtitle_fallback_style() {

	[ "${ffmpeg_subtitle_fallback_style[*]}" ] && {

		info "Option: ffmpeg_subtitle_fallback_style"
		milinc

		[ "${ffmpeg_subtitle_fallback_style@a}" = 'A' ]  || {
			err "ffmpeg_subtitle_fallback_style must be of associated array type."
			config_error=t
		}

		local -n srtst=ffmpeg_subtitle_fallback_style

		[ -v srtst[Fontname] ]  && {
			#
			#  The “font families” in fc-list all have dashes prepended with
			#  a backslash. Weird but…
			#
			fc-list -q "${srtst[Fontname]//-/\\-}"  || {
				warn "“Fontname” specifies font name, that wasn’t found on this system: “""${srtst[Fontname]}""”.
				       Consider running
				          $ fc-list | grep \"${srtst[Fontname]}\""
				config_error=t
			}
		}

		[ -v srtst[Fontsize] ]  && {
			[[ "${srtst[Fontsize]}" =~ ^([8-9]|1?[1-9][0-9])$ ]]  || {
				redmsg "“Fontsize” must be a number in range 8…199."
				config_error=t
			}
	    }

		[ -v srtst[PrimaryColour] ]  && {
			[[ "${srtst[PrimaryColour]}" =~ ^\&H[0-9A-Fa-f]{8}$ ]]  || {
				redmsg "“PrimaryColour” is specified in wrong format. Refer to the
				         description in the default configuration file."
				config_error=t
			}
		}

		[ -v srtst[SecondaryColour] ]  && {
			[[ "${srtst[SecondaryColour]}" =~ ^\&H[0-9A-Fa-f]{8}$ ]]  || {
				redmsg "“SecondaryColour” is specified in wrong format. Refer to the
				         description in the default configuration file."
				config_error=t
			}
		}

		[ -v srtst[OutlineColour] ]  && {
			[[ "${srtst[OutlineColour]}" =~ ^\&H[0-9A-Fa-f]{8}$ ]]  || {
				redmsg "“OutlineColour” is specified in wrong format. Refer to the
				         description in the default configuration file."
				config_error=t
			}
		}

		[ -v srtst[BackColour] ]  && {
			[[ "${srtst[BackColour]}" =~ ^\&H[0-9A-Fa-f]{8}$ ]]  || {
				redmsg "“BackColour” is specified in wrong format. Refer to the
				         description in the default configuration file."
				config_error=t
			}
		}

		[ -v srtst[Bold] ]  && {
			[[ "${srtst[Bold]}" =~ ^(-1|0)$ ]]  || {
				redmsg "“Bold” takes only values “-1” and “0”."
				config_error=t
			}
		}

		[ -v srtst[BorderStyle] ]  && {
			[[ "${srtst[BorderStyle]}" =~ ^(1|3)$ ]]  || {
				redmsg "“BorderStyle” takes only values “1” and “3”."
				config_error=t
			}
		}

		[ -v srtst[Outline] ]  && {
			[[ "${srtst[Outline]}" =~ ^$FLOAT$ ]]  || {
				redmsg "“Outline” must be a number."
				config_error=t
			}
		}

		[ -v srtst[Shadow] ]  && {
			[[ "${srtst[Shadow]}" =~ ^$FLOAT$ ]]  || {
				redmsg "“Shadow” must be a number."
				config_error=t
			}
		}

		if [ -v config_error ]; then
			err "Wrong subtitle fallback style specified."
		else
			# info "Style: OK"
			mildec
		fi

	}

	return 0
}


 # Validates the target colour space set of properties and makes the
#  “target_colour_space” variable point at that set, if the validation was
#  successful.
#
#  Runs in 03_run_checks.sh during validation of the colour space
#  settings, if conversion is requested.
#
validate_option_target_colour_space() {
	declare -g  colour_space_filter

	local  t_csp="${target_colour_space:-}"

	[ -v do_colour_space_conversion ] || return 0

	info "Option: target_colour_space"
	milinc

	[[ "$t_csp" =~ ^[A-Za-z0-9_]+$ ]]  || {
		redmsg "Impossible value set for the config option “target_colour_space”:
		        the target may contain only the characters of the Latin alphabet,
		        digits and the underscore character."
		err "Config – target_colour_space: wrong value."
	}

	local -n t="target_colour_space_$t_csp"  \
		&& [[ "${t@a}" =~ .*A.* ]]  \
		|| {
			redmsg "The value specified to the “target_colour_space” config option must
			        have a corresponding array “target_colour_space_$t_csp” set, which
			        would describe this target, but the array isn’t defined."
			err "Config – target_colour_space: no array describing the target."
		   }

	local -n available_matrix_coefs_ref=known_cspace_matrix_coefs_for_filter_${colour_space_filter}
	local available_matrix_coefs="$(
		IFS='|'; echo "${!available_matrix_coefs_ref[*]}"
	)"
	[[ "${t[matrix_coefs]:-}" =~ ^($available_matrix_coefs)$ ]]  || {
		available_matrix_coefs="$(
			IFS=','; echo "${!available_matrix_coefs_ref[*]}"
		)"
		redmsg "\$target_colour_space_${t_csp}[matrix_coefs] must be one of: $available_matrix_coefs."
		err "Config – target_colour_space: target has wrong matrix coefficients set."
	}

	local -n available_primaries_ref=known_cspace_primaries_for_filter_${colour_space_filter}
	local available_primaries="$(
		IFS='|'; echo "${!available_primaries_ref[*]}"
	)"
	[[ "${t[primaries]:-}" =~ ^($available_primaries)$ ]]  || {
		available_primaries="$(
			IFS=','; echo "${!available_primaries_ref[*]}"
		)"
		redmsg "\$target_colour_space_${t_csp}[primaries] must be one of: $available_primaries."
		err "Config – target_colour_space: target has wrong primaries set."
	}

	local -n available_tone_curves_ref=known_cspace_tone_curves_for_filter_${colour_space_filter}
	local available_tone_curves="$(
		IFS='|'; echo "${!available_tone_curves_ref[*]}"
	)"
	[[ "${t[tone_curve]:-}" =~ ^($available_tone_curves)$ ]]  || {
		available_tone_curves="$(
			IFS=','; echo "${!available_tone_curves_ref[*]}"
		)"
		redmsg "\$target_colour_space_${t_csp}[tone_curve] must be one of: $available_tone_curves."
		err "Config – target_colour_space: target has wrong tone curve set."
	}

	[[ "${t[colour_range]:-}" =~ ^(tv|pc)$ ]]  || {
		redmsg "\$target_colour_space_${t_csp}[colour_range] must be either “tv” (for the
		        limited range) or “pc” (for the full range)."
		err "Config – target_colour_space: target has wrong colour range set."
	}

	# info "Target colour space is defined properly."
	unset target_colour_space
	declare -gn target_colour_space="target_colour_space_${t_csp}"
	mildec
	return 0
}


 # Validates the “ffmpeg_filter_normalize_smoothing” config option, which is
#  a part of colour space conversion parameters. And in particular, the part
#  that deals with high dynamic range content (HDR). See the description
#  in “defconf/…”.
#
#  NB “-ize” – it’s from the ffmpeg filter name.
#
validate_option_ffmpeg_filter_normalize_smoothing() {
	declare -g  ffmpeg_filter_normalize_smoothing

	local smoothing="$ffmpeg_filter_normalize_smoothing"

	if [[ "$smoothing" =~ ^$INT$ ]]; then
		if (( smoothing >= 0  &&  smoothing <= 99999 )); then
			ffmpeg_filter_normalize_smoothing="$smoothing"
			return 0
		else
			err "Config – ffmpeg_filter_normalize_smoothing: out of range (0…99999)."
		fi

	elif [[ "$smoothing" =~ ^(${INT})xFR$ ]]; then
		ffmpeg_filter_normalize_smoothing=$((   ${BASH_REMATCH[1]}
		                                      * ${src_v[frame_rate]%\.*} ))
		return 0

	else
		redmsg "\$ffmpeg_filter_normalize_smoothing accepts value in one of the
		        two forms:
		          - a number of frames (an integer number in range 0…99999);
		          - “NxFR”, where FR is a literal “FR”, standing for “frame rate”
		            and N is a multiplier, an integer number."
		err "Config – ffmpeg_filter_normalize_smoothing: wrong value, see the log file."
	fi

	retrun 0
}


 # Runs in 03_run_checks.sh during validation of the target
#  directory path, if necessary.
#
validate_option_target_directory_if_the_intended_is_nonwriteable() {
	local -n target_dir='target_directory_if_the_intended_is_nonwriteable'

	case "${target_dir:-}" in
		auto)
			if [ -d "${XDG_DESKTOP_DIR:-}"  -a  -w "${XDG_DESKTOP_DIR:-}" ]; then
				target_dir="$XDG_DESKTOP_DIR"

			elif [ -d "$HOME"  -a  -w "$HOME" ]; then
				target_dir="$HOME"

			elif [ -d '/tmp'  -a  -w '/tmp' ]; then
				target_dir='/tmp'

			else
				redmsg "The config option “target_directory_if_the_intended_is_nonwriteable”
				        couldn’t autoselect the path: neither of \$XDG_DESKTOP_DIR, \$HOME
				        or /tmp are writeable directories."
				err "Config – target_directory_if…_nonwriteable: bad value"
			fi
			;;

		*)
			[ -d "${target_dir:-}"  -a  -w "${target_dir:-}" ] || {
				redmsg "Option “target_directory_if_the_intended_is_nonwriteable” should be
				        either set to “auto” or specify a path to a writeable directory."
				err "Config – target_directory_if…_nonwriteable: bad value"
			}
	esac

	return 0
}


post_read_rcfile() {
	declare -g  muxing_sets
	declare -g  codec_name_as_formats
	declare -g  minimal_bitrate_pct
	declare -g  scale
	declare -g  RC_DEFAULT_scale
	declare -g  deinterlace
	declare -g  RC_DEFAULT_deinterlace
	declare -g  seekbefore
	declare -g  RC_DEFAULT_seekbefore
	declare -g  seekbefore_s
	declare -g  custom_output_framerate_set
	declare -g  known_res_list=()
	declare -g  CACHED_ATTS_LIFESPAN_MONTHS

	local  pct_varname
	local  pct_var
	local  vcodec=${ffmpeg_vcodec//-/_}
	local  varname
	local  i

	[ -v desktop_notifications ] && {
		#  Enabling it as early as possible, or the errors about wrong argu-
		#  ments won’t have desktop notifications.
		bahelite_load_module 'messages_to_desktop'
		check_required_utils
	}

	#  Processing the rest of the variables
	max_size_default_name="$max_size_default"
	declare -gn max_size_default=max_size_${max_size_default}
	declare -gn codec_name_as_formats=${vcodec}_codec_name_as_formats
	declare -gn muxing_sets=${vcodec}_muxing_sets
	declare -gn minimal_bitrate_pct=${vcodec}_minimal_bitrate_pct

	compose_known_res_list

	#  NB “scale” from RC doesn’t set force_scale!
	if [ "${scale:-}" = no ]; then
		unset scale
	else
		scale="${scale%p}"
		RC_DEFAULT_scale=$scale
	fi

	if [ "$deinterlace" = 'auto' ]; then
		deinterlace='auto'
		RC_DEFAULT_deinterlace='auto'

	elif [[ "$deinterlace" =~ ^$PBOOL_TRUE$ ]]; then
		deinterlace=t
		RC_DEFAULT_deinterlace=t

	else
		unset deinterlace
		unset RC_DEFAULT_deinterlace
	fi

	if [ "$libx264_keyint" = auto ]; then
		unset libx264_keyint

	elif (( libx264_keyint < 2  || libx264_keyint > 999999 )); then
		#
		# ^ we already know that if it’s not set to auto,
		#   then it must be an $INT.
		#
		err "Config – “libx264_keyint” must be in range 2…999999."
	fi

	if [ "$libx264_keyint_min" = auto ]; then
		unset libx264_keyint_min

	elif (( libx264_keyint_min < 2  || libx264_keyint_min > 999999 )); then
		#
		# ^ we already know that if it’s not set to auto,
		#   then it must be an $INT.
		#
		err "Config – “libx264_keyint_min” must be in range 2…999999 and be not higher than libx264_keyint/2+1."
	fi

	#  The config option is united for simplicity, but in the code we make
	#  a distinction between flag options and options bearing value.
	#
	if [ -v seekbefore ]; then
		seekbefore=t
		RC_DEFAULT_seekbefore=t
		seekbefore_s="$seekbefore"
	else
		unset seekbefore
		unset RC_DEFAULT_seekbefore
	fi

	[ -v cached_attachments_lifespan_in_months ]  \
		&& CACHED_ATTS_LIFESPAN_MONTHS=$cached_attachments_lifespan_in_months

	video_sps_threshold_default=$(
		sed -rn 's/^\s*video_sps_threshold=([0-9\.]+)/\1/p'  \
		        "$DEFCONFDIR/nadeshiko.10_main.rc.sh"
	)
	compare_versions "$video_sps_threshold" '<' "$video_sps_threshold_default" && {
		warn "Config – ${__bri}${__y}video_sps_threshold is set to “$video_sps_threshold”, which is lower than the recom-
		      mended value of “$video_sps_threshold_default”. Lower values increase the number of possible re-runs
		      whithout any benefits.${__s} Details for this option may be found in the file
		      “$DEFCONFDIR/nadeshiko.10_main.rc.sh”."
	}

	# [ -v time_stat ] && ffmpeg_input_options+=( -benchmark )  # test later

	return 0
}


return 0