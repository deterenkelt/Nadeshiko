#  Should be sourced.

#  03_run_checks.sh
#  Nadeshiko module that combines defconf, user’s rcfile and commandline
#  options together. Then it verifies, that the options safely overlap each
#  other, and that their dependencies are satisfied. The module runs addi-
#  tional checks on the source video to determine the frame count and scene
#  complexity of the fragment to be cut.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # Check, that all needed utils are in place and ffmpeg supports
#  user’s encoders.
#
check_basic_util_support() {
	declare -g  REQUIRED_UTILS
	declare -g  REQUIRED_UTILS_HINTS
	#  Encoding modules may need to know versions of ffmpeg or its libraries.
	declare -g  ffmpeg_version_output
	declare -g  ffmpeg_ver
	declare -g  libavutil_ver
	declare -g  libavcodec_ver
	declare -g  libavformat_ver

	local ffmpeg="$ffmpeg -hide_banner"

	info "Checking for third-party tools"
	milinc

	REQUIRED_UTILS+=(
		#  For encoding. 3.4.2+ recommended.
		ffmpeg

		#  For retrieving data from the source video
		#  and verifying resulting video.
		mediainfo

		#  For the parts not retrievable with mediainfo
		#  and as a fallback option.
		ffprobe

		#  To parse mediainfo output.
		xmlstarlet

		#  To determine the mime types of video and subtitle files
		#  correctly, “file” command is not sufficient.
		mimetype

		#  However, only “file” reports correctly the MIME type
		#  for MPEG transport stream files of the old format.
		file

		#  For the floating point calculations, that are necessary
		#  e.g. in determining scene_complexity.
		bc
	)
	REQUIRED_UTILS_HINTS+=(
		[ffprobe]='ffprobe comes along with FFmpeg.
		https://www.ffmpeg.org/'

		[mimetype]='mimetype is a part of File-MimeInfo.
		https://metacpan.org/pod/File::MimeInfo'
	)
	check_required_utils

	#  Checking ffmpeg version
	ffmpeg_version_output=$($ffmpeg -version)
	# (( ${VERBOSITY_CHANNELS[log]} > 0 ))  \
	# 	&& echo "$ffmpeg_version_output" > "$LOGDIR/ffmpeg_version"
	ffmpeg_ver=$(
		sed -rn '1 s/ffmpeg version (\S+) .*/\1/p' <<<"$ffmpeg_version_output"
	)
	libavutil_ver=$(
		sed -rn '/^libavutil/ { s/^[^\/]+\/(.+)$/\1/; s/\s//g; p }'  \
			<<<"$ffmpeg_version_output"
	)
	libavcodec_ver=$(
		sed -rn '/^libavcodec/ { s/^[^\/]+\/(.+)$/\1/; s/\s//g; p }'  \
			<<<"$ffmpeg_version_output"
	)
	libavformat_ver=$(
		sed -rn '/^libavformat/ { s/^[^\/]+\/(.+)$/\1/; s/\s//g; p }'  \
			<<<"$ffmpeg_version_output"
	)

	is_version_valid "$libavutil_ver"  \
		|| err "Cannot determine libavutil version!
		        Incorrect version for libavutil: “$libavutil_ver”."

	is_version_valid "$libavcodec_ver"  \
		|| err "Cannot determine libavcodec version!
		        Incorrect version for libavcodec: “$libavcodec_ver”."

	is_version_valid "$libavformat_ver"  \
		|| err "Cannot determine libavformat version!
		        Incorrect version for libavformat: “$libavformat_ver”."

	info "System ffmpeg: $ffmpeg_ver
	      libavutil   $libavutil_ver
	      libavcodec  $libavcodec_ver
	      libavformat $libavformat_ver"
	if	   compare_versions "$libavutil_ver" '<' "$libavutil_minver"  \
		|| compare_versions "$libavcodec_ver" '<' "$libavcodec_minver"  \
		|| compare_versions "$libavformat_ver" '<' "$libavformat_minver"
	then
		err 'The FFmpeg version you are running is too old!'"
		$(cat <<-EOF | column -t  -o '    '  -N ' ','Needed','In your FFmpeg' | sed -r "s/.*/$__mi&/g"
		libavutil      $libavutil_minver+      $libavutil_ver
		libavcodec     $libavcodec_minver+     $libavcodec_ver
		libavformat    $libavformat_minver+    $libavformat_ver
		EOF
		)"
	fi

	mildec
	return 0
}


check_muxing_set() {
	declare -g ffmpeg_muxer
	declare -g container

	local  combination
	local  combination_passes
	local  muxer_info
	local  ffmpeg="$ffmpeg -hide_banner"
	local  i

	[ "$container" = auto ] && {
		for combination in "${muxing_sets[@]}"; do
			[[ "$combination" =~ ^([^[:space:]]+)[[:space:]]+$ffmpeg_acodec$ ]]  \
				&& container="${BASH_REMATCH[1]}"  \
				&& break
		done
	}
	for combination in "${muxing_sets[@]}"; do
		[[ "$combination" =~ ^$container[[:space:]]+$ffmpeg_acodec$ ]]  \
			&& combination_passes=t  \
			&& break
	done
	#  Muxer is the FFmpeg name of the chosen container.
	#  It may or may not correspond to the common name (file extension).
	[[ "$container" =~ ^[A-Za-Z0-9_-]+$ ]]  \
		|| err "Invalid container: “$container”."

	ffmpeg_muxer=${ffmpeg_muxers[$container]}
	muxer_info=$($ffmpeg -h muxer="$ffmpeg_muxer" | head -n1)
	[[ "$muxer_info" =~ ^Muxer\ $ffmpeg_muxer\  ]]  \
		|| err "FFmpeg doesn’t support muxing into “$container” container."
	[ -v combination_passes ] || {
		redmsg "“$container”, “$ffmpeg_vcodec” and “$ffmpeg_acodec” cannot be used together.
		        Possible combinations for container “$container” are:
		        $(for ((i=0; i<${#muxing_sets[@]}; i++)); do
		              vcodec=$ffmpeg_vcodec
		              acodec=${muxing_sets[i]##* }
		              container=${muxing_sets[i]%% *}
		              echo "  $((i+1)). $vcodec + $acodec → $container"
		          done)"
		err 'Incompatible set of container format and A/V codecs.'
	}
	return 0
}


select_reserved_space_by_container_type() {
	#  “||…” because [ -v ${container}_space_reserved_frames_to_esp ]
	#  is impossible.
	declare -gn container_space_reserved_frames_to_esp=${container}_space_reserved_frames_to_esp  || {
		warn "No predicted overhead table is specified for the $container container.
		      If the video exceeds maximum file size, re-encode will be inevitable."
		declare -g container_space_reserved_frames_to_esp=( [0]=0 )
	}
	return 0
}


check_for_mutually_exclusive_options() {
	[ -v crop  -a  -v scale ]  \
		&& err "“crop” and “scale” cannot be used at the same time."
	return 0
}


check_times() {
	#  This flag is set when Nadeshiko is encoding entire file – it’s a
	#  reminder to not place timestamps in the output file name.
	#
	declare -g  encoding_entire_file

	 # Getting video duration to compare with the requested.
	#  It’s given a large number when not present, but if the assumption
	#  would be replaced and that value might become an invalid string,
	#  add a “|| true” to this.
	#
	new_time_array  source_duration  \
	                "${src_v[duration_total_s_ms]}"

	[ ! -v time1  -a  ! -v time2 ] && {
		#  If neither Time1 nor Time2 is set, use the full video duration.
		new_time_array  time1  "00:00:00.000"  \
			|| err 'Couldn’t set Time1 to 00:00:00.000'
		new_time_array  time2  "${source_duration[ts]}"  \
			|| err "Couldn’t set Time2 to ${source_duration[ts]}"
		encoding_entire_file=t
	}

	(( ${time2[total_ms]} > ${time1[total_ms]} ))  \
		&& declare -gn start='time1'  stop='time2'  \
		|| declare -gn start='time2'  stop='time1'
	(( ${time1[total_ms]} == ${time2[total_ms]} ))  \
		&& err 'Time1 and Time2 are the same.'

	new_time_array   duration  \
	                "$(total_ms_to_total_s_ms "$((   ${stop[total_ms]}
	                                               - ${start[total_ms]} ))" )"

	if [ -v source_duration ]; then
		if [[ "${source_duration[total_s]}" =~ ^[0-9]+$ ]]; then
			#  Trying to prevent negative space_for_video_track.
			(( ${start[total_s]} > ${source_duration[total_s]} ))  \
				&& err "Start time ${start[ts]#00:} is behind the end."
			(( ${stop[total_s]} > ${source_duration[total_s]} ))  \
				&& err "Stop time ${stop[ts]#00:} is behind the end."
		else
			unset source_duration
		fi
	else
		#  We still can try to work, there is a check
		#  for negative space_for_video_track ahead.
		unset source_duration
	fi
	return 0
}


check_target_directory() {
	declare -g  where_to_place_new_file
	declare -g  target_directory_if_the_intended_is_nonwriteable

	local  old_path

	[ -d "$where_to_place_new_file"  -a  -w "$where_to_place_new_file" ] || {
		validate_option_target_directory_if_the_intended_is_nonwriteable
		old_path="$where_to_place_new_file"
		where_to_place_new_file="$target_directory_if_the_intended_is_nonwriteable"
		warn "The requested target directory
		      $old_path
		      is not writeable! Encoded file will be placed to
		      $target_directory_if_the_intended_is_nonwriteable"
	}

	return 0
}


 # Frame count is used:
#   - In the calculation of expected muxing overhead.
#      Using output frame rate in the formula calculating muxing overhead is,
#      honestly speaking, a tongue-in-cheek solution, as the codecs may as
#      well use variable frame rate.
#   - As a rough reference for the amount of work done, when running pass 1.
#      FFmpeg doesn’t provide timestamps in the progress during pass 1, so
#      frame count is the only point of reference, that progress can be
#      tied to.
#
#  The global $frame_count refers to the count of frames /in the output file,/
#  so ${src_v[frame_count]} won’t help us here. The value is rounded up.
#
calc_frame_count() {
	declare -g  frame_count

	frame_count=$( echo "scale=3;  fc =    (   ${duration[total_s_ms]}  \
	                                         + ${loop_last_frame_s:-0}  \
	                                       )                            \
	                                     * $frame_rate;                 \
	                     scale=0;  (fc+0.5)/1"  \
	               | bc
	)
    return 0
}


check_subtitles() {
	local  known_sub_codecs_list

	known_sub_codecs_list=$(IFS='|'; echo "${known_sub_codecs[*]}")

	if [ -v src_s[external_file]  ]; then
		[[ "${src_s[codec]}" =~ ^($known_sub_codecs_list)$ ]] || {
			#  External subs can only be requested,
			#    so it’s always an error, when they can’t be rendered.
			#  This error may be triggered, if mpv or a wrapper like
			#    smplayer load every text file in the directory, thinking
			#    it’s subtitles. The option is called “fuzzy matching”.
			err "Cannot use external subtitles – “${src_s[mimetype]}” type is not supported."
		}

	else
		#  Unlike with a video that simply has no subtitle stream,
		#  having one that we cannot encode is always an error.
		[[ "${src_s[codec]}" =~ ^($known_sub_codecs_list)$ ]]  \
			|| err "Cannot use built-in subtitles – “${src_s[codec]}” type is not supported. Try “nosubs”?"
		#  Sic! ---------------------------------------------^^^^^

	fi

	[[ "${src_s[codec]}" =~ ^(subrip|srt|webvtt|vtt|mov_text)$ ]]  \
		&& validate_option_ffmpeg_subtitle_fallback_style

	return 0
}


check_subtitles_delay() {
	declare -g  sub_delay

	[[ "${sub_delay/\./}" =~ ^\-?0+$ ]] && {
		warn "Subtitle delay specified as 0, unsetting it."
		unset sub_delay
	}
	return 0
}


check_subtitles_scale() {
	declare -g  sub_scale

	[ -v sub_scale  -a  "${src_s[codec]}" != 'ass' ] && {
		redmsg "Option “sub_scale” can be applied only to ASS/SSA subtitles.
		        SRT and (Web)VTT font size can be adjusted in the config file."
		err "Option “sub_scale” cannot be applied to subtitles of “${src_s[codec]}” type."
	}

	[[ "${sub_scale/\./}" =~ ^0+$ ]] && {
		warn "Subtitle scale specified as 0, unsetting it.
		      (Setting “nosubs” would be saner.)"
		unset sub_scale
	}
	[[ "$sub_scale" =~ ^1(\.0+)$ ]] && {
		warn "Subtitle scale specified as 1, unsetting it.
		      (Value of 1.0 would do nothing.)"
		unset sub_scale
	}
	return 0
}


 # Check for non-square pixel aspect ratio
#  Primarily found in the content of 480p…576p era, pixels in the bit stream
#  may have an aspect ratio differing from 1.0. For subtitles using the “sub-
#  title” ffmpeg filter, this unusual now aspect ratio has to be converted
#  to 1.0 at encoding time, or the subs will come out stretched.
#
check_for_non_square_pixel_ar() {
	declare -g  needs_sar_conversion

	local  sub_type

	[ -v subs ] || return 0

	info 'Checking for pixel aspect ratio.'
	milinc
	[  -v src_v[pixel_aspect_ratio]  ] || {
		warn 'Couldn’t retrieve pixel aspect ratio. The output
		      may come out stretched.'
		mildec
		return 0
	}
	if [[ "${src_v[pixel_aspect_ratio]}" =~ ^1(\.0+|)$ ]]; then
		info 'Video has square pixels, can continue safely.'
	else
		for sub_type in "${subtitle_types_to_restretch[@]}"; do
			[ "${src_s[codec]}" = "$sub_type" ]  \
				&& needs_sar_conversion=t
		done
		if [ -v needs_sar_conversion ]; then
			info 'Video has non-square pixels and the “subtitle” filter is to be used.
			      Applying a rule to convert aspect ratio to 1.0 to avoid stretched
			      subtitles or frame.'
		else
			info 'The fragment won’t need the “subtitle” filter,
			      SAR correction doesn’t seem to be needed.'
		fi

	fi
	mildec
	return 0
}


check_audio() {
	declare -g  src_a
	#  Check the decoder
	return 0
}


check_audio_delay() {
	declare -g  audio_delay

	[[ "${audio_delay/\./}" =~ ^\-?0+$ ]] && {
		warn "Audio delay specified as 0, unsetting it."
		unset audio_delay
		return 0
	}

	warn "Audio delay feature is an experimental implementation.
	      The reason for it is that shifting PTS back or forth is more common
	      and less prone to errors, when it’s done on the entire file. And when
	      it’s applied to a fragment to be cut, one of the audio boundaries hap-
	      pens to be outside of the fragment’s. While it’s been worked around
	      for positive delays to a good enough result, for negative delays it’s
	      still a problem. See release notes for v2.30.0 for more details."

	[ -v audio_delay_is_negative ]  \
		&& warn "Negative audio delay was detected.
		         This isn’t supported well. If you encode a small video (within
		         1–2 seconds), expect to hear silence instead of audio."
	return 0
}


check_for_resolution_scale_crop() {
	declare -g  closest_res
	declare -g  closest_lowres_index
	declare -g  needs_bitrate_correction_by_origres
	declare -g  needs_bitrate_correction_by_cropres

	local i

	 # Original resolution is a prerequisite for the intelligent mode.
	#    Dumb mode with some hardcoded default bitrates bears
	#    little usefulness, so it was decided to flex it out.
	#    Intelligent and forced modes (that overrides things in the former)
	#    are the two modes now.
	#  Getting native video resolution is of utmost importance
	#    to not do accidental upscale. It is also needed for knowing,
	#    with which resolution to start scaling down, if needed.
	#
	[ -v src_v[resolution] ]  \
		|| err "Couldn’t determine source video resolution."

	#  Since the values in our bitrate-resolution profiles are given
	#  for 16:9 aspect ratio, 4:3 video and special ones like 1920×820
	#  would require less bitrate, as there’s less pixels.
	#
	[ -v src_v[is_16to9] ]  \
		|| warn "Couldn’t determine display aspect ratio.
		         Distribution of bitrate for non-16:9 resolutions may be unoptimal."
	[ "${src_v[is_16to9]:-}" = 'no' ]  \
		&& needs_bitrate_correction_by_origres=t

	[ -v src_v[resolution] ]  && {
		#  Videos with nonstandard resolutions must be associated
		#    with a higher profile.
		#  [ -v doesn’t work here, somehow O_o
		declare -p bitres_profile_${src_v[height]}p  &>/dev/null  || {
			for ((i=${#known_res_list[@]}-1; i>0; i--)); do
				(( src_v[height] > known_res_list[i] ))  \
					&& continue  \
					|| break
			done
			(( i == -1 )) && ((i++, 1))  # sic!
			closest_res=${known_res_list[i]}
			needs_bitrate_correction_by_origres=t
		}
	}

	[ -v crop  -a  ! -v crop_uses_profile_vbitrate ]  \
		&& needs_bitrate_correction_by_cropres=t

	[ -v scale ] && (( scale == src_v[height] )) && {
		warn "Disabling scale to ${scale}p – it is the native resolution."
		unset scale
	}
	[ -v scale ] && (( scale > src_v[height] )) && {
		warn "Disabling scale to ${scale}p – would be an upscale."
		unset scale
	}

	closest_lowres_index=0
	for ((i=0; i<${#known_res_list[@]}; i++ )); do
		# If a profile resolution is greater than or equal
		#   to the source video height, such resolution isn’t
		#   actually lower, and we don’t need upscales.
		# If we intend to scale down the source and the desired
		#   resolution if higher than the table resolution,
		#   again, it should be skipped.
		(
			[  -v src_v[height]  ] && (( known_res_list[i] >= src_v[height] ))
		)||(
			[ -v scale ] && (( known_res_list[i] > scale ))
		) \
		&& ((closest_lowres_index++, 1))
	done

	return 0
}


check_for_same_codecs() {
	declare -g  orig_vcodec_same_as_enc
	declare -g  orig_acodec_same_as_enc

	local codec_format

	shopt -s nocasematch
	for codec_format in ${vcodec_name_as_formats[@]}; do
		[[ "$codec_format" = "${src_v[format]}" ]]  \
			&& orig_vcodec_same_as_enc=t  \
			&& break
	done

	for codec_format in ${acodec_name_as_formats[@]}; do
		[[ "$codec_format" = "${src_a[format]}" ]]  \
			&& orig_acodec_same_as_enc=t  \
			&& break
	done
	shopt -u nocasematch

	return 0
}


 # Determines how fast the scenes in the video change.
#  For example, a 30 seconds video may contain 2–3 static scenes with somebody
#    having a dinner under a tree, or it may be an opening, which is filled
#    with dynamic scenes that change every 2–3 seconds on top of that.
#  These two videos would have different requirements for the bitrate to pre-
#    serve quality, hence for the dynamic one we want to lock available bit-
#    rates to the desired value (and if it won’t fit, then IMMEDIATELY go one
#    resolution lower, unless the difference between the desried bitrate and
#    what fits is marginal.
#  See also:
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Researches%E2%80%89%E2%80%93%E2%80%89Video-complexity
#
#  SETS
#    scene_complexity to either “static” or “dynamic”.
#
determine_scene_complexity() {
	declare -g  scene_complexity
	declare -g  scene_complexity_assumed

	[ -v scene_complexity ] && {
		info "Scene complexity is requested as ${__bri}${__w}$scene_complexity${__s}."
		return 0
	}

	info 'Determining scene complexity.
	      It takes 2–20 seconds depending on video.'
	[ -v seekbefore ]  \
		&& msg '…may take a bit more in “seekbefore” mode.'
	milinc

	local  total_scenes
	local  is_dynamic
	local  some_ffmpeg_opts

	[ -v seekbefore ]  \
		&& some_ffmpeg_opts=( -ss "${ffmpeg_adaptive_seek_ss1[ts]}"
			                   -i "${src[path]}"
			                  -ss "${ffmpeg_adaptive_seek_ss2_offset[ts]}"
			                  -to "${ffmpeg_adaptive_seek_to_offset[ts]}"  )  \
		|| some_ffmpeg_opts=(
			                  -ss "${start[ts]}"
			                  -to "${stop[ts]}"
			                  -i "${src[path]}"  )

	total_scenes=$(
		FFREPORT="file=$LOGDIR/${LOGNAME//:/\\:}.ffmpeg-scene-complexity:level=32"  \
		$ffmpeg  -hide_banner  -v error  -nostdin  \
		          "${some_ffmpeg_opts[@]}"  \
		         -vf "select='gte(scene,0.3)',metadata=print:file=-"  \
		         -an -sn -f null -
	) || err "Couldn’t determine scene complexity: ffmpeg error."


	 # There may be no scenes to detect, if the video is very short (or it
	#  has very little change and is basically one big scene). In this case,
	#  “$total_scenes” will be an empty string. Then grep will print the count
	#  of lines as 0 and will quit with return code 1 as no matches were found.
	#  “|| true” is to prevent grep from triggering an error here.
	#
	total_scenes=$(
		grep -cE '^lavfi\.scene_score=0\.[0-9]{6}$' <<<"$total_scenes" || true
	)


	[[ "$total_scenes" =~ ^[0-9]+$ ]] && {
		((total_scenes++, 1))  # add the initial scene
		sps_ratio=$(
			echo "scale=2; ${duration[total_s_ms]} / $total_scenes" | bc
		) || true
		#                      dynamic < $video_sps_threshold < static
		is_dynamic=$(
			echo "$sps_ratio < $video_sps_threshold" | bc
		) || true
		[[ "$is_dynamic" =~ ^[01]$ ]] && {
			case "$is_dynamic" in
				0) # 0 = negative in bc
					scene_complexity='static'
					;;
				1) # 1 = positive in bc
					scene_complexity='dynamic'
					;;
			esac
		}
	}

	#  If the type is undeterminable, allow the use
	#  of bitrate-resolution ranges.
	[ -v scene_complexity ] || {
		warn 'Cannot determine video complexity, assuming dynamic.'
		scene_complexity='dynamic'
		scene_complexity_assumed=t
	}

	mildec
	return 0
}


check_for_vbitrate_abitrate_scale_locks() {
	declare -g  forced_vbitrate
	declare -g  forced_scale
	declare -g  vbitrate_locked_on_desired

	 # If any of these variables are set by this time,
	#  they force (fixate) corresponding settings.
	[ -v vbitrate ] && forced_vbitrate=t
	#  scale is special, it can be set in RC file.
	[ -v scale  -a  ! -v RC_DEFAULT_scale ] && forced_scale=t

	[ "$scene_complexity" = dynamic ] && {
		info "Locking video bitrates on desired values."
		vbitrate_locked_on_desired=t
	}

	return 0
}


check_video_encoder_support() {
	declare -g  missing_components

	local  encoder_info
	local  pix_fmt_valid

	[ ! -v "vcodec_name_as_formats_${ffmpeg_vcodec//-/_}" ]  \
		&& err "Video codec is unknown to Nadeshiko: “$ffmpeg_vcodec”.
		        Perhaps ffmpeg can use it, but someone must describe it
		        as formats, its combinations with containers and the
		        bit rate tables first."

	# ^ It’s important to quit here instead of setting “missing components”,
	#   because further checks would imply, that we have necessary codec
	#   description in defconf/metaconf.
	#
	local -n chosen_pix_fmt=${ffmpeg_vcodec//-/_}_pix_fmt

	#  A check with “ffmpeg -h encoder=<name>” produces a more stable result
	#  than “ffmpeg -codecs | grep …” that was used before. It is confirmed,
	#  that when a codec is not supported, ffmpeg returns
	#      “Codec '<name>' is not recognized by FFmpeg”.
	#  (FFmpeg N-94168-g0f39ef4db2, 5 July 2019)
	encoder_info=$(
		$ffmpeg -hide_banner -h encoder="$ffmpeg_vcodec"
	)
	encoder_info_header=$(head -n1 <<<"$encoder_info")
	[[ "${encoder_info_header:-}" =~ ^Encoder\ $ffmpeg_vcodec\  ]] || {
		redmsg "“$ffmpeg_vcodec” video codec is missing."
		missing_components=t
	}

	#  Checking, if the encoder supports chosen pixel format.
	supported_pixel_formats=($(
		sed -rn 's/^\s*Supported pixel formats: (.*)$/\1/p' <<<"$encoder_info"
	))
	(( ${#supported_pixel_formats[*]} != 0 )) && {
		for pix_fmt in ${supported_pixel_formats[@]}; do
			[ "$pix_fmt" = "$chosen_pix_fmt" ] && {
				pix_fmt_valid=t
				break
			}
		done
		[ -v pix_fmt_valid ]  \
			|| err "Config – wrong value for “${ffmpeg_vcodec//-/_}_pix_fmt”.
			        $ffmpeg_vcodec doesn’t support pixel format “$chosen_pix_fmt”.
			        It must be one of: ${supported_pixel_formats[*]}."
	}

	return 0
}


check_audio_encoder_support() {
	declare -g  missing_components

	local  encoder_info

	[ ! -v "acodec_name_as_formats_${ffmpeg_acodec//-/_}" ]  \
		&& err "Audio codec is unknown to Nadeshiko: “$ffmpeg_vcodec”.
		        Perhaps ffmpeg can use it, but someone must describe it
		        as formats, its combinations with containers and the
		        bit rate tables first."

	# ^ It’s important to quit here instead of setting “missing components”,
	#   because further checks would imply, that we have necessary codec
	#   description in defconf/metaconf.
	#

	#  It is confirmed, that when a codec is not supported, ffmpeg returns
	#      “Codec '<name>' is not recognized by FFmpeg”.
	#  (FFmpeg N-94168-g0f39ef4db2, 5 July 2019)
	encoder_info=$(
		$ffmpeg -hide_banner -h encoder="$ffmpeg_acodec" | head -n1
	)
	[[ "$encoder_info" =~ ^Encoder\ $ffmpeg_acodec\  ]] || {
		redmsg "“$ffmpeg_acodec” audio codec is missing."
		missing_components=t
	}
	return 0
}


 # Late stage check, for the route with required hardsub.
#
check_subtitle_filter_support() {
	declare -g  missing_components

	local  filter_info
	local  subtitle_filter
	local  helpmsg
	local  font_rendering_problems
	#
	#  ASS filter:
	#    - OpenType font features can be enabled only with the “ass”
	#      filter, not available with “subtitles” filter;
	#    - depends on libass;
	#    - “Same as the subtitles filter, except that it doesn’t require
	#      libavcodec and libavformat to work.” ― FFmpeg filters documentation.
	#
	#  Subtitles fitler:
	#    - depends on libass;
	#
	#  At this point src_s[codec] is guaranteed to have a value
	#    from $known_sub_codecs.
	#
	case "${src_s[codec]}" in
		ass|ssa)
			subtitle_filter='ass'
			helpmsg="Make sure that FFmpeg is compiled with “--enable-libass”."
			;;

		subrip|srt|webvtt|vtt|mov_text)
			subtitle_filter='subtitles'
			helpmsg="Make sure that FFmpeg is compiled with “--enable-libass”."
			;;

		dvd_subtitle|hdmv_pgs_subtitle)
			subtitle_filter='overlay'
			#  No $helpmsg, because dependencies for the “overlay” filter
			#  are not known.
			;;
	esac
	helpmsg=${helpmsg:+$helpmsg$'\n'}
	helpmsg+="Try encoding without subtitles? (Add “nosub” to cmdline options.)"
	#
	#  It is confirmed, that when “ass” subtitle codec IS NOT supported,
	#  `ffmpeg -h filter=ass` returns
	#      “Unknown filter 'ass'.”
	#  (FFmpeg N-94168-g0f39ef4db2, 5 July 2019)
	#
	#  Note, unlike with audio and video codecs, “ass” and “subtitles”
	#  are “filters”, not “encoders”.
	#
	filter_info=$(
		$ffmpeg -hide_banner -h filter="$subtitle_filter" | head -n1
	)
	[[ "$filter_info" =~ ^Filter\ $subtitle_filter ]] || {
		redmsg "“$subtitle_filter” subtitle filter is missing."
		[ -v helpmsg ] && msg "$helpmsg"
		missing_components=t
	}

	 # If the subtitle filter to be used is “ass”, then also check,
	#  that the OpenType features are supported.
	#
	[[ "$subtitle_filter" =~ ^(ass|subtitles)$ ]] && {
		#
		#  ffmpeg with libavutil-56.30.100 has --enable-fontconfig
		#  ffmpeg with libavutil-56.31.100 has --enable-libfontconfig
		#
		[[ "${ffmpeg_version_output}"  =~  .*--enable-(lib|)fontconfig.* ]]  || {
			warn "FFmpeg was built without fontconfig!"
			ffmpeg_missing+=( [fontconfig]=yes )
			font_rendering_problems=t
		}
		#
		#  Fixed so as to conform to the issue with fontconfig above. Actual
		#  differences in (lib)freetype weren’t spotted, but anticipated.
		#
		[[  "${ffmpeg_version_output}"  =~ .*--enable-(lib|)freetype.* ]]  || {
			warn "FFmpeg was built without freetype!"
			ffmpeg_missing+=( [freetype]=yes )
			font_rendering_problems=t
		}

		[ -v font_rendering_problems ]  \
			&& warn "Without freetype and fontconfig support FFmpeg will not be able
			         to render subtitles “as in the player”, or even render them at all."
	}

	return 0
}


check_misc_util_support() {
	declare -g REQUIRED_UTILS
	declare -g REQUIRED_UTILS_HINTS

	local arg

	for arg in "$@"; do
		case "$arg" in
			time_stat)
				REQUIRED_UTILS+=(
					#  To output how many seconds the encoding took.
					#  Only pass1 and pass2, no fonts/subtitles extraction.
					time
				)
				;;
		esac
	done
	REQUIRED_UTILS_HINTS+=(
		[time]='time is found in the package of the same name.
		https://www.gnu.org/directory/time.html'
	)
	check_required_utils
	return 0
}


 # Saving time in “seekbefore” mode (adaptive seeking with moving “-ss”
#  and “to” after “-i” and placing another “-ss” before “-i”.
#
#  How far back before the actual Time1 we’ll seek, would depend on the reason,
#  why “seekbefore” mode is enabled: the cause may be the need of an earlier
#  key frame, or to balance luminocity in HDR content. In general we won’t
#  seek farther away from Time1, than 120 seconds.
#
#  More details about the “seekbefore” concept can be found in Release notes
#  for v2.28.0 and in the descriptions to config options whose name starts
#  with “seekbefore…”.
#
set_seekbefore_s() {
	declare -g  seekbefore_s
	declare -g  seekbefore_in_search_for_a_keyframe_adjusted

	local  format
	local  try_setting_seekbefore_by_keyframe
	local  closest_kf
	local  _sb_s

	[ -v seekbefore ] || return 0
	[ -v seekbefore_s ] && return 0

	 # In case a video would be both HDR with NCL /and/ would happen to be
	#  a transport stream, consider bigger requirement of HDR as more neces-
	#  sary. Hence the check on HDR comes first.
	#
	[ -v seekbefore_due_to_HDR_NCL_matrix_coefs ] && {
		let "seekbefore_s =
		       seekbefore_for_HDR_NCL_s > ${seekbefore_s:-0}
		       ?
		       seekbefore_for_HDR_NCL_s
		       :
		       ${seekbefore_s:-0}
		       , 1"  # guarantee a safe exit code, if zero is assigned.
	}

	[ -v seekbefore_due_to_tr_or_pr_stream  \
	    -o  \
	  -v seekbefore_due_to_x264_444_8x8_bug ] && {

		 # Try to further save a bit of time spent on decoding, by finding
		#  the precise position of the key frame before Time1. Decoding just
		#  1 second before the needed position and 120 seconds (the maximum
		#  possible with the current default) can make a difference on heavy
		#  files, even if we speak only about decoding of the file.
		#
		#  Not all codecs nowadays rely on key frames (VP9, AV1), but it’s
		#  hardly the case with those to get a TS or PS encoded with those.
		#  Should it happen, if the key frame won’t be found within the span
		#  of 120 seconds from Time1, the config defalut would be used. That
		#  is to say, we won’t get into a situation, when the file would have
		#  to be decoded wholly.
		#
		closest_kf=$(
			#
			#  Start seeking 120 seconds prior to Time1. Unfortunately,
			#    ffprobe’s “-read_intervals” recognizes only explicit time-
			#    stamps, and offsets may be only positive, not negative.
			#    Surprisingly enough, ffprobe recognises a negative value,
			#    but instead of applying it as a negative offest, it drops
			#    the value down to 0:00.000.
			#  Actually, to get the last key frame it is currently possible
			#    to request “-read_intervals "${start[ts]%#1}"” – and it
			#    will return exactly what’s needed – however, this is not
			#    in the hard specification, so it’s safer to retrieve
			#    a list from point to point and get the last line properly.
			#
			_seekbefore=$(( ${start[total_s_ceil]} - 120 ))
			(( _seekbefore < 0 )) && _seekbefore=0
			$ffprobe -v error  \
			        -select_streams V  \
			        -show_frames  \
			        -show_entries frame=pkt_pts_time  \
			        -skip_frame nokey  \
			        -sexagesimal  \
			        -of default=noprint_wrappers=1  \
			        -read_intervals "${_seekbefore}%${start[ts]}"  \
			        "${src[path]}"  \
			\
			| sed -rn 's/^pkt_pts_time=(.+)000$/\1/; T out; h; :out $ {g; p}'
		)
		is_valid_timestamp "$closest_kf"  && {
			_sb_s="$(ts_to_seconds "$closest_kf")"
			_sb_s=$(( ${start[total_s_ceil]} - _sb_s + 1 ))
			seekbefore_in_search_for_a_keyframe_adjusted=t
		}

		#  Covers both cases: when “$closest_kf” wasn’t a timestamp, and when
		#  _sb_s was not set yet, because the source isn’t H.264 or H.265
		#
		: ${_sb_s:=$seekbefore_in_search_for_a_keyframe_s}

		let "seekbefore_s =
		       _sb_s > ${seekbefore_s:-0}
		       ?
		       _sb_s
		       :
		       ${seekbefore_s:-0}
		       , 1"  # guarantee a safe exit code, if zero is assigned.
	}

	[ -v seekbefore_due_to_request_from_cmdline ] && {
		let "seekbefore_s =
		       seekbefore_for_cmdline_s > ${seekbefore_s:-0}
		       ?
		       seekbefore_for_cmdline_s
		       :
		       ${seekbefore_s:-0}
		       , 1"  # guarantee a safe exit code, if zero is assigned.
	}

	return 0
}


 # Make sure that “seekbefore” value points to a timestamp within the duration
#  bounds of the source file and convert its value in seconds to a timestamp,
#  which then would be set as the value for $ffmpeg_adaptive_seek_ss1 – the
#  actual value to be used in the ffmpeg command.
#
set_ffmpeg_adaptive_seek() {
	[ -v seekbefore ] || return 0

	# NB: “$ffmpeg_adaptive_seek_ss1”, “…_ss2_offset” and “…_to_offset”
	# must not be declared beforehand.
	declare -g  seekbefore_s

	local  _ss2_offset_s
	local  _ss2_offset_ms
	local  _to_offset_s
	local  _to_offset_ms

	 # Bringing the “seekbefore” /point/ to zero, if it’s negative. That’s for
	#  the cases, when Time1 is close to the beginning of the file – so close
	#  that the 5…30…120 seconds, that we pointed the decoder back ended up
	#  beyond 0:00.000 in the negative time. Don’t think, that if you just
	#  “return 0” here, the decoder will have to start decoding from 0:00.000 –
	#  it won’t. (Yes, it’s a retarded thought, but it came once even to me.)
	#
	if ((    seekbefore_s == 0
		  || ${start[total_s]} - seekbefore_s < 0 ))
	then
		new_time_array  ffmpeg_adaptive_seek_ss1 '0:00.000'

	else
		new_time_array  ffmpeg_adaptive_seek_ss1 $((   ${start[total_s]}
		                                             - seekbefore_s  ))
	fi

	 # Altering “${start[@]}”
	#  Now, as we’re going  to use “-ss … -i … -ss … -to …”, thus /two “-ss”/,
	#  we must take into account, than in this case the second “-ss” now /can-
	#  not set the timestamp directly/ – by the ffmpeg rules, if there’s two
	#  “-ss”, the one before “-i” sets the timestamp /directly/ (e.g. as
	#  HH:MM:SS.ms), and the one that goes after “-i” must be /an offset/
	#  from the timestamp specified with the first “-ss”. The “-to …” part
	#  also has to be an offset. Hence more math…
	#
	_ss2_offset_s=${start[total_s_ms]%\.*}  # precise, unlike “[total_s]”
	_ss2_offset_ms=${start[ms]}
	_ss2_offset_s=$(( _ss2_offset_s - ${ffmpeg_adaptive_seek_ss1[total_s]} ))
	new_time_array  ffmpeg_adaptive_seek_ss2_offset "${_ss2_offset_s}.${_ss2_offset_ms}"

	_to_offset_s=${stop[total_s_ms]%\.*}  # precise, unlike “[total_s]”
	_to_offset_ms=${stop[ms]}
	_to_offset_s=$(( _to_offset_s - ${ffmpeg_adaptive_seek_ss1[total_s]} ))
	new_time_array  ffmpeg_adaptive_seek_to_offset "${_to_offset_s}.${_to_offset_ms}"

	return 0
}


 # A non-critical check for a source encoded with a libx264 version below 151,
#  using 4:4:4 chroma and CABAC option 8x8dct, resulting in garbage on regular
#  conversion. If noticed, print a bright warning about using “seekbefore”
#  option.
#
check_for_x264_444_8x8_bug() {
	declare -g  seekbefore
	declare -g  seekbefore_due_to_x264_444_8x8_bug

	local  x264_short_ver

	[ "${src_v[encoded_library_name]:-}" = 'x264' ]  || return 0

	x264_short_ver=$(
		sed -rn 's/.*core ([0-9]+) .*/\1/p' <<<"${src_v[encoded_library_version]}"
	)
	[[ "$x264_short_ver" =~ ^[0-9]+$ ]]  || return 0

	(( 10#${x264_short_ver} < 151 ))  || return 0

	[ "${src_v[chroma_subsampling]}" = '4:4:4' ] || return 0

	[[ "${src_v[encoded_library_settings]}" =~ \ 8x8dct=1\  ]]  || return 0

	warn "${__bri}${__y}Detected a potential bug: the source file was encoded with a specific ver-
	                    sion of x264 and uses 4:4:4 chroma subsampling and 8x8dct CABAC feature.
	                    The encoded file will quite probably have garbage, so, in order to prevent
	                    this, the “seekbefore” option will be turned on.${__s}"
	warn "Fulldecode is ${__y}ON${__s}.
	      The encoding will take more time than usual."
	seekbefore=t
	seekbefore_due_to_x264_444_8x8_bug=t
	return 0
}


 # Unless fixed frame rate is requested, preserve the original rate for videos,
#  whose fps is between 23…30 fps (that is, for most videos). Quality of many
#  videos, especially where it wasn’t that good to begin with, would suffer
#  from the frame rate change. (See defconf setting description for details.)
#
#  Since it was discovered, that some codecs (libx265) use default rate of
#  60 fps, it was decided that frame rate should be a mandatory output option.
#
check_for_frame_rate() {
	declare -g  frame_rate
	declare -g  motion_interp_allowed
	declare -g  motion_interp_filter_needed

	#  Converting rate from defconf to decimal notation, if it’s specified
	#  as a fraction.
	#
	[[ "$frame_rate" =~ ^($INT)/($INT)$ ]]  \
		&& frame_rate=$( echo "scale=3; $frame_rate" | bc )

	if [ -v frame_rate_fixed ]; then
		#  Value from defconf to be used for all cases. Assume, that motion
		#  interpolation is needed for all cases, except when the input and
		#  output frame
		#
		[ "$(echo "${src_v[frame_rate]:-0} == $frame_rate" | bc)" = '0' ]  \
			&& [ -v motion_interp_allowed ]  \
				&& motion_interp_filter_needed=t

	else
		#  Adapt frame rate to the input.
		#
		[[ ${src_v[frame_rate]:-} =~ ^([1-9]|[1-9][0-9]*)(\.[0-9]+|)$ ]]  && {
			if ((    ${src_v[frame_rate]%.*} >= 30  \
			      || ${src_v[frame_rate]%.*} <  23  ))
			then
				#  Using default frame rate from defconf + motion interp.
				#  filter
				[ -v motion_interp_allowed ]  \
					&& motion_interp_filter_needed=t

			else
				#  Using the input frame rate (better quality, frame-exact
				#  conversion). The global $frame_rate, the output option,
				#  takes its value from either $src_v[frame_rate] or
				#  $src_v[frame_rate_max]. Both are already converted
				#  to decimal notation.
				#
				[ "${src_v[frame_rate_mode]:-}" = VFR  -a  -v src_v[frame_rate_max] ]  \
					&& frame_rate=${src_v[frame_rate_max]}  \
					|| frame_rate=${src_v[frame_rate]}
			fi
		}
	fi

	return 0
}


check_colour_space_data() {
	declare -g  do_colour_space_conversion          # this one is set outside
	declare -g  do_only_colour_range_conversion     # and this one is set here
	declare -g  colour_space_filter

	local  filter_cannot_be_used_safely
	local  ignored_cspace


	info "Validating the colour space in the source."
	milinc

	#  If the colour range in the source couldn’t be determined as either
	#  “tv” or “pc” (limited or full), it’s set now to “input” for zscale
	#  to pick whatever it finds reasonable.

	if [ -v src_v[cspace_matrix_coefs] ]  \
		&& [ -v src_v[cspace_primaries] ]  \
		&& [ -v src_v[cspace_tone_curve] ]  \
		&& [ -v src_v[cspace_colour_range] ]
	then
		info "The data is complete and valid."
		src_v[cspace_valid]=t
	else
		unset src_v[cspace_valid]
	fi

	if [ -v src_v[cspace_valid] ]  \
		&& [ "${src_v[cspace_matrix_coefs]}" = "${target_colour_space[matrix_coefs]}" ]  \
		&& [ "${src_v[cspace_tone_curve]}" = "${target_colour_space[tone_curve]}" ]  \
		&& [ "${src_v[cspace_primaries]}" = "${target_colour_space[primaries]}" ]
	then
		info "The source and the target colour spaces match, so there’s no need
		      for conversion."
		unset  do_colour_space_conversion
	fi

	if	[ "${src_v[cspace_colour_range]:-}" ]  \
		&& [ "${src_v[cspace_colour_range]:-}" != "${target_colour_space[colour_range]}" ]  \
		&& [ ! -v do_colour_space_conversion ]
	then
		do_only_colour_range_conversion=t
	fi

	for ignored_cspace in "${preserve_colour_spaces[@]}"; do
		[ "${src_v[cspace_matrix_coefs]}" = "$ignored_space" ] && {
			info "The source colour space matches one from the list to be preserved.
			      Disabling conversion."
			unset  do_colour_space_conversion
			break
		}
	done

	if [ ! -v src_v[cspace_valid] ]  \
		&& [ ! -v force_colour_space_conversion ]
	then
		warn "Not enough metadata is present in the source file to consider
		      its colour space information complete. Colour space conversion
		      won’t be possible. (Unless it is forced.)"
		unset  do_colour_space_conversion
	fi

	mildec
	[ -v do_colour_space_conversion ] || return 0


	info "Validating the compatibility of the source colour space
	      with the “${colour_space_filter}” filter."
	milinc

	local -n available_matrix_coefs_ref=known_cspace_matrix_coefs_for_filter_${colour_space_filter}
	local available_matrix_coefs="$(
		IFS='|'; echo "${!available_matrix_coefs_ref[*]}"
	)"
	[[ "${src_v[cspace_matrix_coefs]}" =~ ^($available_matrix_coefs)$ ]]  || {
		warn "Matrix coefficients in the source cannot be read by the
		      “${colour_space_filter}” filter, which is doing colour
		      space conversion."
		filter_cannot_be_used_safely=t
		if [ -v force_colour_space_conversion ]; then
			warn "Setting the source matrix coefficients to autodetection."
			src_v[cspace_matrix_coefs]='input'
		else
			unset  do_colour_space_conversion
		fi
	}

	local -n available_primaries_ref=known_cspace_primaries_for_filter_${colour_space_filter}
	local available_primaries="$(
		IFS='|'; echo "${!available_primaries_ref[*]}"
	)"
	[[ "${src_v[cspace_primaries]}" =~ ^($available_primaries)$ ]]  || {
		warn "Primaries in the source cannot be read by the “${colour_space_filter}” filter,
		      which is doing colour space conversion."
		filter_cannot_be_used_safely=t
		if [ -v force_colour_space_conversion ]; then
			warn "Setting the source colour space primaries to autodetection."
			src_v[cspace_primaries]='input'
		else
			unset  do_colour_space_conversion
		fi
	}

	local -n available_tone_curves_ref=known_cspace_tone_curves_for_filter_${colour_space_filter}
	local available_tone_curves="$(
		IFS='|'; echo "${!available_tone_curves_ref[*]}"
	)"
	[[ "${src_v[cspace_tone_curve]}" =~ ^($available_tone_curves)$ ]]  || {
		warn "Tone curve in the source cannot be read by the “${colour_space_filter}” filter,
		      which is doing colour space conversion."
		filter_cannot_be_used_safely=t
		if [ -v force_colour_space_conversion ]; then
			warn "Setting the source colour space tone curve to autodetection."
			src_v[cspace_tone_curve]='input'
		else
			unset  do_colour_space_conversion
		fi
	}


	if [ -v do_colour_space_conversion ]; then
		[ -v filter_cannot_be_used_safely ]  \
			&& warn "Colour space conversion will be performed, but the success ${__bri}${__y}is not guaranteed.${__s}"
	else
		warn "Colour space conversion will be disabled for safety. The encoding
		      will proceed without running $colour_space_filter filter."
	fi

	mildec
	return 0
}


check_colour_space_filters() {
	declare -g  missing_components

	local  filter_info

	[ -v do_colour_space_conversion ] || return 0

	if	[ -v src_v[cspace_valid] -o  -v force_colour_space_conversion ]; then
		filter_info=$(
			$ffmpeg -hide_banner -h filter='zscale' | head -n1
		)
		[[ "$filter_info" =~ ^Filter\ zscale ]] || {
			redmsg "zscale filter is required for the colour space conversion,
			        but it’s missing."
			msg "Make sure that FFmpeg is compiled with “--enable-libzimg”."
			missing_components=t
		}

		[ -v ffmpeg_filter_use_normalize ] && {
			filter_info=$(
				$ffmpeg -hide_banner -h filter='normalize' | head -n1
			)
			[[ "$filter_info" =~ ^Filter\ normalize ]] || {
				redmsg "normalize filter is required for the colour space conversion,
				        but it’s missing. (You can disable this filter in the config
				        file, however.)"
				missing_components=t
			}
		}
	fi

	return 0
}


 # Late stage check, employed when encountering the route with colour space
#  conversion and a mismatch in the dynamic range of tone curves between the
#  source and the target colour spaces.
#
check_colour_space_filters_extra() {
	declare -g  missing_components

	local  filter_info
	local  filter_name

	for filter_name in tonemap normalize; do
		filter_info=$(
			$ffmpeg -hide_banner -h filter="$filter_name" | head -n1
		)
		[[ "$filter_info" =~ ^Filter\ $filter_name ]]  || {
			redmsg "Filter “$filter_name” is required for the colour space conversion,
			        but it’s missing."
			missing_components=t
		}
	done

	return 0
}


check_for_HDR_NCL_matrix_coefs() {
	declare -g  seekbefore
	declare -g  seekbefore_due_to_HDR_NCL_matrix_coefs

	local  ncl_mc

	for ncl_mc in ${HDR_NCL_matrix_coefs[@]}; do
		[ "${src_v[cspace_matrix_coefs]:-}" = "$ncl_mc" ] && {
			seekbefore=t
			seekbefore_due_to_HDR_NCL_matrix_coefs=t
			info "The video has colour space properties indicating HDR content
			      with non-constant luminance. Turning “seekbefore” mode on."
			break
		}
	done
	return 0
}


check_for_interlace() {
	declare -g  deinterlace_forced
	declare -g  deinterlace_autoselected
	declare -g  src_v
	declare -g  missing_components

	local  filter_info

	if ! (                                                             \
	         (  [ -v deinterlace  -a  -v RC_DEFAULT_deinterlace ]      \
	            && [ "$deinterlace" = "$RC_DEFAULT_deinterlace" ]      \
	         )                                                         \
	         || [ ! -v deinterlace  -a  ! -v RC_DEFAULT_deinterlace ]  \
	     )
	then
		deinterlace_forced=t
	fi

	[ "${deinterlace:-}" = 'auto' ] && {
		#  Confirm the switch from ‘auto’ to ‘t’ or unset the flag.
		if [ "${src_v[scan_type]:-}" = 'Interlaced' ]; then
			deinterlace=t
		else
			unset deinterlace
		fi
		deinterlace_autoselected=t
	}

	[ -v deinterlace ] && {
		case "${src_v[scan_order]:-}" in
			TFF) deinterlace_filter_bwdif_opts[parity]='tff';;
			BFF) deinterlace_filter_bwdif_opts[parity]='bff';;
			*) deinterlace_filter_bwdif_opts[parity]='auto';;
		esac
	}

	filter_info=$(
		$ffmpeg -hide_banner -h filter='bwdif' | head -n1
	)
	[[ "$filter_info" =~ ^Filter\ bwdif ]] || {
		redmsg "bwdif filter is required for deinterlacing, but it’s missing."
		missing_components=t
	}


	return 0
}


choose_biggest_probe_duration() {
	declare -g  probe_duration

	local  pd=5
	local  a_pd="${src_a[probe_duration]:-5M}";  a_pd=${a_pd%M}
	local  s_pd="${src_s[probe_duration]:-5M}";  s_pd=${s_pd%M}

	(( a_pd > pd )) && pd=$a_pd
	(( s_pd > pd )) && pd=$s_pd

	probe_duration="${pd}M"

	return 0
}


check_for_loop_last_frame() {
	[ -v loop_last_frame ] || return 0

	declare -g  loop_last_frame_s

	[ -v loop_last_frame_compensatory_second ]  \
		&& let 'loop_last_frame_s++, 1'
	return 0
}


 # Verifies the input data further, prepares the necessary files and sets
#  (or unsets) variables. Uses only warnings and errors, info for the user
#  is printed later by display_settings().
#
run_checks() {
	#  This flag will be set, if this function completes. It’s used in situ-
	#  ations where it makes difference, whether the execution proceeds to
	#  encoding and where the exit is “early”. In a way its role is similar
	#  to $dryrun.
	declare -gx  run_checks_passed
	declare -g   missing_components
	declare -g   max_size

	[ -v src[path] ]  || err "Source video is not specified."

	#  The first check: to confirm that essential utilities like ffmpeg and bc
	#  are available (the paths in $ffmpeg and $ffprobe are set correctly).
	check_basic_util_support

	#  The second check begins with us grabbing the Video/Audio/Subtitle pro-
	#  perties with the help pf mediainfo and ffprobe and converting the values
	#  into a format usable for Nadeshiko. When two steps below we’ll be check-
	#  ing what ffmpeg components are present, we’ll need to see into the
	#  source file properties, to determine, whether we’ll need e.g. bwdif
	#  filter (for deinterlacing) or not.
	#
	source "$LIBDIR/gather_source_info.sh"
	#
	#  This is a costly step. At some point, this call was placed below other
	#  checks. The idea was that some of the simpler checks in this list might
	#  deny the execution, and wasting several seconds on grabbing the proper-
	#  ties would be futile. A thorough inspection confirmed, however, that
	#  this call needs to be placed here. The order, in which the checks depend
	#  one on another, states, that “check_muxing_set” depends on the batch of
	#  “ffmpeg components” checks, which in its turn depends on this.
	#
	#  $1 = Type of information to gather.
	#  $2 = name for the new global variable that will store the data.
	gather_source_info 'before-encode' 'src'

	info 'Verifying, that FFmpeg supports the required codecs and filters…'
	milinc
	#  The third check will Goes second, for it’s important to have $vcodec and $acodec set cor-
	#  rectly for checking muxing sets later.
	check_video_encoder_support

	#  The following checks have to be performed next, as they’re in the
	#  “ffmpeg components” list, and if anything is missing, the global
	#  “$missing_components” variable is set, which is checked later /once/.
	#

	[ -v audio ]  \
		&& check_audio_encoder_support

	[ -v subs ]  \
		&& check_subtitle_filter_support

	[ -v do_colour_space_conversion ] && {
		validate_option_target_colour_space
		validate_option_ffmpeg_filter_normalize_smoothing
		check_colour_space_data
		check_colour_space_filters
	}

	check_for_interlace

	choose_biggest_probe_duration

	[ -v missing_components ]  \
		&& err "FFmpeg doesn’t support required encoders or filters."
	mildec

	check_muxing_set

	select_reserved_space_by_container_type

	[ -v max_size ] || max_size=$max_size_default

	check_for_mutually_exclusive_options

	check_times

	check_target_directory

	calc_frame_count

	check_for_non_square_pixel_ar

	 # There are three sources for subtitles:
	#  - default subtitle track (if any present at all), ffmpeg’s -map 0:s:0?;
	#  - an external file, specified in the “subs” parameter
	#    in the command line as subs=/path/to/external/file;
	#  - an internal subtitle track, but not the default one: it’s specified
	#    through the command line as well, as subs:5 for example.
	#
	[ -v subs ] && {
		check_subtitles
		[ -v sub_delay ] && check_subtitles_delay
		[ -v sub_scale ] && check_subtitles_scale
		check_attachments_cache
	}

	[ -v audio ] && {
		check_audio
		[ -v audio_delay ] && check_audio_delay
	}

	check_for_resolution_scale_crop

	check_for_same_codecs

	check_for_x264_444_8x8_bug

	 # This is a check on HDR content with non-constant luminance, which also
	#  needs “seekbefore”. But for the HDR content it’s not critical to run
	#  scenecomp in “seekbefore” mode (there won’t be garbage). Running this
	#  check after scene complexity test will save a considerable amount
	#  of time.
	#
	check_for_HDR_NCL_matrix_coefs

	set_seekbefore_s

	 # Should run after any checks, that set “$seekbefore”.
	#  And after “set_seekbefore_s”
	#
	set_ffmpeg_adaptive_seek

	 # Should run after any checks, that set “$seekbefore” (primarily to
	#  avoid possible decoder’s garbage at the beginning) and after setting
	#  ffmpeg fastseek.
	#
	determine_scene_complexity

	check_for_frame_rate

	check_for_loop_last_frame

	check_for_vbitrate_abitrate_scale_locks

	check_misc_util_support  ${time_stat:+time_stat}  \
	                         ${ffmpeg_progressbar:+ffmpeg_progressbar}

	echo
	run_checks_passed=t
	return 0
}


return 0