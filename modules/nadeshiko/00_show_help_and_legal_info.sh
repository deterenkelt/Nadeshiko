#  Should be sourced.

#  00_show_help_and_legal_info.sh
#  Nadeshiko module to output help and legal use information.
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko.sh
#


show_help() {
	local b="${__bright}"
	local g="${__green}"
	local w="${__white}"
	local y="${__yellow}"
	local bg="${__bright}${__green}"
	local s="${__stop}"

	 # Use this line instead to check that the output fits into 78 columns.
	#
	#  When restoring the $SHOW_HELP_PAGER line, there should be an empty
	#  line left between it and the header of the manual page.
	#
	# cat <<-EOF |& ansifilter |& grep -nE '^.{79,}$' || info 'All fits!'
	$SHOW_HELP_PAGER  >/dev/tty  <<-EOF
	   
	                      ${__bri}${__blue}Nadeshiko usage and list of options${s}

	Usage

	  ${b}./nadeshiko.sh${s}  [${b}start_time${s}] [${b}stop_time${s}] [<${b}other options${s}>] <${b}source_video${s}>


	Workflow options

	  <${bg}source_video${s}>
	      Path to the source video file. The only required option.

	  <${bg}start_time${s}>
	  <${bg}stop_time${s}>
	      Timestamps marking the bounds of the fragment to cut. When not speci-
	      fied, Nadeshiko encodes entire video. Any time format is valid:
	          11:23:45.670   =  11 h 23 min 45 s 670 ms
	             23:45.1     =  23 min 45 s 100 ms
	               125.340   =  2 min 5 s 340 ms
	                 5       =  5 s
	      Padding zeroes aren’t required for anything above milliseconds:
	          1:12:5.0  =  01:12:05.000
	      while
	          1:12.5.1  =  01:12:05.100    (≠ 01:12:05.001)

	      Timestamps may be specified in any order.

	  ${b}subs${s}
	  ${b}nosubs${s}
	  ${b}subs:${s}<${bg}track_number${s}>
	  ${b}subs=${s}<${bg}external_file${s}>
	      Selects subtitles to burn into video (also called “hardsubbing”). The
	      ${b}subs${s} option turns the hardsub on, ${b}nosubs${s} switches it off.

	      ${bg}track_number${s} points to the number of the stream among subtitle
	      streams. First track is 0.

	      ${bg}external_file${s} points at subtitles in a standalone file.

	      Examples:
	        “nosubs” – disable hardcoding the subtitles.
	        “subs” – use autoselected subtitles. Unlike the config file option,
	            command line “subs” forces the addition of a track. That means,
	            that in case a video wouldn’t have a default track, the config
	            option “subs=yes” would ignore that and convert the video without
	            subtitles, but command line “subs” will throw an error, if there
	            would be no subtitles.
	        “subs:0” – use the first subtitles in the container. This is often the
	            default track, so in most cases it would have the same effect as
	            “subs”.
	        “subs:4” – use the fifth subtitle track.
	        “subs=/home/video/films/Stalker/Stalker.eng.ass” – use the specified
	            external subtitle track.

	      To see, what streams are in the file:
	        $ ffprobe video.mkv
	        $ mediainfo video.mkv

	      Supported internal subtitles:
	        ASS/SSA;
	        SubRip/WebVTT;
	        VobSub (DVD);
	        PGS and HDMV (Blu-ray disks).

	      Supported external subtitles:
	        ASS/SSA (.ass, .ssa);
	        SubRip/WebVTT (.srt, .vtt).

	      The mnemonic to remember the meaning of “:” and “=” signs is that ”:” is
	      a shorter “=”. In other words, the sign between the option and the argu-
	      ment indicates, how close are the subtitles to the video stream: shorter
	      “:” indicates closer proximity, hence is used for the built-in tracks,
	      while longer “=” hints at the “bigger distance” and points to an exter-
	      nal file.

	      Nadeshiko never adds subtitles as a separate stream – most players on
	      the web cannot play subtitles from a built-in stream, and those that
	      do, can’t display them decently anyway.

	  ${b}sub_delay=${s}<${bg}delay${s}>
	      Shift the timing of subtitle track by the specified time. Examples:
	        “sub_delay=5.365” – delay subtitles on 5.365 seconds.
	        “sub_delay=-1” – make subtitles appear faster on one second.

	  ${b}sub_scale=${s}<${bg}factor${s}>
	      Make subtitles bigger or smaller by multiplying font size on the
	      specified factor (0.01–19.99). The normal scale is 1.0. Examples:
	        “sub_scale=0.8” – make subtitles smaller on one fifth.
	        “sub_scale=2” – make subtitles twice as big.

	      This option will work only if hardsubbing is on and the original
	      subtitle format is SubStation Alpha (SSA/ASS).

	      Nadeshiko emulates the behaviour of mpv’s “sub-scale” option, which
	      affects only “normally positioned” lines. That is, lines whose posi-
	      tion is not altered by a \\pos() rule or the like (\\move(), \\org(),
	      \\frxNNN, \\fryNNN \\frzNNN). Scaling does affect subtitles that are
	      aligned to a screen edge (using the “numpad alignment” – \\anN).

	  ${b}audio${s}
	  ${b}noaudio${s}
	  ${b}audio:${s}<${bg}track_number${s}>
	  ${b}audio=${s}<${bg}external_file${s}>
	      Whether to encode the audio track.

	      ${bg}track_number${s} specifies the number of the stream among audio streams.
	      First track is 0.

	      ${bg}external_file${s} specifies a path to an audio track in a separate file.

	      Examples:
	        “noaudio” – do not add audio. The cut will be video only.
	        “audio” – encode the default audio track.
	        “audio:0” – use the first audio track. In most cases would be equal
	            to using just “audio”.
	        “audio:7” – use the eighth audio track.
	        “audio=/home/video/films/Stalker/Stalker.eng.mka” – use an external
	            audio track.

	      The mnemonic for “:” and “=” is explained above in the section
	      about the “subs” option.

	      To see, what streams are in the file:
	        $ ffprobe video.mkv
	        $ mediainfo video.mkv

	  ${b}audio_delay=${s}<${bg}delay${s}>
	      Shift the timing of audio track by the specified time. Examples:
	        “audio_delay=5.365” – delay audio on 5.365 seconds.
	        “audio_delay=-1” – advance audio track on one second.

	      ${y}Experimental!${s} Positive delays work well enough, but with negative
	      delays you may hear silence on short videos (1–2 seconds long).

	  [${b}size=${s}]<${b}tiny${s}|${b}small${s}|${b}normal${s}|${b}unlimited${s}>
	  [${b}size=${s}]<${bg}size${s}[${bg}k${s}|${bg}M${s}|${bg}G${s}]>
	      Sets the maximum file size, that the encoded file must fit to. For con-
	      venience, several aliases are defined in the configuration file. The
	      default values currently in effect are:
	        “tiny” = ${b}$max_size_tiny${s};
	        “small” = ${b}$max_size_small${s};
	        “normal” = ${b}$max_size_normal${s};
	        “unlimited” = ${b}$max_size_unlimited${s}.

	      It’s also possible to request the fragment to be encoded to an arbit-
	      rary size. In this case the argument is specified as a number with an
	      optional suffix: ${bg}size${s}[${bg}k${s}|${bg}M${s}|${bg}G${s}]. For example:
	        “size=500k” = 500 KiB;
	        “size=7M”   = 7 MiB;
	        “size=1G”   = 1 GiB.

	      When no command line option specifies the maximum size, config option
	      “max_size_default” selects one of the aforementioned aliases. Currently
	      it’s set to… ${b}$max_size_default_name${s}.

	  ${b}k=1000${s}
	  ${b}si${s}
	      This option switches the multiplier for the ${b}k${s}, ${b}M${s}, ${b}G${s} suffixes, specify-
	      ing the file size, from binary to decimal, i.e.

	          Normally                          With “si” or “k=1000”
	          1k = 1 KiB = 1024 Bytes           1k = 1 kB = 1000 Bytes
	          1M = 1 MiB = 1024×1024 B          1M = 1 MB = 1000×1000 B
	          1G = 1 GiB = 1024×1024×1024 B     1G = 1 GB = 1000×1000×1000 B

	      The option is needed only when you have problems with uploading .webm
	      or .mp4 to a server, that has the upload limit set in decimal units,
	      and not in binary.

	  ${b}scale=${s}<${bg}height${s}>[p]
	  <${bg}height${s}>${b}p${s}
	      An ad-hoc option to set the resolution limit. May be useful, when encod-
	      ing at full resolution is not needed or will take too long (a screen
	      recording, for example).

	      The option must specify an existing bitrate-resolution profile,
	      one of: 360p, 480p, 576p, 720p, 1080p or 2140p.

	  ${b}vb=${s}<${bg}bit_rate${s}[${bg}k${s}|${bg}M${s}|${bg}G${s}]>
	      Encode video with the specified median bit rate. A suffix may be app-
	      lied: vb=300000, vb=1200k, vb=2M. (“si” or “k=1000” does not affect
	      the suffix here.)

	      The use of ${b}vb${s} replaces the default algorithm of finding an appropriate
	      median bitrate. By default, the maximum allowed bitrate is calculated,
	      and then looked up in the bitrate-resolution profiles to determine,
	      which range would it fall to. The ${b}vb${s} option sets this bitrate
	      explicitly, and the rest of the encoding options – such as quantiser
	      settings – are taken from the bitrate-resolution profile, that was
	      chosen first. Allowed deviance from the median bitrate is calculated
	      in both cases (as with automatic calculation, as with setting it expli-
	      citly with “vb=”).

	      This option may be used together with ${b}unlimited${s} to make Nadeshiko
	      convert long (or big sized) videos for storage purposes.

	  [${b}crop=${s}]${bg}W${w}x${g}H${w}+${g}X${w}+${g}Y${s}
	  [${b}crop=${s}]${bg}W${w}:${g}H${w}:${g}X${w}:${g}Y${s}
	      Crop the video. The parameters must specify the ${b}W${s}idth, the ${b}H${s}eight of the
	      fragment, and the ${b}X${s}, ${b}Y${s} coordinates of its top left corner.

	      0,0  X —>                   Video frame
	         ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
	       Y ┃                                                             ┃
	         ┃                                                             ┃
	       │ ┃                                                             ┃
	       v ┃                                                             ┃
	         ┃                              Fragment to be cropped         ┃
	         ┃                       X, Y  ┌──────────────────────┐        ┃
	         ┃                             │                      │        ┃
	         ┃                             │                      │        ┃
	         ┃                             │                      │        ┃
	         ┃                             └──────────────────────┘ W, H   ┃
	         ┃                                                             ┃
	         ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

	      This option cannot be used together with ${b}scale${s}.

	  <${bg}folder_path${s}>
	      Place encoded file by the specified ${bg}folder_path${s}.

	  <${bg}config_file${s}>
	      Chooses an alternative configuration file (preset). May be just the
	      file name or a path.
	      There are several requirements:
	        - it must be an existing file in ~/.config/nadeshiko/;
	        - the file name must be in format “$MYNAME_NOEXT${bg}suffix${s}.rc.sh”;
	      The ${bg}suffix${s} may consist only of these characters:
	          ${b}A-Z a-z 0-9 . , : ; _ -${s}
	      and may not contain spaces. For the only (the default) preset, the
	      suffix isn’t needed. Presets are a useful feature, read about how
	      to use them on the wiki (see the link at the bottom).

	  ${b}fname_pfx=${s}<${bg}string${s}>
	      Specifies a custom string, that will be put in front of the file name
	      of the encoded file. Maximum length of the prefix is 200 B (some alpha-
	      bets or special characters may take two or three bytes).

	  ${b}loop_last_frame=${s}<${bg}seconds${s}>
	      Creates an effect of a pause at the end of the video. Essentially this
	      will multiply the last frame enough times to fill the specified amount
	      of ${bg}seconds${s}. This will extend the duration of the fragment (the normal
	      duration between Time1 and Time2), as necessary, hence also increase
	      the bit rate and disk space needed to encode the file and remain
	      within the size constraint.

	      This option is created to aid in making those short webm/mp4, that
	      would normally look abrupt when looped. The extended duration is meant
	      for short spans (3…7 seconds), longer durations are not recommended.
	      Nadeshiko accepts two short names for this option, ${b}llf=${s} and ${b}lfl=${s}
	      (the latter is an acronym for “last frame looped” – as this option is
	      sometimes reffered to in the wiki.

	      While with libvpx-vp9 codec looping the last frame is nearly cost-free
	      (as the extra frames exist only virtually, perusing the real last frame
	      data), libx264 /does add actual frames/ to fill the extra time. Keep
	      this in mind, if you encounter troubles with getting out of size limits.

	  ${b}seekbefore=${s}<${bg}seconds${s}>
	      Force the decoder to start reconstructing frames not on the Time1 posi-
	      tion, but earlier, on the specified amount of ${bg}seconds${s}.

	      Usually, there’s no need in this option. In most cases, when it’d be
	      needed, the “seekbefore” mode activates automatically, with an appropri-
	      ate amount of ${bg}seconds${s} passed to decoder to “rewind” it back a bit.
	      The typical cases include transport and program streams (.ts, .m2ts,
	      .mpts…), HDR content with non-constant luminance (though with the recent
	      test it still retains flickering) and a particular case with the source
	      encoded with a specific version of a certain encoding library. Artefacts
	      related to key frames typically appear like garbage in the first seconds
	      of the encoded file. If you would notice something like that – it may
	      make sense to try out this option.

	      This option replaces the old one – ${b}fulldecode${s}, which had a similar func-
	      tion, but could point the decoder only to 0:00.000. The old behaviour
	      may be forced with ${b}seekbefore=0${s}.

	      In case there might be broken files in abundance, there’s also a confi-
	      guration file option with the same name.

	  ${b}deinterlace=${s}<${b}on${s}|${b}off${s}|${b}auto${s}>
	      Controls the deinterlacing filter, that “removes the ‘comb’ effect”
	      from the video. This option overrides the configuration file setting.
	        - auto – enable the filter, if the metadata in the source file
	          report the video as interlaced (this is the default);
	        - on – force-enable the filter, disregard the metadata;
	        - off – force-disables the filter, disregarding the metadata.

	      A short variant of this option name ${b}deint=${s} is also accepted.

	      What the deinterlacing filter does is restoring the proper order of
	      pixel rows /on the decoding stage/. Some videos (that are old) would
	      have the video stream packed to file with rows of pixels switched for
	      technical purposes. The filter just allows to enforce decoding the
	      video with or without assumption about its inherent interlace – for
	      the case when the metadata would be missing or objectively wrong.

	      Once again: deinterlacing should only be applied to those videos,
	      whose frame structure bears interlace (lines interleaving being inher-
	      ent to how they are written in the video stream). If the video bears
	      a “comb” effect, and it isn’t actually interlaced, this means that
	      some wise guy forgot to turn it on when converting, and now those
	      shameful traces of wrongly decoded frames are burned into the video,
	      that you have at hand, – Nadeshiko’s “deint=on” cannot help here.


	Verbosity options

	  ${b}--verbosity${s} <${bg}channelA${w}=${g}level1${w},${g}channelB${w}=${g}level2${w},${s}…${b},${g}channelN${w}=${g}levelM${s}>
	      Sets verbosity for the specified channels. For the names and the
	      possible levels refer to the list of channels (see below). The
	      “Troubleshooting guide” on the wiki has a section, that describes,
	      when this option comes helpful.

	      This option and the ${b}VERBOSITY${s} environment variable accept
	      the same value.

	  ${b}--verbosity-list-channels${s}, ${b}--verbosity-list${s}
	      Displays the list of verbosity channels.

	  ${b}--quiet${s}
	      A synonym for ${b}--verbosity $VERBOSITY_QUIET_SET${s}.


	Informing options

	  ${b}--verbosity-help${s}
	      Displays the introduction to verbosity channels, what groups there are,
	      and how the channels work.

	  ${b}--verbosity-help-${g}Channel_name${s}
	      Shows the description for a particular channel: its purpose and what
	      information becomes available, when its verbosity level is raised.

	      Mind the capitalisation (or the lack of it) in channel names.

	  ${b}-h${s}, ${b}--help${s}
	      Shows this help.

	  ${b}-v${s}, ${b}--version${s}
	      Print program version and licence.


	Files

	  Config file: ${b}${__b}~/.config/nadeshiko/nadeshiko.rc.sh${s}
	  Logs: ${b}${__b}~/.cache/nadeshiko/logs/${s}
	  Fonts and subtitles cache: ${b}${__b}~/.cache/nadeshiko/attachments_cache/${s}


	About

	  Nadeshiko is a tool to cut small videos, that can be shared on the
	  internet. It aims to convert files, that are efficiently encoded and
	  are compatible with most devices. Before all, that means, that the video
	  can have only one audio track and that subtitles must be burned onto the
	  video. Compatibility also places restrictions on the encoding features,
	  that affect quality. Nadeshiko aims to provide only the “good enough”
	  quality, as the “best” is hardly achievable in the common case under
	  these restrictions.

	  Nadeshiko isn’t a universal conversion tool, neither it suits for the
	  purposes of archiving. It may, to some extent, be used to convert long
	  or large files, but if your aim is to keep some important stuff (HQ or
	  not) for a long time, “Lazily archive video”, that comes together with
	  Nadeshiko, is a better tool.


	Links

	  Wiki: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/wiki/${s}
	  Report bugs here: ${b}${__b}https://codeberg.org/deterenkelt/Nadeshiko/issues${s}

	                                                  Updated for Nadeshiko 2.30.0
	                                                               30 January 2025

	                                    ◈ ◈ ◈

	EOF
	#  Maximum 78 characters ————————————————————————————————————————————————————>|<——
	return 0
}


show_licence() {
	cat <<-EOF
	Author: deterenkelt, 2018–2025.
	Licence: GNU GPL version 3  <http://gnu.org/licenses/gpl.html>
	This is free software: you are free to change and redistribute it.
	There is no warranty, to the extent permitted by law.
	EOF
	return 0
}


return 0
