#  Should be sourced.

#  04_display_assessed_parameters.sh
#  Nadeshiko module with a function to display
#    - the information gathered about the source file;
#    - a mould of options set by defconf, user’s RC file and from the command
#      line, along with with which of the options were altered at runtime
#      (e.g. audio disabled because the source file had none and no external
#      audio track was specified).
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


prepare_raw_probe_duration_for_the_output() {
	#
	#  At this point the global $probe_duration is already determined, and the
	#  raw values in src_*[probe_duration] variables won’t be needed any more.
	#  We’ll use them one last time to display human-readable values.
	#
	#  [ts] and not [ts_short_no_ms] for it’s printed to a sort of table,
	#  where the full value to be expected (unlike with in-line mention
	#  within a debug message).
	#
	[ -v src_a[probe_duration] ]  && {
		src_a[probe_duration]=${probe_for_audio_required_time[ts]%\.*}
		if [[ "${src_a[probe_duration]}" =~ ^00:0 ]]; then
			src_a[probe_duration]=${src_a[probe_duration]#00:0}

		elif [[ "${src_a[probe_duration]}" =~ ^00: ]]; then
			src_a[probe_duration]=${src_a[probe_duration]#00:}

		elif [[ "${src_a[probe_duration]}" =~ ^0.: ]]; then
			src_a[probe_duration]=${src_a[probe_duration]#0}
		fi
	}

	[ -v src_s[probe_duration] ]  && {
		src_s[probe_duration]=${probe_for_subtitle_required_time[ts]%\.*}
		if [[ "${src_s[probe_duration]}" =~ ^00:0 ]]; then
			src_s[probe_duration]=${src_s[probe_duration]#00:0}

		elif [[ "${src_s[probe_duration]}" =~ ^00: ]]; then
			src_s[probe_duration]=${src_s[probe_duration]#00:}

		elif [[ "${src_s[probe_duration]}" =~ ^0.: ]]; then
			src_s[probe_duration]=${src_s[probe_duration]#0}
		fi
	}

	return 0
}


 # Prints to the user the accumulated set of options, that will affect the
#  encoding. This output combines the data from the RC file, options given
#  on the command line, and what the program itself enabled, disabled or
#  adjusted according to the specifics of the source video.
#
#  No decision on how to fit the fragment in the size limits
#  has been made yet.
#
#  Colour indication is as follows:
#    Dark green: when the final value of an option, i.e. that, which will be
#       in effect, equals the config file(s) default. Command line value has
#       not been set or agrees with the config value.
#    Bright white: the option, that was specified on the command line, over-
#       rides the config default.
#    Bright yellow: the option value was adjusted (turned on, off or set to
#       something different) accordign to the specifics of the source file
#       or fragment to be encoded (e.g. turning on bitrate corrections due
#       to display asepect ratio or cropping)
#    Dark red: when the option value forced by the user can produce unexpected
#       results (e.g. forcing colour space conversion while the source has
#       no proper colour space metadata – meaning the user attempts to guess
#       those properties right).
#
display_assessed_parameters() {
	local  hl
	local  crop_string
	local  src_varname
	local  src_varval
	local  key
	local  varname
	local  matrix_coefs
	local  primaries
	local  tone_curve
	local  colour_range
	local  keys_not_to_show

	prepare_raw_probe_duration_for_the_output

	info 'Source file properties:'
	milinc

	for src_varname in  src_c  src_v  src_a  src_s; do
		[ "$src_varname" = src_a ] && [ ! -v audio ] && continue
		[ "$src_varname" = src_s ] && [ ! -v subs  ] && continue
		local -n src_varval=$src_varname
		case $src_varname in
			src_c)
				msg 'Container'
				;;
			src_v)
				msg 'Video'
				;;
			src_a)
				msg 'Audio'
				;;
			src_s)
				msg 'Subtitles'
				;;
		esac
		milinc
		for key in $(sort <<<"$(IFS=$'\n'; echo "${!src_varval[*]}")") ; do
			#  Print everything, except “ffprobe”, which is a service property
			#  like “mediainfo”. If it were possible, all “ffprobe”s should
			#  have been one src[ffprobe] and not shown here.
			keys_not_to_show=(
				'ffprobe'
				'.*_mediainfo'
				'encoded_library_settings'
			)
			! [[ "$key" =~ ^($(IFS='|'; echo "${keys_not_to_show[*]}"))$ ]]  \
				&& msg "$key: ${src_varval[$key]}"
		done
		mildec
	done
	echo
	mildec

	#  The colours for all the output should be:
	#    - default colour for default/computed/retrieved data;
	#    - bright white colour indicates command line overrides;
	#    - bright yellow colour shows the changes, that the code applied itself.
	#  This includes lowering bitrates for 4:3 videos, going downscale,
	#   when the size doesn’t allow for encode at the native resolution etc.
	info 'Fragment properties:'
	milinc

	info "$ffmpeg_vcodec + $ffmpeg_acodec → $container"

	#  Highlight only overrides of the defaults.
	#  The defaults are defined in $RCFILE.

	if [ -v subs ]; then
		[ -v RC_DEFAULT_subs ]  \
			&& hl=${__g}  \
			|| hl="${__bri}"
		info "Subtitles are ${hl}ON${__s}."
	else
		[ ! -v RC_DEFAULT_subs ]  \
			&& hl=${__g}  \
			|| hl="${__bri}"
		info "Subtitles are ${hl}OFF${__s}."
	fi
	unset hl

	if [ -v audio ]; then
		[ -v RC_DEFAULT_audio ]  \
			&& hl=${__g}  \
			|| hl="${__bri}"
		info "Audio is ${hl}ON${__s}."
	else
		[ ! -v RC_DEFAULT_audio ]  \
			&& hl=${__g}  \
			|| hl="${__bri}"
		info "Audio is ${hl}OFF${__s}."
	fi
	unset hl

	if [ "$max_size" = "$max_size_default" ]; then
		info "Size to fit into: $max_size (kilo=$kilo)."
	else
		info "Size to fit into: ${__bri}$max_size${__s} (kilo=$kilo)."
	fi

	[ -v scale ] && {
		[ "${RC_DEFAULT_scale:-}" != "${scale:-}" ] && hl=${__bri}
		info "Scaling to ${hl:-}${scale}p${__s}."
	}
	unset hl

	[ -v crop ] && {
		crop_string="${__bri}$crop_w×$crop_h${__s}, X:$crop_x, Y:$crop_y"
		info "Cropping to: $crop_string."
	}

	if [ -v loop_last_frame ]; then
		info "Slice duration: ${duration[ts_short_no_ms]}"
		milinc
		msg "+ $loop_last_frame_s s for the looped part at the end.
		     Exactly (in total): $(( ${duration[total_s]} + loop_last_frame_s )).${duration[ms]} s"
		mildec
	else
		info "Slice duration: ${duration[ts_short_no_ms]} (exactly ${duration[total_s_ms]} s)."
	fi

	if [ -v seekbefore ]; then
		[ -v RC_DEFAULT_seekbefore ]  \
			&& hl="${__g}"  \
			|| hl="${__bri}"
		info "Seekbefore is ${hl}ON${__s}."
		milinc
		msg "due to:"
		milinc
		[ -v seekbefore_due_to_tr_or_pr_stream ]  \
			&& msg "⋅ transport or program stream"
		[ -v seekbefore_due_to_x264_444_8x8_bug ]  \
			&& msg "⋅ libx264 bug"
		[ -v seekbefore_due_to_HDR_NCL_matrix_coefs ]  \
			&& msg "⋅ HDR NCL colour matrix coefficients"
		[ -v seekbefore_due_to_request_from_cmdline ]  \
			&& msg "⋅ request from command line"
		mildec
		msg "Rewinding to $seekbefore_s seconds before Time1
		     Adaptive seek “ss1” is set to ${ffmpeg_adaptive_seek_ss1[ts_short]}
		     Adaptive seek “ss2” offset is set to ${ffmpeg_adaptive_seek_ss2_offset[ts_short]}
		     Adaptive seek “to” offset is set to ${ffmpeg_adaptive_seek_to_offset[ts_short]}"
		mildec
	else
		[ ! -v RC_DEFAULT_seekbefore ]  \
			&& hl="${__g}"  \
			|| hl="${__bri}"
		info "“Seekbefore” is ${hl}OFF${__s}."
	fi
	unset hl

	if [ -v loop_last_frame ]; then
		info "“Last frame looped” is ${__bri}ON${__s}."
		milinc
		if [ -v loop_last_frame_compensatory_second ]; then
			msg "Compensatory second is ${__g}added${__s}."
		else
			msg "Compensatory second is ${__g}not added${__s}."
		fi
		if [ -v loop_last_frame_use_alter_method ]; then
			msg "Method to try first: ${__g}alternative${__s}."
		else
			msg "Method to try first: ${__g}primary${__s}."
		fi
		msg "Loop duration is set to $loop_last_frame_s s.
		     New fragment duration equals $(( ${duration[total_s]} + loop_last_frame_s )).${duration[ms]} s."
		mildec
	else
		info "“Last frame looped” is ${__g}OFF${__s}."
	fi

	if [ -v deinterlace_autoselected ]; then
		hl="${__y}${__bri}"
	elif [ -v deinterlace_forced ]; then
		hl="${__bri}"
	else
		hl="${__g}"
	fi
	if [ -v deinterlace ]; then
		info "Deinterlacing filter is ${hl}ON${__s}"
	else
		info "Deinterlacing filter is ${hl}OFF${__s}"
	fi
	unset hl

	if [ -v do_colour_space_conversion ]; then
		[ -v RC_DEFAULT_do_colour_space_conversion ]  \
			&& hl="${__g}"  \
			|| hl="${__bri}"
		#  Highlight “ON” in red, when colour space conversion is forced
		#  with an unknown result.
		[ ! -v src_v[cspace_valid] ] && hl+="${__r}"
		info "Colour space conversion is ${hl}ON${__s}."
	else
		if [ ! -v RC_DEFAULT_do_colour_space_conversion ]; then
			hl="${__g}"
		else
			#  If it’s softly disabled (because source colour space matches
			#  the destination or because user has set the type found in the
			#  source to be ignored) (white colour). If it’s because metadata
			#  were unavailable (and the conversion wasn’t forced – apparent-
			#  ly), then it’s not the way things should normally go (yellow
			#  colour).
			[ -v src_v[cspace_valid] ]  \
				&& hl=${__bri}  \
				|| hl="${__y}${__bri}"
		fi
		info "Colour space conversion is ${hl}OFF${__s}."
	fi
	unset hl

	if [ -v scene_complexity_assumed ]; then
		info "Scene complexity: assumed to be ${__bri}$scene_complexity${__s}."
	else
		if [ -v scene_complexity_forced ]; then
			info "Scene complexity: ${__bri}$scene_complexity${__s}."
		else
			info "Seconds per scene: $sps_ratio."
			milinc
			msg "Scene complexity: $scene_complexity."
			mildec
		fi
	fi
	info "Frame count: $frame_count"

	[ -v sub_delay -o -v audio_delay ]  && {
		info 'Delays requested:'
		milinc
		[ -v sub_delay ] && info "subtitle track: $sub_delay"
		# [ -v audio_delay ] && info "audio track: $audio_delay"
		mildec
	}

	local -n vcodec_pix_fmt=${ffmpeg_vcodec//-/_}_pix_fmt
	info "Encoding to pixel format “$vcodec_pix_fmt”."
	[ "$vcodec_pix_fmt" != yuv420p ] && {
		warn "Using pixel format other than “yuv420p” will lead to
		        - increased file size;
		        - inability to calculate the muxing overhead precisely
		          (with the default Nadeshiko bitrate-resolution profiles).
		      Nadeshiko was not made to be used with the chosen pixel format. Hence,
		      neither the possibility of the encode, nor its quality can be guaranteed.
		      You’re going to assume the risk and should expect a significant drop in
		      quality and at a significant increase in the encoding time for files rea-
		      ching the maximum size limit."
	}

	if [ -v needs_bitrate_correction_by_origres ]  \
		|| [ -v needs_bitrate_correction_by_cropres ]
	then
	  	infon 'Bitrate corrections to be applied: '
		[ -v needs_bitrate_correction_by_origres ]  \
			&& echo -en "by ${__y}${__bri}orig_res${__s} "
		[ -v needs_bitrate_correction_by_cropres ]  \
			&& echo -en "by ${__y}${__bri}crop_res${__s} "
		echo
	fi

	if [ -v do_colour_space_conversion  ]; then
		info "Colour space conversion to be applied:"

		for varname in  matrix_coefs  \
		                primaries      \
		                tone_curve    \
		                colour_range
		do
			local -n var=$varname
			var=${src_v[cspace_$varname]}
			if [ "$var" = input ]; then
				var=unknown
				[ "$varname" = 'colour_range' ]  \
					&& hl="${__y}${__bri}"  \
					|| hl="${__r}${__bri}"

			elif [ -v src_v[cspace_${varname}_assumed] ]; then
				hl="${__rst}${__bri}"
			fi

			#  As we want to print the data in a table, but at the same time
			#  optionally highlight the data in it, it poses a problem: the
			#  “column” program takes the invisible characters into the length
			#  or the string, and when both highlighted and non-highlighted
			#  items appear in one column, the program drawing the table
			#  thinks, that it’s doing fine, and on screen the next column is
			#  torn, because the invisible characters “pulled” the items to
			#  the right closer to themselves. So, in order to compensate for
			#  the length of the invisible characters, what should look “plain”
			#  is also wrapped in “reset colour” combinations, doing nothing,
			#  but taking space and makign the table looks straight.
			#
			hl=${hl:-${__bri}${__rst}}
			var="${hl}$var${__s}"
		done

		echo
		#  Do not remove non-breakable space under the “cat” below,
		#  it’s there to split the heading from the values.
		cat <<-EOF | column  -t  -o '    '  -N ' ',"${__bri}${__rst}Source${__s}",'Target'  |& sed -r "s/.*/${__mi:-  }  &/g"
		   
		Matrix coeff.    $matrix_coefs     ${target_colour_space[matrix_coefs]}
		Primaries        $primaries        ${target_colour_space[primaries]}
		Tone curve       $tone_curve       ${target_colour_space[tone_curve]}
		Colour range     $colour_range     ${target_colour_space[colour_range]}
		EOF

	elif [ -v do_only_colour_range_conversion ]; then
		info "Colour range conversion: ${src_v[cspace_colour_range]} → ${target_colour_space[colour_range]}"
	fi
	unset hl

	#  Separating the prinout of the initial properties
	#  from the calculations that will follow.
	echo
	mildec
	return 0
}


return 0