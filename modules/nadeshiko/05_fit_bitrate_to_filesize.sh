#  Should be sourced.

#  05_fit_bitrate_to_filesize.sh
#  Nadeshiko module, that offers an algorithm to find appropriate audio/video
#  bitrates and video resolution for the video fragment. It’s done by first
#  selecting a predefined profile and then tailoring it with accordance to the
#  source file peculiarities (resolution, A/V codecs, audio channels, bit-
#  rates…).
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # If you’re going to edit/rewrite the code
#
#  This is the most complex part of the entire program. The code may seem not
#  quite organised, but it’s not in its final form yet. There are features,
#  that are planned, and there are features that may appear along the way,
#  so until these ideas are not shaped, the code retains the form that ref-
#  lects what was planned and implemented so far. Thus, what’s below is a
#  well-thought and balanced attempt to keep the code small, organised, com-
#  mented. These three requirements are crucial, because due to the recur-
#  sion that’s happening here, one has to keep a large part of code in one’s
#  mind. The code /may be/ “unwrapped” and subroutined absolutely to the
#  bones, but then you’d have to keep it all in your mind anyway, and one
#  small file is better than three big ones.
#
#  Don’t worry – the code is clear, and to some extent is even controlled
#  by human-understandable abstractions. It withstood several revisions, which
#  wouldn’t be possible, if it was untidy. This warning is here only to make it
#  clear that here’s some complex stuff going on, and one shouldn’t rush to
#  optimise it. First, one should have a clear idea of how the code works
#  at different stages of the program.
#
#  As for how the code may be further developed, and what reasons must guide
#    the developement,
#    1. The code is on the limit of flexibility right now. The original ver-
#       sion didn’t have post-search optimisations (like vbitrate headpats,
#       fitting better audio bitrate), so those were made as extensions to
#       a rigid frame. New features may be hard to add.
#    2. For this reason, the stages “it fits!” and “now let’s look, how to
#       optimise the video fragment better” should be separated. The latter
#       stage shouldn’t break anything from the former, and to achieve that,
#       the former stage should be remade so as to create not the “finished”
#       set of values, but rather a volatile “intermediate” set. Then the
#       “final” set should be defined only at the end of the “let’s optimise”
#       stage (i.e. to-be the second and the last stage).
#    3. Further atomise basic operations on the bitrates, down to helper utils
#       (but still keeping their number as low as possible);
#

 # File size is (typically) displayed in bytes. But for all values related
#  to bit rates or derived from them, such as the ESP unit and the muxing over-
#  head, they are calculated and shown in (kilo)bits, not bytes. (Technically,
#  even the space is calculated in bits, but that happens deep in this module,
#  the hard work of which is summarised in the line showing the appropriate
#  bit rates, that were found.) So, unless you see a unit referring to bytes:
#  “B”, “kB”, “MiB”, “GiB” or the word “bytes”, assume that the value with
#  “k” is in kilobits, and in bits, if it has a raw value without unit.
#
#  When counting bits, the suffixed values always use power of 10, thus
#  1 kilobit = 1000 bits. It’s not superfluous to remind, that the “si”
#  (or “k=1000”) option refers _only_ to target file size, and it has
#  no relation to the calculation of bitrates.
#


 # Calculates the maximum amount of video bitrate, that fits
#  into max_size. Works only on $vbitrate and $abitrate.
#
recalc_space() {
	declare -g  space_for_video_track   #  used in unset_our_options()
	declare -g  max_fitting_vbitrate
	declare -g  vbitrate

	local  vidres
	local  audio_info
	local  padding_per_duration
	local  duration_secs
	local  old_duration_secs
	local  audio_track_expected_overhead
	local  audio_track_size

	milinc

	if [ -v audio ]; then
		recalc_acodec_size_deviation
		audio_track_size=$((    duration[total_s_ceil] * abitrate
		                      + audio_track_expected_overhead  ))
	else
		audio_track_size=0
	fi

	recalc_muxing_overhead
	space_for_video_track=$((   max_size_bits
	                          - audio_track_size
	                          - muxing_overhead   ))

	max_fitting_vbitrate=$((    space_for_video_track
	                          / (   duration[total_s_ceil]
	                              + ${loop_last_frame_s:-0} )  ))


	[ -v audio ] && audio_info=" / $(pretty "$abitrate")"
	if [ -v scale ]; then
		#  Forced scale
		vidres="${scale}p"
	elif [ -v crop ]; then
		vidres="Cropped"
	elif (( current_bitres_profile != starting_bitres_profile )); then
		#  Going lowres
		vidres="${current_bitres_profile}p"
	else
		vidres='Native'
	fi
	infon "Trying $(pretty "$vbitrate")${audio_info:-} @${vidres}.  "
	echo "Have space for $(pretty "$max_fitting_vbitrate")${audio_info:-}."

	if [ ! -v forced_vbitrate  ]  \
		&& [ -v src_v[bitrate]  ]  \
		&& [ -v orig_vcodec_same_as_enc ]  \
		&& (( max_fitting_vbitrate > src_v[bitrate] ))
	then
		info "Can fit original ${srv_v[bitrate]} kbps!"
		vbitrate=${src_v[bitrate]}
	fi

	mildec
	return 0
}


 # Calculates overhead for the audio track. The values are chosen according to
#  tables, where the deviation is tied to codec and bitrate. The margin here
#  is really tiny, it was implemented as an attempt to create such a table for
#  video codecs in the future to further improve estimation of the the encoded
#  stream size.
#
recalc_acodec_size_deviation() {
	declare -g  audio_track_expected_overhead

	local  duration_secs
	local  durations_high_to_low

	local -n padding_per_duration=${ffmpeg_acodec}_${acodec_profile}_size_deviations_per_duration

	durations_high_to_low=$(
		IFS=$'\n'; echo "${!padding_per_duration[*]}" | tac  # sic!
	)
	for duration_secs in $durations_high_to_low; do
		(( duration[total_s_ceil] <= duration_secs ))  \
			&& break  \
			|| continue
	done
	audio_track_expected_overhead=$(
		echo "scale=3; ao = ${padding_per_duration[duration_secs]} * $abitrate;  \
		      scale=0; ao/1"  | bc
	)
	msg "Expected audio track overhead: $(pretty "$audio_track_expected_overhead")"
	return 0
}


recalc_muxing_overhead() {
	declare -g  muxing_overhead
	declare -g  frame_count
	declare -g  esp_unit

	local  frame_count_border
	local  esp_to_reserve=0

	msg "Calculating the muxing overhead:"
	milinc
	#  Sic! Iterating indices from low to high
	for frame_count_border in ${!container_space_reserved_frames_to_esp[*]}; do
		(( frame_count > frame_count_border ))  \
			&& esp_to_reserve=${container_space_reserved_frames_to_esp[frame_count_border]}
	done

	#  ESP is an equivalent of one second of playback (A+V). Used in predic-
	#  ting muxing overhead. See “Muxing overhead” on the wiki.
	esp_unit=$vbitrate
	[ -v audio ] && let "esp_unit += $abitrate, 1"
	msg "Reserving $esp_to_reserve ESP  (1 ESP = $(pretty "$esp_unit"))"
	muxing_overhead=$( echo "scale=2; ovhead = $esp_to_reserve * $esp_unit;  \
	                         scale=0; ovhead / 1" | bc
	)
	let "muxing_overhead += ${filesize_antiovershoot:-0}, 1"
	msg "Anti-overshoot padding: $(pretty "${filesize_antiovershoot:-0}")"
	msg "Expected muxing overhead: $(pretty "$muxing_overhead")"
	mildec
	return 0
}


 # $1 – variable name, whose value should be converted from the shortened
#       form (e.g. 999k) to a number (e.g. 999’999)
to_bits() {
	local varname="${1#\*}"
	local -n varval=$varname
	varval=${varval//k/*1000}
	varval=${varval//M/*1000*1000}
	varval=$(( $varval ))
	return 0
}


 # $1 – some plain number (without suffix) to shorten into suffixed form.
#       e.g. 999999 → 999k
#
pretty() {
	local var="$1"

	 # Setting _pretty variable for display
	#
	#  The em-cut is too much, seeing 2430k would be more informative,
	#  than “2M”. “Is that 2000k or 2900k?” Those 900k make a difference.
	#
	# if [[ "$var" =~ .......$ ]]; then
	# 	var="$((var/1000000))M"
	# el
	if [[ "$var" =~ ....$ ]]; then
		var="$((var/1000))k"
	fi
	#  If $var contains a 4–8-digit number, introduce a thousand separator: ’
	[[ "$var" =~ .....k  &&  "$var" =~ ^([0-9]{1,3})([0-9]{3}|)([0-9]{3}|)k$ ]] && {
		var="${BASH_REMATCH[1]}"
		var+="${BASH_REMATCH[2]:+’${BASH_REMATCH[2]}}"
		var+="${BASH_REMATCH[3]:+’${BASH_REMATCH[3]}}"
		var+='k'
	}

	echo "$var"
	return 0
}


#  $1 – some xbitrate variable, all numbers.
apply_correction() {
	declare -g autocorrected_vbitrate

	local  varname="${1#\*}"
	local  correction_type
	local  origres_corr
	local  cropres_corr
	local  orig_res_total_px=${src_v[resolution_total_px]}
	local  cropres_total_px

	local -n varval=$varname

	shift  # remaining positional parameters become correction types

	for correction_type in "$@"; do
		case "$correction_type" in
			#  orig_res should be first!
			by_orig_res)
				#  Tricky formula for non-standard resolutions.
				#  Bitres profiles use 16×9.
				starting_profile_res_total_px=$((
				    starting_bitres_profile**2 *16 /9
				))
				origres_corr=$(
					echo "scale=4;   $orig_res_total_px  \
					               / $starting_profile_res_total_px" | bc
				)
				varval=$(
					echo "scale=4; vv = $varval * $origres_corr;  \
					      scale=0; vv/1" | bc
				)
				autocorrected_vbitrate=t
				;;
			by_crop_res)
				crop_res_total_px=$((crop_w * crop_h))
				cropres_corr=$(
					echo "scale=4;   $crop_res_total_px  \
					               / $orig_res_total_px" | bc
				)
				[ "$cropres_corr" = '1' ]  \
					&& unset crop  \
					|| varval=$(echo "scale=4; vv = $varval * $cropres_corr;  \
					                  scale=0; vv/1" | bc )
				autocorrected_vbitrate=t
				;;
			to_minimal_bitrate)
				varval=$(
					echo "scale=4; vv =    $varval  \
					                     * $minimal_bitrate_pct  \
					                     / 100;  \
					      scale=0; vv/1"  | bc
				)
				#  This counts as something, that the user implies to use.
				#  Variations in minimal bitrate should be small,
				#  so there’s no reason to accentuate attention on it.
				;;
		esac
	done

	return 0
}


 # When resolution profile is set initially or changes afterwards,
#  this function is called to assign
#    - desired_vbitrate;
#    - minimal_vbitrate;
#    - acodec_profile;
#    to the values from the bitrate-resolution profile specified via $1.
#  $1 – resolution of some bitres profile, e.g. 1080, 720, 576…
#
#  After the desired values, vbitrate and abitrate are set. They are the
#  “working” values – max_fitting_vbitrate is calculated with them,
#  and it is vbitrate, that’s checked against max_fitting_vbitrate.
#
set_bitres_profile() {
	declare -g  bitres_profile
	declare -g  vbitrate
	declare -g  abitrate
	declare -g  autocorrected_scale
	declare -g  autocorrected_vbitrate
	declare -g  autocorrected_abitrate
	declare -g  acodec_profile

	local  res="$1"
	local  extra_colour

	info "${res}p: loading profile"
	milinc

	#  Setting active bitrate–resolution profile.
	declare -gn bitres_profile="bitres_profile_${res}p"
	(( current_bitres_profile != starting_bitres_profile )) && {
		autocorrected_scale=t
		autocorrected_vbitrate=t
		autocorrected_abitrate=t
	}

	#  Checking audio before video, as vb*** parameter may force us to
	#  return quickly.
	#
	[ -v audio ] && {
		#  The value in {ffmpeg_acodec}_profile looks like a bitrate
		#  but it is but a profile name, that speaks about what median
		#  bitrate this profile aims for. Some audio codecs use VBR, some CBR,
		#  some codecs take precise values, some vaguely set “quality”.
		acodec_profile=${bitres_profile[${ffmpeg_acodec}_profile]}
		#  acodec_profile must be remembered as it is, so that at the encoding
		#  stage the ffmpeg arguments of that that profile could be retrieved.
		#  And for calculations there is  the abitrate  variable.
		abitrate=$(( acodec_profile * 1000))
		info "        abitrate = $(pretty "$abitrate")."
	}

	[ -v forced_vbitrate ] && {
		to_bits '*vbitrate'
		return 0
	}

	 # If bitrates aren’t locked, define desired and minimal values for
	#  the curent profile.
	#
	#  Setting desired_vbitrate
	desired_vbitrate="${bitres_profile[${ffmpeg_vcodec}_desired_bitrate]}"
	to_bits '*desired_vbitrate'
	apply_correction '*desired_vbitrate' \
	                 ${needs_bitrate_correction_by_origres:+by_orig_res}  \
	                 ${needs_bitrate_correction_by_cropres:+by_crop_res}
	[ -v autocorrected_vbitrate ] && extra_colour="${__bri}${__y}"
	info "desired_vbitrate = ${extra_colour:-}$(pretty "$desired_vbitrate")${__s}."
	#  Setting minimal_vbitrate
	minimal_vbitrate=$desired_vbitrate
	apply_correction '*minimal_vbitrate'  \
	                 'to_minimal_bitrate'

	info "minimal_vbitrate = $(pretty "$minimal_vbitrate")."
	#  Setting vbitrate, that will take values from desired to minimal
	#  until it fits to the file size or bitres profile changes.
	vbitrate=$desired_vbitrate

	mildec
	return 0
}


 # Switches off flags in “our_options” array under specific circumstances.
#
unset_our_options() {
	declare -g  vbitrate
	declare -g  max_fitting_vbitrate

	local  cur_video_track_size
	local  max_video_track_size
	local  track_size_difference
	local  total_playback_minutes
	local  allowed_margin

	#  Avoid an eternal cycle when there’s no space in the container for the
	#  video track at all (audio + muxing overhead would occupy >=100%).
	(( space_for_video_track < 0 ))  \
		&& unset our_options[seek_maxfit_here]

	[ -v forced_vbitrate ]  \
		&& unset our_options[seek_maxfit_here]

	 # Disallow scaling when crop is requested.
	#  To predict how much bitrate a cropped fragment would use (in compari-
	#  son to encoding at original resolution) is technically possible, but
	#  would require a large set of tests to run. It’s safer to encode cropped
	#  cuts with the bitrate calculated for the full frame.
	#
	[ -v crop ]  \
		&& unset our_options[lower_resolution]

	[ -v forced_scale ]  \
		&& unset our_options[lower_resolution]

	#  If the scene is dynamic, the video bitrate is locked on desired.
	[ -v vbitrate_locked_on_desired ] && {
		#  Allow marginal deviations.
		#  If vbitrate differs from max_fitting_vbitrate by not more than
		#  space needed for two seconds of video playback per minute
		#  (duration < 1 min equaled to 1 min), then let it stay within
		#  the current resolution, otherwise lock and go a resolution lower.
		cur_video_track_size=$(( vbitrate * duration[total_s_ceil]  ))

		max_video_track_size=$((    max_fitting_vbitrate
		                          * duration[total_s_ceil]  ))

		track_size_difference=$((   cur_video_track_size
		                          - max_video_track_size  ))

		total_playback_minutes=$(( ${duration[h]}*60 + ${duration[m]} ))

		(( total_playback_minutes == 0 )) && total_playback_minutes=1
		allowed_margin=$((   2*max_fitting_vbitrate
		                   * total_playback_minutes    ))

		(( track_size_difference > allowed_margin  ))  \
			&& unset our_options[seek_maxfit_here]
	}
	return 0
}



         #  Estimating possibilities to use higher audio bitrate  #

 # Two factors define, what may be the best audio bitrate for a particular
#  fragment: the audio bitrate specified in the target bitrate-resolution
#  profile and the “best reasonable” bitrate, which is evaluated each time
#  at runtime.
#    With the bitres profile everything is direct and clear: 1080p gets this
#  much, 586p gets something lower. The values are taken from the configura-
#  tion files. With the “best reasonable” bitrate there’s a complex procedure:
#  first the audio track in the souce is checked – is it lossless or lossy? or
#  the is it unknown (because the metadata are empty)? if it’s lossy or unknown,
#  what’s the bitrate? how does it compare to the audio quality from the target
#  bitres profile?
#    Indeed, to keep a table of correspondences between each codec and bitrate
#  (actually, bitrate ranges or some median bitrate) is a nigh impossible task.
#  The data that can be gathered on the internet on this subject are scarce.
#  However, the essential part from that table can be implemented. Nadeshiko
#  compares audio tracks by their proximity to the bar where source and target
#  audio codecs (which may be different) reach audio transparency. (It is ex-
#  plained in details on the wiki, the summary would be the point at which the
#  codec produces quality where it’s impossible to tell it from lossless. Thus
#  all lossless sources pass this check automatically).
#    In the code below the point of audio transparency is refererred to as
#  “…codec_good_at” or “…fairly_good_at” (“fairly” gets omitted in some places
#  purely because names get too long).
#    Most audio codecs, that are in wide use, have theeir point of transparency
#  determined in the configuration files (under “metaconf/”). The bitrates used
#  for the comparison are driven to the 2-channel format by a formula (see
#  below). Unknown audio formats/profiles are tested for their bitrate: they
#  are compared to the “poorest in terms of efficiency” DTS codec, and, if they
#  beat its (very high) bar for transparency, they will be cosidered transpa-
#  rent by the sheer bitrate in the source. So, even if Nadeshiko would encoun-
#  ter some new (or rare) audio format, this algorithm may respect the source
#  quality and assign some extra audio bitrate to the encoded fragment, provi-
#  ded, that there’s enough space in the file. The used space is indeed recal-
#  culated for each attempt.
#    Indeed, there’s a few rough corners. For example, the algorithm doesn’t
#  consider cases, when an under-transparency low-quality source is used to
#  make a long cut, which would have to use an /even lower/ audio bitrate (nor-
#  mally, according to the bitres profile). But for most cases, when the source
#  is at least half-decent – meaning that it may be a rip, a ripped stream, or
#  something like that – this method does a great job!
#    The last important thing, that has to be told about, is the “bitrate ceil-
#  ing” in the audio codec profiles. It’s a restriction taken up consciously
#  and applies to Nadeshiko codebase. The general rule is that profile entries
#  (think of them as bitrates) for a given audio codec must approach and end(!)
#  on the bitrate, where this codec reaches audio transparency. Thus, profile
#  entries for MP3 go as [115], [130], … [225] and end on [245], they go no
#  higher. This may seem at first as an artificial handicap on usage of higher
#  bitrates, but on the other hand, this is actually where the ceiling of Nade-
#  shiko’s purpose is: if the output receives audio transparency, this is enough;
#  there’s no point in encoding 320 kbps Opus from a FLAC source, when a limit
#  on file size doesn’t permit for perfect visuals.
#
#  P.S. And this is also why the “ab=” option – counterpart to “vb=” – was
#  removed: when decent quality would be within reach, Nadeshiko will en-
#  hance it – within what is possible. Most people would seldom, if ever,
#  be considerate, actually reasonable, not leading to futilely bloated
#  files with poor video quality.
#


 # Returns 0, if the source audio is of a higher quality, than
#  what it’s to be encoded with by the bitres profile setting.
#
determine_possibility_to_use_better_abitrate() {
	declare -g  better_abitrate_is_possible
	declare -g  better_abitrate_is_limited_to

	local  acodec_name_as_formats_varname
	local  acodec_name
	local  known_formats
	local  known_format
	local  delimiter
	local  format_per_se
	local  format_profile_per_se
	local  format_matches
	local  source_audio_format_is_unknown
	local  source_audio_stereo_equiv_bitrate


	[ -v src_a[format]  ] || {
		denied 'Audio track format is missing in the source metadata.'
		return 0
	}

	local -n output_acodec_is_good_at="${ffmpeg_acodec//-/_}_is_fairly_good_at"

	                  #  Source track is lossless  #

	 # If it’s lossless, it’s better by definition.
	#
	[ -v src_a[is_lossless]  ]  && {
		info "Audio track in the source is lossless.
		      It makes sense to use a higher audio bitrate!"
		better_abitrate_is_possible=t
		return 0
	}

	                     #  Source is lossy  #

	 # If it’s not lossless, then do we recognise the audio format
	#  (and profile format, if necessary)?
	#
	shopt -s nocasematch  # so that VORBIS == Vorbis
	for acodec_name_as_formats_varname in ${!acodec_name_as_formats*}; do
		#
		#  Remembering acodec’s name for the later check on bitrate.
		#  $acodec_name has the codec name in the format as used in
		#  shell variables.
		#
		acodec_name=${acodec_name_as_formats_varname#*formats_}

		unset -n format_profile_delimiter
		local -n known_formats=$acodec_name_as_formats_varname

		[ -v ${acodec_name_as_formats_varname/acodec_/acodec_delimiter_for_} ]  && {
			#
			#  Delimiter between the format and format profile, e.g. colon
			#  in “AAC:LC”. The /profile/ of a format may or may not be present.
			#
			local -n delimiter=${acodec_name_as_formats_varname/acodec_/acodec_delimiter_for_}
		}

		#  This evaluates per each known acodec
		for known_format in "${known_formats[@]}"; do
			format_per_se="${known_format%${delimiter:-no delim}*}"
			format_profile_per_se="${known_format#*${delimiter:-no delim}}"
			[[ "${src_a[format]}" = "$format_per_se" ]] && {
				#
				#  If at least the format did already match, then let’s see,
				#  whether there’s also a format profile to be compared…
				#
				if [ "$format_profile_per_se" = "$known_format" ]; then
					#  No format profile, a match by format alone is enough.
					format_matches=t
					#  If there would be format profile, then it should be
					#  printed. If not…
					unset format_profile_per_se
					break 2
				else
					#  And if there’s a delimiter, we need to check it too.
					[ -v src_a[format_profile]  ] || {
						denied 'The audio track in the source must have had a format profile,
						        but it wasn’t present and therefore cannot be checked.'
						shopt -u nocasematch
						return 0
					}
					[[ "${src_a[format_profile]}" = "$format_profile_per_se" ]] && {
						format_matches=t
						break 2
					}
				fi
			}

		done
	done
	shopt -u nocasematch

	if [ -v format_matches ]; then
		info "Source audio codec is equivalent to an FFmpeg’s codec with…"
		milinc
		msg "name: $acodec_name"
		msg "format: $format_per_se"
		[ -v format_profile_per_se ]  \
			&& msg "format profile: $format_profile_per_se"
		mildec
	else
		info "Source audio format is not known to Nadeshiko. It may still pass
		      the second check, but the plank will be set extra high."
		source_audio_format_is_unknown=t
	fi
	#
	#  Now we know, that the format isn’t lossess, but it is a known one.
	#  The source bitrate now should be compared to our profile tables,
	#  to determine, if it’s of a higher quality or not.
	#
	#  To compare bitrates, they should be brought to the common denomi-
	#  nator, i.e. to a single number of channels. (Nadeshiko always uses
	#  stereo, but the source may have 2 or 6 or 8 channels.)
	#
	[ -v src_a[channels]  ] || {
		denied 'The number of channels in the source audio track must be known,
		        but it wasn’t present in the metadata.'
		return 0
	}
	#  When the number of channels is known, check for bitrate.
	[ -v src_a[bitrate]  ] || {
		denied 'The bitrate in the source audio track must be known,
		        but it wasn’t present in the metadata.'
		return 0
	}
	#  As per <https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio#Recom
	#  mendedminimumbitratestouse>
	source_audio_stereo_equiv_bitrate=$(
		#
		#  This calculation employs a ceiling rounding at the end,
		#  because it would be a shame to deny an encode with a better
		#  audio profile to a source, whose audio bitrate would be 191999
		#  (and not the required 192000), because one tiny bit is missing.
		#
		echo "scale=4; ch = ${src_a[channels]}/2;  \
		               br = (${src_a[bitrate]}+ch-1) / ch;  \
		      scale=0; (br+1000-1)/1000" | bc
	)
	info "Source audio track has a bitrate, that is
	      equivalent to two-channel ${source_audio_stereo_equiv_bitrate}k"
	#
	#  Ticket on the bullet train to all codecs: if the bitrate is higher than
	#  503 kbps (2 channels), consider that the source is audio transparent,
	#  i.e. worthy of encoding with a better audio bitrate.
	#
	#  The value of 503k stems from the DTS encoder. It bears fame of the
	#  least optimised among the lossy codecs still in use. In reality it may
	#  be not actually that bad. The problem is that nobody can tell how effi-
	#  cient it actually is. There’s little test data. All that’s known is on
	#  the wiki: <https://codeberg.org/deterenkelt/Nadeshiko/wiki/Docs-on-
	#  encoding.-Sound#dts> Assuming DTS as the “codec, that requires the hu-
	#  gest bitrate”, we can take its technical limit, which is 1509.75 kbit/s
	#  for six channels, and after converting this bitrate to stereo we receive
	#  1509750÷(6÷2) ≈ 503250 ≈ 504 kbps. This is the bar for transparency for
	#  any lossy codec in the wild, whose actual efficiency we don’t know (yet).
	#  That is, at which bitrate it becomes “fairly good”. 503k is used instead
	#  of 504k to deal with rounding, as explained above.
	#
	#  This, however, relies on the assumption, that Nadeshiko will not encoun-
	#  ter /really old/ codecs like mp2, that still /may/ offer poor quality
	#  even at such a big bitrate as 503k. On the other hand, bitrates this
	#  big were probably never given for such oldies. AC-3 and DTS alone could
	#  let themselves waste space, as they ruled the optical mediums comple-
	#  tely.
	#
	if (( source_audio_stereo_equiv_bitrate > 503 )); then
		info "${source_audio_stereo_equiv_bitrate}k > 503k!  The source is probably audio transparent."
		better_abitrate_is_possible=t
		#
		#  503k for stereo is quite high, and at least one paper does find DTS
		#  at this bitrate (=1.5 Mbps for 6 channels) imperceptibly close to
		#  lossless. So we don’t set the limit for the output codec, leaving
		#  it as if it was going to convert from a lossless source.
		#
		# : better_abitrate_is_limited_to=$output_acodec_is_good_at
		return 0

	elif [ -v source_audio_format_is_unknown ]; then
		#  This is the last stop for the unrecognised audio formats,
		#  so there has to be a message.
		denied "Source audio track has an unknown format and its bitrate doesn’t guarantee,
		        that re-encoding it with a better audio profile would make sense."
		return 0
	fi

	            #  Source audio codec is known, it’s lossy,  #
	            #     and its bitrate is lower than 503k     #

	 # The amount of bitrate that, if reached, indicates “fairly good” qua-
	#  lity in the source. Our output bitrate could then be bumped up to the
	#  “fairly good” for the target audio codec.
	#
	local -n source_acodec_is_good_at="${acodec_name}_is_fairly_good_at"


	if (( source_audio_stereo_equiv_bitrate >= source_acodec_is_good_at )); then
		better_abitrate_is_limited_to=$output_acodec_is_good_at
		better_abitrate_is_possible=t
		info "Source track bitrate >= ${source_acodec_is_good_at}k!
		      It makes sense to use a higher audio bitrate!"
		return 0

	elif [ "$acodec_name" = "${ffmpeg_acodec//-/_}" ]; then
		#
		#  If the bitrate in the source track is too small to achieve “fairly
		#  good” mark, then give the output bitrate the last chance: if the
		#  encode is going be to the same audio format as the original track,
		#  allow the target audio bitrate be bumped to the bitrate in the
		#  source. This may save some quality for low-resolution videos.
		#
		#  Also with this check we’ll guarantee, that an original low-quality
		#  audio track (Tw*tter quality of 66k) won’t be encoded with some-
		#  thing higher.
		#
		better_abitrate_is_limited_to=$source_audio_stereo_equiv_bitrate
		better_abitrate_is_possible=t
		#
		#  ^ The flag to recalculate bitrate is set right away, before the
		#  check on whether the currently chosen audio bitrate exceeds or
		#  goes below the bitrate in the source, because it really will have
		#  to be set by selecting a proper profile, e.g. [64], [128]… The
		#  reason being that if the value would be too high, we could indeed
		#  set it lower right here, equal to what’s in the source. But
		#    1) setting audio bitrate is not just -b:a, it’s also -vbr N
		#       -cutoff NNNNN and sometimes other parameters. So we can’t
		#       avoid re-selecting /the profile/.
		#    2) output encoder may not be accepting arbitrary values, and
		#       accepting only values known to it – and they are described
		#       in the profile.
		#
		if (( abitrate > source_audio_stereo_equiv_bitrate *1000 )); then
			info "Ho-ho! Turns out, we were going to re-encode $(pretty $((source_audio_stereo_equiv_bitrate*1000))) $format_per_se to $(pretty $abitrate) $format_per_se!
			      A more appropriate profile will be chosen."
			(( ${src_a[channels]:-2} > 2 ))  && {
				milinc
				info "Hrm, a ${src_a[channels]:-2}-channel sound can still be of superiour quality than
				      simple math estimates it to be. And potentially, it may still make
				      sense to use higher audio bitrates for the output here. However,
				      there’s little known about how to estimate quality in this case."
				mildec
			}

		elif (( abitrate < source_audio_stereo_equiv_bitrate *1000 )); then
		  	info "Even though the source doesn’t seem to be of fairly good quality,
		  	      the output bitrate of $(pretty $abitrate) may at least be raised to $(pretty $((source_audio_stereo_equiv_bitrate*1000)))!
		  	      If there would be enough space, indeed…"
		fi
		#
		#  P.S. It would indeed be the best to just stream copy the source
		#  data. But the copied data may or may not be fitting in the limited
		#  space. The attempt to encode with a better audio bitrate is the
		#  latest stage of fitting to size: everything is already set to place,
		#  and we just see, if the remaining free space can be used to enhance
		#  audio. To allow stream copying would mean preventively encoding
		#  the audio track, checking its size, then, if it would pose no prob-
		#  lems with the file size limit, proceed to encoding the video, then
		#  mux video and audio together. That’s a lot of stuff to handle in
		#  parallel to the normal workflow. And that’s for some 360p video
		#  with poor audio? Nah. Maybe later.
		#
		return 0

	else
		denied "Better audio won’t be possible. The $format_per_se track in the source
		        should have had at least $source_acodec_is_good_at kbit/s bitrate to consider
		        its quality good enough."

	fi

	return 0
}


 # Calculates, whether there is some bitrate in ${ffmpeg_acodec}_profiles
#  that would be higher than the default for the current bitres profile,
#  and would fit into the chosen file size with a padding, specific
#  to each codec.
#
check_space_for_better_abitrate() {
	declare -g  space_for_better_abitrate_is_available
	declare -g  better_abitrate_is_limited_to
	declare -g  acodec_profile
	declare -g  abitrate
	declare -g  better_acodec_profile_set
	declare -g  lower_acodec_profile_set

	local  acodec_profiles_high_to_low
	local  old_acodec_profile
	local  array_length
	local  i

	local -n source_acodec_profiles=${ffmpeg_acodec}_profiles

	[ -v better_abitrate_is_limited_to ]  \
		&& info "Source quality limits the reasonable output bitrate to $(pretty $((better_abitrate_is_limited_to*1000)))."

	acodec_profiles_high_to_low=(
		$(
			IFS=$'\n';  echo "${!source_acodec_profiles[*]}" | tac  # Sic!
		 )
	)

	#  Cleaning the array from values that are of no use to us. This is
	#  to not deal with skipping these items later, as that skipping would
	#  have to be reported to console.
	#
	[ -v better_abitrate_is_limited_to ] && {
		array_length=${#acodec_profiles_high_to_low[*]}
		for ((i=0; i<array_length; i++)); do
			(( ${acodec_profiles_high_to_low[i]} > better_abitrate_is_limited_to ))  \
				&& unset acodec_profiles_high_to_low[$i]  \
				|| break
		done
	}


	#  Must use the global “acodec_profile” in order to call “recalc_…”.
	#  Using a local variable might be one way to deal with this, but with
	#  bash being bash…
	#
	old_acodec_profile=$acodec_profile

	for acodec_profile in ${acodec_profiles_high_to_low[@]}; do
		abitrate=$((acodec_profile * 1000))

		(( acodec_profile == old_acodec_profile )) && break

		info "Trying acodec profile ${acodec_profile}k."
		milinc

		recalc_acodec_size_deviation
		recalc_muxing_overhead

		if ((  (        abitrate * duration[total_s_ceil]
			          + audio_track_expected_overhead
			   )
		         <=
		       (        max_size_bits
		              - vbitrate * duration[total_s_ceil]
		              - muxing_overhead
		       )                                         ))
		then
			if (( acodec_profile > old_acodec_profile )); then
				info "${__bri}${__g}Better audio profile ${acodec_profile}k fits!${__s}"
				better_acodec_profile_set=t

			elif (( acodec_profile < old_acodec_profile )); then
				info "Lower audio profile ${acodec_profile}k was selected
				      to prevent wastefulness."
				lower_acodec_profile_set=t
			fi
			mildec
			break

		else
			denied 'Not enough space in the file.'
		fi
		mildec
	done

	[ ! -v better_acodec_profile_set  -a  ! -v lower_acodec_profile_set ]  && {
		denied "The already chosen audio profile ${acodec_profile}k is the only that fits."
		acodec_profile=$old_acodec_profile
	}

	return 0
}


try_to_fit_better_abitrate() {
	determine_possibility_to_use_better_abitrate

	[ -v better_abitrate_is_possible ]  \
		&& check_space_for_better_abitrate

	return 0
}


headpat_vbitrate() {
	declare -g  vbitrate
	declare -g  old_vbitrate
	declare -g  max_fitting_vbitrate
	declare -g  deserves_a_headpat

	local  default_vbitrate_pretty
	local  headpatted_vbitrate_pretty
	local  extra_bitrate_max

	 # When the bitres profile is downgraded (stepping from a higher resolution
	#  to a lower one), an adjustment to the new vbitrate can be done to peruse
	#  the difference in the max value of the current profile and the bitrate
	#  from the higher profile, that failed. This allows for a smoother shift
	#  in the encoding options, aiming to save more quality, when possible.
	#
	#  The algorithm that selects the appropriate options has to work with dif-
	#  ferent sources – for most of them the defult settings work fine, but for
	#  some they do not. This may lead to the bitrate settings being completely
	#  ignored by the codec, which results in heavy overshooting. (Primarily
	#  that concerns synthetic videos, like heavy 3D or 2D graphics.) Say, what
	#  should fit into 10 MiB takes 50 MiB (1800k + 192k @ 1080p), an overshoot-
	#  ing happens, reserved space takes more than max_size (20 MiB), bitres
	#  profile drops one down, new bitres profile (1000k/128k @ 720p) should
	#  take even less space, but the headpat algo would see the entire space
	#  (from supposedly needed ≈7 MiB up to max_size =20 MiB) and headpat the
	#  the video with it, which will not help and instead introduce another
	#  kind of a rollercoaster. To prevent this, headpat is limited to half
	#  the distance between currently selected bitrate (in the downgraded
	#  profile) to the formerly used bitrate (in the higher profile, which
	#  has failed).
	#
	#  On the ties to $scale (in case you have problems):
	#  This check had a bug: it prevented the headpat from being applied
	#  on the recalculation after an overshoot, because current_bitres_profile
	#  was EQUAL to starting_bitres_profile. (After an overshoot, starting_
	#  bitres_profile is set to $scale remembered ($scale is set when going
	#  a resolution down was detected), and $scale was indeed set to the
	#  last chosen bitres profile. If downscale went say, to 360p, then
	#  $scale == 360p, $starting_bitres_profile == $scale == 360p, and
	#  it so happens, that there was no difference between the starting
	#  and current bitres profiles – no downscale jump was detected – and the
	#  headpat therefore wasn’t activated.)
	#
	[ -v deserves_a_headpat ] && (( vbitrate < max_fitting_vbitrate )) && {
		default_vbitrate_pretty=$(pretty "$vbitrate")

		extra_bitrate_max=$((old_vbitrate/2 - vbitrate))
		(( max_fitting_vbitrate > extra_bitrate_max ))  \
			&& vbitrate=$extra_bitrate_max  \
			|| vbitrate=$max_fitting_vbitrate

		headpatted_vbitrate_pretty=$(pretty "$vbitrate")
		[ "$default_vbitrate_pretty" != "$headpatted_vbitrate_pretty" ] && {
			#  Showing the info message only if the difference was big
			#  enough to be shown in “pretty” values, visible to the user.
			info "${__bri}${__g}Headpat to the poor downscaled video:
			      vbitrate=${__bri}${__w}$headpatted_vbitrate_pretty${__s}\n"
		}
	}

	return 0
}


                   #  Main function of the module  #

 # Finds appropriate video bitrate and resolution for encoding.
#  Takes into account bitrate coefficients and constraints.
#
fit_bitrate_to_filesize() {
	declare -g  deserves_a_headpat
	declare -g  max_size_B
	declare -g  max_size_bits
	declare -g  vbitrate
	declare -g  scale

	#  As we may re-run, let’s operate on a local copy.
	local  closest_lowres_index=$closest_lowres_index
	local  cannot_fit

	info "Calculating, how we fit… "
	milinc
	#  Not using  to_bits()  here, because counting size involves the $kilo
	#  variable.
	#
	#  max_size_B will be used later when the encoded file size
	#  would be compared to the maximum size.
	max_size_B=$max_size
	max_size_B=${max_size_B//k/*$kilo}
	max_size_B=${max_size_B//M/*$kilo*$kilo}
	max_size_B=${max_size_B//G/*$kilo*$kilo*$kilo}
	max_size_B=$(($max_size_B))
	max_size_bits=$((max_size_B*8))

	if [ -v scale ]; then
		starting_bitres_profile="$scale"
	elif [ -v closest_res ]; then
		starting_bitres_profile="$closest_res"
	else
		starting_bitres_profile="${src_v[height]}"
	fi
	current_bitres_profile="$starting_bitres_profile"
	info "Starting with ${starting_bitres_profile}p bitrate-resolution profile."
	set_bitres_profile "$starting_bitres_profile"

	recalc_space

	 # What can be done, if bitrates × duration doesn’t fit in $max_size_bits.
	#  I find this array a genius invention, the code was a mess without it.
	#
	declare -A our_options=(
		[seek_maxfit_here]=t  #  Until vbitrate hits the minimum allowed ave-
		                      #    rage bitrate in the current bitres profile.
		[lower_resolution]=t  #  Downgrade to a lower bitrate-resolution
		                      #    profile.
	)

	#  Are we already good?
	#  If not, can we scale to lower resolutions?
	#
	until (( vbitrate <= max_fitting_vbitrate )) || [ -v cannot_fit ]; do

		unset_our_options

		#  The flexibility of this code is amazing – fitting options can be
		#  stringed one on another; audio bitrate may stay fixed on some 200k
		#  while vbitrate would go round and around until the overall size
		#  fits to max_size_bits!
		#
		if [ -v our_options[seek_maxfit_here] ]; then
			if	((     max_fitting_vbitrate >= minimal_vbitrate
				    && max_fitting_vbitrate <= desired_vbitrate  ))
			then
				vbitrate=$max_fitting_vbitrate
				recalc_space
			else
				unset our_options[seek_maxfit_here]
			fi

		elif [ -v our_options[lower_resolution] ]; then
			milinc
			denied "won’t fit with ${current_bitres_profile}p."
			mildec
			#  known_res_list  goes like: 1080p 720p … 360p
			if (( closest_lowres_index < ${#known_res_list[@]} )); then
				current_bitres_profile=${known_res_list[closest_lowres_index]}
				echo
				#
				#  For the rare case when $overshot_times >= 1 and the increase
				#  in the muxing overhead pushed the video out of previously
				#  chosen bitrate-resolution profile. Muxing overhead must be
				#  unset, because the calculating of it was based on the old
				#  bitres profile, and for this new one it’s inapplicable. The
				#  muxing overhead shall be recalculated at least once from
				#  the base of this new profile.
				#
				(( ${overshot_times:-0} > 0 ))  \
					&& unset  muxing_overhead  filesize_antiovershoot
				set_bitres_profile $current_bitres_profile
				#  This eventually will bump the index out of array bounds
				#  essentially telling, that there is nowhere lower to go.
				let 'closest_lowres_index++,  1'
				#  New resolution – new minimal and desired bounds.
				our_options[seek_maxfit_here]=t
				recalc_space
			else
				#  Lower resolutions are depleted.
				unset our_options[lower_resolution]
			fi
		else
			denied 'No more ways to downscale.'
			cannot_fit=t
		fi
	done

	[ -v cannot_fit ] && err "Cannot fit ${duration[ts_short_no_ms]} into $max_size."

	#  This message belongs to the last bitres profile messages group.
	milinc
	info 'It fits!'
	mildec


	 # Detecting automatic downscale
	#
	#  $scale is remembered and in case when an overshoot happens,
	#  $starting_bitres_profile should be assigned the bitres profile
	#  from $scale.
	#
	#  That the video deserves a headpat must be remembered too,
	#  see the comment below.
	#
	(( current_bitres_profile != starting_bitres_profile ))  && {
		scale=$current_bitres_profile
		headpat_vbitrate
	}

	[ -v audio  -a  ! -v forced_abitrate ] && {
		echo
		info "Attempting to fit better audio."
		milinc
		#  No need to backup values: if no higher audio profile would be found,
		#  the algorithm will stop on whatever audio bitrate was previously
		#  selected.
		try_to_fit_better_abitrate
		mildec
	}

	mildrop
	echo
	return 0
}


return 0