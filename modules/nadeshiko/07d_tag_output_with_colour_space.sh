#  Should be sourced.

#  07d_tag_output_with_colour_space.sh
#  Tags the output file with colour space matrix coefficients, primaries,
#  tone curve and colour range, when appropriate. Tagging is done normally
#  to preserve the original tags (when the conversion didn’t change colour
#  space). If the conversion was requested, the output will be tagged accord-
#  ing to the requested conversion settings. In case the output was not tag-
#  ged properly the tagging depends on the configuration options.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


tag_output_with_colour_space() {
	[ -v ffmpeg_colour_space_output_tags_set ] && return 0

	declare -g ffmpeg_colour_space_output_tags=()
	declare -g ffmpeg_colour_space_output_tags_set

	local  tags_origin
	local  matrix_coefs
	local  primaries
	local  tone_curve
	local  colour_range

	info "Setting colour space tags for the output."
	milinc

	if [ -v do_colour_space_conversion ]; then
		if [ -v src_v[cspace_valid] ]  \
			|| [ -v forced_colour_space_conversion_also_forces_tagging ]
		then
			tags_origin='target'
		else
			tags_origin='cancel'
		fi
	else
		tags_origin='source'
	fi

	[ "$tags_origin" = 'cancel' ] && {
		warn "Leaving the output untagged is safer, as the colour space conversion
		      was forced while the input metadata remained unknown. Hence it is not
		      known, whether the conversion took place at all."
		mildec
		return 0
	}

	case "$tags_origin" in
		source)
			#  The source might or might not have all the needed metadata.
			matrix_coefs="${src_v[cspace_matrix_coefs]:-}"
			primaries="${src_v[cspace_primaries]:-}"
			tone_curve="${src_v[cspace_tone_curve]:-}"
			[ -v do_only_colour_range_conversion ]  \
				&& colour_range="${target_colour_space[colour_range]}"  \
				|| colour_range="${src_v[cspace_colour_range]:-}"
			;;

		target)
			matrix_coefs="${target_colour_space[matrix_coefs]}"
			primaries="${target_colour_space[primaries]}"
			tone_curve="${target_colour_space[tone_curve]}"
			colour_range="${target_colour_space[colour_range]}"
			;;
	esac

	if [[ "$matrix_coefs" =~ ^(input|)$ ]]; then
		warn "Matrix coefficients weren’t detected, and won’t be set."
	else
		ffmpeg_colour_space_output_tags+=( -colorspace "$matrix_coefs" )
		msg "Matrix coefficients: ${__bri}$matrix_coefs${__s}."
	fi

	if [[ "$primaries" =~ ^(input|)$ ]]; then
		warn "Primaries weren’t detected, and won’t be set."
	else
		ffmpeg_colour_space_output_tags+=( -color_primaries "$primaries" )
		msg "Primaries: ${__bri}$primaries${__s}."
	fi

	if [[ "$tone_curve" =~ ^(input|)$ ]]; then
		warn "Tone reproduction curve (gamma) wasn’t detected, and won’t be set."
	else
		ffmpeg_colour_space_output_tags+=( -color_trc "$tone_curve" )
		msg "Tone reproduction curve (gamma): ${__bri}$tone_curve${__s}."
	fi

	if [[ "$colour_range" =~ ^(input|)$ ]]; then
		warn "Colour range wasn’t detected, and won’t be set."
	else
		ffmpeg_colour_space_output_tags+=( -color_range "$colour_range"	)
		case "$colour_range" in
			tv)
				local _colour_range="tv (limited)"
				;;
			pc)
				local _colour_range="pc (full)"
				;;
		esac
		msg "Colour range: ${__bri}${_colour_range}${__s}."
	fi

	mildec
	ffmpeg_colour_space_output_tags_set=t
	return 0
}


return 0