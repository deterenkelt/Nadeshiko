#  Should be sourced.

#  07b_assemble_ffmpeg_filter_graph.sh
#  What is related to hardsubbing, overlaying, track time shift, conversions
#  of any kind (crop, scale, colour space) is set up here. This file also works
#  with attachments and the attachments cache.
#  Author: deterenkelt, 2018–2025
#
#  For licence see nadeshiko.sh
#


add_filter_deinterlace() {
	[ -v deinterlace ] || return 0

	declare -g  filter_chain1
	declare -gA saved_filter_string

	local  filter_list
	local  key

	info "Deinterlacing: $deinterlace_filter"
	milinc

	[ -v saved_filter_string['deint'] ] && {
		filter_chain1+=${saved_filter_string['deint']}
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	filter_list=${filter_chain1:+,}
	filter_list+="$deinterlace_filter="
	local -n deint_filter_opts=deinterlace_filter_${deinterlace_filter}_opts
	for key in "${!deint_filter_opts[@]}"; do
		filter_list+="$key="
		filter_list+="${deint_filter_opts[$key]}"
		filter_list+=":"
	done
	filter_list=${filter_list%?}  #  Strips the leftover colon at the end.

	filter_chain1+="$filter_list"
	saved_filter_string['deint']="$filter_list"
	mildec
	return 0
}


 # In case the requested subtitles are built-in and in ASS/SSA format,
#  they have to be extracted. This function extracts the ${src_s[track_id]}
#  and sets  ${src_s[external_file]}  instead.  ${src_s[track_id]}  is then
#  unset to avoid confusion in the later check.
#
extract_subs() {
	declare -g  src_s

	#  A measure against running twice on consecutive reassembling of the
	#  filter chain during the same run. In fact, a flag on a higher level
	#  ($saved_filter_string[*]) does the job, so this here should be not
	#  necessary.
	#
	declare -g  subtitles_extracted

	local  s_encoder

	#  Extracted subtitles must be saved in an appropriate format.
	#  the extension will thus match ffmpeg subtitle encoders, one of
	#  ass, srt, webvtt, mov_text
	#
	case ${src_s[codec]} in
		ass|ssa)
			s_encoder=ass;;

		subrip|srt)
			s_encoder=srt;;

		webvtt|vtt)
			s_encoder=webvtt;;

		mov_text)
			s_encoder=mov_text;;
	esac

	#  Formerly internal-------------------vvv
	src_s[external_file]="$TMPDIR/external/intsubs.$s_encoder"
	src_s[external_file_is_an_extracted_one]=t

	if [ -v using_subtitle_cache ]; then
		info "Using subtitles from cache “${src[atts_cache_id]}”."
		return 0
	elif [ -v subtitles_extracted ]; then
		#  This message shouldn’t be ever seen. Seeing it indicates, that
		#  protection against consecutive runs is broken.
		warn "Subtitles are already extracted, nothing to do."
		return 0
	else
		info "Extracting subtitles…"
	fi

	# NB: -map uses 0:s:<subtitle_track_id> syntax here.
	#   It’s the number among SUBTITLE tracks, not overall!
	# “?” in 0:s:0? is to ignore the lack of subtitle track.
	#   If the default setting is to add subs, it shouldn’t lead
	#   to an error, if the source simply doesn’t have subs:
	#   specifying “nosub” for them shouldn’t be a requirement.
	#
	FFREPORT="file=$LOGDIR/${LOGNAME//:/\\:}.ffmpeg-subs-extraction:level=32"  \
	$ffmpeg -y -hide_banner  -v error  -nostdin  \
	        -i "${src[path]}"  \
	        -map 0:s:${src_s[track_id]}  \
	        -scodec copy  \
	        "${src_s[external_file]}"  \
		|| err "Cannot extract subtitle stream ${src_s[track_id]}: ffmpeg error."
	sub-msg 'Ignore ffmpeg errors, if there are any.'

	#  src_s[track_id] may be unset now. It never should be used
	#  in checks, src_s[external_file] should.

	subtitles_extracted=t
	return 0
}


extract_fonts() {
	declare -g  ATTS_CACHE_POPULATED

	#  A measure against running twice on consecutive reassembling of the
	#  filter chain during the same run. In fact, a flag on a higher level
	#  ($saved_filter_string[*]) does the job, so this here should be not
	#  necessary.
	#
	declare -g  fonts_extracted

	local extracted

	if [ -v using_subtitle_cache ]; then
		info "Using fonts from the same cache."
		return 0
	elif [ -v fonts_extracted ]; then
		#  This message shouldn’t be ever seen. Seeing it indicates, that
		#  protection against consecutive runs is broken.
		warn "Fonts are already extracted, nothing to do."
		return 0
	else
		info "Extracting fonts…"
	fi


	milinc

	if (( src_c[attachments_count] > 0 ));  then
		pushd "$TMPDIR/external" >/dev/null
		#
		#  FFmpeg may complain about that “At least one output file
		#  must be specified” but extracts all attachments. And God
		#  forbid you to try “-f null -”, for this will make FFmpeg
		#  TO ACTUALLY START TRANSCODING the video.
		#
		#  Leave stdin open for “< <(yes)”! It’s to allow ffmpeg to
		#  receive a ‘y’ for the cases when it asks whether to over-
		#  write duplicates.
		#
		if ! (
				set +eE
				FFREPORT="file=$LOGDIR/${LOGNAME//:/\\:}.ffmpeg-fonts-extraction:level=32"  \
				$ffmpeg  -hide_banner  -v error  \
				         -dump_attachment:t ""  \
				         -i "${src[path]}" < <(yes)
		     )
		then
			# ffmpeg’s message will not obey our indentation, so…
			milsave && mildrop
			sub-msg "${__y}FFmpeg returned an error on extracting fonts.${__s}
			         In this particular case, ffmpeg often would report an error, “At least one
			         output…”, and that error can be safely ignored. If you look at the counter
			         of extracted fonts below and it outputs two identical numbers, like “12/12”,
			         no serious error has happened. Even if these numbers don’t match, and you
			         see something like “54/58”, this most often would indicate, that the source
			         video had some duplicate fonts in the container. Fonts added twice. FFmpeg
			         doesn’t like that and throws an error, because one copy would overwrite the
			         other. Nadeshiko tells ffmpeg to overwrite, but the error code and message
			         can’t be subdued. If you don’t see any issues with the encoded file – the
			         fonts and styles are intact – ignore this ffmpeg message."
			milrestore
		fi
		extracted=$(ls -1A | wc -l)
		[ -v src_s[external_file] ]  \
			&& extracted=$(( --extracted ))
		info "Extracted $extracted/${src_c[attachments_count]} attachments."
		popd >/dev/null

		ATTS_CACHE_POPULATED=t

	else
		info 'Video has no attachments.'
	fi


	if	[ "$subtitle_filter" = 'ass' ]  \
		&& [ -v src_s[external_file] ]  \
		&& grep -qEi '^\s*\[Fonts\]\s*$' "${src_s[external_file]}"
	then
		. "$LIBDIR/dump_attachments_from_ass_subs.sh"
		ass_dump_fonts "${src_s[external_file]}" "$TMPDIR/external"
		[ -v ATTS_CACHE_POPULATED ] || ATTS_CACHE_POPULATED=t
	fi

	mildec

	fonts_extracted=t
	return 0
}


check_fontconfig_support() {
	local  font
	local  font_list

	font_list=$(
		find "$TMPDIR/external" \( -iname "*.otf" -o -iname "*.ttf" \)
	)
	for font in ${font_list[@]}; do
		if	   [[ "$font" =~ \.[Oo][Tt][Ff]$ ]]   \
			&& [ -v ffmpeg_missing[fontconfig] ]
		then
			redmsg 'Video uses .otf fonts, but FFmpeg was built without fontconfig
			        and will not be able to render subtitles with their native fonts.'
			err 'FFmpeg is not able to render OTF fonts.
			     See the log for details.'
		fi
	done

	return 0
}


 # There’s a complexity with cache and time shifted subtitles: if the first
#  run, which would be cached, would also has time-shifted subtitles, then
#  this time shift would naturally be picked up for all later runs. Which
#  may not be the intention. Thus, cache should either store only the ori-
#  ginal, non-shifted version, or store each time-shifted subtitle file
#  separately and update the cache correspondingly. The latter case, though
#  would be preferable in terms of saved time, would require to copy the
#  cache from $TMPDIR/extracted files back to the cache folder. Which means
#  that its symbolic links may overwrite original files, if not handled
#  properly. To avoid unnecessary complications, which are easy to overlook,
#  the former way of handling cache was taken: that the cache must store
#  only the original file, and a copy to be made at run time, if a delay
#  is needed.
#
#  One time-shifted copy may still get to the cache, if it was made at the
#  first run. But once a cache is created, no other copies would be cached.
#
adjust_time_in_subtitles() {
	declare -g  subtitles_delay_already_applied

	[ -v subtitles_delay_already_applied ] && return 0

	info "Applying subtitle delay. Shift: $sub_delay."
	milinc

	#  Working only on previously extracted or external subtitles (copied
	#  to TMPDIR), indeed. Basically, a precaution; this message should
	#  never appear.
	#
	[ -v src_s[external_file] ]  \
		|| err "Cannot apply subtitle delay: subtitles avoided extraction?"

	local  s_encoder=${src_s[external_file]##*.}
	local  time_shifted_subsfile

	time_shifted_subsfile="${src_s[external_file]%.*}.sub-delay_applied.$s_encoder"

	FFREPORT="file=$LOGDIR/${LOGNAME//:/\\:}.ffmpeg-subs-delay-timeshift:level=32"  \
	$ffmpeg -y  -hide_banner  -v error  -nostdin  \
	        -itsoffset "$sub_delay"  \
	        -i "${src_s[external_file]}"  \
	        -scodec $s_encoder  \
	        "$time_shifted_subsfile"

	#  It’s better to preserve src_s[external_file] as the target to be
	#  hardsubbed throughout the entire subtitle preparation process, so
	#  it is the subtitle file intended for hardsub that is being replaced
	#  rather than src_s[external_file] changed to a new path.
	#
	#  (src_s[external_file] is NEVER the very same file on the user’s
	#  drive, but either a new file, extracted from the video file or
	#  a symbolic link to the *originally* external file – that’s why
	#  it’s safe to replace the file.)
	#
	#  Having to deal with varoious types of subtitle sources (e.g. sym-
	#  links) affects the filter string composition and the order of
	#  processing them, as well as imposing restrictions on the later
	#  stage processors. In this case the delay adjustment process is
	#  a late-stage one and cannot affect the composition of the filter
	#  string without breaking the order and introducing ambiguity.
	#  For this reason, replacing the file is preferred to altering
	#  src_s[external_file].
	#
	mv "${src_s[external_file]}" "${src_s[external_file]}.before_time_shift"
	mv "$time_shifted_subsfile" "${src_s[external_file]}"


	subtitles_delay_already_applied=t

	mildec
	return 0
}


srt_style_calc_margins() {
	declare -g  current_bitres_profile
	local -n style=ffmpeg_subtitle_fallback_style

	style[MarginL]=$(echo "scale=0; ${style[Fontsize]} * 1.5 * ${src_v[display_aspect_ratio]} /1" | bc)
	style[MarginR]="${style[MarginL]}"

	#  There is some margin already from… somewhere,
	#  so it’s enough to set it to the type size to get 1.5 line spacing.
	style[MarginV]="${style[Fontsize]}"

	return 0
}


 # Imitates “sub-scale” mpv option. Only works on “SSA” currently.
#
#  Mpv’s “sub-scale” works in a peculiar way: it scales subtitles, that are
#  /normally positioned/. That is, not just the style named “Default”, but
#  any style as long as it doesn’t use \pos() or the like (\move(), org(),
#  \frxNNN, \fryNNN \frzNNN). Scaling does affect subtitles that are aligned
#  to a screen edge (using the “numpad alignment” – \anN). Supposedly, because
#  using top/8 allows normal subtitles to avoid clashing with titles burned to
#  frame by design. This behavious is what this function emulates.
#
apply_subtitle_scaling() {
	declare -g  subtitles_already_scaled

	[ -v subtitles_already_scaled ] && return 0

	info "Applying subtitle scaling. Factor: $sub_scale."
	milinc

	local    subsfile_with_scaling_applied
	local    Events_section_entered
	local    Graphics_or_Fonts_section_entered
	local    line
	local -A style_def_fs=()  # the original font size of the style…
	local -A style_sc_fs=()   # …and with scaling applied
	local    line_pre_style_name
	local    line_style_name
	local    line_post_style_name
	local    line_per_se  # I.e. the visible subtitle text plus tags.
	local    line_per_se_processed
	local    st_name
	local    st_fs
	local    key
	local    i

	subsfile_with_scaling_applied="${src_s[external_file]%.*}.scaling_applied.ass"
	echo -n >"$subsfile_with_scaling_applied"

	while read -r line; do
		#
		#  To increase processing speed, the regexps are used only after
		#  a boolean test, which is faster and saves time.
		#
		#  Order of these boolean flags is important!
		#
		if [ -v Graphics_or_Fonts_section_entered ]; then
			: # nothing to do, just copy the $line to destination

		elif [ -v Events_section_entered ]; then
			if [[ "$line" =~ ^\ *(Dialogue\:\ +([^,]*,){3})([^,]+),(([^,]*,){5})(.+)$ ]]; then
				line_pre_style_name="${BASH_REMATCH[1]}"
				line_style_name=${BASH_REMATCH[3]}
				line_post_style_name="${BASH_REMATCH[4]}"
				line_per_se="${BASH_REMATCH[6]}"

				#  We’ll do font size adjustments only if the line won’t
				#  have any of these tags.
				#
				[[ "$line_per_se" =~ \{.*(\\(pos|move|org)\(|\\fr(x|y|z)[0-9]{1,3}).*\} ]] || {
					#
					#  Consider, that “\pos”itioned text may use the same glo-
					#  bal styles, which makes altering font size within style
					#  definition not reasonable. As for the “\fs” rules that
					#  are built in the “line per se”, we process them all.
					#  Althoguh when you think about that, it may seem like
					#  finding matching “\fs” rules is our problem, but it
					#  actually isn’t.
					#
					line_per_se_processed=''
					while [[ "$line_per_se" =~ ^(.*\{.*\\fs)([0-9]{1,3})(.*\}.*)$ ]]; do
						line_per_se_processed="${BASH_REMATCH[1]}"
						line_per_se_processed+=$( round "${BASH_REMATCH[2]} * $sub_scale" )
						line_per_se="${BASH_REMATCH[3]}"
					done
					line_per_se="${line_per_se_processed:-}$line_per_se"

					line_per_se="{\fs${style_sc_fs[$line_style_name]}}$line_per_se"

					line="$line_pre_style_name"
					line+="$line_style_name,"
					line+="$line_post_style_name"
					line+="$line_per_se"
				}

			elif [[ "$line" =~ ^\ *\[(Graphics|Fonts)\] ]]; then
				Graphics_or_Fonts_section_entered=t
			fi

		else
			if [[ "$line" =~ ^\ *Style\:\  ]]; then
				st_name="${line#*Style\:\ }"
				st_name="${st_name%%,*}"
				st_fs="${line#*,}"
				st_fs="${st_fs#*,}"
				st_fs="${st_fs%%,*}"
				style_def_fs[$st_name]=$st_fs

			elif [[ "$line" =~ ^\ *\[Events\] ]]; then
				Events_section_entered=t
				#  Finalise reading the styles section.
				for key in ${!style_def_fs[*]}; do
					style_sc_fs[$key]=$( round "${style_def_fs[$key]} * $sub_scale" )
				done
			fi
		fi
		#
		#  ^ Subtitle lines may end with an \r, so don’t expect
		 #   an expression like “[[ … =~ …\ *$ ]]” to succeed.

		echo "$line"
	done < "${src_s[external_file]}"  >>"$subsfile_with_scaling_applied"

	 # On the role of “src_s[external_file]” as a universal target see the
	#  notes in “adjust_time_in_subtitles()” above.
	#
	mv "${src_s[external_file]}" "${src_s[external_file]}.before_scaling"
	mv "$subsfile_with_scaling_applied" "${src_s[external_file]}"

	subtitles_already_scaled=t

	mildec
	return 0
}


add_filter_subs() {
	[ -v subs ] || return 0

	info "Hardsubbing: overlay / (setpts → subtitles/ass → setpts)"
	milinc

	declare -g  filter_chain1
	declare -gA saved_filter_string

	[ -v saved_filter_string['subs']  ] && {
		filter_chain1+="${saved_filter_string['subs']}"
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	local  filter_list
	local  overlay_subs_w
	local  overlay_subs_h
	local  xcorr=0
	local  ycorr=0
	local  forced_style
	local  key
	local  tr_id
	local  subtitle_filter
	local  using_subtitle_cache
	local  target_path

	[ -d "$TMPDIR/external" ] || mkdir "$TMPDIR/external"

	case "${src_s[codec]}" in

		 # The “overlay” type subtitles, pixel-based.
		#
		#  ffmpeg recognises VobSub as “dvd_subtitle”, but this is not
		#  the only format for subttiles in DVD. See the definition of
		#  “known_sub_codecs” variable in metaconf/nadeshiko.10_sub-
		#  titles_meta.sh
		#
		dvd_subtitle|hdmv_pgs_subtitle)
			filter_list="${filter_chain1:+[v_interm],}"  # Sic!

			[ -v sub_delay ]  \
				&& warn "Subtitle delay isn’t supported for DVD and PGS subtitles.
				         sub_delay=$sub_delay will have to be ignored."

			 # Internal VobSub or PGS (dvd and blu-ray) require
			#  mapping and the “overlay” filter.
			#
			#  Subtitles need to be centered, if their resolution
			#  doesn’t match the resolution of the video.
			#

			 # HDMV_PGS_SUBTITLE hack
			#  In some blu-ray (rips) subtitle stream width and height
			#  are set to “N/A” and get stripped by gather_source_info().
			#  If PGS subtitle resolution is not specified, Nadeshiko will
			#  assume, that it should be equal to the resolution of the
			#  video. (Otherwise how would it play?)
			#
			if [[  "${src_s[width]:-}" =~ ^$INT$  \
			         &&  \
			       "${src_s[height]:-}" =~ ^$INT$  ]]
			then
				overlay_subs_w=${src_s[width]}
				overlay_subs_h=${src_s[height]}
			else
				warn "Subtitles have unspecified dimensions, assuming the dimensions
				      of the video stream. If you experience problems with hardsubbing,
				      consider finding another source for this video, made by people
				      who know what they are doing."
				overlay_subs_w=${src_v[width]}
				overlay_subs_h=${src_v[height]}
			fi

			 # Overlay subtitle correction is implemented for DVD subs
			#  though it may be useful for HDMV PGS too.
			#
			(( overlay_subs_w != src_v[width] )) && {
				#  Need to center by X
				(( overlay_subs_w > src_v[width] ))  \
					&& xcorr="-(overlay_w-main_w)/2" \
					|| xcorr="(main_w-overlay_w)/2"
			}
			(( overlay_subs_h != src_v[height] )) && {
				#  Need to center by Y
				(( overlay_subs_h > src_v[height] ))  \
					&& ycorr="-(overlay_h-main_h)/2"  \
					|| ycorr="(main_h-overlay_h)/2"
			}

			#  Breaks syntax highlight, if put inside as is.
			tr_id=${src_s[track_id]}

			#  Overlay filter needs to be specified either the source
			#  video stream, or the video stream processed in the previous
			#  filter (crop or scale). Simply “[0:V]” won’t work, because
			#  if another filter would be present behind, filtergraph will
			#  create another input-output mapping pair, which will come
			#  in conflict with the specified “[0:V]”.
			#
			[ "${filter_list:-}" ]  \
				&& filter_list+="[v_interm][0:s:$tr_id]"  \
				|| filter_list+="[0:s:$tr_id]"  # [0:V] is added at the end
				                                # of Chain 1.

			filter_list+="overlay=$xcorr:$ycorr"
			;;

		 # Text-based subtitles
		#
		ass|ssa|subrip|srt|webvtt|vtt|mov_text)
			filter_list=${filter_chain1:+,}

			[ ! -v disable_atts_cache ] && find_cached_attachments && {
				copy_attachments_from_cache
				using_subtitle_cache=t
			}

			 # Don’t rake your head, PTS here is a special variable, that counts
			#  decoded frames in TimeBase units. So basically “/TB” here is
			#  needed to convert the human-readable time into TB units. It won’t
			#  be far from truth to say that TB units on the left and the right
			#  parts of equation are cancelled, so the gist of it is the human-
			#  readable seconds that remain. What they do is to set an offset
			#  for the PTS. We effectively shift the PTS position by a certain
			#  amount of seconds, but we have to convert it to TB units, hence
			#  the “/TB” part.
			#
			[ -v seekbefore ]  \
				&& filter_list+="setpts=PTS+${ffmpeg_adaptive_seek_ss1[total_s_ms]}/TB,"  \
				|| filter_list+="setpts=PTS+${start[total_s_ms]}/TB,"

			#  “ass” filter has an option “shaping” for better font
			#  rendering, that’s not available in the “subtitles”
			#  filter. But on the other hand, “ass” doesn’t recognise
			#  option “force_style”, so we can’t use it for converted
			#  SubRip/VTT subs.
			#
			if [[ "${src_s[codec]}" =~ ^(subrip|srt|webvtt|vtt|mov_text)$ ]]; then
				subs_need_style_from_rc=t
				subtitle_filter='subtitles'
				[ ! -v src_s[external_file]  -a  -v sub_delay ]  \
					&& extract_subs
			else
				subtitle_filter='ass'
				#  “ass” filter, being true to its name, cannot be specified
				#  subtitle track id within the video. Yes, it copies the
				#  behaviour of the “subtitles” filter, but here it does
				#  not – be so kind to disembowel the file, that may take
				#  20 gigs on the disk to take out subtitles and put it
				#  on a silver plate to ffmpeg as a separate file.
				#  REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE—
				[ ! -v src_s[external_file] ] && extract_subs
			fi

			filter_list+="$subtitle_filter="

			#  Composing the next part of the filter string depends on whether
			#  the subtitles are in an external file or they are builtin.
			#
			#  Here we are running ahead of the train for a bit, because the
			#  processing (applying delay and scaling) has not been done yet,
			#  but we assign file paths to the filter chain already. This is
			#  because it’s more convenient to unify the paths (make working
			#  copies to $TMPDIR, only one, actually) before starting to pro-
			#  cess the subtitle files. This alleviates the need to catch up
			#  with the changes and evaluate the correct path within each
			#  processing function.
			#
			#  Making a copy is necessary to leave the “$TMPDIR/external” sub-
			#  folder clean of changes, so that it can be cached in its pris-
			#  tine state and used in the next runs by other files without
			#  changes applied to it (otherwise they would accumulate).
			#
			#  “intsubs” are formerly internal ones (built-in in the video
			#  container) and “extsubs” are those, whose origin was a stand-
			#  alone file (they’re external by origin). The “$TMPDIR/external”
			#  subfolder is called so only to make it evident, that links there
			#  lead to files on the disk, that they aren’t temporary in the
			#  sense of “for this run only”, and must be treated as such,
			#  roughly speaking, as “read-only” files.
			#
			if [ -v src_s[external_file] ]; then
				if [ -v src_s[external_file_is_an_extracted_one] ]; then
					cp "${src_s[external_file]}"  \
					   "$TMPDIR/intsubs.${src_s[external_file]##*\.}"
					src_s[external_file]="$TMPDIR/intsubs.${src_s[external_file]##*\.}"
					filter_list+="filename=${src_s[external_file]}"
				else
					target_path="$TMPDIR/extsubs.${src_s[external_file]##*\.}"
					cp "$(realpath "${src_s[external_file]}")"  \
					   "$target_path"
					src_s[external_file]="$target_path"
					filter_list+="filename=${src_s[external_file]}"
				fi

			else  # = (el)if [ -v src_s[track_id] ]; then
				#
				#  Only SRT, (Web)VTT and MovText types, SSA/ASS is always
				#  extracted and uses subtitles as a separate file.
				#

				#  To use built-in subtitles without taking them out, the
				#  subtitles filter needs to be specified /the filename/
				#  with subtitle track index. As escaping ${src[path]} will
				#  probably lead to errors anyway, it’s simpler to create
				#  a symlink in TMPDIR and tell ffmpeg to use the file
				#  via that symlink.
				#
				target_path="$TMPDIR/THE_VIDEO.${src[path]##*\.}"

				ln -s "${src[path]}" "$target_path"

				#  Sic! When the “subtitles” filter is using a file and
				#  stream index, no “filename=” should be there.
				#
				filter_list+="$target_path:si=${src_s[track_id]}"
			fi

			extract_fonts

			[ ! -v using_subtitle_cache  -a  -v extra_fonts_dir ] && {
				while IFS= read -r -d ''; do
					[ ! -e "$TMPDIR/external/${REPLY##*/}" ]  \
						&& ln -s "$REPLY" "$TMPDIR/external/"
				done < <(find -L "$extra_fonts_dir" -type f -size +0 -printf "%p\0")
			}

			check_fontconfig_support

			#  See the description to adjust_time_in_subtitles for
			#  the reason why there’s no check for using_subtitle_cache.
			#
			[ -v sub_delay ]  \
				&& adjust_time_in_subtitles

			[ -v sub_scale  -a  "$subtitle_filter" = 'ass' ]  \
				&& apply_subtitle_scaling

			rmdir "$TMPDIR/external" 2>/dev/null || {
				[ -v using_subtitle_cache ]  \
					|| backup_attachments_to_cache
				filter_list+=":fontsdir=$TMPDIR/external"
			}

			[ -v subs_need_style_from_rc ] && {
				srt_style_calc_margins
				(( ${#ffmpeg_subtitle_fallback_style[*]} > 0 )) && {
					for key in ${!ffmpeg_subtitle_fallback_style[@]}; do
						forced_style="${forced_style:+$forced_style,}"
						forced_style+="$key="
						forced_style+="${ffmpeg_subtitle_fallback_style[$key]}"
					done
					filter_list+=":force_style='$forced_style'"
				}
			}

			#  For OpenType positioning and substitutions.
			[ "$subtitle_filter" = 'ass' ]  \
				&& filter_list+=':shaping=complex'

			filter_list="${filter_list:+$filter_list,}"
			filter_list+='setpts=PTS-STARTPTS'
			;;
	esac

	filter_chain1+="$filter_list"
	saved_filter_string['subs']="$filter_list"
	mildec
	return 0
}


add_filter_crop() {
	[ -v crop ] || return 0

	info "Cropping: crop"
	milinc

	declare -g  filter_chain1
	declare -gA saved_filter_string

	[ -v saved_filter_string['crop']  ] && {
		filter_chain1+="${saved_filter_string['crop']}"
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	local  filter_list

	filter_list=${filter_chain1:+,}
	filter_list+="$crop"

	filter_chain1+="$filter_list"

	#  Yes, it hurts a bit, when the description of what a function is per-
	#  forming takes more space than its actual body, but for consistency’s
	#  sake it must be done. It would seem suspicious in the log, if a certain
	#  filter’s string wouldn’t have a message about it being “perused from
	#  backup” – as if there’s a problem, and that string is assembled again
	#  by some mistake, or as if some shady stuff happens.
	#
	saved_filter_string['crop']="$filter_list"
	mildec
	return 0
}


add_filter_scale_sar_conv() {
	[ -v scale  -o  -v needs_sar_conversion ] || return 0

	info "Scaling, adjusting AR: scale / scale → setsar"
	milinc

	#  For consistency’s sake
	declare -g  filter_chain1
	#
	local  filter_list=${filter_chain1:+,}

	if [ -v scale ]; then
		filter_list+="scale=-2:$tgt_height"
	else
		#  This looks like a regular scale with preserved aspect ratio,
		#  but if we’d add “*sar” to the left part of the expression, the video
		#  would turn out /too wide/, if it has that non-square aspect ratio
		#  in its pixels.
		#
		#  For those old DVDs that packed a 16:9 frame into 4:3 frame reso-
		#  lution, multiplying by dar kinda “implies” the corrected aspect
		#  ratio, as the original (4:3) width would be smaller than specified
		#  in the container. Thus specifying “*sar” would double the effect
		#  and turns out to be superfluous. The expression below may safely be
		#  used as the only expression, however, there still would have to be
		#  an explanation as to why the simple “scale=-2:$scale” weren’t used.
		#
		filter_list+="scale=trunc($tgt_width/2)*2:trunc($tgt_height/2)*2"
		filter_list+=','
		filter_list+='setsar=1:1'
	fi

	filter_chain1+="$filter_list"
	mildec
	#  It’s one of the filter graph composition modules, which are reassembled
	#  on consecutive rebuilding of the filter graph string. (When the video
	#  is to be re-encoded within the same run.) So no reason to back up the
	#  value.
	return 0
}


check_source_and_target_csp_dyn_range() {
	declare -g  cspace_conv_with_dynrange_change

	local  source_dynrange_group='sdr'
	local  target_dynrange_group='sdr'
	local  dr_group_name
	local  dr_group_members=()
	local  dr_group_item

	for dr_group_name in "${!known_cspace_hdr_tone_curves[@]}"; do
		dr_group_members=(
			${known_cspace_hdr_tone_curves[$dr_group_name]}
		)
		for dr_group_item in "${dr_group_members[@]}"; do
			[ "${src_v[cspace_tone_curve]}" = "$dr_group_item" ]  \
				&& source_dynrange_group="$dr_group_name"
			[ "${target_colour_space[tone_curve]}" = "$dr_group_item" ]  \
				&& target_dynrange_group="$dr_group_name"
		done
	done

	[ "$source_dynrange_group" != "$target_dynrange_group" ]  \
		&& cspace_conv_with_dynrange_change=t

	return 0
}


add_filter_csp_conv() {
	[ -v do_colour_space_conversion  -o  -v do_only_colour_range_conversion ]  \
		|| return 0

	declare -g  filter_chain1
	declare -gA saved_filter_string

	info "Colour space conversion: ${colour_space_filter} → format → tonemap → ${colour_space_filter} [→ normalize] / scale"
	milinc

	[ -v saved_filter_string['csp_conv']  ] && {
		filter_chain1+="${saved_filter_string['csp_conv']}"
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	declare -g  colour_space_filter
	declare -g  target_colour_space

	local -n pix_fmt="${ffmpeg_vcodec//-/_}_pix_fmt"

	local  filter_list

	if [ -v do_colour_space_conversion ]; then

		filter_list=${filter_chain1:+,}

		check_source_and_target_csp_dyn_range
		filter_list+="${colour_space_filter}="

		#  Only zscale, at the moment.

		local -n filter_matrix_coefs=known_cspace_matrix_coefs_for_filter_${colour_space_filter}
		local matrix_in=${filter_matrix_coefs[${src_v[cspace_matrix_coefs]}]}
		local matrix_out=${filter_matrix_coefs[${target_colour_space[matrix_coefs]}]}

		local -n filter_primaries=known_cspace_primaries_for_filter_${colour_space_filter}
		local primaries_in=${filter_primaries[${src_v[cspace_primaries]}]}
		local primaries_out=${filter_primaries[${target_colour_space[primaries]}]}

		local -n filter_tone_curves=known_cspace_tone_curves_for_filter_${colour_space_filter}
		local transfer_in=${filter_tone_curves[${src_v[cspace_tone_curve]}]}
		local transfer_out=${filter_tone_curves[${target_colour_space[tone_curve]}]}

		local range_in=${src_v[cspace_colour_range]}
		local range_out=${target_colour_space[colour_range]}

		filter_list+="matrixin=$matrix_in:"
		filter_list+="primariesin=$primaries_in:"
		filter_list+="transferin=$transfer_in:"
		filter_list+="rangein=$range_in"
		[ -v cspace_conv_with_dynrange_change ] && {
			check_colour_space_filters_extra
			filter_list+=":transfer=linear"
			filter_list+=":npl=$ffmpeg_filter_zscale_npl"
			filter_list+=",format=gbrpf32le"
			filter_list+=",tonemap="
			filter_list+="tonemap=$ffmpeg_filter_tonemap_alg:"
			filter_list+="param=$ffmpeg_filter_tonemap_param_value:"
			filter_list+="desat=0"
		}
		filter_list+=",${colour_space_filter}="
		filter_list+="matrix=$matrix_out:"
		filter_list+="primaries=$primaries_out:"
		filter_list+="transfer=$transfer_out:"
		filter_list+="range=$range_out"
		if [ -v cspace_conv_with_dynrange_change   \
		       -a  \
		     -v ffmpeg_filter_use_normalize ]
		then
			filter_list+=",normalize="
			filter_list+="blackpt=black:"
			filter_list+="whitept=white:"
			filter_list+="smoothing=$ffmpeg_filter_normalize_smoothing"
		fi

	elif [ -v do_only_colour_range_conversion ]; then
		filter_list=${filter_chain1:+,}
		filter_list+="scale="
		local range_in=${src_v[cspace_colour_range]}
		local range_out=${target_colour_space[colour_range]}
		filter_list+="in_range=$range_in:"
		filter_list+="out_range=$range_out"
	fi

	filter_chain1+="$filter_list"
	saved_filter_string['csp_conv']="$filter_list"
	mildec
	return 0
}


add_filter_minterpolate() {
	[ -v motion_interp_filter_needed ] || return 0

	info "Smoothing frame rate change: minterpolate"
	milinc

	declare -g  filter_chain1
	declare -gA saved_filter_string

	[ -v saved_filter_string['minterp']  ] && {
		filter_chain1+="${saved_filter_string['minterp']}"
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	local  filter_list=${filter_chain1:+,}

	filter_list+="minterpolate="
	filter_list+="fps=$frame_rate:"
	filter_list+="mi_mode=mci:"
	filter_list+="mc_mode=aobmc:"
	filter_list+="me_mode=bidir:"
	filter_list+="vsbmc=1"

	filter_chain1+="$filter_list"
	#  See the corresponding in “add_filter_crop()”
	saved_filter_string['minterp']="$filter_list"
	mildec
	return 0
}


add_filter_loop_last_frame() {
	[ -v loop_last_frame ] || return 0

	declare -g  filter_chain2=''

	info "Looping the last frame: tpad"
	milinc

	filter_chain2=${filter_chain1:+,}
	filter_chain2+="tpad=stop_mode=clone:stop_duration=$loop_last_frame_s"

	mildec

	 # This function is one of those filter graph composition modules, which
	#  are reassembled on the consecutive rebuilding of the filter graph
	#  string. (When the video is to be re-encoded within the same run.)
	#  So, no reason to back up the value of “$filter_chain2”.
	#
	return 0
}
#  Thanks be to https://stackoverflow.com/a/43417253


add_filter_loop_last_frame_alter() {
	[ -v loop_last_frame ] || return 0

	declare -g  filter_chain1
	declare -g  filter_chain2=''
	declare -g  loop_last_frame_tries_complete
	declare -g  loop_last_frame_incr_reserved_frames
	: ${loop_last_frame_tries_complete:=0}

	local -i  starting_reserve_fr=0

	local  filter_list

	 # Amount of frames to withhold from the seconds, which will be looped,
	#  to make the “loop” and the “setpts” filters work together. As the whole
	#  thing is like an arcane magic, 1/3 of which I figured by trial and error,
	#  I can’t explain it well. But it seems that ffmpeg counts the last frame
	#  /from the ordinary fragment, i.e. non-looped one/ as the part of dura-
	#  tion  that we’re going to loop, and this creates an overlap. The whole
	#  duration, as it is estimated, is in fact not correct (enough) for the
	#  “setpts” filter to work, and without slightly correcting the value we
	#  can’t get the last frame looped properly. (The “loop” filter plays the
	#  last frame a bit, then jumps to the very first frame of the fragment and
	#  loops that. Sometimes – when done not the correct way – an extra frame
	#  flashes at the end of the encoded fragment. Yes, it is that same first
	#  frame.) The adjustment allows the “loop” filter to process the frame(s)
	#  for the duration that was requested and without any extra at the end.
	#
	#  The adjustment is applied to PTS, which is measured in TimeBase units,
	#  that is, every frame has a number, so we need to convert the duration
	#  of a single frame to seconds (or a fraction of a second for a ≈24 fps
	#  target), before we could subtract that amount from the “$loop_last_
	#  frame_s” as specified in the filter string.
	#
	#  By default the amount of frames is set to 1 (which takes away less than
	#  50 ms from the looped part on 24 fps target rate), though the amount
	#  needed depends on the source properties and encoding settings. It doesn’t
	#  depend on the source fps, however: one CFR 23.976 fps source would re-
	#  quire 1 “frame-space” subtracted, and another would need 2.
	#
	#  Supposing that you’ve read the code below and wondering, if it’s that
	#  +1 frame to “$times” which makes it necessary to perform the -(1/frame_
	#  rate/TB) subtraction – some sources can be encoded safely, and with the
	#  last frame looped correctly, if you remove that +1 and make the addition
	#  to “setpts” just “PTS-STARTPTS+$loop_last_frame_s/TB”. Yes, it will work.
	#  But the other sources would still require the addition/subtraction hack
	#  to work. So it has to be there. The “$times” variable being calculated
	#  outside of ffmpeg filter string (and not placed as numbers to it for
	#  ffmpeg to calculate) also has nothing to do with the need for the hack.
	#
	declare -g  setpts_reserved_space_fr

	info "Looping the last frame: setpts → select → loop → setpts; concat"
	milinc
	[ -v seekbefore_due_to_tr_or_pr_stream ] && {
		info "Transport or program stream detected in the source.
		      Forcing ${__y}${__bri}exponential${__s} increment of reserved frames."
		loop_last_frame_incr_reserved_frames=exponential
		starting_reserve_fr+=2
	}

	case "$loop_last_frame_incr_reserved_frames" in
		linear)
			starting_reserve_fr+=1
			setpts_reserved_space_fr=$((   starting_reserve_fr
			                             + loop_last_frame_tries_complete ))
			;;
		exponential)
			setpts_reserved_space_fr=$((   starting_reserve_fr
			                             + 2**(1+loop_last_frame_tries_complete) ))
			;;
	esac

	local  filter_list="${filter_chain1:+[v_fc1];}"
	local  times

	times=$( echo "scale=0; $frame_rate * $loop_last_frame_s / 1 + 1" | bc )
	#  One extra frame to compensate for the possible              ^
	#  fractional part, that is lost

	# Sic! Filter starts its own chain, so no comma in front.
	filter_list+="[0:V] setpts=${stop[total_s_ms]}/TB,"
	filter_list+="select='eq(t\,${stop[total_s_ms]})',"

	#  Won’t it be an issue to use [v_interm] output tags more than once?
	[ "${filter_chain1:-}" ]  \
		&& filter_list+="$filter_chain1,"

	filter_list+="loop="
	filter_list+="loop=$times:"
	filter_list+="size=1,"
	filter_list+="setpts=PTS-STARTPTS+($loop_last_frame_s-$setpts_reserved_space_fr/$frame_rate)/TB [v_fc2];"  #  ← new chain!

	[ "${filter_chain1:-}" ]  \
		&& filter_list+="[v_fc1][v_fc2] concat"  \
		|| filter_list+="[v_fc2] concat"  #  [0:V] in front gets added
		                                  #  when chains are joined.

		                                  #  [v_out] is common, there again.

	filter_chain2="$filter_list"

	mildec

	 # This function is one of those filter graph composition modules, which
	#  are reassembled on the consecutive rebuilding of the filter graph
	#  string. (When the video is to be re-encoded within the same run.)
	#  So, no reason to back up the value of “$filter_chain2”.
	#
	return 0
}


add_filter_audio_delay() {
	[ -v audio -a -v audio_delay ] || return 0

	declare -gA saved_filter_string
	declare -g  filter_chain3

	local  filter_list
	local  audio_delay_with_sign="$audio_delay"
	# local  audio_delay_modulus

	info "Applying audio delay. Shift: $audio_delay."
	milinc

	[ -v saved_filter_string['audio_delay']  ] && {
		filter_chain3+="${saved_filter_string['audio_delay']}"
		info "Using previously built piece of the filter graph."
		mildec
		return 0
	}

	[[ "$audio_delay" =~ ^\- ]]  \
		|| audio_delay_with_sign="+$audio_delay"

	# audio_delay_modulus=${audio_delay_with_sign#?}  # currently not in use

	#  NB:  PTS ± (Adelay s ÷ TB units)
	filter_list="asetpts=PTS$audio_delay_with_sign/TB"
	filter_list+=",aselect=between(t"
	filter_list+="\,${ffmpeg_adaptive_seek_ss2_offset[total_s_ms]}"
	filter_list+="\,${ffmpeg_adaptive_seek_ss2_offset[total_s_ms]}"
	filter_list+="+${duration[total_s_ms]})"

	filter_chain3+="$filter_list"
	saved_filter_string['audio_delay']="$filter_list"

	mildec
	return 0
}


 # Assembles -filter_complex string.
#  DO NOT put commas at the end of each filter. With commas in front, parts
#  of the chain can be safely tossed around as they are.
#
assemble_ffmpeg_filter_graph() {
	declare -g  ffmpeg_filters
	declare -g  current_bitres_profile

	#  Filter chains are assembled anew every time, when re-reuns happen.
	#  This is because generally, the filter graph string is volatile, its
	#  contents may vary between re-runs. Some “solid” parts are backed up
	#  for reuse, however.
	#
	declare -g  filter_chain1=''
	declare -g  filter_chain2=''
	declare -g  filter_chain3=''

	#  The global “already done once” flags are not used, because the variety
	#  of combinations makes it complicated. Instead of “global”, i.e. chain-
	#  based flags, there are local flags, per-subtitles, per-colour space
	#  conversion and so on, that are put to avoid reassembling parts of the
	#  filter chain, which remain unchanged in any case.
	#
	# declare -g  ffmpeg_filter_chain1_assembled
	# declare -g  ffmpeg_filter_chain2_assembled

	#  Stores the backed up parts of the filter graph string, which can safely
	#  be reused, no matter what changes, when a rerun occurs. Generally this
	#  is for parts responsible for subtitles, colour space conver-
	#  sion.
	#
	declare -gA  saved_filter_string

	local  tgt_height
	local  tgt_width
	local  map_ainput

	info "Composing filter graph string for ffmpeg."
	milinc


	 #                              Chain 1
	#                    The main video processing chain

	 # The first part, the main one, assembles filter settings that work with
	#  dimensions (crop, scale…), overlaying subtitles and converting colour
	#  space.
	#
	#  When possible, reassemblage of the filter chain is avoided on consecu-
	#  tive runs (when the same .webm or .mp4 is remade). Ideally, “fit_bitrate
	#  _to_filesize” should be leaving a flag to reassemble the parts for
	#  “scale” and subtitle filters. This, however, would be tedious, as more
	#  filters may appear, and they all comprise a single chain, whose string
	#  would also need to be updated. It’s easier to separate filters on those
	#  that can be set once and used again as is, and those which may require
	#  reassembling their string under specific conditions.
	#

	 # The condition and the flag for the entire chain one are disabled,
	#  because multiple use cases require reassembling it. To avoid unnecessary
	#  time-consuming tasks, such as attachment extraction, the flags are moved
	#  within particular functions. Reasons include overshooting (may require
	#  different scale) and looping the last frame (where, depending on the
	#  method in effect, the first chain is wrapped in I/O tags one way or
	#  the other).
	#
	# if [ ! -v ffmpeg_filter_chain1_assembled  \
	#          -o  \
	#        -v previous_run_had_overshot_size ]
	# then
		if [ -v crop ]; then
			tgt_height=$crop_h
			tgt_width=$(echo "scale=0; $crop_w * ${src_v[pixel_aspect_ratio]} /1" | bc)
		else
			tgt_height=$current_bitres_profile
			tgt_width=$(echo "scale=0; $tgt_height * ${src_v[display_aspect_ratio]} /1" | bc)
		fi

		[ "$ffmpeg_filters_pre" ]  \
			&& filter_chain1+="$ffmpeg_filters_pre"

		 # For debugging purposes: display PTS (top left corner, in small numbers)
		#
		# filter_chain1="${filter_chain1:+$filter_chain1,}"
		# filter_chain1+="drawtext=text='%{pts\:gmtime\:0\:%M\\\\\:%S}'"

		add_filter_deinterlace

		#  Must run before crop and scale, if hardsub is needed.
		add_filter_subs

		add_filter_crop

		add_filter_scale_sar_conv

		#  Better run it after crop and scale to reduce the amount
		#  of data that will be fed to CPU to crunch.
		add_filter_csp_conv

		#  The laziest filter of the world.
		add_filter_minterpolate

		#  Must be the last string added!
		#
		[ "$ffmpeg_filters_post" ] && {
			filter_chain1="${filter_chain1:+$filter_chain1,}"
			filter_chain1+="$ffmpeg_filters_post"
		}

		# ffmpeg_filter_chain1_assembled=t
	# fi


	 #                               Chain 2
	#                (or a continuation of Chain 1, depending
	#                    on how the processing is set up)
	#                       for looping the last frame

	 # The condition and the flag for the entire chain two are disabled,
	#  because the condition
	#   - is unnecessary:
	#      ⋅ there’s no chain3 at the moment;
	#      ⋅ “add_filter_loop_last_frame*()” functions have a built-in
	#        check on whether “$loop_last_frame” flag is set.
	#   - as it is now, it meddles with switching methods for the llf mode:
	#     “[ ! -v ffmpeg_…_chain2_assembled ]” basically means “this block
	#     will run at least once”; the check on the count of performed at-
	#     tempts in llf mode means “on second (and the next) attempts,
	#     allow this block, if llf is set up to redo the encoding.” Now
	#     watch the hands: the first run, llf is set, the block is execu-
	#     ted, because “ffmpeg…chain2_assembled” isn’t set yet. But assume,
	#     that the primary llf method failed, and llf switched to the alter-
	#     native one. The number of attempts is still zero(!) because the
	#     method changed (actually, the primary method never increases the
	#     attempt count, becuase it’s a single-attempt method). What will
	#     happen here, when Nadeshiko re-runs the encode, and “ffmpeg_…
	#     _chain2_assembled” would be set? Correct, the first check will
	#     fail, and the second will to (as the count for llf attempts is
	#     still zero). The block will not re-run. Currently such a global
	#     check like with “ffmpeg_…_chain2_assembled” would only bring
	#     problems.
	#
	# if [ ! -v ffmpeg_filter_chain2_assembled  \
	#          -o  \
	#      ${loop_last_frame_tries_complete:-0} -gt 0 ]
	# then
		if [ -v loop_last_frame_use_alter_method ]; then
			add_filter_loop_last_frame_alter  #  Requires to begin a new chain,
			                                  #  hence why this block is sepa-
			                                  #  rated into a new chain.
		else
			add_filter_loop_last_frame
		fi

		# ffmpeg_filter_chain2_assembled=t
	# fi


	 #                              Chain 3
	#                  Adjustments for audio track delay
	#
	add_filter_audio_delay


	 # Wrapping the video processing chains of the filter graph string in
	#  “[0:V] … [v_out]” is mandatory. ffmpeg needs to be hinted, that we
	#  process the video stream (and not some side track from another angle
	#  or a cover image – which one may get specifying [0:v] or not specifying
	#  anything). The use of specifiers is reflected in the use of the “-map”
	#  option.
	#

	[ "${filter_chain1:-}${filter_chain2:-}${filter_chain3:-}" ]  && {
		[ -v audio ] && {
			[ -v src_a[external_file]  ]  \
				&& map_ainput="${ffmpeg_input_files_idx[ext_audio]}:a:0"  \
				|| map_ainput="0:a:${src_a[track_id]}"
		}

		ffmpeg_filters=( -filter_complex )

		if [ "${filter_chain1:-}${filter_chain2:-}"  -a  -z "${filter_chain3:-}" ]; then
			ffmpeg_filters+=( "[0:V] ${filter_chain1:-}${filter_chain2:-} [v_out]" )

		elif [ "${filter_chain1:-}${filter_chain2:-}"  -a  "${filter_chain3:-}" ]; then
			ffmpeg_filters+=( "[0:V] ${filter_chain1:-}${filter_chain2:-} [v_out]" )
			ffmpeg_filters[-1]+="; [$map_ainput] $filter_chain3 [a_out]"

		else
			ffmpeg_filters+=( "[$map_ainput] $filter_chain3 [a_out]" )
		fi
	}

	mildec
	return 0
}


return 0