#  Should be sourced.

#  08_encode.sh
#  Nadeshiko module that selects and runs a codec-specific encoding module.
#  It is mostly an empty part of the conveyor, but it also defines two func-
#  tions to display a progressbar in the console during the encoding process.
#  (The functions are to be used by the codec-specific encoding modules.)
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


vchan setup  name=Encode_progressbar  \
             level=2  \
             meta=M  \
             group_item=launch_progressbar_for_ffmpeg  \
             group_item=stop_progressbar_for_ffmpeg  \
             min_level=2  \
             max_level=3


show_help_on_verbosity_channel_Encode_progressbar() {
	cat <<-EOF
	Encode_progressbar (self-report channel)
	    Channel for debugging the progress bar used for ffmpeg pass 1
	    and pass 2.

	Levels:
	    2 – no output (the default);
	    3 – adds details on the process that shuts down the progressbar;
	        writes the number of frames processed by ffmpeg to console
	        (or to a log); dumps the contents, retrieved from the progress
	        FIFO file between Nadeshiko and FFmpeg to a separate log file
	        \$TMPDIR/ffmpeg_progress.non-fifo.passN.log" (N = 1 or 2).

	EOF
	return 0
}


 # Launches a coprocess, that will read ffmpeg’s progress log and report
#  progress information to console.
#
launch_progressbar_for_ffmpeg() {
	[ -v ffmpeg_progressbar ] || return 0
	#  ffmpeg_progress_log must be defined before the check on whether
	#  console output should be prevented.
	declare -g  ffmpeg_progress_log="$TMPDIR/ffmpeg_progress.log"
	declare -g  progressbar_pid
	declare -g  frame_count

	#  Must be checked AFTER $ffmpeg_progress_log is set!
	[ -v do_not_report_ffmpeg_progress_to_console ] && return 0

	milinc
	[ "${src_v[frame_rate_mode]:-unknown}" = VFR ]  \
		&& warn 'Video has a variable frame rate. The progress can be only approximate.'
	[ -v seekbefore ]  \
		&& warn 'Decoder is working. The progress bar will appear shortly, please wait.'
	mildec

	local  progress_status
	local  frame_no                   #  First reported frame will have no. 1
	local  ffmpeg_progress_dump
	local  v_level=${VERBOSITY_CHANNELS[Encode_progressbar]}



	mkfifo "$ffmpeg_progress_log"
	tput civis  # sets cursor invisible

	{
		coproc {
			#  Wait for the ffmpeg process to appear.
			until pgrep --session 0  -f ffmpeg  \
			      && [ -r "$ffmpeg_progress_log" ]
			do
				sleep 1
			done

			line_length=$(( TERM_COLUMNS - ${#__mi} - 2 ))
			#  Avoid hitting the terminal border -----^
			readiness_pct='100% '

			progressbar_length=$(( $line_length - ${#readiness_pct} - 2 ))
			#                      opening and closing brackets  -----^

			#  Leave an empty line after the corprocess pid,
			#  when it’s printed to console.
			echo

			exec {ffmpeg_progress_log_fd}<>"$ffmpeg_progress_log"

			#  We’ll be saving the lines read from ffmpeg’s fifo to
			#  $ffmpeg_progress_dump. This coprocess may be terminated though,
			#  if it takes too much time to complete, so writing the log must
			#  be guaranteed when this subprocess receives SIGTERM. And if we
			#  are making a trap, we might as well use it on SIGEXIT.
			#
			(( v_level >= 3 )) && {
				export v_level ffmpeg_progress_dump TMPDIR pass
				write_ffmpeg_progress_non-fifo_dump() {
					echo "$ffmpeg_progress_dump"  \
						> "$TMPDIR/ffmpeg_progress.non-fifo.pass${pass:-unknown}.log"
					return 0
				}
				trap 'write_ffmpeg_progress_non-fifo_dump' EXIT TERM
			}

			 # Progress bar look:
			#   ⋅ [>-----] – at the start
			#   ⋅ [==>---] – processing, 50% done
			#   ⋅ [======] – complete
			#   ⋅ [=====>? – ffmpeg reported progress exceeding 100%.
			#
			until [ "${progress_status:-}" = end ]; do
				read -r -u "$ffmpeg_progress_log_fd"

				(( v_level >= 3 )) && [ "${REPLY:-}" ]  \
					&& ffmpeg_progress_dump+="$REPLY"$'\n'

				case "$REPLY" in

					progress=*)  #  → $progress_status

						[[ "$REPLY" =~ ^progress=(continue|end)$ ]]  \
							&& progress_status=${BASH_REMATCH[1]}  \
							|| err "FFmpeg returned an unknown value for the key “progress”: “${REPLY#progress=}”."
						;;

					frame=*)  #  → $frame_no
						      #
						      #  First pass progress can only be estimated with
						      #  the number of processed frames.

						(( pass == 1 )) && {
							#  Stripping the leading zeroes just in case
							[[ "$REPLY" =~ ^frame=([0]*)([0-9]+)$ ]]  \
								&& frame_no=${BASH_REMATCH[2]}  \
								|| err "FFmpeg returned an unknown value for the key “frame”: “${REPLY#frame=}”."
						}
						;;

					out_time=*)  #  → $encoded_up_to
						         #
						         #  out_time is updated only for second pass.
						         #  It’s indeed more precise than approximate
						         #  estimation by the frame count.
						         #
						         #  NB: out_time_us and out_time_ms are not to
						         #  be relied upon, see release notes to v2.23.

						(( pass == 2 )) && {
							[[ "$REPLY" =~ ^out_time=(-?)([0-9\:]+\.[0-9]{3})[0-9]*$ ]]  \
								|| err "FFmpeg returned an unknown value for the key “out_time”: “${REPLY#out_time=}”."

							#  If stupid ffmpeg returns a negative value in “out_time”,
							#  skip this iteration.
							[ "${BASH_REMATCH[1]}" ] && continue

							unset encoded_up_to
							new_time_array  encoded_up_to "${BASH_REMATCH[2]}"
						}
						;;
					*)
						continue
				esac

				[ "${progress_status:-}" = end ]  && {
					case "$pass" in
						1) frame_no=$frame_count;;
						2) encoded_up_to[total_ms]=$((   ${duration[total_ms]}
						                               + ${loop_last_frame_s:-0}*1000
						                            ))
						                        ;;
					esac
				}

				#  Caret return.
				echo -en "\r"

				#  Message indentation.
				echo -n "$__mi"

				#  Readiness.
				case "$pass" in
					1)
						[ -v frame_no ] || continue
						#
						#  There may be a small discrepancy between the
						#  $frame_count, which we calculate in Nadeshiko and
						#  then round it up, and the actual frame count in the
						#  encoded fragment. The real number of frames depends
						#  on where the frame group border will be. As this
						#  ends up being on the encoder conscience, the dis-
						#  crepancy is not considered an error.
						#
						(( frame_no > frame_count )) && frame_no=$frame_count
						readiness_pct=$(( frame_no * 100 / frame_count ))

						#  Could be done with “printf” but subshells would
						#  slow us down.
						until [[ "$readiness_pct" =~ ^...$ ]]; do
							readiness_pct=" $readiness_pct"
						done
						;;
					2)
						[ -v encoded_up_to ] || continue
						readiness_pct=$((   ${encoded_up_to[total_ms]}
						                  * 100
						                  / (   ${duration[total_ms]}
							                  + ${loop_last_frame_s:-0}*1000
							                )
							           ))

						until [[ "$readiness_pct" =~ ^...$ ]]; do
							readiness_pct=" $readiness_pct"
						done
						;;
				esac
				echo -n "${readiness_pct}% ["

				#  Assembling a line of blocks, that represent what is done.
				#  Literally, how much characters the done blocks comprise
				#  in the progressbar. Also avoiding the bug, when the encod-
				#  ing is finished, but the bar ends with ==>-]
				#
				if (( readiness_pct == 100 )); then
					blocks_done_no=$progressbar_length

				elif (( readiness_pct < 100 )); then
					case "$pass" in
						1)
							blocks_done_no=$((   frame_no
							                   * progressbar_length
							                   / frame_count  ))
							;;
						2)
							blocks_done_no=$((   ${encoded_up_to[total_ms]}
							                   * progressbar_length
							                   / (   ${duration[total_ms]}
							                       + ${loop_last_frame_s:-0}*1000
							                     )
							                ))
							;;
					esac

				elif (( readiness_pct > 100 )); then
					#
					#  A bug was reported, when readiness has exceeded
					#  the bounds. For this bug the bar should have the
					#  “progress going” block (the ‘>’), so the line
					#  should end like '…======>?  '
					#
					blocks_done_no=$(( progressbar_length -1 ))
				fi

				#  Until the encoding would finish, the last block done (the
				#  last ‘=’ in the bar) is always replaced with the “progress”
				#  block (‘>’). But when the encoding begins, the bar would
				#  look like “[------…” and this may lead one to believe, that
				#  ffmpeg couldn’t start for some reason. So we have to guaran-
				#  tee, that there’s at least one “done” block in the bar, so
				#  it could be replaced with “progress” (to indicate that the
				#  encoding goes on).
				#
				(( blocks_done_no == 0 )) && blocks_done_no=1
				blocks_done_chars=''
				for (( i=0; i<blocks_done_no-1; i++ )); do
					blocks_done_chars+='='
				done
				(( readiness_pct == 100 ))  \
					&& blocks_done_chars+='='  \
					|| blocks_done_chars+='>'
				echo -n "$blocks_done_chars"

				#  Now assembling the string of blocks that are “to be done”.
				#
				blocks_to_be_done_no=$(( progressbar_length - blocks_done_no ))
				blocks_to_be_done_chars=''
				for (( i=0; i<blocks_to_be_done_no; i++ )); do
					blocks_to_be_done_chars+='-'
				done
				echo -n "$blocks_to_be_done_chars"

				#  Avoiding the bug with the percentage exceeding 100.
				#  Why it exceeds 100% is not yet known – the reporter didn’t
				#  provide sufficient information about the case.
				#
				(( readiness_pct <= 100 ))  \
					&& echo -n ']'  \
					|| echo -n '?'

			done


			 # Jumping off from the line with the progress bar and making
			#  an empty line after the bar, symmetric to the empty line above.
			#  (The line above is necessary to avoid clutter caused by the
			#  (co)process id that is printed to console.)
			#
			echo -en '\n\n'
			exit 0
		} >&${STDOUT_ORIG_FD} 2>&${STDERR_ORIG_FD}
	} || true   #  errors in progressbar are not critical.
	progressbar_pid=$!
	return 0
}


stop_progressbar_for_ffmpeg() {
	[ -v ffmpeg_progressbar ] || return 0
	[ -v do_not_report_ffmpeg_progress_to_console ] && return 0

	local -x i

	#  Usually there are no problems with ffmpeg writing to progress FIFO and
	#  bash reading from it. It is normal, that the coprocess, that draws the
	#  progress bar on the screen, would be reading the FIFO for a couple more
	#  seconds after ffmpeg process quits.
	#
	#  The coprocess may lag behind in reading for multiple reasons:
	#    ⋅ the shell can’t read and process the data as fast as ffmpeg is
	#      dumping it (this is especially true for the pass 1, which may be
	#      going really fast).
	#    ⋅ the I/O itself is slow: /tmp is not on ramfs, weak CPU etc.
	#    ⋅ another heavy task is consuming the CPU or I/O, leaving little for
	#      ffmpeg and Nadeshiko.
	#  As v2.23 is going to be released, the algorithm of shell reading the
	#  FIFO was optimised, and the lag was reduced from 10 to just 1 second.
	#  So the lag should be minimal.
	#
	#  The 20 seconds of wait are there mostly to not unnerve the users. Sup-
	#  pose the wait is 3 seconds, but encoding is done on a slow computer.
	#  The shell is still reading the fifo, but the ffmpeg process has fin-
	#  ished its job long ago and this function, after it had waited for 3
	#  seconds, sends SIGKILL to the subprocess. The user sees how the pass 1
	#  progress bar (for example) abruptly ends on 87%, and then pass 2 starts
	#  like nothing happened. The user will suspect an error, this seems
	#  wrong. And the message about the coprocess being forcibly finished
	#  also leads him to this idea. However nothing bad happened in reality,
	#  it’s just the process representing the status of an already finished
	#  process took a critically long time. (If ffmpeg would fail on pass 1,
	#  Nadeshiko would report about that and quit.) So, we just allow the
	#  corpocess to take a bit of time on slow systems in order to not raise
	#  unnecessary worries.
	#
	for ((i=0; i<20; i++)); do
		[ -e /proc/$progressbar_pid ] && sleep 1 || break
	done
	#
	#  Progressbar process might quit by itself, but if it did not,
	#  it has to be killed. The “echo” is for the output to jump off
	#  the line with the progressbar, because if kill has succeeded,
	#  the function can’t do it, obviously.
	#
	if [ -e /proc/$progressbar_pid ]; then
		info 'After 5 seconds, progressbar subprocess is still running.'
		if kill -TERM $progressbar_pid 2>/dev/null; then
			echo
			info 'Killed progressbar process.'
		else
			info "Progress bar finished by itself after $i second$(plur_sing $i) in the last moment."
		fi
	else
		info "Progress bar finished by itself after $i second$(plur_sing $i)."
	fi

	rm -f "$ffmpeg_progress_log"
	tput cnorm  # turns the cursor back on
	return 0
}


encode() {
	if [ "$(type -t encode-$ffmpeg_vcodec)" = 'function' ]; then
		info "Running ffmpeg…"
		milinc
		encode-$ffmpeg_vcodec
		mildec
	else
		err "Cannot find encoding function “encode-$ffmpeg_vcodec”."
	fi

	return 0
}


return 0