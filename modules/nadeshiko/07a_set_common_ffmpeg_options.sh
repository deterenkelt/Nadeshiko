#  Should be sourced.

#  07a_set_common_ffmpeg_options.sh
#  “07{a…z}_set_common_ffmpeg_options” is a Nadeshiko module that translates
#  the requested, as well as the determined settings into parameters for
#  ffmpeg and does all the groundwork to gather auxiliary data and files toge-
#  ther, that ffmpeg filters may need.
#    To put it short, it gathers and fills all the remaining output options,
#  such as stream mapping, the filter chain, output metadata tags and the out-
#  put file name (which may need to be shortened); it does all that remains,
#  except the encoding options for a particular video codec library.
#    As the aforementioned is being assembled at the same time – between the
#  calculation stage and running ffmpeg, it comprises a sort of unit, even
#  though its parts are not related to each other. Thus the module is con-
#  sidered “stage 7” in the program and is split into several parts, larger
#  ones detached into separate files. This file, part “a)”, serves as a junc-
#  tion point for the other parts.
#
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


put_time_before_the_ffmpeg_command() {
	declare -ga time

	[ -v time_stat  -a  ! -v time ] && {
		#  Could be put in set_vars, but time is better to be applied after
		#  $ffmpeg_filters is assembled, or we get conditional third time.
		time=( command time -f %e -a -o "$LOGDIR/$LOGNAME.time_output" )
	}

	return 0
}


set_input_files() {
	[ -v ffmpeg_input_files_set ] && return 0

	declare -g  ffmpeg_input_files_set

	declare -g  ffmpeg_input_files=( -i "${src[path]}" )

	 # This array will remember the number by order of each input file, so that
	#  later we could retrieve it for “-map” or “-filter_complex”, knowing only
	#  the “nickname” of that input file.
	#
	#  The main source always has index 0, but index 1 may fall either to an
	#  external audio file or to the duplicate “-i” of the source path for
	#  looping the last frame and attaching it to the end of the output file.
	#
	declare -gA ffmpeg_input_files_idx=(
		[source]='0'
	)

	 # External audio goes as a separate input file, subtitles go within the
	#  string passed as a parameter to -filter_complex.
	#
	#  Though external audio would always land id=1, it is better to assume
	#  it’s not known beforehand, and not hardcode it, for the bits of code
	#  may be tossed around in the future, if other input files may appear.
	#
	[ -v src_a[external_file]  ]  && {
		ffmpeg_input_files+=( -i "${src_a[external_file]}" )
		ffmpeg_input_files_idx[ext_audio]="${#ffmpeg_input_files_idx[*]}"
	}

	 # Attempt to add audio delay “-itsoffset” way. Not any better: silence
	#  at the beginning with positive offset, plus the next input file has
	#  to be limited, ot it runs at full length.
	#
	# [ -v audio_delay ]  && {
	# 	local  audio_delay_with_sign="$audio_delay"
	#
	# 	[[ "$audio_delay" =~ ^\- ]]  \
	# 		|| audio_delay_with_sign="+$audio_delay"
	#
	# 	ffmpeg_input_files+=( -itsoffset "$(echo "scale=3; ${start[total_s_ms]} $audio_delay_with_sign" | bc)"
	# 	                      -ss "${start[ts]}"  \
	# 	                      -t "${duration[total_s_ms]}"  \
	# 	                      -i "${src[path]}"
	# 	                    )
	#
	# 	ffmpeg_input_files+=( -itsoffset "$(echo "scale=3; ${start[total_s_ms]} $audio_delay_with_sign" | bc)"
	# 	                      -ss "${ffmpeg_adaptive_seek_ss1[ts]}"
	# 	                      -t "${duration[total_s_ms]}"
	# 	                      -i "${src[path]}"
	# 	                    )
	#
	# 	ffmpeg_input_files_idx[source_double_for_audio_delay]="${#ffmpeg_input_files_idx[*]}"
	# 	extra_options+=('-shortest')
	# }

	 # In the end looping the last frame was implemented without the need
	#  for using source twice.
	#
	# [ -v loop_last_frame ] && {
	# 	ffmpeg_input_files+=( -ss "${stop[ts]}" -i "${src[path]}" )
	# 	ffmpeg_input_files_idx[last_frame]="${#ffmpeg_input_files_idx[*]}"
	# }

	ffmpeg_input_files_set=t
	return 0
}


 # Designate to ffmpeg, which streams should be encoded and put into the
#  target file.
#
#  Two things must be considered:
#  1. In an ordinary case, when streams are either encoded wholly or discarded
#     the “-map” option would operate on input stream specifiers, like “0:V:0”,
#     “0:a:2” etc. When the streams are cut or otherwise processed, these
#     source stream specifiers cannot be used (well, they may, but when things
#     aren’t as complex as they’re here). When you use “-filter_complex”, you
#     probably will end up using custom output specifiers (like “[v4_out]”),
#     and the ending up with customly named outputs. Now if “-map” would speci-
#     fy “source” streams, and customly named outputs will also come from fil-
#     ter processing, when they’ll end up in the muxer, it will just pack
#     /both/ outputs into the target file. So, the “-map” option has be clever
#     and specify either the source streams (when they aren’t processed by a
#     filter) or the processed customly named outputs (when they undergo some
#     filter(s)).
#  2. Do note, that “map_streams()” is called /after/ “assemble_ffmpeg_filter
#     _graph()”. This makes “$filter_chain1” (…chain2 and …chain3 too) avail-
#     able for this function. Whether to go with “source” streams or with
#     customly named processed outputs is decided based upon whcih of these
#     chains are used.
#
map_streams() {
	[ -v ffmpeg_streams_mapped ] && return 0

	declare -g  ffmpeg_map=()
	declare -g  ffmpeg_streams_mapped

	local  map_audio=()
	local  idx
	local  v_out

	info "Mapping streams."

	[ -v audio ] && {
		if [ -v audio_delay ]; then
			map_audio=( -map '[a_out]' )
		else
			if [ -v src_a[external_file]  ]; then
				idx=${ffmpeg_input_files_idx[ext_audio]}
				map_audio=( -map $idx:a:0 )
			else
				map_audio=( -map 0:a:${src_a[track_id]} )
			fi
		fi
	}
	#  Uncomment for testing audio delay the -itsoffset way (see above). Leave
	#  only the “else” block in the ffmpeg_filters array (see 07b_assemble….sh)
	#
	# map_audio=( -map ${ffmpeg_input_files_idx[source_double_for_audio_delay]}:a:0 )

	[ "${filter_chain1:-}${filter_chain2:-}" ]  \
		&& v_out='[v_out]'  \
		|| v_out='0:V'

	ffmpeg_map=( -map "$v_out" "${map_audio[@]}" )
	ffmpeg_streams_mapped=t
	return 0
}


set_audio_options() {
	[ -v ffmpeg_audio_options_set ] && return 0

	declare -g  ffmpeg_audio_options_set
	#  Codec options from the audio profile in the RC file(s).
	#  Declared as global to be available to the encoding module,
	#  in case it would need those.
	declare -g  acodec_options
	#  The set of audio options, that is substituted into ffmpeg command.
	declare -g  audio_opts

	if [ -v audio ]; then
		declare -n acodec_options=${ffmpeg_acodec}_profiles[$acodec_profile]
		audio_opts=( -c:a $ffmpeg_acodec -ac 2 $acodec_options )
	else
		audio_opts=( -an )
	fi

	ffmpeg_audio_options_set=t
	return 0
}


prepare_common_ffmpeg_options() {
	info "Preparing ffmpeg options…"
	milinc

	put_time_before_the_ffmpeg_command

	set_input_files

	assemble_ffmpeg_filter_graph

	map_streams

	set_audio_options

	set_file_name_and_metadata_tags

	[ -v tag_output_colour_space  -o  -v do_colour_space_conversion ]  \
		&& tag_output_with_colour_space

	mildec
	return 0
}


return 0