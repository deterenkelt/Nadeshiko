#  Should be sourced.

#  07c_set_file_name_and_metadata_tags.sh
#  Prepares strings to be used as the output file name and the metadata tags
#  “title” and description. Adds timestamps of the fragment, attempting to
#  present them in concise form. May add other information tags to the file
#  name string, e.g. “[720p]”, when the video was downscaled. Shortens the
#  file name, if it might exceed the length acceptable to common filesystems
#  (255 B). Shortening is done adaptively, seeking for the less important
#  parts of the file name (such as release group tags, content formats, hash-
#  sums) before shortening what appears to be the real file name. Each string
#  end up with certain specifics, according to its purpose.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # Try to shorten the timestamps of the beginning and the end of the fragment
#  in various ways, while it remains reasonable.
#
#  The ways include getting rid of consecutive zeroes in front and from behind,
#  chossing between 2 and 3 digits for milliseconds (depends on the frame rate
#  in the source), and extra fumigating of zeroes, when there’s an evident
#  lack of space. (That last optimisation is turned on only with the related
#  parameter to this function is set.)
#
#  Zero colon (0:…) for minutes will always stay around to make it understand-
#  able, that you’re looking at a timestamp and not some number weirdly attach-
#  ed to the end of the file name.
#
#  [$1] – may be set to “further” to enable the aforementioned mode of saving
#         bytes by removing the non-significant zeroes from minutes and
#         seconds (which are normally not touched).
#
optimise_timestamps_look() {
	#  Timestamps optimised for file name: stripped of their extra zeroes
	#  (only from from the front).
	local  opt_start_ts
	local  opt_stop_ts
	local  further_optimise_ts

	[ "${1:-}" = 'further' ] && further_optimise_ts=t

	#  Timestamp precision determines the number of digits after the dot.
	#  As it goes for the time being, the choice is always between 2 and 3
	#  digits. While 3 digits is the precision coming from mpv, it’s seldom
	#  needed, and 2 digits is enough for regular content. The value is deter-
	#  mined by a formula ties to fps, see below.
	#
	local  ts_prec_digits=3

	#  Timestamp precision threshold is the smallest number of milliseconds,
	#  which is important in making the last digit in the timestamp meaningful.
	#     When we see a timestamp like NN:NN.85, we suspect that it might in
	#  reality be rounded from .847 or .854 or even .859. Add to this, that
	#  a media player may show different timestamps – when the player approxi-
	#  mates them. Restoring the original bounds from the timestamp may thus
	#  have a delta, that should be accounted for. Nadeshiko uses 20 ms as a
	#  precision “step” or border.
	#     But how it is used?
	#     This, of course, depends on how fast frames switch in the video.
	#  When going frame-by-frame in mpv while watching a video of 23.876, 24,
	#  25 or 30 fps, the frames change in 33…43 ms. And as this is larger than
	#  our step, the timestamp may be cut down to two digits after the dot –
	#  the last digit is guaranteed to be true (the previous or the next frame
	#  would differ on 0.03…0.04 ms > 0.02 ms). But for a source video with
	#  60 fps, we would need the third digit, as the frames go in shorter
	#  spans: 16 ms. ±0.01 precision doesn’t satisfy us.
	#     So, this variable is called a threshold, because it determines the
	#  threshold, at which the 2-digit precision becomes insufficient, and
	#  3-digit precision should be used. (See also release notes for v2.24)
	#
	local  ts_prec_threshold_ms=20

	local  bc_result


	#  If the variables necessary for estimating the rational timestamp
	#  precision to be kept, are not defined, use the default .NNN preci-
	#
	#  No cutting of “extra zeroes” here to save space.
	#
	[ -v src_v[frame_rate] ] && {
		#
		#  .999 ms is the largest number of milliseconds that we get
		#  from mpv and that we work with.
		#
		bc_result=$(
			bc <<<"scale=0; 999/${src_v[frame_rate]} > $ts_prec_threshold_ms"
		) || true

		[[ "$bc_result" =~ ^(0|1)$ ]]  \
			&& (( bc_result == 1 ))  \
				&& ts_prec_digits=2

	}

	case $ts_prec_digits in
		2)
			opt_start_ts=${start[ts]%?}
			opt_stop_ts=${stop[ts]%?}
			;;
		3)
			opt_start_ts=${start[ts]}
			opt_stop_ts=${stop[ts]}
			;;
	esac
	#
	#  Try to remove leading zeroes, as in “00:0”, “00:” or at least “0”,
	#  in the timestamp. Mind, that the minutes themselves are /never/
	#  removed: a range like “5–6” would look confusing, so the format
	#  “0:05–0:06” is where the bare minimum is at.
	#
	if [[ "$opt_start_ts" =~ ^00:0 ]]; then
		opt_start_ts=${opt_start_ts#00:0}

	elif [[ "$opt_start_ts" =~ ^00: ]]; then
		opt_start_ts=${opt_start_ts#00:}

	elif [[ "$opt_start_ts" =~ ^0.: ]]; then
		opt_start_ts=${opt_start_ts#0}
	fi
	if [[ "$opt_stop_ts" =~ ^00:0 ]]; then
		opt_stop_ts=${opt_stop_ts#00:0}

	elif [[ "$opt_stop_ts" =~ ^00: ]]; then
		opt_stop_ts=${opt_stop_ts#00:}

	elif [[ "$opt_stop_ts" =~ ^0.: ]]; then
		opt_stop_ts=${opt_stop_ts#0}
	fi

	[ -v further_optimise_ts ] && {
		opt_start_ts=${opt_start_ts%0}
		opt_start_ts=${opt_start_ts%0}
		opt_start_ts=${opt_start_ts%0}
		opt_start_ts=${opt_start_ts%\.}
		opt_start_ts=${opt_start_ts//:0/:}
		opt_stop_ts=${opt_stop_ts%0}
		opt_stop_ts=${opt_stop_ts%0}
		opt_stop_ts=${opt_stop_ts%0}
		opt_stop_ts=${opt_stop_ts%\.}
		opt_stop_ts=${opt_stop_ts//:0/:}
	}

	[ -v further_optimise_ts ]  \
		&& echo -n "$opt_start_ts-$opt_stop_ts"  \
		|| echo -n "$opt_start_ts–$opt_stop_ts"  #  max. 19 bytes

	return 0
}


assemble_tag_string() {
	local  tags
	local  scale_type_tag

	[ -v scale ] && {
		#  FS – forced scale, AS – automatic scale
		#  (including the scale= from .rc.sh).
		[ -v forced_scale ]  \
			&& scale_type_tag="FS"   \
			|| scale_type_tag="AS"
		tags="[${scale}p][$scale_type_tag]"  # max. 7+4 bytes
	}
	[ -v loop_last_frame ] && {
		#
		#  Subtraction of the extra added second. There are two reasons why it
		#  should be like this:  1) this second is more of a cosmetic change,
		#  and  2) to maintain consistency between the input and the output
		#  if input is 5 seconds, the output should reflect “5”. The addition
		#  is an internal thing.
		#
		[ -v loop_last_frame_compensatory_second ]  \
			&& llf_s=$(( loop_last_frame_s -1 ))  \
			|| llf_s=$loop_last_frame_s
		tags+="[LFL-$llf_s]"  # max. 12 bytes
	}

	echo -n "${tags:-}"  #  max. 23 bytes, including the space
	                     #  in front
	return 0
}


 # As it is allowed for the encoded video to not have timestamps – when the
#  source is encoded from the beginning to the end – this bears a potential
#  danger. In the earlier versions, when timestamps would alaways be added,
#  the outgoing file would always have a different name from the source,
#  that it was encoded from. But if neither timestamps, nor tags (denoting
#  scaling or something else), not filename prefix would be present, these
#  names may match, and we need to prevent accidentally overwriting the
#  source.
#
#  In order to guarantee, that Nadeshiko won’t overwrite the source, the
#  encoded file would be given a suffix to the extension, when the files
#  would otherwise have the same name. So “My_cat.webm” would become
#  “My_cat-1.webm”. If the file “My_cat-1.webm” would already exist, Na-
#  deshiko will read the suffix and increment it, until an appropriate
#  file name is found, which would allow to create a new file, without
#  overwriting anything. While anything above -1 cannot relate to the
#  current encoding process, to try higher numbers is a safe measure to
#  not overwrite other files. Those may be
#    - pieces that might be encoded by the user and named so,
#    - or such “unluckily named” neighbouring files, that are
#      continuation of the source video (its next parts).
#
#  The “-number” format was chosen in favour of “.number” because the dot
#  would be confusing, if it would happen to be adjacent to a timestamp.
#  (Yes, while we speak mostly about encoding /entire/ video, the source
#  video may itself be a cut and bear timestamps.)
#
#  It should be stressed, that Nadeshiko protects only the source file and
#  other files uninvolved in the (current) encoding process, when it encodes
#  the source video from the beginning to the end. That is, when Nadeshiko
#  doesn’t add timestamps of its own to the file name. The encodes that are
#  /fragments/ are always overwritten. The general thought is that more often
#  than not the user wouldn’t like to keep an old version: if it exists, it’s
#  probably a broken encode (ffmpeg ended with an error), or an encode discard-
#  ed by user (wrongly cropped, had a timstamp mistake, the user forgot to
#  enable/disable the audio track) – in most cases the fragment that is encoded
#  the second time is encoded to simply redo it.
#
set_suffix() {
	local  samefile_check_passed
	local  new_file_name
	local  suffix

	until [ -v samefile_check_passed ]; do
		new_file_name="${fname_pfx:+$fname_pfx }"
		new_file_name+="$fname"
		new_file_name+="${tags:+ $tags}"
		# ← NB: no timestamps here.
		new_file_name+="${suffix:+-$suffix}"
		new_file_name+=".$container"

		if	[ "$where_to_place_new_file/$new_file_name" -ef "${src[path]}" ]  \
			|| [ -e "$where_to_place_new_file/$new_file_name" ]
		then
			if [[ "$new_file_name" =~ -($INT)\.$container$ ]]; then
				suffix=$(( BASH_REMATCH[1] + 1))
				continue
			else
				suffix=1
				continue
			fi
		fi
		samefile_check_passed=t
	done

	[ "${suffix:-}" ]  \
		&& warn "The file to be created already exists! Which means that the file
		         name probably matches the source itself. So, to avoid overwriting
		         it, a suffix (…${__bri}-$suffix${__s}.$container) will be added to the name."

	echo -n "${suffix:-}"
	return 0
}


 # Shorten a string to the specified length in bytes, shaving away unnecessary
#  parts (only when processing $fname) and, if it’s sufficient (which means
#  all other cases), shave off character by character.
#
#  $1 – the string to shorten
#  $2 – the size limit, in bytes
# [$3] – can take value “strip tags”, if attempting to get rid of media format
#      tags and release group name should be done before cuting character by
#      character. Only employed for output file name ($fname), not employed
#      to shorten metadata tags.
#
#  Returns the shortened string to stdout.
#
squeeze_string_to_size() {
	local  str="$1"
	local  str_len_B
	local  str_tail
	local  str_tail_len_B
	local  str_tail_chars=7  # how much characters long should the tail be
	local  str_size_limit_B="$2"
	local  strip_tags

	[ "${3:-}" = 'strip_tags' ] && strip_tags=t

	 # (non-fname related paths) If the string is already fitting to the
	#  specified size, return it as is.
	#
	str_len_B=$( echo -n "$str" | wc --bytes )
	(( str_len_B <= str_size_limit_B )) && {
		echo -n "$str"
		return 0
	}

	 # (fname path) Trying to be smart: strip one tag in square brackets “[…]”
	#  from the beginning and the end of $str. It’s probably something like
	#  [Release GroupName] and [1080pHEVCFLAC].
	#
	[ -v strip_tags ] && {
		[[ "$str" =~ ^\[[^\]]+\][\ _]?([^\ _].+)$ ]] && {
			str="${BASH_REMATCH[1]}"
			str_len_B=$( echo -n "$str" | wc --bytes )
		}

		(( str_len_B <= str_size_limit_B )) && {
			echo -n "$str"
			return 0
		}

		[[ "$str" =~ ^(.+[^\ _])[\ _]*\[[^\]]+\]$ ]] && {
			str="${BASH_REMATCH[1]}"
			str_len_B=$( echo -n "$str" | wc --bytes )
		}

		(( str_len_B <= str_size_limit_B )) && {
			echo -n "$str"
			return 0
		}
	}

	 # If nothing helped, generic shortening. Save seven characters from the
	#  end and cut away from the end after that.
	#
	(( str_len_B < 11 ))  \
		&& err "The string “$str” must be at least 11 characters long to apply shortening."

	str_tail="${str: -$str_tail_chars: $str_tail_chars}"
	str_tail_len_B=$( echo -n "$str_tail" | wc --bytes )
	str="${str:: -1 -$str_tail_chars}"
	until (( str_len_B <= (   str_size_limit_B
		                    - 3
		                    - str_tail_len_B
		                  )
		  ))
	do
		str=${str%?}
		str_len_B=$( echo -n "$str" | wc --bytes )
	done

	echo -n "$str…$str_tail"

	return 0
}


 # Composes the strings, which are used to name the output file, to be placed
#  in the metadata “title” tag and in the “description” metadata tag.
#
#    In                                Out
#     fname_pfx                          new_file_name
#     src[path]                          outp_mdata_title
#     src_c[title]                       outp_mdata_comment
#     start[ts]                          outp_mdata_description
#     stop[ts]
#     scale
#     forced_scale
#
#  new_file_name
#      Will keep the name to be given to the output file. Will have $fname_pfx
#      (if set), tags descibing autoscale and forced scale (in case the source
#      underwent downscale) and the timestamps. The latter may be absent, how-
#      ever, if the source video was encoded entirely.
#
#  outp_mdata_title
#      Will hold the original title, if one was present. If the source video
#      was encoded not entirely, but only a fragment of it, the output title
#      will prepend the original string with “(A fragment of) ”. The output
#      title Will inlcude $fname_pfx (if specified by user) only when the con-
#      fig option “include_fname_pfx_in_output_title” is set. And it will never
#      include timestamps.
#
#  outp_mdata_comment
#      The string has format “Converted with Nadeshiko vVERSION / TIMESTAMPS
#      [SCALING_TAG][LAST_FRAME_LOOPED_TAG]”, where the part beginning with a
#      slash may be absent or be presented partly, depending on how the frag-
#      ment was encoded. If any of the three parts, that go after the slash,
#      would exist, the slash will be placed, if none would – it also won’t be
#      placed. The “comment” metadata tag is the place where the timestamps
#      of the fragment are stored instead of taking place in the “title” or in
#      the “description” tag.
#
#  outp_mdata_description
#      This metadata tag stores a copy of the source file name. As the “$new_
#      file_name” may get an abridged copy, with tags added, it may be hard
#      to restore, from which source this was made. On occasion that the output
#      file would be renamed (e.g. after uploading and saving it back), without
#      this metadata it won’t be possible to get the idea, which type of source
#      was downloaded (they may be different). The whole idea to keep the ori-
#      ginal name is to be able to retrieve exactly that source again, if it
#      was deleted from computer. That is, for a regular user – to not spend
#      extra time on comparison, and for the developer – to be able to run en-
#      code with the newest version of Nadeshiko to see how it compares with
#      the one made by an earlier version.
#
set_file_name_and_metadata_tags() {
	declare -g  new_file_name=''
	declare -g  outp_mdata_title=''
	declare -g  outp_mdata_description=''
	declare -g  outp_mdata_comment=''

	local -A fname_length=()
	local    free_chars_B
	local    free_chars_for_fname_B

	local  fname_pfx="${new_filename_user_prefix:-}"

	local  fname
	local  fname_tail
	local  fname_max_length_B=255  # Assuming an ext* filesystem and UTF-8
	local  md_title_max_length_B=255
	local  md_descr_max_length_B=255
	local  length_of_fname_tail_B
	local  real_fname_length_B
	local  shortened_fname_length_B

	local  new_file_name_length_B

	local  timestamps
	local  timestamps_optimised
	local  timestamps_optimised_further

	local  tags

	local  suffix

	info "Setting output file name and metadata tags."

	fname="${src[path]%.*}"
	fname=${fname##*/}
	#
	#  The file name may contain multibyte characters (for example, “Ж” and “ö”
	#  take two bytes, “–” (en dash) and “笑” take 3 bytes). So, if the file
	#  name would come out too long (after we add the user-defined prefix,
	#  timestamps and tags), we’ll have to trim the file name by one character,
	#  until it fits in the limit, but after each trimming we’ll be checking,
	#  how much bytes the string did actually lose.
	#
	#  It’s essential to count bytes in this form (i.e. “echo -n "$str" | …”)
	#  to get the correct amount. For “echo "$str" | …” and “…<<<"str"” would
	#  add a newline character at the end at the end of the string.
	#
	fname_length+=(
		[source]=$( echo -n "$fname" | wc --bytes )  #  never used, but left
		                                             #  to draw attention to
		                                             #  the commentary above.
	)

	if [ "$fname_pfx" ]; then
		fname_length+=(
			[prefix]=$( echo -n "$fname_pfx" | wc --bytes )
		)
	else
		fname_pfx=''
		fname_length+=(	[prefix]=0 )
	fi

	if [ -v encoding_entire_file ]; then
		timestamps=''
		fname_length+=( [timestamps]=0 )
		suffix="$(set_suffix)"
	else
		timestamps_optimised="$(optimise_timestamps_look)"
		local -n timestamps='timestamps_optimised'
		fname_length+=(
			[timestamps]=$( echo -n " $timestamps" | wc --bytes )
		)
		suffix=''
	fi

	tags="$(assemble_tag_string)"
	#
	#  The length of the tags string is of no intereset, for if tags are used,
	#  then they fit (yes), and if tags can’t fit, they won’t be used.
	#
	# [ "${tags:-}" ]  \
	# 	&& fname_length+=( [tags]="$( echo -n "$tags" | wc --bytes)" )  \
	# 	|| fname_length+=( [tags]=0 )

	new_file_name="${fname_pfx:+$fname_pfx }"
	new_file_name+="$fname"
	new_file_name+="${timestamps:+ $timestamps}"
	new_file_name+="${tags:+ $tags}"
	new_file_name+="${suffix:+-$suffix}"
	new_file_name_full="$new_file_name"
	new_file_name+=".$container"

	new_file_name_length_B=$( echo -n "$new_file_name" | wc --bytes)

	(( new_file_name_length_B > fname_max_length_B )) && {
		#
		#  First, check, which part is causing the problem: if the prefix is
		#  too long, trim it, if it’s the the original file name, then trim it.
		#  Between prefix and the original name give whole preference to the
		#  user-defined prefix – down to discarding the original file name com-
		#  pletely – as people put the prefix for it to stay. What’s left for
		#  us is to hope that, that it’s meaningful and is not something like
		#  “a”, “sdfsgsd” or “123”.
		#
		#  There also has to be a check on whether the newly compound name
		#  is unique, so it won’t overwrite some other file, that just happe-
		#  ned to have a name that was automatically reduced to the same name.
		#  (With timestamps the chance for that is slim, but still exists.)
		#
		#  Mind, that normally (without name shortening), it is the default
		#  behaviour to overwrite the output file – as with full names we can
		#  guarantee, that overwriting happens only when it’s intended. Auto-
		#  matic name shortening, on the other hand, makes the matter compli-
		#  cated and takes away the right to not care about overwriting, as
		#  accident matches may overwrite something, that shouldn’t be rep-
		#  laced. Thus, if the situation goes down to an overwrite, it should
		#  be prevented and three bytes taken away from the end to create a
		#  hexadecimal prefix: “.XX”, where XX is a hex number.
		#
		unset new_file_name  # clear it before assembling anew.

		[ "${timestamps:-}" ] && {
			timestamps_optimised_further="$(optimise_timestamps_look 'further')"
			local -n timestamps='timestamps_optimised_further'
			fname_length[timestamps]=$( echo -n " $timestamps" | wc --bytes )
		}

		#  Start fitting our strings, in order of importance:
		#    1) the file’s suffix and extension, separated with a dot
		#    2) $fname_pfx
		#    3) $timestamps and $fname (adapted from $src[path])
		#
		free_chars_B=$(( fname_max_length_B - ${#suffix} - ${#container} -1 ))

		if (( fname_length[prefix] > free_chars_B )); then
			until (( fname_length[prefix] <= free_chars_B )); do
				fname_pfx=${fname_pfx%?}
				fname_length[prefix]=$( echo -n "$fname_pfx " | wc --bytes )
			done
			free_chars_B=0

		else
			free_chars_B=$(( free_chars_B - fname_length[prefix] ))
		fi
		new_file_name="$fname_pfx"

		#  Ok, we have fitted $fname_pfx. Now to look, if we have space
		#  for the original file name (shortable), timestamps (not shortable)
		#  and tags (not shortable).
		#
		#  The distribution is as follows:
		#    ⋅ 1 space that goes after fname_pfx,
		#    ⋅ a minimum of:
		#        - 9 bytes for maximally shortened front part of $fname
		#          (in order for it to have at least three characters in the
		#          worst case);
		#        - 3 bytes for the “…” indicator of shortening;
		#        - enough bytes for 7 last characters of <fname>, which are
		#          preserved.
		#    ⋅ 1 space before timestamps (if timestamps are to be added at all)
		#    ⋅ length of timestamps
		#
		#  Tags are thrown out from the file name, they’ll still be in the
		#  title, as written in the metadata.
		#
		#  Imbuing the separation space needed for timestamps and tags into
		#  the requested string length, for ease of calculating it:
		#
		let 'fname_length[timestamps] > 0 ? fname_length[timestamps]++ : 0, 1'
		#
		#  Here we save seven last characters to fname_tail, but they may require
		#  7…21 bytes. So we have to consider the real length (in bytes) of the
		#  tail, when we shorten the front part.
		#
		fname_tail="${fname: -7: 7}"
		length_of_fname_tail_B=$(echo -n "$fname_tail" | wc --bytes)
		#
		if	(( free_chars_B >= (   ( fname_length[prefix] == 0  ? 0  : 1 )
			                     + 9+3+length_of_fname_tail_B
			                     + fname_length[timestamps]
			                   )
			))
		then
			#  Ok, we have space for $fname and $timestamps here. Now to
			#  count, how much space we have for $fname. What’s the byte
			#  limit for $fname?
			free_chars_for_fname_B=$((   free_chars_B
			                           - ( fname_length[prefix] == 0  ? 0  : 1 )
			                           - fname_length[timestamps]  ))

			fname=$(squeeze_string_to_size "$fname"  \
			                               "$free_chars_for_fname_B"  \
			                               'strip_tags'  )

			# padding from fname_pfx
			new_file_name="${new_file_name:+$new_file_name }"
			new_file_name+="$fname"
			new_file_name+="${timestamps:+ $timestamps}"
		fi
		new_file_name+="${suffix:-}.$container"

		real_fname_length_B=$( echo -n "$new_file_name_full.$container" | wc --bytes )
		shortened_fname_length_B=$( echo -n "$new_file_name" | wc --bytes )
		warn "File name is too long and will have to be shortened!
		      ⋅ the original, full file name would take $real_fname_length_B B
		      ⋅ shortened file name would take only $shortened_fname_length_B B"
	}

	new_file_name="${where_to_place_new_file%%+(\/)}/$new_file_name"

	[ -v create_windows_friendly_filenames ]  \
		&& new_file_name="$(replace_windows_unfriendly_chars "$new_file_name")"


	                       #  Metadata strings  #

	 # After we’ve assembled “$new_file_name”, $fname no longer is needed in
	#  original or shortened form, so we peruse this variable again to assemble
	#  names, suitable for metadata, from scratch with it.
	#

	 # An idea for the future: draw title from DVD/BD metadata, when a chunk
	#  of video is played.
	#
	[ ! -v encoding_entire_file ]  \
	&&	(
			[ "${fname_pfx:-}"  -a  ! -v include_fname_pfx_to_mdata_title ]  \
			|| [ -z "${fname_pfx:-}" ]  \
		)\
		&& outp_mdata_title="(A fragment of) "

	if [ "${src_c[title]:-}" ]; then
		outp_mdata_title="${outp_mdata_title:-}"
		[ -v include_fname_pfx_to_mdata_title ]  \
			&& outp_mdata_title+="$fname_pfx "
		outp_mdata_title+="${src_c[title]}"
		outp_mdata_title=$(
			squeeze_string_to_size "$outp_mdata_title"  \
			                       "$md_title_max_length_B"
		)
	else
		outp_mdata_title="${outp_mdata_title:-}"
		[ -v include_fname_pfx_to_mdata_title ]  \
			&& outp_mdata_title+="$fname_pfx "
		fname="${src[path]%.*}"
		fname=${fname##*/}
		outp_mdata_title+="$fname"
		outp_mdata_title=$(
			squeeze_string_to_size "$outp_mdata_title"  \
			                       "$md_title_max_length_B"
		)
	fi

	fname="${src[path]%.*}"
	fname=${fname##*/}
	outp_mdata_description=$( squeeze_string_to_size "$fname"  \
	                                                 "$md_descr_max_length_B" )

	outp_mdata_comment="Converted with Nadeshiko v$version"
	[ "${timestamps:-}" -o "${tags:-}" ] && {
		outp_mdata_comment+=' / '
		outp_mdata_comment+="[${timestamps:+$timestamps_optimised}]"
		outp_mdata_comment+="${tags:+ $tags}"
	}

	return 0
}


return 0