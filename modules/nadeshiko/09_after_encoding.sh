#  Should be sourced.

#  09_after_encoding.sh
#  Nadeshiko module that holds functions of various purposes for the stage
#  after video fragment was encoded (when the program may finish or do
#  a re-run).
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # File sizes are (typically) disaplayed in bytes. But for all values related
#  to bit rates or derived from them, such as the ESP unit and the muxing over-
#  head, they are calculated and shown in (kilo)bits, not bytes. (Technically,
#  even the space is calculated in bits, but that happens deep in the code of
#  05_fit_bitrate_to_filesize.sh, whose hard work is summarised in the line
#  that displays the appropriate bit rates, that were found.) So, unless you
#  see a unit referring to bytes: “B”, “kB”, “MiB”, “GiB” or the word “bytes”,
#  you should assume, that ‘k’ means kilobits.
#
#  When counting bits, the suffixed values always use power of 10, thus
#  1 kilobit = 1000 bits. The “si” (or “k=1000”) option is not connected to
#  calculating in bits in any way.
#


print_stats() {
	local  stats
	local  pass1_s
	local  pass1_hms
	local  pass2_s
	local  pass2_hms
	local  pass1_and_pass2_s
	local  pass1_and_pass2_hms

	read -d '' pass1_s pass2_s < <( cat "$LOGDIR/$LOGNAME.time_output"; echo -e '\0' )
	pass1_s=${pass1_s%.*}  pass2_s=${pass2_s%.*}
	[[ "$pass1_s" =~ ^[0-9]+$ && "$pass2_s" =~ ^[0-9]+$ ]] || {
		warn "Couldn’t retrieve time spent on the 1st or 2nd pass."
		return 0
	}
	pass1_and_pass2_s=$((  pass1_s + pass2_s  ))
	new_time_array  pass1_time  $pass1_s
	new_time_array  pass2_time  $pass2_s
	new_time_array  pass1_and_pass2_time  $pass1_and_pass2_s
	speed_ratio=$(echo "scale=2; $pass1_and_pass2_s/${duration[total_s_ceil]}" | bc)
	speed_ratio="${__bri}${__y}$speed_ratio${__s}"
	info "Stats:
	      Pass 1 – ${pass1_time[ts_no_ms]}.
	      Pass 2 – ${pass2_time[ts_no_ms]}.
	      Total – ${pass1_and_pass2_time[ts_no_ms]}.
	      Encoding took $speed_ratio× time of the slice duration."
	return 0
}


on_size_overshoot() {
	declare -g  muxing_overhead_antiovershoot
	declare -g  new_file_size_B
	declare -g  max_size_B
	declare -g  esp_unit
	declare -g  overshot_times
	declare -g  old_vbitrate
	declare -g  critical_overshoot

	local  filesize_overshoot
	local  filesize_overshoot_B
	local  filesize_overshoot_in_esp

	local -n ovh_coef=${container}_muxing_ovh_coef

	filesize_overshoot=$(( ( new_file_size_B - max_size_B ) * 8 ))

	filesize_overshoot_B=$(( new_file_size_B - max_size_B ))

	filesize_overshoot_in_esp=$(( filesize_overshoot / esp_unit ))

	warn-ns "${__bri}${__y}Maximum file size was overshot on $(pretty $filesize_overshoot)bit (≈$filesize_overshoot_in_esp ESP)${__s}."
	milinc
	msg "Total size in bytes: $new_file_size_B."

	[ -v ${ffmpeg_vcodec//-/_}_critical_overhead_pct ] && {
		declare -n crit_ovh_pct=${ffmpeg_vcodec//-/_}_critical_overhead_pct
		(( filesize_overshoot_B > max_size_B * crit_ovh_pct / 100 ))  \
			&& critical_overshoot=t
	}

	if [ -v critical_overshoot ]; then
		err "Critically overshot the size limit (>$crit_ovh_pct%)."
		#
		#  Not deleting the file to leave the user with at least something
		#  for the time spent. Who knows, maybe he decides to keep the file
		#  anyway.
		#

	else
		#  Increase the reserved space on 3/4 of the overhead, if it’s greater
		#  than 1 ESP, or on 1 ESP, if it’s lower than that.
		#
		: ${filesize_antiovershoot:=0}
		filesize_overshoot=$(
			echo "scale=2; fso = $filesize_overshoot * $ovh_coef;  \
			      scale=0; (fso+0.5)/1"  \
			| bc
		)
		(( filesize_overshoot > esp_unit ))  \
			&& let "filesize_antiovershoot +=  filesize_overshoot,  1"  \
			|| let "filesize_antiovershoot +=  esp_unit,  1"

		msg "Increasing space, reserved for container on $(pretty $filesize_antiovershoot)bit (coef. = $ovh_coef)."

		#  Deleting the file. As the name of the output file may change due
		#  to scaling tags added, we shouldn’t rely solely on ffmpeg rewriting
		#  the file.
		#
		rm "$new_file_name"
	fi

	: ${overshot_times:=0}
	let "++overshot_times, 1"
	old_vbitrate="$vbitrate"  #  for the adjustment of “headpat”.
	mildec
	return 0
}


post_encode_validity_checks() {
	declare -g  loop_last_frame_tries_complete
	declare -g  loop_last_frame_use_alter_method
	declare -g  setpts_reserved_space_frames
	declare -g  previous_run_had_overshot_size

	local  all_correct=t
	local  new_file_size_B
	local  encoded_duration
	local  encoded_duration_retrieved
	#
	#  In the check for looped last frame this value determines, how much the
	#  encoded duration may differ from that which was set. It is expected,
	#  that the encoder would not fit the frame data precisely 1 to 1, so a
	#  discrepancy within 1 frame is deemed unavoidable.
	local  expected_duration_delta_ms=$(
		echo "scale=4; d=1/$frame_rate*1000; scale=0; d/1" | bc
	)

	new_file_size_B=$(stat --printf %s "$new_file_name")
	(( new_file_size_B <= max_size_B ))  || {
		on_size_overshoot
		#  To know when to redo certain parts of ffmpeg command assemblage
		previous_run_had_overshot_size=t
		unset  all_correct
		#  Getting to the size limit is the thing, that we must guarantee
		#  first: other checks should be performed only when it’s already
		#  known, that the size fits. (Continuing and running other checks
		#  now may even be harmful, as they may set coefficients that would
		#  lower the output quality, while this might now be needed, if the
		#  size adjustments will be applied.)
		return 1
	}
	unset  previous_run_had_overshot_size

	[ -v loop_last_frame ] && {
		encoded_duration=$(
			$ffprobe -hide_banner -v error  \
			         -show_format  \
			         -of default=noprint_wrappers=1  \
			         "$new_file_name"  \
			| sed -rn 's/^duration=(.*)$/\1/p'
		)
		if [[ "$encoded_duration" =~ ^([0-9]+)((\.[0-9]{1,3})[0-9]*|)$ ]]; then
			encoded_duration=${BASH_REMATCH[1]}${BASH_REMATCH[3]}
			encoded_duration_retrieved=t

		else
			encoded_duration=$(
				$ffprobe -hide_banner -v error  \
				         -show_streams  \
				         -select_streams V  \
				         -of default=noprint_wrappers=1  \
				         "$new_file_name"  \
				| sed -rn 's/^duration=(.*)$/\1/p'
			)

			[[ "$encoded_duration" =~ ^([0-9]+)((\.[0-9]{1,3})[0-9]*|)$ ]]  && {
				encoded_duration=${BASH_REMATCH[1]}${BASH_REMATCH[3]}
				encoded_duration_retrieved=t
			}
		fi

		if [ -v encoded_duration_retrieved ]; then
			#  Considering, that we are more likely to encode to a higher frame
			#  rate (preserving 60 fps or more), than to a lower one (like 10
			#  or 1 fps), the length of 1 delta frame would only decrease.
			#  Which means that the difference between the “$loop_last_frame_s”
			#  and the “$expected…delta…” would only be greater, and therefore,
			#  it’s highly unlikely that “$expected…delta…” would get any close
			#  to the duration, that the last frame is looped, confusing us
			#  about whether the looped fragment was encoded and took part in
			#  the (exteneded) duration or our value is within the discrepancy
			#  margin. tl;dr we don’t need the “<” counterpart in the expres-
			#  sion below.
			#
			if ! (( ${encoded_duration/\./} >    ${duration[total_s]}*1000
			                                   + $loop_last_frame_s*1000
			                                   + 10#${duration[ms]}
			                                   - $expected_duration_delta_ms  ))
			then
				warn "The duration of the encoded file ($encoded_duration s) seems too low."
				if [ -v loop_last_frame_use_alter_method ]; then
					let "loop_last_frame_tries_complete++, 1"
					warn "Looping the last frame: attempt ${__bri}$loop_last_frame_tries_complete${__s}/$loop_last_frame_max_attempts, was reserving ${__bri}$setpts_reserved_space_fr${__s} frames."
					(( loop_last_frame_tries_complete == loop_last_frame_max_attempts ))  \
						&& err "Encoding with last frame looped: no more attempts."
				else
					loop_last_frame_use_alter_method=t
					info "It seems that the primary method for looping the last frame
					      didn’t work. Switching to an alternative set of filters."
				fi

				unset all_correct
			fi

		else
			warn 'The encoded file had no metadata to verify its duration
			      and that the encoding of the looped last frame succeeded.'
		fi
	}

	[ -v all_correct ]  && return 0  || return 1
}


post_successful_encode() {
	declare -g  new_file_name

	# new_file_name=${new_file_name//\$/\\\$}
	echo "$__mi  $new_file_name"
	[ -v time_stat ] && print_stats
	# [ -v pedantic ] && comme_il_faut_check "$new_file_name"  # needs update
	return 0
}



return 0