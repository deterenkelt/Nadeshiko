#  Should be sourced.

#  02_parse_commandline_arguments.sh
#  Nadeshiko module to read arguments passed via command line and assign
#  them to variables.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


 # Assigns start time, stop time, source video file
#  and other stuff from command line parameters.
#
parse_args() {
	declare -gA  src=()
	declare -gA  src_c=()
	declare -gA  src_v=()
	declare -gA  src_a=()
	declare -gA  src_s=()
	declare -g   subs
	declare -g   subs_explicitly_requested
	declare -g   audio
	declare -g   audio_explicitly_requested
	declare -g   sub_delay
	declare -g   sub_scale
	declare -g   audio_delay
	declare -g   audio_delay_is_negative
	declare -g   deinterlace
	declare -g   kilo
	declare -g   scale
	declare -g   crop
	declare -g   where_to_place_new_file="$PWD"
	declare -g   new_filename_user_prefix
	declare -g   max_size
	declare -g   vbitrate
	declare -g   scene_complexity
	declare -g   disable_atts_cache
	declare -g   dryrun
	declare -g   loop_last_frame
	declare -g   loop_last_frame_s
	declare -g   seekbefore
	declare -g   seekbefore_due_to_request_from_cmdline
	declare -g   seekbefore_due_to_tr_or_pr_stream
	declare -g   seekbefore_due_to_audio_delay
	declare -g   seekbefore_s
	declare -g   do_not_report_ffmpeg_progress_to_console

	local  arg
	local  pid
	local  ch_name
	local  _pfx_len
	local  help_func

	for arg in "${ARGS[@]}"; do
		if [[ "$arg" =~ ^(-h|--help)$ ]]; then
			show_help
			exit 0

		elif [[ "$arg" =~ ^(-v|--version)$ ]]; then
			#  Name and version are already printed.
			show_licence
			exit 0

		elif is_valid_timestamp "$arg"; then
			if [ ! -v time1  -a  ! -v time2 ]; then
				new_time_array  time1  "$arg"  \
					|| err 'Couldn’t set Time1'

			elif [ -v time1  -a  ! -v time2 ]; then
				new_time_array  time2  "$arg"  \
					|| err 'Couldn’t set Time2.'

			else
				err 'Cannot work with more than 2 timestamps.'
			fi

		elif [[ "$arg" =~ ^(no|)subs?((=|:)(.+)|)$ ]]; then
		#             external file ---^ ^---internal subtitle(!) track ID
			case "${BASH_REMATCH[1]}" in
				no)
					unset subs
					;;
				'')
					subs=t
					#  Remember, that subtitles were explicitly requested.
					#  This is needed to treat differently two cases:
					#  - “subs” are turned on by default. If a video would
					#    have no subs, Nadeshiko should be quiet and encode
					#    without subtitles.
					#  - “subs((=|:)(.+)|)” were set explicitly. In this case
					#    the lack of subtitles to render is an error.
					subs_explicitly_requested=t
					case "${BASH_REMATCH[3]}" in
						'=')
							#  Have to use realpath for the files in the same
							#  directory, or the symlink to it in TMPDIR
							#  would also refer to a file as if it’s local.
							src_s[external_file]="$(realpath "${BASH_REMATCH[4]}")"
							[ -r "${src_s[external_file]}" ]  \
								|| err "External subtitles not found.
								        No such file: “${src_s[external_file]}”"
							;;
						':')
							src_s[track_id]="${BASH_REMATCH[4]}"
							#  A more thorough check is done after gathering
							#  information about the source file.
							[[ "${src_s[track_id]}" =~ ^$INT$ ]]  \
								|| err "Wrong subtitle track ID.
								        Subtitle track ID must be a number, but it is set to “${src_s[track_id]}”."
							;;
					esac
					;;
			esac

		elif [[ "$arg" =~ ^(no|)audio((=|:)(.+)|)$ ]]; then
			case "${BASH_REMATCH[1]}" in
				no) unset audio
					;;
				'')
					audio=t
					#  Remembering it for the same reason as with “subs”.
					audio_explicitly_requested=t
					case "${BASH_REMATCH[3]}" in
						'=')
							#  Realpath for the same reason as with subs ↑.
							src_a[external_file]="$(realpath "${BASH_REMATCH[4]}")"
							[ -r "${src_a[external_file]}" ]  \
								|| err "External audio file not found.
								        No such file (or file is not readable): “${src_a[external_file]}”."
							;;
						':')
							src_a[track_id]="${BASH_REMATCH[4]}"
							#  A more thorough check is done after gathering
							#  information about the source file.
							[[ "${src_a[track_id]}" =~ ^$INT$ ]]  \
								|| err "Wrong audio track ID.
								        Audio track ID must be a number, but it is set to “${src_a[track_id]}”."
							;;
					esac
					;;
			esac

		elif [[ "$arg" =~ ^sub_delay=(\-?([0-9]|[1-9][0-9]{1,5})(\.[0-9]{1,3}|))$ ]]; then
			sub_delay="${BASH_REMATCH[1]}"

		elif [[ "$arg" =~ ^sub_scale=(([0-9]|1[0-9]*)(\.[0-9]{1,2}|))$ ]]; then
			sub_scale="${BASH_REMATCH[1]}"

		elif [[ "$arg" =~ ^audio_delay=(\-?([0-9]|[1-9][0-9]{1,5})(\.[0-9]{1,3}|))$ ]]; then
			audio_delay="${BASH_REMATCH[1]}"
			#
			#  Technically, we need “seekbefore” only for positive delays. (As
			#  in this case we wish to take the audio, that was already played,
			#  and put it sometime later, with a delay, thus “take audio from
			#  the past”. Which effectively means, that to grab something from
			#  the past, we need the decoder to start earlier.) But the “seek-
			#  before” mode is activated for both cases, because
			#    - differentiating between “seekbefore-enabled” and “non-seek-
			#        before” cases within the code, that composes audio delay
			#        part of the ffmpeg filter graph, would complicate what
			#        is already complicated. And as that part is not polished
			#        yet to perfection (this is the first release and some bugs
			#        may appear), it is too early to make things more complex
			#        than they are;
			#    - uses of this option with a /negative/ delay are presumed
			#        to be rare;
			#    - the overhead of running a decoder early is not that costly
			#        now in comparison to fulldecode, and deltas within ±30 s
			#        are negligible.
			#
			[ ${audio_delay:0:1} = '-' ] && audio_delay_is_negative=t
			#
			#  Assigning seekbefore to the audio delay seconds plus two: one
			#  second is to compensate for the dropped decimal component, and
			#  other second is to provide a sort of buffer to guaranteely have
			#  something before the time, where audio should start (to be cut).
			#
			seekbefore=t
			seekbefore_due_to_audio_delay=t
			seekbefore_s=$(( ${BASH_REMATCH[2]} + 2 ))

		elif [[ "$arg" =~ ^(si|kilo=1000|k=1000)$ ]]; then
			kilo=1000

		elif [[ "$arg" =~ ^scale=($(IFS='|'; echo "${known_res_list[*]}"))p?$  \
		        || "$arg" =~ ^($(IFS='|'; echo "${known_res_list[*]}"))p$ ]]
		then
			scale="${BASH_REMATCH[1]}"

		elif [[ "$arg" =~ ^(size=|)(tiny|small|normal|default|unlimited)$ ]]; then
			declare -p max_size &>/dev/null  || {
				#  max_size is declared once as either a reflink or a varaible,
				#  and cannot be re-declared, if the option would be specified
				#  more than once.
				err "The argument for “size” option was already specified."
			}
			declare -gn max_size="max_size_${BASH_REMATCH[2]}"

		elif [[ "$arg" =~ ^size=(${INT}[kMG])$ ]]; then
			declare -p max_size &>/dev/null || {
				#  max_size is declared once as either a reflink or a varaible,
				#  and cannot be re-declared, if the option would be specified
				#  more than once.
				err "The argument for “size” option was already specified."
			}
			max_size="${BASH_REMATCH[1]}"

		elif [[ "$arg" =~ ^vb=(${INT}[kMG])$ ]]; then
			vbitrate="${BASH_REMATCH[1]}"

		elif [[ "$arg" =~ ^(crop=|)([0-9]+)(:|x)([0-9]+)(:|\+)([0-9]+)(:|\+)([0-9]+)$ ]]; then
			crop_w=${BASH_REMATCH[2]}
			crop_h=${BASH_REMATCH[4]}
			crop_x=${BASH_REMATCH[6]}
			crop_y=${BASH_REMATCH[8]}
			crop="crop=$((crop_w/2*2)):"
			crop+="$((crop_h/2*2)):"
			crop+="$crop_x:"
			crop+="$crop_y"

		elif [ -f "$arg" ]; then
			[[ "$(mimetype -L -b "$arg")" =~ ^video/ ]]  \
				&& src[path]="$arg"

			#  There are two reasons to check for MPEG TS
			#  1. To force a keyframe at 0:00.000 during encoding and to
			#     put -ss and -to as *output* options. This is to avoid
			#     garbage-looking artefacts at the beginning.
			#  2. .ts (old MPEG transport stream) files cannot be recognised
			#     properly with mimetype (“mimetype -MLb” also doesn’t work,
			#     reports files as application/x-font-tex-tfm), so to recognise
			#     these input files as video, “file” is necessary to use.
			#     For that purpose, “file” is faster than mediainfo or ffprobe.
			#
			shopt -s nocasematch
			[[ "$(file -L -b  "$arg")" =~ .*(program|transport).* ]] && {
				seekbefore=t
				seekbefore_due_to_tr_or_pr_stream=t
				src[path]="$arg"
			}
			shopt -u nocasematch

			[ -v 'src[path]' ]  \
				|| err "The specified file is not a video.
				        Path: “${arg##*/}”."

			#  In case when the path to the video file is relative, i.e. con-
			#  tains no directories, it’s necessary to expand it, because
			#  later we’ll be switching directories (e.g. when extracting
			#  fonts), and a relative path may not work.
			#
			src[path]=$(realpath --logical "$arg")

			#  This is merely a precaution against file paths containing weird
			#  symbols. None were met so far, but should there be one, a later
			#  error caused by wrong expansion may be troublesome to find.
			#
			[ -r "${src[path]}" ] || err "Failed to set source path."

		elif [ -d "$arg" ]; then
			where_to_place_new_file="$arg"

		elif [[ "$arg" =~ ^fname_pfx=(.+)$ ]]; then
			#
			#  User prefix for the name of the encoded file.
			#  - user might want to add a user prefix to the filename,
			#    e.g. to note who does what on the video.
			#  - test suite uses it to keep the encoded files in order.
			#
			new_filename_user_prefix="${BASH_REMATCH[1]}"
			new_filename_user_prefix="${new_filename_user_prefix##+(\ )}"
			new_filename_user_prefix="${new_filename_user_prefix%%+(\ )}"

			#  Must check the correct length after assigning, or BASH_REMATCH[1]
			#  will become the length
			#
			_pfx_len=$( echo -n "${BASH_REMATCH[1]}" | wc --bytes )
			[[ "${_pfx_len}" =~ ^$INT$ ]]  \
				|| err "Couldn’t estimate length of the user prefix."

			(( _pfx_len > 200 ))  \
				&& err "File name prefix cannot take more than 200 bytes.
				        Please shorten it."

		elif [[ "$arg" =~ ^(loop_last_frame|llf|lfl)=($INT)$ ]]; then
			(( ${BASH_REMATCH[2]} >= 1 && ${BASH_REMATCH[2]} <= 999999 ))  \
				|| err "Seconds for the “loop_last_frame” option must be specified as a number between 1 and 999999."
			loop_last_frame=t
			loop_last_frame_s=${BASH_REMATCH[2]}

		elif [[ "$arg" =~ ^seekbefore=($INT)$ ]]; then
			#
			#  An alternative option to enable the “seekbefore” mode and
			#  simultaneously set the “seekbefore” value.
			#
			seekbefore=t
			seekbefore_due_to_request_from_cmdline=t
			seekbefore_s=${BASH_REMATCH[1]}

		elif [[ "$arg" =~ ^deint(erlace|)=($PBOOL_TRUE|$PBOOL_FALSE|auto)$ ]]; then
			if [ "${BASH_REMATCH[2]}" = 'auto' ]; then
				deinterlace='auto'

			elif [[ "${BASH_REMATCH[2]}" =~ ^$PBOOL_TRUE$ ]]; then
				deinterlace=t

			else
				unset deinterlace
			fi

		elif [[ "$arg" =~ ^--verbosity-help-([A-Za-z_][A-Za-z0-9_-]*)$ ]]; then
			ch_name=${BASH_REMATCH[1]}
			help_func="show_help_on_verbosity_channel_$ch_name"
			if	   [ -v VERBOSITY_CHANNELS[$ch_name]        ]  \
				&& [ "$(type -t "$help_func")" = 'function' ]
			then
				echo
				$help_func
				exit 0
			else
				err "No such verbosity channel: “$ch_name”."
			fi


		                   #  Options for internal use  #

		elif [[ "$arg" =~ ^force_scene_complexity=(static|dynamic)$ ]]; then
			scene_complexity=${BASH_REMATCH[1]}
			scene_complexity_forced=t

		elif [ "$arg" = do_not_report_ffmpeg_progress_to_console ]; then
			#
			#  This is a service option to shut the progressbar output. It is
			#    used by wrappers: Nadeshiko-mpv and Nadeshiko-do-postponed,
			#    whose logs get cluttered, because \r obviously doesn’t work
			#    when the console output in the end gets redirected to a file.
			#  This option does not disable $ffmpeg_progress, so ffmpeg will
			#    still write the progress log, which makes it possible for the
			#    wrappers to implement graphical progressbar (in the future).
			#
			do_not_report_ffmpeg_progress_to_console=t

		elif [ "$arg" = disable_atts_cache ]; then
			disable_atts_cache=t

		elif [ "$arg" = dryrun ]; then
			dryrun=t


			   #  Broken filesystem paths and unrecognised parameters  #
		else
			if [[ "$arg" =~ ^/ ]]  \
				|| [ -d "$PWD/${arg%%/*}" ]  \
				|| [ -d "$HOME/${arg%%/*}" ]
			then
				err "Path doesn’t exist:
				     $arg"
			else
				err "“$arg”: parameter unrecognised."
			fi
		fi
	done

	return 0
}


return 0