#  Should be sourced.

#  encode_libx264.sh
#  Nadeshiko encoding module for libx264.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#

#  Modules in Nadeshiko wiki:
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Writing-custom-modules
#


 # When neither GoP size (“$keyint”), nor “$keyint_min” are specified (set to
#  “auto” in the config), define “keyint_min” as
#    - 1× target frame rate for dynamic videos;
#    - 3× target frame rate for static videos.
#
libx264_set_keyint_min() {
	[ -v libx264_keyint  -o  -v libx264_keyint_min ] && return 0

	case "$scene_complexity" in
		dynamic)
			libx264_keyint_min=$(( libx264_keyint_min_sta * ${frame_rate%\.*} ))
			;;
		static)
			libx264_keyint_min=$(( libx264_keyint_min_dyn * ${frame_rate%\.*} ))
			;;
	esac

	return 0
}

libx264_set_x264_params() {
	declare -ga libx264_params=( -x264-params )

	local value

	[ -v libx264_opengop ]  \
		&& libx264_params+=( open-gop=1 )

	 # When the array has only the ffmpeg parameter and no values for it,
	#  unset the array.
	#
	if (( ${#libx264_params[*]} == 1 )); then
		unset libx264_params

	elif (( ${#libx264_params[*]} == 2 )); then
		return 0  # all good

	else  # combine the the elements 2..n into one
		unset libx264_params[0]
		value="$($IFS=':'; echo "${libx264_params[*]}")"
		libx264_params=( -x264-params "$value" )
	fi
	return 0
}

encode-libx264() {

	libx264_set_keyint_min

	libx264_set_x264_params

	pass() {
		local pass=$1
		local pass1_params=( -pass 1 -sn -an  -f $ffmpeg_muxer  /dev/null )
		local pass2_params=( -pass 2 -sn ${audio_opts[@]}
		                     -movflags +faststart  "$new_file_name" )
		local ffmpeg_caught_an_error
		local ffmpeg_all_options=()
		local -n extra_options=libx264_pass${pass}_extra_options
		local -n mandatory_options=pass${pass}_params

		info "PASS $pass"

		 # Sets ffmpeg_progress_log, so must be called before
		#  setting ffmpeg_all_options.
		#
		launch_progressbar_for_ffmpeg

		 # Do not use addition to this array! This syntax, i.e.
		#      arr+=( new_item )
		#  messes up the order of the elements. Use only assignment.
		#
		ffmpeg_all_options=(
			-y  -hide_banner  -v error  -nostdin  -nostats
			-probesize "$probe_duration"  -analyzeduration "$probe_duration"
			"${ffmpeg_input_options[@]}"
		)

		 # For transport streams, -ss and -to must be specified as output
		#  parameters to ffmpeg, that is, /after/ the input file. Also,
		#  a keyframe at 0:00 must be forced. These measures increase the
		#  encoding time, thus the condition.
		#
		if [ -v seekbefore ]; then
			ffmpeg_all_options+=(
				-ss "${ffmpeg_adaptive_seek_ss1[ts]}"
				-to "${stop[ts]}"
				"${ffmpeg_input_files[@]}"
				-ss "${ffmpeg_adaptive_seek_ss2_offset[ts]}"
				-force_key_frames 00:00:00.000
				-avoid_negative_ts  make_zero
			)
		else
			ffmpeg_all_options+=(
				-ss "${start[ts]}"
				-to "${stop[ts]}"
				"${ffmpeg_input_files[@]}"
			)
		fi

		[ -v loop_last_frame ] && {
			ffmpeg_all_options+=(
				-t $(( ${duration[total_s]} + loop_last_frame_s )).${duration[ms]}
				-shortest
			)
		}

		ffmpeg_all_options+=(
			"${ffmpeg_map[@]}"
			-r:v "$frame_rate"
			"${ffmpeg_filters[@]}"
			-c:v $ffmpeg_vcodec -pix_fmt $libx264_pix_fmt
				${libx264_keyint:+-g $libx264_keyint}
				${libx264_keyint_min:+-keyint_min $libx264_keyint_min}
				"${libx264_params[@]}"
				-b:v $vbitrate
				${libx264_qcomp:+-qcomp $libx264_qcomp}
			-preset:v $libx264_preset -tune:v $libx264_tune
			-profile:v $libx264_profile -level $libx264_level
			"${extra_options[@]}"
			${ffmpeg_progressbar:+-progress "$ffmpeg_progress_log"}
			-map_metadata -1
			-map_chapters -1
			-metadata title="$outp_mdata_title"
			-metadata description="$outp_mdata_description"
			-metadata comment="$outp_mdata_comment"
			"${ffmpeg_colour_space_output_tags[@]}"
			"${mandatory_options[@]}"
		)

		info "$ffmpeg ${ffmpeg_all_options[*]@Q}" >>$LOGPATH

		FFREPORT="file=$LOGDIR/${LOGNAME//:/\\:}.ffmpeg-pass$pass:level=32"  \
		"${time[@]}"  \
		$ffmpeg "${ffmpeg_all_options[@]}"  \
			|| ffmpeg_caught_an_error=t

		stop_progressbar_for_ffmpeg
		[ -v ffmpeg_caught_an_error ]  \
			&& err "ffmpeg error on pass $pass."
		return 0
	}

	pass 1
	pass 2
	return 0
}


return 0