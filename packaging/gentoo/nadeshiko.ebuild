#  nadeshiko-${PV}.ebuild
#  Author: deterenkelt, 2019–2024
#  Distributed under the terms of the GNU General Public License v3

EAPI="7"
inherit eutils

SLOT="0"
DESCRIPTION="A Linux tool to cut short videos with ffmpeg"
SRC_URI="https://codeberg.org/deterenkelt/Nadeshiko/archive/v${PV}.tar.gz -> ${P}.tar.gz"
HOMEPAGE="https://codeberg.org/deterenkelt/Nadeshiko"
LICENSE="GPL-3"
MERGE_TYPE="binary"
KEYWORDS="~*"


IUSE="  +ansifilter
        +colour-space
        +crop
        +fdk-aac
        +hardsub
        +mpv
        +opus
        +parallel
        +rdfind
         time-stat
        +x264
         vorbis
        +vp9
        +lae
     "


RDEPEND="  >=app-shells/bash-4.4
           >=sys-apps/grep-3.0
           >=sys-apps/sed-4.3
             sys-apps/coreutils
             sys-apps/util-linux
             sys-process/procps

           >=media-video/ffmpeg-4.0[encode,threads,bluray]
             media-video/mediainfo
             dev-perl/File-MimeInfo
             sys-apps/file
             sys-devel/bc
             app-text/xmlstarlet

           hardsub? (
                 media-video/ffmpeg[iconv,libass,truetype,fontconfig]
                 media-libs/libass[fontconfig,harfbuzz]
           )

           colour-space? (
                 media-video/ffmpeg[zimg]
               >=media-libs/zimg-3.0.2
           )

           x264? (
               >=media-video/ffmpeg-4.0[x264]
           )

           fdk-aac? (
               >=media-video/ffmpeg-4.0[fdk]
           )

           vp9? (
               >=media-video/ffmpeg-4.0[vpx]
               >=media-libs/libvpx-1.7.0
           )

           opus? (
               >=media-video/ffmpeg-4.0[opus]
           )

           vorbis? (
               >=media-video/ffmpeg-4.0[vorbis]
           )

           mpv? (
               >=media-video/mpv-0.28
               >=dev-lang/python-3.0
               >=dev-python/pygobject-3.20
               >=x11-libs/gtk+-3.20
                 x11-libs/libnotify
                 sys-apps/findutils
                 sys-process/lsof
                 app-misc/jq
                 net-misc/socat
           )

           crop? (
                 sys-fs/inotify-tools
                 media-video/mpv[lua]
           )

           time-stat? (
                 sys-process/time
           )

           parallel? (
                 sys-process/parallel
           )

           ansifilter? (
                 app-text/ansifilter
           )

           rdfind? (
                 app-misc/rdfind
           )

           lae? (
                 >=media-video/ffmpeg-4.0[x265]
           )
        "


REQUIRED_USE="  || ( x264 vp9 )
                fdk-aac? ( x264 )
                opus? ( vp9 )
                vorbis? ( vp9 )
             "


src_unpack() {
	unpack ${A}
	#  Codeberg will pack the directory tree simply as “nadeshiko” in the ar-
	#  chive. Make, however, will expect the directory name as “package name
	#  in lowercase – hyphen – version number”, because the default “source
	#  directory” in EAPI is: $S == $WORKDIR/$P, and $P == $PN-$PV.
	cd "${WORKDIR}"
	mv ${PN}  ${PN}-${PV}
}


src_prepare() {
	default
}


src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install
}
