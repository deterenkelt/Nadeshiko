#! /usr/bin/env bash

#  nadeshiko-do-postponed.sh
#  A companion script to Nadeshiko‑mpv for running postponed encoding jobs.
#  Author: deterenkelt, 2018–2024
#
#  For licence see nadeshiko.sh
#


set -feEuT
shopt -s  inherit_errexit  extglob
MY_SUITE_NAME='nadeshiko'
BAHELITE_LOAD_MODULES=(
	libdir
	modulesdir
	resdir
	logging:use_cachedir
	rcfile:use_metaconf,use_defconf
	messages_to_desktop
	misc
)
mypath=$(dirname "$(realpath --logical "$0")")
case "$mypath" in
	'/usr/bin'|'/usr/local/bin')
		source "${mypath%/bin}/lib/nadeshiko/bahelite/bahelite.sh";;
	*)
		source "$mypath/lib/bahelite/bahelite.sh";;
esac

noglob_off
for module in "$MODULESDIR"/nadeshiko-do-postponed/*.sh ; do
	. "$module" || err "Couldn’t source module $module."
done
noglob_rewind

place_examplerc 'nadeshiko-do-postponed.10_main.rc.sh'

declare -r version="2.8.7"

declare -r postponed_commands_dir="$CACHEDIR/postponed_commands_dir"
declare -r completed_jobs_dir="$postponed_commands_dir/completed"
declare -r failed_jobs_dir="$postponed_commands_dir/failed"

#  For “02_job_control.sh” module
declare -r JOB_CONTROL_MODE='Nadeshiko'


on_exit() {
	#  Make sure, that we move the failed job files to $failed_jobs_dir if the
	#  program is interrupted. Normally, the coprocess running postponed job
	#  handles this (in case of handled interruptions too), so this here is
	#  a sort of precaution measure against termination caused from the out-
	#  side (^C for example).
	#
	[ -v job_logdir  -a  -d "${job_logdir:-}" ]  \
		&& move_job_to_failed "$jobfile" "$job_logdir"

	rmdir "$failed_jobs_dir" 2>/dev/null || true  # Remove directory, if empty.

	return 0
}



print_program_version $version
cd "$TMPDIR"
post_read_rcfile
parse_args
check_required_utils
declare -r xml='xmlstarlet'   # for lib/xml_and_python_functions.sh

single_process_check
pgrep -u $USER -af "bash.*nadeshiko.sh" &>/dev/null  \
	&& err 'Cannot run at the same time with Nadeshiko.'
pgrep -u $USER -af "bash.*nadeshiko-mpv.sh" &>/dev/null  \
	&& err 'Cannot run at the same time with Nadeshiko-mpv.'

collect_jobs

#  When launched from the environment.
#  GUI is less functional, not recommended.
#
if [ -v DISPLAY  -a  ! -v CONNECTED_TO_TERMINAL ]; then
	source "$MODULESDIR/nadeshiko-do-postponed/dialogues_${dialog:=gtk}.sh"
	show_dialogue_launch_jobs "$jobs_to_process" "$failed_jobs_at_init"
	IFS=$'\n' read -r -d ''  resp_action  < <(echo -e "$dialog_output\0")
	if [ "$resp_action" = 'run_jobs' ]; then
		run_jobs
		# show_dialogue_jobs_result
	else
		exit 0
	fi

#  When launched in an interactive terminal
#  Fully functional, recommended.
#
elif [ -v CONNECTED_TO_TERMINAL ]; then
	(( jobs_to_process != 0 )) && {
		echo
		antechamber_prompt="${__cl}Press"
		antechamber_prompt+=" ${__bri}${__g}[${__w}Space${__g}]${__s}"
		antechamber_prompt+=" to abort,"
		antechamber_prompt+=" ${__bri}${__g}[${__w}Enter${__g}]${__s}"
		antechamber_prompt+=" to continue${__s} ${__g}>${__s} "
		while true; do
			read -n1 -s -p "$antechamber_prompt"
			case "$REPLY" in
				' ')
					echo
					exit 0
					;;

				'')
					echo
					run_jobs
					exit 0
					;;
			esac
		done
	}

#  Suitable for fully automated runs, as in cron jobs.
#
else
	(( jobs_to_process != 0 )) && run_jobs
fi


exit 0