---
name: Bug report
about: "​­​"
---

##### What Nadeshiko was supposed to do


##### What happened


##### Steps to reproduce the problem


##### Logs

Attach, spoiler or post links to pastebin with relevant logs from
`$HOME/.cache/nadeshiko/logs/`. If you haven’t done so yet, you may
install `ansifilter` in order to create a plain text version of the
log file in addition to the one with colours. It will have a `.log-nc`
extension.
