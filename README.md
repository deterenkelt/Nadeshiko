<h1 align="center">
    Nadeshiko
</h1>
<p align="center">
    <i>A Linux tool to cut short videos with ffmpeg</i>
</p>

## nadeshiko.sh

Command line tool. Smart. Configurable. Extensible.

![GIF: Using Nadeshiko in terminal](https://codeberg.org/deterenkelt/Nadeshiko/wiki/raw/img/nadeshiko_in_a_terminal.gif)

[Why it is best](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Why-Nadeshiko-is-best)  ⋅  [How to use](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Nadeshiko)  ⋅  [Tips](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Tips-for-Nadeshiko)

 

## nadeshiko-mpv.sh

Frontend that connects to mpv player.

![GIF: Nadeshiko-mpv in action](https://codeberg.org/deterenkelt/Nadeshiko/wiki/raw/img/nadeshiko-mpv.gif)

[Predictor](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Nadeshiko‑mpv.-Predictor)  ⋅  [How to use](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Nadeshiko‑mpv)  ⋅  [Tips](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Tips-for-Nadeshiko‑mpv)

 

## Main features

* <ins>Guarantees</ins> to fit video fragment to specified file size.
  * One run – one clip. No more encoding by trial and error.
* Keeps the quality good or refuses to encode.
  * Optimal bitrate ranges are determined by the codec type, resolution and scene complexity.
  * Quality is proven by numerous test runs (See [this test](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Researches – VP9:-encoding-to-size,-part 1) for example.)
  * No need to learn FFmpeg!
  * Resolution may be lowered as necessary to save quality.
  * Built-in predictor: <br><a href="https://codeberg.org/deterenkelt/Nadeshiko/wiki/Nadeshiko‑mpv.-Predictor">
<img alt="Predictor" src="https://codeberg.org/deterenkelt/Nadeshiko/wiki/raw/img/nadeshiko-mpv-predictor/predictor.gif"/>
</a>

* Customisable
  * Every setting is configurable through a simple config file.
  * Preconfigured options for VP9 (+2 audio codecs) and H.264 (+3 audio codecs).
  * Turn subtitles and audio `on` or `off` by default.
  * Custom style for subtitles, that have none.
  * Fine-tune the encoding mechanism (but be careful!)
  * If you feel like adding a key or two to the default FFmpeg command – you can add them as input or output options (1 pass and 2 pass separately).
  * Clone configs to create custom presets!
* Simple installation
  * Download and run. No compilation is needed.
* Hardsubbing support
  * For text-based subtitles: SubRip (.srt), ASS (.ass, .ssa), WebVTT (.vtt) – built-in or external.
  * For pixel-based DVD and Bluray subtitlies – yes, but only for the built-in ones.
  * With fonts, as you see them in the player! Nadeshiko will extract them, if necessary, and turn on OpenType for FFmpeg.
  * Fonts built-in in the .ass files are extracted.
  * Extracted attachments are cached to save time for the next runs.
* Cropping
  * Set coordinates by hand…
  * …or pick them interactively (built-in [mpv_crop_script](https://github.com/TheAMM/mpv_crop_script)).
* Colour space conversion
  * Enabled optionally. Can be forced.
  * Includes tonemap and contrast restretch for HDR to SDR conversion.
  * Supports anything that ffmpeg supports.
  * Customisable exclusions and colour space targets.
  * Strict and safe tagging of the output by default.
* Optimised composition of the encoded file name
  * smart shortening;
  * timestamp format optimised to frame step in the source.

 

## Don’t look down

Read about [which codec is best at what](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Tips#differences-between-codec-sets) or even better – go through the [**Crash course**](https://codeberg.org/deterenkelt/Nadeshiko/wiki/Crash-course), you d-don’t need to know the scary truth… Don’t look down.


<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>


![Don’t let Nadeshiko die!](https://codeberg.org/deterenkelt/Nadeshiko/wiki/raw/img/Nadeshiko.jpg)

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
.
</p>

<p align="center">
<i>This program’s name is a reference to Nadeshiko Kagamihara, a character from <a href="https://en.wikipedia.org/wiki/Laid-Back_Camp">Yurucamp</a>. The original manga was drawn by あfろ for Houbunsha, and the anime television series is made by studio C-Station.</i>
</p>

<p align="center">
<i>The ghosts on the picture above were taken from <a href="https://en.wikipedia.org/wiki/Katanagatari">Katanagatari</a>. It was originally written as a light novel by Nisio Isin for Kodansha and illustrated by Take. The light novel was animated by studio White Fox.</i>
</p>

<p align="center">
<i>Nadeshiko uses FFmpeg (which in its turn uses libx264, libvpx, libopus, libvorbis, libfdk_aac, aac, libass…), mediainfo, mpv, mpv_crop_script, GNU grep, GNU sed and GNU time.<br><br>Let’s be grateful to their authors for their hard work.</i>
</p>
