#! /usr/bin/env bash

#  nadeshiko.sh
#  A Linux tool to cut short videos with ffmpeg.
#  Author: deterenkelt, 2018–2025
#

 # Nadeshiko is free software; you can redistribute it and/or modify it
#    under the terms of the GNU General Public Licence as published
#    by the Free Software Foundation; either version 3 of the Licence,
#    or (at your option) any later version.
#  Nadeshiko is distributed in the hope that it will be useful,
#    but without any warranty; without even the implied warranty
#    of merchantability or fitness for a particular purpose.
#  See the GNU General Public Licence for more details.
#


set -feEuT
shopt -s  inherit_errexit  extglob

MY_SUITE_NAME='nadeshiko'
BAHELITE_LOAD_MODULES=(
	libdir
	modulesdir
	logging:use_cachedir
	rcfile:use_metaconf,use_defconf
	misc
)
mypath=$(dirname "$(realpath --logical "$0")")
case "$mypath" in
	'/usr/bin'|'/usr/local/bin')
		source "${mypath%/bin}/lib/nadeshiko/bahelite/bahelite.sh";;
	*)
		source "$mypath/lib/bahelite/bahelite.sh";;
esac

noglob_off
rm -f "$LOGDIR/"ffmpeg*     \
      "$LOGDIR/"variables   \
      "$LOGDIR/time_output"
noglob_rewind

#  For manipulating timestamp forms.
. "$LIBDIR/time_functions.sh"
#  Backing up fonts from recently used files to save time on extraction.
. "$LIBDIR/attachment_caching.sh"

noglob_off
for module in "$MODULESDIR"/nadeshiko/*.sh ; do
	. "$module" || err "Couldn’t source module $module."
done
noglob_rewind

place_examplerc 'nadeshiko.10_main.rc.sh'

declare -r version='2.30.0'
declare -r release_hint=''

 # Minimal libav libraries versions
#  1) row-mt appeared after ffmpeg-3.4.2
#  2) -t <duration> doesn’t always grant precise end time,
#     but -to <timestamp> stopped causing problems only after ffmpeg-4.0.0.
declare -r libavutil_minver='56'
declare -r libavcodec_minver='58'
declare -r libavformat_minver='58'



on_exit() {
	#  The “on_exit” trap deals with leftovers and maintenance jobs, which
	#  should run only if encoding was set into motion. This trap is skipped,
	#  however, for all early exits (be it due to a lack of dependency, “dry-
	#  run” mode, or querying --version), as the maintenance jobs, that this
	#  trap watches over, are concerning only the cases, when encoding has
	#  been started.
	#
	[ ! -v run_checks_passed  -o  -v dryrun ] && return 0

	rm -f  ffmpeg2pass-0.log  ffmpeg2pass-0.log.mbtree
	#  Turning the cursor visible again,
	#  in case an error happened before ffmpeg progressbar could restore it.
	[ -v ffmpeg_progressbar ]  \
	&& [ ! -v do_not_report_ffmpeg_progress_to_console ]  \
		&& tput cnorm
	run_maintenance_for_the_attachments_cache

	return 0
}



print_program_version "$version" "$release_hint"
post_read_rcfile
parse_args
run_checks
display_assessed_parameters
until [ -v encoded_duly ]; do
	fit_bitrate_to_filesize
	display_final_encoding_settings
	[ -v dryrun ] && exit 0
	prepare_common_ffmpeg_options
	encode
	post_encode_validity_checks  \
		&& encoded_duly=t
done
#  Nadeshiko-mpv relies on this message to find the encoded file.
info-ns "Encoded successfully."
post_successful_encode


exit 0
