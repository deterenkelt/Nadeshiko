#  nadeshiko.90_eac3.rc.sh
#
#  Parameters for encoding with eac3 audio codec.



 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  This codec is considered to be a “structurally enhanced” version of ac3,
#  so the encoding properties for 2-channel audio are the same as for ac3.
#  For the details and considerations, see nadeshiko.90_ac3.rc.sh.
#
eac3_profiles=(
	[256]="-b:a 256k -cutoff 20000"
	[224]="-b:a 224k -cutoff 20000"
	[192]="-b:a 192k -cutoff 20000"  # Recommended in the official docs.
	[160]="-b:a 160k -cutoff 18000"  # Lowest and recommended on FFmpeg wiki.
	[128]="-b:a 128k -cutoff 14000"  # Lowest for 2-channel audio in the
	                                 #  official docs.
	[112]="-b:a 112k -cutoff 8000"   # Good enough for mp3. Actually, eac3
	                                 #  sounds not that bad even at 64k
	                                 #  (if encoded in 2023).
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
eac3_is_fairly_good_at='192'


bitres_profile_360p+=(
	[eac3_profile]=112
)

bitres_profile_480p+=(
	[eac3_profile]=128
)

bitres_profile_576p+=(
	[eac3_profile]=160
)

bitres_profile_720p+=(
	[eac3_profile]=192
)

bitres_profile_1080p+=(
	[eac3_profile]=192
)

bitres_profile_1440p+=(
	[eac3_profile]=192
)

bitres_profile_2160p+=(
	[eac3_profile]=192
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
eac3_112_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

eac3_128_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

eac3_160_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

eac3_192_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

eac3_224_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

eac3_256_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)
#
#  FFmpeg libavcodec 58.134.100 / 58.134.100