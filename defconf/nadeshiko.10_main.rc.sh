#  nadeshiko.rc.sh
#
#  The primary configuration options for nadeshiko.sh. All options, which
#  ordinary and advanced users may look for, are gathered here. Codec- and
#  feature-specific options, that may be of interest only to pros and experi-
#  menters are to be found in the “defconf” directory. That directory resides
#  either beside the executables (in a standalone configuration, i.e. as a
#  single folder) or by paths like “/usr/share/nadeshiko/defconf” or “/usr/
#  local/share/nadeshiko/defconf”.
#
#  In your own configuration files (those within ~/.config/nadeshiko/) you may
#  copy only those options, that you wish to alter, and remove the rest.
#
#  This RC file uses bash syntax:
#    key=value
#  1. Quotes can be omitted, if the value is a string
#       without spaces.
#  2. The equals sign should stick to both key and value –
#       no spaces around “=”.
#  3. Flag options accept any form for on/off: yes/no, y/n,
#       true/false, t/f, 1/0, enabled/disabled, on/off too.
#       The case of letters doesn’t matter.
#
#  Nadeshiko wiki may answer a question before you ask it!
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki
#



                         #  Common options  #

 # Whether to send notifications to desktop.
#
desktop_notifications=on


 # Presets for maximum size of the cut to be encoded
#  [kMG] suffixes use powers of 2, unless kilo is set to 1000.
#

#  Normal size. Command line option to force this maximum size: “normal”.
#
max_size_normal=20M


#  Small size. Command line option to force this maximum size: “small”.
#
max_size_small=10M


#  Tiny size. Command line option to force this maximum size: “tiny”.
#
max_size_tiny=2M


#  Unlimited size. For manual control and experiments. Intended to be used
#  along with vbNNNN, abNNNN and XXXp. Command line option to force this
#  maximum size: “unlimited”.
#
max_size_unlimited=99999M


#  Which of the max_size_* options to use by default.
#  One of: tiny, small, normal, unlimited.
#  (It’s still possible to set an arbitrary size, that is not among
#  the presets, with the “size=” command line option.)
#
max_size_default=normal


 # The multiplier for ‘k’ ‘M’ ‘G’ suffixes in max_size_*. Can be 1024 or 1000.
#  Change this to 1000, if the server, that you often upload to, uses SI units
#  and complains about exceeding the maximum file size. For a one time override
#  pass “si” or “k=1000” via command line. This option doesn’t affect bit rates
#  specified with “vb=…”, those are always calculated with powers of 2.
#
kilo=1024



                         #  FFmpeg options  #

 # Commands to call FFmpeg binaries
#
#  To direct Nadeshiko to a custom path, where ffmpeg is installed,
#  specify the absolute paths to those binaries here.
#
ffmpeg='ffmpeg'
ffprobe='ffprobe'


 # Input options for encoding. In general, there’s no need to specify anything
#  here. One case may be when your source doesn’t have metadata that ffmpeg
#  can read. To extend the list, specify each option or key as a separete
#  string. Example:
#     ffmpeg_input_options=(
#        -some_key  argument
#        -some_other_key "argument with spaces"
#     )
#
ffmpeg_input_options=()


 # Video codec
#  “libx264” – good quality, fast, options are well-known.
#  “libvpx-vp9” – often better picture quality (sometimes not), better effici-
#     ency in frames per MiB, but slower and its behaviour is less predictable.
#
ffmpeg_vcodec='libvpx-vp9'


 # Audio codec
#        Name  Compatible  Notes
#               container
#     libopus     WebM     The best.
#   libvorbis     WebM     Quite good.
#  libfdk_aac      MP4     Equally good as Vorbis, the best FFmpeg has for MP4.
#                            The licence doesn’t allow to build distributable
#                            packages with this library, so you’ll have to com-
#                            pile FFmpeg yourself in order to have libfdk_aac.
#         aac      MP4     Still good, but worse than libvorbis and worse than
#                            libfdk_aac on bitrates <128k.
#  libmp3lame      MP4     Less efficient than all of the above.
#
ffmpeg_acodec='libopus'


 # User defined set of FFmpeg filters
#
#  Could be used to insert custom filters, like “hwupload” and “hwdownload”,
#  into the string, that will be the argument to -filter_complex, thus
#  “wrapping” all other filters, that may be used – for hardsubbing, scaling
#  and cropping. Primarily intended to make possible hardware accelerated
#  encoding and decoding.
#
#  The comma for separating filters would be placed automatically after
#  “ffmpeg_filters_pre” and before “ffmpeg_filters_post” (you don’t have
#  to handle this).
#
ffmpeg_filters_pre=''
ffmpeg_filters_post=''


 # Default action for hardcoding subtitles
#  “yes” – hardcode the subtitles (burn them into video frames)
#  “no” – discard subtitles.
#
#  Nadeshiko won’t add subtitles as a separate stream, for such videos are not
#  shareable: most players in the browsers and whatnot cannot read subs from a
#  standalone file. So it either hardcoded subs or no subs.
#
#  When the config option “subs” is set to “yes”, the command line option
#  “subs” may further specify a particular stream in the source to hardcode.
#  When this option is set to “no”, requesting subtitles is considered an
#  error.
#
subs=yes


 # Default action for re-encoding the audio track
#  “yes” – encode audio track
#  “no” – discard audio tracks
#
#  When the config option “audio” is set to “yes”, the command line option
#  “audio” may further specify a particular stream in the source to encode.
#  When this option is set to “no”, requesting an audio track is considered
#  an error.
#
audio=yes


 # Output frame rate. The standard 23.976 frames per second for best
#  compatibility.
#
#  An exception is made for source videos, whose frame rate is within bounds
#  of 23…30 inclusively – those videos maintain their original frame rate.
#  Meaning that 23.0, 23.976, 24, 24.5, 25, 25.1, 29.970 or 30 frames per
#  second in the source will be the same rates in the output. This is needed
#  to preserve quality in being frame-exact. Reducing an old 29.97 fps video
#  to 23.97 fps often is not worth it, because the amount of space needed to
#  save a 29.97 fps output is an increase that’s close to being negligible,
#  while the conversion between frame rate will bring in: frame drop and hence
#  fewer original “frame parts”, stuttering and compensating it with a motion
#  interpolation filter, which blurs and reduces quality on the “whole” frames
#  to bring them into a something inbetween two other frames.
#
#  Source frame rates below and above the specified bounds are brought to the
#  rate specified here, and a motion interpolation is applied. (This is mostly
#  intended for converting fragments of 60 fps and 120 fps sources to some-
#  thing more lightweight and shareable.)
#
frame_rate='24000/1001'


 # Whether to enforce conversion to one output frame rate,
#  independently of the source.
#
#  When this option is set to “no”, Nadeshiko adapts the output frame rate
#  as described above (it’s the default setting).
#
#  When this option is set to “yes”, the frame rate specified above is used
#  for all conversions, whichever frame rate the source would have. (Motion
#  interpolation is applied – if allowed, see below.)
#
#  Use this option, only if your device has a trouble with playback, and
#  needs you to encode files for it with a specific rate, e.g. 23.970.
#
frame_rate_fixed=no


 # When frame rate is reduced, extra frames are simply dropped. This causes
#  jittery transition from cadre to cadre. To make the change of frames in
#  the fragment smoother, ffmpeg may be told to employ the motion interpola-
#  tion filter, which will fit each frame to the one going next, better. The
#  drawback of this operation is that it takes much much more time. The entire
#  process of video encoding will be limited to 1 (one) thread. Because of the
#  filter. For this reason it’s advised – if you really need this – to enable
#  this feature in a separate Nadeshiko encoding profile (a separate configu-
#  ration file).
#
motion_interp_allowed=no


 # Whether Nadeshiko should display a progressbar on console
#    for the first and the second pass of ffmpeg.
#  Possible values: on, off.
#
ffmpeg_progressbar=on



                      #  Colour space conversion  #

 # If you’re new to colour spaces, read the Introduction on the wiki:
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Colour-space
#

 # Whether to enable colour space conversion
#
#  When enabled, and the colour space in the source was fully detected (that
#  is, its matrix coefficients, its primaries and its transfer function are
#  known), converts the file to the colour space specified with the “target_
#  colour_space” option (see below). In the case source data would be incom-
#  plete, this option is automatically disabled. Indeed, when the target
#  colour space matches the target one, no conversion is done.
#
#  When not enabled, omits the conversion. In this case colours in the output
#  will depend on what ffmpeg determines as the colour space characteristics
#  of the source file. And how valid that would be, depends on how complete
#  and correct is the metadata set, with which the container was tagged. Nade-
#  shiko itself won’t interfere with colour space detection and application.
#
#  Cautions:
#    - Do not expect all files to be tagged properly. And remember, that im-
#        properly tagged source may produce only improperly converted files.
#    - Do expect, that colour space conversion is lossy: the colours may be
#        off or some artefacts would appear. Though the amount of quality
#        loss depends on the type of conversion. BT.601 to BT.709 conversion
#        is close to perfect, but additional artefacts may be coming, if the
#        source is interlaced (and requires a filter). Variations of BT2020
#        are converted to BT.709 with a significant change in the colours –
#        however, that’s inevitable, as a colour space that’s rich is getting
#        squeezed into a limited space.
#    - Most commonly used colourspaces were tested, but some rarely seen ones
#        may produce unexpected results: Nadeshiko cannot convert well what it
#        does not know about (for one thing, the ARIB B67 transfer function
#        was not tested, as no such source was available).
#
do_colour_space_conversion=yes


 # Whether to push colour space conversion in the cases when said conversion
#  would normally be disabled. That is, if the source colour space charac-
#  teristics are unset or incomplete.
#
#  Generally, forcing it is not recommended. Without knowing the colour space
#  properties of the source file, it’s impossible to guarantee, that the con-
#  version will be performed correctly. This option exists only to allow those
#  people, who create videos, but have trouble tagging them, to rely on the
#  defaults, which ffmpeg assumes when decoding a file. Also it may be used
#  as the last resort option, to encode at least something, which would be
#  better than nothing at all, when the source is some RGB colour space.
#
#  This option takes effect only when “do_colour_space_conversion” is enabled.
#  It cannot force conversion of those colour spaces, that are marked as ones
#  to be preserved (see below).
#
force_colour_space_conversion=no


 # If the colour space conversion is enabled, determines the target colour
#    space, to which the source must be converted to.
#  Possible values:
#                    The value in the left column translates to…
#                 Matrix coef.    Primaries    Tone curve (gamma)    Range
#
#    bt709        BT.709          BT.709       BT.709                limited
#    bt2020_cl    BT.2020 CL      BT.2020      SMPTE ST 2084         limited
#
#  It’s possible to define your own colour space targets, see the description
#  in defconf/nadeshiko.10_colour_space.rc.sh. But before you do that, perhaps
#  the “preserve_colour_spaces” option (see below) would be an easier solution
#  to what you need?
#
target_colour_space='bt709'


 # The list of colour spaces, that must never be converted to any other.
#
#  If colour space conversion is enabled, *and* the source would have all the
#  colour space related properties set (that is, matrix coefficients, prima-
#  ries and the tone curve), *and* the color_space property (as reported by
#  ffprobe) would match any item from the list specified as the value for
#  this option, then the “do_colour_space_conversion” option is switched off,
#  and the “target_colour_space” option is ignored. Exclusion is performed
#  before the “force_colour_space_conversion” may take an effect.
#
#  Example: “preserve_colour_spaces=(bt470bg smpte170m)” which tells
#  to preserve BT.601 colour spaces used for DVDs. (However, most DVDs don’t
#  have all the needed tags set, thus, the conversion for them doesn’t happen
#  by default.)
#
#  The name is a bit confusing, as the preservation doesn’t in fact involve
#  any action. The conversion is simply ignored, and staying in the same colour
#  space is relied to ffmpeg. This works, as long as the source is properly
#  tagged and its colour space (or at least the matrix coefficients) could be
#  detected. The output is then tagged as the source (if tagging is not dis-
#  abled, see below).
#
#  Also, unlike with the “target_colour_space” option, the values accepted
#  by this one are not complete sets, but rather named matrix coefficients
#  (which are commonly, but wrongly equalled to “colour spaces”, which they
#  are only a part of).
#
#  Possible values – anything from this list:
#    $ man -P 'less -p "^\s+colorspace"' ffmpeg-codecs
#
preserve_colour_spaces=()


 # Whether to put colour space properties in the container/stream metadata
#
#  When colour space conversion is performed, tagging is always performed, too,
#  independently of what this option is set to. In case when the conversion is
#  disabled and this option is turned on, it simply tags the output file with
#  the metadata corresponding to the source video (with a presumption, that
#  ffmpeg has detected the colour space and transferred pixel format properly).
#  Tagging helps the decoding software to determine the colour space.
#
tag_output_colour_space=yes


 # Normally, when the forced colour space conversion took place and the meta-
#  data required for that conversion weren’t complete, Nadeshiko doesn’t tag
#  the output file. This option can force the output to be still tagged with
#  the target colour space (tagged fully: with matrix coefficients, primaries,
#  transfer function and colour range).
#
forced_colour_space_conversion_also_forces_tagging=no



                       #  Subtitles, attachments  #

 # Subtitle font and style to use, when they are not defined, e.g. when
#  rendering SubRip or VTT subtitles, which have no embedded style.
#
#  Mpv uses its own options for rendering those, but unfortunately, translat-
#  ing mpv options makes little sense in this case – rendering techniques
#  differ and give different result.
#
#  The items of this list are fed to FFmpeg’s subtitle filter as force_style
#  parameter. force_style uses SSA style fields, see chapter 5. Style lines
#  in this doc: http://moodub.free.fr/video/ass-specs.doc
#
#  If you need to override just one parameter, that’s enough to write e.g.
#     ffmpeg_subtitle_fallback_style[Fontname]='MyFavouriteFont'
#  instead of copying the whose section. To get the correct font name,
#  consult the output of
#     $ fc-list | grep 'MyFavouriteFont'
#  But remember, that “fc-list” may use some backslashes in the output –
#  you don’t need to copy those.
#
ffmpeg_subtitle_fallback_style=(

	#  Font family name.
	#  Format: any string, escaping is not needed.
	[Fontname]='Roboto Medium'

	#  Font size.
	[Fontsize]='20'

	#  Text colour
	#  Format: &HCC332211, hex codes in ABGR order, prepended with “&H”.
	#  Alpha: 00…FF, from opaque to transparent.
	[PrimaryColour]='&H00F0F0F0'

	#  Uncertain. “may be used instead of the Primary colour” (how?!)
	#  Format: see PrimaryColour
	#[SecondaryColour]=''

	#  Text outline colour. Description warns, that it “may be used instead
	#  of the Primary or Secondary colour”
	#  Format: see PrimaryColour
	[OutlineColour]='&H03111111'

	#  Uncertain. The description says: “the colour of the subtitle outline
	#  or shadow, if these are used.”
	#  Format: see PrimaryColour
	#[BackColour]=''
	#
	# ^ Note the British spelling – it is the standard here.

	#  Format: -1 = True, 0 = False
	#[Bold]=''

	#  Format: 1 = Outline + drop shadow, 3 = Opaque box
	[BorderStyle]='1'

	#  If BorderStyle is 1, then this specifies the width of the outline
	#  around the text, in some abstract measure.
	#  Format: 0, 1, 2, 3 or 4.
	[Outline]='1.4'

	#  If BorderStyle is 1, then this specifies the depth of the drop shadow
	#  behind the text, in some abstract measure too, probably. Drop shadow
	#  is always used in addition to an outline – SSA will force an outline
	#  of 1 pixel if no outline width is given.
	#  Format: 0, 1, 2, 3 or 4.
	[Shadow]='0'
)


 # Directory for extra fonts
#
#  To encode with the right fonts, which aren’t installed on the system, or
#  weren’t added as attachments to the container. You may need these in the
#  following cases:
#    - you downloaded subtitles from the internet, and fonts to them
#      came in an archive;
#    - you make your own video and want to take a sample of it with hardsub,
#      so there’s no container with embedded fonts yet;
#    - you keep the list of system fonts short, and auxiliary fonts are put
#      into folders, where only specialised software (for editing or playback)
#      looks for them and loads.
#  There’s no need to use this, if you use only system fonts and those shipped
#    with the source files (as attachments inside a container).
#  Mpv takes additional fonts from  ~/.config/mpv/fonts. If you use it, you
#    probably want to use this path here as well.
#  Format is an absolute path to a directory. No ~/ or $HOME allowed.
#
#extra_fonts_dir=''


 # Cached attachments
#
#  How long should extracted attachments be preserved in cache, before
#  they’ll be considered stale and pruned. Caching saves a lot of time
#  on cutting videos from the same file.
#
#  Three months was considered a sensible default value, as there may be
#  breaks in the period of watching a series, or one may want to return
#  to it to cut a .webm or .mp4 later.
#
#  Possible values: 1…99999
#
cached_attachments_lifespan_in_months=3



                    #  The output file, its name   #
                    #    and where to place it     #

 # Where to place the encoded file, if the other paths, that would be usually
#  chosen, would happen to be non-writeable
#
#  If Nadeshiko is launched by itself (not from Nadeshiko-mpv and not as a
#  postponed job) – then the encoded file will be placed to the current work-
#  ing directory of the shell, which launched Nadeshiko (and by default it’s
#  $HOME).
#
#  Nadeshiko-mpv can read the current working directory from mpv and pass
#  it as an argument to Nadeshiko, so that the encoded file would be placed
#  there. The source, however, may reside on a remote read-only filesystem
#  or it may be a disk or disk image, which also is a filesystem that cannot
#  be written to. It’s for these cases that this option exists.
#
#  Possible values:
#     “auto” – if normally chosen path is not writeable, try $XDG_DESKTOP_DIR,
#         $HOME and /tmp – in this order.
#     <your path>
#
target_directory_if_the_intended_is_nonwriteable='auto'


 # Whether the prefix, given to the output file name, should be copied to the
#  metatadata title (prepending the original title or, if it’s not present,
#  the resulting file name there).
#
include_fname_pfx_to_mdata_title=no


 # Save the encoded file with colons (:) and other windows-unfriendly
#  characters replaced with dots (.) in the filename. This way windows
#  programs could be able to download the file from the internet without
#  renaming it (in Windows™ a colon is an invalid character for a file name).
#  If you’re dualbooting, then enabling this option will allow to launch
#  the videos from the drive.
#
create_windows_friendly_filenames=no



                      #  Miscellaneous options  #

 # Default scaling
#
#  Enable, if you want encoded videos to be not larger than the specified
#  resolution. This option enables a kind of “auto-scale” and differs from
#  the command line option “scale” by that it doesn’t force (fixate) the
#  resolution, so Nadeshiko (and Nadeshiko-mpv) will be able to choose
#  a smaller bitrate-resolution profile, if it will be necessary to fit
#  the duration.
#
#  When set to “no”, the scaling factor (and whether it is necessary
#  to apply it) would be determined at runtime.
#
#  Possible values are 2160p, 1440p, 1080p, 720p, 576p, 480p, 360p and “no”.
#
scale=no


 # Determines, how generous should Nadeshiko be when distributing
#  bitrate to cropped videos
#
#  Cropped videos are a bit special, because they may or may not capture the
#  most motion (and thus bit rate, the actual data of the full-sized frames).
#
#  Thus in most cases it’s best to treat a cropped part as a full-sized
#  frame to preserve quality. Another reason is that at lower resolutions
#  (576p and below) cropped videos may be of such a small size, that the
#  proportionally calculated bit rate would be so low, that the resulting
#  quality would be harmed as if it’s an .swf or a .gif from AD 2004.
#
crop_uses_profile_vbitrate=yes


 # Most often than not the last frame to be looped comes after a some kind of
#  action on screen. Even if it’s a small one, it still creates an impression
#  of a movement on the screen. The cases when the movement doesn’t have an
#  effect are only on low-contrast cadres, where it could as well be over-
#  looked, or on not-so low in terms of contrast, but rather noisy (like grey
#  rain), where particular details are blurred in the overall “noise”. Anyway,
#  what concerns the last frame to be looped, is that the human eye does never
#  perceive at once, that the video turned to a still frame. The brain somehow
#  expects the movement to continue, and thus about one second is lost on the
#  brain catching up and focusing on the object in the frame. The perceived
#  duration of the looped frame is thus perceived shorter, than you wanted to
#  see it. To battle this negative effect, here’s an option that adds a compen-
#  satory second, which is added to the loop duration by default. It increases
#  the factual duration of the looped fragment (and the total duration too,
#  of course), but the perceived duration is closer to what was expected (in
#  most cases). An example when this isn’t needed, would be when the looped
#  frame is close to the scene change (a totally different picture conveys it
#  to the brain, that it should (re)focus on the new picture).
#
loop_last_frame_compensatory_second=no
