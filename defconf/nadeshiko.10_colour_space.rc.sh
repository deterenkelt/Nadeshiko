#  nadeshiko.10_colour_space.rc.sh
#
#  Parameters for setting up colour space targets and nuances of colour
#  space conversion.



                       #  Target colour spaces  #

 # In case you’d need to create your own target colour space
#
#  0. Perhaps, if you’ve missed the “preserve_colour_spaces” option in the con-
#     figuration file, it may be what you looked for?
#
#  1. Create a list as an associative array like the ones below. The name must
#     follow the pattern “target_colour_space_YOURCUSTOMNAME”. Latin letters,
#     numbers and the underscore characters are the only allowed symbols for
#     the name. An associative array requires declaration as specified in the
#     corresponding file metaconf/nadeshiko.10_colour_space.rc.sh. Declaration
#     may be put anywhere before assignment.
#
#  2. To get the values for “matrix_coefs”, “primaries” and the “tone_curve”,
#     refer to the [indices] specified for the following lists
#       - known_cspace_matrix_coefs_for_filter_zscale
#       - known_primaries_for_filter_zscale
#       - known_tone_curves_for_filter_zscale
#       - known_colour_ranges_for_filter_zcale
#     in the metaconf file specified above.
#
#  3. As for the colour range, most of the content uses limited range, so “tv”
#     goes as the most used format and hence the option, that guarantees com-
#     patibility. Having been stuck with 8 bits per colour channel (for the
#     sake of compatibility, again), it doesn’t really make a difference, whe-
#     ther the  range is full  or not. If you’re going to choose “pc” or the
#     full range, make sure that you’re not going to send the video to some-
#     one, who would like to watch it on a TV, connecting their PC or smart-
#     phone to it.
#
#  4. Finally, you must remember, that conversion of the colour space from one
#     standard to another involves such parts of those standards like maximum
#     resolution, frame rate, the type and amount of channels, amount of bits
#     per channel, chroma subsampling. For example, if the fragment should be
#     encoded to the 1080p resolution, BT.601 cannot be the target colour
#     space: the standard offers a maximum of 720 lines per frame.
#
#  5. For reasons that are not clear yet, preserving the “pc” aka “full”
#     colour range is not possible. Even though handling of colour range is
#     left to ffmpeg, and the output is tagged as “pc” afterwards, the picture
#     gets “washed”, bleak, because the result is (supposedly) encoded in the
#     limited “tv” range, and the tag “pc” (full range) stretches the colour
#     spectrum.
#        One possible reason is that codecs ignore -color_range. It was given
#     as input as output and as both, with different combinations, but the
#     result didn’t change, which leads to a thought that it may be ignored.
#     Interesting fact: a “pc” source encoded with libvpx-vp9 to webm, would
#     be automatically tagged by ffmpeg as “tv”. With libx264 and MP4 the “pc”
#     tag would be conveyed to the encoded file. (The result is the same in
#     both cases, “washed out” colours).
#        Another possible reason is that some filter might affect the range.
#     But this happens even when no option like -filter or -filter_complex
#     is used.
#        Finally the suspicion falls on the -pix_fmt option and how the codecs
#     treat the input colour space. (The source that always undergoes this
#     weird contrast loss is in grey10le pixel format.)
#
target_colour_space_bt709=(
	[matrix_coefs]='bt709'
	[primaries]='bt709'
	[tone_curve]='bt709'
	[colour_range]='tv'  # Read note 5 above!
)

target_colour_space_bt2020_cl=(
	[matrix_coefs]='bt2020_cl'
	[primaries]='smpte2084'
	[tone_curve]='bt2020'
	[colour_range]='tv'  # Read note 5 above!
)


 # Options that tweak tone mapping for HDR → SDR conversion
#
#  Zscale nominal peak luminance. 0…10000. 100 is chosen for conversion to
#  fit the SDR display characteristic and presumes that the source video is
#  a bluray or a stream. If the source is your own camera, you may want to
#  copy this line to a custom config and set it to a higher value.
#
ffmpeg_filter_zscale_npl=100


 # Tone mapping algorithm (possible values for the “tonemap” parameter of the
#  “tonemap” filter, “…,tonemap=tonemap=<THIS>,…”). From the FFmpeg docs:
#   - “none” — do not apply any tone map, only desaturate overbright pixels;
#   - “clip” – hard-clip any out-of-range values. Use it for perfect color
#        accuracy for in-range values, while distorting out-of-range values;
#   - “linear” – stretch the entire reference gamut to a linear multiple
#        of the display;
#   - “gamma” – fit a logarithmic transfer between the tone curves;
#   - “reinhard” – preserve overall image brightness with a simple curve,
#        using nonlinear contrast, which results in flattening details and
#        degrading color accuracy;
#   - “hable” – preserve both dark and bright details better than reinhard,
#        at the cost of slightly darkening everything. Use it when detail
#        preservation is more important than color and brightness accuracy;
#   - “mobius” – smoothly map out-of-range values, while retaining contrast
#        and colors for in-range material as much as possible. Use it when
#        color accuracy is more important than detail preservation.
#
ffmpeg_filter_tonemap_alg='hable'


 # If you chose something other than “hable” for the above option, then that
#  algorithm may be tuned further with an additional switch (which is passed
#  as another parameter to the “tonemap” filter). So the resulting filter
#  string it’s like this: “…,tonemap=tonemap=NOT_HABLE:param=<THIS>,…”).
#
#  The value for “param” is a floating point number, that adjusts the function
#  within the algorithm, which is performing the tone mapping. If you decipher
#  the FFmpeg docs, which provide a confusing explanation of this parameter,
#  you can get to the following:
#   - for “none” it’s ignored;
#   - for “linear” it specifies the scale factor to use while stretching.
#       Default is 1.0;
#   - for “gamma” it specifies the exponent of the function. Default is 1.8;
#   - for “clip” it specifies an extra linear coefficient to multiply into
#       the signal before clipping. Default to 1.0;
#   - for “reinhard” it specifies the local contrast coefficient at the disp-
#       lay peak. Default to 0.5, which means that in-gamut values will be
#       about half as bright as when clipping;
#   - for “hable” it’s ignored;
#   - for “mobius” it specifies the transition point from linear to mobius
#       transform. Every value below this point is guaranteed to be mapped
#       1:1. The higher the value, the more accurate the result will be,
#       at the cost of losing bright details. Default to 0.3, which due to
#       the steep initial slope still preserves in-range colors fairly accu-
#       rately.
#
ffmpeg_filter_tonemap_param_value='1.0'


 # Whether to add the “normalize” filter to the chain. It is supposed to apply
#  an additional restretching of contrast to correct the darkening or whitening
#  of the colours in the frame, which is left as the result of applying a tone
#  mapping algorithm (as the tone mapping is inevitable in HDR to SDR conver-
#  sion).
#
#  It’s disabled by default, because while it does make the frame lighter,
#  it harms the details and adds a yellow-ish tone (close to sepia) to the
#  picture. Details in lighter tones (like rain drops) suffer most. More
#  details can be found in Release notes for v2.28.0.
#
#  If enabled, “normalize” will run /only/ when colour space conversion is
#  to be performed and only if on top of that the conversion would involve
#  change in the contrast range (HDR→SDR).
#
ffmpeg_filter_use_normalize=no


 # The “smoothing” parameter for the “normalize” filter. Read the comment to
#  “normalize” above before considering with this option.
#
#  The option takes value in two forms:
#    - an integer in range 0…99999, which is the number of frames that the
#      “normalize” filter averages over;
#    - “NxFR”, where FR is a literal “FR” (which stands for “frame rate”) and
#      N is a multiplier, an integer number. Thus it’s a means to specify the
#      number of frames in relation to how much frames are shown per second,
#      rather than some fixed value (the frame rate in the source may range
#      from ≈24 to ≈120 after all).
#
ffmpeg_filter_normalize_smoothing='2xFR'
