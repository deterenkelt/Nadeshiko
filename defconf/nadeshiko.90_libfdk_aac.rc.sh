#  nadeshiko.90_libfdk_aac.rc.sh
#
#  Parameters for encoding with Fraunhofer FDK AAC audio codec.
#  When possible, VBR is used. However, this codec has a limited number of
#  VBR modes, so what’s lacking is substituted with CBR.



 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  FFmpeg wiki puts libfdk_aac as “lower or equal” to libvorbis in the terms
#  of quality, so it uses bitrate settings close to libvorbis and libopus.
#  https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
#
#  -vbr 0 corresponds to AACENC_BITRATEMODE = CBR.
#
#  “libfdk_aac” accepts arbitrary values for -cutoff, stays close to the spe-
#  cified value.
#
#  A note on VBR modes:
#
#    -vbr    VBR or CBR     max. bit rate¹   low-pass freq. cut²
#
#       0           CBR                 ?           17500(?) Hz
#       1           VBR           40 kbps              13050 Hz
#       2           VBR           64 kbps              13050 Hz
#       3           VBR           96 kbps              14260 Hz
#       4           VBR          128 kbps              15500 Hz
#       5           VBR          192 kbps              not cut³
#
#    ¹ for stereo, both channels.
#    ² ffmpeg’s -cutoff, if specified, overrides the value.
#    ³ actually the range gets cut at 22 kHz.
#
#    http://wiki.hydrogenaud.io/index.php?title=Fraunhofer_FDK_AAC#Bitrate_Modes
#
#  libfdk_aac adjusts the highest possible frequency of the sound (do not con-
#  fuse this with sample rate) depending on the bit rate setting in effect:
#    - at -vbr 5 the frequencies are *not* cut;
#    - at -vbr 4 the frequencies are cut on 15500 Hz;
#    - at -vbr 3 they would be cut to 14620 Hz, but Nadeshiko uses CBR
#      for that bit rate, see below;
#
#  “libfdk_aac” accepts arbitrary values for -cutoff, stays close to the spe-
#  cified value.
#
libfdk_aac_profiles=(
	[256]="-b:a 256k  -vbr 0  -cutoff 20000"
	[224]="-b:a 224k  -vbr 0  -cutoff 20000"
	[192]="-vbr 5  -cutoff 20000"
	[160]="-b:a 160k  -vbr 0  -cutoff 20000"
	[128]="-vbr 4  -cutoff 20000"             #  >=128k is recommended by FFmpeg wiki
	[112]="-b:a 112k  -vbr 0  -cutoff 18000"
	[96]=" -vbr 3  -cutoff 14000"             #  Lowest usable by FFmpeg wiki
	[64]=" -b:a 64k  -vbr 0  -cutoff 8000"    #  Good enough for 360p.
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
libfdk_aac_is_fairly_good_at='128'


bitres_profile_360p+=(
	[libfdk_aac_profile]=64
)

bitres_profile_480p+=(
	[libfdk_aac_profile]=96
)

bitres_profile_576p+=(
	[libfdk_aac_profile]=112
)

bitres_profile_720p+=(
	[libfdk_aac_profile]=112
)

bitres_profile_1080p+=(
	[libfdk_aac_profile]=128
)

bitres_profile_1440p+=(
	[libfdk_aac_profile]=128
)

bitres_profile_2160p+=(
	[libfdk_aac_profile]=128
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
libfdk_aac_64_size_deviations_per_duration=(
	[3]=.0497
	[10]=.0492
	[30]=.0525
	[60]=.0512
	[240]=.0475
)

libfdk_aac_96_size_deviations_per_duration=(
	[3]=.0680
	[10]=.2080
	[30]=2.5525
	[60]=5.3882
	[240]=7.9082
)

libfdk_aac_112_size_deviations_per_duration=(
	[3]=.0265
	[10]=.0257
	[30]=.0307
	[60]=.0262
	[240]=.0265
)

libfdk_aac_128_size_deviations_per_duration=(
	[3]=.3157
	[10]=.7555
	[30]=3.6815
	[60]=7.9755
	[240]=18.2882
)

libfdk_aac_160_size_deviations_per_duration=(
	[3]=.0225
	[10]=.0212
	[30]=.0247
	[60]=.0220
	[240]=.0200
)

libfdk_aac_192_size_deviations_per_duration=(
	[3]=.0595
	[10]=.1905
	[30]=1.1587
	[60]=2.4090
	[240]=1.1130
)

libfdk_aac_224_size_deviations_per_duration=(
	[3]=.0190
	[10]=.0172
	[30]=.0207
	[60]=.0187
	[240]=.0200
)

libfdk_aac_256_size_deviations_per_duration=(
	[3]=.0172
	[10]=.0165
	[30]=.0190
	[60]=.0180
	[240]=.0200
)
#
#  libfdk-aac 2.0.1, FFmpeg libavcodec 58.134.100 / 58.134.100