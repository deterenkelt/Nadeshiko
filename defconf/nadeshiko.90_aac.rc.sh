#  nadeshiko.90_aac.rc.sh
#
#  Parameters for encoding with FFmpeg’s built-in aac audio codec.
#  Only CBR.



 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  FFmpeg wiki puts native aac implementation below libfdk_aac in terms
#  of quality: https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
#  At the same time “man ffmpeg-codecs” states, that “Its quality is on par
#  or better than libfdk_aac at the default bit rate of 128kbps”.
#
#  Settings for the aac codec use CBR, because all that is known about VBR
#  in this implementation, is these two points:
#    - “Effective range for -q:a is around 0.1-2”;
#    - “This VBR is experimental and likely to get even worse results
#      than the CBR”. — FFmpeg wiki/HQaudio
#
#  “aac” accepts arbitrary values for -cutoff, but seems to round them up.
#  For example, -cutoff 14567 will produce a file, whose spectrogram would be
#  cut slightly above 15’000.
#
aac_profiles=(
	#  FFmpeg’s aac is seen as slightly worse than Opus, Vorbis and FDK_AAC,
	#  but still is the nearly same quality, so uses similar bitrates.
	[256]="-b:a 256k -cutoff 20000"
	[224]="-b:a 224k -cutoff 20000"
	[196]="-b:a 196k -cutoff 20000"
	[160]="-b:a 160k -cutoff 20000"
	[128]="-b:a 128k -cutoff 20000"  #  Recommended by FFmpeg wiki.
	[112]="-b:a 112k -cutoff 18000"  #  Assumed lowest usable bit rate.
	                                 #    FFmpeg wiki must be referring
	                                 #    to HE-AAC in > usable range ≥ 32Kbps
	                                 #   (depending on profile and audio).
	[96]='-b:a 96k -cutoff 15000'    #  Seems to be not so bad. At least well
	                                 #    enough to go with the <=480p videos.
	[64]='-b:a 64k -cutoff 8000'     #  Good enough for 360p.
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
aac_is_fairly_good_at='128'


bitres_profile_360p+=(
	[aac_profile]=96
)

bitres_profile_480p+=(
	[aac_profile]=96
)

bitres_profile_576p+=(
	[aac_profile]=112
)

bitres_profile_720p+=(
	[aac_profile]=112
)

bitres_profile_1080p+=(
	[aac_profile]=128
)

bitres_profile_1440p+=(
	[aac_profile]=128
)

bitres_profile_2160p+=(
	[aac_profile]=128
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
aac_64_size_deviations_per_duration=(
	[3]=.0075
	[10]=.0295
	[30]=.0662
	[60]=.1272
	[240]=.5210
)

aac_96_size_deviations_per_duration=(
	[3]=.0022
	[10]=.0200
	[30]=.0410
	[60]=.0852
	[240]=.3412
)

aac_112_size_deviations_per_duration=(
	[3]=.0070
	[10]=.0185
	[30]=.0392
	[60]=.0827
	[240]=.2740
)

aac_128_size_deviations_per_duration=(
	[3]=.0055
	[10]=.0145
	[30]=.0302
	[60]=.0695
	[240]=.2925
)

aac_160_size_deviations_per_duration=(
	[3]=.0100
	[10]=.0142
	[30]=.0310
	[60]=.0755
	[240]=.2742
)

aac_196_size_deviations_per_duration=(
	[3]=.0112
	[10]=.0252
	[30]=.0460
	[60]=.1565
	[240]=.4645
)

aac_224_size_deviations_per_duration=(
	[3]=.0105
	[10]=.0300
	[30]=.0550
	[60]=.2107
	[240]=.7262
)

aac_256_size_deviations_per_duration=(
	[3]=.0162
	[10]=.0970
	[30]=.1240
	[60]=.1595
	[240]=.6547
)
#
#  FFmpeg libavcodec 58.134.100 / 58.134.100