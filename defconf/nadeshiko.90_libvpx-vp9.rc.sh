#  nadeshiko.90_libvpx-vp9.rc.sh
#
#  Parameters for encoding with the libvpx-vp9 video codec.



                        #  libvpx-vp9 options   #
                       #  for advanced users   #
                                              #   wiki: tinyurl.com/VP9wiki

 # Bitrate-resolution profiles
#                                   Read on the wiki
#                                   - what profiles do: tinyurl.com/fkv2nhtm
#                                   - how profiles work: tinyurl.com/NadeProf
#
#  Sometimes, the encoded file wouldn’t fit into the maximum size – its reso-
#    lution and/or bit rate may be too big for the required size. The higher is
#    the resolution, the bigger video bit rate it should have. It’s impossible
#    to fit 1 minute of dynamic 1080p into 10 megabytes, for example. A lower
#    resolution, however – 720p or a lower one – would need less bit rate,
#    so it may fit!
#  Nadeshiko will calculate the bit rate, that would fit into the requested
#    file size, and then will try to find an appropriate resolution for it.
#    If it could be the native resolution, Nadeshiko will not scale. Other-
#    wise, to avoid making an artefact-ridden video, that was cut from a 1080p
#    video for example, Nadeshiko will make a clean one, but in a lower reso-
#    lution.
#  For each resolution, desired and minimal video bit rates are calculated.
#    Desired is the upper border, which will be tried first, then attempts
#    will go down to the lower border, the minimal value. If the maximum fit-
#    ting bit rate wasn’t in the range of the resolution, the next will be
#    tried.
#
#
 # The lower border when seeking for the TARGET video bit rate.
#  Calculated as a percentage of the desired bit rate. Highly depends
#    on CPU time, that encoder spends. If you speed up the encoding
#    by putting laxed values in libvpx_pass*_cpu_used, you should raise
#    the percentage here.
#  Don’t confuse with libvpx_minrate and libvpx_maxrate.
#
libvpx_vp9_minimal_bitrate_pct='60%'


bitres_profile_360p+=(
	[libvpx-vp9_desired_bitrate]=276k
	# [libvpx-vp9_min_q]=35
	[libvpx-vp9_max_q]=40
)

bitres_profile_480p+=(
	[libvpx-vp9_desired_bitrate]=750k
	# [libvpx-vp9_min_q]=33
	[libvpx-vp9_max_q]=39
)

bitres_profile_576p+=(
	[libvpx-vp9_desired_bitrate]=888k
	# [libvpx-vp9_min_q]=33
	[libvpx-vp9_max_q]=38
)

bitres_profile_720p+=(
	[libvpx-vp9_desired_bitrate]=1024k
	# [libvpx-vp9_min_q]=32
	[libvpx-vp9_max_q]=37
)

bitres_profile_1080p+=(
	[libvpx-vp9_desired_bitrate]=1800k
	# [libvpx-vp9_min_q]=31
	[libvpx-vp9_max_q]=36
)

#  Experimental.
bitres_profile_1440p+=(
	[libvpx-vp9_desired_bitrate]=6000k
	# [libvpx-vp9_min_q]=24
	[libvpx-vp9_max_q]=34
)

#  Experimental.
bitres_profile_2160p+=(
	[libvpx-vp9_desired_bitrate]=12000k
	# [libvpx-vp9_min_q]=15
	[libvpx-vp9_max_q]=25
)


 # Pixel format. Sets properties of AVFrame structure, such as the colour
#  space (if not set), chroma subsampling and the number of bits per colour
#  channel.
#
#  Possible values: everything that “ffmpeg -h encoder=libvpx-vp9”
#  mentions in the “Supported pixel formats”.
#
#  Recommended value: yuv420p or yuv444p. VP9-capable browsers usually have
#  no troubles decoding 4:4:4 chroma, but as the most content around is enco-
#  ded with 4:2:0 chroma, it is kind of overkill to use 4:4:4 (at least, with-
#  out using it together with a 10-bit format, but that would harm compatibi-
#  lity.) Mind also, that a higher chroma would increase the file size, and
#  you’ll have to lower the bit rates and increase the reserve for the the
#  muxing overhead (see defconf/nadeshiko.90_webm.rc.sh), because the bitrate-
#  resolution profiles, that Nadeshiko uses, are calculated for the 4:2:0
#  chroma values. If you won’t do that, Nadeshiko will strike re-encodes more
#  often (unless you’d encode with the “unlimited” option set).
#
libvpx_vp9_pix_fmt='yuv420p'


 # Tile columns
#
#  Places an upper constraint on the number of tile-columns,
#  which libvpx-vp9 may use.
#
#  “Tiling splits the video into rectangular regions, which allows
#   multi-threading for encoding and decoding. The number of tiles
#   is always a power of two. 0=1 tile, 1=2, 2=4, 3=8, 4=16, 5=32.”
#  “Tiling splits the video frame into multiple columns, which slightly
#   reduces quality but speeds up encoding performance. Tiles must be
#   at least 256 pixels wide, so there is a limit to how many tiles
#   can be used.”
#  “Depending upon the number of tiles and the resolution of the output frame,
#   more CPU threads may be useful. Generally speaking, there is limited value
#   to multiple threads when the output frame size is very small.”
#  “The requested tile columns will be capped by encoder based on image size
#   limitation. Tile column width minimum is 256 pixels, maximum is 4096.”
#
#  Possible values: 0–6. Must be greater than zero for -threads to work. The
#  docs on Google Devs (2017) recommend to calculate it (see below).
#
libvpx_vp9_tile_columns=6


 # Same as tile-columns, but for rows
#
#  Usefulness is uncertain.
#  “Number of tile rows to use, log2 (set to 0 while threads > 1)” — from
#   the output of “vpxenc --help”. It’s uncertain, whether /the codec/ sets
#   tile rows to zero, when -threads is set to something greater than 0, or
#   it’s /the user, who must set it to 0/, when using -threads. Anyhow, this
#   feature seems to be of no use.
#  “Partitioning into columns works as expected, but partitioning into rows
#   does not” – the bitstream specification (2016).
#
#  Nadeshiko doesn’t use tile rows.
#
#libvpx_vp9_tile_rows


 # Maximum number of CPU threads to use
#  Manual setting. This value is ignored, if $libvpx_vp9_adaptive_tile_columns
#  (below) is set to “yes”.
#
libvpx_vp9_threads=8


 # When enabled, Nadeshiko calculates the values for tile-columns and threads
#  adaptively to the video resolution as the docs on Google Devs recommend:
#  tile-columns = log2(target video width ÷ tile-column minimum width)
#  threads = 2^tile-columns × 2
#
libvpx_vp9_adaptive_tile_columns=yes


 # Frame parallel decodability features
#
#  It’s one of the two features (along with tile-columns), that optimises
#  playback for toasters. Claims to speed up the decoding on 30%.
#
#  “Turns off backward update of probability context”. In other docs they say
#  that this option “allows staged parallel processing of more than one video
#  frames in the decoder”. Spotted to hurt quality in the tests.
#
#  Values: 0 (disabled), 1 (enabled)
#
libvpx_vp9_frame_parallel=0


 # Encode speed / quality profiles
#
#  --deadline ffmpeg option specifies a profile to libvpx. Profiles are
#     “best” – alike to libx264 “placebo” preset, although this one is
#        not as pointless. FFmpeg wiki, however, warns against that
#        this parameter is misnamed and can produce result worse than “good”.
#     “good” – around “slow”, “slower” and “veryslow”, the optimal range.
#     “realtime” – fast encoding for streaming, poor quality as always.
#        “Setting --good and --cpu-used=0 will give quality that is usually
#        very close to and even sometimes better than that obtained with
#        --best, but the encoder will typically run about twice as fast.”
#
#  --cpu-used (aka --speed) tweaks the profiles above. 0, 1 and 2 give best
#        results with “best” and “good” profiles. “Values greater than 0 will
#        increase encoder speed at the expense of quality. Changes in this
#        value influences, among others, the encoder’s selection of motion
#        estimation methods.”
#     This is of CRITICAL importance to get better quality. (Though still
#        in order for libvpx to use CPU, corresponding quality constraints
#        must be set first.)
#     −9…9 are the values for VP9 (−16…16 was allowed for VP8, −8…8 for
#        libvpx < 1.8), but negative values are _never_ used in examples
#        “…settings 0–4 apply for <…> good and best, with 0 being the highest
#        quality and 4 being the lowest. Realtime valid values are 5–8;
#        lower numbers mean higher quality.”
#     Using cpu_used=0 for the first pass didn’t produce any visible
#        differences from when video is encoded with cpu_used=4.
#
#  Changing values here will lower the codec efficiency (details per MiB
#  ratio), so the value in  libvpx_vp9_minimal_bitrate_pct  (see above)
#  will have to be increased.
#
libvpx_vp9_pass1_deadline=good
libvpx_vp9_pass1_cpu_used=4
libvpx_vp9_pass2_deadline=good
libvpx_vp9_pass2_cpu_used=0


 # Alternate reference frames aka “hidden golden frames to stash blocks”.
#  A two-pass feature.
#
#  “Setting auto-alt-ref and lag-in-frames >= 12 will turn on VP9’s alt-ref
#   frames, a VP9 feature that enhances quality.”
#  “When --auto-alt-ref is enabled, the default mode of operation is to either
#   populate the buffer with a copy of the previous golden frame, when this
#   frame is updated, or with a copy of a frame derived from some point
#   of time in the future.”
#  “Use of --auto-alt-ref can substantially improve quality in many
#   situations (though there are still a few where it may hurt).” (2016)
#  “For noisy content, we recommend increasing the number, and strength, of
#   alt-ref frames. Alternate reference frames are ‘invisible’ frames, never
#   shown to the user, but which are used as a reference when creating the
#   final frames” (2017)
#  “Although 8 slots are maintained, any particular frame can make use of
#   at most 3 reference frames” — the bitsream specification (2016)
#
#  All in all, this looks like a feature handy for regular use, but whose
#  higher values are intended to battle noisy (raw camera?) content.
#
#  Research made to compare encodes with auto-alt-ref 1 and 6 revealed that
#  there will be no drastic change for short videos (< 5 seconds), be they
#  dynamic of static, however, longer videos suffer less artefacts with
#  this option is set to 6. See on the wiki: <https://codeberg.org/deterenkelt/
#  Nadeshiko/wiki/Researches – VP9-auto‑alt‑ref-1-and-6>
#
#  Codec default
#    libvpx-1.7.x:  0 – disabled.
#                   1 – enabled.
#
#    libvpx-1.8+:   0 – disabled.
#                   1–6 – higher values help avoid artefacts in dynamic
#                         scenes on low-bitrate videos.
#
#  (Nadeshiko automatically lowers this value to 1, when it detects libvpx
#   version below 1.8.)
#
#  Older devices (smartphones and tablets, mostly), issued before 2015…2016
#  may not support libvpx-1.8 features and may be able to play only the files
#  encoded with -auto-alt-ref 1.
#
libvpx_vp9_auto_alt_ref=6


 # Tests have shown, that -auto-alt-ref 6 adds blur to the picture in compa-
#  rison to -auto-alt-ref 1. Presumably the reason is that the value of “6”
#  makes the encoder rely more on alternate reference frames, which, in their
#  turn, undergo procession by the noise reduction filter. (Makes sense, since
#  after they’re smoothed, those pieces put into alternate frames may fit to
#  more actual frames.) The real benefit of auto-alt-ref 6 reveals itself when
#  the file size grows: this option saves the time spent on encoding and actu-
#  ally improves overall quality – but this is seen only on comparatively long-
#  er files. Short fragments don’t receive the real benefit from -auto-alt-
#  ref 6, but receive its blur. Short fragments get to be encoded a little
#  faster with this option, but for encodes that take only a couple minutes,
#  that doesn’t matter, while the drawback of blur on the picture does.
#
#  The value is the duration of output fragment, in seconds, below which
#  -auto-alt-ref 1 will be used instead of -auto-alt-ref 6.
#
libvpx_vp9_allow_autoaltref6_only_for_videos_longer_than_sec=30


 # When encoder chooses, which alternate reference frames to peruse for encod-
#  ing the current frame, the lag determines, how far the alt-ref frame
#  be spaced away. Has an effect only when -auto-alt-ref is enabled too.
#
#  Possible values are 0…25. 25 is the codec default. 16 is recommended
#  by webmproject.org (2016) and the docs on Google developers (2017).
#
#  In terms of quality, the lesser value here would indeed be best. But to
#  maximise the compression efficiency, there is no good recipe. On one hand,
#  longer distances may push the encoder to reuse more averaged data samples
#  in more samples, which would hurt the quality. On the other hand, in very
#  dynamic videos or in content with quick alternations (changes back and
#  forth within 2 seconds) may aid compression and won’t worsen the quality
#  noticeably. Such moments would be rare though. Another thing of note is
#  the moment of scene changes. How the lag would affect the blend effect is
#  unclear. All in all it seems reasonable to stay within ±0.5 second bounds
#  and use the closest neighbours for alt-ref frames.
#
#  Do mind, that the VP9 level restricts the minimum value:
#  https://www.webmproject.org/vp9/levels/
#
libvpx_vp9_lag_in_frames=16


 # “Set altref noise reduction max frame count.” — man ffmpeg-codecs
#  “AltRef max frames (0…15)” — vpxenc --help
#  A value of 5 recommended in 2017 docs.
#
libvpx_vp9_arnr_maxframes=5


 # “Set altref noise reduction filter strength” — man ffmpeg-codecs
#  “AltRef filter strength (0…6)” — vpxenc --help
#  A value of 3 recommended in 2017 docs.
#
libvpx_vp9_arnr_strength=3


 # “Set altref noise reduction filter type:
#   backward, forward, centered” — man ffmpeg-codecs
#  “AltRef filter type” — vpxenc --help
#
#  Nadeshiko doesn’t use this option by default,
#  but you may uncomment and use it.
#
#libvpx_vp9_arnr_type=



 # Maximum interval between key frames (ffmpeg -g).
#  Webmproject.org recommends to use “-g 9999” (2016)
#  VP9 docs on developers.google.com recommend “up to 240” (2017)
#  Whichever you would pass to ffmpeg, the resulting webm would have a key
#  frame on each 95±5th frame.
#
libvpx_vp9_kf_max_dist=240


 # Isn’t implemented in libvpx-vp9.
#
#libvpx_vp9_kf_min_dist


 # Lower and upper bit rate borders for a GOP, in % from target bit rate.
#  50% and 145% are recommended by the docs on Google Devs.
#
libvpx_vp9_minsection_pct=50
libvpx_vp9_maxsection_pct=145


 # Data rate overshoot (maximum) target (%)
#  How much deviation in size from the target bit rate is allowed.
#  −1…1000, codec default is −1.
#
libvpx_vp9_overshoot_pct=0


 # Data rate undershoot (minimum) target (%)
#
#  How much deviation in size from the target bit rate is allowed.
#  −1…100, codec default is −1.
#
libvpx_vp9_undershoot_pct=0


 # CBR/VBR bias (0=CBR, 100=VBR)
#
#  Codec default is unknown. Nadeshiko doesn’t use it by default
#  (but may apply).
#
#libvpx_vp9_bias_pct=0


 # Adaptive quantisation mode
#
#  A segment-based feature, that allows encoder to adaptively change quantisa-
#  tion parameter for each segment within a frame to improve the subjective
#  quality.
#
#  This option is never used in examples. Tests have shown, that on poor/fast
#  encodes aq-mode=3 gave a significant quality boost, but on proper Q values
#  the difference vanes.
#
#  Some people find that using 0 here produces less artefacts, than other
#  options.
#
#  According to Paul Wilkins in an answer¹ on Google Groups (2015):
#    “variance” – “adapts the quantizer within frames based upon the spatial
#       variance of each region. Low variance regions get a lower quantizer
#       than high variance regions”
#    “complexity” – “adapts the quantizer using as its main trigger the actual
#       number of bits spent coding each region or block, relative to a target
#       average for the frame.”
#    “cyclic refresh” is an “experimental option for live video conferencing”
#  Also “variance” and “complexity” modes “…at the moment only update the seg-
#  mentation map once for each group of frames (defined by the positioning of
#  alternate reference frames) rather than every frame. This is to reduce the
#  overhead and works on the assumption that the map will usuaully remain quite
#  stable for a small group of frames. <…> [‘variance’ and ‘complexity’ modes]
#  both give a strong improvment in psychovisual quality for some content (for
#  example ‘ParkJoy’ from the derf set). They almost always hurt metrics such
#  as PSNR, but in some cases help the SSIM metric. We are continuing to eval-
#  uate AQ methods and their impact on pschovisual and objective metrics over
#  a wider corpus of clips.”
#  ¹ https://groups.google.com/a/webmproject.org/g/codec-devel/c/TNMUvKDCWtg
#
#  0 – off (default)
#  1 – variance
#  2 – complexity
#  3 – cyclic refresh
#  4 – equator360
#
libvpx_vp9_aq_mode=0


 # Maximum keyframe bit rate as a percentage of the target bit rate
#
#  This value controls additional clamping on the maximum size of a keyframe.
#  It is expressed as a percentage of the average per-frame(!) bit rate, with
#  the special (and default) value 0 meaning unlimited, or no additional
#  clamping beyond the codec’s built-in algorithm.
#  Nadeshiko currently doesn’t use it.
#
#libvpx_vp9_max_intra_rate


 # Maximum I-frame bit rate as a percentage of the target bit rate
#
#  This value controls additional clamping on the maximum size of an inter
#  frame. It is expressed as a percentage of the average per-frame bit rate,
#  with the special (and default) value 0 meaning unlimited, or no additional
#  clamping beyond the codec’s built-in algorithm.
#  Nadeshiko currently doesn’t use it; ffmpeg has no option for it.
#
#libvpx_vp9_max_inter_rate


 # Source video type
#  “0” – default type, any video
#  “1” – screen (for streaming one’s desktop?)
#  “2” – film (to preserve film grain?), helps to avoid excessive blurriness.
#
libvpx_vp9_tune_content=2


 # Static threshold
#
#  Should be understood literally: it’s an option to suppress noise on live
#  translations, where real movements are low. Causes regions on image not
#  being updated, which leads to artefacts. If you looked for motion-estima-
#  tion methods, it’s defined by --cpu-used in VP9.
#  “In most scenarios this value should be set to 0.”
#  Nadeshiko doesn’t use this.
#
#libvpx_vp9_static_threshold


 # Commonly assumed to determine the number of bits in chroma and its type.
#
#  Parameter value: bits per sample; chroma subsampling
#  “0” – 8 bits/sample;         4:2:0
#  “1” – 8 bits/sample;                4:2:2,  4:4:4
#  “2” – 10 or 12 bits/sample;  4:2:0
#  “3” – 10 or 12 bits/sample;         4:2:2,  4:4:4
#
#  The confusing description of profiles on the webmproject website
#  https://www.webmproject.org/docs/encoder-parameters/
#  should be disregarded: refer to the 2016 specification instead:
#  https://storage.googleapis.com/downloads.webmproject.org/docs/vp9/
#  vp9-bitstream-specification-v0.6-20160331-draft.pdf, section 5.19.
#
#  Nadeshiko doesn’t use this option: FFmpeg chooses an appropriate profile
#  automatically based on the “ffmpeg_pix_fmt” option).
#
#libvpx_vp9_profile


 # Determines maximum resolution, bit rate, ref frames etc.
#  For 1080p the minimal value is 4.0.
#  https://www.webmproject.org/vp9/levels/
#
libvpx_vp9_level=4.1


 # An optimisation to pass synthetic tests better
#
#  SSIM is considered closer to the human eye perception.
#  Values are: “psnr” or  “ssim”
#  SSIM is still not supported in libvpx-1.8.0.
#  > Failed to set VP8E_SET_TUNING codec control: Invalid parameter
#  > Option --tune=ssim is not currently supported.
#
# libvpx_vp9_tune=ssim


 # Row based multi-threading
#
#  “Allows use of up to 2× thread as tile columns.” ― 2017 docs
#  “Currently, the improved MT encoder works in 1-pass/2-pass good quality
#   mode encoding at speed 0, 1, 2, 3 and 4.” ― <https://groups.google.com/a/
#   webmproject.org/forum/#!topic/codec-devel/oiHjgEdii2U
#
#  Non-deterministic! two encodes will not be same. Rows here refer to blocks
#  of pixels, not the tile rows. This option doesn’t depend on -tile-rows.
#
#  Enabling row-mt improves encoding speed to ¹/₆–2 times, and produces video
#  with a better perceptible quality and an equal SSIM score up to thousands.
#  See “Tests. VP9: row-mt on and off” in the wiki for more details.
#
#  Possible values: ‘0’ – off, ‘1’ – on.
#
libvpx_vp9_row_mt=1


 # Token parts (slices)
#  -tile-columns predecessor in VP8. Has no function in VP9.
#  vpxenc-1.7.0 --help, 2016 docs and WebM SDK explicitly omit support in VP9.
#  Topic in Google groups has a hint: “you can substitute --tile-columns
#     for --token-parts for VP9.”
#  ― https://groups.google.com/a/webmproject.org/d/msg/webm-discuss/ARlIuScFQFQ/j4xnhEpJCAAJ
#  Nadeshiko doesn’t use it.
#
#libvpx_vp9_token_parts


 # Place for user-specified output ffmpeg options
#
#  These options are added to the end of the encoding line after the the
#  common output options, but before the mandatory ones. So the options
#  specified here can override most of the output parameters, except those,
#  that control -pass 1, -pass 2, -sn, and the output file name. Mandatory
#  options are defined in the particular encoding module. Normally there’s
#  nothing that needs to be added. These lists simply provide a possibility
#  to add custom ffmpeg options for specific cases (if you need extra filters,
#  mapping, control of the container format, metadata – such things).
#
#  Example of assignment:
#      =(-key value  -other-key "value with spaces")
#
libvpx_vp9_pass1_extra_options=()
libvpx_vp9_pass2_extra_options=()


 # The amount of overhead in target file size, which is considered critical
#  and switches off Nadeshiko’s quantiser-adjusted encoding in favour of a
#  more simple bare bitrate-based one.
#
#  The value is specified as the percentage of the target file size, upon
#  reaching which the overhead will be considered critical.
#
libvpx_vp9_critical_overhead_pct=15



                     #  Unsafe libvpx-vp9 options  #

 # Manual quantiser control (use at your own risk!)
#  !
#  !  To get predictable bit rate and file size, Nadeshiko uses only values
#  !    in “libvpx-vp9_min_q” and “libvpx-vp9_max_q” variables from the
#  !    resolution profiles above (bitres_profile_NNNp). Uncommenting
#  !    “libvpx_vp9_cq_level”, “libvpx_vp9_min_q” or “libvpx_vp9_max_q”
#  !    turns on manual control over the quantiser; min_q and max_q from
#  !    the resolution profiles will be ignored.
#  !  Precise file size and bit rate are not guaranteed. Issues caused
#  !    by the usage of manual control will be closed as WONTFIX.
#  !  Manual quantiser control is for the people who have read Nadeshiko wiki,
#  !    understand what they’re doing this for and know, what they will get.
#  !
#  Quantiser threshold (ffmpeg -crf / vpxenc --end-usage=cq, --cq-level)
#  0–63. Default is 10.
#  Recommended values:
#  ⋅ 23 for CQ mode by webmproject.org;
#  ⋅ 15–35 in “Understanding rate control modes…”;
#  ⋅ From 31 for 1080 to 36 for 360p in the docs on Google Devs.
#
#libvpx_vp9_cq_level=23


 # Quantiser constraints
#  Because apparently, -crf without -qmax likes 63 too much w
#  These two parameters are the main levers on part with -deadline and
#    -cpu-used, tuning everything else without them is futile.
#
#libvpx_vp9_min_q=23
#libvpx_vp9_max_q=23



 # Some descriptions of libvpx-vp9 options are quoted from
#  https://sites.google.com/a/webmproject.org/wiki/ffmpeg/vp9-encoding-guide
#  and https://developers.google.com/media/vp9/
#  which impose restrictions on sharing, so here are the licences:
#  - text: http://creativecommons.org/licenses/by/3.0/
#  - code samples: http://www.apache.org/licenses/LICENSE-2.0
