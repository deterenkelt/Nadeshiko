#  nadeshiko.90_ac3.rc.sh
#
#  Parameters for encoding with ac3 audio codec.



 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  FFmpeg wiki estimates the quality, that ac3 encoder produces, as equal
#  or inferior to libmp3lame, so using it is not recommended. It’s here for
#  the compatibility with old hardware players.
#  https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
#
#  Considerations for ac3 stem from its proximity to libmp3lame in terms of
#  quality. However, the page on FFmpeg wiki was written a decade ago and
#  the modern ac3 encoder in FFmpeg doesn’t seem to perform that bad.
#
#  The official docs on AC-3 recommend 192k for 2-channel audio. More or less
#  modern DVDs and BD, that feature 6-channel tracks, use maximum bitrate,
#  which is 640k. Converting this value with the formula for 2 channel audio
#  (see FFmpeg wiki), this would result in ≈ 213 kbit stereo. Older DVDs have
#  192k. FFmpeg wiki recommends using 160k for encoding, which places the
#  “transparency” bar below the lowest bitrate in the origin. 213k bitrate
#  is above the “lowest and recommended” 160k on FFmpeg wiki, and still above
#  192k, the bitrate below transparency point used in libmp3lame. Makes sense.
#
#  More information:
#    - http://www.digitalfaq.com/forum/video-conversion/1465-how-encode-audio.html
#    - http://wiki.hydrogenaud.io/index.php?title=AC3
#
#  ac3 accepts arbitrary values for -cutoff, but expect the result
#  be within a ±500 Hz margin.
#
ac3_profiles=(
	[256]="-b:a 256k -cutoff 20000"
	[224]="-b:a 224k -cutoff 20000"
	[192]="-b:a 192k -cutoff 20000"  # Recommended in the official docs.
	[160]="-b:a 160k -cutoff 18000"  # Lowest and recommended on FFmpeg wiki.
	[128]="-b:a 128k -cutoff 14000"  # Lowest for 2-channel audio in the
	                                 #  official docs.
	[112]="-b:a 112k -cutoff 8000"   # Good enough for mp3. Actually, ac3
	                                 #  sounds not that bad even at 64k
	                                 #  (if encoded in 2023).
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
ac3_is_fairly_good_at='192'


bitres_profile_360p+=(
	[ac3_profile]=112
)

bitres_profile_480p+=(
	[ac3_profile]=128
)

bitres_profile_576p+=(
	[ac3_profile]=160
)

bitres_profile_720p+=(
	[ac3_profile]=192
)

bitres_profile_1080p+=(
	[ac3_profile]=192
)

bitres_profile_1440p+=(
	[ac3_profile]=192
)

bitres_profile_2160p+=(
	[ac3_profile]=192
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
ac3_112_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

ac3_128_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

ac3_160_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

ac3_192_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

ac3_224_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)

ac3_256_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0050
	[30]=.0050
	[60]=0
	[240]=.0100
)
#
#  FFmpeg libavcodec 58.134.100 / 58.134.100



 # Last patent on this codec has expired only in 2017, so its implementations
#  are expected to be not full or lacking features (why VLC and ffmpeg could
#  have it). Supposedly an echo of this issue is that some features in ac3 en-
#  coder in ffmpeg are not available:
#
#    -dheadphone_mode mode
#      Dolby Headphone Mode. Indicates whether the stream uses Dolby Headphone
#      encoding (multi-channel matrixed to 2.0 for use with headphones). Using
#      this option does NOT mean the encoder will actually apply Dolby Head-
#      phone processing.
#                                                  ― ffmpeg-codecs manual page
#

 # 19 allowable bitrates can be found in the ATSC specification:
#  http://www.atsc.org/wp-content/uploads/2015/03/A52-201212-17.pdf, page 117.
#

 # Note, that ffmpeg ac3 codec implementation doesn’t have an option,
#  that would represent the most significant bit in “bit_rate_code”, which
#  should determine, whether the bitrate is the constant or only the maximum,
#  that may be reached.
#