#  nadeshiko.90_libmp3lame.rc.sh
#
#  Parameters for encoding with libmp3lame audio codec.
#  Only VBR.



 # Setting compression level.
#  0..9, the lower the value, the higher the quality and the slower is
#  the encode. (Seems to be set to 0 by default.)
#
libmp3lame_common_options="-compression_level 0"


 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  FFmpeg wiki puts libmp3lame after Opus, Vorbis and AAC codecs in terms
#  of quality, so using it is not recommended. It’s here only as a fallback
#  for (half-)broken FFmpeg installations.
#  https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
#
#  Recommendations are taken from the Hydrogen audio wiki:
#  http://wiki.hydrogenaud.io/index.php?title=LAME#Recommended_encoder_settings
#  and conversion of “lame” -V option values to ffmpeg -aq is taken from
#  the FFmpeg wiki https://trac.ffmpeg.org/wiki/Encode/MP3
#
libmp3lame_profiles=(
	#  The top 3 profiles are considered transparent, so a conversion to
	#  320k CBR is just wasteful. (trac.ffmpeg.org/wiki/Encode/MP3)
	[245]="$libmp3lame_common_options -q:a 0 -cutoff 20000"  # 220–260k
	[225]="$libmp3lame_common_options -q:a 1 -cutoff 20000"  # 190–250k
	[192]="$libmp3lame_common_options -q:a 2 -cutoff 20000"  # 170–210k. Highest recommended.
	[175]="$libmp3lame_common_options -q:a 3 -cutoff 20000"  # 150–195k
	[165]="$libmp3lame_common_options -q:a 4 -cutoff 18000"  # 140–185k
	[130]="$libmp3lame_common_options -q:a 5 -cutoff 14000"  # 120–150k
	[115]="$libmp3lame_common_options -q:a 6 -cutoff 8000"   # 100–130k. Lowest recommended.
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
libmp3lame_is_fairly_good_at='192'


bitres_profile_360p+=(
	[libmp3lame_profile]=115
)

bitres_profile_480p+=(
	[libmp3lame_profile]=130
)

bitres_profile_576p+=(
	[libmp3lame_profile]=165
)

bitres_profile_720p+=(
	[libmp3lame_profile]=175
)

bitres_profile_1080p+=(
	[libmp3lame_profile]=192
)

bitres_profile_1440p+=(
	[libmp3lame_profile]=192
)

bitres_profile_2160p+=(
	[libmp3lame_profile]=192
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
libmp3lame_115_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libmp3lame_130_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libmp3lame_165_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libmp3lame_175_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.1145
	[60]=.0075
	[240]=.0025
)

libmp3lame_192_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0187
	[30]=.2362
	[60]=.4275
	[240]=.0025
)

libmp3lame_225_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.1832
	[60]=.3902
	[240]=.0025
)

libmp3lame_245_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0907
	[30]=.5497
	[60]=1.1235
	[240]=.0025
)
#
#  Lame 3.100, FFmpeg libavcodec 58.134.100 / 58.134.100