#  nadeshiko.90_libvorbis.rc.sh
#
#  Parameters for encoding with libvorbis audio codec.
#  Only VBR.



 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  Libvorbis controls variable bit rate with the quality setting, and though
#  there are specific options to set minimum and maximum bit rates, there is
#  no guide or recommendation on what these should be for a given quality.
#

#
libvorbis_profiles=(
	#  The FAQ on vorbis.com oddly lacks a word about AAC or Opus, but as it’s
	#  better than MP3, it’s transparency should be definitely reached at 192k.
	[256]="-q:a 8 -cutoff 20000"
	[224]="-q:a 7 -cutoff 20000"
	[192]="-q:a 6 -cutoff 20000"
	[160]="-q:a 5 -cutoff 20000"  # “near-CD-quality” (vorbis.com/faq)
	[128]="-q:a 4 -cutoff 20000"  #  Recommended by FFmpeg wiki
	[112]="-q:a 3 -cutoff 18000"  # “significantly better fidelity than
                                  #   MP3 @128k”(vorbis.com/faq)
	[96]=" -q:a 2 -cutoff 14000"  #  Lowest usable by FFmpeg wiki
	[80]=" -q:a 1 -cutoff 8000"
	[64]=" -q:a 0 -cutoff 8000"   #  Good enough for 360p

	#  FFmpeg wiki restricts bottom usable range with 96k
	#  https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
libvorbis_is_fairly_good_at='128'


bitres_profile_360p+=(
	[libvorbis_profile]=96
)

bitres_profile_480p+=(
	[libvorbis_profile]=96
)

bitres_profile_576p+=(
	[libvorbis_profile]=112
)

bitres_profile_720p+=(
	[libvorbis_profile]=112
)

bitres_profile_1080p+=(
	[libvorbis_profile]=128
)

bitres_profile_1440p+=(
	[libvorbis_profile]=128
)

bitres_profile_2160p+=(
	[libvorbis_profile]=128
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
libvorbis_64_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_80_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_96_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_112_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_128_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0525
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_160_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_192_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_224_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libvorbis_256_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)
#
#  Libvorbis 1.3.6, FFmpeg libavcodec 58.134.100 / 58.134.100