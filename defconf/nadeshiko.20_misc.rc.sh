#  nadeshiko.20_misc.rc.sh
#
#  Miscellaneous options
#



 # Nadeshiko always guarantees, that encoded file fits the maximum size. How-
#  ever, there are other parameters, that affect compatibility – and if it’s
#  of any concern, Nadeshiko may inspect the file thoroughly to confirm, that
#  the encoded fragment corresponds to what was requested.
#
#  This is an old option, that was implemented only for libx264. It did some
#  tests and printed results to console. The need to run these checks has long
#  gone, so this is a sort of mothballed engine. Turn at your own risk.
#
#pedantic=off


 # Whether to show the time spent on encoding.
#
time_stat=off


 # Container
#
#  “mp4” – use for libx264.
#  “webm” – use for libvpx-vp9.
#  “auto” – pick appropriate container based on the chosen set of A/V codecs.
#
#  The option can force a specific container, but in the current state of
#  things, the codec set essentially determines the choice. It was introduced
#  to attempt libx264 with libopus in an MP4 container, or even in WebM (those
#  were seldom appearing in the wild), but in the end strict rules were fol-
#  lowed, that is, H.264 goes to MP4, VP9 goes to WebM, no exceptions. In the
#  future, if the supported codec list would be extended, new mixes of video
#  and audio codecs may appear, which would leave ambiguity about the desired
#  container, – and then this option may come useful.
#
container=auto


 # Threshold of the seconds-per-scene ratio between static and dynamic videos
#
#  Before the conversion, fragments are analysed for this ratio. If the ratio
#  would be lower, than the one specified to this option, then the cut will
#  be considered “dynamic”, if the ratio happens to be equal to it or higher – 
#  the cut will count as “static”.
#
#  Only the highest (aka “desired”) values are chosen from bitrate-resolution
#  profiles, if the video is “dynamic”. The “static” videos, vice versa, may
#  make use of the range, to which available bit rate falls in a particular
#  profile.
#
#  The number specified to this option refers to the test, which runs at the
#  start of the program. It determines how long – on average – one scene would
#  last within the time span of the fragment to be cut. Values higher than
#  that number (longer scenes) mark the fragment as a (primarily) “static”
#  one, below this number – as “dynamic”.
#
#  It’s not recommended to lower the default value, because if the fragment
#  would be more “dynamic”, than expected, it would also require more bitrate.
#  Requiring more bitrate will probably make Nadeshiko overshoot the maximum
#  file size. Moreover, the dynamic videos suffer the most from insufficiently
#  “cut” bit rate, i.e. using ranges, defined in the bitres profiles (instead
#  of maximum values) will hardly lead to a pleasant result.
#
#
#
video_sps_threshold=3.0


                             #  Seekbefore  #

 # Forces the decoder to start reconstructing frames not on the Time1 position,
#  but earlier, according to the specified amount of seconds.
#
#  Usually, there’s no need in this option. In most cases, when it’d be needed,
#  the “seekbefore” mode activates automatically, with an appropriate amount of
#  seconds passed to decoder to “rewind” it back a bit. The typical cases in-
#  clude transport and program streams (.ts, .m2ts, .mpts…), HDR content with
#  non-constant luminance and a particular case with the source encoded with
#  a specific version of a certain encoding library. Artefacts related to key
#  frames typically appear like garbage in the first seconds of the encoded
#  file. If you would notice something like that – it may make sense to try
#  out this option.
#
#  This option replaces the old one – “fulldecode”, which had a similar pur-
#  pose, but could point the decoder only to 0:00.000. The old behaviour may
#  be forced with “seekbefore=0”.
#
#  This option is a companion to the command line one, of the same name, and
#  is intended as a “hard default” for cases when the command line option would
#  have to be used over and over again. However, it should be pretty rare to
#  encounter a situation, when one would have to use the command line “seekbe-
#  fore”, and even more rare – when it would come to using this config option.
#  So only enable it, if you have a lot of such “broken” files (or encoding
#  jobs).
#
#seekbefore=30


 # The options for “seekbefore” mode determine, how much seconds before Time1
#  should the decoder start decoding to encode the framgent appropriately,
#  without garbage or jittery in the reuslting content. A thorough explanation
#  may be found in Release notes for v2.28.0.
#
#  “seekbefore_for_HDR_NCL_s” allows to point the decoder earlier for HDR NCL
#  source (high-dynamic range video with non-constant luminance). While decod-
#  ing the earlier frames isn’t necessary to build the one that begins on
#  Time1, it helps the algorithms averaging luminosity. Usually those require
#  a modest amount of frames (5…120), so 5 seconds as the default value covers
#  this need.
#
#  “seekbefore_for_cmdline_s” sets the default for the case when the “seekbe-
#  fore” mode is requested from the command line. Provided, that it should be
#  an unordinary case, the default here is set higher, to two minutes.
#
#  “seekbefore_in_search_for_a_keyframe_s”. This option is for the cases, when
#  “seekbefore” mode is (automatically) enabled for the source, which is a
#  transport or a program stream (e.g. a .ts, an .m2ts file or another), or
#  when there’s a known encoder bug, which needs to be avoided, and the purpose
#  of the “seekbefore” mode is to run the decoder through the closest keyframe.
#
#  The value may be zero, in which case Nadeshiko points the decoder at the
#  beginning of the file (0:00.000). If the value specified to a “seekbefore”
#  option would exceed the duration bounds (say, if the Time1 would be at three
#  seconds from the start, and “seekbefore” option would want to set it two
#  minutes back), then the decoder would be pointed to 0:00.000.
#
#  The longest time for the value takes preference. The value for “seekbefore”,
#  which may be set via command line, will override any config file options.
#
seekbefore_for_HDR_NCL_s=5
seekbefore_for_cmdline_s=120
seekbefore_in_search_for_a_keyframe_s=120


                       #  Looping the last frame  #

 # Use an alternative set of filters to loop the last frame, skipping the pri-
#  mary method of doing it. (Primary – “tpad → overlay”, alternative – “; set-
#  pts → select → loop → setpts ; concat”.) Each method has its upsides and
#  downsides:
#
#    Primary                            Alternative
#      - implemented later;               - implemented first;
#      - it either works, or it           - if it doesn’t work right off
#        doesn’t;                           the bat, there are automatic
#                                           adjustments and retries;
#      - works more stably with           - works the same way with both
#        libvpx-vp9, with libx264           codecs;
#        ffmpeg may throw errors about
#        P-slices that are in fact B;
#      - never has PTS mistakes           - on a rare occasion it may add an
#        (an extra frame from the be-       extra frame at the end of the loop-
#        ginning of the fragment never      ed part; the extra frame would be
#        appears);                          from the beginning of the fragment.
#                                           (This bug was observed only on lon-
#                                           ger loops, like 15 second ones, and
#                                           never on normal lengths of 3–5 sec-
#                                           onds – and even that was rare);
#      - has literally no overhead,       - has some overhead in the filter
#        thus faster processing.            chain. The overhead isn’t big, how-
#                                           ever, the difference is not so
#                                           noticeable.
#
#  With both methods the quality of the frames in the looped part should be
#  equally good (as good as it may be with that codec, bit rate etc.).
#
#  When the primary method fails, the alternative will be automatically
#  selected, the counter of attempts will be dropped back to zero and the
#  video would be attempted again, as if the alternative method was chosen.
#  (What’s described in this paragraph is the default behaviour, it doesn’t
#  require enabling this option here to work. The option is basically for the
#  cases when the shorter, faster, usually bugless primary method doesn’t
#  work for you – maybe because you use .mp4, or old ffmpeg, or for whatever
#  other reason hit into errors more than expected.)
#
loop_last_frame_use_alter_method=no


 # (has an effect only if the alternative method is chosen) When encoding
#  a fragment with its last frame looped, this value determines the maximum
#  number of attempts to take, before quitting.
#
#  The possible reason why the encoding of the last frame may be unsuccessful,
#  is that the “setpts” filter needs to adjust the PTS in order to let the loop
#  filter fit the cycled frame into the duration. That adjustment is adding
#  an amount of duration, that is slightly less than the loop duration, to
#  the PTS-STARTPTS. That amount is measured in TB units (as PTS is), so what
#  we subtract is the amount of seconds (actually, fractions of a second), that
#  a frame is displayed at the frame rate, that our encoded fragment uses. In
#  most cases the amount that needs to be subtracted is one frame. But some-
#  times we’d need to subtract an amount equal to two frames. This depends on
#  the source. In theory, this number can be bigger, hence Nadeshiko employs
#  an algorithm to redo the encoding by atomatically increasing the number of
#  reserved frames (actually, their time, in seconds).
#
#  How much this increase will be, is determined by the algorithm (see below). 
#
#  Possible values: 1…125
#
loop_last_frame_max_attempts=5


 # (has an effect only if the alternative method is chosen) Chooses, which way
#  the number of reserved frames should be increased, when the fragment is to
#  be encoded with its last frame looped.
#    - “linear”: the first value for reserved frames is 1, and with each next
#  attempt it will be incremented by 1, thus 2, 3, 4, 5…
#    - “exponential”: the first reserved value is 2, and every next attempt
#  will increase the number twofold, thus to 4, to 8, to 16 and so on.
#
loop_last_frame_incr_reserved_frames=linear

