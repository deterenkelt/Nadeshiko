#  nadeshiko.10_deinterlace.rc.sh
#
#  Parameters for setting up the deinterlacing filter(s). The filter to be
#  applied is chosen by the “deinterlace_filter” option in the configuration
#  file. The options for each filter are keps here.
#
#  Whether the filter should be used, is determined automatically at runtime
#  by reading the metadata in the source video. The command line option “dein-
#  terlace” overrides that and overrides the config file option with the same
#  name.
#


 # Whether to run the deinterlacing filter on the source video.
#
#  If you’re playing a video, and it looks like somebody has raked the frames,
#  you need a deinterlace filter.
#
#  Keep in mind:
#     - your video player may have some force-enabled deinterlace filter(s),
#         software or hardware-supported;
#     - applying more than one deinterlacing filter, as well as attempting to
#         deinterlace a video, that was not originally interlaced, will pro-
#         duce artefacts on the screen;
#     - when the video was interlaced (e.g. by the disc manufacturer), its
#       frames suffered a lossy conversion. The reconstruction is also lossy.
#       So don’t expect that the picture on the screen would have an ideal
#       look: some jitter will remain, and some colours may be off.
#
#  Mpv has a built-in deinterlacing filter, typically switched with the ‘d’
#  key. The command for input.conf is “cycle deinterlace”. If you use Nadeshi-
#  ko-mpv, “deinterlace” will be one of the options read from the mpv and pas-
#  sed to Nadeshiko’s encoding backend. (In case you didn’t know, that this
#  is also can be controlled from within mpv.)
#
#  Possible values:
#    “auto” – enable the filter, if the source metadata report that the video
#        is deinterlaced. (This is the default)
#    “on” – force-applies the deinterlace filter;
#    “off” – never use the deinterlace filter, ignore interlacing properties
#        from the metadata.
#
#  Mind, that the command line option “deinterlace” overrides the value speci-
#  fied in the configuration file.
#
#  Be careful about interlaced videos. Though having “auto” does its job, you
#  should expect, that some videos may be not tagged with proper metadata.
#  Thus it’s best to double check, what you’re going to encode with and check
#  the resulting file. If you see the “comb” effect and have doubts, then the
#  quickest way to assure the best result is, of course, to launch mpv, switch
#  deinterlace “on” or “off” (the ‘d’ key by default, as you remember), and
#  to launch Nadeshiko-mpv, which will read properties from mpv and set the
#  deinterlace as a command line option for Nadeshiko. This would be the most
#  easy, direct and correct way to deal with interlaced videos.
#
deinterlace='auto'


 # Currently, only bwdif.
#
#  Using another filter is possible, if you copy this entire section, which
#  defines “deinterlace_filter” and “deinterlace_filter_FILTERNAME_opts”,
#  setting the former to another ffmpeg filter name, and using it in the
#  place of FILTERNAME in the latter, then specify all the necessary options
#  (according to the FFmpeg filters documentation) in the same fashion as
#  shown below.
#
deinterlace_filter='bwdif'
#
deinterlace_filter_bwdif_opts=(
	#
	#  https://ffmpeg.org/ffmpeg-filters.html#bwdif
	#

	 # The interlacing mode to adopt
	#  Possible values:
	#    - “send_frame” – output one frame for each frame,
	#    - “send_field” (the default) – output one frame for each field.
	#
	[mode]='send_field'

	 # The picture field parity assumed for the input interlaced video
	#  Possible values:
	#    - “tff” – assume the top field is first,
	#    - “bff” – assume the bottom field is first,
	#    - “auto” (the default) – perform automatic detection; when the meta-
	#      data do not specify the order, assume top frame is first.
	#
	[parity]='auto'


	 # Specify which frames to deinterlace
	#  Possible values:
	#    - “all” (the default) – deinterlace all frames,
	#    - “interlaced” – deinterlace only those that are marked as interlaced.
	#
	[deint]='all'
)

