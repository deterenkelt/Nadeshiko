#  nadeshiko.90_libopus.rc.sh
#
#  Parameters for encoding with libopus audio codec.
#  Only VBR.



 # FFmpeg default. Here for clarity.
#
libopus_common_opts='-vbr on  -compression_level 10'


 # Nadeshiko will try to use a higher acodec profile, than the one specified
#  in the bitres profile, if there would be enough space (calculated by the
#  average bit rate, represented in the array index below).
#
#  Cutoff for libopus accepts fixed values: 20k, 12k, 8k, 6k and 4k.
#
libopus_profiles=(
	#  128k is very close to transparency, 160–192 are transparent with very
	#  low chance of artifacts (https://wiki.hydrogenaud.io/index.php?title=
	#  Opus)
	[256]="$libopus_common_opts -b:a 256k -cutoff 20000"
	[224]="$libopus_common_opts -b:a 224k -cutoff 20000"
	[192]="$libopus_common_opts -b:a 192k -cutoff 20000"
	[160]="$libopus_common_opts -b:a 160k -cutoff 20000"
	[128]="$libopus_common_opts -b:a 128k -cutoff 20000"  #  Max for music on opus-codec.org
	[112]="$libopus_common_opts -b:a 112k -cutoff 20000"
	[96]=" $libopus_common_opts -b:a  96k -cutoff 20000"
	[64]=" $libopus_common_opts -b:a  64k -cutoff 12000"  #  Recommended by FFmpeg wiki
	[48]=" $libopus_common_opts -b:a  48k -cutoff 8000"   #  Lowest for music on opus-codec.org

	#  FFmpeg wiki restricts bottom usable range with 32k, but the Opus page
	#  with examples uses a minimum of 48k and maximum of 128k for music.
	#  https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio
	#  http://opus-codec.org/examples/
)

 # The bitrate at which codec reaches audio transparency or at least performs
#  transcode without striking artefacts, good for most cases. The  commonly
#  recommended bitrate for general-purpose use.
#
#  Technically, this value is lower than 128k, the recommended bitrate
#  is even twice as lower: 64k. However, the value chosen is 128k, because
#  the full audio transparency lies closer to 160k. Other codecs will use
#  even more spaace, so why not permit some better quality, especially if
#  the encodec source is a music video.
#
libopus_is_fairly_good_at='128'


bitres_profile_360p+=(
	[libopus_profile]=48
)

bitres_profile_480p+=(
	[libopus_profile]=64
)

bitres_profile_576p+=(
	[libopus_profile]=96
)

bitres_profile_720p+=(
	[libopus_profile]=112
)

bitres_profile_1080p+=(
	[libopus_profile]=128
)

bitres_profile_1440p+=(
	[libopus_profile]=128
)

bitres_profile_2160p+=(
	[libopus_profile]=128
)


 # Size deviations
#
#  For the cuts whose duration is less than or equal to the specified [dura-
#  tion in seconds]="a corresponding deviation" will be applied.
#
#  Deviations are expressed in seconds of playback at the current bit rate
#  (it’s the profile in the name of the variable) to be used as padding when
#  Nadeshiko calculates space required for tracks.
#
libopus_48_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_64_size_deviations_per_duration=(
	[3]=.2000
	[10]=.1137
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_96_size_deviations_per_duration=(
	[3]=.1037
	[10]=.0242
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_112_size_deviations_per_duration=(
	[3]=.0962
	[10]=.0180
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_128_size_deviations_per_duration=(
	[3]=.0900
	[10]=.0195
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_160_size_deviations_per_duration=(
	[3]=.0590
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_192_size_deviations_per_duration=(
	[3]=.0222
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_224_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)

libopus_256_size_deviations_per_duration=(
	[3]=.0025
	[10]=.0025
	[30]=.0025
	[60]=.0025
	[240]=.0025
)
#
#  Opus 1.3.1, FFmpeg libavcodec 58.134.100 / 58.134.100
