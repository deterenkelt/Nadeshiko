#  nadeshiko.90_libx264.rc.sh
#
#  Parameters for encoding with the libx264 video codec.



                         #  libx264 options     #
                        #  for advanced users  #
                                              #   wiki: tinyurl.com/H264wiki

 # Bitrate-resolution profiles
#                                   Read on the wiki
#                                   - what profiles do: tinyurl.com/fkv2nhtm
#                                   - how profiles work: tinyurl.com/NadeProf
#
#  Sometimes, the encoded file wouldn’t fit into the maximum size – its reso-
#    lution and/or bit rate may be too big for the required size. The higher is
#    the resolution, the bigger video bit rate it should have. It’s impossible
#    to fit 1 minute of dynamic 1080p into 10 megabytes, for example. A lower
#    resolution, however – 720p for example, would need less bit rate,
#    so it may fit!
#  Nadeshiko will calculate the bit rate, that would fit into the requested
#    file size, and then will try to find an appropriate resolution for it.
#    If it could be the native resolution, Nadeshiko will not scale. Other-
#    wise, to avoid making an artefact-ridden video, that was cut from a 1080p
#    video for example, Nadeshiko will make a clean one, but in a lower reso-
#    lution.
#  For each resolution, desired and minimal video bit rates are calculated.
#    Desired is the upper border, which will be tried first, then attempts
#    will go down to the lower border, the minimal value. Steps are in 100k.
#    If the maximum fitting bit rate wasn’t in the range of the resolution,
#    the next will be tried.
#
#
 # The lower border when seeking for the TARGET video bit rate.
#  Calculated as a percentage of the desired bit rate. Highly depends
#    on CPU time, that encoder spends. If you speed up the encoding
#    by putting laxed values in libx264_preset, you should raise the
#    percentage here.
#
libx264_minimal_bitrate_pct='60%'


bitres_profile_360p+=(
	[libx264_desired_bitrate]=500k
)

bitres_profile_480p+=(
	[libx264_desired_bitrate]=1000k
)

bitres_profile_576p+=(
	[libx264_desired_bitrate]=1500k
)

bitres_profile_720p+=(
	[libx264_desired_bitrate]=2000k
)

bitres_profile_1080p+=(
	[libx264_desired_bitrate]=3500k
)

#  Experimental.
bitres_profile_1440p+=(
	[libx264_desired_bitrate]=11900k
)

#  Experimental.
bitres_profile_2160p+=(
	[libx264_desired_bitrate]=23900k
)


 # Pixel format. Sets properties of AVFrame structure, such as the colour
#  space (if not set), chroma subsampling and the number of bits per colour
#  channel.
#
#  Possible values: everything that “ffmpeg -h encoder=libx264” mentions
#  in the “Supported pixel formats”.
#
#  Recommended value: yuv420p. Browsers poorly support higher chroma in MP4,
#  and using anything higher hardly makes sense without using it together with
#  a 10-bit format. And a 10-bit format would harm compatibility. Mind also,
#  that a higher chroma would increase the file size, and you’ll have to lower
#  the bit rates and increase the reserve for the the muxing overhead (see
#  defconf/nadeshiko.90_webm.rc.sh), because the bitrate-resolution profiles,
#  that Nadeshiko uses, are calculated for the 4:2:0 chroma values. If you
#  won’t do that, Nadeshiko will strike re-encodes more often (unless you’d
#  encode with the “unlimited” option set).
#
libx264_pix_fmt='yuv420p'


 # Speed / quality preset
#  “veryslow” > “slower” > “slow” > shit > “medium” > … > “ultrafast”.
#
libx264_preset='veryslow'


 # Preset tune. Less significant than the preset itself
#  “animation” / “film” > shit > “fastdecode” > “zerolatency”.
#  “animation” ≈ “film” + more B-frames.
#
libx264_tune='animation'


 # Profile enables encoding features. A decoder must also support them
#  in order to play the video
#  high444p > high10 > high > main > baseline
#  Browsers do not support high10 or high444p.
#
libx264_profile='high'


 # Video codec profile level
#  Higher profiles optimise bit rate better.
#  Very old devices may require level 3.0 and baseline profile.
#
libx264_level='4.2'


 # Group of pictures size (GoP size)
#
#  The shorter it is, the more key frames the encoded video will have. This
#  option alone doesn’t determine, however, how many key frames there will be.
#  If you want to sacrifice quality for size, increase “libx264_keyint_min”
#  (see below).
#
#  When set on “auto”, lets x264 to determine appropriate GoP size. The default
#  is around 250. Meaning, that with the regular frame rate of 23.976 the GoP
#  boundary and an I-frame would be placed /no less than/ every 10.4 seconds.
#  And in those 10.4 seconds there may be 1, 2 or more GoPs.
#
#  Possible values: “auto” or a number in range 2…999999.
#
libx264_keyint=auto


 # The minimal distance between two key frames
#
#  When both `libx264_keyint` (see above) and this option are set to “auto”,
#  the minimal distance between key frames is calculated in relation to output
#  frame rate and source video scene complexity. Dynamic videos will have
#  keyint_min set as 1×frame_rate, while static ones – as 3×frame_rate. The
#  multipliers can be changes with two additional options below.
#
libx264_keyint_min=auto
libx264_keyint_min_dyn=1
libx264_keyint_min_sta=3


 # When enabled, allows B-frames reference other frames beyond the GoP boun-
#  dary. While this feature is not appropriate in streamable content, as it
#  breaks rewinding there, for static files it is of great use. (An .mp4 or
#  a .webm played over the internet still counts as playing a static file.)
#  Using open GoP allows libx264 to be more space-efficient, hence fit more
#  quality to the same disk space or use less space, when space isn’t the
#  problem. For a 10 MiB, 20 sec video the difference would comprise around
#  1 MiB.
#
#  Some prehistoric devices, which would require profile=baseline, may also
#  require you to disable open GoP.
#
libx264_opengop=on


 # Place for user-specified output ffmpeg options
#
#  These options are added to the end of the encoding line after the the
#  common output options, but before the mandatory ones. So the options
#  specified here can override most of the output parameters, except those,
#  that control -pass 1, -pass 2, -sn, and the output file name. Mandatory
#  options are defined in the particular encoding module. Normally there’s
#  nothing that needs to be added. These lists simply provide a possibility
#  to add custom ffmpeg options for specific cases (if you need extra filters,
#  mapping, control of the container format, metadata – such things).
#
#  Assignment example:
#      =(-key value  -other-key "value with spaces")
#
libx264_pass1_extra_options=()
libx264_pass2_extra_options=()


