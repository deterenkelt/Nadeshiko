#  lazily_archive_video.rc.sh
#
#  Configuration options for lazily_archive_vide.sh, a script accompanying
#  Nadeshiko suite.
#
#  This RC file uses bash syntax:
#    key=value
#  1. Quotes can be omitted, if the value is a string
#       without spaces.
#  2. The equals sign should stick to both key and value –
#       no spaces around “=”.
#  3. Flag options accept any form for on/off: yes/no, y/n,
#       true/false, t/f, 1/0, enabled/disabled, on/off too.
#       The case of letters doesn’t matter.
#
#  Nadeshiko wiki may answer a question before you ask it!
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Lazily-archive-video
#


 # Commands to call FFmpeg binaries
#
#  If these binaries cannot be found by default system paths, or if you’d like
#  to use custom builds, replace values with absolute paths to those custom
#  binaries.
#
ffmpeg='ffmpeg'
ffprobe='ffprobe'


 # Whether to speed up the video and how much
#
#  Multiplying by more than ×1.5 results in choppy audio in some fragments.
#  To preserve the speech completely understandable, it is recommended to
#  stay within ×1.5 bounds in the common case. Do mind, that some videos
#  come already sped up by a factor of 1.15…1.25.
#
#  Possible values: 1.0…2.0 with a step of 0.05
#  (when set to 1.0 the filters to speed up the video are not added)
#
default_speed=1.50


 # Whether to downscale
#
#  Usually most content (like lectures) would look good at 576p.
#
#  Possible values: 360, 480, 576, 720, 1080
#
default_scale=576


 # Quality (libx265 constant rate factor, CRF)
#
#  As we’re concerned more about retaining the average quality, and the concern
#  about the space is secondary, it is reasonable to use CRF encoding, and not
#  2-pass. (A fine-tuned 2-pass would indeed be better both in terms of quality
#  and space efficiency, but fine-tuning it requires more work on tailoring the
#  encoding options for each particular video.)
#
#  CRF below 24–26 will lose the benefit of better compression, that x265
#  provides in comparison to x264. However, if you worry about quality more
#  than about size (say, when you downscale the source), throwing more bits
#  to have something close to 1:1 quality may make sense.
#
#  CRF in range 15–18 is when you don’t care about disk space at all, and only
#  care about staying close to lossless.
#
#  CRF in range 24–28 is the normal encode, few visible artefacts, in most
#  cases you wouldn’t notice the difference, unless you stop encoded video
#  and the original one and compare.
#
#  CRF in range 29–35 will produce noticeable artefacts in abundance.
#
#  Possible values: 15…35. 28 is the libx265’s internal default.
#
default_quality=28


 # Audio bitrate
#
#  Speech is deemed less needy than music, when it concerns bit rate. For Opus
#  the lowest border for music is at 48k. Using a bit rate one “step” higher
#  guarantees, that the speech will stay recognisable, even after the audio
#  is sped up.
#
#  Possible values: “48k”, “64k”, “96k”, “112k”, “128k”, “160k”, “192k”,
#  “224k”, “256k”, “copy” (to use the original audio stream as is) and
#  “audio disabled” (to encode without an audio track)
#
default_audio_bitrate='96k'


 # Which pixel format to use
#
#  10 bits profiles help encoder to retain more details on the picture, this
#  doesn’t affect the file size much, but greatly increases the time for
#  encoding in comparison to 8-bit formats.
#
#  4:4:4 chroma subsampling can highly increase the preservation of colours
#  at the cost of increased disk space used. Usually, you don’t want diy videos
#  ot lectures to be preserved at that awesome level of quality and there’s
#  no  point in it. 4:4:4 chroma makes sense when you encode something gra-
#  phically rich, like close-up on paintings, architecture, other works of
#  art. Cinema etc. probably will benefit from this, but if you are going to
#  save a rip, better download something made with a 2-pass made by people
#  who know what they are doing.
#
#  Possible values: yuv420p, yuv444p, yuv420p10le, yuv444p10le.
#
default_pix_fmt='yuv420p10le'

