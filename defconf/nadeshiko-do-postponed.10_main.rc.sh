#  nadeshiko-do-postponed.rc.sh
#
#  Main configuration file for nadeshiko-do-postponed.sh. Copied as an example
#  to user’s configuration directory from defconf/.
#
#  RC file uses bash syntax:
#    key=value
#  Quotes can be omitted, if the value is a string without spaces.
#  The equals sign should stick to both key and value – no spaces around “=”.
#  Flags accept any form for on/off: yes/no, y/n, true/false, t/f, 1/0,
#  enabled/disabled. Case of letters doesn’t matter.
#
#  Nadeshiko wiki may answer a question before you ask it!
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki



 # Limits the number of CPUs available to encoding jobs
#
#  If specified, must be a number between 1 and the maximum number of proces-
#  sors found on the system. When the option is not specified, the value is
#  determined at runtime as the maximum number of available CPUs minus one.
#
#  If Nadeshiko-do-postponed is running in a terminal, then the CPU affinity
#  is adjustable with arrow keys.
#
#processors=3


 # OS process priority. −20…19, the lowest is the *higher* priority,
#    default OS priority is 0. Negative values require superuser privileges.
#  Not used in the default configuration.
#
#niceness_level='-20'


 # Defines verbosity level for the backend.
#  This option controls whether notifications would be sent after each
#    encoded video, or there will be just one “All jobs processed.”
#  Possible values:
#    “none” – Nadeshiko (the backend) will not send any desktop notifications.
#             (But Nadeshiko-do-postponed will still inform you, when all jobs
#              would be done.)
#    “error” – Send a desktop notification only when a video wasn’t encoded
#              successfully.
#    “all” – The normal behaviour, each time a video is encoded, a message
#            is sent. This allows to watch it as soon as it is ready.
#  Default: all
#
nadeshiko_desktop_notifications='all'


 # Forces the specified verbosity level for the encoding backend.
#  If set, then the value of $nadeshiko_desktop_notifications is ignored.
#
#  This variable is handy only for debugging problems, the line put as an
#  example increases the verbosity so as to allow  “set±x”  commands within
#  Nadeshiko code to work. Verbosity levels are described on the wiki:
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Troubleshooting#verbosity
#
#nadeshiko_verbosity=log=2,console=0,desktop=3


 # Sets icon for the dialogue windows
#  One of: “nade1”, ”nade2”, … “nade5” or “random”.
#
icon_set='nade3'


 # Whether to display the small table with keys to control the number of
#  available CPU cores and the keys to interrupt the batch processing.
#  For new users or those who are seldom using Nadeshiko, it’s probably
#  better to leave this reminder in place. Experienced users, who want
#  the output go straight to the jobs, may set this option to “no”.
#
show_control_keys_prompt=yes