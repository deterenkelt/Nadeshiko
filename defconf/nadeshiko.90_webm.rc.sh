#  nadeshiko.90_webm.rc.sh
#
#  Predicting the muxing overhead for WebM container.
#  More details are on the wiki:
#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Under-the-hood.-Overhead-prediction




 # Space to be reserved for muxing overhead, in ESP
#
#  Format: [frame count]=ESP
#  1 ESP is an equivalent of a space required to hold 1 second of video play-
#  back (and audio playback too, if audio track is present and its encoding
#  is requested).
#
#  The number of frames in the cut is compared sequentially to each frame
#  count in the array, starting with the lowermost one, and if the cut would
#  have more frames, than in said item, the corresponding ESP amount will be
#  assigned as an expected muxing overhead for that cut.
#
webm_space_reserved_frames_to_esp=(
	[0]=2

	#  These values are obsolete.
	#  https://codeberg.org/deterenkelt/Nadeshiko/wiki/Researches – Muxing-
	#  overhead#addendum-iv-futility
	#
	# [228]=2
	# [644]=3
	# [1417]=4
	# [1884]=5
	# [2354]=9
	# [2738]=12
)


 # Multiplier that’s to be applied to the excess file size in calculating the
#  the muxing overhead (happening when when the maximum file size was overshot).
#
#  The excess size in bytes is multiplied to this coefficient and then added
#  to a variable containing the anti-overshoot padding, whose value accumulates
#  on each overshooting.
#
#  Sane values for this coefficient lie in the range of 0.75…1.25. The smaller
#  the coefficient, the more precise the video and audio data could be fit
#  into the file size limit. At the same time, smaller coefficient guarantees
#  finer steps for the re-runs, and the finer are the steps, the more steps
#  it may take to fit a video in a particular size. Nadeshiko default settings
#  are chosen so that at the 0.75 coefficients there would be (in the common
#  case) at most 1, maximum 2 re-runs. A coefficient of 1.0 should eliminate
#  the most re-runs, but at the same time it will leave a significant chunk
#  of space unused (0.25…1 MiB). A coefficient of 1.25 is the lazy person’s
#  choice, who’d like to avoid re-runs as much as possible.
#
webm_muxing_ovh_coef=0.75